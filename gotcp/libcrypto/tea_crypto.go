package libcrypto

/*
#cgo CFLAGS: -I./
#cgo LDFLAGS: -L/root/go/src/github.com/HT_GOGO/gotcp/libcrypto -lteacrypto
#include <stdlib.h>
#include "TEACrypto.h"
*/
import "C"
import "unsafe"

const (
	CCryptoBufSize = 64 * 1024
)

func TEAEncrypt(plainText string, cryptoKey string) (cryptoText string) {
	pt := C.CString(plainText)
	defer C.free(unsafe.Pointer(pt))
	ptLen := len(plainText)
	ck := C.CString(cryptoKey)
	defer C.free(unsafe.Pointer(ck))

	var cryptoLen C.uint32_t
	cryptoBuf := unsafe.Pointer(C.malloc(CCryptoBufSize))
	defer C.free(cryptoBuf)

	C.xTEAEncryptWithKey(pt, C.uint32_t(ptLen), ck, (*C.char)(cryptoBuf), &cryptoLen)
	cryptoText = C.GoStringN((*C.char)(cryptoBuf), C.int(cryptoLen))
	return cryptoText
}

func TEADecrypt(cryptoText string, cryptoKey string) (plainText string) {
	ct := C.CString(cryptoText)
	defer C.free(unsafe.Pointer(ct))
	ctLen := len(cryptoText)
	ck := C.CString(cryptoKey)
	defer C.free(unsafe.Pointer(ck))

	var plainLen C.uint32_t
	plainBuf := unsafe.Pointer(C.malloc(CCryptoBufSize))
	defer C.free(plainBuf)
	C.xTEADecryptWithKey(ct, C.uint32_t(ctLen), ck, (*C.char)(plainBuf), &plainLen)
	plainText = C.GoStringN((*C.char)(plainBuf), C.int(plainLen))
	return plainText
}
