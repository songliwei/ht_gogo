package libcomm

/*
#cgo CFLAGS: -I ./cinclude/
#cgo LDFLAGS: -L/root/go/src/github.com/HT_GOGO/gotcp/libcomm -lneocomm
#include <stdlib.h>
#include "Attr_API.h"
*/
import "C"
import "unsafe"

func AttrAdd(name string, value int32) {
	cs := C.CString(name)
	defer C.free(unsafe.Pointer(cs))
	C.AttrAdd(cs, C.int(value))
}

func ProAttrAdd(ProjectName, Attrname string, value int32) {
	cs := C.CString(ProjectName + "/" + Attrname)
	defer C.free(unsafe.Pointer(cs))
	C.AttrAdd(cs, C.int(value))
}

func ProAttrAddAvg(ProjectName, Attrname string, value int32) {
	cs := C.CString(ProjectName + "/" + Attrname)
	defer C.free(unsafe.Pointer(cs))
	C.AttrAddAvg(cs, C.int(value))
}

func AttrAddAvg(name string, value int32) {
	cs := C.CString(name)
	defer C.free(unsafe.Pointer(cs))
	C.AttrAddAvg(cs, C.int(value))
}

func AttrSet(name string, value int32) {
	cs := C.CString(name)
	defer C.free(unsafe.Pointer(cs))
	C.AttrSet(cs, C.int(value))
}
