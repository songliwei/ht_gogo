// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// Error type
var (
	ErrWriteBlocking = errors.New("write packet was blocking")
	ErrNilDbObject   = errors.New("err nil db obj")
)

const (
	MESSAGELIMIT = 50
)

type VoipMessageLog struct {
	FromId       uint32
	ToId         uint32
	CallStatus   uint8
	MsgTimeStamp int64
}

type LogDbUtil struct {
	db             *sql.DB
	infoLog        *log.Logger
	packetSendChan chan *VoipMessageLog // packeet receive chanel
}

func NewLogDbUtil(mysqlDb *sql.DB, logger *log.Logger, chanLen uint32) *LogDbUtil {
	return &LogDbUtil{
		db:             mysqlDb,
		infoLog:        logger,
		packetSendChan: make(chan *VoipMessageLog, chanLen),
	}
}

func (this *LogDbUtil) Do() {
	asyncDo(this.writeLoop)
}

func (this *LogDbUtil) writeLoop() {
	defer func() {
		recover()
	}()

	var voipMsgs []*VoipMessageLog
	for {
		select {
		case p := <-this.packetSendChan:
			voipMsgs = append(voipMsgs, p)
			if len(voipMsgs) >= MESSAGELIMIT {
				err := this.BatchWriteVoipMessageLog(voipMsgs)
				if err != nil {
					this.infoLog.Printf("writeLoop failed err=%s", err)
				} else {
					this.infoLog.Printf("writeLoop success")
				}
				// 清空voipMsgs
				voipMsgs = make([]*VoipMessageLog, 0)
			}
		}
	}
}

func asyncDo(fn func()) {
	go func() {
		fn()
	}()
}

func (this *LogDbUtil) AsyncAddVoipMessageLog(p *VoipMessageLog, timeout time.Duration) (err error) {
	this.infoLog.Printf("AsyncAddVoipMessageLog from=%v to=%v callstat=%v timestamp=%v", p.FromId, p.ToId, p.CallStatus, p.MsgTimeStamp)
	if timeout == 0 {
		select {
		case this.packetSendChan <- p:
			return nil

		default:
			return ErrWriteBlocking
		}

	} else {
		select {
		case this.packetSendChan <- p:
			return nil

		case <-time.After(timeout):
			return ErrWriteBlocking
		}
	}
}

//2017-01-12 09:56:56
func TimeStampToSqlTime(ts int64) (outTime string) {
	tm := time.Unix(ts, 0)
	tm = tm.UTC()
	outTime = fmt.Sprintf("%4d-%02d-%02d %02d:%02d:%02d", tm.Year(), tm.Month(), tm.Day(), tm.Hour(), tm.Minute(), tm.Second())
	return outTime
}

func (this *LogDbUtil) BatchWriteVoipMessageLog(voipLogs []*VoipMessageLog) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	query := "INSERT INTO LOG_VOIP_STATUS(FROMID,TOID,STATUS,TIME) VALUES "
	var value string
	for i, v := range voipLogs {
		if v.ToId == 0 && v.FromId == 0 {
			this.infoLog.Printf("BatchWriteVoipMessageLog from=%v to=%v callStat=%v err param", v.FromId, v.ToId, v.CallStatus)
			continue
		}

		if i != 0 {
			value += ","
		}
		value += fmt.Sprintf("(%v, %v, %v, %q)",
			v.FromId,
			v.ToId,
			v.CallStatus,
			TimeStampToSqlTime(v.MsgTimeStamp))
	}

	query += value
	query += ";"
	//this.infoLog.Printf("BatchWriteVoipMessageLog query=%s", query)
	_, err = this.db.Exec(query)
	if err != nil {
		this.infoLog.Printf("BatchWriteVoipMessageLog insert faield err=%v", err)
		return err
	} else {
		return nil
	}
}
