// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"log"
	"sync"
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc"
	"github.com/golang/protobuf/proto"
)

var (
	ErrNilParam       = errors.New("err nil param obj")
	ErrInputParamErr  = errors.New("err input param err")
	ErrOnlineIpErr    = errors.New("err online state ip not exist")
	ErrSendToMucFaild = errors.New("err send packet to muc failed")
)

const (
	USE_TIMEOUT_THRESHOLD = 30 // gvoip 用户超时时间 30秒
)

type GVOIPSessionInfo struct {
	CreateUid               uint32
	ChannelId               []byte
	CreateTime              int64
	GVoipMemberCountWatcher *common.Set
	GVoipMemberActiveTime   map[uint32]int64
}

type GVoipManager struct {
	infoLog          *log.Logger
	mucApi           *common.MucApi
	gvoipSessionInfo map[uint32]*GVOIPSessionInfo // groupId-->gvoip_session_info
	sessionLock      sync.RWMutex
	sendSeq          uint16
}

func NewGVoipManager(logger *log.Logger, muc *common.MucApi) *GVoipManager {
	return &GVoipManager{
		infoLog:          logger,
		mucApi:           muc,
		gvoipSessionInfo: map[uint32]*GVOIPSessionInfo{},
		sendSeq:          0,
	}
}

func (this *GVoipManager) GetPacketSeq() (packetSeq uint16) {
	this.sendSeq++
	packetSeq = this.sendSeq
	return packetSeq
}

func (this *GVoipManager) IsLegalCreateRequest(createUid uint32, groupId uint32) (ok bool) {
	if createUid == 0 || groupId == 0 {
		this.infoLog.Printf("IsLegalCreateRequest invalid param createUid=%v groupId=%v", createUid, groupId)
		ok = false
		return ok
	}
	this.infoLog.Printf("IsLegalCreateRequest createUid=%v groupId=%v", createUid, groupId)
	this.sessionLock.RLock()
	defer this.sessionLock.RUnlock()
	_, ok = this.gvoipSessionInfo[groupId]
	return !ok
}

func (this *GVoipManager) AddGVOIPSessionInfo(createUid, groupId uint32, channelId []byte, timeStamp int64) {
	if createUid == 0 || groupId == 0 {
		this.infoLog.Printf("AddGVOIPSessionInfo invalid param createUid=%v groupId=%v channelId=%s timeStamp=%v",
			createUid,
			groupId,
			channelId,
			timeStamp)
		return
	}
	this.sessionLock.Lock()
	defer this.sessionLock.Unlock()
	if _, ok := this.gvoipSessionInfo[groupId]; ok {
		this.infoLog.Printf("AddGVOIPSessionInfo session already exist groupId=%v createUid=%v", groupId, createUid)
	} else {
		session := &GVOIPSessionInfo{
			CreateUid:  createUid,
			ChannelId:  channelId,
			CreateTime: timeStamp,
			GVoipMemberActiveTime: map[uint32]int64{
				createUid: time.Now().Unix(),
			},
			GVoipMemberCountWatcher: common.NewSet(),
		}
		this.gvoipSessionInfo[groupId] = session
		this.infoLog.Printf("AddGVOIPSessionInfo new session groupId=%v createUid=%v", groupId, createUid)
	}
}

func (this *GVoipManager) DelGVOIPSessionInfo(groupId uint32) {
	if groupId == 0 {
		this.infoLog.Printf("DelGVOIPSessionInfo invalid param groupId=%v", groupId)
		return
	}
	this.sessionLock.Lock()
	defer this.sessionLock.Unlock()
	delete(this.gvoipSessionInfo, groupId)
}

func (this *GVoipManager) AddToMemberCountWatcherList(groupId, uid uint32) (ret bool) {
	if groupId == 0 || uid == 0 {
		this.infoLog.Printf("AddToMemberCountWatcherList invalid param groupId=%v uid=%v", groupId, uid)
		ret = false
		return ret
	}

	this.sessionLock.Lock()
	defer this.sessionLock.Unlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		v.GVoipMemberCountWatcher.Add(uid)
		this.infoLog.Printf("AddToMemberCountWatcherList add uid=%v groupId=%v", uid, groupId)
		ret = true
	} else {
		this.infoLog.Printf("AddToMemberCountWatcherList session not exist groupId=%v uid=%v",
			groupId,
			uid)
		ret = false
	}

	return ret
}

func (this *GVoipManager) DelFromMemberCountWatcherList(groupId, uid uint32) (ret bool) {
	if groupId == 0 || uid == 0 {
		this.infoLog.Printf("DelFromMemberCountWatcherList invalid param groupId=%v uid=%v", groupId, uid)
		ret = false
		return ret
	}

	this.sessionLock.Lock()
	defer this.sessionLock.Unlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		v.GVoipMemberCountWatcher.Remove(uid)
		this.infoLog.Printf("DelFromMemberCountWatcherList remove uid=%v groupId=%v", uid, groupId)
		ret = true
	} else {
		this.infoLog.Printf("DelFromMemberCountWatcherList session not exist groupId=%v uid=%v",
			groupId,
			uid)
		ret = false
	}

	return ret
}

func (this *GVoipManager) AddToActiveList(groupId, uid uint32) (ret bool) {
	if groupId == 0 || uid == 0 {
		this.infoLog.Printf("AddToActiveList invalid param groupId=%v uid=%v", groupId, uid)
		ret = false
		return ret
	}

	this.sessionLock.Lock()
	defer this.sessionLock.Unlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		v.GVoipMemberActiveTime[uid] = time.Now().Unix()
		this.infoLog.Printf("AddToActiveList add groupId=%v uid=%v", groupId, uid)
		ret = true
	} else {
		this.infoLog.Printf("AddToActiveList session not exist groupId=%v uid=%v", groupId, uid)
		ret = false
	}
	return ret
}

func (this *GVoipManager) DelFromActiveList(groupId, uid uint32) (ret bool) {
	if groupId == 0 || uid == 0 {
		this.infoLog.Printf("DelFromActiveList invalid param groupId=%v uid=%v", groupId, uid)
		ret = false
		return ret
	}
	this.sessionLock.Lock()
	defer this.sessionLock.Unlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		delete(v.GVoipMemberActiveTime, uid)
		this.infoLog.Printf("DelFromActiveList Remove groupId=%v uid=%v active size=%v", groupId, uid, len(v.GVoipMemberActiveTime))
		ret = true
	} else {
		this.infoLog.Printf("DelFromActiveList session not exist groupId=%v uid=%v", groupId, uid)
		ret = false
	}
	return ret
}

func (this *GVoipManager) DelFromActiveListThreatUnsafe(groupId, uid uint32) (ret bool) {
	if groupId == 0 || uid == 0 {
		this.infoLog.Printf("DelFromActiveListThreatUnsafe invalid param groupId=%v uid=%v", groupId, uid)
		ret = false
		return ret
	}
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		delete(v.GVoipMemberActiveTime, uid)
		this.infoLog.Printf("DelFromActiveListThreatUnsafe Remove groupId=%v uid=%v active size=%v", groupId, uid, len(v.GVoipMemberActiveTime))
		ret = true
	} else {
		this.infoLog.Printf("DelFromActiveListThreatUnsafe session not exist groupId=%v uid=%v", groupId, uid)
		ret = false
	}
	return ret
}

func (this *GVoipManager) GetActiveMemberList(groupId uint32) (memberList []uint32) {
	this.sessionLock.RLock()
	defer this.sessionLock.RUnlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		for i, _ := range v.GVoipMemberActiveTime {
			memberList = append(memberList, i)
		}
		this.infoLog.Printf("GetActiveMemberList groupId=%v active size=%v", groupId, len(memberList))
	} else {
		this.infoLog.Printf("GetActiveMemberList groupId=%v not exist", groupId)
	}
	return memberList
}

func (this *GVoipManager) GetActiveMemberCount(groupId uint32) (count uint32) {
	this.sessionLock.RLock()
	defer this.sessionLock.RUnlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		count = uint32(len(v.GVoipMemberActiveTime))
		this.infoLog.Printf("GetActiveMemberCount groupId=%v active size=%v", groupId, count)
	} else {
		this.infoLog.Printf("GetActiveMemberCount groupId=%v not exist", groupId)
	}
	return count
}

func (this *GVoipManager) IsUserCanJoin(groupId, uid uint32) (ret bool) {
	this.sessionLock.RLock()
	defer this.sessionLock.RUnlock()
	if _, ok := this.gvoipSessionInfo[groupId]; ok {
		ret = true
	} else {
		this.infoLog.Printf("IsUserCanJoin groupId=%v not exist uid=%v", groupId, uid)
		ret = false
	}
	return ret
}

func (this *GVoipManager) IsRoomGvoiping(roomId uint32) (ret uint32) {
	this.sessionLock.RLock()
	defer this.sessionLock.RUnlock()
	if _, ok := this.gvoipSessionInfo[roomId]; ok {
		ret = 1
	} else {
		this.infoLog.Printf("IsRoomGvoiping roomId=%v not gvoiping", roomId)
		ret = 0
	}
	return ret
}

func (this *GVoipManager) BroadCastGVOIPMemberChange(groupId, uid uint32, cmd uint16) (err error) {
	if groupId == 0 || uid == 0 || cmd == 0 {
		this.infoLog.Printf("BroadCastGVOIPMemberChange invalid param groupId=%v uid=%v cmd=%v", groupId, uid, cmd)
		err = ErrInputParamErr
		return err
	}
	this.sessionLock.RLock()
	defer this.sessionLock.RUnlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		head := &common.HeadV3{
			Flag:     uint8(common.CServToServ),
			Version:  common.CVerMmedia,
			CryKey:   uint8(common.CNoneKey),
			TermType: uint8(0),
			Cmd:      cmd,
			Seq:      this.GetPacketSeq(),
			From:     uid,
			To:       groupId,
			Len:      0,
		}

		reqBody := new(ht_muc.MucReqBody)
		reqBody.GvoipMemberChangeBraodcastReqbody = &ht_muc.S2SGvoipMemberChangeBroadCastReqBody{
			RoomId:            proto.Uint32(groupId),
			ChannelId:         v.ChannelId,
			ChangeUid:         proto.Uint32(uid),
			MemberCount:       proto.Uint32(uint32(len(v.GVoipMemberActiveTime))),
			TotalWatcherCount: proto.Uint32(uint32(v.GVoipMemberCountWatcher.Len())),
			TotalWatcherList:  v.GVoipMemberCountWatcher.List(),
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			this.infoLog.Printf("BroadCastGVOIPMemberChange proto marshal groupId=%v uid=%v cmd=0x%4x err=%v",
				groupId,
				uid,
				cmd,
				err)
			return err
		}

		err = this.mucApi.JustSendPacket(head, payLoad)
		if err != nil {
			this.infoLog.Printf("BroadCastGVOIPMemberChange mucApi.JustSendPacket failed groupId=%v uid=%v cmd=0x%4x err=%s",
				groupId,
				uid,
				cmd,
				err)
			err = ErrSendToMucFaild
		}
	} else {
		this.infoLog.Printf("BroadCastGVOIPMemberChange gvoip not exist groupId=%v uid=%v", groupId, uid)
		err = ErrInputParamErr
		return err
	}
	return err
}

func (this *GVoipManager) BroadCastGVOIPMemberChangeThreatUnsafe(groupId, uid uint32, cmd uint16) (err error) {
	if groupId == 0 || uid == 0 || cmd == 0 {
		this.infoLog.Printf("BroadCastGVOIPMemberChangeThreatUnsafe invalid param groupId=%v uid=%v cmd=%v", groupId, uid, cmd)
		err = ErrInputParamErr
		return err
	}
	this.infoLog.Printf("BroadCastGVOIPMemberChangeThreatUnsafe recv req groupId=%v uid=%v cmd=%v",
		groupId,
		uid,
		cmd)
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		head := &common.HeadV3{
			Flag:     uint8(common.CServToServ),
			Version:  common.CVerMmedia,
			CryKey:   uint8(common.CNoneKey),
			TermType: uint8(0),
			Cmd:      cmd,
			Seq:      this.GetPacketSeq(),
			From:     uid,
			To:       groupId,
			Len:      0,
		}

		reqBody := new(ht_muc.MucReqBody)
		reqBody.GvoipMemberChangeBraodcastReqbody = &ht_muc.S2SGvoipMemberChangeBroadCastReqBody{
			RoomId:            proto.Uint32(groupId),
			ChannelId:         v.ChannelId,
			ChangeUid:         proto.Uint32(uid),
			MemberCount:       proto.Uint32(uint32(len(v.GVoipMemberActiveTime))),
			TotalWatcherCount: proto.Uint32(uint32(v.GVoipMemberCountWatcher.Len())),
			TotalWatcherList:  v.GVoipMemberCountWatcher.List(),
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			this.infoLog.Printf("BroadCastGVOIPMemberChangeThreatUnsafe proto marshal groupId=%v uid=%v cmd=0x%4x err=%v",
				groupId,
				uid,
				cmd,
				err)
			return err
		}

		err = this.mucApi.JustSendPacket(head, payLoad)
		if err != nil {
			this.infoLog.Printf("BroadCastGVOIPMemberChangeThreatUnsafe mucApi.JustSendPacket failed groupId=%v uid=%v cmd=0x%4x err=%s",
				groupId,
				uid,
				cmd,
				err)
			err = ErrSendToMucFaild
		}
	} else {
		this.infoLog.Printf("BroadCastGVOIPMemberChangeThreatUnsafe gvoip not exist groupId=%v uid=%v", groupId, uid)
		err = ErrInputParamErr
		return err
	}
	return err
}

func (this *GVoipManager) BroadCastGVOIPIsBegin(createUid, groupId uint32, nickName []byte, channelId []byte, Ts int64) (err error) {
	if createUid == 0 || groupId == 0 || len(channelId) == 0 {
		this.infoLog.Printf("BroadCastGVOIPIsBegin invalid param createUid=%v groupId=%v nickName=%s channelId=%s ts=%v",
			createUid,
			groupId,
			nickName,
			channelId,
			Ts)
		err = ErrInputParamErr
		return err
	}

	head := &common.HeadV3{
		Flag:     uint8(common.CServToServ),
		Version:  common.CVerMmedia,
		CryKey:   uint8(common.CNoneKey),
		TermType: uint8(0),
		Cmd:      uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_INVITE_BROADCAST),
		Seq:      this.GetPacketSeq(),
		From:     createUid,
		To:       groupId,
		Len:      0,
	}

	reqBody := new(ht_muc.MucReqBody)
	reqBody.GvoipInviteBroadcastReqbody = &ht_muc.S2SGvoipInviteBroadCastReqBody{
		CreateUid:  proto.Uint32(createUid),
		CreateName: nickName,
		RoomId:     proto.Uint32(groupId),
		ChannelId:  channelId,
		Timestamp:  proto.Uint64(uint64(Ts)),
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("BroadCastGVOIPIsBegin proto marshal createUid=%v groupId=%v nickName=%s channelId=%s ts=%v err=%v",
			createUid,
			groupId,
			nickName,
			channelId,
			Ts,
			err)
		return err
	}

	err = this.mucApi.JustSendPacket(head, payLoad)
	if err != nil {
		this.infoLog.Printf("BroadCastGVOIPIsBegin mucApi.JustSendPacket failed createUid=%v groupId=%v nickName=%s channelId=%s ts=%v err=%v",
			createUid,
			groupId,
			nickName,
			channelId,
			Ts,
			err)
		err = ErrSendToMucFaild
	}
	return err
}

func (this *GVoipManager) BroadCastGVOIPIsEnd(fromId, groupId uint32, channelId []byte, timeStamp uint64) (err error) {
	if fromId == 0 || groupId == 0 {
		this.infoLog.Printf("BroadCastGVOIPIsEnd invalid param fromId=%v groupId=%v channelId=%s timeStamp=%v",
			fromId,
			groupId,
			channelId,
			timeStamp)
		err = ErrInputParamErr
		return err
	}

	this.infoLog.Printf("BroadCastGVOIPIsEnd fromId=%v groupId=%v channelId=%s timeStamp=%v",
		fromId,
		groupId,
		channelId,
		timeStamp)

	this.sessionLock.RLock()
	defer this.sessionLock.RUnlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		head := &common.HeadV3{
			Flag:     uint8(common.CServToServ),
			Version:  common.CVerMmedia,
			CryKey:   uint8(common.CNoneKey),
			TermType: uint8(0),
			Cmd:      uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_END_BROADCAST),
			Seq:      this.GetPacketSeq(),
			From:     fromId,
			To:       groupId,
			Len:      0,
		}

		reqBody := new(ht_muc.MucReqBody)
		reqBody.GvoipEndBreadcastReqbody = &ht_muc.S2SGvoipEndBroadCastReqBody{
			RoomId:    proto.Uint32(groupId),
			ChannelId: v.ChannelId,
			Timestamp: proto.Uint64(timeStamp),
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			this.infoLog.Printf("BroadCastGVOIPIsEnd proto marshal groupId=%v uid=%v cmd=0x%4x err=%v",
				groupId,
				fromId,
				head.Cmd,
				err)
			return err
		}

		err = this.mucApi.JustSendPacket(head, payLoad)
		if err != nil {
			this.infoLog.Printf("BroadCastGVOIPIsEnd mucApi.JustSendPacket failed groupId=%v uid=%v cmd=0x%4x err=%s",
				groupId,
				fromId,
				head.Cmd,
				err)
			err = ErrSendToMucFaild
		}
	} else {
		this.infoLog.Printf("BroadCastGVOIPIsEnd gvoip not exist groupId=%v uid=%v", groupId, fromId)
		err = ErrInputParamErr
	}
	return err
}

func (this *GVoipManager) BroadCastGVOIPIsEndThreatUnsafe(fromId, groupId uint32, channelId []byte, timeStamp uint64) (err error) {
	if fromId == 0 || groupId == 0 {
		this.infoLog.Printf("BroadCastGVOIPIsEndThreatUnsafe invalid param fromId=%v groupId=%v channelId=%s timeStamp=%v",
			fromId,
			groupId,
			channelId,
			timeStamp)
		err = ErrInputParamErr
		return err
	}

	this.infoLog.Printf("BroadCastGVOIPIsEndThreatUnsafe fromId=%v groupId=%v channelId=%s timeStamp=%v",
		fromId,
		groupId,
		channelId,
		timeStamp)

	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		head := &common.HeadV3{
			Flag:     uint8(common.CServToServ),
			Version:  common.CVerMmedia,
			CryKey:   uint8(common.CNoneKey),
			TermType: uint8(0),
			Cmd:      uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_END_BROADCAST),
			Seq:      this.GetPacketSeq(),
			From:     fromId,
			To:       groupId,
			Len:      0,
		}

		reqBody := new(ht_muc.MucReqBody)
		reqBody.GvoipEndBreadcastReqbody = &ht_muc.S2SGvoipEndBroadCastReqBody{
			RoomId:    proto.Uint32(groupId),
			ChannelId: v.ChannelId,
			Timestamp: proto.Uint64(timeStamp),
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			this.infoLog.Printf("BroadCastGVOIPIsEndThreatUnsafe proto marshal groupId=%v uid=%v cmd=0x%4x err=%v",
				groupId,
				fromId,
				head.Cmd,
				err)
			return err
		}

		err = this.mucApi.JustSendPacket(head, payLoad)
		if err != nil {
			this.infoLog.Printf("BroadCastGVOIPIsEndThreatUnsafe mucApi.JustSendPacket failed groupId=%v uid=%v cmd=0x%4x err=%s",
				groupId,
				fromId,
				head.Cmd,
				err)
			err = ErrSendToMucFaild
		}
	} else {
		this.infoLog.Printf("BroadCastGVOIPIsEndThreatUnsafe gvoip not exist groupId=%v uid=%v", groupId, fromId)
		err = ErrInputParamErr
	}
	return err
}

func (this *GVoipManager) Do() {
	asyncDo(this.CheckGVOIPUserAlive)
}

// func asyncDo(fn func()) {
// 	go func() {
// 		fn()
// 	}()
// }

func (this *GVoipManager) CheckGVOIPUserAlive() {
	defer func() {
		recover()
	}()

	for {
		time.Sleep(10 * time.Second) // every 10 second
		this.sessionLock.Lock()
		for groupId, session := range this.gvoipSessionInfo {
			this.infoLog.Printf("CheckGVOIPUserAlive groupId=%v createUid=%v channelId=%s",
				groupId,
				session.CreateUid,
				session.ChannelId)
			var targetUid uint32
			for uid, time := range session.GVoipMemberActiveTime {
				targetUid = uid
				ret := this.IsUserAlive(uid, time)
				if !ret { // user timeout
					this.infoLog.Printf("CheckGVOIPUserAlive uid=%v lastActiveTime=%v timeout", uid, time)
					// 删除timeout 用户
					delete(session.GVoipMemberActiveTime, uid)
					this.BroadCastGVOIPMemberChangeThreatUnsafe(groupId, uid, uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_MEMBER_LEAVE_BROADCAST))
				}
			}
			if len(session.GVoipMemberActiveTime) == 0 { // 整个用户全部超时了直接删除group
				this.infoLog.Printf("CheckGVOIPUserAlive groupId=%v all timeout", groupId)
				if session.CreateTime != 0 {
					this.BroadCastGVOIPIsEndThreatUnsafe(targetUid, groupId, session.ChannelId, uint64(session.CreateTime))
				}
				// 先广播结束 再将其删除 不能颠倒循序
				delete(this.gvoipSessionInfo, groupId)
			}
		}
		this.sessionLock.Unlock()
	}
}

func (this *GVoipManager) IsUserAlive(uid uint32, lastActiveTime int64) (ret bool) {
	duration := time.Now().Unix() - lastActiveTime
	if duration >= int64(USE_TIMEOUT_THRESHOLD) {
		ret = false
	} else {
		ret = true
	}
	return ret
}

func (this *GVoipManager) HandleGVOIPPing(groupId, uid uint32, channelId []byte, timeStamp uint64) (ret bool) {
	this.sessionLock.Lock()
	defer this.sessionLock.Unlock()
	if v, ok := this.gvoipSessionInfo[groupId]; ok {
		if activeTime, searchRes := v.GVoipMemberActiveTime[uid]; searchRes {
			if (time.Now().Unix() - activeTime) > USE_TIMEOUT_THRESHOLD {
				this.DelFromActiveListThreatUnsafe(groupId, uid)
				this.BroadCastGVOIPMemberChangeThreatUnsafe(groupId,
					uid,
					uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_MEMBER_LEAVE_BROADCAST))
				ret = false
			} else {
				v.GVoipMemberActiveTime[uid] = time.Now().Unix()
				ret = true
			}
		} else {
			this.infoLog.Printf("HandleGVOIPPing gvoip uid not exist groupId=%v uid=%v channelId=%s", groupId, uid, channelId)
			ret = true
		}
	} else {
		this.infoLog.Printf("HandleGVOIPPing gvoip not exist groupId=%v uid=%v channelId=%s", groupId, uid, channelId)
		ret = true
	}
	return ret
}
