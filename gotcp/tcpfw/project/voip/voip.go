package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/libcrypto"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc"
	"github.com/HT_GOGO/gotcp/tcpfw/project/voip/util"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	// ErrNilDbObject    = errors.New("err nil db obj")
	ErrNilParam       = errors.New("err nil param obj")
	ErrInputParamErr  = errors.New("err input param err")
	ErrOnlineIpErr    = errors.New("err online state ip not exist")
	ErrSendToMucFaild = errors.New("err send packet to muc failed")
)

var (
	infoLog      *log.Logger
	logDb        *sql.DB
	mcApi        *common.MemcacheApi
	offlineApi   *common.OfflineApiV2
	imServerPool map[string]*common.ImServerApiV2
	sendSeq      uint16
	LogDbUtil    *util.LogDbUtil
	mapVoipInfo  map[string]*VoipInfo
	voipLock     sync.Mutex
	gvoipManager *util.GVoipManager
)

const (
	CMD_S2S_MESSAGE_PUSH     = 0x8027
	CMD_S2S_MESSAGE_PUSH_ACK = 0x8028

	// 1V1 Voip chat
	CMD_P2P_VOIP_INVITE                   = 0x4019
	CMD_P2P_VOIP_INVITE_RECEIPT           = 0x401A
	CMD_P2P_VOIP_INVITE_REJECT            = 0x401B
	CMD_P2P_VOIP_INVITE_REJECT_RECEIPT    = 0x401C
	CMD_P2P_VOIP_INVITE_CANCEL            = 0x401D
	CMD_P2P_VOIP_INVITE_CANCEL_RECEIPT    = 0x401E
	CMD_P2P_VOIP_INVITE_ACCEPT            = 0x401F
	CMD_P2P_VOIP_INVITE_ACCEPT_RECEIPT    = 0x4020
	CMD_P2P_VOIP_CALL_END                 = 0x4021
	CMD_P2P_VOIP_CALL_END_RECEIPT         = 0x4022
	CMD_VOIP_CONNECTMEDIA_SRV_OK          = 0x4023
	CMD_VOIP_CONNECTMEDIA_SRV_OK_RECEIPT  = 0x4024
	CMD_VOIP_SYNCCONNECTMEDIA_SRV_RECEIPT = 0x4027
	CMD_VOIP_SYNCCONNECTMEDIA_SRV         = 0x4026
	CMD_QUERY_GROUP_VOIP_REQ              = 0x4030
	CMD_QUERY_GROUP_VOIP_ACK              = 0x4031

	//-----------------------------------------------------------------------
	// 1V1 Video Chat
	//-----------------------------------------------------------------------
	CMD_P2P_VIDEO_CHAT_INVITE                   = 0x4119
	CMD_P2P_VIDEO_CHAT_INVITE_RECEIPT           = 0x411A
	CMD_P2P_VIDEO_CHAT_INVITE_REJECT            = 0x411B
	CMD_P2P_VIDEO_CHAT_INVITE_REJECT_RECEIPT    = 0x411C
	CMD_P2P_VIDEO_CHAT_INVITE_CANCEL            = 0x411D
	CMD_P2P_VIDEO_CHAT_INVITE_CANCEL_RECEIPT    = 0x411E
	CMD_P2P_VIDEO_CHAT_INVITE_ACCEPT            = 0x411F
	CMD_P2P_VIDEO_CHAT_INVITE_ACCEPT_RECEIPT    = 0x4120
	CMD_P2P_VIDEO_CHAT_CALL_END                 = 0x4121
	CMD_P2P_VIDEO_CHAT_CALL_END_RECEIPT         = 0x4122
	CMD_VIDEO_CHAT_CONNECTMEDIA_SRV_OK          = 0x4123
	CMD_VIDEO_CHAT_CONNECTMEDIA_SRV_OK_RECEIPT  = 0x4124
	CMD_VIDEO_CHAT_SYNCCONNECTMEDIA_SRV_RECEIPT = 0x4127
	CMD_VIDEO_CHAT_SYNCCONNECTMEDIA_SRV         = 0x4126
	CMD_CLOSE_VIDEO_VIEW                        = 0x4129
	CMD_CLOSE_VIDEO_VIEW_RES                    = 0x412A

	//-----------------------------------------------------------------------
	//  gvoip Chat
	//-----------------------------------------------------------------------
	CMD_GVOIP_CREATE_REQUEST            = 0x7101
	CMD_GVOIP_CREATE_RESPONSE           = 0x7102
	CMD_GVOIP_INVITE_BROADCAST          = 0x7103
	CMD_GVOIP_INVITE_BROADCAST_RECEIPT  = 0x7104
	CMD_GVOIP_JOIN_REQUEST              = 0x7105
	CMD_GVOIP_JOIN_RESPONSE             = 0x7106
	CMD_GVOIP_LOGIN_REPORT              = 0x7107
	CMD_GVOIP_LOGIN_REPORT_RECEIPT      = 0x7108
	CMD_GVOIP_LOGOUT_REPORT             = 0x710B
	CMD_GVOIP_LOGOUT_REPORT_RECEIPT     = 0x710C
	CMD_GVOIP_GET_MEMBER_LIST           = 0x710F
	CMD_GVOIP_GET_MEMBER_LIST_RESPONSE  = 0x7110
	CMD_GVOIP_END_BROADCAST             = 0x7111
	CMD_GVOIP_END_BROADCAST_RECEIPT     = 0x7112
	CMD_GVOIP_PING                      = 0x7113
	CMD_GVOIP_PING_ACK                  = 0x7114
	CMD_GVOIP_UNWATCH_MEMBER_CHANGE     = 0x7115
	CMD_GVOIP_UNWATCH_MEMBER_CHANGE_RES = 0x7116
)
const (
	ENUM_CALL_INVITE     = 0
	ENUM_CALL_CANCLED    = 1
	ENUM_CALL_TIME_OUT   = 2
	ENUM_CALL_REJECT     = 3
	ENUM_CALL_BUSY       = 4
	ENUM_CALL_CONNECT    = 5
	ENUM_CALL_ACCEPT     = 6
	ENUM_CALL_TERMINATE  = 7
	ENUM_CALL_NON_STATUS = 255
)

const (
	CT_P2P = 0
	CT_MUC = 1
)

const (
	NICKNAME_LEN    = 128
	CONTENT_LEN     = 128
	NORMALDEC_LEN   = 128
	SERVER_COMM_KEY = "lp$5F@nfN0Oh8I*5"
)

const (
	CNotBeenAt = 0
	CBeenAt    = 1
)

const (
	PUSH_TEXT                    = 0
	PUSH_VOICE                   = 1
	PUSH_IMAGE                   = 2
	PUSH_INTRODUCE               = 3
	PUSH_LOCATION                = 4
	PUSH_FRIEND_INVITE           = 5
	PUSH_LANGUAGE_EXCHANGE       = 6
	PUSH_LANGUAGE_EXCHANGE_REPLY = 7
	PUSH_CORRECT_SENTENCE        = 8
	PUSH_STICKERS                = 9
	PUSH_DOODLE                  = 10
	PUSH_GIFT                    = 11
	PUSH_VOIP                    = 12
	PUSH_ACCEPT_INVITE           = 13
	PUSH_VIDEO                   = 14
	PUSH_GVOIP                   = 15 // group voip
	PUSH_LINK                    = 16
	PUSH_CARD                    = 17
	PUSH_FOLLOW                  = 18
	PUSH_REPLY_YOUR_COMMENT      = 19
	PUSH_COMMENTED_YOUR_POST     = 20
	PUSH_CORRECTED_YOUR_POST     = 21
	PUSH_MOMENT_LIKE             = 22
)

const (
	PUSH_PARAM_CALLINCOMING = "voip_call_incoming"
	PUSH_PARAM_CALLIMISS    = "voip_call_miss"
	PUSH_PARAM_CALLICANCEL  = "voip_call_cancel"
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)

const (
	RET_SUCCESS            = 0
	ERR_SYSERR_START       = 100
	ERR_SERVER_BUSY        = 100
	ERR_INTERNAL_ERROR     = 101
	ERR_UNFORMATTED_PACKET = 102
	ERR_NO_ACCESS          = 103
	ERR_INVALID_CLIENT     = 104
	ERR_INVALID_SESSION    = 105
	ERR_INVALID_PARAM      = 106
)

const (
	CREATE_SUCCESS               = 0
	BAND_CREATE_USER             = 1
	AlREADY_EXIST_AND_DIREC_JOIN = 2
	UN_KNOW_CREAT_CODE           = 255
)

const (
	ALLOW_JOIN        = 0
	BAND_JOIN         = 1
	JOIN_FAILED       = 2
	UN_KNOW_JOIN_CODE = 255
)

type VoipInfo struct {
	FromId    uint32
	ToId      uint32
	TimeStamp int64
}
type InviteMessage struct {
	UserName []byte
	HeadUrl  []byte
	RoomId   []byte
}

type VoipPush struct {
	NickName  []byte
	PushParam []byte
}
type VoipRoomId struct {
	RoomId []byte
}
type CancleMessage struct {
	UserName     []byte
	RoomId       []byte
	CancleReason uint8 //取消原因，0正常，1超时，被叫未接听，给被叫发推送
}

type RejectMessage struct {
	RoomId       []byte
	RejectReason uint8 //拒接原因，0正常拒绝，1在通话
}

type GVoipCreateMessage struct {
	GroupId   uint32
	TimeStamp uint64
	ChannelId []byte
	UserName  []byte
}

type GVoipJoinMessage struct {
	GroupId   uint32
	TimeStamp uint64
	ChannelId []byte
}

type GVoipLoginReportMessage struct {
	GroupId   uint32
	TimeStamp uint64
	ChannelId []byte
}

type GVoipLogoutReportMessage struct {
	GroupId   uint32
	TimeStamp uint64
	ChannelId []byte
}

type GVoipGetMemeberListMessage struct {
	GroupId   uint32
	TimeStamp uint64
	ChannelId []byte
}

type GVoipPingMessage struct {
	GroupId   uint32
	TimeStamp uint64
	ChannelId []byte
}

type GVoipRevokeMessage struct {
	GroupId   uint32
	TimeStamp uint64
	ChannelId []byte
}

// Convert uint to net.IP http://www.outofmemory.cn
func inet_ntoa(ipnr int64) net.IP {
	var bytes [4]byte
	bytes[0] = byte((ipnr >> 24) & 0xFF)
	bytes[1] = byte((ipnr >> 16) & 0xFF)
	bytes[2] = byte((ipnr >> 8) & 0xFF)
	bytes[3] = byte(ipnr & 0xFF)

	return net.IPv4(bytes[3], bytes[2], bytes[1], bytes[0])
}

// Convert net.IP to int64 ,  http://www.outofmemory.cn
func inet_aton(ipnr net.IP) int64 {
	bits := strings.Split(ipnr.String(), ".")

	b0, _ := strconv.Atoi(bits[0])
	b1, _ := strconv.Atoi(bits[1])
	b2, _ := strconv.Atoi(bits[2])
	b3, _ := strconv.Atoi(bits[3])

	var sum int64

	sum += int64(b0) << 24
	sum += int64(b1) << 16
	sum += int64(b2) << 8
	sum += int64(b3)

	return sum
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.XTHeadPacket)
	if !ok { // 不是XTHeadPacket报文
		infoLog.Println("OnMessage packet can not change to xtpacket")
		return false
	}

	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Println("OnMessage Get head failed", err)
		return false
	}
	attr := "govoip/recv_req_count"
	libcomm.AttrAdd(attr, 1)

	//infoLog.Printf("OnMessage:[%#v] len=%v payLoad=%v\n", head, len(packet.GetBody()), packet.GetBody())
	infoLog.Printf("OnMessage: head=[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckXTPacketValid()
	if err != nil {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Println("OnMessage Invalid packet", err)
		return false
	}

	plainText := libcrypto.TEADecrypt(string(packet.GetBody()), SERVER_COMM_KEY)
	plainSlice := []byte(plainText)

	switch head.Cmd {
	case CMD_P2P_VOIP_INVITE, CMD_P2P_VIDEO_CHAT_INVITE:
		go ProcInviteReq(c, head, plainSlice)
	case CMD_P2P_VOIP_INVITE_REJECT, CMD_P2P_VIDEO_CHAT_INVITE_REJECT:
		go ProcInviteReject(c, head, plainSlice)
	case CMD_P2P_VOIP_INVITE_ACCEPT, CMD_P2P_VIDEO_CHAT_INVITE_ACCEPT, CMD_P2P_VOIP_CALL_END, CMD_P2P_VIDEO_CHAT_CALL_END:
		go ProcInviteAcceptOrCallEnd(c, head, plainSlice)
	case CMD_P2P_VOIP_INVITE_CANCEL, CMD_P2P_VIDEO_CHAT_INVITE_CANCEL:
		go ProcInviteCancle(c, head, plainSlice)
	case CMD_VOIP_CONNECTMEDIA_SRV_OK, CMD_VIDEO_CHAT_CONNECTMEDIA_SRV_OK:
		go ProcConnectMediaServerOk(c, head, plainSlice)
	case CMD_GVOIP_CREATE_REQUEST:
		go ProcGVoipCreateReq(c, head, plainSlice)
	case CMD_GVOIP_JOIN_REQUEST:
		go ProcGVoipJoinReq(c, head, plainSlice)
	case CMD_GVOIP_LOGIN_REPORT:
		go ProcGVoipLoginReq(c, head, plainSlice)
	case CMD_GVOIP_LOGOUT_REPORT:
		go ProcGVoipLogoutReq(c, head, plainSlice)
	case CMD_GVOIP_GET_MEMBER_LIST:
		go ProcGVoipGetMemberListReq(c, head, plainSlice)
	case CMD_GVOIP_PING:
		go ProcGVoipPingReq(c, head, plainSlice)
	case CMD_GVOIP_UNWATCH_MEMBER_CHANGE:
		go ProcGVoipRevokeMemberChangeReq(c, head, plainSlice)
	case CMD_QUERY_GROUP_VOIP_REQ:
		go ProcRoomIsGVoipReq(c, head, plainSlice)
	default:
		infoLog.Println("UnHandle Cmd =", head.Cmd)
	}
	return true
}

func SendResp(c *gotcp.Conn, reqHead *common.XTHead, ret uint8) bool {
	head := new(common.XTHead)
	if reqHead == nil {
		infoLog.Printf("SendResp nil reqHead")
		return false
	}
	*head = *reqHead
	head.Cmd = reqHead.Cmd + 1 // ack cmd = req cmd + 1
	head.CryKey = common.CServKey
	var respPayLoad []byte
	common.MarshalUint8(ret, &respPayLoad)
	// 使用Server key 加密
	cryptoText := libcrypto.TEAEncrypt(string(respPayLoad), SERVER_COMM_KEY)
	head.Len = uint32(len(cryptoText)) // sizeof(uint8)
	buf := make([]byte, common.XTHeadLen+head.Len)
	err := common.SerialXTHeadToSlice(head, buf[:])
	if err != nil {
		infoLog.Println("SendResp SerialXTHeadToSlice failed head=[%#v]", *reqHead)
		return false
	}
	copy(buf[common.XTHeadLen:], []byte(cryptoText)) // return code
	resp := common.NewXTHeadPacket(buf)
	c.AsyncWritePacket(resp, time.Second)
	// add static
	attr := "govoip/sendresp_count"
	libcomm.AttrAdd(attr, 1)
	return true
}

func SendRespWithPayLoad(c *gotcp.Conn, reqHead *common.XTHead, payLoad []byte) bool {
	head := new(common.XTHead)
	if reqHead == nil {
		infoLog.Printf("SendRespWithPayLoad nil reqHead")
		return false
	}
	*head = *reqHead
	head.Cmd = reqHead.Cmd + 1 // ack cmd = req cmd + 1
	head.CryKey = common.CServKey
	// 使用Server key 加密
	cryptoText := libcrypto.TEAEncrypt(string(payLoad), SERVER_COMM_KEY)
	head.Len = uint32(len(cryptoText)) //
	buf := make([]byte, common.XTHeadLen+head.Len)
	err := common.SerialXTHeadToSlice(head, buf[:])
	if err != nil {
		infoLog.Println("SendRespWithPayLoad SerialXTHeadToSlice failed head=[%#v]", *reqHead)
		return false
	}
	copy(buf[common.XTHeadLen:], []byte(cryptoText)) // return code
	resp := common.NewXTHeadPacket(buf)
	c.AsyncWritePacket(resp, time.Second)
	// add static
	attr := "govoip/sendresp_with_payload_count"
	libcomm.AttrAdd(attr, 1)
	return true
}

func ProcInviteReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcInviteReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	if head.Cmd == CMD_P2P_VOIP_INVITE {
		attr := "govoip/voip_invite_req_count"
		libcomm.AttrAdd(attr, 1)
	} else {
		attr := "govoip/video_invite_req_count"
		libcomm.AttrAdd(attr, 1)
	}

	var inviteMsg InviteMessage
	inviteMsg.UserName = common.UnMarshalSlice(&plainSlice)
	inviteMsg.HeadUrl = common.UnMarshalSlice(&plainSlice)
	inviteMsg.RoomId = common.UnMarshalSlice(&plainSlice)
	if len(inviteMsg.UserName) == 0 || len(inviteMsg.RoomId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcInviteReq error param UserName=%s HeadUrl=%s RoomId=%s",
			inviteMsg.UserName,
			inviteMsg.HeadUrl,
			inviteMsg.RoomId)
		return false
	}
	// add logic here
	infoLog.Printf("ProcInviteReq recv Invite Message from=%v to=%v seq=%v cmd=0x%4x UserName=%s HeadUrl=%s RoomId=%s",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		inviteMsg.UserName,
		inviteMsg.HeadUrl,
		inviteMsg.RoomId)
	// answer first
	SendResp(c, head, uint8(RET_SUCCESS))
	// continue process
	callStat := ENUM_CALL_NON_STATUS
	var respPayLoad []byte
	common.MarshalSlice(inviteMsg.UserName, &respPayLoad)
	common.MarshalSlice(inviteMsg.HeadUrl, &respPayLoad)
	common.MarshalSlice(inviteMsg.RoomId, &respPayLoad)
	var curTS uint64
	curTS = uint64(time.Now().Unix())
	common.MarshalUint64(curTS, &respPayLoad)
	curPush := &VoipPush{
		NickName:  inviteMsg.UserName,
		PushParam: []byte(PUSH_PARAM_CALLINCOMING),
	}
	DispatchVOIPMessage(head, respPayLoad, curPush)
	// write log
	callStat = ENUM_CALL_INVITE
	WriteMessageLog(head.From, head.To, uint8(callStat), uint64(time.Now().Unix()))
	return true
}

func DispatchVOIPMessage(reqHead *common.XTHead, payLoad []byte, push *VoipPush) {
	if reqHead == nil || mcApi == nil {
		infoLog.Printf("DispatchVOIPMessage nil param reqHead=%v mcApi=%v return", reqHead, mcApi)
		return
	}
	onlineStat, err := mcApi.GetUserOnlineStat(reqHead.To)
	if err != nil {
		infoLog.Printf("DispatchVOIPMessage mcApi.GetUserOnlineStat failed uid=%v err=%s", reqHead.To, err)
		// store offline
		attr := "govoip/get_online_failed_count"
		libcomm.AttrAdd(attr, 1)
		ret, err := offlineApi.SendPacketWithXTHead(reqHead, payLoad, nil)
		if err != nil {
			infoLog.Printf("DispatchVOIPMessage GetUserOnlineStat failed store offline failed uid=%v err=%s", reqHead.To, err)
			attr := "govoip/store_offline_failed_count"
			libcomm.AttrAdd(attr, 1)
		} else {
			infoLog.Printf("DispatchVOIPMessage GetUserOnlineStat failed store offline ret=%v", ret)
		}
		return
	}
	if onlineStat.OnlineStat == common.ST_ONLINE {
		err = SendPacketToIMServerRelabile(onlineStat, reqHead, payLoad)
		if err != nil {
			attr := "govoip/send_packet_to_im_failed_count"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("DispatchVOIPMessage exec SendPacketToIMServerRelabile failed from=%v to=%v err=%v",
				reqHead.From,
				reqHead.To,
				err)
			// store offline
			ret, err := offlineApi.SendPacketWithXTHead(reqHead, payLoad, nil)
			if err != nil {
				infoLog.Printf("DispatchVOIPMessage SendPacketToIMServerRelabile failed store offline failed uid=%v err=%s", reqHead.To, err)
				attr := "govoip/store_offline_failed_count"
				libcomm.AttrAdd(attr, 1)
			} else {
				infoLog.Printf("DispatchVOIPMessage SendPacketToIMServerRelabile failed store offline success uid=%v ret=%v", reqHead.To, ret)
			}
		}
	} else {
		if onlineStat.OnlineStat != common.ST_LOGOUT && push != nil {
			pushPayLoad, err := BuildPushPacket(onlineStat.ClientType, reqHead.From, reqHead.To, string(push.NickName), string(push.PushParam))
			if err != nil {
				attr := "govoip/build_push_packet_failed_count"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("DispatchVOIPMessage Build push packet failed. from=%v to=%v nikcname=%s terminatye=%v",
					reqHead.From,
					reqHead.To,
					push.NickName,
					onlineStat.ClientType)
				ret, err := offlineApi.SendPacketWithXTHead(reqHead, payLoad, nil)
				if err != nil {
					infoLog.Printf("DispatchVOIPMessage Build push packet failed store offline failed uid=%v err=%s", reqHead.To, err)
					attr := "govoip/store_offline_failed_count"
					libcomm.AttrAdd(attr, 1)
				} else {
					infoLog.Printf("DispatchVOIPMessage Build push packet failed store offline success uid=%v ret=%v", reqHead.To, ret)
				}
			} else {
				ret, err := offlineApi.SendPacketWithXTHead(reqHead, payLoad, pushPayLoad)
				if err != nil {
					infoLog.Printf("DispatchVOIPMessage Build push packet success store offline failed uid=%v err=%s", reqHead.To, err)
					attr := "govoip/store_offline_failed_count"
					libcomm.AttrAdd(attr, 1)
				} else {
					infoLog.Printf("DispatchVOIPMessage Build push packet success store offline success uid=%v ret=%v", reqHead.To, ret)
				}
			}
		} else {
			ret, err := offlineApi.SendPacketWithXTHead(reqHead, payLoad, nil)
			if err != nil {
				infoLog.Printf("DispatchVOIPMessage only store offline failed uid=%v err=%s", reqHead.To, err)
				attr := "govoip/store_offline_failed_count"
				libcomm.AttrAdd(attr, 1)
			} else {
				infoLog.Printf("DispatchVOIPMessage only store offline success uid=%v ret=%v", reqHead.To, ret)
			}
		}
	}
	return
}

func BuildPushPacket(terminalType uint8, fromId, toId uint32, nickName, pushContent string) (outPacket []byte, err error) {
	infoLog.Printf("BuildPushPacket fromId=%v toId=%v terminalType=%v nickName=%s pushContent=%s",
		fromId,
		toId,
		terminalType,
		nickName,
		pushContent)
	if fromId == 0 || toId == 0 || terminalType > 1 {
		infoLog.Printf("BuildPushPacket invalid param from=%v to=%v terminalType=%v", fromId, toId, terminalType)
		err = ErrInputParamErr
		return nil, err
	}
	// VOIP推送的默认参数
	var roomId uint32 = 0
	var chatType uint8 = uint8(CT_P2P)
	var pushType uint8 = uint8(PUSH_VOIP)
	var sound uint8 = uint8(1)
	var lights uint8 = uint8(1)

	limitNickeName := make([]byte, NICKNAME_LEN)
	copy(limitNickeName, []byte(nickName))

	limitPushContent := make([]byte, CONTENT_LEN)
	copy(limitPushContent, []byte(pushContent))

	limitMsgId := make([]byte, CONTENT_LEN)
	msgId := fmt.Sprintf("%v", time.Now().Unix())
	copy(limitMsgId, []byte(msgId))

	var packetPayLoad []byte
	common.MarshalUint8(terminalType, &packetPayLoad)
	common.MarshalUint8(chatType, &packetPayLoad)
	common.MarshalUint32(fromId, &packetPayLoad)
	common.MarshalUint32(toId, &packetPayLoad)
	common.MarshalUint32(roomId, &packetPayLoad)
	common.MarshalUint8(pushType, &packetPayLoad)
	common.MarshalSlice(limitNickeName, &packetPayLoad)
	common.MarshalSlice(limitPushContent, &packetPayLoad)
	common.MarshalUint8(sound, &packetPayLoad)
	common.MarshalUint8(lights, &packetPayLoad)
	common.MarshalSlice(limitMsgId, &packetPayLoad)
	var actionId uint32
	var byAt uint8 = uint8(CNotBeenAt)
	common.MarshalUint32(actionId, &packetPayLoad)
	common.MarshalUint8(byAt, &packetPayLoad)

	head := &common.XTHead{
		Flag:     common.CServToServ,
		Version:  common.CVerMmedia,
		CryKey:   common.CServKey,
		TermType: terminalType,
		Cmd:      CMD_S2S_MESSAGE_PUSH,
		Seq:      GetPacketSeq(),
		From:     0,
		To:       0,
		Len:      0,
	}
	// 使用Server key 加密
	cryptoText := libcrypto.TEAEncrypt(string(packetPayLoad), SERVER_COMM_KEY)
	infoLog.Printf("BuildPushPacket cryptoText len=%v", len(cryptoText))

	head.Len = uint32(len(cryptoText)) //
	outPacket = make([]byte, common.XTHeadLen+head.Len)
	err = common.SerialXTHeadToSlice(head, outPacket[:])
	if err != nil {
		infoLog.Println("BuildPushPacket SerialXTHeadToSlice failed")
		return nil, err
	}
	copy(outPacket[common.XTHeadLen:], []byte(cryptoText)) // return code
	return outPacket, nil
}

func GetPacketSeq() (packetSeq uint16) {
	sendSeq++
	return sendSeq
}

func SendPacketToIMServerRelabile(stat *common.UserState, head *common.XTHead, payLoad []byte) (err error) {
	if stat.OnlineStat != common.ST_ONLINE {
		infoLog.Printf("SendPacketToIMServerRelabile online stat=%v error", stat.OnlineStat)
		return ErrInputParamErr
	}

	svrIp := inet_ntoa(int64(stat.SvrIp)).String()
	infoLog.Printf("SendPacketToIMServerRelabile svrIP=%s oriIP=%v", svrIp, stat.SvrIp)
	v, ok := imServerPool[svrIp]
	if !ok { // 不存在直接打印日志返回错误
		infoLog.Printf("SendPacketToIMServerRelabile not exist IM ip=%v imServerPool=%v", svrIp, imServerPool)
		return ErrOnlineIpErr
	}
	// 使用ServerKey加密发送到QIM
	cryptoText := libcrypto.TEAEncrypt(string(payLoad), SERVER_COMM_KEY)
	head.CryKey = common.CServKey
	tryCount := 2
	for tryCount > 0 {
		err = v.SendXTPacket(head, []byte(cryptoText))
		if err == nil {
			infoLog.Printf("SendPacketToIMServerRelabile send succ from=%v to=%v seq=%v cmd=%v", head.From, head.To, head.Seq, head.Cmd)
			return nil
		} else {
			infoLog.Printf("SendPacketToIMServerRelabile failed from=%v to=%v seq=%v cmd=%v err=%v", head.From, head.To, head.Seq, head.Cmd, err)
		}
		tryCount--
	}
	return common.ErrReachMaxTryCount
}

func WriteMessageLog(from, to uint32, callStat uint8, timeStamp uint64) {
	voipLog := &util.VoipMessageLog{
		FromId:       from,
		ToId:         to,
		CallStatus:   callStat,
		MsgTimeStamp: int64(timeStamp),
	}
	LogDbUtil.AsyncAddVoipMessageLog(voipLog, 1) //写入logDb超时时间1秒中
}

func ProcInviteAcceptOrCallEnd(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcInviteAcceptOrCallEnd invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	if head.Cmd == CMD_P2P_VOIP_INVITE_ACCEPT {
		attr := "govoip/voip_invite_accept_count"
		libcomm.AttrAdd(attr, 1)
	} else if head.Cmd == CMD_P2P_VIDEO_CHAT_INVITE_ACCEPT {
		attr := "govoip/video_invite_accept_count"
		libcomm.AttrAdd(attr, 1)
	} else if head.Cmd == CMD_P2P_VOIP_CALL_END {
		attr := "govoip/voip_call_end_count"
		libcomm.AttrAdd(attr, 1)
	} else if head.Cmd == CMD_P2P_VIDEO_CHAT_CALL_END {
		attr := "govoip/video_call_end_count"
		libcomm.AttrAdd(attr, 1)
	}

	var roomIdMsg VoipRoomId
	roomIdMsg.RoomId = common.UnMarshalSlice(&plainSlice)
	if len(roomIdMsg.RoomId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcInviteAcceptOrCallEnd error from=%v to=%v seq=%v cmd=0x%4x RoomId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			roomIdMsg.RoomId)
		return false
	}
	// add logic here
	infoLog.Printf("ProcInviteAcceptOrCallEnd recv req from=%v to=%v seq=%v cmd=0x%4x RoomId=%s",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		roomIdMsg.RoomId)
	// answer first
	SendResp(c, head, uint8(RET_SUCCESS))
	// continue process
	err := HandleSynchronizeMessage(head, roomIdMsg.RoomId)
	if err != nil {
		infoLog.Printf("ProcInviteAcceptOrCallEnd from=%v to=%v seq=%v cmd=0x%4x RoomId=%s HandleSynchronizeMessage failed",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			roomIdMsg.RoomId)
	}
	callStatus := ENUM_CALL_NON_STATUS
	var respPayLoad []byte
	common.MarshalSlice(roomIdMsg.RoomId, &respPayLoad)
	var curTS uint64
	curTS = uint64(time.Now().Unix())
	common.MarshalUint64(curTS, &respPayLoad)
	DispatchVOIPMessage(head, respPayLoad, nil)

	// write log
	if head.Cmd == CMD_P2P_VOIP_INVITE_ACCEPT ||
		head.Cmd == CMD_P2P_VIDEO_CHAT_INVITE_ACCEPT {
		callStatus = ENUM_CALL_ACCEPT
	} else if head.Cmd == CMD_P2P_VOIP_CALL_END ||
		head.Cmd == CMD_P2P_VIDEO_CHAT_CALL_END {
		callStatus = ENUM_CALL_TERMINATE
	}
	WriteMessageLog(head.From, head.To, uint8(callStatus), uint64(time.Now().Unix()))
	return true
}

func ProcInviteCancle(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcInviteCancle invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	if head.Cmd == CMD_P2P_VOIP_INVITE_CANCEL {
		attr := "govoip/voip_invite_cancle_count"
		libcomm.AttrAdd(attr, 1)
	} else {
		attr := "govoip/video_invite_cancle_count"
		libcomm.AttrAdd(attr, 1)
	}

	var cancleMsg CancleMessage
	cancleMsg.UserName = common.UnMarshalSlice(&plainSlice)
	cancleMsg.RoomId = common.UnMarshalSlice(&plainSlice)
	cancleMsg.CancleReason = common.UnMarshalUint8(&plainSlice)
	if len(cancleMsg.UserName) == 0 || len(cancleMsg.RoomId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcInviteCancle error from=%v to=%v seq=%v cmd=0x%4x UserName=%s RoomId=%s CancleReason=%v",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			cancleMsg.UserName,
			cancleMsg.RoomId,
			cancleMsg.CancleReason)
		return false
	}
	// add logic here
	infoLog.Printf("ProcInviteCancle from=%v to=%v seq=%v cmd=0x%4x UserName=%s RoomId=%s CancleReason=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		cancleMsg.UserName,
		cancleMsg.RoomId,
		cancleMsg.CancleReason)

	// answer first
	SendResp(c, head, uint8(RET_SUCCESS))

	err := HandleSynchronizeMessage(head, cancleMsg.RoomId)
	if err != nil {
		infoLog.Printf("ProcInviteCancle HandleSynchronizeMessage error=%s UserName=%s RoomId=%s CancleReason=%v",
			err,
			cancleMsg.UserName,
			cancleMsg.RoomId,
			cancleMsg.CancleReason)

	}

	callStatus := ENUM_CALL_NON_STATUS
	var respPayLoad []byte
	common.MarshalSlice(cancleMsg.UserName, &respPayLoad)
	common.MarshalSlice(cancleMsg.RoomId, &respPayLoad)
	common.MarshalUint8(cancleMsg.CancleReason, &respPayLoad)
	var curTS uint64
	curTS = uint64(time.Now().Unix())
	common.MarshalUint64(curTS, &respPayLoad)
	var pushParam []byte
	if cancleMsg.CancleReason == 0 {
		pushParam = []byte(PUSH_PARAM_CALLICANCEL)
		callStatus = ENUM_CALL_CANCLED
	} else {
		pushParam = []byte(PUSH_PARAM_CALLIMISS)
		callStatus = ENUM_CALL_TIME_OUT
	}
	curPush := &VoipPush{
		NickName:  cancleMsg.UserName,
		PushParam: pushParam,
	}
	DispatchVOIPMessage(head, respPayLoad, curPush)
	// write log
	if callStatus != ENUM_CALL_NON_STATUS && callStatus != ENUM_CALL_CONNECT {
		WriteMessageLog(head.From, head.To, uint8(callStatus), uint64(time.Now().Unix()))
	}
	return true
}

func HandleSynchronizeMessage(reqHead *common.XTHead, roomId []byte) (err error) {
	if reqHead == nil || len(roomId) == 0 {
		infoLog.Printf("HandleSynchronizeMessage invalid param reqHead=%v roomId=%s", reqHead, roomId)
		err = ErrInputParamErr
		return err
	}
	switch reqHead.Cmd {
	case CMD_VOIP_CONNECTMEDIA_SRV_OK, CMD_VIDEO_CHAT_CONNECTMEDIA_SRV_OK:
		voipInfo := &VoipInfo{
			FromId:    0,
			ToId:      reqHead.From,
			TimeStamp: time.Now().Unix(),
		}
		toId, ret, err := InsertVoipInfo(roomId, voipInfo)
		if err == nil {
			if ret && toId != reqHead.From { //已近存在roomId 对应的voipInfo 且两次请求的uid不相同
				var syncHead *common.XTHead
				if reqHead.Cmd == CMD_VOIP_CONNECTMEDIA_SRV_OK {
					syncHead = &common.XTHead{
						Flag:     reqHead.Flag,
						Version:  reqHead.Version,
						CryKey:   reqHead.CryKey,
						TermType: reqHead.TermType,
						Cmd:      CMD_VOIP_SYNCCONNECTMEDIA_SRV,
						Seq:      GetPacketSeq(),
						From:     reqHead.From,
						To:       toId,
						Len:      0,
					}
				} else {
					syncHead = &common.XTHead{
						Flag:     reqHead.Flag,
						Version:  reqHead.Version,
						CryKey:   reqHead.CryKey,
						TermType: reqHead.TermType,
						Cmd:      CMD_VIDEO_CHAT_SYNCCONNECTMEDIA_SRV,
						Seq:      GetPacketSeq(),
						From:     reqHead.From,
						To:       toId,
						Len:      0,
					}
				}
				DispatchSynchronizeMediaMessage(syncHead, roomId)
				// change call stat
				callStat := ENUM_CALL_ACCEPT
				WriteMessageLog(syncHead.From, syncHead.To, uint8(callStat), uint64(time.Now().Unix()))
			}
		} else {
			infoLog.Printf("HandleSynchronizeMessage InsertVoipInfo faield from=%v to=%v roomId=%s err=%s",
				reqHead.From,
				reqHead.To,
				roomId,
				err)
		}

	case CMD_P2P_VOIP_INVITE_CANCEL, CMD_P2P_VIDEO_CHAT_INVITE_CANCEL, CMD_P2P_VOIP_INVITE_REJECT, CMD_P2P_VIDEO_CHAT_INVITE_REJECT, CMD_P2P_VOIP_CALL_END, CMD_P2P_VIDEO_CHAT_CALL_END:
		DeleteRoomIdFromMap(roomId)
	default:
		infoLog.Printf("HandleSynchronizeMessage unhandle cmd=%v head=%#v", reqHead.Cmd, *reqHead)
	}
	return err
}

func DeleteRoomIdFromMap(roomId []byte) {
	voipLock.Lock()
	defer voipLock.Unlock()
	delete(mapVoipInfo, string(roomId))
}

func InsertVoipInfo(roomId []byte, voipInfo *VoipInfo) (toId uint32, ret bool, err error) {
	if len(roomId) == 0 || voipInfo == nil {
		infoLog.Printf("InsertVoipInfo input param err roomId=%s voipInfo=%#v", roomId, voipInfo)
		err = ErrInputParamErr
		return toId, ret, err
	}
	voipLock.Lock()
	defer voipLock.Unlock()
	if v, ok := mapVoipInfo[string(roomId)]; ok {
		toId = v.ToId
		ret = true
		delete(mapVoipInfo, string(roomId))
	} else {
		ret = false
		mapVoipInfo[string(roomId)] = voipInfo
	}
	return toId, ret, nil
}

func DispatchSynchronizeMediaMessage(head *common.XTHead, roomId []byte) (err error) {
	if head == nil || len(roomId) == 0 {
		infoLog.Printf("DispatchSynchronizeMediaMessage invalid param head=%v roomId=%s", head, roomId)
		err = ErrInputParamErr
		return err
	}
	fromId := head.From
	var respPayLoad []byte
	common.MarshalSlice(roomId, &respPayLoad)
	head.From = 0
	err = SendDateTo(head, respPayLoad)
	if err != nil {
		infoLog.Printf("DispatchSynchronizeMediaMessage SendDateTo to=%v failed err=%s", head.To, err)
		attr := "govoip/send_date_to_failed_count"
		libcomm.AttrAdd(attr, 1)
	}
	head.To = fromId
	err = SendDateTo(head, respPayLoad)
	if err != nil {
		infoLog.Printf("DispatchSynchronizeMediaMessage SendDateTo to=%v failed err=%s", head.To, err)
		attr := "govoip/send_date_to_failed_count"
		libcomm.AttrAdd(attr, 1)
	}
	return err
}

func SendDateTo(reqHead *common.XTHead, payLoad []byte) (err error) {
	if reqHead == nil || mcApi == nil {
		infoLog.Printf("SendDateTo nil param reqHead=%v mcApi=%v return", reqHead, mcApi)
		err = ErrNilParam
		return err

	}
	onlineStat, err := mcApi.GetUserOnlineStat(reqHead.To)
	if err == nil {
		err = SendPacketToIMServerRelabile(onlineStat, reqHead, payLoad)
		if err != nil {
			attr := "govoip/send_packet_to_im_failed_count"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("SendDateTo exec SendPacketToIMServerRelabile failed from=%v to=%v err=%v",
				reqHead.From,
				reqHead.To,
				err)
		}
	} else {
		infoLog.Printf("SendDateTo GetUserOnlineStat failed from=%v to=%v cmd=%v payLoad=%s err=%s",
			reqHead.From,
			reqHead.To,
			reqHead.Cmd,
			payLoad,
			err)
	}
	return err
}

func ProcInviteReject(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcInviteReject invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	if head.Cmd == CMD_P2P_VOIP_INVITE_CANCEL {
		attr := "govoip/voip_invite_reject_count"
		libcomm.AttrAdd(attr, 1)
	} else {
		attr := "govoip/video_invite_reject_count"
		libcomm.AttrAdd(attr, 1)
	}

	var rejectMsg RejectMessage
	rejectMsg.RoomId = common.UnMarshalSlice(&plainSlice)
	rejectMsg.RejectReason = common.UnMarshalUint8(&plainSlice)
	if len(rejectMsg.RoomId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcInviteReject error from=%v to=%v seq=%v cmd=0x%4x RoomId=%s rejectReason=%v",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			rejectMsg.RoomId,
			rejectMsg.RejectReason)
		return false
	}
	// add logic here
	infoLog.Printf("ProcInviteReject recv req from=%v to=%v seq=%v cmd=0x%4x RoomId=%s rejectReason=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		rejectMsg.RoomId,
		rejectMsg.RejectReason)

	// answer first
	SendResp(c, head, uint8(RET_SUCCESS))

	err := HandleSynchronizeMessage(head, rejectMsg.RoomId)
	if err != nil {
		infoLog.Printf("ProcInviteReject HandleSynchronizeMessage error=%s RoomId=%s RejectReason=%v",
			err,
			rejectMsg.RoomId,
			rejectMsg.RejectReason)
	}

	callStatus := ENUM_CALL_NON_STATUS
	var respPayLoad []byte
	common.MarshalSlice(rejectMsg.RoomId, &respPayLoad)
	common.MarshalUint8(rejectMsg.RejectReason, &respPayLoad)
	var curTS uint64
	curTS = uint64(time.Now().Unix())
	common.MarshalUint64(curTS, &respPayLoad)

	DispatchVOIPMessage(head, respPayLoad, nil)

	if rejectMsg.RejectReason != 0 {
		callStatus = ENUM_CALL_BUSY
	} else {
		callStatus = ENUM_CALL_REJECT
	}
	if callStatus != ENUM_CALL_NON_STATUS && callStatus != ENUM_CALL_CONNECT {
		WriteMessageLog(head.From, head.To, uint8(callStatus), uint64(time.Now().Unix()))
	}

	return true
}

func ProcConnectMediaServerOk(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcConnectMediaServerOk invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	if head.Cmd == CMD_VOIP_CONNECTMEDIA_SRV_OK {
		attr := "govoip/voip_connect_media_srv_ok_count"
		libcomm.AttrAdd(attr, 1)
	} else {
		attr := "govoip/video_connect_media_srv_ok_count"
		libcomm.AttrAdd(attr, 1)
	}

	var roomIdMsg VoipRoomId
	roomIdMsg.RoomId = common.UnMarshalSlice(&plainSlice)
	if len(roomIdMsg.RoomId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcConnectMediaServerOk error from=%v to=%v seq=%v cmd=0x%4x RoomId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			roomIdMsg.RoomId)
		return false
	}
	// add logic here
	infoLog.Printf("ProcConnectMediaServerOk recv req from=%v to=%v seq=%v Cmd==0x%4x RoomId=%s",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		roomIdMsg.RoomId)

	// answer first
	SendResp(c, head, uint8(RET_SUCCESS))
	// handle message
	err := HandleSynchronizeMessage(head, roomIdMsg.RoomId)
	if err != nil {
		infoLog.Printf("ProcConnectMediaServerOk HandleSynchronizeMessage error=%s from=%v to=%v seq=%v Cmd=0x%4x RoomId=%s",
			err,
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			roomIdMsg.RoomId)
	}

	callStatus := ENUM_CALL_CONNECT
	WriteMessageLog(head.From, head.To, uint8(callStatus), uint64(time.Now().Unix()))

	return true
}

func ProcGVoipCreateReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcGVoipCreateReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	// add static
	attr := "govoip/gvoip_create_req_count"
	libcomm.AttrAdd(attr, 1)

	// resolve param

	var creatReq GVoipCreateMessage
	creatReq.GroupId = common.UnMarshalUint32(&plainSlice)
	creatReq.ChannelId = common.UnMarshalSlice(&plainSlice)
	creatReq.UserName = common.UnMarshalSlice(&plainSlice)
	creatReq.TimeStamp = common.UnMarshalUint64(&plainSlice)

	//use server time
	creatReq.TimeStamp = uint64(time.Now().Unix())
	// Action log
	infoLog.Printf("ProcGVoipCreateReq recv req from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s userName=%s time=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		creatReq.GroupId,
		creatReq.ChannelId,
		creatReq.UserName,
		creatReq.TimeStamp)
	// Param check
	if head.From == 0 || creatReq.GroupId == 0 || len(creatReq.ChannelId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGVoipCreateReq param err from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			creatReq.GroupId,
			creatReq.ChannelId)
		return false
	}

	// logic process
	ret := gvoipManager.IsLegalCreateRequest(head.From, creatReq.GroupId)
	infoLog.Printf("ProcGVoipCreateReq IsLegalCreateRequest ret=%v", ret)
	if !ret { // gvoid is already exist direct join the session
		infoLog.Printf("ProcGVoipCreateReq gvoip already exist direct join from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s userName=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			creatReq.GroupId,
			creatReq.ChannelId,
			creatReq.UserName)
		SendResp(c, head, uint8(AlREADY_EXIST_AND_DIREC_JOIN))
		ret = gvoipManager.AddToActiveList(creatReq.GroupId, head.From)
		if !ret {
			infoLog.Printf("ProcGVoipCreateReq AddToActiveList failed groupId=%v channelId=%s from=%v",
				creatReq.GroupId,
				creatReq.ChannelId,
				head.From)
		}
		err := gvoipManager.BroadCastGVOIPMemberChange(creatReq.GroupId, head.From, uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_MEMBER_JOIN_BROADCAST))
		if err != nil {
			infoLog.Printf("ProcGVoipCreateReq BroadCastGVOIPMemberChange faild groupId=%v channelId=%s from=%",
				creatReq.GroupId,
				creatReq.ChannelId,
				head.From)
		}
	} else {
		// Send response
		SendResp(c, head, uint8(CREATE_SUCCESS))
		infoLog.Printf("ProcGVoipCreateReq new gvoip chat from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s userName=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			creatReq.GroupId,
			creatReq.ChannelId,
			creatReq.UserName)
		gvoipManager.AddGVOIPSessionInfo(head.From, creatReq.GroupId, creatReq.ChannelId, int64(creatReq.TimeStamp))
		err := gvoipManager.BroadCastGVOIPIsBegin(head.From, creatReq.GroupId, creatReq.UserName, creatReq.ChannelId, int64(creatReq.TimeStamp))
		if err != nil {
			infoLog.Printf("ProcGVoipCreateReq BroadCastGVOIPIsBegin faild groupId=%v channelId=%s from=%",
				creatReq.GroupId,
				creatReq.ChannelId,
				head.From)
		}
	}
	return true
}

func ProcGVoipJoinReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcGVoipJoinReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	// add static
	attr := "govoip/gvoip_join_req_count"
	libcomm.AttrAdd(attr, 1)

	// resolve param

	var joinReq GVoipJoinMessage
	joinReq.GroupId = common.UnMarshalUint32(&plainSlice)
	joinReq.ChannelId = common.UnMarshalSlice(&plainSlice)
	joinReq.TimeStamp = common.UnMarshalUint64(&plainSlice)

	// Action log
	infoLog.Printf("ProcGVoipJoinReq recv req from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s time=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		joinReq.GroupId,
		joinReq.ChannelId,
		joinReq.TimeStamp)
	// Param check
	if head.From == 0 || joinReq.GroupId == 0 || len(joinReq.ChannelId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGVoipJoinReq param err from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			joinReq.GroupId,
			joinReq.ChannelId)
		return false
	}
	// logic process
	ret := gvoipManager.IsUserCanJoin(joinReq.GroupId, head.From)
	if !ret {
		infoLog.Printf("ProcGVoipJoinReq uid=%v group=%v can't not join", head.From, joinReq.GroupId)
		SendResp(c, head, uint8(JOIN_FAILED))
	} else {
		infoLog.Printf("ProcGVoipJoinReq uid=%v group=%v join success", head.From, joinReq.GroupId)
		SendResp(c, head, uint8(ALLOW_JOIN))
	}
	return true
}

func ProcGVoipLoginReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcGVoipLoginReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	// add static
	attr := "govoip/gvoip_login_req_count"
	libcomm.AttrAdd(attr, 1)

	// resolve param

	var loginReq GVoipLoginReportMessage
	loginReq.GroupId = common.UnMarshalUint32(&plainSlice)
	loginReq.ChannelId = common.UnMarshalSlice(&plainSlice)
	loginReq.TimeStamp = common.UnMarshalUint64(&plainSlice)

	// Action log
	infoLog.Printf("ProcGVoipLoginReq recv req from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s time=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		loginReq.GroupId,
		loginReq.ChannelId,
		loginReq.TimeStamp)
	// Param check
	if head.From == 0 || loginReq.GroupId == 0 || len(loginReq.ChannelId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGVoipLoginReq param err from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			loginReq.GroupId,
			loginReq.ChannelId)
		return false
	}

	// logic process
	SendResp(c, head, uint8(0))

	ret := gvoipManager.AddToActiveList(loginReq.GroupId, head.From)
	if !ret {
		infoLog.Printf("ProcGVoipLoginReq join faild from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			loginReq.GroupId,
			loginReq.ChannelId)
	}
	err := gvoipManager.BroadCastGVOIPMemberChange(loginReq.GroupId, head.From, uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_MEMBER_JOIN_BROADCAST))
	if err != nil {
		infoLog.Printf("ProcGVoipLoginReq BroadCastGVOIPMemberChange failed.from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s err=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			loginReq.GroupId,
			loginReq.ChannelId,
			err)
	}
	return true
}

func ProcGVoipLogoutReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcGVoipLogoutReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	// add static
	attr := "govoip/gvoip_logout_req_count"
	libcomm.AttrAdd(attr, 1)

	// resolve param

	var logoutReq GVoipLogoutReportMessage
	logoutReq.GroupId = common.UnMarshalUint32(&plainSlice)
	logoutReq.ChannelId = common.UnMarshalSlice(&plainSlice)
	logoutReq.TimeStamp = common.UnMarshalUint64(&plainSlice)

	// Action log
	infoLog.Printf("ProcGVoipLogoutReq recv req from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s time=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		logoutReq.GroupId,
		logoutReq.ChannelId,
		logoutReq.TimeStamp)
	// Param check
	if head.From == 0 || logoutReq.GroupId == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGVoipLogoutReq param err from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			logoutReq.GroupId,
			logoutReq.ChannelId)
		return false
	}

	// logic process
	SendResp(c, head, uint8(0))

	ret := gvoipManager.DelFromActiveList(logoutReq.GroupId, head.From)
	if !ret {
		infoLog.Printf("ProcGVoipLoginReq DelFromActiveList faild from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			logoutReq.GroupId,
			logoutReq.ChannelId)
	}
	err := gvoipManager.BroadCastGVOIPMemberChange(logoutReq.GroupId, head.From, uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_MEMBER_LEAVE_BROADCAST))
	if err != nil {
		infoLog.Printf("ProcGVoipLoginReq BroadCastGVOIPMemberChange failed.from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s err=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			logoutReq.GroupId,
			logoutReq.ChannelId,
			err)
	}
	activeCount := gvoipManager.GetActiveMemberCount(logoutReq.GroupId)
	if activeCount == 0 {
		infoLog.Printf("ProcGVoipLoginReq active member empty from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			logoutReq.GroupId,
			logoutReq.ChannelId)
		err := gvoipManager.BroadCastGVOIPIsEnd(head.From,
			logoutReq.GroupId,
			logoutReq.ChannelId,
			uint64(time.Now().Unix()))
		if err != nil {
			infoLog.Printf("ProcGVoipLoginReq BroadCastGVOIPIsEnd faied from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
				head.From,
				head.To,
				head.Seq,
				head.Cmd,
				logoutReq.GroupId,
				logoutReq.ChannelId)
		}
		gvoipManager.DelGVOIPSessionInfo(logoutReq.GroupId)
	}
	return true
}

func ProcGVoipGetMemberListReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcGVoipGetMemberListReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	// add static
	attr := "govoip/gvoip_get_member_list_req_count"
	libcomm.AttrAdd(attr, 1)

	// resolve param

	var getMemberListReq GVoipGetMemeberListMessage
	getMemberListReq.GroupId = common.UnMarshalUint32(&plainSlice)
	getMemberListReq.ChannelId = common.UnMarshalSlice(&plainSlice)
	getMemberListReq.TimeStamp = common.UnMarshalUint64(&plainSlice)

	// Action log
	infoLog.Printf("ProcGVoipGetMemberListReq recv req from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s time=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		getMemberListReq.GroupId,
		getMemberListReq.ChannelId,
		getMemberListReq.TimeStamp)
	// Param check
	if head.From == 0 || getMemberListReq.GroupId == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGVoipGetMemberListReq param err from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			getMemberListReq.GroupId,
			getMemberListReq.ChannelId)
		return false
	}

	// logic process
	ret := gvoipManager.AddToMemberCountWatcherList(getMemberListReq.GroupId, head.From)
	if !ret {
		infoLog.Printf("ProcGVoipGetMemberListReq AddToMemberCountWatcherList failed from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			getMemberListReq.GroupId,
			getMemberListReq.ChannelId)
	}

	memberList := gvoipManager.GetActiveMemberList(getMemberListReq.GroupId)
	var respPayLoad []byte
	common.MarshalUint8(uint8(0), &respPayLoad)
	common.MarshalUint32(getMemberListReq.GroupId, &respPayLoad)
	common.MarshalSlice(getMemberListReq.ChannelId, &respPayLoad)
	common.MarshalUint32(uint32(len(memberList)), &respPayLoad)
	for _, v := range memberList {
		common.MarshalUint32(v, &respPayLoad)
	}

	sendRet := SendRespWithPayLoad(c, head, respPayLoad)
	if !sendRet {
		infoLog.Printf("ProcGVoipGetMemberListReq ProcGVoipGetMemberListReq failed from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			getMemberListReq.GroupId,
			getMemberListReq.ChannelId)
	}
	return true
}

func ProcGVoipPingReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcGVoipPingReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	// add static
	attr := "govoip/gvoip_ping_req_count"
	libcomm.AttrAdd(attr, 1)

	// resolve param

	var pingReq GVoipPingMessage
	pingReq.GroupId = common.UnMarshalUint32(&plainSlice)
	pingReq.ChannelId = common.UnMarshalSlice(&plainSlice)
	pingReq.TimeStamp = common.UnMarshalUint64(&plainSlice)

	// Action log
	infoLog.Printf("ProcGVoipPingReq recv req from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s time=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		pingReq.GroupId,
		pingReq.ChannelId,
		pingReq.TimeStamp)
	// Param check
	if head.From == 0 || pingReq.GroupId == 0 || len(pingReq.ChannelId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGVoipPingReq param err from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			pingReq.GroupId,
			pingReq.ChannelId)
		return false
	}

	// logic process
	sendRet := SendResp(c, head, uint8(0))
	if !sendRet {
		infoLog.Printf("ProcGVoipPingReq SendResp failed from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			pingReq.GroupId,
			pingReq.ChannelId)
	}

	ret := gvoipManager.HandleGVOIPPing(pingReq.GroupId, head.From, pingReq.ChannelId, pingReq.TimeStamp)
	if !ret {
		infoLog.Printf("ProcGVoipPingReq HandleGVOIPPing client timeout from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			pingReq.GroupId,
			pingReq.ChannelId)
	}
	return true
}

func ProcGVoipRevokeMemberChangeReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcGVoipRevokeMemberChangeReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	// add static
	attr := "govoip/gvoip_revoke_member_req_count"
	libcomm.AttrAdd(attr, 1)

	// resolve param

	var revokeReq GVoipRevokeMessage
	revokeReq.GroupId = common.UnMarshalUint32(&plainSlice)
	revokeReq.ChannelId = common.UnMarshalSlice(&plainSlice)
	revokeReq.TimeStamp = common.UnMarshalUint64(&plainSlice)

	// Action log
	infoLog.Printf("ProcGVoipRevokeMemberChangeReq recv req from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s time=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		revokeReq.GroupId,
		revokeReq.ChannelId,
		revokeReq.TimeStamp)
	// Param check
	if head.From == 0 || revokeReq.GroupId == 0 || len(revokeReq.ChannelId) == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGVoipRevokeMemberChangeReq param err from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			revokeReq.GroupId,
			revokeReq.ChannelId)
		return false
	}

	// logic process
	sendRet := SendResp(c, head, uint8(0))
	if !sendRet {
		infoLog.Printf("ProcGVoipRevokeMemberChangeReq SendResp failed from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			revokeReq.GroupId,
			revokeReq.ChannelId)
	}

	ret := gvoipManager.DelFromMemberCountWatcherList(revokeReq.GroupId, head.From)
	if !ret {
		infoLog.Printf("ProcGVoipRevokeMemberChangeReq DelFromMemberCountWatcherList failed from=%v to=%v seq=%v cmd=0x%4x groupId=%v channelId=%s",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			revokeReq.GroupId,
			revokeReq.ChannelId)
	}
	return true
}

func ProcRoomIsGVoipReq(c *gotcp.Conn, head *common.XTHead, plainSlice []byte) bool {
	if c == nil || head == nil || len(plainSlice) == 0 {
		infoLog.Printf("ProcRoomIsGVoipReq invalid param c=%v head=%v packet=%v", c, head, plainSlice)
		return false
	}
	// add static
	attr := "govoip/is_gvoip_req_count"
	libcomm.AttrAdd(attr, 1)

	// resolve param
	roomId := common.UnMarshalUint32(&plainSlice)
	// Action log
	infoLog.Printf("ProcRoomIsGVoipReq recv req from=%v to=%v seq=%v cmd=0x%4x roomId=%v",
		head.From,
		head.To,
		head.Seq,
		head.Cmd,
		roomId)
	// Param check
	if head.From == 0 || roomId == 0 {
		SendResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcRoomIsGVoipReq param err from=%v to=%v seq=%v cmd=0x%4x roomId=%v",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			roomId)
		return false
	}
	ret := gvoipManager.IsRoomGvoiping(roomId)
	infoLog.Printf("ProcRoomIsGVoipReq roomId=%v ret=%v", roomId, ret)

	// logic process
	sendRet := SendResp(c, head, uint8(ret))
	if !sendRet {
		infoLog.Printf("ProcRoomIsGVoipReq SendResp failed from=%v to=%v seq=%v cmd=0x%4x roomId=%v",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			roomId)
	}
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mysql
	// mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	// mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	// mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	// mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	// mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	// infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
	// 	mysqlHost,
	// 	mysqlUser,
	// 	mysqlPasswd,
	// 	mysqlDbName,
	// 	mysqlPort)

	// db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	// if err != nil {
	// 	infoLog.Println("open mysql failed")
	// 	panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	// }

	// init mysql logDB
	mysqlLogHost := cfg.Section("MYSQLLOG").Key("mysql_host").MustString("127.0.0.1")
	mysqlLogUser := cfg.Section("MYSQLLOG").Key("mysql_user").MustString("IMServer")
	mysqlLogPasswd := cfg.Section("MYSQLLOG").Key("mysql_passwd").MustString("hello")
	mysqlLogDbName := cfg.Section("MYSQLLOG").Key("mysql_db").MustString("HT_IMDB")
	mysqlLogPort := cfg.Section("MYSQLLOG").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysqlLog host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlLogHost,
		mysqlLogUser,
		mysqlLogPasswd,
		mysqlLogDbName,
		mysqlLogPort)

	logDb, err = sql.Open("mysql", mysqlLogUser+":"+mysqlLogPasswd+"@"+"tcp("+mysqlLogHost+":"+mysqlLogPort+")/"+mysqlLogDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Println("open mysqlLog failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	logChangLen := cfg.Section("LOGCHANLEN").Key("length").MustInt(2000)
	LogDbUtil = util.NewLogDbUtil(logDb, infoLog, uint32(logChangLen))
	LogDbUtil.Do()

	// 读取 Memcache Ip and port
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustInt(11211)
	infoLog.Printf("memcache ip=%v port=%v", mcIp, mcPort)
	mcApi = new(common.MemcacheApi)
	mcApi.Init(mcIp + ":" + strconv.Itoa(mcPort))

	// 读取offline 配置
	offlineIp := cfg.Section("OFFLINE").Key("offline_ip").MustString("127.0.0.1")
	offlinePort := cfg.Section("OFFLINE").Key("offline_port").MustString("0")
	offlineConnLimit := cfg.Section("OFFLINE").Key("pool_limit").MustInt(1000)
	infoLog.Printf("offline server ip=%v port=%v connLimit=%v", offlineIp, offlinePort, offlineConnLimit)
	offlineApi = common.NewOfflineApiV2(offlineIp, offlinePort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, offlineConnLimit)

	// 读取muc 配置
	mucIp := cfg.Section("MUC").Key("muc_ip").MustString("127.0.0.1")
	mucPort := cfg.Section("MUC").Key("muc_port").MustString("0")
	mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	infoLog.Printf("muc server ip=%v port=%v connLimit=%v", mucIp, mucPort, mucConnLimit)
	mucApi := common.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, mucConnLimit)
	gvoipManager = util.NewGVoipManager(infoLog, mucApi)
	gvoipManager.Do()

	// 读取IMServer 配置
	imCount := cfg.Section("IMSERVER").Key("imserver_cnt").MustInt(2)
	imConnLimit := cfg.Section("IMSERVER").Key("pool_limit").MustInt(1000)
	imServerPool = make(map[string]*common.ImServerApiV2, imCount)
	infoLog.Printf("IMServer Count=%v", imCount)
	for i := 0; i < imCount; i++ {
		ipKey := "imserver_ip_" + strconv.Itoa(i)
		ipOnlineKye := "imserver_ip_online_" + strconv.Itoa(i)
		portKey := "imserver_port_" + strconv.Itoa(i)
		imIp := cfg.Section("IMSERVER").Key(ipKey).MustString("127.0.0.1")
		imIpOnline := cfg.Section("IMSERVER").Key(ipOnlineKye).MustString("127.0.0.1")
		imPort := cfg.Section("IMSERVER").Key(portKey).MustString("18380")

		infoLog.Printf("im server ip=%v ip_online=%v port=%v", imIp, imIpOnline, imPort)
		imServerPool[imIpOnline] = common.NewImServerApiV2(imIp, imPort, 3*time.Second, 3*time.Second, &common.XTHeadProtocol{}, imConnLimit)
	}
	infoLog.Printf("imServerPool=%v", imServerPool)

	// init send seq
	sendSeq = 0

	// 创建mapVoipInfo
	mapVoipInfo = map[string]*VoipInfo{}

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}
	srv := gotcp.NewServer(config, &Callback{}, &common.XTHeadProtocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	logDb.Close()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
