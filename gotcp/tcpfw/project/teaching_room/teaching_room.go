package main

import (
	"database/sql"
	"encoding/json"

	//simplejson "github.com/bitly/go-simplejson"
	"github.com/HT_GOGO/gotcp"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_teachingroom"
	"github.com/HT_GOGO/gotcp/tcpfw/project/teaching_room/util"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

const (
	ProcSlowThreshold = 300000
	WhiteBoardName    = "whiteboard"
	RecordName        = "record"
	HangUpName        = "hangup"
)

type Callback struct{}

var (
	infoLog *log.Logger
	db      *sql.DB
	//ssdb        *gossdb.Connectors
	mongoSess           *mgo.Session
	DbUtil              *util.DbUtil
	teachingRoomManager *util.TeachingRoomManager
	mucApi              *common.MucApi
	voipApi             *common.VoipApi
)

type CallbackDataItemUrls struct {
	Url    string `json:"url" binding:"omitempty"`
	Userid string `json:"userid" binding:"omitempty"`
}

type CallbackDataItem struct {
	AppId      string `json:"app_id" binding:"required"`
	ObjectName string `json:"object" binding:"required"`
	RoomName   string `json:"roomname" binding:"omitempty"`

	Url          string `json:"url" binding:"omitempty"` //这个只有record 才有
	BusinessData string `json:"businessdata" binding:"omitempty"`

	Totalsecond uint32 `json:"totalsecond" binding:"omitempty"` //下面四个只有 hangup 才有
	Totalminute uint32 `json:"totalminute" binding:"omitempty"`
	TimeStart   string `json:"time_start" binding:"omitempty"`
	TimeEnd     string `json:"time_end" binding:"omitempty"`

	Urls []CallbackDataItemUrls `json:"urls" binding:"omitempty"` //只有whiteboard才有

}

type CallbackData struct {
	Id         uint32           `json:"id" binding:"required"`
	TimeCreate uint64           `json:"time_create" binding:"required"`
	Type       string           `json:"type" binding:"required"`
	Data       CallbackDataItem `json:"data" binding:"required"`
}

func SubString(str string, begin, length int) (substr string) {
	// 将字符串的转换成[]rune
	rs := []rune(str)
	lth := len(rs)

	// 简单的越界判断
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length
	if end > lth {
		end = lth
	}

	// 返回子串
	return string(rs[begin:end])
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV3packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}
	// 统计总的请求量
	attr := "gotr/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	if head.Cmd == uint16(ht_teaching_room.TR_CMD_TYPE_CMD_WHITE_BOARD_CALL_BACK) {
		go ProcWhiteBoardCallBack(c, head, packet)
		return true
	}

	switch head.Cmd {
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_CREATE_TEACHING_ROOM):
		go ProcCreateTeachingRoom(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_REQ_BR_CREATE_TEACHING_ROOM):
		go ProcReqBroadCastRoomCreated(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_REQ_JOIN_TEACHING_ROOM):
		go ProcReqJoinRoom(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_REQ_LEAVE_TEACHING_ROOM):
		go ProcReqLeaveRoom(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_TEACHING_ROOM_HEAT_BEAT):
		go ProcTeachingRoomHeartBeat(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_UPDATE_TEACHING_ROOM_STAT):
		go ProcUpdateRoomStat(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_REQ_HANDS_UP):
		go ProcHandsUpReq(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_CANCLE_HANDS_UP):
		go ProcCancleHandsUpReq(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_TEACHER_HANDLE_HANDS_UP):
		go ProcHandleStudentHandsUp(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_UPDATE_FAV_AND_SHARE_COUNT):
		go ProcUpdateFavAndShareCount(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_GET_FAV_AND_SHARE_COUNT):
		go ProcGetFavAndSharecount(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_GET_LESSONS_BY_OBID):
		go ProcGetLessonByObid(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_DEL_LESSONS_BY_OBIDS):
		go ProcDelLessonByObid(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_TEACHING_ROOM_END):
		go ProcTeachingRoomEnd(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_UPDATE_LESSON_URL):
		go ProcUpdateLessonCoverUrl(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_GET_LESSON_BY_ONE_OBID):
		go ProcGetLessonByOneObid(c, head, packet)
	case uint16(ht_teaching_room.TR_CMD_TYPE_CMD_GET_SERVER_TS):
		go ProcSyncSrvTs(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV3, rspBody *ht_teaching_room.TeachingRoomRspBody, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendCreatRoomResp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 1.Proc create teaching room
func ProcCreateTeachingRoom(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcCreateTeachingRoom no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcCreateTeachingRoom invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/creat_teaching_room_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcCreateTeachingRoom proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetCreateTeachingRoomReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcCreateTeachingRoom GetCreateTeachingRoomReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	createUid := subReqBody.GetCreateUid()
	notifyUidList := subReqBody.GetNotifyUidList()
	lessonName := subReqBody.GetLessonName()
	lessonAbstrace := subReqBody.GetLessonAbstract()
	lessonCover := subReqBody.GetLessonCoverUrl()

	infoLog.Printf("ProcCreateTeachingRoom roomId=%v wbid=%v createUid=%#v notifyUidLen=%v lessonName=%s lessonAbstract=%s lessonCover=%s",
		roomId,
		wBid,
		*createUid,
		len(notifyUidList),
		lessonName,
		lessonAbstrace,
		lessonCover)

	// Step1: 输入参数检查
	if roomId == 0 ||
		wBid == 0 ||
		createUid.GetUid() == 0 ||
		len(notifyUidList) == 0 ||
		len(lessonName) == 0 {
		infoLog.Printf("ProcCreateTeachingRoom invalid param roomId=%v wbid=%v createUid=%v notifyUidLen=%v lessonName=%s lessonAbstract=%s lessonCover=%s",
			roomId,
			wBid,
			createUid.GetUid(),
			len(notifyUidList),
			lessonName,
			lessonAbstrace,
			lessonCover)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 检查当前是否已经正在进行教学
	ok, err := teachingRoomManager.IsRoomTeaching(roomId)
	if err != nil {
		infoLog.Printf("ProcCreateTeachingRoom teachingRoomManager.IsRoomTeaching roomId=%v err=%s",
			roomId,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// 教学房间正在进行教学 返回课程已经开始了
	if ok {
		infoLog.Printf("ProcCreateTeachingRoom roomId=%v wbid=%v createUid=%v notifyUidLen=%v lessonName=%s lessonAbstract=%s lessonCover=%s already begin",
			roomId,
			wBid,
			createUid.GetUid(),
			len(notifyUidList),
			lessonName,
			lessonAbstrace,
			lessonCover)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_LESSION_ALREADY_BEGIN
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("already begin"),
			},
		}
		return false
	}
	// Step3: 查询muc申请用户是否管理员
	isAdmin, err := mucApi.IsUserAdmin(roomId, createUid.GetUid())
	if err != nil || isAdmin == false {
		infoLog.Printf("ProcCreateTeachingRoom roomId=%v createUid=%v mucApi.IsUserAdmin isAdmin=%v err=%s",
			roomId,
			createUid.GetUid(),
			isAdmin,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_NO_PERMISSION
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("no permission"),
			},
		}
		return false
	}
	// Step4: 查询voip 当前房间是否正在进行群语音
	xtHead := &common.XTHead{
		Flag:     head.Flag,
		Version:  head.Version,
		CryKey:   head.CryKey,
		TermType: head.TermType,
		Cmd:      head.Cmd,
		Seq:      head.Seq,
		From:     head.From,
		To:       head.To,
		Len:      0,
	}
	isVoipping, err := voipApi.GetRoomIsVoipping(xtHead, roomId)
	if err != nil || isVoipping == true {
		infoLog.Printf("ProcCreateTeachingRoom roomId=%v createUid=%v voipApi.GetRoomIsVoipping isVoipping=%v err=%s",
			roomId,
			createUid.GetUid(),
			isVoipping,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_IS_VOIPPING
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("is voipping"),
			},
		}
		return false
	}
	// Step5: 产生HelloTalk uid到WhiteBoard 的映射
	outUidList, mapedUidList, err := teachingRoomManager.GenerateWightBoardUidMap(createUid.GetUid(), roomId, wBid, notifyUidList)
	if err != nil {
		infoLog.Printf("ProcCreateTeachingRoom teachingRoomManager.GenerateWightBoardUidMap roomId=%v wbid=%v createUid=%v notifyUidLen=%v lessonName=%s lessonAbstract=%s lessonCover=%s failed err=%s",
			roomId,
			wBid,
			createUid.GetUid(),
			len(notifyUidList),
			lessonName,
			lessonAbstrace,
			lessonCover,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step6: 记录教学房间状态
	obid, err := teachingRoomManager.AddTeachingRoomInfo(roomId, wBid, createUid, lessonName, lessonAbstrace, lessonCover, outUidList, mapedUidList)
	if err != nil {
		infoLog.Printf("ProcCreateTeachingRoom teachingRoomManager.AddTeachingRoomInfo roomId=%v wbid=%v createUid=%v notifyUidLen=%v lessonName=%s lessonAbstract=%s lessonCover=%s failed err=%s",
			roomId,
			wBid,
			createUid.GetUid(),
			len(notifyUidList),
			lessonName,
			lessonAbstrace,
			lessonCover,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcCreateTeachingRoom teachingRoomManager.AddTeachingRoomInfo roomId=%v wbid=%v createUid=%v", roomId, obid, createUid.GetUid())

	// Step7: 返回响应
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.CreateTeachingRoomRspbody = &ht_teaching_room.CreateTeachingRoomRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		HtUidList: outUidList,
		WbUidList: mapedUidList,
		Obid:      []byte(obid),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/create_teaching_room_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.proc req boradcast room created
func ProcReqBroadCastRoomCreated(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcReqBroadCastRoomCreated no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.ReqBroaCastRoomCreatedRspbody = &ht_teaching_room.ReqBroadCastRoomCreatedRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcReqBroadCastRoomCreated invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/req_bc_room_created_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcReqBroadCastRoomCreated proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.ReqBroaCastRoomCreatedRspbody = &ht_teaching_room.ReqBroadCastRoomCreatedRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetReqBroaCastRoomCreatedReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcReqBroadCastRoomCreated GetReqBroaCastRoomCreatedReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.ReqBroaCastRoomCreatedRspbody = &ht_teaching_room.ReqBroadCastRoomCreatedRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	reqUid := subReqBody.GetHtUid()
	infoLog.Printf("ProcReqBroadCastRoomCreated roomId=%v wbid=%v reqUid=%v",
		roomId,
		wBid,
		reqUid)

	// Step1: param check
	if roomId == 0 || wBid == 0 || reqUid == 0 {
		infoLog.Printf("ProcReqBroadCastRoomCreated invalid param roomId=%v wBid=%v reqUid=%v",
			roomId,
			wBid,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.ReqBroaCastRoomCreatedRspbody = &ht_teaching_room.ReqBroadCastRoomCreatedRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step2: get roominfo by roomid
	roomInfo, err := teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcReqBroadCastRoomCreated teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.ReqBroaCastRoomCreatedRspbody = &ht_teaching_room.ReqBroadCastRoomCreatedRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step3: check whether user is creater
	if reqUid != roomInfo.CreateUid.Uid {
		infoLog.Printf("ProcReqBroadCastRoomCreated roomId=%v reqUid=%v createUid=%v not equal",
			roomId,
			reqUid,
			roomInfo.CreateUid.Uid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.ReqBroaCastRoomCreatedRspbody = &ht_teaching_room.ReqBroadCastRoomCreatedRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step5: Broad cast notification
	retCode, err := mucApi.BroadCastTeachingRoomCreated(
		roomId,
		roomInfo.WhiteBoardId,
		roomInfo.CreateUid.Uid,
		roomInfo.CreateUid.NickName,
		roomInfo.CreateUid.HeadPhotoUrl,
		roomInfo.CreateUid.Country,
		roomInfo.NotifyUidList,
		roomInfo.MapedUidList,
		roomInfo.LessonName,
		roomInfo.LessonAbstract,
		roomInfo.LessonConverUrl,
		roomInfo.RoomTS,
		[]byte(roomInfo.Obid))
	if err != nil || retCode != 0 {
		attr := "gotr/req_bc_room_created_failed"
		libcomm.AttrAdd(attr, 1)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.ReqBroaCastRoomCreatedRspbody = &ht_teaching_room.ReqBroadCastRoomCreatedRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("broad cast failed"),
			},
		}
		return false
	}
	// Step6: send response
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.ReqBroaCastRoomCreatedRspbody = &ht_teaching_room.ReqBroadCastRoomCreatedRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		TimeStamp: proto.Uint64(roomInfo.RoomTS),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/req_bc_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func isUserAlreadyInMap(reqUid uint32, alreadyInMap map[uint32]*util.MemberSimpleInfo) (ret bool) {
	ret = false
	if reqUid == 0 || len(alreadyInMap) == 0 {
		return ret
	}
	if _, ok := alreadyInMap[reqUid]; ok {
		ret = true
	}
	return ret
}

func isUserAlreadyInSlic(reqUid uint32, alreadyInSlic []*util.MemberSimpleInfo) (ret bool) {
	ret = false
	if reqUid == 0 || len(alreadyInSlic) == 0 {
		return ret
	}
	for _, v := range alreadyInSlic {
		if v.Uid == reqUid {
			ret = true
		}
	}
	return ret
}

// 3.proc req join room
func ProcReqJoinRoom(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcReqJoinRoom no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcReqJoinRoom invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/req_join_room_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcReqJoinRoom proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetRequestJoinTeachingRoomReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcReqJoinRoom GetRequestJoinRoomReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	uidInfo := subReqBody.GetUidInfo()
	reqUid := uidInfo.GetHtUid()
	mapedUid := uidInfo.GetMapedUid()
	infoLog.Printf("ProcReqJoinRoom roomId=%v wbid=%v reqUid=%v mapedUid=%v",
		roomId,
		wBid,
		reqUid,
		mapedUid)

	// Step1: param check
	if roomId == 0 || wBid == 0 || reqUid == 0 || mapedUid == 0 {
		infoLog.Printf("ProcReqJoinRoom invalid param roomId=%v wBid=%v reqUid=%v mapedUid=%v",
			roomId,
			wBid,
			reqUid,
			mapedUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: Check whether user is already in
	roomInfo, err := teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		if err == util.ErrNotExist {
			infoLog.Printf("ProcReqJoinRoom roomId=%v not exist. reqUid=%v", roomId, reqUid)
			result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_NOT_EXIST
			rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
				Status: &ht_teaching_room.TeachingRoomHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("not exist"),
				},
			}
			return true
		} else {
			infoLog.Printf("ProcReqJoinRoom teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
				roomId,
				err)
			result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
			rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
				Status: &ht_teaching_room.TeachingRoomHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}

	alreadyInList := roomInfo.AlreadyInList
	ret := isUserAlreadyInMap(reqUid, alreadyInList)
	if ret { // 用户已经存在，直接返回成功
		infoLog.Printf("ProcReqJoinRoom roomId=%v reqUid=%v already in",
			roomId,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_USER_ALREADT_IN
		rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("already in"),
			},
		}
		return true
	}

	// Step3: update room start time
	// 当房间只存在创建者 即第一个用户申请加入
	if len(alreadyInList) == 1 {
		sessionTime := uint64(time.Now().UnixNano() / 1000000)
		err = teachingRoomManager.UpdateRoomSessionTime(roomId, sessionTime)
		if err != nil {
			infoLog.Printf("ProcReqJoinRoom UpdateRoomSessionTime failed roomId=%v err=%s",
				roomId,
				err)
			result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
			rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
				Status: &ht_teaching_room.TeachingRoomHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}
	// Step4: add req user to room
	err = teachingRoomManager.AddReqUserToAlreadyInList(roomId, reqUid, mapedUid)
	if err != nil {
		infoLog.Printf("ProcReqJoinRoom teachingRoomManager.AddReqUserToAlreadyInList failed roomId=%v reqUid=%v mapedUid=%v err=%s",
			roomId,
			reqUid,
			mapedUid,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	roomInfo, err = teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcReqJoinRoom teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	handsUpList := roomInfo.HandsUpList
	microphoneConnList := roomInfo.MicrophoneConnList

	alreadyInSlic := make([]uint32, len(alreadyInList))
	i := 0
	for _, v := range alreadyInList {
		infoLog.Printf("ProcReqJoinRoom index=%v uid=%v", i, v.Uid)
		alreadyInSlic[i] = v.Uid
		i += 1
	}

	handsUpSlic := make([]uint32, len(handsUpList))
	for index, v := range handsUpList {
		handsUpSlic[index] = v.Uid
	}

	connSlic := make([]uint32, len(microphoneConnList))
	microLen := len(microphoneConnList)
	for index := 0; index < microLen; index += 1 {
		connSlic[index] = microphoneConnList[microLen-index-1].Uid
	}

	// Step5: 返回响应
	rspBody.RequestJoinTeachingRoomRspbody = &ht_teaching_room.RequestJoinTeachingRoomRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
		HandsUpStat:        ht_teaching_room.TR_STAT_OP_TYPE(roomInfo.HandsUpStat).Enum(),
		WhiteBoardStat:     ht_teaching_room.TR_STAT_OP_TYPE(roomInfo.WhiteBoardStat).Enum(),
		AlreadyInList:      alreadyInSlic,
		HandsUpList:        handsUpSlic,
		MicrophoneConnList: connSlic,
		SessionTime:        proto.Uint64(roomInfo.SessionTime),
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step6: begin broad cast
	err = teachingRoomManager.BroadCastRoomStatWithRoomInfo(reqUid, &roomInfo)
	if err != nil {
		attr := "gotr/req_join_room_bc_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcReqJoinRoom mucApi.BroadCastRoomStat failed reqUid=%v roomId=%v err=%s",
			reqUid,
			roomId,
			err)
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/req_join_room_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 4.proc req leave room
func ProcReqLeaveRoom(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcReqLeaveRoom no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcReqLeaveRoom invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/req_leave_room_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcReqLeaveRoom proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetRequestLeaveTeachingRoomReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcReqLeaveRoom GetRequestLeaveRoomReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	uidInfo := subReqBody.GetUidInfo()
	reqUid := uidInfo.GetHtUid()
	mapedUid := uidInfo.GetMapedUid()
	infoLog.Printf("ProcReqLeaveRoom roomId=%v wbid=%v reqUid=%v mapedUid=%v",
		roomId,
		wBid,
		reqUid,
		mapedUid)

	// Step1: param check
	if roomId == 0 || wBid == 0 || reqUid == 0 || mapedUid == 0 {
		infoLog.Printf("ProcReqLeaveRoom invalid param roomId=%v wBid=%v reqUid=%v mapedUid=%v",
			roomId,
			wBid,
			reqUid,
			mapedUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: Check whether user is already in
	roomInfo, err := teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcReqLeaveRoom teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	alreadyInList := roomInfo.AlreadyInList
	ret := isUserAlreadyInMap(reqUid, alreadyInList)
	if !ret { // 用户不存在，直接返回成功
		infoLog.Printf("ProcReqLeaveRoom roomId=%v reqUid=%v already leave",
			roomId,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_USER_ALREADT_IN
		rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("already leave"),
			},
		}
		return true
	}

	// Step3: remove req user to room
	err = teachingRoomManager.RemoveReqUserFromAllList(roomId, reqUid, mapedUid)
	if err != nil {
		infoLog.Printf("ProcReqLeaveRoom teachingRoomManager.RemoveReqUserFromAlreadyInList failed roomId=%v reqUid=%v mapedUid=%v err=%s",
			roomId,
			reqUid,
			mapedUid,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	roomInfo, err = teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcReqLeaveRoom teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// Step4: 返回响应
	rspBody.RequestLeaveTeachingRoomRspbody = &ht_teaching_room.RequestLeaveTeachingRoomRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step5: begin broad cast
	err = teachingRoomManager.BroadCastRoomStatWithRoomInfo(reqUid, &roomInfo)
	if err != nil {
		attr := "gotr/req_leave_room_bc_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcReqLeaveRoom mucApi.BroadCastRoomStat failed reqUid=%v roomId=%v err=%s",
			reqUid,
			roomId,
			err)
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/req_leave_room_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 5.proc heart beat
func ProcTeachingRoomHeartBeat(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcTeachingRoomHeartBeat no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.TeachingRoomHeatBeatRspbody = &ht_teaching_room.TeachingRoomHeartBeatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcTeachingRoomHeartBeat invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/heart_beat_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcTeachingRoomHeartBeat proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.TeachingRoomHeatBeatRspbody = &ht_teaching_room.TeachingRoomHeartBeatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetTeachingRoomHeatBeatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcTeachingRoomHeartBeat GetTeachingRoomHeatBeatReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.TeachingRoomHeatBeatRspbody = &ht_teaching_room.TeachingRoomHeartBeatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	uidInfo := subReqBody.GetUidInfo()
	reqUid := uidInfo.GetHtUid()
	// Step1: param check
	if roomId == 0 || reqUid == 0 {
		infoLog.Printf("ProcTeachingRoomHeartBeat invalid param roomId=%v reqUid=%v",
			roomId,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.TeachingRoomHeatBeatRspbody = &ht_teaching_room.TeachingRoomHeartBeatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step4: 返回响应
	rspBody.TeachingRoomHeatBeatRspbody = &ht_teaching_room.TeachingRoomHeartBeatRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	ret := teachingRoomManager.HandleHeartBeat(roomId, reqUid)
	infoLog.Printf("ProcTeachingRoomHeartBeat roomId=%v reqUid=%v teachingRoomManager.HandleHeartBeat ret=%v",
		roomId,
		reqUid,
		ret)
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/heart_beat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 6.proc update room stat
func ProcUpdateRoomStat(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcUpdateRoomStat no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateRoomStat invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/update_room_stat_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateRoomStat proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateTeachingRoomStatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateRoomStat GetUpdateTeachingRoomStatRspbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	reqUid := subReqBody.GetReqUid()
	statType := subReqBody.GetStatType()
	opStat := subReqBody.GetOpStat()
	infoLog.Printf("ProcUpdateRoomStat roomId=%v wbid=%v reqUid=%v statType=%v opStat=%v",
		roomId,
		wBid,
		reqUid,
		statType,
		opStat)

	// Step1: param check
	if roomId == 0 || wBid == 0 || reqUid == 0 {
		infoLog.Printf("ProcUpdateRoomStat invalid param roomId=%v wBid=%v reqUid=%v statType=%v opStat=%v",
			roomId,
			wBid,
			reqUid,
			statType,
			opStat)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step2: check whether req uid is room create
	isCreater, err := teachingRoomManager.IsRoomCreater(roomId, reqUid)
	if err != nil {
		infoLog.Printf("ProcUpdateRoomStat teachingRoomManager.IsRoomCreater roomId=%v reqUid=%v err=%s", roomId, reqUid, err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	if !isCreater {
		infoLog.Printf("ProcUpdateRoomStat roomId=%v reqUid=%v isCreater=%v not equal",
			roomId,
			reqUid,
			isCreater)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("is not admin"),
			},
		}
		return false
	}

	// step3: update room stat
	err = teachingRoomManager.UpdateRoomStat(roomId, reqUid, statType, opStat)
	if err != nil {
		infoLog.Printf("ProcUpdateRoomStat teachingRoomManager.UpdateRoomStat roomId=%v wbid=%v reqUid=%v statType=%v opStat=%v faild err=%s",
			roomId,
			wBid,
			reqUid,
			statType,
			opStat,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// step4: broad cast new stat
	roomInfo, err := teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcUpdateRoomStat teachingRoomManager.GetRoomInfoByRoomId roomId=%v err=%s", roomId, err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5: 返回响应
	rspBody.UpdateTeachingRoomStatRspbody = &ht_teaching_room.UpdateTeachingRoomStatRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step5: begin broad cast
	err = teachingRoomManager.BroadCastRoomStatWithRoomInfo(reqUid, &roomInfo)
	if err != nil {
		attr := "gotr/update_room_stat_bc_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcReqLeaveRoom mucApi.BroadCastRoomStat failed reqUid=%v roomId=%v err=%s",
			reqUid,
			roomId,
			err)
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/update_room_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 7.proc hands up req
func ProcHandsUpReq(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcHandsUpReq no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcHandsUpReq invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/hands_up_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcHandsUpReq proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetHandsUpReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcHandsUpReq GetHandsUpRspbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	uidInfo := subReqBody.GetUidInfo()
	reqUid := uidInfo.GetHtUid()
	mapedUid := uidInfo.GetMapedUid()
	infoLog.Printf("ProcHandsUpReq roomId=%v wbid=%v reqUid=%v mapedUid=%v",
		roomId,
		wBid,
		reqUid,
		mapedUid)

	// Step1: param check
	if roomId == 0 || wBid == 0 || reqUid == 0 {
		infoLog.Printf("ProcHandsUpReq invalid param roomId=%v wBid=%v reqUid=%v",
			roomId,
			wBid,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step3: check whether hands up open
	isOpen, err := teachingRoomManager.IsHandsUpOpen(roomId)
	if err != nil {
		infoLog.Printf("ProcHandsUpReq teachingRoomManager.IsHandsUpOpen failed roomId=%v reqUid=%v err=%s",
			roomId,
			reqUid,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	if !isOpen {
		infoLog.Printf("ProcHandsUpReq handsStat not open roomId=%v reqUid=%v",
			roomId,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_HANDS_UP_NOT_OPNE
		rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("hands up not open"),
			},
		}
		return true
	}

	// Step2: add user to hands up list even if it already in list
	err = teachingRoomManager.AddReqUserToHandsUpList(roomId, reqUid, mapedUid)
	if err != nil {
		infoLog.Printf("ProcHandsUpReq teachingRoomManager.AddReqUserToHandsUpList failed roomId=%v reqUid=%v mapedUid=%v",
			roomId,
			reqUid,
			mapedUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// step3: exec broad cast
	roomInfo, err := teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcHandsUpReq teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// Step4: 返回响应
	rspBody.HandsUpRspbody = &ht_teaching_room.HandsUpRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step5: begin broad cast
	err = teachingRoomManager.BroadCastRoomStatWithRoomInfo(reqUid, &roomInfo)
	if err != nil {
		attr := "gotr/hands_up_bc_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcHandsUpReq mucApi.BroadCastRoomStat failed reqUid=%v roomId=%v err=%s",
			reqUid,
			roomId,
			err)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/hands_up_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 8.proc cancle hands up req
func ProcCancleHandsUpReq(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcCancleHandsUpReq no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcCancleHandsUpReq invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/cancle_hands_up_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcCancleHandsUpReq proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetCancleHandsUpReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcCancleHandsUpReq GetCancleHandsUpReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	uidInfo := subReqBody.GetUidInfo()
	reqUid := uidInfo.GetHtUid()
	mapedUid := uidInfo.GetMapedUid()
	infoLog.Printf("ProcCancleHandsUpReq roomId=%v wbid=%v reqUid=%v mapedUid=%v",
		roomId,
		wBid,
		reqUid,
		mapedUid)

	// Step1: param check
	if roomId == 0 || wBid == 0 || reqUid == 0 {
		infoLog.Printf("ProcCancleHandsUpReq invalid param roomId=%v wBid=%v reqUid=%v",
			roomId,
			wBid,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: check whether req uid is hands up list
	roomInfo, err := teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcCancleHandsUpReq teachingRoomManager.GetRoomInfoByRoomId roomId=%v err=%s", roomId, err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	handsUpList := roomInfo.HandsUpList
	microConnList := roomInfo.MicrophoneConnList
	retHandsUp := isUserAlreadyInSlic(reqUid, handsUpList)
	retConnList := isUserAlreadyInSlic(reqUid, microConnList)
	if !retHandsUp && !retConnList { // 用户不存在 直接返回成功
		infoLog.Printf("ProcCancleHandsUpReq roomId=%v reqUid=%v not in hands up list or micro conn list",
			roomId,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
		rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
		}
		return true
	}
	// step3: exec remove
	err = teachingRoomManager.RemoveReqUserFromHandsUpList(roomId, reqUid, mapedUid)
	if err != nil {
		infoLog.Printf("ProcCancleHandsUpReq teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// Step4: 返回响应
	rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step5: begin broad cast
	roomInfo, err = teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcCancleHandsUpReq teachingRoomManager.GetRoomInfoByRoomId roomId=%v err=%s", roomId, err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.CancleHandsUpRspbody = &ht_teaching_room.CancleHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	err = teachingRoomManager.BroadCastRoomStatWithRoomInfo(reqUid, &roomInfo)
	if err != nil {
		attr := "gotr/cancle_hands_up_bc_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcCancleHandsUpReq mucApi.BroadCastRoomStat failed reqUid=%v roomId=%v err=%s",
			reqUid,
			roomId,
			err)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/cancle_hands_up_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 9.proc handle student hands up req
func ProcHandleStudentHandsUp(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcHandleStudentHandsUp no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.HandleStudentHandsUpRspbody = &ht_teaching_room.HandleStudentHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcHandleStudentHandsUp invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/handle_hands_up_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcHandleStudentHandsUp proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.HandleStudentHandsUpRspbody = &ht_teaching_room.HandleStudentHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetHandleStudentHandsUpReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcHandleStudentHandsUp GetHandleStudentHandsUpReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.HandleStudentHandsUpRspbody = &ht_teaching_room.HandleStudentHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	handleType := subReqBody.GetHandleType()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	uidList := subReqBody.GetUidList()
	infoLog.Printf("ProcHandleStudentHandsUp handleType=%v roomId=%v opUid=%v uidListLen=%v",
		handleType,
		roomId,
		opUid,
		len(uidList))

	// Step1: param check
	if roomId == 0 || opUid == 0 || len(uidList) == 0 {
		infoLog.Printf("ProcHandleStudentHandsUp invalid param handleType=%v roomId=%v opUid=%v uidListLen=%v",
			handleType,
			roomId,
			opUid,
			len(uidList))
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.HandleStudentHandsUpRspbody = &ht_teaching_room.HandleStudentHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: check whether opUid is admin
	isCreater, err := teachingRoomManager.IsRoomCreater(roomId, opUid)
	if err != nil {
		infoLog.Printf("ProcHandleStudentHandsUp teachingRoomManager.IsRoomCreater roomId=%v opUid=%v err=%s", roomId, opUid, err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.HandleStudentHandsUpRspbody = &ht_teaching_room.HandleStudentHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	if !isCreater {
		infoLog.Printf("ProcHandleStudentHandsUp roomId=%v opUid=%v isCreater=%v not equal",
			roomId,
			opUid,
			isCreater)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.HandleStudentHandsUpRspbody = &ht_teaching_room.HandleStudentHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("is not creater"),
			},
		}
		return false
	}
	// Step3: handle student hand up
	err = teachingRoomManager.HandleStudenHandsUpStat(roomId, opUid, uidList, handleType)
	if err != nil {
		infoLog.Printf("ProcHandleStudentHandsUp teachingRoomManager.HandleStudenHandsUpStat failed roomId=%v reqUid=%v err=%s",
			roomId,
			opUid,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.HandleStudentHandsUpRspbody = &ht_teaching_room.HandleStudentHandsUpRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step4: send resp
	rspBody.HandleStudentHandsUpRspbody = &ht_teaching_room.HandleStudentHandsUpRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step5: broadcast notification
	roomInfo, err := teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcHandleStudentHandsUp teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		return false
	}
	err = teachingRoomManager.BroadCastRoomStatWithRoomInfo(opUid, &roomInfo)
	if err != nil {
		attr := "gotr/handle_stu_hands_up_bc_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcHandleStudentHandsUp mucApi.BroadCastRoomStat failed reqUid=%v roomId=%v err=%s",
			opUid,
			roomId,
			err)
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/handle_stu_hands_up_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 10.proc favorite and share count
func ProcUpdateFavAndShareCount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcUpdateFavAndShareCount no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdateFavAndShareCountRspbody = &ht_teaching_room.UpdateFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateFavAndShareCount invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/update_fav_and_share_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateFavAndShareCount proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.UpdateFavAndShareCountRspbody = &ht_teaching_room.UpdateFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateFavAndShareCountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateFavAndShareCount GetUpdateFavAndShareCountReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.UpdateFavAndShareCountRspbody = &ht_teaching_room.UpdateFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	roomId := subReqBody.GetRoomId()
	obid := subReqBody.GetObid()
	updateType := subReqBody.GetUpdateType()
	updateCount := subReqBody.GetUpdateCount()
	infoLog.Printf("ProcUpdateFavAndShareCount roomId=%v obid=%v upateType=%v updateCount=%v",
		roomId,
		obid,
		updateType,
		updateCount)

	// Step1: param check
	if roomId == 0 || len(obid) == 0 || updateCount == 0 {
		infoLog.Printf("ProcUpdateFavAndShareCount invalid param roomId=%v obid=%v upateType=%v updateCount=%v",
			roomId,
			obid,
			updateType,
			updateCount)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.UpdateFavAndShareCountRspbody = &ht_teaching_room.UpdateFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step2: check whether req uid is room create
	err = teachingRoomManager.UpdateFavAndShareCount(roomId, obid, updateType, updateCount)
	if err != nil {
		infoLog.Printf("ProcUpdateFavAndShareCount teachingRoomManager.UpdateFavAndShareCount roomId=%v obid=%v updateType=%v updateCount=%v err=%s", roomId, obid, updateType, updateCount, err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdateFavAndShareCountRspbody = &ht_teaching_room.UpdateFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// step3: send response
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.UpdateFavAndShareCountRspbody = &ht_teaching_room.UpdateFavAndShareCountRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/up_fav_and_share_count_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 11.proc get favorite and share count
func ProcGetFavAndSharecount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetFavAndSharecount no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.GetFavAndShareCountRspbody = &ht_teaching_room.GetFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetFavAndSharecount invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/get_fav_and_share_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetFavAndSharecount proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.GetFavAndShareCountRspbody = &ht_teaching_room.GetFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetFavAndShareCountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetFavAndSharecount GetGetFavAndShareCountReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.GetFavAndShareCountRspbody = &ht_teaching_room.GetFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	obid := subReqBody.GetObid()
	infoLog.Printf("ProcGetFavAndSharecount roomId=%v reqUid=%v obid=%s",
		roomId,
		reqUid,
		obid)

	// Step1: param check
	if roomId == 0 || len(obid) == 0 {
		infoLog.Printf("ProcGetFavAndSharecount invalid param roomId=%v obid=%v reqUid=%v",
			roomId,
			obid,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.GetFavAndShareCountRspbody = &ht_teaching_room.GetFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step2: check whether req uid is room create
	favCount, shareCount, err := teachingRoomManager.GetFavAndShareCount(roomId, obid)
	if err != nil {
		infoLog.Printf("ProcGetFavAndSharecount teachingRoomManager.GetFavAndShareCount roomId=%v obid=%v err=%s", roomId, obid, err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.GetFavAndShareCountRspbody = &ht_teaching_room.GetFavAndShareCountRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// step3: send response
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.GetFavAndShareCountRspbody = &ht_teaching_room.GetFavAndShareCountRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		FavCount:   proto.Uint32(favCount),
		ShareCount: proto.Uint32(shareCount),
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/get_fav_and_share_count_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetBeginHexString() string {
	timeBegin := time.Date(2017, time.Month(8), 15, 0, 0, 0, 0, time.Local)
	objId := bson.NewObjectIdWithTime(timeBegin)
	return objId.Hex()
}

// 12.proc get lessoin list by obid req
func ProcGetLessonByObid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetLessonByObid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.GetLessonByObidRspbody = &ht_teaching_room.GetLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetLessonByObid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/get_lesson_by_obid_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetLessonByObid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.GetLessonByObidRspbody = &ht_teaching_room.GetLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetLessonByObidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetLessonByObid GetGetLessonByObidReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.GetLessonByObidRspbody = &ht_teaching_room.GetLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetUid()
	cliObid := subReqBody.GetCliObid()
	infoLog.Printf("ProcGetLessonByObid roomId=%v reqUid=%v cliObid=%s",
		roomId,
		reqUid.GetHtUid(),
		cliObid)

	// Step1: param check
	if roomId == 0 || reqUid.GetHtUid() == 0 || len(cliObid) == 0 {
		infoLog.Printf("ProcGetLessonByObid invalid param roomId=%v reqUid=%v cliObid=%s",
			roomId,
			reqUid.GetHtUid(),
			cliObid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.GetLessonByObidRspbody = &ht_teaching_room.GetLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	if string(cliObid) == "0" {
		cliObid = []byte(GetBeginHexString())
		infoLog.Printf("ProcGetLessonByObid roomId=%v cliObid=%v", roomId, cliObid)
	}
	// Step2: check whether req uid is room create
	lessonList, maxObid, err := teachingRoomManager.GetLessonByObid(roomId, cliObid)
	if err != nil {
		infoLog.Printf("ProcGetLessonByObid teachingRoomManager.GetLessonByObid roomId=%v reqUid=%v cliObid=%s err=%s",
			roomId,
			reqUid.GetHtUid(),
			cliObid,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.GetLessonByObidRspbody = &ht_teaching_room.GetLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcGetLessonByObid lessonList=%#v maxObid=%s", lessonList, maxObid)
	if len(lessonList) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
		rspBody.GetLessonByObidRspbody = &ht_teaching_room.GetLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			MaxObid:    cliObid,
			LessonList: nil,
		}
		return true
	}
	// Step3: send response
	var lessons []*ht_teaching_room.Lesson
	for _, v := range lessonList {
		var whiteUrl []*ht_teaching_room.WhiteBoardUrl
		storeUrlSlic := v.WhiteBoard.UrlSlic
		for _, v := range storeUrlSlic {
			url := &ht_teaching_room.WhiteBoardUrl{
				Wbid: proto.Uint32(v.WBid),
				Url:  []byte(v.Url),
			}
			whiteUrl = append(whiteUrl, url)
		}

		item := &ht_teaching_room.Lesson{
			Obid:           []byte(v.Id.Hex()),
			LessonName:     []byte(v.LessonName),
			LessonAbstract: []byte(v.LessonAbstract),
			LessonCoverUrl: []byte(v.LessonConverUrl),
			Record: &ht_teaching_room.Record{
				TimeCreate: proto.Uint64(v.Record.TimeCreate),
				Type:       []byte(v.Record.EventType),
				RoomName:   []byte(v.Record.RoomName),
				Url:        []byte(v.Record.RecordUrl),
			},
			WhiteBoard: &ht_teaching_room.WhiteBoard{
				TimeCreate: proto.Uint64(v.WhiteBoard.TimeCreate),
				Type:       []byte(v.WhiteBoard.EventType),
				RoomName:   []byte(v.WhiteBoard.RoomName),
				UrlList:    whiteUrl,
			},
			HandsUp: &ht_teaching_room.HandsUp{
				TimeCreate:  proto.Uint64(v.HandsUp.TimeCreate),
				Type:        []byte(v.HandsUp.EventType),
				RoomName:    []byte(v.HandsUp.RoomName),
				TotalSecond: proto.Uint32(v.HandsUp.TotalSecond),
				TotalMinute: proto.Uint32(v.HandsUp.TotalMinute),
				TimeStart:   []byte(v.HandsUp.TimeStart),
				TimeEnd:     []byte(v.HandsUp.TimeEnd),
			},
			CreateUid: proto.Uint32(v.CreateUid),
			RoomId:    proto.Uint32(v.RoomId),
			BeginTs:   proto.Uint64(v.BeginTs),
			EndTs:     proto.Uint64(v.EndTs),
		}
		lessons = append(lessons, item)
	}
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.GetLessonByObidRspbody = &ht_teaching_room.GetLessonByObidRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MaxObid:    maxObid,
		LessonList: lessons,
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/get_lesson_by_obid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true

}

// 13.proc del lesson by obid request
func ProcDelLessonByObid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcDelLessonByObid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.DelLessonByObidRspbody = &ht_teaching_room.DelLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelLessonByObid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/del_lesson_by_obid_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelLessonByObid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.DelLessonByObidRspbody = &ht_teaching_room.DelLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelLessonByObidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelLessonByObid GetDelLessonByObidReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.DelLessonByObidRspbody = &ht_teaching_room.DelLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetCreaterUid()
	obidList := subReqBody.GetObidList()
	infoLog.Printf("ProcDelLessonByObid roomId=%v reqUid=%v obidListLen=%v",
		roomId,
		reqUid,
		len(obidList))

	// Step1: param check
	if roomId == 0 || reqUid == 0 || len(obidList) == 0 {
		infoLog.Printf("ProcDelLessonByObid invalid param roomId=%v reqUid=%v obidListLen=%v",
			roomId,
			reqUid,
			len(obidList))
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.DelLessonByObidRspbody = &ht_teaching_room.DelLessonByObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step2: check whether req uid is room create
	for i, v := range obidList {
		infoLog.Printf("ProcDelLessonByObid index=%v obid=%s", i, v)
		err := teachingRoomManager.DelLessonByObid(roomId, v)
		if err != nil {
			attr := "gotr/del_lesson_by_obid_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcDelLessonByObid roomId=%v obid=%s err=%s", roomId, v, err)
			continue
		}
	}
	// step3: send response
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.DelLessonByObidRspbody = &ht_teaching_room.DelLessonByObidRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/del_lesson_by_obid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 14.proc teaching room end
func ProcTeachingRoomEnd(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcTeachingRoomEnd no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcTeachingRoomEnd invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/teaching_room_end_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcTeachingRoomEnd proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetTeachingRoomEndReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcTeachingRoomEnd GetTeachingRoomEndReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	reqUid := subReqBody.GetCreateUid()
	infoLog.Printf("ProcTeachingRoomEnd roomId=%v wbid=%v reqUid=%v",
		roomId,
		wBid,
		reqUid)

	// Step1: param check
	if roomId == 0 || wBid == 0 || reqUid == 0 {
		infoLog.Printf("ProcTeachingRoomEnd invalid param roomId=%v wBid=%v reqUid=%v",
			roomId,
			wBid,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step2: check whether room is teaching
	ret, err := teachingRoomManager.IsRoomTeaching(roomId)
	if err != nil {
		infoLog.Printf("ProcTeachingRoomEnd teachingRoomManager.IsRoomTeaching  roomId=%v wBid=%v reqUid=%v err=%s",
			roomId,
			wBid,
			reqUid,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	if !ret {
		infoLog.Printf("ProcTeachingRoomEnd roomId=%v wBid=%v reqUid=%v is not teaching",
			roomId,
			wBid,
			reqUid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("not teaching"),
			},
		}
		return true
	}

	// Step3: check whether req uid is room create
	roomInfo, err := teachingRoomManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcTeachingRoomEnd teachingRoomManager.GetRoomInfoByRoomId roomId=%v err=%s", roomId, err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	if reqUid != roomInfo.CreateUid.Uid {
		infoLog.Printf("ProcTeachingRoomEnd roomId=%v reqUid=%v createUid=%v not equal",
			roomId,
			reqUid,
			roomInfo.CreateUid.Uid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// step3: exec braod cast
	code, err := mucApi.BroadCastTeachingRoomEnd(roomId, wBid, reqUid, roomInfo.NotifyUidList, uint64(time.Now().UnixNano()/1000000))
	if err != nil || code != 0 {
		infoLog.Printf("ProcTeachingRoomEnd roomId=%v reqUid=%v mucApi.BroadCastTeachingRoomEnd failed code=%v err=%s",
			roomId,
			reqUid,
			code,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// Step4: update room end time
	endTime := uint64(time.Now().UnixNano() / 1000000) // 毫秒 ms
	err = teachingRoomManager.UpdateRoomEndTime(roomId, endTime)
	if err != nil {
		infoLog.Printf("ProcTeachingRoomEnd UpdateRoomEndTime failed roomId=%v err=%s",
			roomId,
			err)
		// update failed no need return
		// result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		// rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
		// 	Status: &ht_teaching_room.TeachingRoomHeader{
		// 		Code:   proto.Uint32(uint32(result)),
		// 		Reason: []byte("internal error"),
		// 	},
		// }
		// return false
	}

	// step4: del room info
	err = teachingRoomManager.DelRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcTeachingRoomEnd roomId=%v reqUid=%v err=%s", roomId, reqUid, err)
	}
	// step5: send response
	rspBody.TeachingRoomEndRspbody = &ht_teaching_room.TeachingRoomEndRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/store_muc_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 15.proc white board call back
func ProcWhiteBoardCallBack(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// 检查输入参数是否为空
	result := uint16(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendWhiteBoardCallRsp(c, head, result)
		} else {
			infoLog.Printf("ProcWhiteBoardCallBack no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM)
		infoLog.Printf("ProcWhiteBoardCallBack invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/wb_call_back_count"
	libcomm.AttrAdd(attr, 1)
	//reqJson, err := simplejson.NewJson(payLoad)
	infoLog.Printf("ProcWhiteBoardCallBack payLoad=%s", payLoad)
	reqJson := &CallbackData{}
	infoLog.Printf("payLoad=%s", payLoad)
	err := json.Unmarshal(payLoad, reqJson) // JSON to Struct

	if err != nil {
		infoLog.Printf("ProcWhiteBoardCallBack simplejson new packet error=%s", err)
		result = uint16(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR)
		return false
	}
	objectName := reqJson.Data.ObjectName
	obid := reqJson.Data.BusinessData
	if objectName == RecordName {
		record := &util.RecordStore{
			TimeCreate: reqJson.TimeCreate,
			EventType:  reqJson.Type,
			RoomName:   reqJson.Data.RoomName,
			RecordUrl:  reqJson.Data.Url,
		}
		err = teachingRoomManager.UpdateRecord(obid, record)
		if err != nil {
			infoLog.Printf("ProcWhiteBoardCallBack teachingRoomManager.UpdateRecord obid=%s failed err=%s", obid, err)
			result = uint16(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR)
			return false
		}
	} else if objectName == WhiteBoardName {
		// urls := reqJson.Get("data").Get("urls").MustArray(nil)
		var whiteBoardUrls []util.WhiteBoardUrlStore
		for _, v := range reqJson.Data.Urls {
			WbId, _ := strconv.Atoi(v.Userid)
			item := util.WhiteBoardUrlStore{
				WBid: uint32(WbId),
				Url:  v.Url,
			}
			whiteBoardUrls = append(whiteBoardUrls, item)

		}
		whiteBoard := &util.WhiteBoardStore{
			TimeCreate: reqJson.TimeCreate,
			EventType:  reqJson.Type,
			RoomName:   reqJson.Data.RoomName,
			UrlSlic:    whiteBoardUrls,
		}
		err = teachingRoomManager.UpdateWhiteBoardUrl(obid, whiteBoard)
		if err != nil {
			infoLog.Printf("ProcWhiteBoardCallBack teachingRoomManager.UpdateWhiteBoardUrl obid=%s failed err=%s", obid, err)
			result = uint16(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR)
			return false
		}
	} else if objectName == HangUpName {
		hang := &util.HandsUpStore{
			TimeCreate:  reqJson.TimeCreate,
			EventType:   reqJson.Type,
			RoomName:    reqJson.Data.RoomName,
			TotalSecond: reqJson.Data.Totalsecond,
			TotalMinute: reqJson.Data.Totalminute,
			TimeStart:   reqJson.Data.TimeStart,
			TimeEnd:     reqJson.Data.TimeEnd,
		}
		err = teachingRoomManager.UpdateHangUp(obid, hang)
		if err != nil {
			infoLog.Printf("ProcWhiteBoardCallBack teachingRoomManager.UpdateHangUp obid=%s failed err=%s", obid, err)
			result = uint16(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR)
			return false
		}
	} else {
		infoLog.Printf("ProcWhiteBoardCallBack unknow objectName=%s error=%s", objectName, err)
		result = uint16(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)
		return false
	}
	infoLog.Printf("ProcWhiteBoardCallBack obid=%s success", obid)
	result = uint16(ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS)
	return true
}

// 16.proc update lessoin cover url req
func ProcUpdateLessonCoverUrl(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcUpdateLessonCoverUrl no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdateLessonCoverUrlRspbody = &ht_teaching_room.UpdateLessonCoverUrlRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateLessonCoverUrl invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/update_lesson_url_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateLessonCoverUrl proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.UpdateLessonCoverUrlRspbody = &ht_teaching_room.UpdateLessonCoverUrlRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateLessonCoverUrlReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateLessonCoverUrl GetUpdateLessonCoverUrlReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.UpdateLessonCoverUrlRspbody = &ht_teaching_room.UpdateLessonCoverUrlRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetSelfUid()
	cliObid := subReqBody.GetObid()
	url := subReqBody.GetLessonCoverUrl()
	infoLog.Printf("ProcUpdateLessonCoverUrl roomId=%v uid=%v obid=%s url=%s",
		roomId,
		reqUid,
		cliObid,
		url)

	// Step1: param check
	if roomId == 0 || reqUid == 0 || len(cliObid) == 0 || len(url) == 0 {
		infoLog.Printf("ProcUpdateLessonCoverUrl invalid param roomId=%v uid=%v obid=%s url=%s",
			roomId,
			reqUid,
			cliObid,
			url)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.UpdateLessonCoverUrlRspbody = &ht_teaching_room.UpdateLessonCoverUrlRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: check whether req uid is room create
	err = teachingRoomManager.UpdateLessonCoverUrl(roomId, reqUid, string(cliObid), string(url))
	if err != nil {
		infoLog.Printf("ProcUpdateLessonCoverUrl teachingRoomManager.UpdateLessonCoverUrl roomId=%v reqUid=%v cliObid=%s err=%s",
			roomId,
			reqUid,
			cliObid,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdateLessonCoverUrlRspbody = &ht_teaching_room.UpdateLessonCoverUrlRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step3: send response
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.UpdateLessonCoverUrlRspbody = &ht_teaching_room.UpdateLessonCoverUrlRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/update_lesson_url_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 17.proc get lessoin by one obid
func ProcGetLessonByOneObid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetLessonByOneObid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.GetLessonByOneObidRspbody = &ht_teaching_room.GetLessonByOneObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetLessonByOneObid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/get_lesson_by_one_obid_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetLessonByOneObid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.GetLessonByOneObidRspbody = &ht_teaching_room.GetLessonByOneObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetLessonByOneObidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetLessonByOneObid GetGetLessonByOneObidReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.GetLessonByOneObidRspbody = &ht_teaching_room.GetLessonByOneObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	cliObid := subReqBody.GetObid()
	infoLog.Printf("ProcGetLessonByOneObid reqUid=%v cliObid=%s",
		reqUid,
		cliObid)

	// Step1: param check
	if reqUid == 0 || len(cliObid) == 0 {
		infoLog.Printf("ProcGetLessonByOneObid invalid param reqUid=%v cliObid=%s",
			reqUid,
			cliObid)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.GetLessonByOneObidRspbody = &ht_teaching_room.GetLessonByOneObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: 根据obid获取课程
	lesson, err := teachingRoomManager.GetLessonByOneObid(cliObid)
	if err != nil {
		infoLog.Printf("ProcGetLessonByOneObid teachingRoomManager.GetLessonByOneObid reqUid=%v cliObid=%s err=%s",
			reqUid,
			cliObid,
			err)
		if err == mgo.ErrNotFound {
			result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_LESSION_NOT_FOUND
		} else {
			result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		}
		rspBody.GetLessonByOneObidRspbody = &ht_teaching_room.GetLessonByOneObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcGetLessonByOneObid lesson=%#v ", lesson)
	// Step3: send response
	var whiteUrl []*ht_teaching_room.WhiteBoardUrl
	storeUrlSlic := lesson.WhiteBoard.UrlSlic
	for _, v := range storeUrlSlic {
		url := &ht_teaching_room.WhiteBoardUrl{
			Wbid: proto.Uint32(v.WBid),
			Url:  []byte(v.Url),
		}
		whiteUrl = append(whiteUrl, url)
	}

	item := &ht_teaching_room.Lesson{
		Obid:           []byte(lesson.Id.Hex()),
		LessonName:     []byte(lesson.LessonName),
		LessonAbstract: []byte(lesson.LessonAbstract),
		LessonCoverUrl: []byte(lesson.LessonConverUrl),
		Record: &ht_teaching_room.Record{
			TimeCreate: proto.Uint64(lesson.Record.TimeCreate),
			Type:       []byte(lesson.Record.EventType),
			RoomName:   []byte(lesson.Record.RoomName),
			Url:        []byte(lesson.Record.RecordUrl),
		},
		WhiteBoard: &ht_teaching_room.WhiteBoard{
			TimeCreate: proto.Uint64(lesson.WhiteBoard.TimeCreate),
			Type:       []byte(lesson.WhiteBoard.EventType),
			RoomName:   []byte(lesson.WhiteBoard.RoomName),
			UrlList:    whiteUrl,
		},
		HandsUp: &ht_teaching_room.HandsUp{
			TimeCreate:  proto.Uint64(lesson.HandsUp.TimeCreate),
			Type:        []byte(lesson.HandsUp.EventType),
			RoomName:    []byte(lesson.HandsUp.RoomName),
			TotalSecond: proto.Uint32(lesson.HandsUp.TotalSecond),
			TotalMinute: proto.Uint32(lesson.HandsUp.TotalMinute),
			TimeStart:   []byte(lesson.HandsUp.TimeStart),
			TimeEnd:     []byte(lesson.HandsUp.TimeEnd),
		},
		CreateUid: proto.Uint32(lesson.CreateUid),
		RoomId:    proto.Uint32(lesson.RoomId),
		BeginTs:   proto.Uint64(lesson.BeginTs),
		EndTs:     proto.Uint64(lesson.EndTs),
	}

	// Step3: 查询muc用户是否在群里面
	isInRoom, err := mucApi.IsUserAlreadyInRoom(lesson.RoomId, reqUid)
	if err != nil {
		infoLog.Printf("ProcGetLessonByOneObid mucApi.IsUserAlreadyInRoom roomId=%v reqUid=%v err=%s",
			lesson.RoomId,
			reqUid,
			err)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INTERNAL_ERR
		rspBody.GetLessonByOneObidRspbody = &ht_teaching_room.GetLessonByOneObidRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query user is in room failed"),
			},
		}
		return false
	}
	var retValue uint32
	if isInRoom {
		retValue = 1
	} else {
		retValue = 0
	}
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.GetLessonByOneObidRspbody = &ht_teaching_room.GetLessonByOneObidRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		IsUserInGroup: proto.Uint32(retValue),
		Lesson:        item,
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/get_lesson_by_one_obid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true

}

func SendWhiteBoardCallRsp(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	head.Len = uint32(common.PacketV3HeadLen + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err := common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendCreatRoomResp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	buf[head.Len-1] = common.HTV3MagicEnd
	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 18.proc sync server ts
func ProcSyncSrvTs(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody := new(ht_teaching_room.TeachingRoomRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcSyncSrvTs no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_INVALID_PARAM
		rspBody.SyncServerTsRspbody = &ht_teaching_room.SyncServerTsRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSyncSrvTs invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gotr/sync_srv_ts_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSyncSrvTs proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.SyncServerTsRspbody = &ht_teaching_room.SyncServerTsRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSyncServerTsReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSyncSrvTs GetSyncServerTsReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_PB_ERR
		rspBody.SyncServerTsRspbody = &ht_teaching_room.SyncServerTsRspBody{
			Status: &ht_teaching_room.TeachingRoomHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	infoLog.Printf("ProcSyncSrvTs reqUid=%v", reqUid)
	result = ht_teaching_room.TEACHING_ROOM_RET_CODE_RET_SUCCESS
	rspBody.SyncServerTsRspbody = &ht_teaching_room.SyncServerTsRspBody{
		Status: &ht_teaching_room.TeachingRoomHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		SrvTs: proto.Int64(time.Now().UnixNano() / 1000000),
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gotr/sync_srv_ts_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mongo
	// 创建mongodb对象
	mongo_url := cfg.Section("MONGO").Key("url").MustString("localhost")
	infoLog.Printf("Mongo url=%s", mongo_url)
	mongoSess, err = mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
		return
	}
	defer mongoSess.Close()
	//Optional. Switch the session to a monotonic behavior.
	mongoSess.SetMode(mgo.Monotonic, true)

	// 读取mucApi 配置
	mucIp := cfg.Section("MUC").Key("ip").MustString("127.0.0.1")
	mucPort := cfg.Section("MUC").Key("port").MustString("0")
	mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	infoLog.Printf("muc server ip=%v port=%v connLimit=%v", mucIp, mucPort, mucConnLimit)
	mucApi = common.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, mucConnLimit)

	// 读取mucApi 配置
	voipIp := cfg.Section("VOIP").Key("ip").MustString("127.0.0.1")
	voipPort := cfg.Section("VOIP").Key("port").MustString("0")
	voipConnLimit := cfg.Section("VOIP").Key("pool_limit").MustInt(1000)
	infoLog.Printf("voip server ip=%v port=%v connLimit=%v", voipIp, voipPort, voipConnLimit)
	voipApi = common.NewVoipApi(voipIp, voipPort, 3*time.Second, 3*time.Second, &common.XTHeadProtocol{}, voipConnLimit)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	//创建RoomManager 和 DbUtil 对象
	DbUtil = util.NewDbUtil(db, mongoSess, infoLog)
	teachingRoomManager = util.NewTeachingRoomManager(DbUtil, mucApi, infoLog)
	teachingRoomManager.AsyncDo() // 启动自动检测
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
