// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"log"
	"sync"
	"time"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_teachingroom"
	"gopkg.in/mgo.v2/bson"
)

var (
	ErrNilParam       = errors.New("err nil param obj")
	ErrInputParam     = errors.New("err input param err")
	ErrAlreadyExist   = errors.New("err already exist")
	ErrNotExist       = errors.New("err not exist")
	ErrOnlineIpErr    = errors.New("err online state ip not exist")
	ErrSendToMucFaild = errors.New("err send packet to muc failed")
)

const (
	USE_TIMEOUT_THRESHOLD = 30 // 白板用户超时时间 30秒
)

// 每个群成员详细信息
type MemberSimpleInfo struct {
	Uid          uint32 // 用户uid
	WhiteBoardId uint32 // 白板映射id
	ActiveTS     int64  // 上次收到心跳的时刻 单位是秒
}

type MemberDetailInfo struct {
	Uid          uint32
	NickName     []byte
	HeadPhotoUrl []byte
	Country      []byte
}

type RoomInfo struct {
	RoomId             uint32                       // 群聊Id
	WhiteBoardId       uint32                       // 白板功能Id
	CreateUid          MemberDetailInfo             // 创建者uid
	NotifyUidList      []uint32                     // 待通知的用户
	MapedUidList       []uint32                     // 白板映射之后的uid列表
	AlreadyInList      map[uint32]*MemberSimpleInfo // 已经加入白板用户列表 将老师包含在内
	HandsUpList        []*MemberSimpleInfo          // 举手用户列表
	MicrophoneConnList []*MemberSimpleInfo          // 麦克风接通用户列表
	LessonName         []byte                       // 课程名称
	LessonAbstract     []byte                       // 课程简介
	LessonConverUrl    []byte                       // 课程第一页封面
	HandsUpStat        uint32                       // 举手状态 是否开启举手功能 1:开启 0:关闭
	WhiteBoardStat     uint32                       // 白板状态 是否开启白板功能 1:开启 0:关闭
	SessionTime        uint64                       // 教学房间开始时间计时
	RoomTS             uint64                       // 教学房间创建时间戳
	Obid               string                       // 课程在mongodb中的唯一标识
}

type TeachingRoomManager struct {
	mucApi           *common.MucApi
	dbManager        *DbUtil
	infoLog          *log.Logger
	sendSeq          uint16
	roomInfoLock     sync.RWMutex // sync mutex goroutines use this.roomIdToRoomInfo
	roomIdToRoomInfo map[uint32]*RoomInfo
}

func NewTeachingRoomManager(dbUtil *DbUtil, muc *common.MucApi, logger *log.Logger) *TeachingRoomManager {
	return &TeachingRoomManager{
		mucApi:           muc,
		dbManager:        dbUtil,
		infoLog:          logger,
		roomIdToRoomInfo: map[uint32]*RoomInfo{},
		sendSeq:          0,
	}
}

func (this *TeachingRoomManager) GetPacketSeq() (packetSeq uint16) {
	this.sendSeq++
	packetSeq = this.sendSeq
	return packetSeq
}

// 判断当前房间是否正在进行教学
func (this *TeachingRoomManager) IsRoomTeaching(roomId uint32) (ok bool, err error) {
	if roomId == 0 {
		err = ErrInputParam
		this.infoLog.Printf("IsRoomTeaching invalid param roomId=%v", roomId)
		ok = false
		return ok, err
	}
	this.roomInfoLock.RLock()
	defer this.roomInfoLock.RUnlock()
	_, ok = this.roomIdToRoomInfo[roomId]
	this.infoLog.Printf("IsRoomTeaching roomId=%v isTeaching=%v", roomId, ok)
	return ok, nil
}

func (this *TeachingRoomManager) IsRoomCreater(roomId, reqUid uint32) (isCreater bool, err error) {
	if roomId == 0 || reqUid == 0 {
		this.infoLog.Printf("IsRoomCreater roomId=%v reqUid=%v param error", roomId, reqUid)
		err = ErrInputParam
		return isCreater, err
	}
	this.roomInfoLock.RLock()
	defer this.roomInfoLock.RUnlock()
	if v, ok := this.roomIdToRoomInfo[roomId]; ok {
		if reqUid == v.CreateUid.Uid {
			isCreater = true
		} else {
			isCreater = false
		}
	} else {
		isCreater = false
	}
	return isCreater, nil
}

func (this *TeachingRoomManager) IsHandsUpOpen(roomId uint32) (isOpen bool, err error) {
	if roomId == 0 {
		this.infoLog.Printf("IsHandsUpOpen roomId=%v param error", roomId)
		err = ErrInputParam
		return isOpen, err
	}
	this.roomInfoLock.RLock()
	defer this.roomInfoLock.RUnlock()
	if v, ok := this.roomIdToRoomInfo[roomId]; ok {
		if v.HandsUpStat == 1 {
			isOpen = true
		} else {
			isOpen = false
		}
	} else {
		err = ErrNotExist
		return isOpen, err
	}
	return isOpen, nil
}

func (this *TeachingRoomManager) AddTeachingRoomInfo(roomId, whiteBoardId uint32,
	createrUid *ht_teaching_room.TeachingRoomMemberInfo,
	lessonName, lessonAbstract, lessonConverUrl []byte,
	notifyUidList, mapedUidList []uint32) (obid string, err error) {
	if createrUid == nil || createrUid.GetUid() == 0 || roomId == 0 || whiteBoardId == 0 || len(lessonName) == 0 || len(notifyUidList) == 0 || len(mapedUidList) == 0 {
		this.infoLog.Printf("AddTeachingRoomInfo invalid param createrUid=%#v roomId=%v whiteBoardId=%v lessonName=%s lessonAbstract=%s lessonConverUrl=%s notifyUidLen=%v mapedUidLen=%v",
			createrUid,
			roomId,
			whiteBoardId,
			lessonName,
			lessonAbstract,
			lessonConverUrl,
			len(notifyUidList),
			len(mapedUidList))
		err = ErrInputParam
		return obid, err
	}

	// Step1: 首先查看内存中是否已经存在该教学房间相关信息
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	if _, ok := this.roomIdToRoomInfo[roomId]; ok {
		this.infoLog.Printf("AddTeachingRoomInfo room already exist createUid=%v roomId=%v whiteBoardId=%v lessonName=%s lessonAbstract=%s lessonConverUrl=%s",
			createrUid.GetUid(),
			roomId,
			whiteBoardId,
			lessonName,
			lessonAbstract,
			lessonConverUrl)
		err = ErrAlreadyExist
		return obid, err
	}
	// Step2 : 将课程相关信息存储到mongo中
	obid, err = this.dbManager.StoreLessonInfo(roomId, whiteBoardId, createrUid.GetUid(), lessonName, lessonAbstract, lessonConverUrl)
	if err != nil {
		this.infoLog.Printf("AddTeachingRoomInfo dbManager.StoreLessonInfo faild createUid=%v roomId=%v whiteBoardId=%v lessonName=%s lessonAbstract=%s lessonConverUrl=%s err=%s",
			createrUid.GetUid(),
			roomId,
			whiteBoardId,
			lessonName,
			lessonAbstract,
			lessonConverUrl,
			err)
		return obid, err
	}
	// Step3 : 存储成功之后更新内存中的map
	roomInfo := &RoomInfo{
		RoomId:       roomId,
		WhiteBoardId: whiteBoardId,
		CreateUid: MemberDetailInfo{
			Uid:          createrUid.GetUid(),
			NickName:     createrUid.GetNickName(),
			HeadPhotoUrl: createrUid.GetHeadPhotoUrl(),
			Country:      createrUid.GetCountry(),
		},
		NotifyUidList: notifyUidList,
		MapedUidList:  mapedUidList,
		AlreadyInList: map[uint32]*MemberSimpleInfo{
			createrUid.GetUid(): &MemberSimpleInfo{
				Uid:          createrUid.GetUid(),
				WhiteBoardId: uint32(WightBoardCreateUid),
				ActiveTS:     time.Now().Unix(),
			},
		},
		HandsUpList:        []*MemberSimpleInfo{},
		MicrophoneConnList: []*MemberSimpleInfo{},
		LessonName:         lessonName,
		LessonAbstract:     lessonAbstract,
		LessonConverUrl:    lessonConverUrl,
		HandsUpStat:        0,                                       // 0: 关闭  1：开启 默认关闭
		WhiteBoardStat:     0,                                       // 0: 关闭  1：开启 默认关闭
		SessionTime:        0,                                       // 第一个人加入之后开始计时
		RoomTS:             uint64(time.Now().UnixNano() / 1000000), // 创建时间 毫秒
		Obid:               obid,
	}
	this.roomIdToRoomInfo[roomId] = roomInfo
	this.infoLog.Printf("AddTeachingRoomInfo new roomInfo createrUid=%v roomId=%v whiteBoardId=%v lessonName=%s lessonAbstract=%s lessonConverUrl=%s",
		createrUid.GetUid(),
		roomId,
		whiteBoardId,
		lessonName,
		lessonAbstract,
		lessonConverUrl)

	return obid, err
}

func (this *TeachingRoomManager) GenerateWightBoardUidMap(createUid, roomId, wbRoomId uint32, notifyUidList []uint32) (outUidList, mapedUidList []uint32, err error) {
	if createUid == 0 || roomId == 0 || wbRoomId == 0 || len(notifyUidList) == 0 {
		err = ErrInputParam
		return nil, nil, err
	}
	outUidList, mapedUidList, err = this.dbManager.GenerateWightBoardUidMap(createUid, roomId, wbRoomId, notifyUidList)
	if err != nil {
		this.infoLog.Printf("GenerateWightBoardUidMap dbManager.GenerateWightBoardUidMap createUid=%v roomId=%v wbRoomId=%v notifyUidListLen=%v err=%s",
			createUid,
			roomId,
			wbRoomId,
			len(notifyUidList),
			err)
		return nil, nil, err
	}
	return outUidList, mapedUidList, err
}

func (this *TeachingRoomManager) GetRoomInfoByRoomId(roomId uint32) (roomInfo RoomInfo, err error) {
	if roomId == 0 {
		this.infoLog.Printf("GetRoomInfoByRoomId roomId=%v input err", roomId)
		err = ErrInputParam
		return roomInfo, err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("GetRoomInfoByRoomId GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return roomInfo, err
	}
	roomInfo = *roomInfoPoint
	return roomInfo, nil
}

func (this *TeachingRoomManager) GetRoomInfoWithOutLock(roomId uint32) (roomInfo *RoomInfo, err error) {
	roomInfo, ok := this.roomIdToRoomInfo[roomId]
	if ok {
		this.infoLog.Printf("GetRoomInfoWithOutLock roomId=%v exist in memory", roomId)
		return roomInfo, nil
	} else {
		this.infoLog.Printf("GetRoomInfoWithOutLock roomId=%v not exist in memory", roomId)
		err = ErrNotExist
		return nil, err
	}
}

func (this *TeachingRoomManager) AddReqUserToAlreadyInList(roomId, reqUid, mapedUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 || mapedUid == 0 {
		this.infoLog.Printf("AddReqUserToAlreadyInList param error roomId=%v reqUid=%v mapedUid=%v",
			roomId,
			reqUid,
			mapedUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("AddReqUserToAlreadyInList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	roomInfoPoint.AlreadyInList[reqUid] = &MemberSimpleInfo{
		Uid:          reqUid,
		WhiteBoardId: mapedUid,
		ActiveTS:     time.Now().Unix(),
	}
	return nil
}

func (this *TeachingRoomManager) RemoveReqUserFromAlreadyInList(roomId, reqUid, mapedUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 || mapedUid == 0 {
		this.infoLog.Printf("RemoveReqUserFromAlreadyInList param error roomId=%v reqUid=%v mapedUid=%v",
			roomId,
			reqUid,
			mapedUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("RemoveReqUserFromAlreadyInList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	delete(roomInfoPoint.AlreadyInList, reqUid)
	return nil
}

func (this *TeachingRoomManager) RemoveReqUserFromAllList(roomId, reqUid, mapedUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 || mapedUid == 0 {
		this.infoLog.Printf("RemoveReqUserFromAllList param error roomId=%v reqUid=%v mapedUid=%v",
			roomId,
			reqUid,
			mapedUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("RemoveReqUserFromAllList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	delete(roomInfoPoint.AlreadyInList, reqUid)
	// 从HandsUpdList 中删除
	roomInfoPoint.HandsUpList = this.DeleteFromListThreatUnSafe(roomInfoPoint.HandsUpList, reqUid)

	// 从连麦列表删除
	roomInfoPoint.MicrophoneConnList = this.DeleteFromListThreatUnSafe(roomInfoPoint.MicrophoneConnList, reqUid)
	return nil
}

func (this *TeachingRoomManager) DeleteFromListThreatUnSafe(oldList []*MemberSimpleInfo, reqUid uint32) (newList []*MemberSimpleInfo) {
	if len(oldList) == 0 { // 老的列表为空直接返回空的列表
		newList = []*MemberSimpleInfo{}
		return newList
	}
	if reqUid == 0 { // 待删除的reqUid为0 直接返回老的列表
		newList = oldList
		return newList
	}
	// 否则返回删除requid之后的列表
	for _, v := range oldList {
		if v.Uid != reqUid {
			newList = append(newList, v)
		} else {
			this.infoLog.Printf("DeleteFromListThreatUnSafe delete uid=%v", reqUid)
		}
	}
	return newList
}

func (this *TeachingRoomManager) AddReqUserToHandsUpList(roomId, reqUid, mapedUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 || mapedUid == 0 {
		this.infoLog.Printf("AddReqUserToHandsUpList param error roomId=%v reqUid=%v mapedUid=%v",
			roomId,
			reqUid,
			mapedUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("AddReqUserToHandsUpList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	roomInfoPoint.HandsUpList = this.DeleteFromListThreatUnSafe(roomInfoPoint.HandsUpList, reqUid)
	item := &MemberSimpleInfo{
		Uid:          reqUid,
		WhiteBoardId: mapedUid,
		ActiveTS:     0,
	}
	roomInfoPoint.HandsUpList = append(roomInfoPoint.HandsUpList, item)
	return nil
}

func (this *TeachingRoomManager) RemoveReqUserFromHandsUpList(roomId, reqUid, mapedUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 || mapedUid == 0 {
		this.infoLog.Printf("RemoveReqUserFromHandsUpList param error roomId=%v reqUid=%v mapedUid=%v",
			roomId,
			reqUid,
			mapedUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("RemoveReqUserFromHandsUpList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	roomInfoPoint.HandsUpList = this.DeleteFromListThreatUnSafe(roomInfoPoint.HandsUpList, reqUid)
	// 从连麦列表删除
	roomInfoPoint.MicrophoneConnList = this.DeleteFromListThreatUnSafe(roomInfoPoint.MicrophoneConnList, reqUid)
	return nil
}

func (this *TeachingRoomManager) RemoveReqUserFromHandsUpListThreatUnSafe(roomId, reqUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 {
		this.infoLog.Printf("RemoveReqUserFromHandsUpListThreatUnSafe param error roomId=%v reqUid=%v",
			roomId,
			reqUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("RemoveReqUserFromHandsUpListThreatUnSafe GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	roomInfoPoint.HandsUpList = this.DeleteFromListThreatUnSafe(roomInfoPoint.HandsUpList, reqUid)
	return nil
}

func (this *TeachingRoomManager) AddReqUserToMicrophoneConnList(roomId, reqUid, mapedUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 || mapedUid == 0 {
		this.infoLog.Printf("AddReqUserToMicrophoneConnList param error roomId=%v reqUid=%v mapedUid=%v",
			roomId,
			reqUid,
			mapedUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("AddReqUserToMicrophoneConnList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	// 首先将用户从列表中删除
	roomInfoPoint.MicrophoneConnList = this.DeleteFromListThreatUnSafe(roomInfoPoint.MicrophoneConnList, reqUid)
	item := &MemberSimpleInfo{
		Uid:          reqUid,
		WhiteBoardId: mapedUid,
		ActiveTS:     0,
	}
	// 再将用户添加到列表中
	roomInfoPoint.MicrophoneConnList = append(roomInfoPoint.MicrophoneConnList, item)
	return nil
}

func (this *TeachingRoomManager) HandleStudenHandsUpStat(roomId, opUid uint32, uidList []*ht_teaching_room.MapedUidInfo, handleType ht_teaching_room.HANDLE_TYPE) (err error) {
	if roomId == 0 || opUid == 0 || len(uidList) == 0 {
		this.infoLog.Printf("HandleStudenHandsUpStat invalid param roomId=%v opUid=%v uidListLen=%v handleType=%v",
			roomId,
			opUid,
			len(uidList),
			handleType)
		err = ErrInputParam
		return err
	}
	switch handleType {
	case ht_teaching_room.HANDLE_TYPE_HANDLE_ACCEPT:
		this.roomInfoLock.Lock()
		defer this.roomInfoLock.Unlock()
		roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
		if err != nil {
			this.infoLog.Printf("AddReqUserToMicrophoneConnList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
			return err
		}
		for i, v := range uidList {
			this.infoLog.Printf("AddReqUserToMicrophoneConnList roomId=%v index=%v uid=%v", roomId, i, v.GetHtUid())
			// 将用户添加到连麦列表中
			item := &MemberSimpleInfo{
				Uid:          v.GetHtUid(),
				WhiteBoardId: v.GetMapedUid(),
				ActiveTS:     0,
			}
			roomInfoPoint.MicrophoneConnList = append(roomInfoPoint.MicrophoneConnList, item)
			// 将用户添加到连麦列表时同时删除用户举手状态
			roomInfoPoint.HandsUpList = this.DeleteFromListThreatUnSafe(roomInfoPoint.HandsUpList, v.GetHtUid())
		}
	case ht_teaching_room.HANDLE_TYPE_HANDLE_REJECT:
		this.roomInfoLock.Lock()
		defer this.roomInfoLock.Unlock()
		roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
		if err != nil {
			this.infoLog.Printf("AddReqUserToMicrophoneConnList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
			return err
		}
		for i, v := range uidList {
			this.infoLog.Printf("AddReqUserToMicrophoneConnList roomId=%v index=%v uid=%v", roomId, i, v.GetHtUid())
			roomInfoPoint.MicrophoneConnList = this.DeleteFromListThreatUnSafe(roomInfoPoint.MicrophoneConnList, v.GetHtUid())
			// 将用户从连麦列表删除时也添加到举手列表中
			// roomInfoPoint.HandsUpList[v.GetHtUid()] = &MemberSimpleInfo{
			// 	Uid:          v.GetHtUid(),
			// 	WhiteBoardId: v.GetMapedUid(),
			// 	ActiveTS:     0,
			// }
		}
	default:
		this.infoLog.Printf("HandleStudenHandsUpStat unhandle roomId=%v opUid=%v uidLen=%v handleType=%v",
			roomId,
			opUid,
			len(uidList),
			handleType)
	}
	return nil
}

func (this *TeachingRoomManager) DelRoomInfo(roomId uint32) (err error) {
	if roomId == 0 {
		this.infoLog.Printf("DelRoomInfo invalid param roomId=%v", roomId)
		err = ErrInputParam
		return err
	}
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	delete(this.roomIdToRoomInfo, roomId)
	return nil
}

func (this *TeachingRoomManager) UpdateRoomStat(roomId, reqUid uint32,
	statType ht_teaching_room.TR_UPDATE_STAT_TYPE,
	opStat ht_teaching_room.TR_STAT_OP_TYPE) (err error) {
	if roomId == 0 || reqUid == 0 {
		this.infoLog.Printf("UpdateRoomStat invalid param roomId=%v reqUid=%v updateType=%v opStat=%v",
			roomId,
			reqUid,
			statType,
			opStat)
		err = ErrInputParam
		return err
	}
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	if v, ok := this.roomIdToRoomInfo[roomId]; ok {
		switch statType {
		case ht_teaching_room.TR_UPDATE_STAT_TYPE_UPDATE_HANDS_UP_STAT:
			if opStat == ht_teaching_room.TR_STAT_OP_TYPE_STAT_OPEN {
				v.HandsUpStat = 1
			} else {
				v.HandsUpStat = 0
				// 关闭举手开关时清空举手列表和连麦列表
				v.HandsUpList = []*MemberSimpleInfo{}
				v.MicrophoneConnList = []*MemberSimpleInfo{}
			}
		case ht_teaching_room.TR_UPDATE_STAT_TYPE_UPDATE_WHITE_BOARD_STAT:
			if opStat == ht_teaching_room.TR_STAT_OP_TYPE_STAT_OPEN {
				v.WhiteBoardStat = 1
			} else {
				v.WhiteBoardStat = 0
			}
		case ht_teaching_room.TR_UPDATE_STAT_TYPE_UPDATE_HP_AND_WB_STAT:
			if opStat == ht_teaching_room.TR_STAT_OP_TYPE_STAT_OPEN {
				v.HandsUpStat = 1
				v.WhiteBoardStat = 1
			} else {
				v.HandsUpStat = 0
				v.WhiteBoardStat = 0
			}
		default:
			this.infoLog.Printf("UpdateRoomStat roomId=%v reqUid=%v updateType=%v opStat=%v unhandle",
				roomId,
				reqUid,
				statType,
				opStat)
			err = ErrInputParam
		}
	} else {
		this.infoLog.Printf("UpdateRoomStat session not exist roomId=%v uid=%v", roomId, reqUid)
	}
	return err
}

func (this *TeachingRoomManager) UpdateRoomSessionTime(roomId uint32, sessionTime uint64) (err error) {
	if roomId == 0 || sessionTime == 0 {
		this.infoLog.Printf("UpdateRoomSessionTime invalid param roomId=%v sessionTime=%v",
			roomId,
			sessionTime)
		err = ErrInputParam
		return err
	}
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	if v, ok := this.roomIdToRoomInfo[roomId]; ok {
		v.SessionTime = sessionTime
	} else {
		this.infoLog.Printf("UpdateRoomSessionTime session not exist roomId=%v ", roomId)
	}
	return err
}

func (this *TeachingRoomManager) UpdateRoomEndTime(roomId uint32, endTime uint64) (err error) {
	if roomId == 0 || endTime == 0 {
		this.infoLog.Printf("UpdateRoomEndTime invalid param roomId=%v endTime=%v",
			roomId,
			endTime)
		err = ErrInputParam
		return err
	}
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	var sessionTime uint64
	var obid string
	if v, ok := this.roomIdToRoomInfo[roomId]; ok {
		sessionTime = v.SessionTime
		obid = v.Obid
	} else {
		this.infoLog.Printf("UpdateRoomEndTime session not exist roomId=%v ", roomId)
		err = ErrNotExist
		return err
	}
	this.infoLog.Printf("UpdateRoomEndTime roomId=%v beginTs=%v endTs=%v obid=%s",
		roomId,
		sessionTime,
		endTime,
		obid)
	// update mongodb
	err = this.dbManager.UpdateBeginAndEndTime(roomId, sessionTime, endTime, obid)
	if err != nil {
		this.infoLog.Printf("UpdateRoomEndTime roomId=%v obid=%s beginTs=%v endTs=%v err=%s",
			roomId,
			obid,
			sessionTime,
			endTime,
			err)
	}
	return err
}

func (this *TeachingRoomManager) UpdateRoomEndTimeWithOutLock(roomId uint32, beginTs, endTs uint64, obid string) (err error) {
	if roomId == 0 || beginTs == 0 || endTs == 0 || len(obid) == 0 {
		this.infoLog.Printf("UpdateRoomEndTimeWithOutLock invalid param roomId=%v beginTs=%v endTs=%v obid=%s",
			roomId,
			beginTs,
			endTs,
			obid)
		err = ErrInputParam
		return err
	}
	this.infoLog.Printf("UpdateRoomEndTimeWithOutLock roomId=%v beginTs=%v endTs=%v obid=%s",
		roomId,
		beginTs,
		endTs,
		obid)
	// update mongodb
	err = this.dbManager.UpdateBeginAndEndTime(roomId, beginTs, endTs, obid)
	if err != nil {
		this.infoLog.Printf("UpdateRoomEndTimeWithOutLock roomId=%v obid=%s beginTs=%v endTs=%v err=%s",
			roomId,
			obid,
			beginTs,
			endTs,
			err)
	}
	return err
}

func (this *TeachingRoomManager) AsyncDo() {
	asyncDo(this.CheckAlreadyInUserAlive)
}

func asyncDo(fn func()) {
	go func() {
		fn()
	}()
}

func (this *TeachingRoomManager) CheckAlreadyInUserAlive() {
	defer func() {
		recover()
	}()

	for {
		time.Sleep(10 * time.Second) // every 10 second
		this.roomInfoLock.Lock()
		for roomId, session := range this.roomIdToRoomInfo {
			this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v wBid=%v createUid=%v",
				roomId,
				session.WhiteBoardId,
				session.CreateUid.Uid)
			var targetUid uint32
			for uid, simpleInfo := range session.AlreadyInList {
				targetUid = uid
				ret := this.IsUserAlive(uid, simpleInfo.ActiveTS)
				if !ret { // user timeout
					this.infoLog.Printf("CheckAlreadyInUserAlive uid=%v lastActiveTime=%v timeout", uid, simpleInfo.ActiveTS)
					// 删除timeout 用户
					delete(session.AlreadyInList, uid)
					err := this.BroadCastRoomStatWithRoomInfo(targetUid, session)
					if err != nil {
						attr := "gotr/bc_room_info_failed"
						libcomm.AttrAdd(attr, 1)
						this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v uid=%v BroadCastRoomStatWithRoomInfo err=%s", roomId, uid, err)
					}
				}
			}

			if len(session.AlreadyInList) == 0 { // 整个用户全部超时了直接删除room
				this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v all timeout session.SessionTime=%v", roomId, session.SessionTime)
				if session.SessionTime > 0 {
					// Step1: 更新课程的开始结束时间
					endTime := uint64(time.Now().UnixNano() / 1000000) // 毫秒 ms
					err := this.UpdateRoomEndTimeWithOutLock(roomId, session.SessionTime, endTime, session.Obid)
					if err != nil {
						this.infoLog.Printf("CheckAlreadyInUserAlive UpdateRoomEndTime failed roomId=%v err=%s",
							roomId,
							err)
					}
					this.infoLog.Printf("CheckAlreadyInUserAlive all time out roomId=%v beginTs=%v endTs=%v",
						roomId,
						session.SessionTime,
						endTime)
					// Step2: 广播课程结束
					err = this.BroadCastTeachingRoomEnd(session.RoomId, session.WhiteBoardId, session.CreateUid.Uid, session.NotifyUidList)
					if err != nil {
						this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v wBid=%v createUid=%v BroadCastTeachingRoomEnd failed",
							session.RoomId,
							session.WhiteBoardId,
							session.CreateUid.Uid)
					}
				}
				// 先广播结束 再将其删除 不能颠倒循序
				delete(this.roomIdToRoomInfo, roomId)
			}
		}
		this.roomInfoLock.Unlock()
	}
}

func (this *TeachingRoomManager) IsUserAlive(uid uint32, lastActiveTime int64) (ret bool) {
	duration := time.Now().Unix() - lastActiveTime
	if duration >= int64(USE_TIMEOUT_THRESHOLD) {
		ret = false
	} else {
		ret = true
	}
	return ret
}

func (this *TeachingRoomManager) BroadCastRoomStat(roomId, wBid, reqUid uint32,
	handsUpStat, whiteBoardStat uint32,
	alreadyInSlic, handsUpSlic, connSlic []uint32,
	sessionTime uint64) (code uint32, err error) {
	if roomId == 0 || wBid == 0 || reqUid == 0 {
		err = ErrInputParam
		this.infoLog.Printf("BroadCastRoomStat roomId=%v wBid=%v reqUid=%v handsUpStat=%v whiteBoardStat=%v alreadyInLen=%v handsUpLen=%v connSlicLen=%v sessionTime=%v",
			roomId,
			wBid,
			reqUid,
			handsUpStat,
			whiteBoardStat,
			len(alreadyInSlic),
			len(handsUpSlic),
			len(connSlic),
			sessionTime)
		return code, err
	}
	// Step5: begin broad cast
	code, err = this.mucApi.BroadCastRoomStat(roomId,
		wBid,
		reqUid,
		int32(handsUpStat),
		int32(whiteBoardStat),
		alreadyInSlic,
		handsUpSlic,
		connSlic,
		sessionTime)
	this.infoLog.Printf("BroadCastRoomStat roomId=%v wBid=%v reqUid=%v code=%v err=%s",
		roomId,
		wBid,
		reqUid,
		code,
		err)
	return code, err
}

func (this *TeachingRoomManager) BroadCastRoomStatWithRoomInfo(reqUid uint32, roomInfo *RoomInfo) (err error) {
	if roomInfo == nil {
		this.infoLog.Printf("BroadCastRoomStatWithRoomInfo input param error nil roomInfo reqUid=%v", reqUid)
		err = ErrInputParam
		return err
	}
	// get user slic
	alreadyInList := roomInfo.AlreadyInList
	handsUpList := roomInfo.HandsUpList
	microphoneConnList := roomInfo.MicrophoneConnList

	alreadyInSlic := make([]uint32, len(alreadyInList))
	i := 0
	for _, v := range alreadyInList {
		alreadyInSlic[i] = v.Uid
		i += 1
	}

	handsUpSlic := make([]uint32, len(handsUpList))
	for index, v := range handsUpList {
		handsUpSlic[index] = v.Uid
	}

	connSlic := make([]uint32, len(microphoneConnList))
	connLen := len(microphoneConnList)
	for index := 0; index < connLen; index += 1 {
		connSlic[index] = microphoneConnList[connLen-index-1].Uid
	}
	this.infoLog.Printf("BroadCastRoomStatWithRoomInfo alreadyInSlic=%v handsUpSlic=%v connSlic=%v",
		alreadyInSlic,
		handsUpSlic,
		connSlic)
	code, err := this.BroadCastRoomStat(
		roomInfo.RoomId,
		roomInfo.WhiteBoardId,
		reqUid,
		roomInfo.HandsUpStat,
		roomInfo.WhiteBoardStat,
		alreadyInSlic,
		handsUpSlic,
		connSlic,
		roomInfo.SessionTime)
	if err != nil || code != 0 {
		this.infoLog.Printf("BroadCastRoomStatWithRoomInfo roomId=%v BroadCastRoomStatWithRoomInfo failed code=%v err=%s",
			roomInfo.RoomId,
			code,
			err)
		if err == nil {
			err = ErrSendToMucFaild
		}
		return err
	}
	return nil
}

func (this *TeachingRoomManager) BroadCastTeachingRoomEnd(roomId, wBid, reqUid uint32, notifyUidList []uint32) (err error) {
	if roomId == 0 || wBid == 0 || reqUid == 0 || len(notifyUidList) == 0 {
		this.infoLog.Printf("BroadCastTeachingRoomEnd invalid param roomId=%v wBid=%v reqUid=%v notifyUidListLen=%v",
			roomId,
			wBid,
			reqUid,
			len(notifyUidList))
		err = ErrInputParam
		return err
	}

	code, err := this.mucApi.BroadCastTeachingRoomEnd(roomId, wBid, reqUid, notifyUidList, uint64(time.Now().UnixNano()/1000000))
	if err != nil || code != 0 {
		this.infoLog.Printf("BroadCastTeachingRoomEnd roomId=%v reqUid=%v mucApi.BroadCastTeachingRoomEnd failed code=%v err=%s",
			roomId,
			reqUid,
			code,
			err)
		if err == nil {
			err = ErrSendToMucFaild
		}
		return err
	}
	return err
}
func (this *TeachingRoomManager) HandleHeartBeat(roomId, uid uint32) (ret bool) {
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	if v, ok := this.roomIdToRoomInfo[roomId]; ok {
		if simpleInfo, searchRes := v.AlreadyInList[uid]; searchRes {
			if (time.Now().Unix() - simpleInfo.ActiveTS) > USE_TIMEOUT_THRESHOLD {
				this.RemoveReqUserFromHandsUpListThreatUnSafe(roomId, uid)
				// get user slic
				alreadyInList := v.AlreadyInList
				handsUpList := v.HandsUpList
				microphoneConnList := v.MicrophoneConnList

				alreadyInSlic := make([]uint32, len(alreadyInList))
				i := 0
				for _, v := range alreadyInList {
					alreadyInSlic[i] = v.Uid
					i += 1
				}

				handsUpSlic := make([]uint32, len(handsUpList))
				for index, v := range handsUpList {
					handsUpSlic[index] = v.Uid
				}

				connSlic := make([]uint32, len(microphoneConnList))
				connLen := len(microphoneConnList)
				for index := 0; index < connLen; index += 1 {
					connSlic[index] = microphoneConnList[connLen-index-1].Uid
				}
				code, err := this.BroadCastRoomStat(roomId,
					v.WhiteBoardId,
					uid,
					v.HandsUpStat,
					v.WhiteBoardStat,
					alreadyInSlic,
					handsUpSlic,
					connSlic,
					v.SessionTime)
				if err != nil {
					this.infoLog.Printf("roomId=%v uid=%v BroadCastRoomStat failed code=%v err=%s",
						roomId,
						uid,
						code,
						err)
				}
				ret = false
			} else {
				v.AlreadyInList[uid].ActiveTS = time.Now().Unix()
				ret = true
			}
		} else {
			this.infoLog.Printf("HandleHeartBeat uid not exist roomId=%v uid=%v ", roomId, uid)
			ret = true
		}
	} else {
		this.infoLog.Printf("HandleHeartBeat roomId not exist roomId=%v uid=%v", roomId, uid)
		ret = true
	}
	return ret
}

func (this *TeachingRoomManager) UpdateFavAndShareCount(roomId uint32, obid []byte, updateType ht_teaching_room.UpdateStaticType, updateCount int32) (err error) {
	if roomId == 0 || len(obid) == 0 || updateCount == 0 {
		this.infoLog.Printf("UpdateFavAndShareCount invalid param roomId=%v obid=%s upateType=%v updateCount=%v", roomId, obid, updateType, updateCount)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("UpdateFavAndShareCount invalid obid roomId=%v obid=%s upateType=%v updateCount=%v", roomId, obid, updateType, updateCount)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.UpdateFavAndShareCount(roomId, obid, updateType, updateCount)
	if err != nil {
		this.infoLog.Printf("UpdateFavAndShareCount roomId=%v obid=%s upateType=%v updateCount=%v", roomId, obid, updateType, updateCount)
	}
	return err
}

func (this *TeachingRoomManager) GetFavAndShareCount(roomId uint32, obid []byte) (favCount, shareCount uint32, err error) {
	if roomId == 0 || len(obid) == 0 {
		this.infoLog.Printf("GetFavAndShareCount invalid param roomId=%v obid=%s", roomId, obid)
		err = ErrInputParam
		return favCount, shareCount, err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("GetFavAndShareCount invalid obid roomId=%v obid=%s", roomId, obid)
		err = ErrInputParam
		return favCount, shareCount, err
	}

	favCount, shareCount, err = this.dbManager.GetFavAndShareCount(roomId, obid)
	return favCount, shareCount, err
}

func (this *TeachingRoomManager) GetLessonByObid(roomId uint32, cliObid []byte) (lessonList []LessonStore, maxObid []byte, err error) {
	if roomId == 0 || len(cliObid) == 0 {
		this.infoLog.Printf("GetLessonByObid invalid param roomId=%v cliObid=%v", roomId, cliObid)
		err = ErrInputParam
		return lessonList, maxObid, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetLessonByObid invalid obid roomId=%v obid=%s", roomId, cliObid)
		err = ErrInputParam
		return lessonList, maxObid, err
	}
	lessonList, maxObid, err = this.dbManager.GetLessonByObid(roomId, cliObid)
	return lessonList, maxObid, err
}
func (this *TeachingRoomManager) DelLessonByObid(roomId uint32, obid []byte) (err error) {
	if roomId == 0 || len(obid) == 0 {
		this.infoLog.Printf("DelLessonByObid invalid param roomId=%v obid=%v", roomId, obid)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("DelLessonByObid invalid obid roomId=%v obid=%s", roomId, obid)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.DelLessonByObid(roomId, obid)
	return err
}

func (this *TeachingRoomManager) UpdateRecord(obid string, record *RecordStore) (err error) {
	if len(obid) == 0 || record == nil {
		this.infoLog.Printf("UpdateRecord invalid param obid=%s record=%v", obid, *record)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateRecord invalid obid obid=%s record=%v", obid, *record)
		err = ErrInputParam
		return err
	}

	err = this.dbManager.UpdateRecord(obid, record)
	return err
}

func (this *TeachingRoomManager) UpdateWhiteBoardUrl(obid string, urls *WhiteBoardStore) (err error) {
	if len(obid) == 0 || urls == nil {
		this.infoLog.Printf("UpdateWhiteBoardUrl invalid param obid=%s urls=%v", obid, *urls)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateWhiteBoardUrl invalid obid obid=%s urls=%v", obid, *urls)
		err = ErrInputParam
		return err
	}

	err = this.dbManager.UpdateWhiteBoardUrl(obid, urls)
	return err
}

func (this *TeachingRoomManager) UpdateHangUp(obid string, hangUp *HandsUpStore) (err error) {
	if len(obid) == 0 || hangUp == nil {
		this.infoLog.Printf("UpdateHangUp invalid param obid=%s hangUp=%v", obid, *hangUp)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateHangUp invalid obid obid=%s hangUp=%v", obid, *hangUp)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.UpdateHangUp(obid, hangUp)
	return err
}

func (this *TeachingRoomManager) UpdateLessonCoverUrl(roomId, reqUid uint32, obid, url string) (err error) {
	if roomId == 0 || reqUid == 0 || len(obid) == 0 || len(url) == 0 {
		this.infoLog.Printf("UpdateLessonCoverUrl invalid param roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateLessonCoverUrl invalid obid roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.UpdateLessonCoverUrl(roomId, reqUid, obid, url)
	return err
}

func (this *TeachingRoomManager) GetLessonByOneObid(cliObid []byte) (lesson LessonStore, err error) {
	if len(cliObid) == 0 {
		this.infoLog.Printf("GetLessonByOneObid invalid param cliObid=%v", cliObid)
		err = ErrInputParam
		return lesson, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetLessonByOneObid invalid obid obid=%s", cliObid)
		err = ErrInputParam
		return lesson, err
	}
	lesson, err = this.dbManager.GetLessonByOneObid(cliObid)
	return lesson, err
}
