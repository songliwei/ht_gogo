// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"log"
	"time"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_teachingroom"
	_ "github.com/go-sql-driver/mysql"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Error type
var (
	ErrNilDbObject     = errors.New("not set  object current is nil")
	ErrDbParam         = errors.New("err param error")
	ErrNilNoSqlObject  = errors.New("not set nosql object current is nil")
	ErrCloseExplicitly = errors.New("Closed explicitly")
)

var (
	TeachingRoomDB      = "teaching_room"
	LessonTable         = "lesson_table"
	pageSize            = 20
	WightBoardCreateUid = 1000
)

type FavAndShareCount struct {
	Id         string `bson:"_id"` // 在Mongo中唯一标识一条记录
	FavCount   uint32 // 收藏计数
	ShareCount uint32 // 分享计数
}

type RecordStore struct {
	TimeCreate uint64 // 录音创建时间
	EventType  string // 事件类型
	RoomName   string // 房间名称
	RecordUrl  string // 录音链接
}

type WhiteBoardUrlStore struct {
	WBid uint32 // 白板映射uid
	Url  string // 白板url
}

type WhiteBoardStore struct {
	TimeCreate uint64               // 录音创建时间
	EventType  string               // 事件类型
	RoomName   string               // 房间名称
	UrlSlic    []WhiteBoardUrlStore // 录音链接
}

type HandsUpStore struct {
	TimeCreate  uint64 // 录音创建时间
	EventType   string // 事件类型
	RoomName    string // 房间名称
	TotalSecond uint32 // 总时间的分部 秒
	TotalMinute uint32 // 总时间的分部 分钟
	TimeStart   string // 开始时间
	TimeEnd     string // 结束时间
}

type LessonStore struct {
	Id              bson.ObjectId `bson:"_id,omitempty" json:"-"` // 在Mongo中唯一标识一条记录
	RoomId          uint32
	WhiteBoardId    uint32
	CreateUid       uint32
	FavCount        uint32
	ShareCount      uint32
	LessonName      string
	LessonAbstract  string
	LessonConverUrl string
	Record          RecordStore
	WhiteBoard      WhiteBoardStore
	HandsUp         HandsUpStore
	LessonStat      uint32 //课程状态，删除课程时并不真正的删除，只是标记一下 0:正常 1：删除
	BeginTs         uint64 //课程开始时间
	EndTs           uint64 //课程结束时间
	UpdateTS        uint32
}

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
	noSqlDb *mgo.Session
}

func NewDbUtil(mysqlDb *sql.DB, mongoSess *mgo.Session, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		noSqlDb: mongoSess,
		infoLog: logger,
	}
}

func (this *DbUtil) RefreshSession(err error) {
	// 统计mongodb err count
	attr := "gotr/mongodb_error_count"
	libcomm.AttrAdd(attr, 1)
	this.noSqlDb.Refresh()
	this.infoLog.Printf("RefreshSession input err=%s", err)
	// if err == io.EOF || err == ErrCloseExplicitly {
	// 	this.noSqlDb.Refresh()
	// 	this.infoLog.Printf("RefreshSession input err=%s", err)
	// }
}

func (this *DbUtil) StoreLessonInfo(roomId, whiteBoardId, createUid uint32, lessonName []byte, lessonAbstract []byte, lessonCover []byte) (obid string, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return obid, err
	}

	if roomId == 0 || whiteBoardId == 0 || createUid == 0 || len(lessonName) == 0 {
		this.infoLog.Printf("AddLessonInfo invalid param roomId=%v whiteBoardId=%v createUid=%v lessonName=%s lessonAbstract=%s lessonCover=%s",
			roomId,
			whiteBoardId,
			createUid,
			lessonName,
			lessonAbstract,
			lessonCover)
		err = ErrDbParam
		return obid, err
	}

	// 将教学房间信息写入Mongo中 首先获取集合对象
	bsObid := bson.NewObjectId()
	obid = bsObid.Hex()

	this.infoLog.Printf("AddLessonInfo obid=%v obid srting=%v",
		obid,
		bsObid.String(),
	)

	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	lessonInfo := &LessonStore{
		Id:              bsObid,
		RoomId:          roomId,
		WhiteBoardId:    whiteBoardId,
		CreateUid:       createUid,
		FavCount:        0,
		ShareCount:      0,
		LessonName:      string(lessonName),
		LessonAbstract:  string(lessonAbstract),
		LessonConverUrl: string(lessonCover),
		Record: RecordStore{
			TimeCreate: 0,
			EventType:  "",
			RoomName:   "",
			RecordUrl:  "",
		},
		WhiteBoard: WhiteBoardStore{
			TimeCreate: 0,
			EventType:  "",
			RoomName:   "",
			UrlSlic:    nil,
		},
		HandsUp: HandsUpStore{
			TimeCreate:  0,
			EventType:   "",
			RoomName:    "",
			TotalSecond: 0,
			TotalMinute: 0,
			TimeStart:   "",
			TimeEnd:     "",
		},
		LessonStat: 0,
		BeginTs:    0,
		EndTs:      0,
		UpdateTS:   uint32(time.Now().Unix()),
	}

	err = mongoConn.Insert(lessonInfo)
	// 如果RoomId+Uid 作为唯一所以 如果RoomId+Uid已经存在则插入失败
	if err != nil {
		this.RefreshSession(err)
		this.infoLog.Printf("AddLessonInfo Mongo Insert failed, roomId=%v createUid=%v err=%v",
			roomId,
			createUid,
			err)
		return obid, err
	}

	// 更新完毕无需再次更新maxOrder了
	return obid, nil
}

func (this *DbUtil) GenerateWightBoardUidMap(createUid, roomId, wbRoomId uint32, notifyUidList []uint32) (outUidList, mapedUidList []uint32, err error) {
	if createUid == 0 || roomId == 0 || wbRoomId == 0 || len(notifyUidList) == 0 {
		this.infoLog.Printf("GenerateWightBoardUidMap param error createUid=%v roomId=%v wbRoomId=%v notifyUidListLen=%v", createUid, roomId, wbRoomId, len(notifyUidList))
		err = ErrDbParam
		return nil, nil, err
	}
	if this.db == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	// 首先插入其他人的映射关系
	mapedBeginUid := uint32(WightBoardCreateUid)
	for _, v := range notifyUidList {
		mapedBeginUid += 1 // 首先将maped uid 加1
		_, err = this.db.Exec("INSERT INTO HT_WB_UID_MAP (ROOMID, WBROOMID, USERID, WBID, UPDATETIME) VALUES (?, ?, ?, ?, UTC_TIMESTAMP())",
			roomId,
			wbRoomId,
			v,
			mapedBeginUid)
		if err != nil {
			this.infoLog.Printf("GenerateWightBoardUidMap failed createUid=%v roomId=%v wbRoomId=%v err=%v", createUid, roomId, wbRoomId, err)
			return outUidList, mapedUidList, err
		}
		// 将映射之后的uid添加到mapedUidList中
		outUidList = append(outUidList, v)
		mapedUidList = append(mapedUidList, mapedBeginUid)
	}
	// 然后插入创建者的映射关系
	_, err = this.db.Exec("INSERT INTO HT_WB_UID_MAP (ROOMID, WBROOMID, USERID, WBID, UPDATETIME) VALUES (?, ?, ?, ?, UTC_TIMESTAMP())",
		roomId,
		wbRoomId,
		createUid,
		WightBoardCreateUid)
	if err != nil {
		this.infoLog.Printf("GenerateWightBoardUidMap failed createUid=%v roomId=%v wbRoomId=%v err=%v", createUid, roomId, wbRoomId, err)
		return outUidList, mapedUidList, err
	}
	// 将创建者的uid添加到mapedUidList中
	outUidList = append(outUidList, createUid)
	mapedUidList = append(mapedUidList, uint32(WightBoardCreateUid))
	return outUidList, mapedUidList, nil
}

func (this *DbUtil) UpdateFavAndShareCount(roomId uint32, obid []byte, updateType ht_teaching_room.UpdateStaticType, updateCount int32) (err error) {
	if roomId == 0 || len(obid) == 0 || updateCount == 0 {
		this.infoLog.Printf("UpdateFavAndShareCount invalid param roomId=%v obid=%s upateType=%v updateCount=%v", roomId, obid, updateType, updateCount)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("UpdateFavAndShareCount invalid obid roomId=%v obid=%s upateType=%v updateCount=%v", roomId, obid, updateType, updateCount)
		err = ErrInputParam
		return err
	}

	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	switch updateType {
	case ht_teaching_room.UpdateStaticType_UPDATE_FAV_COUNT:
		if updateCount > 0 {
			err = mongoConn.Update(bson.M{"_id": bsObid, "roomid": roomId}, bson.M{"$inc": bson.M{"favcount": updateCount}})
		} else {
			err = mongoConn.Update(bson.M{"_id": bsObid, "roomid": roomId, "favcount": bson.M{"$gt": 0}}, bson.M{"$inc": bson.M{"favcount": updateCount}})
		}
	case ht_teaching_room.UpdateStaticType_UPDATE_SHARE_COUNT:
		if updateCount > 0 {
			err = mongoConn.Update(bson.M{"_id": bsObid, "roomid": roomId}, bson.M{"$inc": bson.M{"sharecount": updateCount}})
		} else {
			err = mongoConn.Update(bson.M{"_id": bsObid, "roomid": roomId, "sharecount": bson.M{"$gt": 0}}, bson.M{"$inc": bson.M{"sharecount": updateCount}})
		}
	default:
		this.infoLog.Printf("UpdateFavAndShareCount unhandle roomId=%v obid=%s updateType=%v updateCount=%v",
			roomId,
			obid,
			updateType,
			updateCount)
	}
	return err
}

func (this *DbUtil) GetFavAndShareCount(roomId uint32, obid []byte) (favCount, shareCount uint32, err error) {
	if roomId == 0 || len(obid) == 0 {
		this.infoLog.Printf("GetFavAndShareCount invalid param roomId=%v obid=%s", roomId, obid)
		err = ErrInputParam
		return favCount, shareCount, err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("GetFavAndShareCount invalid obid roomId=%v obid=%s", roomId, obid)
		err = ErrInputParam
		return favCount, shareCount, err
	}

	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	result := FavAndShareCount{}
	err = mongoConn.Find(bson.M{"_id": bsObid, "roomid": roomId}).Select(bson.M{"_id": 1, "favcount": 1, "sharecount": 1}).One(&result)
	if err != nil {
		this.RefreshSession(err)
		this.infoLog.Printf("GetFavAndShareCount roomId=%v obid=%s failed err=%s", roomId, obid, err)
		return favCount, shareCount, err
	}
	favCount = result.FavCount
	shareCount = result.ShareCount
	this.infoLog.Printf("GetFavAndShareCount roomId=%v obid=%s favCount=%v shareCount=%v", roomId, obid, favCount, shareCount)
	return favCount, shareCount, nil
}

func (this *DbUtil) GetLessonByObid(roomId uint32, cliObid []byte) (lessonList []LessonStore, maxObid []byte, err error) {
	if roomId == 0 || len(cliObid) == 0 {
		this.infoLog.Printf("GetLessonByObid invalid param roomId=%v cliObid=%v", roomId, cliObid)
		err = ErrInputParam
		return lessonList, maxObid, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetLessonByObid invalid obid roomId=%v obid=%s", roomId, cliObid)
		err = ErrInputParam
		return lessonList, maxObid, err
	}
	bsObid := bson.ObjectIdHex(string(cliObid))
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	err = mongoConn.Find(bson.M{"roomid": roomId, "lessonstat": 0, "_id": bson.M{"$gt": bsObid}, "begints": bson.M{"$gt": 0}}).Sort("_id").Limit(pageSize).All(&lessonList)
	if err != nil {
		this.infoLog.Printf("GetLessonByObid roomId=%v cliObid=%s err=%s", roomId, cliObid, err)
		this.RefreshSession(err)
		return lessonList, maxObid, err
	}
	this.infoLog.Printf("GetLessonByObid roomId=%v cliObid=%s lessonListLen=%v", roomId, cliObid, len(lessonList))
	if len(lessonList) > 0 {
		maxObid = []byte(lessonList[len(lessonList)-1].Id.Hex())
	}
	return lessonList, maxObid, nil
}

func (this *DbUtil) DelLessonByObid(roomId uint32, obid []byte) (err error) {
	if roomId == 0 || len(obid) == 0 {
		this.infoLog.Printf("DelLessonByObid invalid param roomId=%v obid=%v", roomId, obid)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("DelLessonByObid invalid obid roomId=%v obid=%s", roomId, obid)
		err = ErrInputParam
		return err
	}
	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid, "roomid": roomId}, bson.M{"$set": bson.M{"lessonstat": uint32(1), "updatets": uint32(time.Now().Unix())}})
	//需要判断一下是否存在 todo
	if err != nil {
		this.infoLog.Printf("DelLessonByObid roomId=%v obid=%s failed err=%s", roomId, obid, err)
		this.RefreshSession(err)
		if err == mgo.ErrNotFound {
			this.infoLog.Printf("procDelReq roomId=%v obid=%s not found continue", roomId, obid)
			return nil
		} else {
			return err
		}
	}
	return nil
}

func (this *DbUtil) UpdateRecord(obid string, record *RecordStore) (err error) {
	if len(obid) == 0 || record == nil {
		this.infoLog.Printf("UpdateRecord invalid param obid=%s record=%v", obid, *record)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateRecord invalid obid obid=%s record=%v", obid, *record)
		err = ErrInputParam
		return err
	}

	bsObid := bson.ObjectIdHex(obid)
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid}, bson.M{"$set": bson.M{"record": *record}})
	if err != nil {
		this.infoLog.Printf("UpdateRecord obid=%s failed refresh session", obid)
		this.RefreshSession(err)
	}
	return err
}

func (this *DbUtil) UpdateWhiteBoardUrl(obid string, urls *WhiteBoardStore) (err error) {
	if len(obid) == 0 || urls == nil {
		this.infoLog.Printf("UpdateWhiteBoardUrl invalid param obid=%s urls=%v", obid, *urls)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateWhiteBoardUrl invalid obid obid=%s urls=%v", obid, *urls)
		err = ErrInputParam
		return err
	}

	bsObid := bson.ObjectIdHex(obid)
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid}, bson.M{"$set": bson.M{"whiteboard": *urls}})
	if err != nil {
		this.infoLog.Printf("UpdateWhiteBoardUrl obid=%s failed refresh session", obid)
		this.RefreshSession(err)
	}
	return err
}

func (this *DbUtil) UpdateHangUp(obid string, hangUp *HandsUpStore) (err error) {
	if len(obid) == 0 || hangUp == nil {
		this.infoLog.Printf("UpdateHangUp invalid param obid=%s hangUp=%v", obid, *hangUp)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateHangUp invalid obid obid=%s hangUp=%v", obid, *hangUp)
		err = ErrInputParam
		return err
	}

	bsObid := bson.ObjectIdHex(obid)
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid}, bson.M{"$set": bson.M{"handsup": *hangUp}})
	if err != nil {
		this.infoLog.Printf("UpdateHangUp obid=%s failed refresh session", obid)
		this.RefreshSession(err)
	}
	return err
}

func (this *DbUtil) UpdateLessonCoverUrl(roomId, reqUid uint32, obid, url string) (err error) {
	if roomId == 0 || reqUid == 0 || len(obid) == 0 || len(url) == 0 {
		this.infoLog.Printf("UpdateLessonCoverUrl invalid param roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateLessonCoverUrl invalid obid roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}

	bsObid := bson.ObjectIdHex(obid)
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid, "roomid": roomId, "createuid": reqUid}, bson.M{"$set": bson.M{"lessonconverurl": url}})
	if err != nil {
		this.infoLog.Printf("UpdateLessonCoverUrl roomId=%v reqUid=%v obid=%s url=%s failed refresh session",
			roomId,
			reqUid,
			obid,
			url)
		this.RefreshSession(err)
	}
	return err
}

func (this *DbUtil) UpdateBeginAndEndTime(roomId uint32, beginTs, endTs uint64, obid string) (err error) {
	if roomId == 0 || beginTs == 0 || endTs == 0 || len(obid) == 0 {
		this.infoLog.Printf("UpdateBeginAndEndTime invalid param roomId=%v beginTs=%v endTs=%v obid=%s",
			roomId,
			beginTs,
			endTs,
			obid)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateBeginAndEndTime invalid obid roomId=%v beginTs=%v endTs=%v obid=%s",
			roomId,
			beginTs,
			endTs,
			obid)
		err = ErrInputParam
		return err
	}

	bsObid := bson.ObjectIdHex(obid)
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid, "roomid": roomId}, bson.M{"$set": bson.M{"begints": beginTs, "endts": endTs}})
	if err != nil {
		this.infoLog.Printf("UpdateBeginAndEndTime roomId=%v beginTs=%v endTs=%v obid=%s update failed",
			roomId,
			beginTs,
			endTs,
			obid)
		this.RefreshSession(err)
	}
	return err
}

func (this *DbUtil) GetLessonByOneObid(cliObid []byte) (lesson LessonStore, err error) {
	if len(cliObid) == 0 {
		this.infoLog.Printf("GetLessonByOneObid invalid param cliObid=%v", cliObid)
		err = ErrInputParam
		return lesson, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetLessonByOneObid invalid obid obid=%s", cliObid)
		err = ErrInputParam
		return lesson, err
	}
	bsObid := bson.ObjectIdHex(string(cliObid))
	mongoConn := this.noSqlDb.DB(TeachingRoomDB).C(LessonTable)
	err = mongoConn.Find(bson.M{"lessonstat": 0, "_id": bsObid}).One(&lesson)
	if err != nil {
		this.infoLog.Printf("GetLessonByOneObid cliObid=%s err=%s", cliObid, err)
		this.RefreshSession(err)
		return lesson, err
	}
	this.infoLog.Printf("GetLessonByOneObid cliObid=%s lesson=%#v", cliObid, lesson)
	return lesson, nil
}
