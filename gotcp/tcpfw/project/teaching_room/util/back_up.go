// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"bytes"
	"compress/zlib"
	"errors"
	"fmt"
	"log"
	"net"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/libcrypto"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/golang/protobuf/proto"
)

var (
	ErrNilMCObject = errors.New("not set memcache object current is nil")
	ErrInputParam  = errors.New("err input param error")
	ErrOnlineIpErr = errors.New("err online state ip not exist")
	ErrExecLimit   = errors.New("err member exec limit")
	ErrAlreadyIn   = errors.New("err member already in")
	ErrNotInRoom   = errors.New("err member not int room")
)

const (
	NICKNAME_LEN    = 128
	CONTENT_LEN     = 128
	NORMALDEC_LEN   = 128
	SERVER_COMM_KEY = "lp$5F@nfN0Oh8I*5"
)

const (
	CMD_P2P_MESSAGE     = 0x4001
	CMD_P2P_MESSAGE_ACK = 0x4002

	CMD_MUC_MESSAGE     = 0x7009
	CMD_MUC_MESSAGE_ACK = 0x700A

	CMD_S2S_MESSAGE_PUSH     = 0x8027
	CMD_S2S_MESSAGE_PUSH_ACK = 0x8028

	CMD_NOTIFY_REMOVE_MEMBER  = 0x700D
	CMD_RECEIPT_REMOVE_MEMBER = 0x700E

	CMD_GVOIP_INVITE_BROADCAST               = 0x7103
	CMD_GVOIP_INVITE_BROADCAST_RECEIPT       = 0x7104
	CMD_GVOIP_MEMBER_JOIN_BROADCAST          = 0x7109
	CMD_GVOIP_MEMBER_JOIN_BROADCAST_RECEIPT  = 0x710A
	CMD_GVOIP_MEMBER_LEAVE_BROADCAST         = 0x710D
	CMD_GVOIP_MEMBER_LEAVE_BROADCAST_RECEIPT = 0x710E
	CMD_GVOIP_END_BROADCAST                  = 0x7111
	CMD_GVOIP_END_BROADCAST_RECEIPT          = 0x7112
)

// 每个群成员详细信息
type MemberInfoStruct struct {
	Uid          uint32 // 用户uid
	WhiteBoardId uint32 // 白板映射id
	NickName     string // 用户昵称
}

type RoomInfo struct {
	RoomId             uint32              // 群聊Id
	WhiteBoardId       uint32              // 白板功能Id
	CreateUid          uint32              // 创建者uid
	AlreadyInList      []*MemberInfoStruct // 已经加入白板用户列表
	HandsUpList        []*MemberInfoStruct // 举手用户列表
	MicrophoneConnList []*MemberInfoStruct // 麦克风接通用户列表
	RoomTS             int64               // 教学房间创建时间戳
}

type WhiteBoardManager struct {
	roomIdToRoomInfo map[uint32]*RoomInfo
	dbManager        *DbUtil
	sendSeq          uint16
	infoLog          *log.Logger
	roomInfoLock     sync.Mutex // sync mutex goroutines use this.roomIdToRoomInfo
}

func NewWhiteBoardManager(dbUtil *DbUtil,
	mc *common.MemcacheApi,
	offline *common.OfflineApiV2,
	pushServer *common.PushServerApi,
	imServerApi map[string]*common.ImServerApiV2,
	imOldServerApi map[string]*common.ImServerApiV2,
	pcImServerApi map[string]*common.ImServerApiV2,
	logger *log.Logger) *RoomManager {
	return &RoomManager{
		roomIdToRoomInfo: map[uint32]*RoomInfo{},
		roomIdToUser:     map[uint32][]uint32{},
		dbManager:        dbUtil,
		sendSeq:          0,
		mcApi:            mc,
		offlineApi:       offline,
		pushServerApi:    pushServer,
		imServer:         imServerApi,
		imOldServer:      imOldServerApi,
		pcImServer:       pcImServerApi,
		infoLog:          logger,
	}
}

// Convert uint to net.IP http://www.outofmemory.cn
func inet_ntoa(ipnr int64) net.IP {
	var bytes [4]byte
	bytes[0] = byte((ipnr >> 24) & 0xFF)
	bytes[1] = byte((ipnr >> 16) & 0xFF)
	bytes[2] = byte((ipnr >> 8) & 0xFF)
	bytes[3] = byte(ipnr & 0xFF)

	return net.IPv4(bytes[3], bytes[2], bytes[1], bytes[0])
}

// Convert net.IP to int64 ,  http://www.outofmemory.cn
func inet_aton(ipnr net.IP) int64 {
	bits := strings.Split(ipnr.String(), ".")

	b0, _ := strconv.Atoi(bits[0])
	b1, _ := strconv.Atoi(bits[1])
	b2, _ := strconv.Atoi(bits[2])
	b3, _ := strconv.Atoi(bits[3])

	var sum int64

	sum += int64(b0) << 24
	sum += int64(b1) << 16
	sum += int64(b2) << 8
	sum += int64(b3)

	return sum
}

func (this *RoomManager) GetPacketSeq() (packetSeq uint16) {
	this.sendSeq++
	return this.sendSeq
}

func (this *RoomManager) CreateMuc(createUid uint32, memberList []uint32, memberLimit uint32) (roomId uint32, roomTS uint64, err error) {
	roomId, roomTS, err = this.dbManager.CreateMucRoom(createUid, memberList, memberLimit)
	if err != nil {
		this.infoLog.Printf("CreateMuc db err =%v", err)
		return roomId, roomTS, err
	}
	return roomId, roomTS, nil
}

func (this *RoomManager) UidIsInSlice(uidList []uint32, uid uint32) bool {
	if uid == 0 || len(uidList) == 0 {
		return false
	}
	for _, v := range uidList {
		if uid == v {
			return true
		}
	}

	return false
}

func (this *RoomManager) MultiCastInviteReq(head *common.HeadV3, roomId uint32, inviter *ht_muc.RoomMemberInfo, inviteeList []*ht_muc.RoomMemberInfo, roomIdFrom uint32, msgId []byte, roomProfile *ht_muc.RoomProfile) (err error) {
	if roomId == 0 || inviter == nil || len(inviteeList) == 0 || roomProfile == nil {
		this.infoLog.Printf("MultiCastInviteReq invalid param roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v msgId=%v roomProfile=%v",
			roomId,
			inviter.GetUid(),
			len(inviteeList),
			roomIdFrom,
			msgId,
			*roomProfile)
		err = ErrInputParam
		return err
	}
	roomInfo, err := this.GetRoomInfo(roomId)
	if err != nil {
		this.infoLog.Printf("MultiCastInviteReq roomId=%v inviterUid=%v inviteeUidSize=%v roomUrl=%s err=%s",
			roomId,
			inviter.GetUid(),
			len(inviteeList),
			roomProfile.GetRoomPhotoUrl(),
			err)
		return err
	}
	var rebuildHeader common.XTHead
	rebuildHeader.Flag = head.Flag
	rebuildHeader.Version = head.Version
	rebuildHeader.CryKey = uint8(common.CServKey)
	rebuildHeader.TermType = head.TermType
	rebuildHeader.Cmd = uint16(CMD_P2P_MESSAGE)
	rebuildHeader.Seq = head.Seq
	rebuildHeader.From = inviter.GetUid()
	rebuildHeader.To = 0 // 设置ToId
	rebuildHeader.Len = 0

	rootObj := simplejson.New()
	rootObj.Set("msg_id", msgId)
	formatTime, ts := getCurrentFormatTime()
	rootObj.Set("send_time", formatTime)
	rootObj.Set("msg_type", RoomInvite)
	rootObj.Set("msg_model", "normal")
	rootObj.Set("from_profile_ts", time.Now().Unix())
	rootObj.Set("from_nickname", inviter.GetNickName())
	roomInvite := simplejson.New()
	roomInvite.Set("room_id", roomId)
	roomInvite.Set("inviter_uid", inviter.GetUid())
	roomInvite.Set("inviter_nickname", inviter.GetNickName())
	roomInvite.Set("room_head_url", roomProfile.GetRoomPhotoUrl())
	roomInvite.Set("room_name", roomInfo.RoomName)
	roomInvite.Set("room_desc", roomInfo.RoomDesc)
	roomInvite.Set("room_member_count", len(roomInfo.MemberList))
	roomInvite.Set("msg_id", msgId)
	roomInvite.Set("room_id_from", roomIdFrom)
	rootObj.Set("roominvite", roomInvite)
	// add se
	rootObj.Set("server_time", formatTime)
	rootObj.Set("server_ts", ts)

	strMsgBody, err := rootObj.MarshalJSON()
	if err != nil {
		this.infoLog.Printf("MultiCastInviteReqToOldVersion simpleJson MarshalJSON failed roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v msgId=%v roomProfile=%v",
			roomId,
			inviter.GetUid(),
			len(inviteeList),
			roomIdFrom,
			msgId,
			*roomProfile)
		return err
	}
	// 压缩JSON消息数据
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(strMsgBody)
	w.Close()
	compressSlice := b.Bytes()
	cryStr := libcrypto.TEAEncrypt(string(compressSlice), SERVER_COMM_KEY)
	crySlice := []byte(cryStr)
	for _, v := range inviteeList {
		inviteeUid := v.GetUid()
		rebuildHeader.To = inviteeUid
		// 用户消息处理方式 默认为存储离线
		var procType int = CSaveOffline
		// 查询用户的在线状态
		onlineStat, err := this.mcApi.GetUserOnlineStat(inviteeUid)
		if err == nil {
			memberInfo := &MemberInfoStruct{Uid: inviteeUid}
			procType = this.GetMucMsgProcType(onlineStat, memberInfo, false)
			switch procType {
			case CSendToIMServer:
				err := this.SendPacketToOldIMServerRelabile(onlineStat, &rebuildHeader, crySlice)
				if err != nil {
					this.infoLog.Printf("MultiCastInviteReqToOldVersion SendPacketToOldIMServerRelabile failed from=%v to=%v seq=%v",
						rebuildHeader.From,
						rebuildHeader.To,
						rebuildHeader.Seq)
					// 发送到IM失败存储离线
					ret, err := this.offlineApi.SendPacketWithXTHead(&rebuildHeader, compressSlice, nil)
					if err != nil {
						this.infoLog.Printf("MultiCastInviteReqToOldVersion SendPacketWithXTHead faield from=%v to=%v seq=%v err=%s",
							rebuildHeader.From,
							rebuildHeader.To,
							rebuildHeader.Seq,
							err)
					} else {
						this.infoLog.Printf("MultiCastInviteReqToOldVersion SendPacketWithXTHead success from=%v to=%v cmd=%v seq=%v ret=%v",
							rebuildHeader.From,
							rebuildHeader.To,
							rebuildHeader.Cmd,
							rebuildHeader.Seq,
							ret)
					}
				} else {
					this.infoLog.Printf("MultiCastInviteReqToOldVersion SendPacketToOldIMServerRelabile success from=%v to=%v cmd=%v seq=%v",
						rebuildHeader.From,
						rebuildHeader.To,
						rebuildHeader.Cmd,
						rebuildHeader.Seq)
				}
			case CSaveOffLineAndPush, CSaveOffline:
				ret, err := this.offlineApi.SendPacketWithXTHead(&rebuildHeader, compressSlice, nil)
				if err != nil {
					this.infoLog.Printf("MultiCastInviteReqToOldVersion SendPacketWithXTHead faield from=%v to=%v cmd=%v seq=%v err=%v",
						rebuildHeader.From,
						rebuildHeader.To,
						rebuildHeader.Cmd,
						rebuildHeader.Seq,
						err)
				} else {
					this.infoLog.Printf("MultiCastInviteReqToOldVersion SendPacketWithXTHead success from=%v to=%v cmd=%v seq=%v ret=%v",
						rebuildHeader.From,
						rebuildHeader.To,
						rebuildHeader.Cmd,
						rebuildHeader.Seq,
						ret)
				}
			default:
				this.infoLog.Printf("Unhandle stat=%v", procType)
			}
		} else {
			this.infoLog.Printf("MultiCastInviteReqToOldVersion Get msg proc failed safe offline uid=%v err=%v", inviteeUid, err)
			// 获取不到在线，根据onlyOnlie 存储老版本的离线
			ret, err := this.offlineApi.SendPacketWithXTHead(&rebuildHeader, compressSlice, nil)
			if err != nil {
				this.infoLog.Printf("MultiCastInviteReqToOldVersion SendPacketWithXTHead faield roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v msgId=%v roomProfile=%v err=%s",
					roomId,
					inviter.GetUid(),
					len(inviteeList),
					roomIdFrom,
					msgId,
					*roomProfile,
					err)
			} else {
				this.infoLog.Printf("MultiCastInviteReqToOldVersion SendPacketWithXTHead success roomId=%v from=%v to=%v cmd=%v seq=%v ret=%v",
					roomId,
					rebuildHeader.From,
					rebuildHeader.To,
					rebuildHeader.Cmd,
					rebuildHeader.Seq,
					ret)
			}
		}
	}
	return nil
}

func getCurrentFormatTime() (formatTime string, timeStamp int64) {
	currTime := time.Now().UTC()
	formatTime = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d",
		currTime.Year(),
		currTime.Month(),
		currTime.Day(),
		currTime.Hour(),
		currTime.Minute(),
		currTime.Second())
	timeStamp = currTime.UnixNano() / (1000 * 1000)
	return
}

func (this *RoomManager) GenerateWBUidMap(createUid, roomId, wbRoomId uint32, notifyUidList []uint32) (outUidList, mapedUidList []uint32, err error) {
	if createUid == 0 || roomId == 0 || wbRoomId == 0 || len(notifyUidList) == 0 {
		this.infoLog.Printf("GenerateWBUidMap param error createUid=%v roomId=%v wbRoomId=%v notifyUidListLen=%v", createUid, roomId, wbRoomId, len(notifyUidList))
		err = ErrInputParam
		return nil, nil, err
	}
	outUidList, mapedUidList, err = this.dbManager.GenerateWightBoardUidMap(createUid, roomId, wbRoomId, notifyUidList)
	if err != nil {
		this.infoLog.Printf("GenerateWBUidMap this.dbManager.GenerateWightBoardUidMap failed createUid=%v roomId=%v wbRoomId=%v notifyUidListLen=%v err=%s",
			createUid,
			roomId,
			wbRoomId,
			len(notifyUidList),
			err)
		return nil, nil, err
	}
	return outUidList, mapedUidList, nil
}
