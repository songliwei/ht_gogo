package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_teachingroom"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	Cmd int `short:"t" long:"cmd" description:"Command type" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)
	//inviteUidCfg := cfg.Section("TEST_UID").Key("uid_slice").MustString("2325928")
	//uidSlic := strings.Split(inviteUidCfg, ",")
	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	v3Protocol := &common.HeadV3Protocol{}
	var head *common.HeadV3
	head = &common.HeadV3{Flag: 0xF0,
		Version:  4,
		CryKey:   0,
		TermType: 0,
		Cmd:      0x00,
		Seq:      0x1000,
		From:     2325928,
		To:       0,
		Len:      0,
	}

	var payLoad []byte
	reqBody := new(ht_teaching_room.TeachingRoomReqBody)
	switch options.Cmd {
	// 创建教学房间
	case 0x7311:
		head.Cmd = 0x7311
		subReqBody := &ht_teaching_room.CreateTeachingRoomReqBody{
			RoomId:       proto.Uint32(17361),
			WhiteBoardId: proto.Uint32(17361),
			CreateUid: &ht_teaching_room.TeachingRoomMemberInfo{
				Uid:          proto.Uint32(2325928),
				NickName:     []byte("songliwei"),
				HeadPhotoUrl: nil,
				Country:      nil,
			},

			NotifyUidList: []uint32{
				3249944,
				3249889,
				900866,
			},
			LessonName:     []byte("lesson1"),
			LessonAbstract: []byte("first lesson"),
			LessonCoverUrl: []byte("www.hellotalk.com"),
		}
		reqBody.CreateTeachingRoomReqbody = subReqBody

	// 申请广播教学房间已创建
	case 0x7313:
		head.Cmd = 0x7313
		subReqBody := &ht_teaching_room.ReqBroadCastRoomCreatedReqBody{
			RoomId:       proto.Uint32(17361),
			WhiteBoardId: proto.Uint32(17361),
			HtUid:        proto.Uint32(2325928),
		}
		reqBody.ReqBroaCastRoomCreatedReqbody = subReqBody

	case 0x7315:
		head.Cmd = 0x7315
		subReqBody := &ht_teaching_room.RequestJoinTeachingRoomReqBody{
			RoomId:       proto.Uint32(17361),
			WhiteBoardId: proto.Uint32(17361),
			UidInfo: &ht_teaching_room.MapedUidInfo{
				HtUid:    proto.Uint32(3249944),
				MapedUid: proto.Uint32(1001),
			},
		}
		reqBody.RequestJoinTeachingRoomReqbody = subReqBody
	case 0x7317:
		head.Cmd = 0x7317
		subReqBody := &ht_teaching_room.RequestLeaveTeachingRoomReqBody{
			RoomId:       proto.Uint32(17361),
			WhiteBoardId: proto.Uint32(17361),
			UidInfo: &ht_teaching_room.MapedUidInfo{
				HtUid:    proto.Uint32(3249944),
				MapedUid: proto.Uint32(1001),
			},
		}
		reqBody.RequestLeaveTeachingRoomReqbody = subReqBody
	case 0x7319:
		head.Cmd = 0x7319
		subReqBody := &ht_teaching_room.TeachingRoomHeartBeatReqBody{
			RoomId: proto.Uint32(17361),
			UidInfo: &ht_teaching_room.MapedUidInfo{
				HtUid:    proto.Uint32(3249944),
				MapedUid: proto.Uint32(1001),
			},
		}
		reqBody.TeachingRoomHeatBeatReqbody = subReqBody
	case 0x731B:
		head.Cmd = 0x731B
		subReqBody := &ht_teaching_room.UpdateTeachingRoomStatReqBody{
			RoomId:       proto.Uint32(17361),
			WhiteBoardId: proto.Uint32(17361),
			ReqUid:       proto.Uint32(2325928),
			StatType:     ht_teaching_room.TR_UPDATE_STAT_TYPE_UPDATE_HANDS_UP_STAT.Enum(),
			OpStat:       ht_teaching_room.TR_STAT_OP_TYPE_STAT_OPEN.Enum(),
		}
		reqBody.UpdateTeachingRoomStatReqbody = subReqBody
	case 0x731D:
		head.Cmd = 0x731D
		subReqBody := &ht_teaching_room.HandsUpReqBody{
			RoomId:       proto.Uint32(17361),
			WhiteBoardId: proto.Uint32(17361),
			UidInfo: &ht_teaching_room.MapedUidInfo{
				HtUid:    proto.Uint32(3249944),
				MapedUid: proto.Uint32(1001),
			},
		}
		reqBody.HandsUpReqbody = subReqBody
	case 0x731F:
		head.Cmd = 0x731F
		subReqBody := &ht_teaching_room.CancleHandsUpReqBody{
			RoomId:       proto.Uint32(17361),
			WhiteBoardId: proto.Uint32(17361),
			UidInfo: &ht_teaching_room.MapedUidInfo{
				HtUid:    proto.Uint32(3249944),
				MapedUid: proto.Uint32(1001),
			},
		}
		reqBody.CancleHandsUpReqbody = subReqBody
	case 0x7321:
		head.Cmd = 0x7321
		subReqBody := &ht_teaching_room.HandleStudentHandsUpReqBody{
			HandleType: ht_teaching_room.HANDLE_TYPE_HANDLE_ACCEPT.Enum(),
			RoomId:     proto.Uint32(17361),
			OpUid:      proto.Uint32(2325928),
			UidList: []*ht_teaching_room.MapedUidInfo{
				&ht_teaching_room.MapedUidInfo{
					HtUid:    proto.Uint32(3249944),
					MapedUid: proto.Uint32(1001),
				},
			},
		}
		reqBody.HandleStudentHandsUpReqbody = subReqBody
	case 0x7323:
		head.Cmd = 0x7323
		subReqBody := &ht_teaching_room.UpdateFavAndShareCountReqBody{
			RoomId:      proto.Uint32(17361),
			Obid:        []byte("59a3bcf6372cbf3e041797cc"),
			UpdateType:  ht_teaching_room.UpdateStaticType_UPDATE_FAV_COUNT.Enum(),
			UpdateCount: proto.Int32(1),
		}
		reqBody.UpdateFavAndShareCountReqbody = subReqBody
	case 0x7325:
		head.Cmd = 0x7325
		subReqBody := &ht_teaching_room.GetFavAndShareCountReqBody{
			RoomId: proto.Uint32(17361),
			ReqUid: proto.Uint32(2325928),
			Obid:   []byte("59a3bcf6372cbf3e041797cc"),
		}
		reqBody.GetFavAndShareCountReqbody = subReqBody
	case 0x7327:
		head.Cmd = 0x7327
		subReqBody := &ht_teaching_room.GetLessonByObidReqBody{
			RoomId: proto.Uint32(17361),
			Uid: &ht_teaching_room.MapedUidInfo{
				HtUid:    proto.Uint32(3249944),
				MapedUid: proto.Uint32(1001),
			},
			CliObid: []byte("59a3bcf6372cbf3e041797cc"),
		}
		reqBody.GetLessonByObidReqbody = subReqBody
	case 0x7329:
		head.Cmd = 0x7329
		subReqBody := &ht_teaching_room.DelLessonByObidReqBody{
			RoomId:     proto.Uint32(17361),
			CreaterUid: proto.Uint32(2325928),
			ObidList: [][]byte{
				[]byte("59a3bcf6372cbf3e041797cc"),
			},
		}
		reqBody.DelLessonByObidReqbody = subReqBody

	// 教学房间结束广播
	case 0x732B:
		head.Cmd = 0x732B
		subReqBody := &ht_teaching_room.TeachingRoomEndReqBody{
			RoomId:       proto.Uint32(17361),
			WhiteBoardId: proto.Uint32(17361),
			CreateUid:    proto.Uint32(2325928),
		}
		reqBody.TeachingRoomEndReqbody = subReqBody

	// 根据单个id取课程详情
	case 0x7345:
		head.Cmd = 0x7345
		subReqBody := &ht_teaching_room.GetLessonByOneObidReqBody{
			ReqUid: proto.Uint32(2325928),
			Obid:   []byte("59c37564372cbf109b8e53d1"),
		}
		reqBody.GetLessonByOneObidReqbody = subReqBody

	default:
		infoLog.Println("UnKnow input cmd =", options.Cmd)
	}

	payLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return
	}

	head.Len = uint32(common.PacketV3HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV3ToSlice failed")
		return
	}
	copy(buf[common.PacketV3HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	infoLog.Printf("len=%v payLaod=%v\n", len(payLoad), payLoad)
	// write
	conn.Write(buf)
	// read
	p, err := v3Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket := p.(*common.HeadV3Packet)
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("resp len=%v cmd=%v\n", rspHead.Len, rspHead.Cmd)
		rspBody := &ht_teaching_room.TeachingRoomRspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Println("proto Unmarshal failed")
			return
		}
		switch rspHead.Cmd {
		case 0x7311:
			subRspBody := rspBody.GetCreateTeachingRoomRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("CreateTeachingRoom rsp code=%v msg=%s obid=%s htuidlist=%v wbuidlist=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetObid(),
				subRspBody.GetHtUidList(),
				subRspBody.GetWbUidList())

		case 0x7313:
			subRspBody := rspBody.GetReqBroaCastRoomCreatedRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetReqBroaCastRoomCreatedRspbody code=%v reason=%s ts=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetTimeStamp())

		case 0x7315:
			subRspBody := rspBody.GetRequestJoinTeachingRoomRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetRequestJoinTeachingRoomRspbody code=%v reason=%s handsUpStat=%v wbStat=%v alreadyInList=%v handsUpList=%v connList=%v sessTime=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetHandsUpStat(),
				subRspBody.GetWhiteBoardStat(),
				subRspBody.GetAlreadyInList(),
				subRspBody.GetHandsUpList(),
				subRspBody.GetMicrophoneConnList(),
				subRspBody.GetSessionTime())

		case 0x7317:
			subRspBody := rspBody.GetRequestLeaveTeachingRoomRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetRequestLeaveRoomRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())

		case 0x7319:
			subRspBody := rspBody.GetTeachingRoomHeatBeatRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetTeachingRoomHeatBeatRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())

		case 0x731B:
			subRspBody := rspBody.GetUpdateTeachingRoomStatRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetUpdateRoomStatRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())

		case 0x731D:
			subRspBody := rspBody.GetHandsUpRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetHandsUpRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())

		case 0x731F:
			subRspBody := rspBody.GetCancleHandsUpRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetCancleHandsUpRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())

		case 0x7321:
			subRspBody := rspBody.GetHandleStudentHandsUpRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetHandleStudentHandsUpRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())

		case 0x7323:
			subRspBody := rspBody.GetUpdateFavAndShareCountRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetUpdateFavAndShareCountRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())

		case 0x7325:
			subRspBody := rspBody.GetGetFavAndShareCountRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetGetFavAndShareCountRspbody code=%v reason=%s favCount=%v shareCount=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetFavCount(),
				subRspBody.GetShareCount())
		case 0x7327:
			subRspBody := rspBody.GetGetLessonByObidRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetGetLessonByObidRspbody code=%v reason=%s MaxObid=%s",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetMaxObid())

			lessList := subRspBody.GetLessonList()
			for i, v := range lessList {
				infoLog.Printf("GetGetLessonByObidRspbody index=%v obid=%s name=%s abstract=%s cover=%s Record=%#v WhiteBoard=%#v HandsUp=%#v",
					i,
					v.GetObid(),
					v.GetLessonName(),
					v.GetLessonAbstract(),
					v.GetLessonCoverUrl(),
					*(v.GetRecord()),
					*(v.GetWhiteBoard()),
					*(v.GetHandsUp()))
			}
		case 0x7329:
			subRspBody := rspBody.GetDelLessonByObidRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetDelLessonByObidRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())
		case 0x732B:
			subRspBody := rspBody.GetTeachingRoomEndRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("teahing room end code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 0x7345:
			subRspBody := rspBody.GetGetLessonByOneObidRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("teahing room end code=%v msg=%s isInGroup=%v lesson=%#v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetIsUserInGroup(),
				*(subRspBody.GetLesson()))

		default:
			infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
		}

	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
