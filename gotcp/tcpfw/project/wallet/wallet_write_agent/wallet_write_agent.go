package main

import (
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_wallet"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	redisMasterApi *common.RedisApi
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
	ExpirePeriod       = 604800 //7 * 86400 = 30 day   units:second
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV3packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v from=%v", head.Cmd, head.Seq, head.From)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gowalletagent/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch uint32(head.Cmd) {
	case uint32(ht_wallet.WALLET_CMD_TYPE_CMD_SET_USER_BALANCE_REQ):
		go ProcSetUserBalance(c, head, packet)
	case uint32(ht_wallet.WALLET_CMD_TYPE_CMD_CLEAR_USER_BALANCE_REQ):
		go ProcClearUserBalance(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
	}
	return true
}

// 1.proc set base user cache
func ProcSetUserBalance(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.SetUserBalanceRspbody = &ht_wallet.SetUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserBalance invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletagent/set_user_balance_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserBalance proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SetUserBalanceRspbody = &ht_wallet.SetUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserBalanceReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserBalance GetSetUserBalanceReqbody() failed")
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SetUserBalanceRspbody = &ht_wallet.SetUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetReqUid()
	balanceInfo := subReqBody.GetCurrencyInfo()
	infoLog.Printf("ProcSetUserBalance reqUid=%v currence=%s amount=%v dollerAmount=%v symbol=%s",
		reqUid,
		balanceInfo.GetCurrencyType(),
		balanceInfo.GetAmount(),
		balanceInfo.GetCorrespondingDollar(),
		balanceInfo.GetCurrencySymbol())
	keyName := GetUserBalanceInfoKeyName(reqUid)
	value, err := proto.Marshal(balanceInfo)
	if err != nil {
		infoLog.Printf("reqUid=%v currence=%s amount=%v dollerAmount=%v proto.Marshal failed err=%s",
			reqUid,
			balanceInfo.GetCurrencyType(),
			balanceInfo.GetAmount(),
			balanceInfo.GetCorrespondingDollar(),
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_REDIS_ERR)
		rspBody.SetUserBalanceRspbody = &ht_wallet.SetUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("redis exec error"),
			},
		}
		attr := "gowalletagent/set_user_balance_redis_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	err = redisMasterApi.Set(keyName, string(value))
	if err != nil {
		infoLog.Printf("ProcSetUserBalance redisMasterApi.Set reqUid=%v keyName=%s failed err=%s", reqUid, keyName, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_REDIS_ERR)
		rspBody.SetUserBalanceRspbody = &ht_wallet.SetUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("redis exec error"),
			},
		}
		attr := "gowalletagent/set_user_balance_redis_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	err = redisMasterApi.Expire(keyName, ExpirePeriod)
	if err != nil {
		infoLog.Printf("ProcSetUserBalance redisMasterApi.Expire reqUid=%v keyName=%s failed err=%s", reqUid, keyName, err)
		attr := "gowalletagent/set_key_expire_redis_err"
		libcomm.AttrAdd(attr, 1)
	}
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.SetUserBalanceRspbody = &ht_wallet.SetUserBalanceRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func GetUserBalanceInfoKeyName(uid uint32) (key string) {
	key = fmt.Sprintf("%v#balance", uid)
	return key
}

// 2.proc clear user balance
func ProcClearUserBalance(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.ClearUserBalanceRsqbody = &ht_wallet.ClearUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcClearUserBalance invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletagent/clear_user_balance_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcClearUserBalance proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.ClearUserBalanceRsqbody = &ht_wallet.ClearUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetClearUserBalanceReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcClearUserBalance GetClearUserBalanceReqBody() failed")
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.ClearUserBalanceRsqbody = &ht_wallet.ClearUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetReqUid()
	infoLog.Printf("ProcClearUserBalance reqUid=%v", reqUid)
	keyName := GetUserBalanceInfoKeyName(reqUid)

	err = redisMasterApi.Del(keyName)
	if err != nil {
		infoLog.Printf("ProcClearUserBalance redisMasterApi.Del reqUid=%v keyName=%s failed err=%s", reqUid, keyName, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_REDIS_ERR)
		rspBody.ClearUserBalanceRsqbody = &ht_wallet.ClearUserBalanceRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("redis exec error"),
			},
		}
		attr := "gowalletagent/set_user_balance_redis_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.ClearUserBalanceRsqbody = &ht_wallet.ClearUserBalanceRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV3, rspBody *ht_wallet.WalletRspBody, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRsp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
