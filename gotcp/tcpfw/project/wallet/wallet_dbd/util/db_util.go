// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gansidui/gotcp/libcomm"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
)

// Error type
var (
	ErrNilDbObject      = errors.New("not set  object current is nil")
	ErrDbParam          = errors.New("err param error")
	ErrBalanceNotEnough = errors.New("balance is not enough")
)

const (
	BatchGetCount = 10
	MaxUniqId     = 90000000000
	TS_SOFT       = 8 * 3600
)

const (
	TYPE_COLLECTION_MONEY = 0
	TYPE_WITHDRAW_REQ     = 1
	TYPE_WITHDRAW_FAILED  = 2
	TYPE_WITHDRAW_SUCC    = 3
)

const (
	TYPE_RECHARGE    = 1
	TYPE_WITHDRAW    = 2
	TYPE_CONSUMPTION = 3
	TYPE_COLLECTION  = 4
)

type WithdrawalsAccount struct {
	AccountType uint32
	RealName    string
	Account     string
	RemarkInfo  string
	UniqId      uint64
	UpdateTime  uint64
}

type PayUserInfo struct {
	Uid      uint32 `json:"uid" binding:"required"`
	NickName string `json:"nickname" binding:"required"`
	HeadUrl  string `json:"headurl" binding:"required"`
	PayTs    uint64 `json:"payts" binding:"required"`
	National string `json:"national" binding:"required"`
}

type PayUserList struct {
	UserList []PayUserInfo `json:"userlist" binding:"required"`
}

type ChargingInfo struct {
	ChargingId   uint64
	OpUid        uint32
	PaymentFrom  uint32
	LessonObid   string
	LessonTitle  string
	Currence     string
	Amount       uint64
	Description  string
	RoomId       uint32
	RoomName     string
	PayUserCount uint32
	PayUserList  []PayUserInfo
	ChargingStat uint32
	CreateTime   uint64
}

type ChargingSimpleInfo struct {
	ChargingId   uint64
	OpUid        uint32
	PaymentFrom  uint32
	LessonTitle  string
	Currence     string
	Amount       uint64
	Description  string
	RoomId       uint32
	RoomName     string
	PayUserCount uint32
	ChargingStat uint32
	CreateTime   uint64
}

type BalanceInfo struct {
	Currence     string
	Amount       uint64
	DollerAmount uint64
}

type CollectMoneyRecord struct {
	UniqId        uint64 // 收款流水的唯一标识符
	PayUid        uint32 // 付款方
	CollectUid    uint32 // 收款方
	CharginId     uint64 // 收费商品的标识符
	Description   string // 交易描述
	Currence      string // 收费币种
	Amount        uint64 // 收费金额
	PlatAmont     uint64 // 平台费用金额
	TransId       string // 交易单号
	AccountType   uint32 // 支付方式
	BalandeAmount uint64 // 余额
	TimeStamp     uint64 // 收费时刻
}

type WithdrawalsStat struct {
	Stat      uint32 `json:"stat" binding:"required"`       // 提现状态
	OpUid     uint32 `json:"uid" binding:"required"`        // 草在用户id
	TimeStamp uint64 `json:"time_stamp" binding:"required"` // 时间戳
}

type WithdrawalsStatList struct {
	StatList []WithdrawalsStat `json:"statlist" binding:"required"` // 提现状态
}

type WithdrawalsRecord struct {
	Id            uint64
	ReqUid        uint32
	Currence      string            // 收费币种
	Amount        uint64            // 收费金额
	PlatAmont     uint64            // 平台费用金额
	Remarks       string            // 备注
	StatSlic      []WithdrawalsStat // 提现状态
	AccountType   uint32            // 支付方式
	Account       string            // 账号
	RealName      string            // 真实姓名
	BalandeAmount uint64            // 余额
	TimeStamp     uint64            // 发起提现时刻
	ServerRemarks string            // 服务端的备注
}

type TransationRecord struct {
	Id          uint64
	TransType   uint32 //交易类型 0:课程收费 1:提现 2:提现失败
	UniqId      uint64 //如果是收款则为收款记录的唯一标识符、如果是提现 则为提现记录的唯一标识符
	Currence    string // 收费币种
	Amount      uint64 // 收费金额
	AccountType uint32 //涉及到的帐号类型
	TransTime   uint64 //交易时间
}

type ExchangeRateInfo struct {
	Rate   uint64
	Symbol string
}

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

func (this *DbUtil) ClearTransaction(tx *sql.Tx) {
	err := tx.Rollback()
	if err != sql.ErrTxDone && err != nil {
		this.infoLog.Printf("ClearTransaction err=%s", err)
		attr := "gowalletdbd/clear_tran_err_count"
		libcomm.AttrAdd(attr, 1)
	}
}

// 1、插入提现账号
func (this *DbUtil) InsertWithdrawalsAccount(uid uint32, accountType uint32, realName string, account string, remarkInfo string) (err error) {
	if this.db == nil || uid == 0 || realName == "" || account == "" {
		this.infoLog.Printf("InsertWithdrawalsAccount uid=%v accountType=%v realName=%s account=%s remarkInfo=%s param error",
			uid,
			accountType,
			realName,
			account,
			remarkInfo)
		err = ErrDbParam
		return err
	}
	// Step1: 插入用户收款记录到数据库中
	_, err = this.db.Exec("insert into HT_USER_WALLET_ACCOUNT (user_id, account_type, real_name, account, remark, account_stat, update_time) VALUES (?, ?, ?, ?, ?, ?, UTC_TIMESTAMP()) ;",
		uid,
		accountType,
		realName,
		account,
		remarkInfo,
		0) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsAccount insert HT_USER_WALLET_ACCOUNT uid=%v accountType=%v realName=%s account=%s remarkInfo=%s failed err=%s",
			uid,
			accountType,
			realName,
			account,
			remarkInfo,
			err)
		return err
	}
	return nil
}

// 2、更新提现账号
func (this *DbUtil) UpdateWithdrawalsAccount(uid uint32, uniqId uint64, accountType uint32, realName string, account string, remarkInfo string) (err error) {
	if this.db == nil || uid == 0 || uniqId == 0 || realName == "" || account == "" {
		this.infoLog.Printf("UpdateWithdrawalsAccount uid=%v uniqId=%v accountType=%v realName=%s account=%s remarkInfo=%s param error",
			uid,
			uniqId,
			accountType,
			realName,
			account,
			remarkInfo)
		err = ErrDbParam
		return err
	}
	// Step1: 插入用户收款记录到数据库中
	_, err = this.db.Exec("update HT_USER_WALLET_ACCOUNT set account_type = ?, real_name = ?, account = ?, remark = ?, update_time = UTC_TIMESTAMP() where id = ?  and user_id=?;",
		accountType,
		realName,
		account,
		remarkInfo,
		uniqId,
		uid) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("UpdateWithdrawalsAccount update HT_USER_WALLET_ACCOUNT uid=%v uniqId=%v accountType=%v realName=%s account=%s remarkInfo=%s failed err=%s",
			uid,
			uniqId,
			accountType,
			realName,
			account,
			remarkInfo,
			err)
		return err
	}
	return nil
}

// 3、删除提现账号
func (this *DbUtil) DelWithdrawalsAccount(uid uint32, uniqId uint64) (err error) {
	if this.db == nil || uid == 0 || uniqId == 0 {
		this.infoLog.Printf("DelWithdrawalsAccount uid=%v uniqId=%v param error",
			uid,
			uniqId)
		err = ErrDbParam
		return err
	}
	// Step1: 插入用户收款记录到数据库中
	_, err = this.db.Exec("update HT_USER_WALLET_ACCOUNT set account_stat = 1, update_time = UTC_TIMESTAMP() where id = ? and user_id=?;",
		uniqId,
		uid) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("DelWithdrawalsAccount update HT_USER_WALLET_ACCOUNT uid=%v uniqid=%v failed err=%s",
			uid,
			uniqId,
			err)
		return err
	}
	return nil
}

// 4、批量取提现账号
func (this *DbUtil) GetWithdrawalsAccountByUid(uid uint32) (accountSlic []*WithdrawalsAccount, err error) {
	if this.db == nil || uid == 0 {
		this.infoLog.Printf("DelWithdrawalsAccount uid=%v param error", uid)
		err = ErrDbParam
		return nil, err
	}
	// 遍历 tmp_user_momet_info 表读取每个uid 查询 ssdb 得到moment数和likecoutn
	rows, err := this.db.Query("SELECT id, account_type, real_name, account, remark FROM HT_USER_WALLET_ACCOUNT WHERE user_id=? AND account_stat=0", uid)
	if err != nil {
		this.infoLog.Printf("GetWithdrawalsAccountByUid mysql query failed uid=%v err=%s", uid, err)
		return nil, err
	}
	defer rows.Close()
	index := 0
	for rows.Next() {
		var id uint64
		var accountType uint32
		var realName, account, remark string
		if err := rows.Scan(&id, &accountType, &realName, &account, &remark); err != nil {
			this.infoLog.Printf("GetWithdrawalsAccountByUid mysql get uid rows.Scan failed index=%v err=%s", index, err)
			continue
		}
		this.infoLog.Printf("index=%v uid=%v uniqId=%v accountType=%v realName=%s account=%s remark=%s",
			index,
			uid,
			id,
			accountType,
			realName,
			account,
			remark)
		accountInfo := &WithdrawalsAccount{
			AccountType: accountType,
			RealName:    realName,
			Account:     account,
			RemarkInfo:  remark,
			UniqId:      id,
		}
		accountSlic = append(accountSlic, accountInfo)
		index = index + 1
	}
	return accountSlic, nil
}

// 5、插入收款记录
func (this *DbUtil) InsertChargingRecord(reqUid uint32,
	lessonFrom uint32,
	lessonObid string,
	lessonTitle string,
	currenct string,
	amount uint64,
	description string,
	roomId uint32,
	roomName string) (lastInsertId int64, err error) {
	if this.db == nil || reqUid == 0 || lessonObid == "" || amount == 0 || currenct == "" {
		err = ErrDbParam
		this.infoLog.Printf("InsertChargingRecord reqUid=%v lessonFrom=%v lessonObid=%s lessonTitle=%s currenct=%s amount=%v description=%s roomId=%v roomName=%s param error",
			reqUid,
			lessonFrom,
			lessonObid,
			lessonTitle,
			currenct,
			amount,
			description,
			roomId,
			roomName)
		return lastInsertId, err
	}

	// Step1: 插入用户收款记录到数据库中
	res, err := this.db.Exec("insert into HT_COLLECT_MONEY_RECORD (user_id, lesson_from, lesson_obid, lesson_title, currency, amount, `desc`, room_id, room_name, pay_user_count, stat, create_time, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP()) ;",
		reqUid,
		lessonFrom,
		lessonObid,
		lessonTitle,
		currenct,
		fmt.Sprintf("%v", amount),
		description,
		roomId,
		roomName,
		0, // 0 人付费
		0) // 当前状态为正常收费
	if err != nil {
		this.infoLog.Printf("InsertChargingRecord insert HT_COLLECT_MONEY_RECORD opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s failed err=%s",
			reqUid,
			lessonFrom,
			lessonObid,
			lessonTitle,
			currenct,
			amount,
			description,
			roomId,
			roomName,
			err)
		return lastInsertId, err
	}
	lastInsertId, err = res.LastInsertId()
	if err != nil {
		this.infoLog.Printf("InsertChargingRecord reqUid=%v lessonFrom=%v lessonObid=%s lessonTitle=%s currenct=%s amount=%v description=%s roomId=%v roomName=%s get last insert id failed err=%s",
			reqUid,
			lessonFrom,
			lessonObid,
			lessonTitle,
			currenct,
			amount,
			description,
			roomId,
			roomName,
			err)
		return lastInsertId, err
	}
	return lastInsertId, nil
}

// 6、通过单个id查询收款的详情
func (this *DbUtil) GetChargingInfoById(uid uint32, uniqId uint64) (chargingInfo *ChargingInfo, err error) {
	if this.db == nil || uniqId == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetChargingInfoById uniqId=%v param error", uniqId)
		return nil, err
	}

	var storeUserId, storeFrom, storeAmount, storeRoomId, storePayUserCount, storeStat, storeCreateTime sql.NullInt64
	var storeLessonObid, storelessonTitle, storeCurrence, storeDescription, storeRoomName, storePayUserList sql.NullString
	err = this.db.QueryRow("SELECT user_id, lesson_from, lesson_obid, lesson_title, currency, amount, `desc`, room_id, room_name, pay_user_count, pay_uid_list, stat, UNIX_TIMESTAMP(create_time) FROM HT_COLLECT_MONEY_RECORD WHERE id=?;", uniqId).Scan(
		&storeUserId,
		&storeFrom,
		&storeLessonObid,
		&storelessonTitle,
		&storeCurrence,
		&storeAmount,
		&storeDescription,
		&storeRoomId,
		&storeRoomName,
		&storePayUserCount,
		&storePayUserList,
		&storeStat,
		&storeCreateTime)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetChargingInfoById not found uid=%v uinqId=%v in HT_COLLECT_MONEY_RECORD err=%s", uid, uniqId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetChargingInfoById exec failed HT_COLLECT_MONEY_RECORD uid=%v uniqId=%v err=%s", uid, uniqId, err)
		return nil, err
	default:
	}
	chargingInfo = &ChargingInfo{
		ChargingId:   uniqId,
		OpUid:        0,
		PaymentFrom:  0,
		LessonObid:   "",
		LessonTitle:  "",
		Currence:     "",
		Amount:       0,
		Description:  "",
		RoomId:       0,
		RoomName:     "",
		PayUserCount: 0,
		PayUserList:  nil,
		ChargingStat: 0,
		CreateTime:   0,
	}
	if storeUserId.Valid {
		chargingInfo.OpUid = uint32(storeUserId.Int64)
	}
	if storeFrom.Valid {
		chargingInfo.PaymentFrom = uint32(storeFrom.Int64)
	}
	if storeLessonObid.Valid {
		chargingInfo.LessonObid = storeLessonObid.String
	}
	if storelessonTitle.Valid {
		chargingInfo.LessonTitle = storelessonTitle.String
	}
	if storeCurrence.Valid {
		chargingInfo.Currence = storeCurrence.String
	}
	if storeAmount.Valid {
		chargingInfo.Amount = uint64(storeAmount.Int64)
	}
	if storeDescription.Valid {
		chargingInfo.Description = storeDescription.String
	}
	if storeRoomId.Valid {
		chargingInfo.RoomId = uint32(storeRoomId.Int64)
	}
	if storeRoomName.Valid {
		chargingInfo.RoomName = storeRoomName.String
	}
	if storePayUserCount.Valid {
		chargingInfo.PayUserCount = uint32(storePayUserCount.Int64)
	}
	if storePayUserList.Valid && storePayUserList.String != "" {
		payUserList := &PayUserList{}
		err = json.Unmarshal([]byte(storePayUserList.String), &payUserList) // JSON to Struct
		if err != nil {
			attr := "gowalletdbd/json_unmarshal_count"
			libcomm.AttrAdd(attr, 1)
			this.infoLog.Printf("GetChargingInfoById uid=%v uinqId=%v json.Unmarshal failed err=%s", uid, uniqId, err)
		} else {
			for i := len(payUserList.UserList) - 1; i >= 0; i -= 1 {
				chargingInfo.PayUserList = append(chargingInfo.PayUserList, payUserList.UserList[i])
			}
		}
	}
	if storeStat.Valid {
		chargingInfo.ChargingStat = uint32(storeStat.Int64)
	}
	if storeCreateTime.Valid {
		chargingInfo.CreateTime = uint64(storeCreateTime.Int64 + TS_SOFT)
	}
	return chargingInfo, nil
}

// 7、停止收款
func (this *DbUtil) StopChargingById(uid uint32, uniqId uint64) (err error) {
	if this.db == nil || uid == 0 || uniqId == 0 {
		this.infoLog.Printf("StopChargingById uid=%v uniqId=%v param error",
			uid,
			uniqId)
		err = ErrDbParam
		return err
	}
	// Step1: 插入用户收款记录到数据库中
	_, err = this.db.Exec("update HT_COLLECT_MONEY_RECORD set stat = 1, update_time = UTC_TIMESTAMP() where id = ? and user_id=?;",
		uniqId,
		uid) // 当前状态为正常账号
	if err != nil {
		attr := "gowalletdbd/stop_charge_failed_count"
		libcomm.AttrAdd(attr, 1)
		this.infoLog.Printf("StopChargingById update HT_COLLECT_MONEY_RECORD uid=%v uniqid=%v failed err=%s",
			uid,
			uniqId,
			err)
		return err
	}
	return nil
}

// 8、通过收款id查询该收款的状态
func (this *DbUtil) GetChargingStatById(uid uint32, uniqId uint64) (stat uint32, err error) {
	if this.db == nil || uniqId == 0 || uid == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetChargingStatById uniqId=%v uid=%v param error", uniqId, uid)
		return stat, err
	}

	var storeStat sql.NullInt64
	err = this.db.QueryRow("SELECT stat FROM HT_COLLECT_MONEY_RECORD WHERE id=?;", uniqId).Scan(&storeStat)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetChargingStatById not found uid=%v uinqId=%v in HT_COLLECT_MONEY_RECORD err=%s", uid, uniqId, err)
		return stat, err
	case err != nil:
		this.infoLog.Printf("GetChargingStatById exec failed HT_COLLECT_MONEY_RECORD uid=%v uniqId=%v err=%s", uid, uniqId, err)
		return stat, err
	default:
	}

	if storeStat.Valid {
		stat = uint32(storeStat.Int64)
	}

	return stat, nil
}

// 9、批量取所有自己发起过的收款记录
func (this *DbUtil) BatchGetChargingInfoByMinId(uid uint32, minUniqId uint64) (chargingSimpleInfoSlic []*ChargingSimpleInfo, outMinId uint64, err error) {
	if this.db == nil || uid == 0 {
		this.infoLog.Printf("BatchGetChargingInfoByMinId uid=%v param error", uid)
		err = ErrDbParam
		return nil, outMinId, err
	}
	if minUniqId == 0 {
		minUniqId = MaxUniqId
		this.infoLog.Printf("BatchGetChargingInfoByMinId uid=%v after transformed minUniqId=%v", uid, minUniqId)
	}

	// 遍历 tmp_user_momet_info 表读取每个uid 查询 ssdb 得到moment数和likecoutn
	rows, err := this.db.Query("SELECT id, lesson_from, lesson_title, currency, amount, `desc`, room_id, room_name, pay_user_count, stat, UNIX_TIMESTAMP(create_time) FROM HT_COLLECT_MONEY_RECORD  WHERE user_id=? AND id < ? ORDER BY id DESC LIMIT ?", uid, minUniqId, BatchGetCount)
	if err != nil {
		this.infoLog.Printf("BatchGetChargingInfoByMinId mysql query failed uid=%v err=%s", uid, err)
		return nil, outMinId, err
	}
	defer rows.Close()
	index := 0
	for rows.Next() {
		var id, amount, createTime uint64
		var from, roomId, payUserCount, stat uint32
		var lessonTitle, currency, description, roomName string
		if err := rows.Scan(&id, &from, &lessonTitle, &currency, &amount, &description, &roomId, &roomName, &payUserCount, &stat, &createTime); err != nil {
			this.infoLog.Printf("BatchGetChargingInfoByMinId mysql get uid rows.Scan failed uid=%v index=%v err=%s", uid, index, err)
			continue
		}
		this.infoLog.Printf("index=%v uid=%v id=%v from=%v lessonTitle=%s currency=%s description=%s roomId=%v roomName=%s payUserCount=%v stat=%v createTime=%v",
			index,
			uid,
			id,
			from,
			lessonTitle,
			currency,
			description,
			roomId,
			roomName,
			payUserCount,
			stat,
			createTime)
		simpleInfo := &ChargingSimpleInfo{
			ChargingId:   id,
			OpUid:        uid,
			PaymentFrom:  from,
			LessonTitle:  lessonTitle,
			Currence:     currency,
			Amount:       amount,
			Description:  description,
			RoomId:       roomId,
			RoomName:     roomName,
			PayUserCount: payUserCount,
			ChargingStat: stat,
			CreateTime:   createTime + TS_SOFT,
		}
		chargingSimpleInfoSlic = append(chargingSimpleInfoSlic, simpleInfo)
		outMinId = id // 增加MaxId的值
		index = index + 1
	}
	return chargingSimpleInfoSlic, outMinId, nil
}

// 10、通过收款记录唯一标识符查询已经付费的用户id列表
func (this *DbUtil) GetPayUserListByChargingId(uid uint32, uniqId uint64) (payUidSlic []PayUserInfo, err error) {
	if this.db == nil || uniqId == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetPayUserListByChargingId uniqId=%v param error", uniqId)
		return nil, err
	}

	var storePayUserList sql.NullString
	err = this.db.QueryRow("SELECT pay_uid_list FROM HT_COLLECT_MONEY_RECORD WHERE id=? AND user_id=?;", uniqId, uid).Scan(&storePayUserList)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetPayUserListByChargingId not found uid=%v uinqId=%v in HT_COLLECT_MONEY_RECORD err=%s", uid, uniqId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetPayUserListByChargingId exec failed HT_COLLECT_MONEY_RECORD uid=%v uniqId=%v err=%s", uid, uniqId, err)
		return nil, err
	default:
	}
	if storePayUserList.Valid {
		payUserList := &PayUserList{}
		err = json.Unmarshal([]byte(storePayUserList.String), &payUserList) // JSON to Struct
		if err != nil {
			attr := "gowalletdbd/json_unmarshal_count"
			libcomm.AttrAdd(attr, 1)
			this.infoLog.Printf("GetPayUserListByChargingId uid=%v uinqId=%v json.Unmarshal failed err=%s", uid, uniqId, err)
		} else {
			for i := len(payUserList.UserList) - 1; i >= 0; i -= 1 {
				payUidSlic = append(payUidSlic, payUserList.UserList[i])
			}
		}
	}
	return payUidSlic, nil
}

// 11、查询自己的账户余额
func (this *DbUtil) GetUserBalanceByUid(uid uint32) (balanceInfo BalanceInfo, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return balanceInfo, err
	}
	if uid == 0 {
		this.infoLog.Printf("GetUserBalanceByUid invalid param uid=%v", uid)
		err = ErrDbParam
		return balanceInfo, err
	}

	err = this.db.QueryRow("SELECT currency, amount, doller_amount FROM HT_USER_BALANCE WHERE user_id = ?;", uid).Scan(&balanceInfo.Currence, &balanceInfo.Amount, &balanceInfo.DollerAmount)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetUserBalanceByUid not found uid=%v", uid)
		break
	case err != nil:
		this.infoLog.Printf("GetUserBalanceByUid exec failed uid=%v, err=%s", uid, err)
		break
	default:
		this.infoLog.Printf("GetUserBalanceByUid uid=%v balanceInfo=%#v", uid, balanceInfo)
	}
	return balanceInfo, err
}

// 12、批量查询单个用户所有的收款流水
func (this *DbUtil) BatchGetCollectMoneyRecordByMinId(uid uint32, minUniqId uint64) (collectMoneySlic []*CollectMoneyRecord, outMinId uint64, err error) {
	if this.db == nil || uid == 0 {
		this.infoLog.Printf("BatchGetCollectMoneyRecordByMinId uid=%v param error", uid)
		err = ErrDbParam
		return nil, outMinId, err
	}
	if minUniqId == 0 {
		minUniqId = MaxUniqId
		this.infoLog.Printf("BatchGetCollectMoneyRecordByMinId uid=%v after transformed minUniqId=%v", uid, minUniqId)
	}
	// 遍历 tmp_user_momet_info 表读取每个uid 查询 ssdb 得到moment数和likecoutn
	rows, err := this.db.Query("SELECT id, pay_uid, collect_uid, trans_id, charging_id, remark, pay_type, currency, amount, plat_amount, balance_amount, UNIX_TIMESTAMP(update_time) FROM HT_USER_COLLECT_FLOW  WHERE collect_uid=? AND id < ? ORDER BY id DESC LIMIT ?", uid, minUniqId, BatchGetCount)
	if err != nil {
		this.infoLog.Printf("BatchGetCollectMoneyRecordByMinId mysql query failed uid=%v err=%s", uid, err)
		return nil, outMinId, err
	}
	defer rows.Close()
	index := 0
	for rows.Next() {
		var id, charginId, amount, paltAmount, balanceAmount, timeStamp uint64
		var payUid, collectUid, payType uint32
		var transId, remark, currency string
		if err := rows.Scan(&id, &payUid, &collectUid, &transId, &charginId, &remark, &payType, &currency, &amount, &paltAmount, &balanceAmount, &timeStamp); err != nil {
			this.infoLog.Printf("BatchGetCollectMoneyRecordByMinId mysql get uid rows.Scan failed uid=%v index=%v err=%s", uid, index, err)
			continue
		}
		this.infoLog.Printf("index=%v uid=%v id=%v payUid=%v collectUid=%v transId=%s chargingId=%v remark=%s payType=%v currency=%s amount=%v paltAmount=%v balanceAmount=%v timeStamp=%v",
			index,
			uid,
			id,
			payUid,
			collectUid,
			transId,
			charginId,
			remark,
			payType,
			currency,
			amount,
			paltAmount,
			balanceAmount,
			timeStamp)
		collectRecord := &CollectMoneyRecord{
			UniqId:        id,
			PayUid:        payUid,
			CollectUid:    collectUid,
			CharginId:     charginId,
			Description:   remark,
			Currence:      currency,
			Amount:        amount,
			PlatAmont:     paltAmount,
			TransId:       transId,
			AccountType:   payType,
			BalandeAmount: balanceAmount,
			TimeStamp:     timeStamp + TS_SOFT,
		}
		collectMoneySlic = append(collectMoneySlic, collectRecord)
		outMinId = id // 增加MaxId的值
		index = index + 1
	}
	return collectMoneySlic, outMinId, nil
}

// 13、批量查询单个用户所有的提现记录 记录按照时间逆序排序返回 客户端第一次拉取时填0来拉取最新的提现记录
func (this *DbUtil) BatchGetWithdrawalRecordByMinId(uid uint32, minUniqId uint64) (withdrawalsSlic []*WithdrawalsRecord, outMinId uint64, err error) {
	if this.db == nil || uid == 0 {
		this.infoLog.Printf("BatchGetWithdrawalRecordByMinId uid=%v param error", uid)
		err = ErrDbParam
		return nil, outMinId, err
	}
	if minUniqId == 0 {
		minUniqId = MaxUniqId
		this.infoLog.Printf("BatchGetWithdrawalRecordByMinId uid=%v after transformed minUniqId=%v", uid, minUniqId)
	}

	rows, err := this.db.Query("SELECT id, currency, amount, plat_amount, remark, serv_remark, applicat_stat, pay_type, account, real_name, balance_amount, UNIX_TIMESTAMP(applicate_time) FROM HT_WITHDRAW_RECORD  WHERE req_uid=? AND id < ? ORDER BY id DESC LIMIT ?", uid, minUniqId, BatchGetCount)
	if err != nil {
		this.infoLog.Printf("BatchGetWithdrawalRecordByMinId mysql query failed uid=%v err=%s", uid, err)
		return nil, outMinId, err
	}
	defer rows.Close()
	index := 0
	for rows.Next() {
		var id, amount, paltAmount, balanceAmount, timeStamp uint64
		var payType uint32
		var remark, serverRemark, currency, statStr, account, realName string
		if err := rows.Scan(&id, &currency, &amount, &paltAmount, &remark, &serverRemark, &statStr, &payType, &account, &realName, &balanceAmount, &timeStamp); err != nil {
			this.infoLog.Printf("BatchGetWithdrawalRecordByMinId mysql get uid rows.Scan failed uid=%v index=%v err=%s", uid, index, err)
			continue
		}
		this.infoLog.Printf("index=%v id=%v currency=%s amount=%v paltAmount=%v remark=%s serverRemark=%s statStr=%s payType=%v account=%s realName=%s balanceAmount=%v timeStamp=%v",
			index,
			id,
			currency,
			amount,
			paltAmount,
			remark,
			serverRemark,
			statStr,
			payType,
			account,
			realName,
			balanceAmount,
			timeStamp)

		withdrawalsStat := WithdrawalsStatList{}
		if statStr != "" {
			err = json.Unmarshal([]byte(statStr), &withdrawalsStat) // JSON to Struct
			if err != nil {
				attr := "gowalletdbd/json_unmarshal_count"
				libcomm.AttrAdd(attr, 1)
				this.infoLog.Printf("BatchGetWithdrawalRecordByMinId uid=%v uinqId=%v json.Unmarshal failed err=%s", uid, id, err)
			}
		}

		withdrawalsRecord := &WithdrawalsRecord{
			Id:            id,
			ReqUid:        uid,
			Currence:      currency,
			Amount:        amount,
			PlatAmont:     paltAmount,
			Remarks:       remark,
			StatSlic:      withdrawalsStat.StatList,
			AccountType:   payType,
			Account:       account,
			RealName:      realName,
			BalandeAmount: balanceAmount,
			TimeStamp:     timeStamp + TS_SOFT,
			ServerRemarks: serverRemark,
		}
		withdrawalsSlic = append(withdrawalsSlic, withdrawalsRecord)
		outMinId = id // 增加MaxId的值
		index = index + 1
	}
	return withdrawalsSlic, outMinId, nil
}

// 14、查询汇率 最终结果为汇率*1000
func (this *DbUtil) GetExchangeRate(srcCurrency string) (rateInfo ExchangeRateInfo, err error) {
	if this.db == nil || srcCurrency == "nil" {
		err = ErrDbParam
		this.infoLog.Printf("GetExchangeRate srcCurrency=%s param error", srcCurrency)
		return rateInfo, err
	}

	var storeRate sql.NullFloat64
	var storeSymbol sql.NullString
	err = this.db.QueryRow("SELECT static_rate, from_symbol FROM HT_CURRENCY_RATE WHERE `from`=? AND `to`=USD;", srcCurrency).Scan(&storeRate, &storeSymbol)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetExchangeRate not found srcCurrency=%s in HT_CURRENCY_RATE err=%s", srcCurrency, err)
		return rateInfo, err
	case err != nil:
		this.infoLog.Printf("GetExchangeRate exec failed HT_CURRENCY_RATE srcCurrency=%s err=%s", srcCurrency, err)
		return rateInfo, err
	default:
	}
	rateInfo = ExchangeRateInfo{}
	if storeRate.Valid {
		rateInfo.Rate = uint64(storeRate.Float64 * 1000)
	}
	if storeSymbol.Valid {
		rateInfo.Symbol = storeSymbol.String
	}
	return rateInfo, nil
}

// 15、批量查询所有其他币种到美元的汇率
func (this *DbUtil) BatchGetExchangeRate() (currencyToRate map[string]ExchangeRateInfo, err error) {
	if this.db == nil {
		this.infoLog.Printf("BatchGetExchangeRate  param error")
		err = ErrDbParam
		return currencyToRate, err
	}

	// 遍历 tmp_user_momet_info 表读取每个uid 查询 ssdb 得到moment数和likecoutn
	rows, err := this.db.Query("SELECT `from`, static_rate ,from_symbol FROM HT_CURRENCY_RATE  WHERE `to`=\"USD\";")
	if err != nil {
		this.infoLog.Printf("BatchGetExchangeRate mysql query failed err=%s", err)
		return currencyToRate, err
	}
	defer rows.Close()
	index := 0
	currencyToRate = make(map[string]ExchangeRateInfo)
	for rows.Next() {
		var rate float64
		var currency, symbol string
		if err := rows.Scan(&currency, &rate, &symbol); err != nil {
			this.infoLog.Printf("BatchGetExchangeRate mysql get uid rows.Scan failed index=%v err=%s", index, err)
			continue
		}
		this.infoLog.Printf("BatchGetExchangeRate index=%v currency=%s reate=%v symbol=%s", index, currency, rate, symbol)
		currencyToRate[currency] = ExchangeRateInfo{
			Rate:   uint64(rate * 10000), // 返回的汇率为当前浮点汇率*10000 之后的整形 保留四位有效数字
			Symbol: symbol,
		}
		index = index + 1
	}
	return currencyToRate, nil
}

// 16、申请提现
func (this *DbUtil) InsertWithdrawalsRecord(reqUid uint32,
	currency string,
	amount uint64,
	dollerAmount uint64,
	platAmount uint64,
	platDollerAmount uint64,
	platAmountRate float64,
	remark string,
	accountType uint32,
	realName string,
	account string) (uniqId uint64, err error) {
	if this.db == nil || reqUid == 0 || currency == "" || amount == 0 {
		err = ErrDbParam
		this.infoLog.Printf("InsertWithdrawalsRecord reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v remark=%s accountType=%v realName=%s account=%s param error",
			reqUid,
			currency,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			remark,
			accountType,
			realName,
			account)
		return uniqId, err
	}

	// Step1: 准备申请的状态
	statList := []WithdrawalsStat{}
	item := WithdrawalsStat{
		Stat:      0,
		OpUid:     0,
		TimeStamp: uint64(time.Now().Unix()),
	}
	statList = append(statList, item)

	withdrawalsStat := WithdrawalsStatList{
		StatList: statList,
	}
	withdrawalsStatSlic, err := json.Marshal(withdrawalsStat)
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v remark=%s accountType=%v realName=%s account=%s json.Marshal failed err=%s",
			reqUid,
			currency,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			remark,
			accountType,
			realName,
			account,
			err)
		return uniqId, err
	}
	// Step1: 首先查询个人的余额 判断是否大于提现金额 如果不大于直接返回错误
	var storeAmount sql.NullInt64
	err = this.db.QueryRow("SELECT amount FROM HT_USER_BALANCE WHERE user_id=?;", reqUid).Scan(&storeAmount)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("InsertWithdrawalsRecord not found user_id=%v in HT_USER_BALANCE err=%s", reqUid, err)
		return uniqId, err
	case err != nil:
		this.infoLog.Printf("InsertWithdrawalsRecord exec failed HT_USER_BALANCE reqUid=%v err=%s", reqUid, err)
		return uniqId, err
	default:
	}
	var totalAmount uint64
	if storeAmount.Valid {
		totalAmount = uint64(storeAmount.Int64)
	}
	// 检测余额是否足够
	if amount > totalAmount {
		this.infoLog.Printf("InsertWithdrawalsRecord reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v remark=%s accountType=%v realName=%s account=%s amount > totalAmount=%v",
			reqUid,
			currency,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			remark,
			accountType,
			realName,
			account,
			totalAmount)
		err = ErrBalanceNotEnough
		return uniqId, err
	}
	// Step2: 发起一个事务
	tx, err := this.db.Begin()
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord db.Begin reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v remark=%s accountType=%v realName=%s account=%s failed err=%s",
			reqUid,
			currency,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			remark,
			accountType,
			realName,
			account,
			err)
		return uniqId, err
	}
	defer this.ClearTransaction(tx)

	// Step3: 更新用户的个人余额表 减去提现的金额
	_, err = tx.Exec("insert into HT_USER_BALANCE (user_id, currency, amount, doller_amount, update_time) VALUES (?, ?, ?, ?, UTC_TIMESTAMP()) ON DUPLICATE KEY UPDATE amount=amount-VALUES(amount), doller_amount=doller_amount-VALUES(doller_amount), update_time=UTC_TIMESTAMP();",
		reqUid,
		currency,
		amount,
		dollerAmount)
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord update HT_USER_BALANCE reqUid=%v currency=%s amount=%v dollerAmount=%v err=%s",
			reqUid,
			currency,
			amount,
			dollerAmount,
			err)
		return uniqId, err
	}

	// Step4: 查询个人余额
	var storeBalance, sotreBalanceDollerAmount sql.NullInt64
	err = tx.QueryRow("SELECT amount, doller_amount FROM HT_USER_BALANCE WHERE user_id=?;", reqUid).Scan(&storeBalance, &sotreBalanceDollerAmount)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("InsertWithdrawalsRecord not found uid=%v in HT_USER_BALANCE err=%s", reqUid, err)
		return uniqId, err
	case err != nil:
		this.infoLog.Printf("InsertWithdrawalsRecord exec failed HT_USER_BALANCE reqUid=%v err=%s", reqUid, err)
		return uniqId, err
	default:
	}
	var balanceAmount uint64
	var balanceDollerAmount uint64
	if storeBalance.Valid {
		balanceAmount = uint64(storeBalance.Int64)
	}
	if sotreBalanceDollerAmount.Valid {
		balanceDollerAmount = uint64(sotreBalanceDollerAmount.Int64)
	}

	// Step5: 将用户的提现记录插入到数据库中
	res, err := tx.Exec("insert into HT_WITHDRAW_RECORD (req_uid, currency, amount, doller_amount, plat_amount, plat_doller_amount, plat_amount_rate, applicat_stat, remark, applicate_time, pay_type, real_name, account, balance_amount, balance_doller_amount, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP(), ?, ?, ?, ?, ?, UTC_TIMESTAMP()) ;",
		reqUid,
		currency,
		amount,
		dollerAmount,
		platAmount,
		platDollerAmount,
		platAmountRate,
		string(withdrawalsStatSlic),
		remark,
		accountType,
		realName,
		account,
		balanceAmount,
		balanceDollerAmount) // 当前状态为正常收费
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v platAmountRate=%v remark=%s accountType=%v realName=%s account=%s balanceAmount=%v balanceDollerAmount=%v exec insert failed err=%s",
			reqUid,
			currency,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			platAmountRate,
			remark,
			accountType,
			realName,
			account,
			balanceAmount,
			balanceDollerAmount,
			err)
		return uniqId, err
	}

	lastId, err := res.LastInsertId()
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v remark=%s accountType=%v realName=%s account=%s balanceAmount=%v balanceDollerAmount=%v get last insert id failed err=%s",
			reqUid,
			currency,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			remark,
			accountType,
			realName,
			account,
			balanceAmount,
			balanceDollerAmount,
			err)
		return uniqId, err
	}
	uniqId = uint64(lastId)

	// Step6: 发起个人提现申请时插入1条提现交易记录
	_, err = tx.Exec("insert into HT_TRANSACTION_RECORD (req_uid, uinq_id, trans_type, pay_type, currency, amount, doller_amount, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP()) ;",
		reqUid,
		uniqId,
		TYPE_WITHDRAW_REQ,
		accountType,
		currency,
		amount,
		dollerAmount)
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord insert HT_TRANSACTION_RECORD reqUid=%v uniqId=%v transType=%v payType=%v currency=%s amount=%v dollerAmount=%v err=%s",
			reqUid,
			uniqId,
			TYPE_WITHDRAW_REQ,
			accountType,
			currency,
			amount,
			dollerAmount,
			err)
		return uniqId, err
	}
	err = tx.Commit()
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord tx.Commit reqUid=%v uniqId=%v transType=%v payType=%v currency=%s amount=%v dollerAmount=%v err=%s",
			reqUid,
			uniqId,
			TYPE_WITHDRAW_REQ,
			accountType,
			currency,
			amount,
			dollerAmount,
			err)
		return uniqId, err
	}
	return uniqId, nil
}

func (this *DbUtil) InsertBalanceRecord(reqUid uint32, currency string, amount, dollerAmount uint64) (err error) {
	if this.db == nil || reqUid == 0 || currency == "" {
		err = ErrDbParam
		this.infoLog.Printf("InsertBalanceRecord reqUid=%v currency=%s amount=%v dollerAmount=%v param error",
			reqUid,
			currency,
			amount,
			dollerAmount)
		err = ErrDbParam
		return err
	}
	// Step3: 更新用户的个人余额表 减去提现的金额
	_, err = this.db.Exec("INSERT IGNORE INTO HT_USER_BALANCE (user_id, currency, amount, doller_amount, update_time) VALUES (?, ?, ?, ?, UTC_TIMESTAMP()) ;",
		reqUid,
		currency,
		amount,
		dollerAmount)
	if err != nil {
		this.infoLog.Printf("InsertBalanceRecord INSERT IGNORE INTO HT_USER_BALANCE reqUid=%v currency=%s amount=%v dollerAmount=%v err=%s",
			reqUid,
			currency,
			amount,
			dollerAmount,
			err)
		return err
	}
	return nil
}

// 17、更改提现的状态 此函数用于更新审核成功、已到账更新，不用用户审核失败更新
func (this *DbUtil) UpdateWithdrawalsStat(opUid uint32, uniqId uint64, newStat uint32, server_description string) (err error) {
	if this.db == nil || opUid == 0 || uniqId == 0 || newStat == 0 {
		this.infoLog.Printf("UpdateWithdrawalsStat opUid=%v uniqId=%v newStat=%v param error",
			opUid,
			uniqId,
			newStat)
		err = ErrDbParam
		return err
	}
	this.infoLog.Printf("UpdateWithdrawalsStat opUid=%v uniqId=%v newStat=%v desc=%s",
		opUid,
		uniqId,
		newStat,
		server_description)
	// Step1: 首先去除原来的状态添加一个新的状态到队列的末端
	var storeStat sql.NullString
	err = this.db.QueryRow("SELECT applicat_stat FROM HT_WITHDRAW_RECORD WHERE id=?;", uniqId).Scan(&storeStat)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("UpdateWithdrawalsStat not found opUid=%v uinqId=%v in HT_WITHDRAW_RECORD err=%s", opUid, uniqId, err)
		return err
	case err != nil:
		this.infoLog.Printf("UpdateWithdrawalsStat exec failed HT_WITHDRAW_RECORD opUid=%v uniqId=%v err=%s", opUid, uniqId, err)
		return err
	default:
	}
	// json 反序列化成结构体
	withdrawalsStat := WithdrawalsStatList{}
	err = json.Unmarshal([]byte(storeStat.String), &withdrawalsStat)
	if err != nil {
		this.infoLog.Printf("UpdateWithdrawalsStatjson.Unmarshal failed opUid=%v uinqId=%v storeStat=%s err=%s", opUid, uniqId, storeStat.String, err)
		return err
	}
	item := WithdrawalsStat{
		Stat:      newStat,
		OpUid:     opUid,
		TimeStamp: uint64(time.Now().Unix()),
	}
	withdrawalsStat.StatList = append(withdrawalsStat.StatList, item)
	// 将新的数组序列化成string
	newStatSlic, err := json.Marshal(withdrawalsStat)
	if err != nil {
		this.infoLog.Printf("UpdateWithdrawalsStat json.Marshal failed opUid=%v uinqId=%v err=%s", opUid, uniqId, err)
		return err
	}
	// Step2: 更新数据库提现的状态
	_, err = this.db.Exec("UPDATE HT_WITHDRAW_RECORD SET applicat_stat = ?, op_uid = ?, serv_remark=?, withdraw_stat=?, update_time = UTC_TIMESTAMP() where id = ?;",
		string(newStatSlic),
		opUid,
		server_description,
		newStat,
		uniqId) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("UpdateWithdrawalsStat update HT_WITHDRAW_RECORD opUid=%v uniqId=%v newStatSlic=%s failed err=%s",
			opUid,
			uniqId,
			newStatSlic,
			err)
		return err
	}
	return nil
}

// 18、审核不通过时 用此函数更新审核的状态和用户的余额表插入交易失败的记录
func (this *DbUtil) FailureAuditProcess(opUid uint32, uniqId uint64, description string) (err error) {
	if this.db == nil || opUid == 0 || uniqId == 0 {
		this.infoLog.Printf("UpdateWithdrawalsStat opUid=%v uniqId=%v param error",
			opUid,
			uniqId)
		err = ErrDbParam
		return err
	}
	// Step1: 首先去除原来的状态添加一个新的状态到队列的末端
	var storeUid, storePayType, storeAmount, storeDollerAmount sql.NullInt64
	var storeCurrency, storeStat sql.NullString
	err = this.db.QueryRow("SELECT req_uid, currency, amount, doller_amount, pay_type, applicat_stat FROM HT_WITHDRAW_RECORD WHERE id=?;", uniqId).Scan(
		&storeUid,
		&storeCurrency,
		&storeAmount,
		&storeDollerAmount,
		&storePayType,
		&storeStat)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("FailureAuditProcess not found opUid=%v uinqId=%v in HT_WITHDRAW_RECORD err=%s", opUid, uniqId, err)
		return err
	case err != nil:
		this.infoLog.Printf("FailureAuditProcess exec failed HT_WITHDRAW_RECORD opUid=%v uniqId=%v err=%s", opUid, uniqId, err)
		return err
	default:
	}
	// json 反序列化成结构体
	withdrawalsStat := WithdrawalsStatList{}
	err = json.Unmarshal([]byte(storeStat.String), &withdrawalsStat)
	if err != nil {
		this.infoLog.Printf("FailureAuditProcess json.Unmarshal failed opUid=%v uinqId=%v storeStat=%s err=%s", opUid, uniqId, storeStat.String, err)
		return err
	}
	item := WithdrawalsStat{
		Stat:      TYPE_WITHDRAW_FAILED,
		OpUid:     opUid,
		TimeStamp: uint64(time.Now().Unix()),
	}
	withdrawalsStat.StatList = append(withdrawalsStat.StatList, item)
	// 将新的数组序列化成string
	newStatSlic, err := json.Marshal(withdrawalsStat)
	if err != nil {
		this.infoLog.Printf("FailureAuditProcess json.Marshal failed opUid=%v uinqId=%v err=%s", opUid, uniqId, err)
		return err
	}

	var reqUid, payType, amount, dollerAmount uint32
	var currency string
	if storeUid.Valid {
		reqUid = uint32(storeUid.Int64)
	}
	if storePayType.Valid {
		payType = uint32(storePayType.Int64)
	}
	if storeAmount.Valid {
		amount = uint32(storeAmount.Int64)
	}
	if storeDollerAmount.Valid {
		dollerAmount = uint32(storeDollerAmount.Int64)
	}

	if storeCurrency.Valid {
		currency = storeCurrency.String
	}

	// Step2: 发起一个事务
	tx, err := this.db.Begin()
	if err != nil {
		this.infoLog.Printf("FailureAuditProcess db.Begin reqUid=%v uniqId=%v currency=%s payType=%v amount=%v dollerAmount=%v failed err=%s",
			reqUid,
			uniqId,
			currency,
			payType,
			amount,
			dollerAmount,
			err)
		return err
	}
	defer this.ClearTransaction(tx)

	// Step3: 更新HT_WITHDRAW_RECORD 的状态
	_, err = tx.Exec("update HT_WITHDRAW_RECORD SET applicat_stat = ?, op_uid = ?, serv_remark=?, withdraw_stat=?, update_time = UTC_TIMESTAMP() where id = ?;",
		string(newStatSlic),
		opUid,
		description,
		TYPE_WITHDRAW_FAILED,
		uniqId) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("FailureAuditProcess update HT_WITHDRAW_RECORD opUid=%v uniqId=%v newStatSlic=%s failed err=%s",
			opUid,
			uniqId,
			newStatSlic,
			err)
		return err
	}
	// Step4: 更新个人的用户余额
	_, err = tx.Exec("update HT_USER_BALANCE SET amount=amount+?, doller_amount=doller_amount+?, update_time=UTC_TIMESTAMP() where user_id = ?;",
		amount,
		dollerAmount,
		reqUid)
	if err != nil {
		this.infoLog.Printf("FailureAuditProcess update HT_USER_BALANCE reqUid=%v amount=%v dollerAmount=%v err=%s",
			reqUid,
			amount,
			dollerAmount,
			err)
		return err
	}
	// Step5: 插入一条交易记录
	_, err = tx.Exec("insert into HT_TRANSACTION_RECORD (req_uid, uinq_id, trans_type, pay_type, currency, amount, doller_amount, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP()) ;",
		reqUid,
		uniqId,
		TYPE_WITHDRAW_FAILED,
		payType,
		currency,
		amount,
		dollerAmount)
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord insert HT_TRANSACTION_RECORD reqUid=%v uniqId=%v transType=%v payType=%v currency=%s amount=%v dollerAmount=%v err=%s",
			reqUid,
			uniqId,
			TYPE_WITHDRAW_FAILED,
			payType,
			currency,
			amount,
			dollerAmount,
			err)
		return err
	}
	err = tx.Commit()
	if err != nil {
		this.infoLog.Printf("InsertWithdrawalsRecord tx.Commit reqUid=%v uniqId=%v transType=%v payType=%v currency=%s amount=%v dollerAmount=%v err=%s",
			reqUid,
			uniqId,
			TYPE_WITHDRAW_FAILED,
			payType,
			currency,
			amount,
			dollerAmount,
			err)
		return err
	}
	return nil
}

// 19、查询当个用户所有的交易记录 包括购课程收款、提现、提现失败记录
func (this *DbUtil) BatchGetTransationRecordByMinId(uid uint32, minUniqId uint64) (transationSlic []*TransationRecord, outMinId uint64, err error) {
	if this.db == nil || uid == 0 {
		this.infoLog.Printf("BatchGetTransationRecordByMinId uid=%v param error", uid)
		err = ErrDbParam
		return nil, outMinId, err
	}
	if minUniqId == 0 {
		minUniqId = MaxUniqId
		this.infoLog.Printf("BatchGetTransationRecordByMinId uid=%v after transformed minUniqId=%v", uid, minUniqId)
	}

	rows, err := this.db.Query("SELECT id, uinq_id, trans_type, pay_type, currency, amount, doller_amount, UNIX_TIMESTAMP(update_time) FROM HT_TRANSACTION_RECORD  WHERE req_uid=? AND id < ? ORDER BY id DESC LIMIT ?", uid, minUniqId, BatchGetCount)
	if err != nil {
		this.infoLog.Printf("BatchGetTransationRecordByMinId mysql query failed uid=%v err=%s", uid, err)
		return nil, outMinId, err
	}
	defer rows.Close()
	index := 0
	for rows.Next() {
		var id, uniqId, amount, dollerAmount, timeStamp uint64
		var transType, payType uint32
		var currency string
		if err := rows.Scan(&id, &uniqId, &transType, &payType, &currency, &amount, &dollerAmount, &timeStamp); err != nil {
			this.infoLog.Printf("BatchGetTransationRecordByMinId mysql get uid rows.Scan failed uid=%v index=%v err=%s", uid, index, err)
			continue
		}
		this.infoLog.Printf("index=%v id=%v uniqId=%v transType=%v payType=%v currency=%s amount=%s dollerAmount=%v timeStamp=%v",
			index,
			id,
			uniqId,
			transType,
			payType,
			currency,
			amount,
			dollerAmount,
			timeStamp)

		transRecord := &TransationRecord{
			Id:          id,
			TransType:   transType,
			UniqId:      uniqId,
			Currence:    currency,
			Amount:      amount,
			AccountType: payType,
			TransTime:   timeStamp + TS_SOFT,
		}
		transationSlic = append(transationSlic, transRecord)
		outMinId = id // 增加MaxId的值
		index = index + 1
	}
	return transationSlic, outMinId, nil
}

// 20、通过id查询单个收款流水
func (this *DbUtil) GetCollectMoneyRecordById(uniqId uint64) (collectMoneyRecord *CollectMoneyRecord, err error) {
	if this.db == nil || uniqId == 0 {
		this.infoLog.Printf("GetCollectMoneyRecordById uniqId=%v param error", uniqId)
		err = ErrDbParam
		return nil, err
	}
	var storePayUid, storeCollectUid, storeChargingId, storePayType, storeAmount, storePlatAmount, storeBalanceAmount, storeTimeStamp sql.NullInt64
	var storeTransId, storeRemark, storeCurrency sql.NullString
	// 查询uiqid 为id 的记录
	err = this.db.QueryRow("SELECT pay_uid, collect_uid, trans_id, charging_id, remark, pay_type, currency, amount, plat_amount, balance_amount, UNIX_TIMESTAMP(update_time) FROM HT_USER_COLLECT_FLOW  WHERE id= ?", uniqId).Scan(
		&storePayUid,
		&storeCollectUid,
		&storeTransId,
		&storeChargingId,
		&storeRemark,
		&storePayType,
		&storeCurrency,
		&storeAmount,
		&storePlatAmount,
		&storeBalanceAmount,
		&storeTimeStamp)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetCollectMoneyRecordById not found uniqId=%v in HT_USER_COLLECT_FLOW err=%s", uniqId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetCollectMoneyRecordById exec failed HT_USER_COLLECT_FLOW uniqId=%v err=%s", uniqId, err)
		return nil, err
	default:
	}

	collectMoneyRecord = &CollectMoneyRecord{
		UniqId:        uniqId,
		PayUid:        0,
		CollectUid:    0,
		CharginId:     0,
		Description:   "",
		Currence:      "",
		Amount:        0,
		PlatAmont:     0,
		TransId:       "",
		AccountType:   0,
		BalandeAmount: 0,
		TimeStamp:     0,
	}
	if storePayUid.Valid {
		collectMoneyRecord.PayUid = uint32(storePayUid.Int64)
	}

	if storeCollectUid.Valid {
		collectMoneyRecord.CollectUid = uint32(storeCollectUid.Int64)
	}

	if storeChargingId.Valid {
		collectMoneyRecord.CharginId = uint64(storeChargingId.Int64)
	}

	if storeRemark.Valid {
		collectMoneyRecord.Description = storeRemark.String
	}

	if storeCurrency.Valid {
		collectMoneyRecord.Currence = storeCurrency.String
	}
	if storeAmount.Valid {
		collectMoneyRecord.Amount = uint64(storeAmount.Int64)
	}
	if storePlatAmount.Valid {
		collectMoneyRecord.PlatAmont = uint64(storePlatAmount.Int64)
	}
	if storeTransId.Valid {
		collectMoneyRecord.TransId = storeTransId.String
	}
	if storePayType.Valid {
		collectMoneyRecord.AccountType = uint32(storePayType.Int64)
	}
	if storeBalanceAmount.Valid {
		collectMoneyRecord.BalandeAmount = uint64(storeBalanceAmount.Int64)
	}
	if storeTimeStamp.Valid {
		collectMoneyRecord.TimeStamp = uint64(storeTimeStamp.Int64 + TS_SOFT)
	}

	this.infoLog.Printf("id=%v payUid=%v collectUid=%v transId=%s chargingId=%v remark=%s payType=%v currency=%s amount=%v paltAmount=%v balanceAmount=%v timeStamp=%v",
		uniqId,
		collectMoneyRecord.PayUid,
		collectMoneyRecord.CollectUid,
		collectMoneyRecord.TransId,
		collectMoneyRecord.CharginId,
		collectMoneyRecord.Description,
		collectMoneyRecord.AccountType,
		collectMoneyRecord.Currence,
		collectMoneyRecord.Amount,
		collectMoneyRecord.PlatAmont,
		collectMoneyRecord.BalandeAmount,
		collectMoneyRecord.TimeStamp)

	return collectMoneyRecord, nil
}

// 21、通过id查询单个提现记录
func (this *DbUtil) GetWithdrawalsRecordById(uniqId uint64) (withdrawalsRecord *WithdrawalsRecord, err error) {
	if this.db == nil || uniqId == 0 {
		this.infoLog.Printf("GetWithdrawalsRecordById uniqId=%v param error", uniqId)
		err = ErrDbParam
		return nil, err
	}
	var storeReqUid, storeAmount, storePlatAmount, storePayType, storeBalanceAmount, storeTimeStamp sql.NullInt64
	var storeCurrency, storeRemark, storeServerRemark, storeApplicatStat, storeAccount, storeRealName sql.NullString
	// 查询uiqid 为id 的记录
	err = this.db.QueryRow("SELECT req_uid, currency, amount, plat_amount, remark, serv_remark, applicat_stat, pay_type, account, real_name, balance_amount, UNIX_TIMESTAMP(applicate_time) FROM HT_WITHDRAW_RECORD WHERE id= ?", uniqId).Scan(
		&storeReqUid,
		&storeCurrency,
		&storeAmount,
		&storePlatAmount,
		&storeRemark,
		&storeServerRemark,
		&storeApplicatStat,
		&storePayType,
		&storeAccount,
		&storeRealName,
		&storeBalanceAmount,
		&storeTimeStamp)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetWithdrawalsRecordById not found uniqId=%v in HT_WITHDRAW_RECORD err=%s", uniqId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetWithdrawalsRecordById exec failed HT_WITHDRAW_RECORD uniqId=%v err=%s", uniqId, err)
		return nil, err
	default:
	}

	var withdrawalsStat WithdrawalsStatList
	if storeApplicatStat.String != "" {
		err = json.Unmarshal([]byte(storeApplicatStat.String), &withdrawalsStat) // JSON to Struct
		if err != nil {
			attr := "gowalletdbd/json_unmarshal_count"
			libcomm.AttrAdd(attr, 1)
			this.infoLog.Printf("GetWithdrawalsRecordById uinqId=%v json.Unmarshal failed err=%s", uniqId, err)
		}
	}

	withdrawalsRecord = &WithdrawalsRecord{
		Id:            uniqId,
		ReqUid:        0,
		Currence:      "",
		Amount:        0,
		PlatAmont:     0,
		Remarks:       "",
		StatSlic:      withdrawalsStat.StatList,
		AccountType:   0,
		Account:       "",
		RealName:      "",
		BalandeAmount: 0,
		TimeStamp:     0,
		ServerRemarks: "",
	}
	if storeReqUid.Valid {
		withdrawalsRecord.ReqUid = uint32(storeReqUid.Int64)
	}

	if storeCurrency.Valid {
		withdrawalsRecord.Currence = storeCurrency.String
	}
	if storeAmount.Valid {
		withdrawalsRecord.Amount = uint64(storeAmount.Int64)
	}
	if storePlatAmount.Valid {
		withdrawalsRecord.PlatAmont = uint64(storePlatAmount.Int64)
	}
	if storeRemark.Valid {
		withdrawalsRecord.Remarks = storeRemark.String
	}

	if storeServerRemark.Valid {
		withdrawalsRecord.ServerRemarks = storeServerRemark.String
	}

	if storePayType.Valid {
		withdrawalsRecord.AccountType = uint32(storePayType.Int64)
	}
	if storeAccount.Valid {
		withdrawalsRecord.Account = storeAccount.String
	}
	if storeRealName.Valid {
		withdrawalsRecord.RealName = storeRealName.String
	}

	if storeBalanceAmount.Valid {
		withdrawalsRecord.BalandeAmount = uint64(storeBalanceAmount.Int64)
	}
	if storeTimeStamp.Valid {
		withdrawalsRecord.TimeStamp = uint64(storeTimeStamp.Int64 + TS_SOFT)
	}

	return withdrawalsRecord, nil
}

// 22、用户成功购买了某个课程
func (this *DbUtil) SuccessPurchaseCourseProcess(payUid uint32,
	nickName, headUrl, national string,
	payTs uint64,
	collectUid uint32,
	transationId, remark, currency string,
	chargingId, amount, dollerAmount, platAmount, platDollerAmount uint64,
	payType uint32) (err error) {
	if this.db == nil || payUid == 0 || collectUid == 0 || transationId == "" || currency == "" || chargingId == 0 || amount == 0 || dollerAmount == 0 {
		return ErrNilDbObject
	}
	this.infoLog.Printf("SuccessPurchaseCourseProcess payUid=%v collectUid=%v transationId=%s remark=%s currency=%s chargingId=%v payType=%v amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v err=%s",
		payUid,
		collectUid,
		transationId,
		remark,
		currency,
		chargingId,
		payType,
		amount,
		dollerAmount,
		platAmount,
		platDollerAmount)
	// Step1: 发起一个事务
	tx, err := this.db.Begin()
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess db.Begin payUid=%v collectUid=%v transationId=%s remark=%s currency=%s chargingId=%v payType=%v amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v err=%s",
			payUid,
			collectUid,
			transationId,
			remark,
			currency,
			chargingId,
			payType,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			err)
		return err
	}
	defer this.ClearTransaction(tx)

	// Step2: 更新收款方的账户余额
	_, err = tx.Exec("insert into HT_USER_BALANCE (user_id, currency, amount, doller_amount, update_time) VALUES (?, ?, ?, ?, UTC_TIMESTAMP()) ON DUPLICATE KEY UPDATE amount=amount+VALUES(amount), doller_amount=doller_amount+VALUES(doller_amount), update_time=UTC_TIMESTAMP();",
		collectUid,
		currency,
		amount,
		dollerAmount)
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess insert HT_USER_BALANCE userId=%v currency=%s amount=%v dollerAmount=%v err=%v",
			collectUid,
			currency,
			amount,
			dollerAmount,
			err)
		return err
	}
	// Step3: 查询收款方总的账户余额
	var storeTotalAmount, storeTotalDollerAmount sql.NullInt64
	err = tx.QueryRow("SELECT amount, doller_amount FROM HT_USER_BALANCE WHERE user_id = ?;", collectUid).Scan(&storeTotalAmount, &storeTotalDollerAmount)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("SuccessPurchaseCourseProcess not found uid=%v", collectUid)
		break
	case err != nil:
		this.infoLog.Printf("SuccessPurchaseCourseProcess exec failed uid=%v, err=%s", collectUid, err)
		break
	default:
		this.infoLog.Printf("SuccessPurchaseCourseProcess uid=%v totalAmount=%v totalDollerAmount=%v", collectUid, storeTotalAmount.Int64, storeTotalDollerAmount.Int64)
	}
	var totalAmount, totalDollerAmount uint64
	if storeTotalAmount.Valid {
		totalAmount = uint64(storeTotalAmount.Int64)
	}
	if storeTotalDollerAmount.Valid {
		totalDollerAmount = uint64(storeTotalDollerAmount.Int64)
	}

	// Step4: 将购买用户插入支付用户列表
	var storePayUserList sql.NullString
	err = tx.QueryRow("SELECT pay_uid_list FROM HT_COLLECT_MONEY_RECORD WHERE id=?;", chargingId).Scan(&storePayUserList)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("SuccessPurchaseCourseProcess not found uid=%v uinqId=%v in HT_COLLECT_MONEY_RECORD err=%s", payUid, chargingId, err)
		return err
	case err != nil:
		this.infoLog.Printf("SuccessPurchaseCourseProcess exec failed HT_COLLECT_MONEY_RECORD uid=%v uniqId=%v err=%s", payUid, chargingId, err)
		return err
	default:
	}
	payUserList := &PayUserList{}
	if storePayUserList.Valid && storePayUserList.String != "" {
		err = json.Unmarshal([]byte(storePayUserList.String), &payUserList) // JSON to Struct
		if err != nil {
			attr := "gowalletdbd/json_unmarshal_count"
			libcomm.AttrAdd(attr, 1)
			this.infoLog.Printf("SuccessPurchaseCourseProcess uid=%v uinqId=%v json.Unmarshal failed err=%s", payUid, chargingId, err)
		}
	}
	payUserList.UserList = append(payUserList.UserList, PayUserInfo{
		Uid:      payUid,
		NickName: nickName,
		HeadUrl:  headUrl,
		National: national,
		PayTs:    payTs,
	})

	payUserListSlic, err := json.Marshal(payUserList)
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess json.Marshal payUid=%v collectUid=%v transationId=%s remark=%s currency=%s chargingId=%v payType=%v amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v err=%s",
			payUid,
			collectUid,
			transationId,
			remark,
			currency,
			chargingId,
			payType,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			err)
		return err
	}

	_, err = tx.Exec("UPDATE HT_COLLECT_MONEY_RECORD SET pay_user_count=pay_user_count+1, pay_uid_list=?, update_time = UTC_TIMESTAMP() WHERE id=?;", payUserListSlic, chargingId)
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess UPDATE HT_COLLECT_MONEY_RECORD  payUid=%v collectUid=%v transId=%s chargingId=%v currency=%s amount=%v dollerAmount=%v err=%v",
			payUid,
			collectUid,
			transationId,
			chargingId,
			currency,
			amount,
			dollerAmount,
			err)
		return err
	}

	// Step5: 写入个人支付流水
	_, err = tx.Exec("insert into HT_USER_PAY_FLOW (pay_uid, collect_uid, trans_id, charging_id, currency, amount, doller_amount, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP());",
		payUid,
		collectUid,
		transationId,
		chargingId,
		currency,
		amount,
		dollerAmount)
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess insert HT_USER_PAY_FLOW  payUid=%v collectUid=%v transId=%s chargingId=%v currency=%s amount=%v dollerAmount=%v err=%v",
			payUid,
			collectUid,
			transationId,
			chargingId,
			currency,
			amount,
			dollerAmount,
			err)
		return err
	}

	// Step5: 写入个人收款流水
	res, err := tx.Exec("insert into HT_USER_COLLECT_FLOW (pay_uid, collect_uid, trans_id, charging_id, remark, pay_type, currency, amount, doller_amount, plat_amount, plat_doller_amount, balance_amount, balance_doller_amount, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP());",
		payUid,
		collectUid,
		transationId,
		chargingId,
		remark,
		payType,
		currency,
		amount,
		dollerAmount,
		platAmount,
		platDollerAmount,
		totalAmount,
		totalDollerAmount)
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess insert HT_USER_COLLECT_FLOW payUid=%v collectUid=%v transId=%s chargingId=%v remark=%s payType=%v currency=%s amount=%v dollerAmount=%v  platAmount=%v platDollerAmount=%v balanceAmount=%v balnaceDollerAmount=%v err=%v",
			payUid,
			collectUid,
			transationId,
			chargingId,
			remark,
			payType,
			currency,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			totalAmount,
			totalDollerAmount,
			err)
		return err
	}
	lastCollectId, err := res.LastInsertId()
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess HT_USER_COLLECT_FLOW res.LastInsertId failed payUid=%v collectUid=%v transationId=%s remark=%s currency=%s chargingId=%v payType=%v amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v err=%s",
			payUid,
			collectUid,
			transationId,
			remark,
			currency,
			chargingId,
			payType,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			err)
		return err
	}
	// Stpe6: 插入收款记录到交易表中
	_, err = tx.Exec("insert into HT_TRANSACTION_RECORD (req_uid, uinq_id, trans_type, pay_type, currency, amount, doller_amount, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP()) ;",
		collectUid,
		lastCollectId,
		TYPE_COLLECTION_MONEY,
		payType,
		currency,
		amount,
		dollerAmount)
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess insert HT_TRANSACTION_RECORD reqUid=%v uniqId=%v transType=%v payType=%v currency=%s amount=%v dollerAmount=%v err=%s",
			collectUid,
			chargingId,
			TYPE_COLLECTION_MONEY,
			payType,
			currency,
			amount,
			dollerAmount,
			err)
		return err
	}
	err = tx.Commit()
	if err != nil {
		this.infoLog.Printf("SuccessPurchaseCourseProcess insert HT_TRANSACTION_RECORD reqUid=%v uniqId=%v transType=%v payType=%v currency=%s amount=%v dollerAmount=%v err=%s",
			collectUid,
			chargingId,
			TYPE_COLLECTION_MONEY,
			payType,
			currency,
			amount,
			dollerAmount,
			err)
		return err
	}
	return nil
}

// 23、通过单个id查询收款的状态和自己是否已经购买
func (this *DbUtil) GetLessonAndPurchaseStatById(uid uint32, uniqId uint64) (stat uint32, isPurchase bool, err error) {
	if this.db == nil || uniqId == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetLessonAndPurchaseStatById uniqId=%v param error", uniqId)
		return stat, isPurchase, err
	}

	var storeStat sql.NullInt64
	var storePayUserList sql.NullString
	err = this.db.QueryRow("SELECT pay_uid_list, stat FROM HT_COLLECT_MONEY_RECORD WHERE id=?;", uniqId).Scan(
		&storePayUserList,
		&storeStat)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetLessonAndPurchaseStatById not found uid=%v uinqId=%v in HT_COLLECT_MONEY_RECORD err=%s", uid, uniqId, err)
		return stat, isPurchase, err
	case err != nil:
		this.infoLog.Printf("GetLessonAndPurchaseStatById exec failed HT_COLLECT_MONEY_RECORD uid=%v uniqId=%v err=%s", uid, uniqId, err)
		return stat, isPurchase, err
	default:
	}
	payUserList := &PayUserList{}
	if storePayUserList.Valid {
		err = json.Unmarshal([]byte(storePayUserList.String), &payUserList) // JSON to Struct
		if err != nil {
			attr := "gowalletdbd/json_unmarshal_count"
			libcomm.AttrAdd(attr, 1)
			this.infoLog.Printf("GetLessonAndPurchaseStatById uid=%v uinqId=%v json.Unmarshal failed err=%s", uid, uniqId, err)
		}
	}
	if storeStat.Valid {
		stat = uint32(storeStat.Int64)
	}
	isPurchase = false
	for _, v := range payUserList.UserList {
		if uid == v.Uid {
			isPurchase = true
			this.infoLog.Printf("GetLessonAndPurchaseStatById uid=%v uinqId=%v hase purchaed", uid, uniqId)
			break
		}
	}
	return stat, isPurchase, nil
}

// 24、通过单个群id查询是否存在群课程正在收费中
func (this *DbUtil) CheckExistLessonCharginByRoomId(uid uint32, roomId uint32) (isExist bool, err error) {
	if this.db == nil || roomId == 0 {
		err = ErrDbParam
		this.infoLog.Printf("CheckExistLessonCharginByRoomId roomId=%v param error", roomId)
		return isExist, err
	}

	// Step1: 插入用户收款记录到数据库中
	var storeCount sql.NullInt64
	err = this.db.QueryRow("SELECT COUNT(id) FROM HT_COLLECT_MONEY_RECORD WHERE room_id=? AND stat=0;", roomId).Scan(&storeCount) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("CheckExistLessonCharginByRoomId SELECT COUNT(id) FROM HT_USER_WALLET_ACCOUNT uid=%v roomId=%v failed err=%s",
			uid,
			roomId,
			err)
		return isExist, err
	}
	var count int64
	if storeCount.Valid {
		count = storeCount.Int64
	}
	this.infoLog.Printf("CheckExistLessonCharginByRoomId count=%v roomId=%v", count, roomId)
	if count > 0 {
		isExist = true
	}
	return isExist, nil
}

// 25、查询单个提现账号
func (this *DbUtil) GetWithdrawalsAccountById(uid uint32, uniqId uint64) (account *WithdrawalsAccount, err error) {
	if this.db == nil || uid == 0 || uniqId == 0 {
		this.infoLog.Printf("GetWithdrawalsAccountById uid=%v uniqId=%v param error", uid, uniqId)
		err = ErrDbParam
		return nil, err
	}
	this.infoLog.Printf("GetWithdrawalsAccountById uid=%v uniqId=%v", uid, uniqId)
	var storeAccountType, storeUpdateTs sql.NullInt64
	var storeRealName, storeAccount, storeRemarks sql.NullString
	err = this.db.QueryRow("SELECT account_type, real_name, account, remark, UNIX_TIMESTAMP(update_time) FROM HT_USER_WALLET_ACCOUNT WHERE id=? AND user_id=?", uniqId, uid).Scan(
		&storeAccountType,
		&storeRealName,
		&storeAccount,
		&storeRemarks,
		&storeUpdateTs)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetWithdrawalsAccountById not found uid=%v uinqId=%v in HT_USER_WALLET_ACCOUNT err=%s", uid, uniqId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetWithdrawalsAccountById exec failed HT_USER_WALLET_ACCOUNT uid=%v uniqId=%v err=%s", uid, uniqId, err)
		return nil, err
	default:
	}

	account = &WithdrawalsAccount{
		UniqId: uniqId,
	}
	if storeAccountType.Valid {
		account.AccountType = uint32(storeAccountType.Int64)
	}

	if storeUpdateTs.Valid {
		account.UpdateTime = uint64(storeUpdateTs.Int64)
	}

	if storeRealName.Valid {
		account.RealName = storeRealName.String
	}
	if storeAccount.Valid {
		account.Account = storeAccount.String
	}
	if storeRemarks.Valid {
		account.RemarkInfo = storeRemarks.String
	}
	return account, nil
}

// 26、2018-02-05 通过课程id查询课程当前是否正在收费中
func (this *DbUtil) CheckExistLessonCharginByLessonObid(uid uint32, roomId uint32, lessonObid string) (isExist bool, err error) {
	if this.db == nil || roomId == 0 || lessonObid == "" {
		err = ErrDbParam
		this.infoLog.Printf("CheckExistLessonCharginByLessonObid roomId=%v uid=%v lessonObid=%s param error", roomId, uid, lessonObid)
		return isExist, err
	}

	// Step1: 插入用户收款记录到数据库中
	var storeCount sql.NullInt64
	err = this.db.QueryRow("SELECT COUNT(id) FROM HT_COLLECT_MONEY_RECORD WHERE room_id=? AND lesson_obid = ? AND stat=0;", roomId, lessonObid).Scan(&storeCount) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("CheckExistLessonCharginByLessonObid SELECT COUNT(id) FROM HT_USER_WALLET_ACCOUNT uid=%v roomId=%v lessonObid=%s failed err=%s",
			uid,
			roomId,
			lessonObid,
			err)
		return isExist, err
	}
	var count int64
	if storeCount.Valid {
		count = storeCount.Int64
	}
	this.infoLog.Printf("CheckExistLessonCharginByLessonObid count=%v roomId=%v storeCount.Valid=%v value=%v", count, roomId, storeCount.Valid, storeCount.Int64)
	if count > 0 {
		isExist = true
	}
	return isExist, nil
}

// 27、2018-02-27 提现转账成功之后调用此接口
func (this *DbUtil) WithdrawalsSuccTransfer(uniqId, amount, dollerAmount uint64, accountType uint32, account string, transUid uint32) (err error) {
	if this.db == nil || transUid == 0 || uniqId == 0 || amount == 0 || dollerAmount == 0 {
		this.infoLog.Printf("WithdrawalsSuccTransfer uniqId=%v amount=%v dollerAmount=%v accountType=%v account=%s transUid=%v param error",
			uniqId,
			amount,
			dollerAmount,
			accountType,
			account,
			transUid)
		err = ErrDbParam
		return err
	}
	this.infoLog.Printf("WithdrawalsSuccTransfer uniqId=%v amount=%v dollerAmount=%v accountType=%v account=%s transUid=%v ",
		uniqId,
		amount,
		dollerAmount,
		accountType,
		account,
		transUid)
	// Step1: 首先去除原来的状态添加一个新的状态到队列的末端
	var storeStat sql.NullString
	err = this.db.QueryRow("SELECT applicat_stat FROM HT_WITHDRAW_RECORD WHERE id=?;", uniqId).Scan(&storeStat)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("WithdrawalsSuccTransfer not found uinqId=%v in HT_WITHDRAW_RECORD err=%s", uniqId, err)
		return err
	case err != nil:
		this.infoLog.Printf("WithdrawalsSuccTransfer exec failed HT_WITHDRAW_RECORD uniqId=%v err=%s", uniqId, err)
		return err
	default:
	}
	// json 反序列化成结构体
	withdrawalsStat := WithdrawalsStatList{}
	err = json.Unmarshal([]byte(storeStat.String), &withdrawalsStat)
	if err != nil {
		this.infoLog.Printf("WithdrawalsSuccTransfer json.Unmarshal failed transUid=%v uinqId=%v storeStat=%s err=%s", transUid, uniqId, storeStat.String, err)
		return err
	}
	item := WithdrawalsStat{
		Stat:      TYPE_WITHDRAW_SUCC,
		OpUid:     transUid,
		TimeStamp: uint64(time.Now().Unix()),
	}
	withdrawalsStat.StatList = append(withdrawalsStat.StatList, item)
	// 将新的数组序列化成string
	newStatSlic, err := json.Marshal(withdrawalsStat)
	if err != nil {
		this.infoLog.Printf("WithdrawalsSuccTransfer json.Marshal failed transUid=%v uinqId=%v err=%s", transUid, uniqId, err)
		return err
	}
	// Step2: 更新数据库提现的状态
	_, err = this.db.Exec("UPDATE HT_WITHDRAW_RECORD SET applicat_stat = ?, withdraw_stat=?, transfer_amount = ?, transfer_doller_amount=?, transfer_type=?, transfer_account=?, transfer_uid=?, update_time = UTC_TIMESTAMP() where id = ?;",
		string(newStatSlic),
		TYPE_WITHDRAW_SUCC,
		amount,
		dollerAmount,
		accountType,
		account,
		transUid,
		uniqId) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("WithdrawalsSuccTransfer update HT_WITHDRAW_RECORD uniqId=%v amount=%v dollerAmount=%v accountType=%v account=%s transUid=%v error=%s",
			uniqId,
			amount,
			dollerAmount,
			accountType,
			account,
			transUid,
			err)
		return err
	}
	return nil
}

// 28、通过群id和课程obid查询正在收费的课程项详情
func (this *DbUtil) GetChargingInfoByLessonObidAndRoomId(roomId uint32, lessonObid string) (chargingInfoList []*ChargingInfo, err error) {
	if this.db == nil || roomId == 0 || len(lessonObid) == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetChargingInfoById roomId=%v lessonObid=%s param error", roomId, lessonObid)
		return nil, err
	}
	var stat uint32
	stat = 0
	rows, err := this.db.Query("SELECT id, user_id, lesson_from, lesson_title, currency, amount, `desc`, room_name, pay_user_count, pay_uid_list, UNIX_TIMESTAMP(create_time) FROM HT_COLLECT_MONEY_RECORD WHERE room_id=? AND lesson_obid = ? AND stat=?;", roomId, lessonObid, stat)
	if err != nil {
		this.infoLog.Printf("GetChargingInfoByLessonObidAndRoomId mysql query failed roomId=%v lessonObid=%s err=%s", roomId, lessonObid, err)
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var userId, from, payUserCount uint32
		var id, amount, createTime uint64
		var lessonTitle, currency, description, roomName, payUserListStr string
		if err := rows.Scan(&id, &userId, &from, &lessonTitle, &currency, &amount, &description, &roomName, &payUserCount, &payUserListStr, &createTime); err != nil {
			this.infoLog.Printf("GetChargingInfoByLessonObidAndRoomId mysql get rows.Scan failed roomId=%v lessonObid=%s err=%s", roomId, lessonObid, err)
			continue
		}
		this.infoLog.Printf("GetChargingInfoByLessonObidAndRoomId id=%v user_id=%v lesson_from=%v lesson_title=%s currency=%s amount=%v description=%s room_name=%s pay_user_count=%v pay_uid_list=%s stat=%v creat_time=%v room_id=%v lesson_obid=%s",
			id,
			userId,
			from,
			lessonTitle,
			currency,
			amount,
			description,
			roomName,
			payUserCount,
			payUserListStr,
			stat,
			createTime,
			roomId,
			lessonObid)
		payUserList := &PayUserList{}
		err = json.Unmarshal([]byte(payUserListStr), &payUserList) // JSON to Struct
		if err != nil {
			attr := "gowalletdbd/json_unmarshal_count"
			libcomm.AttrAdd(attr, 1)
			this.infoLog.Printf("GetChargingInfoByLessonObidAndRoomId roomId=%v lessonObid=%s json.Unmarshal failed err=%s", roomId, lessonObid, err)
		}
		chargingInfo := &ChargingInfo{
			ChargingId:   id,
			OpUid:        userId,
			PaymentFrom:  from,
			LessonObid:   lessonObid,
			LessonTitle:  lessonTitle,
			Currence:     currency,
			Amount:       amount,
			Description:  description,
			RoomId:       roomId,
			RoomName:     roomName,
			PayUserCount: payUserCount,
			PayUserList:  payUserList.UserList,
			ChargingStat: stat,
			CreateTime:   createTime,
		}
		chargingInfoList = append(chargingInfoList, chargingInfo)
	}

	return chargingInfoList, nil
}

// 29、更改提现的平台费率对应的修改平台金额和对应的平台美元金额
func (this *DbUtil) UpdateWithdrawalsRate(opUid uint32, uniqId uint64, withdrawalsRate float64, platAmount, platDollerAmount uint64) (err error) {
	if this.db == nil || opUid == 0 || uniqId == 0 {
		this.infoLog.Printf("UpdateWithdrawalsRate opUid=%v uniqId=%v param error", opUid, uniqId)
		err = ErrDbParam
		return err
	}
	this.infoLog.Printf("UpdateWithdrawalsRate opUid=%v uniqId=%v rate=%v platAmount=%v platDollerAmount=%v",
		opUid,
		uniqId,
		withdrawalsRate,
		platAmount,
		platDollerAmount)

	// Step2: 更新数据库提现的状态
	_, err = this.db.Exec("UPDATE HT_WITHDRAW_RECORD SET plat_amount = ?, plat_doller_amount = ?, plat_amount_rate = ?, update_time = UTC_TIMESTAMP() where id = ?;",
		platAmount,
		platDollerAmount,
		withdrawalsRate,
		uniqId) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("UpdateWithdrawalsRate update HT_WITHDRAW_RECORD opUid=%v uniqId=%v rate=%v platAmount=%v platDollerAmount=%v failed err=%s",
			opUid,
			uniqId,
			withdrawalsRate,
			platAmount,
			platDollerAmount,
			err)
		return err
	}
	return nil
}

//
