package main

import (
	"database/sql"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_wallet"
	"github.com/HT_GOGO/gotcp/tcpfw/project/wallet/wallet_dbd/util"

	"log"
	"math"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog                *log.Logger
	dbUtil                 *util.DbUtil
	writeAgentMastreApi    *common.SrvToSrvApiV3
	writeAgentSlaveApi     *common.SrvToSrvApiV3
	ticker                 *time.Ticker
	mucApi                 *common.MucApi
	globalRate             map[string]util.ExchangeRateInfo
	platFeeRate            float64
	withdrawalsPlatFeeRate float64
	checkInterval          int
	limitDollerAmount      uint64 //为美元*1000
	limitDeadLineLimit     uint32 //提现限制天数
	userInfoCacheApi       *common.UserInfoCacheApi
	supportCurrency        []string
)

const (
	ProcSlowThreshold = 300000
	AdminUid          = 10000
	DollerCurrency    = "USD"
	JPYCurrency       = "JPY"
	DollerSymbol      = "$"
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Printf("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v from=%v", head.Cmd, head.Seq, head.From)
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gowalletdbd/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch ht_wallet.WALLET_CMD_TYPE(head.Cmd) {
	case ht_wallet.WALLET_CMD_TYPE_CMD_START_CHARGING_REQ:
		go ProcStartCharging(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_CHARGING_INFO_BY_ID_REQ:
		go ProcGetChargingInfoById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_STOP_CHARGING_REQ:
		go ProcStopChargingById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_CHARGING_STAT_REQ:
		go ProcQueryChargingStatById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_GET_CHARGIN_INFO_REQ:
		go ProcBatchGetChargingInfo(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_PAY_USER_LIST_BY_CHARGING_ID_REQ:
		go ProcGetPayUserListById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_BALANCE_INF_BY_UID_REQ:
		go ProcGetBalanceByUid(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_GET_COLLECT_RECORD_REQ:
		go ProcBatchGetCollectMoneyRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_GET_WITHDRAWALS_RECORD_REQ:
		go ProcBatchGetWithdrawalsRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_EXCHANGE_REATE_REQ:
		go ProcQueryExchageRate(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_WITHDRAWALS_REQ:
		go ProcWithdrawalsReq(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_ADD_WITHDRAWALS_ACCOUNT_REQ:
		go ProcAddWithdrawalsAccount(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_MODIFY_WITHDRAWALS_ACCOUNT_REQ:
		go ProcModifyWithdrawalsAccount(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_DEL_WITHDRAWALS_ACCOUNT_REQ:
		go ProcDelWithdrawalsAccount(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_WITHDRAWALS_ACCOUNT_REQ:
		go ProcGetAllWithdrawalsAccount(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_ALL_TRANS_RECORD_REQ:
		go ProcBatchGetTransationRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_TRANS_DETAIL_INFO_BY_ID_REQ:
		go ProcGetTransationDetailInfo(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_WITHDRAWALS_RECORD_BY_ID_REQ:
		go ProcWithdrawalsRecordById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_LESSON_AND_PURCHASE_STAT_REQ:
		go ProcQueryLessonAndPurchaseStat(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_SUCC_PURCHASE_LESSON_REQ:
		go ProcSuccessPurchaseLesson(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_ROOM_EXIST_LESSON_CHARGE_REQ:
		go ProcQueryRoomExistLessonCharging(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_AUDIT_WITHDRAWALS_RECORD_REQ:
		go ProcAuditWithdrawalsRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_WITHDRAWALS_ACCOUNT_BY_ID_REQ:
		go ProcGetWithdrawalsAccountById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_LESSON_IS_CHARGE_REQ:
		go ProcQueryLessonIsCharging(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_SUCC_TRANSFER_REQ:
		go ProcTransferSuccRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_UPDATE_EXCHANGE_RATE_REQ:
		go ProcUpdateExchangeRate(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_STOP_LESSON_CHARGING:
		go ProcBatchStopLessonCharge(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_UPDATE_WITHDRAWALS_RATE_REQ:
		go ProcUpdateWithdrawalsRate(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
		c.Close()
	}
	return true
}

// 1.proc store moment info
func ProcStartCharging(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStartCharging not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStartCharging invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletdbd/start_charge_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStartCharging proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	// Step2: 判断请求对象是否为空
	subReqBody := reqBody.GetStartChargingReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcStartCharging GetStartChargingReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	infoLog.Printf("ProcStartCharging opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s",
		subReqBody.GetOpUid(),
		subReqBody.GetPaymentFrom(),
		subReqBody.GetLessonObid(),
		subReqBody.GetPaymentTitle(),
		subReqBody.GetPayCurrency().GetCurrencyType(),
		subReqBody.GetPayCurrency().GetAmount(),
		subReqBody.GetCollectionDescription(),
		subReqBody.GetRoomId(),
		subReqBody.GetRoomName())

	// Step3: 将记录写入数据库
	lastId, err := dbUtil.InsertChargingRecord(subReqBody.GetOpUid(),
		uint32(subReqBody.GetPaymentFrom()),
		subReqBody.GetLessonObid(),
		subReqBody.GetPaymentTitle(),
		subReqBody.GetPayCurrency().GetCurrencyType(),
		subReqBody.GetPayCurrency().GetAmount(),
		subReqBody.GetCollectionDescription(),
		subReqBody.GetRoomId(),
		subReqBody.GetRoomName())
	// Step4: 插入失败返回错误
	if err != nil {
		infoLog.Printf("ProcStartCharging opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s err=%s",
			subReqBody.GetOpUid(),
			subReqBody.GetPaymentFrom(),
			subReqBody.GetLessonObid(),
			subReqBody.GetPaymentTitle(),
			subReqBody.GetPayCurrency().GetCurrencyType(),
			subReqBody.GetPayCurrency().GetAmount(),
			subReqBody.GetCollectionDescription(),
			subReqBody.GetRoomId(),
			subReqBody.GetRoomName(),
			err)
		attr := "gowalletdbd/start_charge_insert_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("insert failed"),
			},
		}
		return false
	}
	// Step5: 插入成功返回成功
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ChargingId: proto.Uint64(uint64(lastId)),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// 发送请求到MUC广播开始群课程收费
	if subReqBody.GetRoomId() != 0 {
		msgId := fmt.Sprintf("MSG_%v", time.Now().UnixNano()/1000000)
		// 将用户的收款金额转换成对应的美元
		rate, symbol, err := GetCurrencyRate(subReqBody.GetPayCurrency().GetCurrencyType())
		if err == nil {
			dollerAmount := uint64(float64(subReqBody.GetPayCurrency().GetAmount()) * (float64(rate) / 10000))
			infoLog.Printf("ProcStartCharging opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v rate=%v dollerAmount=%v",
				subReqBody.GetOpUid(),
				subReqBody.GetPaymentFrom(),
				subReqBody.GetLessonObid(),
				subReqBody.GetPaymentTitle(),
				subReqBody.GetPayCurrency().GetCurrencyType(),
				subReqBody.GetPayCurrency().GetAmount(),
				rate,
				dollerAmount)
			rateMap := make(map[string]uint64)
			for _, v := range supportCurrency {
				if rate, ok := globalRate[v]; ok {
					rateMap[v] = rate.Rate
				} else {
					rateInfo, err := dbUtil.GetExchangeRate(v)
					if err == nil {
						rateMap[v] = rateInfo.Rate
					} else {
						infoLog.Printf("ProcStartCharging currency=%s not foun rate", v)
					}
				}
			}
			code, err := mucApi.BroadCastStatLessonChange(subReqBody.GetRoomId(),
				subReqBody.GetOpUid(),
				subReqBody.GetNickName(),
				uint64(lastId),
				subReqBody.GetPayCurrency().GetCurrencyType(),
				subReqBody.GetPayCurrency().GetAmount(),
				dollerAmount,
				symbol,
				subReqBody.GetPaymentTitle(),
				subReqBody.GetLessonObid(),
				msgId,
				rateMap)
			infoLog.Printf("ProcStartCharging opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s muc retCod=%v err=%s",
				subReqBody.GetOpUid(),
				subReqBody.GetPaymentFrom(),
				subReqBody.GetLessonObid(),
				subReqBody.GetPaymentTitle(),
				subReqBody.GetPayCurrency().GetCurrencyType(),
				subReqBody.GetPayCurrency().GetAmount(),
				subReqBody.GetCollectionDescription(),
				subReqBody.GetRoomId(),
				subReqBody.GetRoomName(),
				code,
				err)
			if code != 0 {
				attr := "gowalletdbd/bc_start_charge_failed"
				libcomm.AttrAdd(attr, 1)
			}
		} else {
			attr := "gowalletdbd/bc_start_charge_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcStartCharging opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s find rate failed err=%d",
				subReqBody.GetOpUid(),
				subReqBody.GetPaymentFrom(),
				subReqBody.GetLessonObid(),
				subReqBody.GetPaymentTitle(),
				subReqBody.GetPayCurrency().GetCurrencyType(),
				subReqBody.GetPayCurrency().GetAmount(),
				subReqBody.GetCollectionDescription(),
				subReqBody.GetRoomId(),
				subReqBody.GetRoomName(),
				err)
		}

	}

	// 计算耗时
	// 调用user_info_cache 接口更新用户创建群聊的时间戳
	err = userInfoCacheApi.UpdateUserWalletVer(subReqBody.GetOpUid())
	if err != nil {
		infoLog.Printf("ProcStartCharging opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s userInfoCacheApi.UpdateUserWalletVer failed err=%s",
			subReqBody.GetOpUid(),
			subReqBody.GetPaymentFrom(),
			subReqBody.GetLessonObid(),
			subReqBody.GetPaymentTitle(),
			subReqBody.GetPayCurrency().GetCurrencyType(),
			subReqBody.GetPayCurrency().GetAmount(),
			subReqBody.GetCollectionDescription(),
			subReqBody.GetRoomId(),
			subReqBody.GetRoomName(),
			err)
	}

	// 插入空的记录到mysql的HT_USER_BALANCE表中
	err = dbUtil.InsertBalanceRecord(subReqBody.GetOpUid(), subReqBody.GetPayCurrency().GetCurrencyType(), 0, 0)
	if err != nil {
		infoLog.Printf("ProcStartCharging opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s dbUtil.InsertBalanceRecord failed err=%s",
			subReqBody.GetOpUid(),
			subReqBody.GetPaymentFrom(),
			subReqBody.GetLessonObid(),
			subReqBody.GetPaymentTitle(),
			subReqBody.GetPayCurrency().GetCurrencyType(),
			subReqBody.GetPayCurrency().GetAmount(),
			subReqBody.GetCollectionDescription(),
			subReqBody.GetRoomId(),
			subReqBody.GetRoomName(),
			err)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		attr := "gowalletdbd/start_charge_proc_slow"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcStartCharging opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s",
			subReqBody.GetOpUid(),
			subReqBody.GetPaymentFrom(),
			subReqBody.GetLessonObid(),
			subReqBody.GetPaymentTitle(),
			subReqBody.GetPayCurrency().GetCurrencyType(),
			subReqBody.GetPayCurrency().GetAmount(),
			subReqBody.GetCollectionDescription(),
			subReqBody.GetRoomId(),
			subReqBody.GetRoomName())
	}
	return true
}

// 2、通过唯一标识符id查询收款的详情
func ProcGetChargingInfoById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetChargingInfoById not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetChargingInfoById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletdbd/get_charge_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetChargingInfoById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	// Step2:检查子对象是否为空
	subReqBody := reqBody.GetQueryChargingInfoByIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetChargingInfoById GetQueryChargingInfoByIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	chargingId := subReqBody.GetChargingId()
	infoLog.Printf("ProcGetChargingInfoById recv req reqUid=%v chargingId=%v", reqUid, chargingId)

	// Step4:执行db操作
	chargingInfo, err := dbUtil.GetChargingInfoById(reqUid, chargingId)
	if err != nil {
		infoLog.Printf("ProcGetChargingInfoById recv req reqUid=%v chargingId=%v err=%s", reqUid, chargingId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	var payUserList []*ht_wallet.PayUserInfo
	for _, v := range chargingInfo.PayUserList {
		item := &ht_wallet.PayUserInfo{
			Uid:      proto.Uint32(v.Uid),
			NickName: []byte(v.NickName),
			HeadUrl:  []byte(v.HeadUrl),
			PayTs:    proto.Uint64(v.PayTs),
			National: []byte(v.National),
		}
		payUserList = append(payUserList, item)
	}
	outRate, symbol, err := GetCurrencyRate(chargingInfo.Currence)
	if err != nil {
		infoLog.Printf("ProcGetChargingInfoById recv req reqUid=%v chargingId=%v GetCurrencyRate failed err=%s",
			reqUid,
			chargingId,
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get currence rate failed"),
			},
		}
		return false
	}
	dollerAmount := uint64(chargingInfo.Amount * outRate / 10000)
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		CharginInfo: &ht_wallet.ChargingInfo{
			ChargingId:  proto.Uint64(chargingInfo.ChargingId),
			OpUid:       proto.Uint32(chargingInfo.OpUid),
			PaymentFrom: ht_wallet.PAYMENT_FROM_TYPE(chargingInfo.PaymentFrom).Enum(),
			LessonObid:  proto.String(chargingInfo.LessonObid),
			LessonTitle: proto.String(chargingInfo.LessonTitle),
			PayCurrency: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(chargingInfo.Currence),
				Amount:              proto.Uint64(chargingInfo.Amount),
				CorrespondingDollar: proto.Uint64(dollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			CollectionDescription: proto.String(chargingInfo.Description),
			RoomId:                proto.Uint32(chargingInfo.RoomId),
			RoomName:              proto.String(chargingInfo.RoomName),
			PayUserCount:          proto.Uint32(chargingInfo.PayUserCount),
			PayUserList:           payUserList,
			ChargingStat:          ht_wallet.ChargingStat(chargingInfo.ChargingStat).Enum(),
			CreateTime:            proto.Uint64(chargingInfo.CreateTime),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// 计时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/get_charge_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 3、通过唯一标识符id停止收款
func ProcStopChargingById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStopChargingById not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStopChargingById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/stop_charge_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStopChargingById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStopChargingReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcStopChargingById GetStopChargingReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	chargingId := subReqBody.GetChargingId()
	infoLog.Printf("ProcStopChargingById reqUid=%v chargingId=%v", reqUid, chargingId)
	// Step4:执行db操作
	err = dbUtil.StopChargingById(reqUid, chargingId)
	if err != nil {
		infoLog.Printf("ProcStopChargingById reqUid=%v chargingId=%v err=%s", reqUid, chargingId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// 发送请求到MUC广播结束群课程收费
	chargeInfo, err := dbUtil.GetChargingInfoById(reqUid, chargingId)
	if err != nil {
		infoLog.Printf("ProcStopChargingById dbUtil.GetChargingInfoById failed reqUid=%v chargingId=%v err=%s", reqUid, chargingId, err)
		attr := "gowalletdbd/get_charge_info_failed"
		libcomm.AttrAdd(attr, 1)
	} else {
		if chargeInfo.RoomId != 0 {
			msgId := fmt.Sprintf("MSG_%v", time.Now().UnixNano()/1000000)
			// 将用户的收款金额转换成对应的美元
			rate, symbol, err := GetCurrencyRate(chargeInfo.Currence)
			if err == nil {
				dollerAmount := uint64(float64(chargeInfo.Amount) * float64(rate) / 10000)
				code, err := mucApi.BroadCastStopLessonChange(chargeInfo.RoomId,
					chargeInfo.OpUid,
					subReqBody.GetNickName(),
					chargeInfo.ChargingId,
					chargeInfo.Currence,
					chargeInfo.Amount,
					dollerAmount,
					symbol,
					chargeInfo.LessonTitle,
					chargeInfo.LessonObid,
					msgId,
					true)
				infoLog.Printf("ProcStopChargingById opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s muc retCod=%v err=%s",
					chargeInfo.OpUid,
					chargeInfo.PaymentFrom,
					chargeInfo.LessonObid,
					chargeInfo.LessonTitle,
					chargeInfo.Currence,
					chargeInfo.Amount,
					chargeInfo.Description,
					chargeInfo.RoomId,
					chargeInfo.RoomName,
					code,
					err)
				if code != 0 {
					attr := "gowalletdbd/bc_stop_charge_failed"
					libcomm.AttrAdd(attr, 1)
				}
			} else {
				attr := "gowalletdbd/bc_stop_charge_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcStopChargingById opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s get rate failed err=%s",
					chargeInfo.OpUid,
					chargeInfo.PaymentFrom,
					chargeInfo.LessonObid,
					chargeInfo.LessonTitle,
					chargeInfo.Currence,
					chargeInfo.Amount,
					chargeInfo.Description,
					chargeInfo.RoomId,
					chargeInfo.RoomName,
					err)
			}
		}
	}
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/stop_charge_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 4、查询单个收费记录的状态
func ProcQueryChargingStatById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryChargingStatById not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryChargingStatById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/query_charge_stat_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryChargingStatById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryChargingStatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryChargingStatById GetQueryChargingStatRspbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	chargingId := subReqBody.GetChargingId()
	infoLog.Printf("ProcQueryChargingStatById reqUid=%v chargingId=%v", reqUid, chargingId)
	// Step4:执行db操作
	stat, err := dbUtil.GetChargingStatById(reqUid, chargingId)
	if err != nil {
		infoLog.Printf("ProcQueryChargingStatById reqUid=%v chargingId=%v err=%s", reqUid, chargingId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("get req body failed"),
		},
		ChargingStat: ht_wallet.ChargingStat(stat).Enum(),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/query_charge_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 5、批量查询所有的收款记录
func ProcBatchGetChargingInfo(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetChargingInfo not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetChargingInfo invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/bathc_get_charge_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetChargingInfo proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetChargingInfoListReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetChargingInfo GetBatchGetChargingInfoListReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	minChargingId := subReqBody.GetMinChargingId()
	infoLog.Printf("ProcBatchGetChargingInfo reqUid=%v minChargingId=%v", reqUid, minChargingId)
	if reqUid == 0 {
		infoLog.Printf("ProcBatchGetChargingInfo reqUid=%v minChargingId=%v", reqUid, minChargingId)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step4:执行db操作
	simpleInfoSlic, outMinId, err := dbUtil.BatchGetChargingInfoByMinId(reqUid, minChargingId)
	if err != nil {
		infoLog.Printf("ProcBatchGetChargingInfo reqUid=%v minChargingId=%v err=%s", reqUid, minChargingId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	var chargingInfoslic []*ht_wallet.ChargingSimpleInfo
	for _, v := range simpleInfoSlic {
		var dollerAmount uint64
		rate, symbol, err := GetCurrencyRate(v.Currence)
		if err != nil {
			attr := "gowalletdbd/get_currency_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcBatchGetChargingInfo reqUid=%v minChargingId=%v GetCurrencyRate currency=%s failed err=%s",
				reqUid,
				minChargingId,
				v.Currence,
				err)
		} else {
			dollerAmount = v.Amount * rate / 10000
		}
		item := &ht_wallet.ChargingSimpleInfo{
			ChargingId:  proto.Uint64(v.ChargingId),
			OpUid:       proto.Uint32(v.OpUid),
			PaymentFrom: ht_wallet.PAYMENT_FROM_TYPE(v.PaymentFrom).Enum(),
			LessonTitle: proto.String(v.LessonTitle),
			PayCurrency: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(v.Currence),
				Amount:              proto.Uint64(v.Amount),
				CorrespondingDollar: proto.Uint64(dollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			CollectionDescription: proto.String(v.Description),
			RoomId:                proto.Uint32(v.RoomId),
			RoomName:              proto.String(v.RoomName),
			PayUserCount:          proto.Uint32(v.PayUserCount),
			ChargingStat:          ht_wallet.ChargingStat(v.ChargingStat).Enum(),
			CreateTime:            proto.Uint64(v.CreateTime),
		}
		chargingInfoslic = append(chargingInfoslic, item)
	}

	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		CharginInfoList: chargingInfoslic,
		MinChargingId:   proto.Uint64(outMinId),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/bathc_get_charge_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 6、获取单个收款记录的付费用户列表
func ProcGetPayUserListById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetPayUserListById not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetPayUserListById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/get_pay_user_list_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetPayUserListById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryPayUserListByChargingIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetPayUserListById GetQueryPayUserListByChargingIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	chargingId := subReqBody.GetChargingId()
	infoLog.Printf("ProcGetPayUserListById reqUid=%v chargingId=%v", reqUid, chargingId)
	if reqUid == 0 || chargingId == 0 {
		infoLog.Printf("ProcGetPayUserListById reqUid=%v chargingId=%v input param error", reqUid, chargingId)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step4:执行db操作
	payUidSlic, err := dbUtil.GetPayUserListByChargingId(reqUid, chargingId)
	if err != nil {
		infoLog.Printf("ProcGetPayUserListById reqUid=%v chargingId=%v err=%s", reqUid, chargingId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	var userSlic []*ht_wallet.PayUserInfo
	for _, v := range payUidSlic {
		item := &ht_wallet.PayUserInfo{
			Uid:      proto.Uint32(v.Uid),
			NickName: []byte(v.NickName),
			HeadUrl:  []byte(v.HeadUrl),
			PayTs:    proto.Uint64(v.PayTs),
			National: []byte(v.National),
		}
		userSlic = append(userSlic, item)
	}
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		PayUserList: userSlic,
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/get_pay_user_list_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 7、获取单个用户的余额
func ProcGetBalanceByUid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetBalanceByUid not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetBalanceByUid invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/get_user_balance_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetBalanceByUid proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryBalanceInfoByUidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetBalanceByUid GetQueryBalanceInfoByUidReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	infoLog.Printf("ProcGetBalanceByUid reqUid=%v", reqUid)
	if reqUid == 0 {
		infoLog.Printf("ProcGetBalanceByUid reqUid=%v input param error", reqUid)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step4:执行db操作
	balanceInfo, err := dbUtil.GetUserBalanceByUid(reqUid)
	if err != nil {
		infoLog.Printf("ProcGetBalanceByUid reqUid=%v err=%s", reqUid, err)
		if err == sql.ErrNoRows {
			result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
			rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
				Status: &ht_wallet.WalletHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("success"),
				},
				CurrencyInfo: &ht_wallet.CurrencyInfo{
					Amount:              proto.Uint64(0),
					CorrespondingDollar: proto.Uint64(0),
				},
				LimitDolllerAmount: proto.Uint64(limitDollerAmount),
			}
			return true
		} else {
			result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
			rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
				Status: &ht_wallet.WalletHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}
	_, symbol, err := GetCurrencyRate(balanceInfo.Currence)
	if err != nil {
		infoLog.Printf("ProcGetBalanceByUid reqUid=%v GetCurrencyRate err=%s", reqUid, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		CurrencyInfo: &ht_wallet.CurrencyInfo{
			CurrencyType:        proto.String(balanceInfo.Currence),
			Amount:              proto.Uint64(balanceInfo.Amount),
			CorrespondingDollar: proto.Uint64(balanceInfo.DollerAmount),
			CurrencySymbol:      proto.String(symbol),
		},
		LimitDolllerAmount: proto.Uint64(limitDollerAmount),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	//双写更新Redis
	setReqBody := &ht_wallet.WalletReqBody{
		SetUserBalanceReqbody: &ht_wallet.SetUserBalanceReqBody{
			ReqUid: proto.Uint32(reqUid),
			CurrencyInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(balanceInfo.Currence),
				Amount:              proto.Uint64(balanceInfo.Amount),
				CorrespondingDollar: proto.Uint64(balanceInfo.DollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcGetBalanceByUid proto.Marshal  SetKeyAndValueReqBody failed from=%v err=%s", reqUid, err)
		attr := "gowalletdbd/get_user_balance_proto_error"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint16(ht_wallet.WALLET_CMD_TYPE_CMD_SET_USER_BALANCE_REQ)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/get_user_balance_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 8、批量查询单个用户所有的收款流水
func ProcBatchGetCollectMoneyRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetCollectMoneyRecord not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetCollectMoneyRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/batch_get_collect_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetCollectMoneyRecord proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetCollectMoneyRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetCollectMoneyRecord GetBatchGetCollectMoneyRecordRspbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	minCollectId := subReqBody.GetMinCollectId()
	infoLog.Printf("ProcBatchGetCollectMoneyRecord reqUid=%v minCollectId=%v", reqUid, minCollectId)
	// Step4:执行db操作
	collectSlic, outMinId, err := dbUtil.BatchGetCollectMoneyRecordByMinId(reqUid, minCollectId)
	if err != nil {
		infoLog.Printf("ProcBatchGetCollectMoneyRecord reqUid=%v minCollectId=%v err=%s", reqUid, minCollectId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	var collectMoneyRecordSlic []*ht_wallet.CollectMoneyRecord
	for _, v := range collectSlic {
		rate, symbol, err := GetCurrencyRate(v.Currence)
		var collectDollerAmount, feeDollerAmount, balanceDollerAmount uint64
		if err != nil {
			infoLog.Printf("ProcBatchGetCollectMoneyRecord reqUid=%v minCollectId=%v GetCurrencyRate err=%s", reqUid, minCollectId, err)
			attr := "gowalletdbd/get_currency_failed"
			libcomm.AttrAdd(attr, 1)
		} else {
			collectDollerAmount = v.Amount * rate / 10000
			feeDollerAmount = v.PlatAmont * rate / 10000
			balanceDollerAmount = v.BalandeAmount * rate / 10000
		}
		item := &ht_wallet.CollectMoneyRecord{
			UniqId:     proto.Uint64(v.UniqId),
			PayUid:     proto.Uint32(v.PayUid),
			CollectUid: proto.Uint32(v.CollectUid),
			ChargingId: proto.Uint64(v.CharginId),
			TransDesc:  proto.String(v.Description),
			CurrencyInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(v.Currence),
				Amount:              proto.Uint64(v.Amount),
				CorrespondingDollar: proto.Uint64(collectDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			PlatFee: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(v.Currence),
				Amount:              proto.Uint64(v.PlatAmont),
				CorrespondingDollar: proto.Uint64(feeDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			TransId:     proto.String(v.TransId),
			AccountType: ht_wallet.AccountType(v.AccountType).Enum(),
			BalanceInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(v.Currence),
				Amount:              proto.Uint64(v.BalandeAmount),
				CorrespondingDollar: proto.Uint64(balanceDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			TimeStamp: proto.Uint64(v.TimeStamp),
		}
		collectMoneyRecordSlic = append(collectMoneyRecordSlic, item)
	}
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		RecordList:   collectMoneyRecordSlic,
		MinCollectId: proto.Uint64(outMinId),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/batch_get_collect_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 9、查询当个用户所有的提现记录
func ProcBatchGetWithdrawalsRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetWithdrawalsRecord not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetWithdrawalsRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/batch_get_withdrawals_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetWithdrawalsRecord proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetWithdrawalsRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetWithdrawalsRecord GetBatchGetWithdrawalsRecordReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	minId := subReqBody.GetMinWithdrawalsId()
	infoLog.Printf("ProcBatchGetWithdrawalsRecord reqUid=%v minId=%v", reqUid, minId)
	// Step4:执行db操作
	withDrawalsSlic, outMinId, err := dbUtil.BatchGetWithdrawalRecordByMinId(reqUid, minId)
	if err != nil {
		infoLog.Printf("ProcBatchGetWithdrawalsRecord reqUid=%v minId=%v err=%s", reqUid, minId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	var withdrawalsRecordSlic []*ht_wallet.WithdrawalsInfo
	for _, v := range withDrawalsSlic {
		var statSlic []*ht_wallet.WithdrawalsStat
		for _, j := range v.StatSlic {
			statItem := &ht_wallet.WithdrawalsStat{
				StatType:  ht_wallet.StatType(j.Stat).Enum(),
				TimeStamp: proto.Uint64(j.TimeStamp),
			}
			statSlic = append(statSlic, statItem)
		}
		infoLog.Printf("ProcBatchGetWithdrawalsRecord statSlicLen=%v", len(statSlic))
		rate, symbol, err := GetCurrencyRate(v.Currence)
		var collectDollerAmount, feeDollerAmount, balanceDollerAmount uint64
		if err != nil {
			infoLog.Printf("ProcBatchGetWithdrawalsRecord reqUid=%v minId=%v GetCurrencyRate err=%s", reqUid, minId, err)
			attr := "gowalletdbd/get_currency_failed"
			libcomm.AttrAdd(attr, 1)
		} else {
			collectDollerAmount = v.Amount * rate / 10000
			feeDollerAmount = v.PlatAmont * rate / 10000
			balanceDollerAmount = v.BalandeAmount * rate / 10000
		}
		withdrawalsItme := &ht_wallet.WithdrawalsInfo{
			WithdrawalsId:   proto.Uint64(v.Id),
			WithdrawalsTime: proto.Uint64(v.TimeStamp),
			CurrencyInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(v.Currence),
				Amount:              proto.Uint64(v.Amount),
				CorrespondingDollar: proto.Uint64(collectDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			FeeInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(v.Currence),
				Amount:              proto.Uint64(v.PlatAmont),
				CorrespondingDollar: proto.Uint64(feeDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			Remarks:            proto.String(v.Remarks),
			WithdrawalsStat:    statSlic,
			AccountType:        ht_wallet.AccountType(v.AccountType).Enum(),
			WithdrawalsAccount: proto.String(v.Account),
			RealName:           proto.String(v.RealName),
			BalanceInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(v.Currence),
				Amount:              proto.Uint64(v.BalandeAmount),
				CorrespondingDollar: proto.Uint64(balanceDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			ServerRemarks: proto.String(v.ServerRemarks),
		}
		withdrawalsRecordSlic = append(withdrawalsRecordSlic, withdrawalsItme)
	}
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		WithdrawalsInfo:  withdrawalsRecordSlic,
		MinWithdrawalsId: proto.Uint64(outMinId),
	}
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/batch_get_withdrawals_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 10、查询汇率
func ProcQueryExchageRate(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryExchageRate not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryExchageRate invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/query_exchange_rate_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryExchageRate proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryExchangeRateReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryExchageRate GetBatchGetWithdrawalsRecordReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	currenceType := subReqBody.GetSrcCurrencyType()
	infoLog.Printf("ProcQueryExchageRate reqUid=%v currency=%s", reqUid, currenceType)
	var outRate uint64
	var symbol string
	// Step4: 首先查询内存缓存如果没有找到则查询db 如果db中不存在直接返回错误
	if rate, ok := globalRate[currenceType]; ok {
		infoLog.Printf("ProcQueryExchageRate reqUid=%v currency=%s rate=%v", reqUid, currenceType, rate)
		outRate = rate.Rate
		symbol = rate.Symbol
	} else {
		rateInfo, err := dbUtil.GetExchangeRate(currenceType)
		if err != nil {
			mysqlerr, ok := err.(*mysql.MySQLError)
			if ok && mysqlerr.Number == 1054 {
				infoLog.Printf("ProcQueryExchageRate reqUid=%v currency=%s not found", reqUid, currenceType)
				result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
				rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
					Status: &ht_wallet.WalletHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("success"),
					},
					CorrespondingDollar: proto.Uint64(1000), // 所有的净额需要乘以1000之后返回给客户端
					CurrencySymbol:      proto.String(DollerSymbol),
					DollerAmountLimit:   proto.Uint64(limitDollerAmount),
					DeadLineDateLimit:   proto.Uint32(limitDeadLineLimit),
					SupportCurrency:     supportCurrency,
				}
				return true
			} else {
				infoLog.Printf("ProcQueryExchageRate reqUid=%v currency=%s rate=%v load from db failed err=%s", reqUid, currenceType, outRate, err)
				result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
				rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
					Status: &ht_wallet.WalletHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("internal error"),
					},
				}
				return false
			}

		}
		outRate = rateInfo.Rate
		symbol = rateInfo.Symbol
		infoLog.Printf("ProcQueryExchageRate reqUid=%v currency=%s rate=%v symbol=%s load from db", reqUid, currenceType, outRate, symbol)
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		CorrespondingDollar: proto.Uint64(outRate),
		CurrencySymbol:      proto.String(symbol),
		DollerAmountLimit:   proto.Uint64(limitDollerAmount),
		DeadLineDateLimit:   proto.Uint32(limitDeadLineLimit),
		SupportCurrency:     supportCurrency,
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/query_exchange_rate_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 11、用户申请提现
func ProcWithdrawalsReq(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcWithdrawalsReq not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcWithdrawalsReq invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/withdrawals_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcWithdrawalsReq proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetWithdrawalsReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcWithdrawalsReq GetWithdrawalsReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	currency := subReqBody.GetCurrencyInfo().GetCurrencyType()
	amount := subReqBody.GetCurrencyInfo().GetAmount()
	withdrawalsRemark := subReqBody.GetRemarkInfo()
	accountInfo := subReqBody.GetAccountDetailInfo()
	accountType := accountInfo.GetAccountType()
	realName := accountInfo.GetRealName()
	account := accountInfo.GetWithdrawalsAccount()
	accountRemark := accountInfo.GetRemarkInfo()
	uniqId := accountInfo.GetUinqId()
	outRate, _, err := GetCurrencyRate(currency)
	if err != nil {
		infoLog.Printf("ProcWithdrawalsReq reqUid=%v currency=%s amount=%v withdrawalsRemark=%s accountType=%s realName=%s account=%s accountRemark=%s uniqId=%v err=%s",
			reqUid,
			currency,
			amount,
			withdrawalsRemark,
			accountType,
			realName,
			account,
			accountRemark,
			uniqId,
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// 首先将美元换算成doller
	dollerAmount := uint64(float64(amount) * (float64(outRate) / 10000))
	var platAmount uint64
	if currency == JPYCurrency {
		// mysql 中存储的净额是实际净额*1000 之后的 所以需要先除以1000 换算成实际币种 然后向上取整 在乘以1000即可
		platAmount = uint64(math.Ceil(float64(amount)*withdrawalsPlatFeeRate/1000) * 1000)
	} else {
		platAmount = uint64(float64(amount) * withdrawalsPlatFeeRate)
	}

	platDollerAmount := uint64(float64(platAmount) * (float64(outRate) / 10000))
	infoLog.Printf("ProcWithdrawalsReq reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v withdrawalsRemark=%s accountType=%s realName=%s account=%s accountRemark=%s uniqId=%v",
		reqUid,
		currency,
		amount,
		dollerAmount,
		platAmount,
		platDollerAmount,
		withdrawalsRemark,
		accountType,
		realName,
		account,
		accountRemark,
		uniqId)
	// Step4:执行db操作
	outUniqId, err := dbUtil.InsertWithdrawalsRecord(reqUid,
		currency,
		amount,
		dollerAmount,
		platAmount,
		platDollerAmount,
		withdrawalsPlatFeeRate,
		withdrawalsRemark,
		uint32(accountType),
		realName,
		account)

	if err != nil {
		infoLog.Printf("ProcWithdrawalsReq reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v withdrawalsRemark=%s accountType=%s realName=%s account=%s accountRemark=%s uniqId=%v err=%s",
			reqUid,
			currency,
			amount,
			dollerAmount,
			platAmount,
			platDollerAmount,
			withdrawalsRemark,
			accountType,
			realName,
			account,
			accountRemark,
			uniqId)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internale error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcWithdrawalsReq reqUid=%v currency=%s amount=%v dollerAmount=%v platAmount=%v platDollerAmount=%v uniqId=%v",
		reqUid,
		currency,
		amount,
		dollerAmount,
		platAmount,
		platDollerAmount,
		outUniqId)
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		UniqId: proto.Uint64(outUniqId),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	// 删除redis中的缓存
	//双写更新Redis
	setReqBody := &ht_wallet.WalletReqBody{
		ClearUserBalanceReqbody: &ht_wallet.ClearUserBalanceReqBody{
			ReqUid: proto.Uint32(reqUid),
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcWithdrawalsReq proto.Marshal  SetKeyAndValueReqBody failed from=%v err=%s", reqUid, err)
		attr := "gowalletdbd/get_user_balance_proto_error"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint16(ht_wallet.WALLET_CMD_TYPE_CMD_CLEAR_USER_BALANCE_REQ)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/withdrawals_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetCurrencyRate(currency string) (outRate uint64, symbol string, err error) {
	if currency == "" {
		infoLog.Printf("GetCurrencyRate currency empty")
		err = util.ErrDbParam
		return outRate, symbol, err
	}
	if rate, ok := globalRate[currency]; ok {
		outRate = rate.Rate
		symbol = rate.Symbol
	} else {
		rateInfo, err := dbUtil.GetExchangeRate(currency)
		if err != nil {
			return outRate, symbol, err
		} else {
			outRate = rateInfo.Rate
			symbol = rateInfo.Symbol
		}
	}
	return outRate, symbol, nil
}

// 12、添加提现账号
func ProcAddWithdrawalsAccount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcAddWithdrawalsAccount not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcAddWithdrawalsAccount invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/add_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcAddWithdrawalsAccount proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetAddWithdrawalsAccountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcAddWithdrawalsAccount GetWithdrawalsReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	uid := subReqBody.GetUid()
	accountInfo := subReqBody.GetAccountDetailInfo()
	accountType := accountInfo.GetAccountType()
	realName := accountInfo.GetRealName()
	account := accountInfo.GetWithdrawalsAccount()
	remark := accountInfo.GetRemarkInfo()
	infoLog.Printf("ProcAddWithdrawalsAccount from=%v accountType=%v realName=%v account=%v remark=%s",
		uid,
		accountType,
		realName,
		account,
		remark)
	if uid == 0 || account == "" || accountType > ht_wallet.AccountType_OTHER_ACCOUNT || realName == "" {
		infoLog.Printf("ProcAddWithdrawalsAccount from=%v accountType=%v realName=%v account=%v remark=%s input param error",
			uid,
			accountType,
			realName,
			account,
			remark)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step4:执行db操作
	err = dbUtil.InsertWithdrawalsAccount(uid, uint32(accountType), realName, account, remark)
	if err != nil {
		infoLog.Printf("ProcAddWithdrawalsAccount from=%v accountType=%v realName=%v account=%v remark=%s err=%s",
			uid,
			accountType,
			realName,
			account,
			remark,
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/add_account_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 13、更新提现账号
func ProcModifyWithdrawalsAccount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModifyWithdrawalsAccount not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModifyWithdrawalsAccount invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/modify_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModifyWithdrawalsAccount proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetModifyWithdrawalsAccountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcModifyWithdrawalsAccount GetModifyWithdrawalsAccountReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	uid := subReqBody.GetUid()
	uniqId := subReqBody.GetUniqId()
	accountInfo := subReqBody.GetAccountDetailInfo()
	accountType := accountInfo.GetAccountType()
	realName := accountInfo.GetRealName()
	account := accountInfo.GetWithdrawalsAccount()
	remark := accountInfo.GetRemarkInfo()
	infoLog.Printf("ProcModifyWithdrawalsAccount from=%v uniqId=%v accountType=%v realName=%v account=%v remark=%s",
		uid,
		uniqId,
		accountType,
		realName,
		account,
		remark)
	// Step4:执行db操作
	err = dbUtil.UpdateWithdrawalsAccount(uid, uniqId, uint32(accountType), realName, account, remark)
	if err != nil {
		infoLog.Printf("ProcModifyWithdrawalsAccount from=%v uniqId=%v accountType=%v realName=%v account=%v remark=%s err=%s",
			uid,
			uniqId,
			accountType,
			realName,
			account,
			remark,
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/modify_account_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 14、删除提现账号
func ProcDelWithdrawalsAccount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcDelWithdrawalsAccount not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelWithdrawalsAccount invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/del_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelWithdrawalsAccount proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelWithdrawalsAccountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelWithdrawalsAccount GetDelWithdrawalsAccountReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	uid := subReqBody.GetUid()
	uniqId := subReqBody.GetUniqId()
	infoLog.Printf("ProcDelWithdrawalsAccount from=%v uniqId=%v", uid, uniqId)
	// Step4:执行db操作
	err = dbUtil.DelWithdrawalsAccount(uid, uniqId)
	if err != nil {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/del_account_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 15、获取所有的提现账号
func ProcGetAllWithdrawalsAccount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetAllWithdrawalsAccount not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetAllWithdrawalsAccount invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/get_all_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetAllWithdrawalsAccount proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetAllWithdrawalsAccountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetAllWithdrawalsAccount GetGetAllWithdrawalsAccountReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	uid := subReqBody.GetUid()
	infoLog.Printf("ProcGetAllWithdrawalsAccount from=%v", uid)
	// Step4:执行db操作
	accountSlic, err := dbUtil.GetWithdrawalsAccountByUid(uid)
	if err != nil {
		infoLog.Printf("ProcGetAllWithdrawalsAccount from=%v dbUtil.GetWithdrawalsAccountByUid err=%s", uid, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	var pbAccountSlic []*ht_wallet.AccountDetailInfo
	for _, v := range accountSlic {
		item := &ht_wallet.AccountDetailInfo{
			AccountType:        ht_wallet.AccountType(v.AccountType).Enum(),
			RealName:           proto.String(v.RealName),
			WithdrawalsAccount: proto.String(v.Account),
			RemarkInfo:         proto.String(v.RemarkInfo),
			UinqId:             proto.Uint64(v.UniqId),
		}
		pbAccountSlic = append(pbAccountSlic, item)
	}
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		AccountInfoList: pbAccountSlic,
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/get_all_account_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 16、查询当个用户所有的交易记录 包括购课程收款、提现、提现失败记录
func ProcBatchGetTransationRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetTransationRecord not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetTransationRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/batch_get_trans_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetTransationRecord proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetTransationRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetTransationRecord GetBatchGetTransationRecordReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	minId := subReqBody.GetMinTransId()
	infoLog.Printf("ProcBatchGetTransationRecord reqUid=%v minId=%v", reqUid, minId)
	// Step4:执行db操作
	transationSlic, outMinId, err := dbUtil.BatchGetTransationRecordByMinId(reqUid, minId)
	if err != nil {
		infoLog.Printf("ProcBatchGetTransationRecord reqUid=%v minId=%v dbUtil.BatchGetTransationRecordByMinId failed err=%s", reqUid, minId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	var pbTransationSlic []*ht_wallet.TransationInfo
	for _, v := range transationSlic {
		var dollerAmount uint64
		outRate, symbol, err := GetCurrencyRate(v.Currence)
		if err != nil {
			infoLog.Printf("ProcBatchGetTransationRecord reqUid=%v minId=%v GetCurrencyRate failed Currence=%s err=%s",
				reqUid,
				minId,
				v.Currence,
				err)
			attr := "gowalletdbd/get_currency_failed"
			libcomm.AttrAdd(attr, 1)
		} else {
			dollerAmount = uint64(v.Amount * outRate / 10000)
		}
		item := &ht_wallet.TransationInfo{
			TransId:   proto.Uint64(v.Id),
			TransType: ht_wallet.TransType(v.TransType).Enum(),
			UinqId:    proto.Uint64(v.UniqId),
			CurrencyInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(v.Currence),
				Amount:              proto.Uint64(v.Amount),
				CorrespondingDollar: proto.Uint64(dollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			AccountType: ht_wallet.AccountType(v.AccountType).Enum(),
			TransTime:   proto.Uint64(v.TransTime),
		}
		pbTransationSlic = append(pbTransationSlic, item)
	}
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("internal error"),
		},
		TransInfoList: pbTransationSlic,
		MinTransId:    proto.Uint64(outMinId),
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/batch_get_trans_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 17、通过uinq_id 查询单个收款、提现的详情
func ProcGetTransationDetailInfo(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetTransationDetailInfo not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetTransationDetailInfo invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/get_trans_detail_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetTransationDetailInfo proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryTransDetailInfoByIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetTransationDetailInfo GetQueryTransDetailInfoByIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	transType := subReqBody.GetTransType()
	uniqId := subReqBody.GetUniqId()
	infoLog.Printf("ProcGetTransationDetailInfo reqUid=%v tranType=%v uniqId=%v", reqUid, transType, uniqId)
	// Step4:执行db操作
	if transType == ht_wallet.TransType_LESSON_CHARGING {
		chargingInfo, err := dbUtil.GetCollectMoneyRecordById(uniqId)
		if err != nil {
			infoLog.Printf("ProcGetTransationDetailInfo reqUid=%v tranType=%v uniqId=%v dbUtil.GetCollectMoneyRecordById err=%s", reqUid, transType, uniqId, err)
			result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
			rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
				Status: &ht_wallet.WalletHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("get req body failed"),
				},
			}
			return false
		}
		rate, symbol, err := GetCurrencyRate(chargingInfo.Currence)
		var collectDollerAmount, feeDollerAmount, balanceDollerAmount uint64
		if err != nil {
			infoLog.Printf("ProcGetTransationDetailInfo reqUid=%v tranType=%v uniqId=%v GetCurrencyRate err=%s", reqUid, transType, uniqId, err)
			attr := "gowalletdbd/get_currency_failed"
			libcomm.AttrAdd(attr, 1)
		} else {
			collectDollerAmount = chargingInfo.Amount * rate / 10000
			feeDollerAmount = chargingInfo.PlatAmont * rate / 10000
			balanceDollerAmount = chargingInfo.BalandeAmount * rate / 10000
		}

		result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			TransType: transType.Enum(),
			CollectMoneyRecored: &ht_wallet.CollectMoneyRecord{
				UniqId:     proto.Uint64(chargingInfo.UniqId),
				PayUid:     proto.Uint32(chargingInfo.PayUid),
				CollectUid: proto.Uint32(chargingInfo.CollectUid),
				ChargingId: proto.Uint64(chargingInfo.CharginId),
				TransDesc:  proto.String(chargingInfo.Description),
				CurrencyInfo: &ht_wallet.CurrencyInfo{
					CurrencyType:        proto.String(chargingInfo.Currence),
					Amount:              proto.Uint64(chargingInfo.Amount),
					CorrespondingDollar: proto.Uint64(collectDollerAmount),
					CurrencySymbol:      proto.String(symbol),
				},
				PlatFee: &ht_wallet.CurrencyInfo{
					CurrencyType:        proto.String(chargingInfo.Currence),
					Amount:              proto.Uint64(chargingInfo.PlatAmont),
					CorrespondingDollar: proto.Uint64(feeDollerAmount),
					CurrencySymbol:      proto.String(symbol),
				},
				TransId:     proto.String(chargingInfo.TransId),
				AccountType: ht_wallet.AccountType(chargingInfo.AccountType).Enum(),
				BalanceInfo: &ht_wallet.CurrencyInfo{
					CurrencyType:        proto.String(chargingInfo.Currence),
					Amount:              proto.Uint64(chargingInfo.BalandeAmount),
					CorrespondingDollar: proto.Uint64(balanceDollerAmount),
					CurrencySymbol:      proto.String(symbol),
				},
				TimeStamp: proto.Uint64(chargingInfo.TimeStamp),
			},
		}
		bNeedCall = false
		SendRsp(c, head, rspBody, uint16(result))
	} else {
		withdrawalsRecord, err := dbUtil.GetWithdrawalsRecordById(uniqId)
		if err != nil {
			infoLog.Printf("ProcGetTransationDetailInfo reqUid=%v tranType=%v uniqId=%v dbUtil.GetWithdrawalsRecordById err=%s", reqUid, transType, uniqId, err)
			result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
			rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
				Status: &ht_wallet.WalletHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("get req body failed"),
				},
			}
			return false
		}
		var statSlic []*ht_wallet.WithdrawalsStat
		for _, j := range withdrawalsRecord.StatSlic {
			statItem := &ht_wallet.WithdrawalsStat{
				StatType:  ht_wallet.StatType(j.Stat).Enum(),
				TimeStamp: proto.Uint64(j.TimeStamp),
			}
			statSlic = append(statSlic, statItem)
		}
		rate, symbol, err := GetCurrencyRate(withdrawalsRecord.Currence)
		var collectDollerAmount, feeDollerAmount, balanceDollerAmount uint64
		if err != nil {
			infoLog.Printf("ProcGetTransationDetailInfo reqUid=%v tranType=%v uniqId=%v GetCurrencyRate err=%s", reqUid, transType, uniqId, err)
			attr := "gowalletdbd/get_currency_failed"
			libcomm.AttrAdd(attr, 1)
		} else {
			collectDollerAmount = withdrawalsRecord.Amount * rate / 10000
			feeDollerAmount = withdrawalsRecord.PlatAmont * rate / 10000
			balanceDollerAmount = withdrawalsRecord.BalandeAmount * rate / 10000
		}

		result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			TransType: transType.Enum(),
			WithdrawalsInfo: &ht_wallet.WithdrawalsInfo{
				WithdrawalsId:   proto.Uint64(withdrawalsRecord.Id),
				WithdrawalsTime: proto.Uint64(withdrawalsRecord.TimeStamp),
				CurrencyInfo: &ht_wallet.CurrencyInfo{
					CurrencyType:        proto.String(withdrawalsRecord.Currence),
					Amount:              proto.Uint64(withdrawalsRecord.Amount),
					CorrespondingDollar: proto.Uint64(collectDollerAmount),
					CurrencySymbol:      proto.String(symbol),
				},
				FeeInfo: &ht_wallet.CurrencyInfo{
					CurrencyType:        proto.String(withdrawalsRecord.Currence),
					Amount:              proto.Uint64(withdrawalsRecord.PlatAmont),
					CorrespondingDollar: proto.Uint64(feeDollerAmount),
					CurrencySymbol:      proto.String(symbol),
				},
				Remarks:            proto.String(withdrawalsRecord.Remarks),
				WithdrawalsStat:    statSlic,
				AccountType:        ht_wallet.AccountType(withdrawalsRecord.AccountType).Enum(),
				WithdrawalsAccount: proto.String(withdrawalsRecord.Account),
				RealName:           proto.String(withdrawalsRecord.RealName),
				BalanceInfo: &ht_wallet.CurrencyInfo{
					CurrencyType:        proto.String(withdrawalsRecord.Currence),
					Amount:              proto.Uint64(withdrawalsRecord.BalandeAmount),
					CorrespondingDollar: proto.Uint64(balanceDollerAmount),
					CurrencySymbol:      proto.String(symbol),
				},
				ServerRemarks: proto.String(withdrawalsRecord.ServerRemarks),
			},
		}
		bNeedCall = false
		SendRsp(c, head, rspBody, uint16(result))
	}
	// Step5:拼成响应返回rsp
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/get_trans_detail_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 18、通过唯一id查询单个提现记录
func ProcWithdrawalsRecordById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcWithdrawalsRecordById not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcWithdrawalsRecordById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/get_withdrawals_by_id_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcWithdrawalsRecordById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetWithdrawalsRecordByIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcWithdrawalsRecordById GetGetWithdrawalsRecordByIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	uniqId := subReqBody.GetUniqId()
	infoLog.Printf("ProcWithdrawalsRecordById reqUid=%v uniqId=%v", reqUid, uniqId)
	// Step4:执行db操作
	withdrawalsRecord, err := dbUtil.GetWithdrawalsRecordById(uniqId)
	if err != nil {
		infoLog.Printf("ProcWithdrawalsRecordById reqUid=%v uniqId=%v dbUtil.GetWithdrawalsRecordById err=%s", reqUid, uniqId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	var statSlic []*ht_wallet.WithdrawalsStat
	for _, j := range withdrawalsRecord.StatSlic {
		statItem := &ht_wallet.WithdrawalsStat{
			StatType:  ht_wallet.StatType(j.Stat).Enum(),
			TimeStamp: proto.Uint64(j.TimeStamp),
		}
		statSlic = append(statSlic, statItem)
	}
	rate, symbol, err := GetCurrencyRate(withdrawalsRecord.Currence)
	var collectDollerAmount, feeDollerAmount, balanceDollerAmount uint64
	if err != nil {
		infoLog.Printf("ProcWithdrawalsRecordById reqUid=%v uniqId=%v GetCurrencyRate failed currence=%s err=%s",
			reqUid,
			uniqId,
			withdrawalsRecord.Currence,
			err)
		attr := "gowalletdbd/get_currency_failed"
		libcomm.AttrAdd(attr, 1)
	} else {
		collectDollerAmount = withdrawalsRecord.Amount * rate / 10000
		feeDollerAmount = withdrawalsRecord.PlatAmont * rate / 10000
		balanceDollerAmount = withdrawalsRecord.BalandeAmount * rate / 10000
	}

	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		WithdrawalsInfo: &ht_wallet.WithdrawalsInfo{
			WithdrawalsId:   proto.Uint64(withdrawalsRecord.Id),
			WithdrawalsTime: proto.Uint64(withdrawalsRecord.TimeStamp),
			CurrencyInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(withdrawalsRecord.Currence),
				Amount:              proto.Uint64(withdrawalsRecord.Amount),
				CorrespondingDollar: proto.Uint64(collectDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			FeeInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(withdrawalsRecord.Currence),
				Amount:              proto.Uint64(withdrawalsRecord.PlatAmont),
				CorrespondingDollar: proto.Uint64(feeDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			Remarks:            proto.String(withdrawalsRecord.Remarks),
			WithdrawalsStat:    statSlic,
			AccountType:        ht_wallet.AccountType(withdrawalsRecord.AccountType).Enum(),
			WithdrawalsAccount: proto.String(withdrawalsRecord.Account),
			RealName:           proto.String(withdrawalsRecord.RealName),
			BalanceInfo: &ht_wallet.CurrencyInfo{
				CurrencyType:        proto.String(withdrawalsRecord.Currence),
				Amount:              proto.Uint64(withdrawalsRecord.BalandeAmount),
				CorrespondingDollar: proto.Uint64(balanceDollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			ServerRemarks: proto.String(withdrawalsRecord.ServerRemarks),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/get_withdrawals_by_id_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 19、查询单个群课程的是否收费和用户是否已经购买
func ProcQueryLessonAndPurchaseStat(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryLessonAndPurchaseStat not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryLessonAndPurchaseStat invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/query_lesson_and_purchase_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryLessonAndPurchaseStat proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryLessonAndPurchaseStatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryLessonAndPurchaseStat GetQueryLessonAndPurchaseStatReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	chargingId := subReqBody.GetChargingId()
	infoLog.Printf("ProcQueryLessonAndPurchaseStat reqUid=%v chargingId=%v", reqUid, chargingId)
	// Step4:执行db操作
	stat, isPurchased, err := dbUtil.GetLessonAndPurchaseStatById(reqUid, chargingId)
	if err != nil {
		infoLog.Printf("ProcQueryLessonAndPurchaseStat dbUtil.GetLessonAndPurchaseStatById failed reqUid=%v chargingId=%v err=%s", reqUid, chargingId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query db error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ChargingStat: ht_wallet.ChargingStat(stat).Enum(),
		HasPurchase:  proto.Bool(isPurchased),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/query_lesson_and_purchase_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 20、成功购买某个课程
func ProcSuccessPurchaseLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcSuccessPurchaseLesson not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSuccessPurchaseLesson invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/succ_purchase_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLesson proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSuccessPurchaseLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSuccessPurchaseLesson GetSuccessPurchaseLessonReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	nickName := subReqBody.GetNickName()
	headUrl := subReqBody.GetHeadUrl()
	national := subReqBody.GetNational()
	payTs := subReqBody.GetPayTs()
	chargingId := subReqBody.GetChargingId()
	accountType := subReqBody.GetAccountType()
	transId := subReqBody.GetTransactionId()
	remarks := subReqBody.GetRemarks()
	infoLog.Printf("ProcSuccessPurchaseLesson reqUid=%v nickName=%s headUrl=%s national=%s payTs=%v chargingId=%v accountType=%v transId=%s remark=%s",
		reqUid,
		nickName,
		headUrl,
		national,
		payTs,
		chargingId,
		accountType,
		transId,
		remarks)
	// Step4:执行db操作
	chargeInfo, err := dbUtil.GetChargingInfoById(reqUid, chargingId)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLesson dbUtil.GetChargingInfoById failed reqUid=%v chargingId=%v  accountType=%v transId=%s err=%s",
			reqUid,
			chargingId,
			accountType,
			transId,
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query db failed"),
			},
		}
		return false
	}

	outRate, _, err := GetCurrencyRate(chargeInfo.Currence)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLesson GetCurrencyRate failed reqUid=%v chargingId=%v transId=%s currency=%s err=%s",
			reqUid,
			chargingId,
			transId,
			chargeInfo.Currence,
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get currence rate failed"),
			},
		}
		return false
	}
	infoLog.Printf("ProcSuccessPurchaseLesson reqUid=%v chargingId=%v origin amount=%v currency=%s",
		reqUid,
		chargingId,
		chargeInfo.Amount,
		chargeInfo.Currence)
	dollerAmount := uint64(float64(chargeInfo.Amount) * (float64(outRate) / 10000))
	var platAmount uint64
	if chargeInfo.Currence == JPYCurrency {
		// mysql 中存储的净额是实际净额*1000 之后的 所以需要先除以1000 换算成实际币种 然后向上取整 在乘以1000即可
		platAmount = uint64(math.Ceil(float64(chargeInfo.Amount)*platFeeRate/1000) * 1000)
	} else {
		platAmount = uint64(float64(chargeInfo.Amount) * platFeeRate)
	}

	platDollerAmount := uint64(float64(platAmount) * (float64(outRate) / 10000))
	realAmount := chargeInfo.Amount - platAmount
	realDollerAmount := dollerAmount - platDollerAmount
	err = dbUtil.SuccessPurchaseCourseProcess(reqUid,
		nickName,
		headUrl,
		national,
		payTs,
		chargeInfo.OpUid,
		transId,
		remarks,
		chargeInfo.Currence,
		chargingId,
		realAmount,
		realDollerAmount,
		platAmount,
		platDollerAmount,
		uint32(accountType))
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLesson dbUtil.SuccessPurchaseCourseProcess failed reqUid=%v nickName=%s headUrl=%s national=%s chargingId=%v transId=%s currency=%s err=%s",
			reqUid,
			nickName,
			headUrl,
			national,
			chargingId,
			transId,
			chargeInfo.Currence,
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query db failed"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	//双写更新Redis
	setReqBody := &ht_wallet.WalletReqBody{
		ClearUserBalanceReqbody: &ht_wallet.ClearUserBalanceReqBody{
			ReqUid: proto.Uint32(chargeInfo.OpUid),
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLesson proto.Marshal  SetKeyAndValueReqBody failed from=%v err=%s", reqUid, err)
		attr := "gowalletdbd/get_user_balance_proto_error"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint16(ht_wallet.WALLET_CMD_TYPE_CMD_CLEAR_USER_BALANCE_REQ)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/succ_purchase_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 21、查询某个群是否存在课程正在收费中
func ProcQueryRoomExistLessonCharging(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryRoomExistLessonCharging not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryRoomExistLessonCharging invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/query_room_exist_charging_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryRoomExistLessonCharging proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryRoomExistLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryRoomExistLessonCharging GetQueryRoomExistLessonRspbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	roomId := subReqBody.GetRoomId()
	infoLog.Printf("ProcQueryRoomExistLessonCharging reqUid=%v roomId=%v", reqUid, roomId)
	// Step4:执行db操作
	isExist, err := dbUtil.CheckExistLessonCharginByRoomId(reqUid, roomId)
	if err != nil {
		infoLog.Printf("ProcQueryRoomExistLessonCharging dbUtil.CheckExistLessonCharginByRoomId failed reqUid=%v roomId=%v err=%s", reqUid, roomId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query db failed"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		HasLessonCharge: proto.Bool(isExist),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/query_room_exist_charging_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 22、后台审批操作
func ProcAuditWithdrawalsRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcAuditWithdrawalsRecord not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcAuditWithdrawalsRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/audit_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcAuditWithdrawalsRecord proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetAuditWithdrawalsRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcAuditWithdrawalsRecord GetAuditWithdrawalsRecordRspbody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	id := subReqBody.GetWithdrawalsId()
	opUid := subReqBody.GetOpUid()
	opType := subReqBody.GetStatType()
	serverMarks := subReqBody.GetServerRemarks()
	withdrawalsUid := subReqBody.GetWithdrawalsUid()
	infoLog.Printf("ProcAuditWithdrawalsRecord id=%v opUid=%v opType=%v serverMarks=%s withdrawalsUid=%v",
		id,
		opUid,
		opType,
		serverMarks,
		withdrawalsUid)
	// Step4:执行db操作
	switch opType {
	case ht_wallet.StatType_SUCCESS_AUDIT:
		fallthrough
	case ht_wallet.StatType_HAVE_TRANSFERRED:
		err = dbUtil.UpdateWithdrawalsStat(opUid, id, uint32(opType), serverMarks)
		if err != nil {
			infoLog.Printf("ProcAuditWithdrawalsRecord id=%v opUid=%v opType=%v serverMarks=%s err=%s",
				id,
				opUid,
				opType,
				serverMarks,
				err)
		}
	case ht_wallet.StatType_FAILED_AUDIT:
		err = dbUtil.FailureAuditProcess(opUid, id, serverMarks)
		if err != nil {
			infoLog.Printf("ProcAuditWithdrawalsRecord id=%v opUid=%v opType=%v serverMarks=%s err=%s",
				id,
				opUid,
				opType,
				serverMarks,
				err)
		}
	default:
		infoLog.Printf("ProcAuditWithdrawalsRecord id=%v opUid=%v opType=%v serverMarks=%s unhandle",
			id,
			opUid,
			opType,
			serverMarks)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step5:拼成响应返回rsp
	if err != nil {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	//双写更新Redis
	setReqBody := &ht_wallet.WalletReqBody{
		ClearUserBalanceReqbody: &ht_wallet.ClearUserBalanceReqBody{
			ReqUid: proto.Uint32(withdrawalsUid),
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcAuditWithdrawalsRecord proto.Marshal  SetKeyAndValueReqBody failed from=%v err=%s", withdrawalsUid, err)
		attr := "gowalletdbd/audit_record_proto_error"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint16(ht_wallet.WALLET_CMD_TYPE_CMD_CLEAR_USER_BALANCE_REQ)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/audit_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 23、获取单个提现账号
func ProcGetWithdrawalsAccountById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetWithdrawalsAccountById not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetWithdrawalsAccountById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/get_account_by_id_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetWithdrawalsAccountById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetWithdrawalsAccountByIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetWithdrawalsAccountById GetGetWithdrawalsAccountByIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	uniqId := subReqBody.GetUniqId()
	infoLog.Printf("ProcGetWithdrawalsAccountById reqUid=%v uniqId=%v", reqUid, uniqId)
	if reqUid == 0 || uniqId == 0 {
		infoLog.Printf("ProcGetWithdrawalsAccountById reqUid=%v uniqId=%v input param error", reqUid, uniqId)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step4:执行db操作
	account, err := dbUtil.GetWithdrawalsAccountById(reqUid, uniqId)
	if err != nil {
		infoLog.Printf("ProcGetWithdrawalsAccountById dbUtil.GetWithdrawalsAccountById reqUid=%v uniqId=%v internal error=%s", reqUid, uniqId, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		WithdrawAccount: &ht_wallet.AccountDetailInfo{
			AccountType:        ht_wallet.AccountType(account.AccountType).Enum(),
			RealName:           proto.String(account.RealName),
			WithdrawalsAccount: proto.String(account.Account),
			RemarkInfo:         proto.String(account.RemarkInfo),
			UinqId:             proto.Uint64(uniqId),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/get_account_by_id_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 24、查询单个课程是否正在收费中
func ProcQueryLessonIsCharging(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryLessonIsCharging not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryLessonIsCharging invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/query_lesson_is_charging_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryLessonIsCharging proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryLessonIsChargingReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryLessonIsCharging GetQueryLessonIsChargingReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	roomId := subReqBody.GetRoomId()
	lessonObid := subReqBody.GetLessonObid()
	if reqUid == 0 || roomId == 0 || lessonObid == "" {
		infoLog.Printf("ProcQueryLessonIsCharging invalid param reqUid=%v roomId=%v lessonObid=%s", reqUid, roomId, lessonObid)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	infoLog.Printf("ProcQueryLessonIsCharging recv req reqUid=%v roomId=%v lessonObid=%s", reqUid, roomId, lessonObid)
	// Step4:执行db操作
	isExist, err := dbUtil.CheckExistLessonCharginByLessonObid(reqUid, roomId, lessonObid)
	if err != nil {
		infoLog.Printf("ProcQueryLessonIsCharging dbUtil.CheckExistLessonCharginByLessonObid failed reqUid=%v roomId=%v lessonObid=%s err=%s", reqUid, roomId, lessonObid, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step5:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		IsCharge: proto.Bool(isExist),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/query_lesson_is_charging_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 25、后台转账成功操作
func ProcTransferSuccRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcTransferSuccRecord not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcTransferSuccRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/tran_succ_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcTransferSuccRecord proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetTransferSuccessReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcTransferSuccRecord GetTransferSuccessReqbody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	id := subReqBody.GetWithdrawalsId()
	transUid := subReqBody.GetTransUid()
	currencyInfo := subReqBody.GetTransCurrencyInfo()
	currency := currencyInfo.GetCurrencyType()
	amount := currencyInfo.GetAmount()
	accountType := subReqBody.GetTransAccountType()
	transAccount := subReqBody.GetTransAccount()
	if currency == JPYCurrency {
		amount = uint64(math.Ceil(float64(amount)/1000) * 1000)
	}
	infoLog.Printf("ProcTransferSuccRecord id=%v transUid=%v currency=%s amount=%v accountType=%v transAccount=%s",
		id,
		transUid,
		currency,
		amount,
		accountType,
		transAccount)
	// 首先将美元净额设置成amount
	dollerAmount := amount
	// 如果用户的提现金额不是美元金额执行换算步骤
	if currency != DollerCurrency {
		outRate, _, err := GetCurrencyRate(currency)
		if err != nil {
			infoLog.Printf("ProcTransferSuccRecord GetCurrencyRate id=%v transUid=%v currency=%s amount=%v accountType=%v transAccount=%s err=%s",
				id,
				transUid,
				currency,
				amount,
				accountType,
				transAccount)
			attr := "gowalletdbd/get_currency_failed"
			libcomm.AttrAdd(attr, 1)
		} else {
			dollerAmount = uint64(amount * outRate / 10000)
			infoLog.Printf("ProcTransferSuccRecord amount=%v outRate=%v dollerAmount=%v",
				amount,
				outRate,
				dollerAmount)
		}
	}
	// Step4:执行db操作
	err = dbUtil.WithdrawalsSuccTransfer(id, amount, dollerAmount, uint32(accountType), transAccount, transUid)
	if err != nil {
		infoLog.Printf("ProcTransferSuccRecord id=%v transUid=%v currency=%s amount=%v accountType=%v transAccount=%s err=%s",
			id,
			transUid,
			currency,
			amount,
			accountType,
			transAccount,
			err)
	}

	// Step5:拼成响应返回rsp
	if err != nil {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/tran_succ_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 26、更新汇率
func ProcUpdateExchangeRate(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateExchangeRate not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateExchangeRate invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/update_exchange_rate_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateExchangeRate proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateExchangeRateReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateExchangeRate GetUpdateExchangeRateReqbody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	infoLog.Printf("ProcUpdateExchangeRate reqUid=%v", reqUid)

	// Step4:执行db操作
	globalRate, err = dbUtil.BatchGetExchangeRate()
	if err != nil {
		infoLog.Printf("ProcUpdateExchangeRate dbUtil.BatchGetExchangeRate failed reqUid=%v err=%s", reqUid, err)
		return false
	}

	// Step5:拼成响应返回rsp
	if err != nil {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/update_exchange_rate_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 27、删除个人课程后批量停止课程
func ProcBatchStopLessonCharge(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchStopLessonCharge not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchStopLessonCharge invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/batch_stop_charge_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchStopLessonCharge proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchStopLessonChargingReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchStopLessonCharge BatchStopLessonChargingReqBody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// Step3:获取参数打印日志
	reqUid := subReqBody.GetReqUid()
	lessonList := subReqBody.GetLessonList()
	infoLog.Printf("ProcBatchStopLessonCharge reqUid=%v lessonListLen=%v", reqUid, len(lessonList))

	// Step4:拼成响应返回rsp
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step5:执行删除操作
	for _, v := range lessonList {
		roomId := v.GetRoomId()
		groupLessObid := v.GetGroupLessonObid()
		infoLog.Printf("ProcBatchStopLessonCharge reqUid=%v roomId=%v groupLessonObid=%s", reqUid, roomId, groupLessObid)
		chargingList, err := dbUtil.GetChargingInfoByLessonObidAndRoomId(roomId, groupLessObid)
		if err != nil {
			infoLog.Printf("ProcBatchStopLessonCharge reqUid=%v roomId=%v groupLessonObid=%s GetChargingInfoByLessonObidAndRoomId failed err=%s", reqUid, roomId, groupLessObid, err)
			attr := "gowalletdbd/batch_stop_get_lesson_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		infoLog.Printf("ProcBatchStopLessonCharge reqUid=%v roomId=%v groupLessonObid=%s charginListLen=%v", reqUid, roomId, groupLessObid, len(chargingList))
		for _, v := range chargingList {
			// 第一步停止课程收费
			err = dbUtil.StopChargingById(reqUid, v.ChargingId)
			if err != nil {
				infoLog.Printf("ProcBatchStopLessonCharge reqUid=%v chargingId=%v err=%s", reqUid, v.ChargingId, err)
				continue
			}
			// 第二步发送请求到muc广播通知停止课程收费
			msgId := fmt.Sprintf("MSG_%v", time.Now().UnixNano()/1000000)
			// 将用户的收款金额转换成对应的美元
			rate, symbol, err := GetCurrencyRate(v.Currence)
			if err == nil {
				dollerAmount := uint64(float64(v.Amount) * float64(rate) / 10000)
				code, err := mucApi.BroadCastStopLessonChange(v.RoomId,
					v.OpUid,
					"",
					v.ChargingId,
					v.Currence,
					v.Amount,
					dollerAmount,
					symbol,
					v.LessonTitle,
					v.LessonObid,
					msgId,
					false)
				infoLog.Printf("ProcBatchStopLessonCharge opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s muc retCod=%v err=%s",
					v.OpUid,
					v.PaymentFrom,
					v.LessonObid,
					v.LessonTitle,
					v.Currence,
					v.Amount,
					v.Description,
					v.RoomId,
					v.RoomName,
					code,
					err)
				if code != 0 {
					attr := "gowalletdbd/bc_stop_charge_failed"
					libcomm.AttrAdd(attr, 1)
				}
			} else {
				attr := "gowalletdbd/bc_stop_charge_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcBatchStopLessonCharge opUid=%v paymentFrom=%v lessonObid=%s paymentTitle=%s currenceType=%s amount=%v descry=%s roomId=%v roomName=%s get rate failed err=%s",
					v.OpUid,
					v.PaymentFrom,
					v.LessonObid,
					v.LessonTitle,
					v.Currence,
					v.Amount,
					v.Description,
					v.RoomId,
					v.RoomName,
					err)
			}
		}
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/batch_stop_charge_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 28、更新个人提现的平台费率
func ProcUpdateWithdrawalsRate(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateWithdrawalsRate not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateWithdrawalsRate invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2:检查子对象是否为空
	attr := "gowalletdbd/update_withdraw_rate_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateWithdrawalsRate proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateWithdrawalsRateReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateWithdrawalsRate GetUpdateWithdrawalsRateReqbody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// Step3:获取参数打印日志
	uniqId := subReqBody.GetWithdrawalsId()
	opUid := subReqBody.GetOpUid()
	rate := subReqBody.GetWithdrawalsRate()
	currency := subReqBody.GetPayCurrency().GetCurrencyType()
	amount := subReqBody.GetPayCurrency().GetAmount()
	infoLog.Printf("ProcUpdateWithdrawalsRate uniqId=%v opUid=%v rate=%v currency=%s amount=%v",
		uniqId,
		opUid,
		rate,
		currency,
		amount)
	outRate, _, err := GetCurrencyRate(currency)
	if err != nil {
		infoLog.Printf("ProcUpdateWithdrawalsRate uniqId=%v opUid=%v rate=%v currency=%s amount=%v GetCurrencyRate failed err=%s",
			uniqId,
			opUid,
			rate,
			currency,
			amount,
			err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// 首先将美元换算成doller
	platAmount := uint64(float64(amount) * rate)
	platDollerAmount := uint64(float64(platAmount) * (float64(outRate) / 10000))
	infoLog.Printf("ProcUpdateWithdrawalsRate uniqId=%v opUid=%v rate=%v currency=%s amount=%v platAmount=%v platDollerAmount=%v",
		uniqId,
		opUid,
		rate,
		currency,
		amount,
		platAmount,
		platDollerAmount)
	// Step4:执行db操作
	err = dbUtil.UpdateWithdrawalsRate(opUid, uniqId, rate, platAmount, platDollerAmount)
	// Step5:拼成响应返回rsp
	if err != nil {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step6:计算耗时
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletdbd/update_withdraw_rate_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}
func SetRedisCache(head *common.HeadV3, reqPayLoad []byte) {
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV3Packet)
		if ok { // 是HeadV3Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisMasterPacket from=%v ret=%v", head.From, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisMasterPacket head failed from=%v err=%s", head.From, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed from=%v err=%s", head.From, err)

	}

	redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisSlavePacket, ok := redisPacket.(*common.HeadV3Packet)
		if ok { // 是HeadV3Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisSlavePacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisSlavePacket from=%v ret=%v", head.From, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisSlavePacket head failed from=%v err=%s", head.From, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentSlaveApi.SendAndRecvPacket failed from=%v err=%s", head.From, err)

	}
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV3, rspBody *ht_wallet.WalletRspBody, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRsp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Printf("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%v port=%v", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV3(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, 1000)
	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%v port=%v", slaveIp, slavePort)
	writeAgentSlaveApi = common.NewSrvToSrvApiV3(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, 1000)

	// 读取mucApi 配置
	mucIp := cfg.Section("MUC").Key("ip").MustString("127.0.0.1")
	mucPort := cfg.Section("MUC").Key("port").MustString("0")
	mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	infoLog.Printf("muc server ip=%v port=%v connLimit=%v", mucIp, mucPort, mucConnLimit)
	mucApi = common.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, mucConnLimit)

	// user info cache api
	userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	infoLog.Printf("user info cache ip=%v port=%v", userInfoIp, userInfoPort)
	userInfoCacheApi = common.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	supportCount := cfg.Section("SUPPORT_CURRENCY").Key("count").MustInt(0)
	for i := 0; i < supportCount; i += 1 {
		currencyKey := "currency_" + strconv.Itoa(i)
		currencyItem := cfg.Section("SUPPORT_CURRENCY").Key(currencyKey).MustString("USD")
		supportCurrency = append(supportCurrency, currencyItem)
	}
	infoLog.Printf("support currency count=%v supportCurrency=%v", supportCount, supportCurrency)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	dbUtil = util.NewDbUtil(db, infoLog)
	globalRate, err = dbUtil.BatchGetExchangeRate()
	if err != nil {
		infoLog.Printf("dbUtil.BatchGetExchangeRate failed err=%s", err)
		return
	}
	// get check interval
	checkInterval = cfg.Section("CHECK").Key("internal_second").MustInt(300)
	limitDollerAmount = cfg.Section("LIMIT_AMOUNT").Key("doller_amount").MustUint64(50000)
	limitDeadLineLimit = uint32(cfg.Section("LIMIT_AMOUNT").Key("dead_line").MustInt(7))
	platFeeRate = cfg.Section("LIMIT_AMOUNT").Key("plat_fee").MustFloat64(0.1)
	withdrawalsPlatFeeRate = cfg.Section("LIMIT_AMOUNT").Key("withdrawals_plat_fee").MustFloat64(0.03)
	infoLog.Printf("main checkInternal=%v limitDollerAmount=%v limitDeadLineLimit=%v platFeeRate=%v",
		checkInterval,
		limitDollerAmount,
		limitDeadLineLimit,
		platFeeRate)
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Printf("listening:", listener.Addr())

	// 启动tikcer
	ticker = time.NewTicker(time.Second * time.Duration(checkInterval))
	go func() {
		index := 0
		for range ticker.C {
			tempGlobalRate, err := dbUtil.BatchGetExchangeRate()
			if err != nil {
				infoLog.Printf("index=%v DbUtil.GetCurrencyRate failed err=%s", index, err)
			} else {
				infoLog.Printf("main ticker tick index=%v", index)
				globalRate = tempGlobalRate
			}
			index++
		}
	}()
	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Printf("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
