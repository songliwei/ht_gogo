package main

import (
	"encoding/json"
	"errors"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_wallet"
	"github.com/golang/protobuf/proto"

	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	dbdAgentApi    *common.SrvToSrvApiV3
	redisMasterApi *common.RedisApi
	exchangeRate   map[string]uint32
)

const (
	ProcSlowThreshold = 300000
	ExpirePeriod      = 259200 //3 * 86400 = 3 day   units:second
)

var (
	ErrInvalidParam     = errors.New("err invalid param")
	ErrProtoBuff        = errors.New("pb error")
	ErrInternalErr      = errors.New("internal err")
	ErrNotExistInReids  = errors.New("err not exist in redis")
	ErrNilDbObject      = errors.New("err not set object current is nil")
	ErrDbParam          = errors.New("err param error")
	ErrChagePacket      = errors.New("err change packet")
	ErrGetResultFromDbd = errors.New("err get result from dbs")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV3packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v from=%v", head.Cmd, head.Seq, head.From)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		return false
	}

	// 统计总的请求量
	attr := "gowalletlg/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	infoLog.Printf("OnMessage trans cmd=%v", ht_wallet.WALLET_CMD_TYPE(head.Cmd))
	switch ht_wallet.WALLET_CMD_TYPE(head.Cmd) {
	case ht_wallet.WALLET_CMD_TYPE_CMD_START_CHARGING_REQ:
		go ProcStartCharging(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_CHARGING_INFO_BY_ID_REQ:
		go ProcGetChargingInfoById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_STOP_CHARGING_REQ:
		go ProcStopChargingById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_CHARGING_STAT_REQ:
		go ProcQueryChargingStatById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_GET_CHARGIN_INFO_REQ:
		go ProcBatchGetChargingInfo(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_PAY_USER_LIST_BY_CHARGING_ID_REQ:
		go ProcGetPayUserListById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_BALANCE_INF_BY_UID_REQ:
		go ProcGetBalanceByUid(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_GET_COLLECT_RECORD_REQ:
		go ProcBatchGetCollectMoneyRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_GET_WITHDRAWALS_RECORD_REQ:
		go ProcBatchGetWithdrawalsRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_EXCHANGE_REATE_REQ:
		go ProcQueryExchageRate(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_WITHDRAWALS_REQ:
		go ProcWithdrawalsReq(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_ADD_WITHDRAWALS_ACCOUNT_REQ:
		go ProcAddWithdrawalsAccount(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_MODIFY_WITHDRAWALS_ACCOUNT_REQ:
		go ProcModifyWithdrawalsAccount(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_DEL_WITHDRAWALS_ACCOUNT_REQ:
		go ProcDelWithdrawalsAccount(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_WITHDRAWALS_ACCOUNT_REQ:
		go ProcGetAllWithdrawalsAccount(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_ALL_TRANS_RECORD_REQ:
		go ProcBatchGetTransationRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_TRANS_DETAIL_INFO_BY_ID_REQ:
		go ProcGetTransationDetailInfo(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_WITHDRAWALS_RECORD_BY_ID_REQ:
		go ProcGetWithdrawalsRecordById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_LESSON_AND_PURCHASE_STAT_REQ:
		go ProcQueryLessonAndPurchaseStat(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_SUCC_PURCHASE_LESSON_REQ:
		go ProcSuccessPurchaseLesson(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_ROOM_EXIST_LESSON_CHARGE_REQ:
		go ProcQueryRoomExistLessonCharging(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_SUCC_PURCHASE_LESSON_JSON_REQ:
		go ProcSuccessPurchaseLessonJsonReq(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_AUDIT_WITHDRAWALS_RECORD_REQ:
		go ProcAuditWithdrawalsRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_GET_WITHDRAWALS_ACCOUNT_BY_ID_REQ:
		go ProcGetWithdrawalsAccountById(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_LESSON_IS_CHARGE_REQ:
		go ProcQueryLessonIsCharging(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_SUCC_TRANSFER_REQ:
		go ProcTransferSuccRecord(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_UPDATE_EXCHANGE_RATE_REQ:
		go ProcUpdateExchangeRate(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_STOP_LESSON_CHARGING:
		go ProcBatchStopLessonCharge(c, head, packet)
	case ht_wallet.WALLET_CMD_TYPE_CMD_UPDATE_WITHDRAWALS_RATE_REQ:
		go ProcUpdateWithdrawalsRate(c, head, packet)

	default:
		infoLog.Printf("OnMessage UnHandle Cmd =%v", head.Cmd)
	}
	return true
}

// 1、发起课程收费
func ProcStartCharging(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStartCharging not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStartCharging invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/start_charge_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStartCharging proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStartChargingReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcStartCharging GetStartChargingReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcStartCharging send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/start_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcStartCharging dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/start_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcStartCharging dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StartChargingRspbody = &ht_wallet.StartChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/start_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcStartCharging recv dbd ret code=%v", dbRespHeadV3.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/start_charge_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2、通过唯一标识符id查询收款的详情
func ProcGetChargingInfoById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetChargingInfoById not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetChargingInfoById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/get_charge_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetChargingInfoById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryChargingInfoByIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetChargingInfoById GetQueryChargingInfoByIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcGetChargingInfoById send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_charge_info_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcGetChargingInfoById dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_charge_info_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGetChargingInfoById dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingInfoByIdRspbody = &ht_wallet.QueryChargingInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_charge_info_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcGetChargingInfoById recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/get_charge_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 3、通过唯一标识符id停止收款
func ProcStopChargingById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStopChargingById not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStopChargingById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/stop_charge_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStopChargingById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStopChargingReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcStopChargingById GetStopChargingReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcStopChargingById send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/stop_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcStopChargingById dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/stop_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcStopChargingById dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StopChargingRspbody = &ht_wallet.StopChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/stop_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcStopChargingById recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/stop_charge_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 4、查询单个收费记录的状态
func ProcQueryChargingStatById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryChargingStatById not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryChargingStatById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/query_charge_stat_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryChargingStatById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryChargingStatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryChargingStatById GetQueryChargingStatRspbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcQueryChargingStatById send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_charge_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcQueryChargingStatById dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_charge_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcQueryChargingStatById dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryChargingStatRspbody = &ht_wallet.QueryChargingStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_charge_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcQueryChargingStatById recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/query_charge_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 5、批量查询所有的收款记录
func ProcBatchGetChargingInfo(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetChargingInfo not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetChargingInfo invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/bathc_get_charge_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetChargingInfo proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetChargingInfoListReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetChargingInfo GetBatchGetChargingInfoListReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcBatchGetChargingInfo send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/bathc_get_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcBatchGetChargingInfo dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/bathc_get_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcBatchGetChargingInfo dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetChargingInfoListRspbody = &ht_wallet.BatchGetChargingInfoListRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/bathc_get_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcBatchGetChargingInfo recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/bathc_get_charge_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 6、获取单个收款记录的付费用户列表
func ProcGetPayUserListById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetPayUserListById not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetPayUserListById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/get_pay_user_list_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetPayUserListById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryPayUserListByChargingIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetPayUserListById GetQueryPayUserListByChargingIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcGetPayUserListById send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_pay_user_list_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcGetPayUserListById dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_pay_user_list_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGetPayUserListById dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryPayUserListByChargingIdRspbody = &ht_wallet.QueryPayUserListByChargingIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_pay_user_list_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcGetPayUserListById recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/get_pay_user_list_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 7、获取单个用户的余额
func ProcGetBalanceByUid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetBalanceByUid not need call")
		}
	}()

	// Step1:检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetBalanceByUid invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// Step2: add static and get subReqBody
	attr := "gowalletlg/get_user_balance_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetBalanceByUid proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryBalanceInfoByUidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetBalanceByUid GetQueryBalanceInfoByUidReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetReqUid()
	infoLog.Printf("ProcGetBalanceByUid reqUid=%v", reqUid)
	// Step3: 首先查询redis中是否存在
	amount, dollerAmount, currency, symbol, err := GetUserBalanceInfo(head, reqUid, payLoad)
	if err != nil {
		infoLog.Printf("ProcGetBalanceByUid from=%v GetUserBalanceInfo failed err=%s", reqUid, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody.QueryBalanceInfoByUidRspbody = &ht_wallet.QueryBalanceInfoByUidRspBody{
		Status: &ht_wallet.WalletHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		CurrencyInfo: &ht_wallet.CurrencyInfo{
			CurrencyType:        proto.String(currency),
			Amount:              proto.Uint64(amount),
			CorrespondingDollar: proto.Uint64(dollerAmount),
			CurrencySymbol:      proto.String(symbol),
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/get_user_balance_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

//查看个人账户余额
func GetUserBalanceInfo(head *common.HeadV3, uid uint32, payLoad []byte) (amount, dollerAmount uint64, currency, symbol string, err error) {
	if head == nil || redisMasterApi == nil || dbdAgentApi == nil || uid == 0 {
		err = ErrNilDbObject
		return amount, dollerAmount, currency, symbol, err
	}

	amount, dollerAmount, currency, symbol, err = GetUserBalanceInfoFromRedis(head, uid)
	if err == nil {
		infoLog.Printf("GetUserBalanceInfo GetUserBalanceInfoFromRedis success from=%v amount=%v dollerAmount=%v currency=%s symbol=%s",
			uid,
			amount,
			dollerAmount,
			currency,
			symbol)
		return amount, dollerAmount, currency, symbol, nil
	} else {
		infoLog.Printf("GetUserBalanceInfo GetUserBalanceInfoFromRedis from=%v err=%s load from db", uid, err)
		attr := "gowalletlg/get_user_balance_info_redis_failed"
		libcomm.AttrAdd(attr, 1)
		amount, dollerAmount, currency, symbol, err = GetUserBalanceInfoFromDb(head, payLoad)
		if err != nil {
			infoLog.Printf("GetUserBalanceInfo GetUserBalanceInfoFromDb faield from=%v userid=%v err=%s", head.From, uid, err)
			return amount, dollerAmount, currency, symbol, err
		}
		return amount, dollerAmount, currency, symbol, nil
	}
}

//首先查询redis中的余额
func GetUserBalanceInfoFromRedis(head *common.HeadV3, uid uint32) (amount, dollerAmount uint64, currency, symbol string, err error) {
	if head == nil || redisMasterApi == nil || uid == 0 {
		err = ErrNilDbObject
		return amount, dollerAmount, currency, symbol, err
	}
	infoLog.Printf("GetUserBalanceInfoFromRedis fromId=%v from=%v", head.From, uid)
	keyName := GetUserBalanceInfoKeyName(uid)
	exists, err := redisMasterApi.Exists(keyName)
	if err != nil {
		infoLog.Printf("GetUserBalanceInfoFromRedis from=%v keyName=%s redisMasterApi.Exists err=%s", uid, keyName, err)
		return amount, dollerAmount, currency, symbol, err
	}
	infoLog.Printf("GetUserBalanceInfoFromRedis from=%v keyName=%s exist=%v", uid, keyName, exists)
	if !exists {
		err = ErrNotExistInReids
		infoLog.Printf("GetUserBalanceInfoFromRedis from=%v keyName=%s not exist in redis", uid, keyName)
		return amount, dollerAmount, currency, symbol, err
	}

	value, err := redisMasterApi.Get(keyName)
	if err != nil {
		infoLog.Printf("GetUserBalanceInfoFromRedis from=%v redisMasterApi.Get keyName=%s err=%s", uid, keyName, err)
		return amount, dollerAmount, currency, symbol, err
	}

	err = redisMasterApi.Expire(keyName, ExpirePeriod)
	if err != nil {
		infoLog.Printf("GetUserBalanceInfoFromRedis expire key=%s err=%s", keyName, err)
		attr := "gowalletlg/expire_key_failed"
		libcomm.AttrAdd(attr, 1)
	}

	infoBody := new(ht_wallet.CurrencyInfo)
	err = proto.Unmarshal([]byte(value), infoBody)
	if err != nil {
		infoLog.Printf("GetUserBalanceInfoFromRedis proto Unmarshal failed from=%v keyName=%s value=%s", uid, keyName, value)
		return amount, dollerAmount, currency, symbol, err
	}
	amount = infoBody.GetAmount()
	dollerAmount = infoBody.GetCorrespondingDollar()
	currency = infoBody.GetCurrencyType()
	symbol = infoBody.GetCurrencySymbol()
	return amount, dollerAmount, currency, symbol, nil
}

func GetUserBalanceInfoKeyName(uid uint32) (key string) {
	key = fmt.Sprintf("%v#balance", uid)
	return key
}

//查询db中的余额
func GetUserBalanceInfoFromDb(head *common.HeadV3, payLoad []byte) (amount, dollerAmount uint64, currency, symbol string, err error) {
	// send packet to dbd
	if head == nil || len(payLoad) == 0 {
		infoLog.Printf("GetUserBalanceInfoFromDb nil param")
		err = ErrInvalidParam
		return amount, dollerAmount, currency, symbol, err
	}

	dbdPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("GetUserBalanceInfoFromDb dbdAgentApi.SendAndRecvPacket send packet failed from=%v err=%s", head.From, err)
		return amount, dollerAmount, currency, symbol, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV3Packet)
	if !ok {
		infoLog.Printf("GetUserBalanceInfoFromDb change packet failed from=%v", head.From)
		err = ErrChagePacket
		return amount, dollerAmount, currency, symbol, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		infoLog.Printf("GetUserBalanceInfoFromDb GetHead failed from=%v err=%s", head.From, err)
		return amount, dollerAmount, currency, symbol, err
	}

	if rspHead.Ret != uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("GetUserBalanceInfoFromDb ret=%v not success", head.Ret)
		err = ErrGetResultFromDbd
		return amount, dollerAmount, currency, symbol, err
	}

	rspBody := new(ht_wallet.WalletRspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		infoLog.Printf("GetUserBalanceInfoFromDb proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return amount, dollerAmount, currency, symbol, err
	}
	subRspBody := rspBody.GetQueryBalanceInfoByUidRspbody()
	if subRspBody == nil {
		infoLog.Printf("GetUserBalanceInfoFromDb QueryBalanceInfoByUidRspbody failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		err = ErrGetResultFromDbd
		return amount, dollerAmount, currency, symbol, err
	}
	currencyInfo := subRspBody.GetCurrencyInfo()
	amount = currencyInfo.GetAmount()
	dollerAmount = currencyInfo.GetCorrespondingDollar()
	currency = currencyInfo.GetCurrencyType()
	symbol = currencyInfo.GetCurrencySymbol()
	return amount, dollerAmount, currency, symbol, nil
}

// 8、批量查询单个用户所有的收款流水
func ProcBatchGetCollectMoneyRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetCollectMoneyRecord not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetCollectMoneyRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/batch_get_collect_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetCollectMoneyRecord proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetCollectMoneyRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetCollectMoneyRecord GetBatchGetCollectMoneyRecordRspbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcBatchGetCollectMoneyRecord send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_get_collect_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcBatchGetCollectMoneyRecord dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_get_collect_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcBatchGetCollectMoneyRecord dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetCollectMoneyRecordRspbody = &ht_wallet.BatchGetCollectMoneyRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_get_collect_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	infoLog.Printf("ProcBatchGetCollectMoneyRecord recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/batch_get_collect_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 9、查询当个用户所有的提现记录
func ProcBatchGetWithdrawalsRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetWithdrawalsRecord not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetWithdrawalsRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/batch_get_withdrawals_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetWithdrawalsRecord proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetWithdrawalsRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetWithdrawalsRecord GetBatchGetWithdrawalsRecordReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcBatchGetWithdrawalsRecord send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_get_withdrawals_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcBatchGetWithdrawalsRecord dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_get_withdrawals_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcBatchGetWithdrawalsRecord dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetWithdrawalsRecordRspbody = &ht_wallet.BatchGetWithdrawalsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_get_withdrawals_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	infoLog.Printf("ProcBatchGetWithdrawalsRecord recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/batch_get_withdrawals_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 10、查询汇率
func ProcQueryExchageRate(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryExchageRate not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryExchageRate invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/query_exchange_rate_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryExchageRate proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryExchangeRateReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryExchageRate GetBatchGetWithdrawalsRecordReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcQueryExchageRate send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_exchange_rate_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcQueryExchageRate dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_exchange_rate_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcQueryExchageRate dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryExchangeRateRspbody = &ht_wallet.QueryExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_exchange_rate_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcQueryExchageRate recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/query_exchange_rate_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 11、用户申请提现
func ProcWithdrawalsReq(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcWithdrawalsReq not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcWithdrawalsReq invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/withdrawals_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcWithdrawalsReq proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetWithdrawalsReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcWithdrawalsReq GetWithdrawalsReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcWithdrawalsReq send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/withdrawals_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcWithdrawalsReq dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/withdrawals_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcWithdrawalsReq dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.WithdrawalsRspbody = &ht_wallet.WithdrawalsRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/withdrawals_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcWithdrawalsReq recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/withdrawals_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 12、添加提现账号
func ProcAddWithdrawalsAccount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcAddWithdrawalsAccount not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcAddWithdrawalsAccount invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/add_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcAddWithdrawalsAccount proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetAddWithdrawalsAccountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcAddWithdrawalsAccount GetWithdrawalsReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcAddWithdrawalsAccount send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/add_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcAddWithdrawalsAccount dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/add_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcAddWithdrawalsAccount dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.AddWithdrawalsAccountRspbody = &ht_wallet.AddWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/withdrawals_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcAddWithdrawalsAccount recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/add_account_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 13、更新提现账号
func ProcModifyWithdrawalsAccount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModifyWithdrawalsAccount not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModifyWithdrawalsAccount invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/modify_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModifyWithdrawalsAccount proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetModifyWithdrawalsAccountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcModifyWithdrawalsAccount GetModifyWithdrawalsAccountReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcModifyWithdrawalsAccount send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/modify_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcModifyWithdrawalsAccount dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/modify_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcModifyWithdrawalsAccount dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModifyWithdrawalsAccountRspbody = &ht_wallet.ModifyWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/modify_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcModifyWithdrawalsAccount recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/modify_account_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 14、删除提现账号
func ProcDelWithdrawalsAccount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcDelWithdrawalsAccount not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelWithdrawalsAccount invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/del_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelWithdrawalsAccount proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelWithdrawalsAccountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelWithdrawalsAccount GetDelWithdrawalsAccountReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcDelWithdrawalsAccount send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/del_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcDelWithdrawalsAccount dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/del_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcDelWithdrawalsAccount dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelWithdrawalsAccountRspbody = &ht_wallet.DelWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/del_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcDelWithdrawalsAccount recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/del_account_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 15、获取所有的提现账号
func ProcGetAllWithdrawalsAccount(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetAllWithdrawalsAccount not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetAllWithdrawalsAccount invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/get_all_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetAllWithdrawalsAccount proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetAllWithdrawalsAccountReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetAllWithdrawalsAccount GetGetAllWithdrawalsAccountReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcGetAllWithdrawalsAccount send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_all_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcGetAllWithdrawalsAccount dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_all_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGetAllWithdrawalsAccount dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetAllWithdrawalsAccountRspbody = &ht_wallet.GetAllWithdrawalsAccountRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_all_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcGetAllWithdrawalsAccount recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/get_all_account_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 16、查询当个用户所有的交易记录 包括购课程收款、提现、提现失败记录
func ProcBatchGetTransationRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetTransationRecord not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetTransationRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/batch_get_trans_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetTransationRecord proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetTransationRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetTransationRecord GetBatchGetTransationRecordReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcBatchGetTransationRecord send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_get_trans_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcBatchGetTransationRecord dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_get_trans_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcBatchGetTransationRecord dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetTransationRecordRspbody = &ht_wallet.BatchGetTransationRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_all_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcBatchGetTransationRecord recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/batch_get_trans_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 17、通过uinq_id 查询单个收款、提现的详情
func ProcGetTransationDetailInfo(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetTransationDetailInfo not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetTransationDetailInfo invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/get_trans_detail_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetTransationDetailInfo proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryTransDetailInfoByIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetTransationDetailInfo GetQueryTransDetailInfoByIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcGetTransationDetailInfo send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_trans_detail_info_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcGetTransationDetailInfo dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_trans_detail_info_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGetTransationDetailInfo dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryTransDetailInfoByIdRspbody = &ht_wallet.QueryTransDetailInfoByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_trans_detail_info_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcGetTransationDetailInfo recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/get_trans_detail_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 18、通过唯一id查询单个提现记录
func ProcGetWithdrawalsRecordById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetWithdrawalsRecordById not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetWithdrawalsRecordById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/get_withdrawals_by_id_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetWithdrawalsRecordById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetWithdrawalsRecordByIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetWithdrawalsRecordById GetGetWithdrawalsRecordByIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcGetWithdrawalsRecordById send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_withdrawals_by_id_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcGetWithdrawalsRecordById dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_withdrawals_by_id_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGetWithdrawalsRecordById dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetWithdrawalsRecordByIdRspbody = &ht_wallet.GetWithdrawalsRecordByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_trans_detail_info_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcGetWithdrawalsRecordById recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/get_withdrawals_by_id_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 19、查询单个群课程的是否收费和用户是否已经购买
func ProcQueryLessonAndPurchaseStat(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryLessonAndPurchaseStat not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryLessonAndPurchaseStat invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/query_lesson_and_purchase_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryLessonAndPurchaseStat proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryLessonAndPurchaseStatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryLessonAndPurchaseStat GetQueryLessonAndPurchaseStatReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcQueryLessonAndPurchaseStat send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_lesson_and_purchase_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcQueryLessonAndPurchaseStat dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_lesson_and_purchase_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcQueryLessonAndPurchaseStat dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryLessonAndPurchaseStatRspbody = &ht_wallet.QueryLessonAndPurchaseStatRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_lesson_and_purchase_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcQueryLessonAndPurchaseStat recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/query_lesson_and_purchase_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 20、成功购买某个课程
func ProcSuccessPurchaseLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcSuccessPurchaseLesson not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSuccessPurchaseLesson invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/succ_purchase_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLesson proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSuccessPurchaseLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSuccessPurchaseLesson GetSuccessPurchaseLessonReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLesson send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/succ_purchase_lesson_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcSuccessPurchaseLesson dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/succ_purchase_lesson_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcSuccessPurchaseLesson dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/succ_purchase_lesson_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcSuccessPurchaseLesson recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/succ_purchase_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 21、查询某个群是否存在课程正在收费中
func ProcQueryRoomExistLessonCharging(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryRoomExistLessonCharging not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryRoomExistLessonCharging invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/query_room_exist_charging_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryRoomExistLessonCharging proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryRoomExistLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryRoomExistLessonCharging GetQueryRoomExistLessonRspbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcQueryRoomExistLessonCharging send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_room_exist_charging_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcQueryRoomExistLessonCharging dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_room_exist_charging_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcQueryRoomExistLessonCharging dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryRoomExistLessonRspbody = &ht_wallet.QueryRoomExistLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_room_exist_charging_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcQueryRoomExistLessonCharging recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/query_room_exist_charging_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 22、成功购买某个课程Json请求
func ProcSuccessPurchaseLessonJsonReq(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendJsonRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcSuccessPurchaseLessonJsonReq not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSuccessPurchaseLessonJsonReq invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/succ_purchase_lesson_json_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := json.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLessonJsonReq json Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSuccessPurchaseLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSuccessPurchaseLessonJsonReq GetSuccessPurchaseLessonReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	s, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLessonJsonReq GetSuccessPurchaseLessonReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("protoc marshal failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbHead := new(common.HeadV3)
	*dbHead = *head
	dbHead.Cmd = uint16(ht_wallet.WALLET_CMD_TYPE_CMD_SUCC_PURCHASE_LESSON_REQ)
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(dbHead, s)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLessonJsonReq send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/succ_purchase_lesson_json_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcSuccessPurchaseLessonJsonReq dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/succ_purchase_lesson_json_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcSuccessPurchaseLessonJsonReq dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/succ_purchase_lesson_json_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcSuccessPurchaseLessonJsonReq recv dbd ret code=%v", dbRespHeadV3.Ret)
	// 将pb序列化之后的响应改成json序列的响应
	dbRspPayLoad := dbPacketHeadV3.GetBody()
	dbRspBody := new(ht_wallet.WalletRspBody)
	err = proto.Unmarshal(dbRspPayLoad, dbRspBody)
	if err != nil {
		infoLog.Printf("ProcSuccessPurchaseLessonJsonReq protoc unmarshl failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.SuccessPurchaseLessonRspbody = &ht_wallet.SuccessPurchaseLessonRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("protoc unmarshal failed"),
			},
		}
		return false
	}
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendJsonRsp(c, head, dbRspBody, result)
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/succ_purchase_lesson_json_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 23、后台审批操作
func ProcAuditWithdrawalsRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcAuditWithdrawalsRecord not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcAuditWithdrawalsRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/audit_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcAuditWithdrawalsRecord proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetAuditWithdrawalsRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcAuditWithdrawalsRecord GetAuditWithdrawalsRecordRspbody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcAuditWithdrawalsRecord send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/audit_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcAuditWithdrawalsRecord dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/audit_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcAuditWithdrawalsRecord dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.AuditWithdrawalsRecordRspbody = &ht_wallet.AuditWithdrawsRecordRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/audit_record_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcAuditWithdrawalsRecord recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/audit_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 24、获取单个提现账号
func ProcGetWithdrawalsAccountById(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetWithdrawalsAccountById not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetWithdrawalsAccountById invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/get_account_by_id_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetWithdrawalsAccountById proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetWithdrawalsAccountByIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetWithdrawalsAccountById GetGetWithdrawalsAccountByIdReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcGetWithdrawalsAccountById send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_account_by_id_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcGetWithdrawalsAccountById dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_account_by_id_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcGetWithdrawalsAccountById dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetWithdrawalsAccountByIdRspbody = &ht_wallet.GetWithDrawalsAccountByIdRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/get_account_by_id_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcGetWithdrawalsAccountById recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/get_account_by_id_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 25、查询单个课程是否正在收费中
func ProcQueryLessonIsCharging(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryLessonIsCharging not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryLessonIsCharging invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/query_lesson_is_charging_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryLessonIsCharging proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryLessonIsChargingReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryLessonIsCharging GetQueryLessonIsChargingReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 检查输入参数 如果参数不合法直接返回不用去到dbd了
	reqUid := subReqBody.GetReqUid()
	roomId := subReqBody.GetRoomId()
	lessonObid := subReqBody.GetLessonObid()
	if reqUid == 0 || roomId == 0 || lessonObid == "" {
		infoLog.Printf("ProcQueryLessonIsCharging invalid param reqUid=%v roomId=%v lessonObid=%s", reqUid, roomId, lessonObid)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcQueryLessonIsCharging send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_lesson_is_charging_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcQueryLessonIsCharging dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_lesson_is_charging_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcQueryLessonIsCharging dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryLessonIsChargingRspbody = &ht_wallet.QueryLessonIsChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/query_lesson_is_charging_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcQueryLessonIsCharging recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/query_lesson_is_charging_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 26、后台转账成功操作
func ProcTransferSuccRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcTransferSuccRecord not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcTransferSuccRecord invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/tran_succ_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcTransferSuccRecord proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetTransferSuccessReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcTransferSuccRecord GetTransferSuccessReqbody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcTransferSuccRecord send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/tran_succ_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcTransferSuccRecord dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/tran_succ_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcTransferSuccRecord dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.TransferSuccessRspbody = &ht_wallet.TransferSuccessRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/tran_succ_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcTransferSuccRecord recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/tran_succ_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 27、更新汇率
func ProcUpdateExchangeRate(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateExchangeRate not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateExchangeRate invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/update_exchange_rate_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateExchangeRate proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateExchangeRateReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateExchangeRate GetUpdateExchangeRateReqbody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateExchangeRate send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/update_exchange_rate_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcUpdateExchangeRate dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/update_exchange_rate_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateExchangeRate dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateExchangeRateRspbody = &ht_wallet.UpdateExchangeRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/update_exchange_rate_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateExchangeRate recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/update_exchange_rate_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 28、删除客人课程后批量停止课程
func ProcBatchStopLessonCharge(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchStopLessonCharge not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchStopLessonCharge invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/batch_stop_charge_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchStopLessonCharge proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchStopLessonChargingReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchStopLessonCharge BatchStopLessonChargingReqBody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcBatchStopLessonCharge send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_stop_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcBatchStopLessonCharge dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_stop_charge_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcBatchStopLessonCharge dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchStopLessonChargingRspbody = &ht_wallet.BatchStopLessonChargingRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/batch_stop_charge_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcBatchStopLessonCharge recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/batch_stop_charge_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 29、更新提现平台费率
func ProcUpdateWithdrawalsRate(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_wallet.WalletRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateWithdrawalsRate not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INVALID_PARAM)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateWithdrawalsRate invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gowalletlg/update_withdraw_rate_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_wallet.WalletReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateWithdrawalsRate proto.Unmarshal failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateWithdrawalsRateReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateWithdrawalsRate GetUpdateWithdrawalsRateReqbody() failed from=%v cmd=0x%4x seq=%v err=%s", head.From, head.Cmd, head.Seq, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_PB_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateWithdrawalsRate send to dbd failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/update_withdraw_rate_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV3, ok := dbPacket.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("ProcUpdateWithdrawalsRate dbpacket can not change to HeadV3packet from=%v", head.From)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/update_withdraw_rate_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	dbRespHeadV3, err := dbPacketHeadV3.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateWithdrawalsRate dbpacket Get head failed from=%v err=%s", head.From, err)
		result = uint16(ht_wallet.WALLET_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateWithdrawalsRateRspbody = &ht_wallet.UpdateWithdrawalsRateRspBody{
			Status: &ht_wallet.WalletHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gowalletlg/update_withdraw_rate_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateWithdrawalsRate recv dbd ret code=%v", dbRespHeadV3.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV3)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gowalletlg/update_withdraw_rate_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV3, rspBody *ht_wallet.WalletRspBody, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRsp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV3Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func SendJsonRsp(c *gotcp.Conn, reqHead *common.HeadV3, rspBody *ht_wallet.WalletRspBody, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}

	s, err := json.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendJsonRsp json.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	// infoLog.Printf("SendJsonRsp slic=%s", s)
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendJsonRsp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustString("6379")
	infoLog.Printf("cache dbd ip=%v port=%v", redisIp, redisPort)
	dbdAgentApi = common.NewSrvToSrvApiV3(dbdIp, dbdPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, 100)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
