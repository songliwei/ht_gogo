package main

import (
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"sync"
	"syscall"

	"github.com/alexjlockwood/gcm"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/gansidui/gotcp/libcomm"
	"github.com/jessevdk/go-flags"
	"github.com/nsqio/go-nsq"
	"gopkg.in/ini.v1"
)

var wg *sync.WaitGroup
var infoLog *log.Logger

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

const (
	GCM_PUSH_NOPREVIEW     = "no_preview"
	GCM_PUSH_TEXT          = "text"
	GCM_PUSH_VOICE         = "voice"
	GCM_PUSH_IMAGE         = "image"
	GCM_PUSH_LOCATION      = "location"
	GCM_PUSH_INTRODUCE     = "introduce"
	GCM_PUSH_CORRECT       = "correction"
	GCM_PUSH_STICKER       = "sticker"
	GCM_PUSH_DOODLE        = "doodle"
	GCM_PUSH_INVITE        = "friend_invite"
	GCM_PUSH_LX            = "language_exchange"
	GCM_PUSH_LX_REPLY      = "language_exchange_reply" // Reply Code "Declined" "Accepted" "Terminated"
	GCM_PUSH_GIFT          = "gift"
	GCM_PUSH_CALL_INCOMING = "call_incoming"
	GCM_PUSH_CALL_CANCEL   = "call_cancel"
	GCM_PUSH_CALL_MISS     = "call_miss"
	GCM_PUSH_ACCEPT_INVITE = "accept_invite"
	GCM_PUSH_VIDEO         = "video" // add video push
	GCM_PUSH_GVOIP         = "gvoip"
	GCM_PUSH_LINK          = "message_preview_example_no"
	GCM_PUSH_CARD          = "card"

	// 2016-08-26 add by songliwei
	GCM_PUSH_FOLLOW              = "s_has_followed_you"
	GCM_PUSH_REPLY_YOUR_COMMENT  = "s_replied_your_comment"
	GCM_PUSH_COMMENTED_YOUR_POST = "s_commented_your_post"
	GCM_PUSH_CORRECTED_YOUR_POST = "s_corrected_your_post"
	GCM_PUSH_POST_MNT            = "s_post_mnt"
	GCM_PUSH_CREATE_WHITE_BOARD  = "s_create_white_board"
	GCM_PUSH_NEW_MSG_NOTIFY      = "s_new_msg_notify"
	GMC_PUSH_GROUP_LESSON        = "s_group_lesson"
	GCM_PUSH_START_CHARGE        = "s_start_charge"
)

const (
	APIKey = "AIzaSyCOu1hU2Moz-GuqpjiLavpoNUNwfxrwxBc"
)

var options Options
var parser = flags.NewParser(&options, flags.Default)

func MessageHandle(message *nsq.Message) error {
	// 统计gcm处理总量
	attr := "gcm/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	// log.Printf("MessageHandle Got a message: %v", message)
	wg.Add(1)
	go func() {
		PostData(message)
		wg.Done()
	}()

	return nil
}

func PostData(message *nsq.Message) error {
	// 然后进行下一步无处理
	rootObj, err := simplejson.NewJson(message.Body)
	if err != nil {
		log.Printf("ProcData simplejson new packet error", err)
		return err
	}
	// log.Printf("ProcData rootObj=%#v", rootObj)
	intPushType := rootObj.Get("int_push_type").MustInt(0)
	regId := rootObj.Get("registration_id").MustString("")
	registerionIds := []string{regId}

	var textVal, voiceVal, pictureVal, videoVal uint8
	strPushType := rootObj.Get("push_type").MustString("")
	fromId := uint32(rootObj.Get("from_id").MustInt64(0))
	data := map[string]interface{}{"type": strPushType}

	if strPushType != GCM_PUSH_NOPREVIEW {
		data["from_id"] = fromId
		data["sender"] = rootObj.Get("sender").MustString("")
	}
	if strPushType == GCM_PUSH_TEXT {
		data["message"] = rootObj.Get("push_param").MustString("")
	}

	if strPushType == GCM_PUSH_INTRODUCE {
		data["user"] = rootObj.Get("push_param").MustString("")
	}

	if strPushType == GCM_PUSH_LX_REPLY {
		data["reply_code"] = rootObj.Get("push_param").MustString("")
	}

	if strPushType == GCM_PUSH_TEXT {
		textVal = 1
	} else if strPushType == GCM_PUSH_VOICE {
		voiceVal = 1
	} else if strPushType == GCM_PUSH_IMAGE {
		pictureVal = 1
	} else if strPushType == GCM_PUSH_VIDEO {
		videoVal = 1
	}

	data["sound"] = rootObj.Get("sound").MustInt64(0)
	data["msg_id"] = strings.Trim(rootObj.Get("msg_id").MustString(""), "\u0000")
	toId := uint32(rootObj.Get("to_id").MustInt64(0))
	data["to_id"] = toId
	data["actionId"] = rootObj.Get("action_id").MustInt64(0)
	data["byAt"] = rootObj.Get("by_at").MustInt64(0)
	data["chat_type"] = rootObj.Get("chat_type").MustInt(0)
	if strPushType == GCM_PUSH_POST_MNT {
		data["content"] = strings.Trim(rootObj.Get("push_param").MustString(""), "\u0000")
	}
	// Create the message to be sent.
	pushMsg := gcm.NewMessage(data, registerionIds...)
	pushMsg.Priority = "high"
	// Create a Sender to send the message.
	sender := &gcm.Sender{ApiKey: APIKey}
	// Send the message and receive the response after at most two retries.
	response, err := sender.Send(pushMsg, 2)
	log.Printf("ProcData data=%#v response = %#v", data, response)
	if err == nil && response.Success == 1 && response.Failure == 0 {
		// 统计gcm成功重量
		attr := "gcm/total_push_succ_count"
		libcomm.AttrAdd(attr, 1)
	} else {
		// 统计gcm失败量
		attr := "gcm/total_push_failed_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("Failed to send message toId=%v err=%s", toId, err)
	}
	WriteStaticLog(fromId, toId, intPushType, 1, textVal, voiceVal, pictureVal, videoVal)
	return nil
}

func WriteStaticLog(fromId, toId uint32, pushType int, terminalType, text, voice, picture, video uint8) {
	contentObj := simplejson.New()
	contentObj.Set("text", text)
	contentObj.Set("voice", voice)
	contentObj.Set("picture", picture)
	contentObj.Set("video", video)
	logObj := simplejson.New()
	logObj.Set("fromid", fromId)
	logObj.Set("toid", toId)
	logObj.Set("type", pushType)
	logObj.Set("terminal", terminalType)
	logObj.Set("content", contentObj)
	logObj.Set("createtime", time.Now().Unix())
	logSlic, err := logObj.MarshalJSON()
	if err != nil {
		log.Printf("WriteStaticLog fromId=%v toId=%v pushType=%v terminalType=%v type=%v voice=%v picture=%v video=%v err=%s",
			fromId,
			toId,
			pushType,
			terminalType,
			pushType,
			voice,
			picture,
			video,
			err)
		return
	}
	infoLog.Printf("%s", logSlic)
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}
	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	wg = &sync.WaitGroup{}
	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
