package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_relation"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	Cmd int `short:"t" long:"cmd" description:"Command type" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(12750)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	v3Protocol := &common.HeadV3Protocol{}
	var head *common.HeadV3
	head = &common.HeadV3{
		Flag:     0xF0,
		Version:  4,
		CryKey:   0,
		TermType: 0,
		Cmd:      0,
		Seq:      1,
		From:     2325928,
		To:       0,
		Len:      0,
		Ret:      0,
	}

	var payLoad []byte
	reqBody := new(ht_relation.ReqBody)
	switch options.Cmd {
	case 8:
		head.Cmd = 8
		subReqBody := &ht_relation.GetFollowerListReqBody{
			UidList: []uint32{2325928},
		}
		reqBody.GetFollowerListReqbody = subReqBody
	// 添加到分组中
	case 17:
		head.Cmd = 17
		subReqBody := &ht_relation.AddToGroupReqBody{
			GroupName: []byte("special_follower"),
			UidList:   []uint32{3249820},
		}
		reqBody.AddToGroupReqbody = subReqBody
	case 18:
		head.Cmd = 18
		subReqBody := &ht_relation.RemoveFromGroupReqBody{
			GroupName: []byte("special_follower"),
			UidList:   []uint32{900866},
		}
		reqBody.RemoveFromGroupReqbody = subReqBody
	case 19:
		head.Cmd = 19
		subReqBody := &ht_relation.ModifyGroupNameReqBody{
			OldGroupName: []byte("special_follower"),
			NewGroupName: []byte("trival"),
			UidList:      []uint32{900866},
		}
		reqBody.ModifyGroupNameReqbody = subReqBody
	default:
		infoLog.Println("UnKnow input cmd =", options.Cmd)
	}

	payLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v cmd=%v seq=%v",
			head.From,
			head.Cmd,
			head.Seq)
		return
	}

	head.Len = uint32(common.PacketV3HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV3ToSlice failed")
		return
	}
	copy(buf[common.PacketV3HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	infoLog.Printf("len=%v payLaod=%s\n", len(payLoad), payLoad)
	// write
	conn.Write(buf)
	// read
	p, err := v3Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket := p.(*common.HeadV3Packet)
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("resp len=%v cmd=%v\n", rspHead.Len, rspHead.Cmd)
		// protobuf unmarshal
		rspBody := &ht_relation.RspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Println("proto Unmarshal failed")
			return
		}
		switch rspHead.Cmd {
		case 8:
			subRspBody := rspBody.GetGetFollowerListRspbody()
			followerList := subRspBody.GetFollowerList()
			for _, v := range followerList {
				baseList := v.GetBaseList()
				infoLog.Printf("follower uid=%v followerSize=%v", v.GetUid(), len(v.GetBaseList()))
				for j, z := range baseList {
					infoLog.Printf("index=%v followeruid=%v groupListLen=%v", j, z.GetUid(), len(z.GetGroupInfo()))
					groupList := z.GetGroupInfo()
					for _, w := range groupList {
						infoLog.Printf("index=%v, name=%s ts=%v", j, w.GetGroupName(), w.GetAddTime())
					}
				}
			}
		case 17:
			subRspBody := rspBody.GetAddToGroupRspbody()
			infoLog.Printf("GetAddToGroupRspbody rsp ts=%v", subRspBody.GetLastTimestamp())
		case 18:
			subRspBody := rspBody.GetRemoveFromGroupRspbody()
			infoLog.Printf("GetRemoveFromGroupRspbody rsp ts=%v", subRspBody.GetLastTimestamp())
		case 19:
			subRspBody := rspBody.GetModifyGroupNameRspbody()
			infoLog.Printf("GetModifyGroupNameRspbody rsp ts=%v", subRspBody.GetLastTimestamp())
		default:
			infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
		}
	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
