package main

import (
	"fmt"
	"runtime/pprof"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_relation"
	"github.com/HT_GOGO/gotcp/tcpfw/project/relation/util"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	ssdbOperator   *util.SsdbOperator
	cpuProfile     string
	memProfile     string
	memProfileRate int
)

const (
	ProcSlowThreshold = 300000
	HashMapSizeLimit  = 1000
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV3packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v from=%v to=%v", head.Cmd, head.Seq, head.From, head.To)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gorelation/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint16(ht_relation.CMD_TYPE_CMD_ADD_TO_GROUP):
		go ProcAddToGroup(c, head, packet)
	case uint16(ht_relation.CMD_TYPE_CMD_REMOVE_FROM_GROUP):
		go ProcRemoveFromGroup(c, head, packet)
	case uint16(ht_relation.CMD_TYPE_CMD_MODIFY_GROUP_NAME):
		go ProcModifyGroupName(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

func GetFollowingHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#following", uid)
	return hashName
}

func GetFollowerHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#follower", uid)
	return hashName
}

// 1.proc add to group 修改发起方的关注列表和被关注方的粉丝列表
func ProcAddToGroup(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_relation.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_relation.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcAddToGroup no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_relation.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.AddToGroupRspbody = &ht_relation.AddToGroupRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		infoLog.Printf("ProcAddToGroup invalid param uid=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gorelation/add_to_group_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_relation.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcAddToGroup proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_relation.RET_CODE_RET_PB_ERR)
		rspBody.AddToGroupRspbody = &ht_relation.AddToGroupRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		return false
	}
	subReqBody := reqBody.GetAddToGroupReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcAddToGroup GetAddToGroupReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_relation.RET_CODE_RET_PB_ERR)
		rspBody.AddToGroupRspbody = &ht_relation.AddToGroupRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		return false
	}
	groupName := subReqBody.GetGroupName()
	uidList := subReqBody.GetUidList()
	fromHashName := GetFollowingHashMapName(head.From)
	updateTs := uint64(time.Now().Unix())
	for i, v := range uidList {
		// Step1: 首先修改发起方的关注列表
		infoLog.Printf("ProcAddToGroup index=%v from=%v uid=%v groupName=%s", i, head.From, v, groupName)
		// add v to group
		err = ssdbOperator.AddUserToGroup(fromHashName, v, groupName, updateTs)
		if err != nil {
			infoLog.Printf("ProcAddToGroup ssdbOperator.AddUserToGroup failed hashName=%v uid=%v groupName=%v err=%s",
				fromHashName,
				v,
				groupName,
				err)
			attr := "gorelation/update_ssdb_group_err"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		// Step2: 然后修改被关注方的粉丝列表
		targetHashName := GetFollowerHashMapName(v)
		err = ssdbOperator.AddUserToGroup(targetHashName, head.From, groupName, updateTs)
		if err != nil {
			infoLog.Printf("ProcAddToGroup ssdbOperator.AddUserToGroup failed hashName=%v uid=%v groupName=%v err=%s",
				targetHashName,
				v,
				groupName,
				err)
			attr := "gorelation/update_ssdb_group_err"
			libcomm.AttrAdd(attr, 1)
			continue
		}
	}

	rspBody.AddToGroupRspbody = &ht_relation.AddToGroupRspBody{
		LastTimestamp: proto.Uint32(uint32(updateTs)),
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gorelation/add_to_group_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.proc remove from group 修改发起方的关注列表和被关注方的粉丝列表
func ProcRemoveFromGroup(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_relation.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_relation.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcRemoveFromGroup no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_relation.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.RemoveFromGroupRspbody = &ht_relation.RemoveFromGroupRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		infoLog.Printf("ProcRemoveFromGroup invalid param uid=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gorelation/remove_from_group_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_relation.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcRemoveFromGroup proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_relation.RET_CODE_RET_PB_ERR)
		rspBody.RemoveFromGroupRspbody = &ht_relation.RemoveFromGroupRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		return false
	}
	subReqBody := reqBody.GetRemoveFromGroupReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcRemoveFromGroup GetRemoveFromGroupReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_relation.RET_CODE_RET_PB_ERR)
		rspBody.RemoveFromGroupRspbody = &ht_relation.RemoveFromGroupRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		return false
	}
	groupName := subReqBody.GetGroupName()
	uidList := subReqBody.GetUidList()
	fromHashName := GetFollowingHashMapName(head.From)
	updateTs := uint64(time.Now().Unix())
	for i, v := range uidList {
		// Step1: 首先修改发起方的关注列表
		infoLog.Printf("ProcRemoveFromGroup index=%v from=%v uid=%v groupName=%s", i, head.From, v, groupName)
		// add v to group
		err = ssdbOperator.RemoveUserFromGroup(fromHashName, v, groupName, updateTs)
		if err != nil {
			infoLog.Printf("ProcRemoveFromGroup ssdbOperator.RemoveUserFromGroup failed hashName=%v uid=%v groupName=%v err=%s",
				fromHashName,
				v,
				groupName,
				err)
			attr := "gorelation/update_ssdb_group_err"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		// Step2: 然后修改被关注方的粉丝列表
		targetHashName := GetFollowerHashMapName(v)
		err = ssdbOperator.RemoveUserFromGroup(targetHashName, head.From, groupName, updateTs)
		if err != nil {
			infoLog.Printf("ProcRemoveFromGroup ssdbOperator.RemoveUserFromGroup failed hashName=%v uid=%v groupName=%v err=%s",
				targetHashName,
				v,
				groupName,
				err)
			attr := "gorelation/update_ssdb_group_err"
			libcomm.AttrAdd(attr, 1)
			continue
		}
	}

	rspBody.RemoveFromGroupRspbody = &ht_relation.RemoveFromGroupRspBody{
		LastTimestamp: proto.Uint32(uint32(updateTs)),
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gorelation/remove_from_group_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 3.proc modify group name 修改发起方的关注列表和被关注方的粉丝列表
func ProcModifyGroupName(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := uint16(ht_relation.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_relation.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModifyGroupName no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_relation.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModifyGroupNameRspbody = &ht_relation.ModifyGroupNameRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		infoLog.Printf("ProcModifyGroupName invalid param uid=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gorelation/modify_group_name_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_relation.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModifyGroupName proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_relation.RET_CODE_RET_PB_ERR)
		rspBody.ModifyGroupNameRspbody = &ht_relation.ModifyGroupNameRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		return false
	}
	subReqBody := reqBody.GetModifyGroupNameReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcModifyGroupName GetModifyGroupNameReqbody() failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint16(ht_relation.RET_CODE_RET_PB_ERR)
		rspBody.ModifyGroupNameRspbody = &ht_relation.ModifyGroupNameRspBody{
			LastTimestamp: proto.Uint32(0),
		}
		return false
	}
	oldName := subReqBody.GetOldGroupName()
	newName := subReqBody.GetNewGroupName()
	uidList := subReqBody.GetUidList()
	fromHashName := GetFollowingHashMapName(head.From)
	updateTs := uint64(time.Now().Unix())
	for i, v := range uidList {
		// Step1: 首先修改发起方的关注列表
		infoLog.Printf("ProcModifyGroupName index=%v from=%v uid=%v oldName=%s newName=%s", i, head.From, v, oldName, newName)
		// add v to group
		err = ssdbOperator.ModifyGroupName(fromHashName, v, oldName, newName, updateTs)
		if err != nil {
			infoLog.Printf("ProcModifyGroupName ssdbOperator.ModifyGroupName failed hashName=%v uid=%v oldName=%v newName=%s err=%s",
				fromHashName,
				v,
				oldName,
				newName,
				err)
			attr := "gorelation/update_ssdb_group_err"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		// Step2: 然后修改被关注方的粉丝列表
		targetHashName := GetFollowerHashMapName(v)
		err = ssdbOperator.ModifyGroupName(targetHashName, head.From, oldName, newName, updateTs)
		if err != nil {
			infoLog.Printf("ProcModifyGroupName ssdbOperator.ModifyGroupName failed hashName=%v uid=%v oldName=%v newName=%s err=%s",
				targetHashName,
				v,
				oldName,
				newName,
				err)
			attr := "gorelation/update_ssdb_group_err"
			libcomm.AttrAdd(attr, 1)
			continue
		}
	}

	rspBody.ModifyGroupNameRspbody = &ht_relation.ModifyGroupNameRspBody{
		LastTimestamp: proto.Uint32(uint32(updateTs)),
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gorelation/modify_group_name_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV3, resp *ht_relation.RspBody, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV3Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV3ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV3Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV3MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV3Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func startCPUProfile() {
	if cpuProfile != "" {
		f, err := os.Create(cpuProfile)
		if err != nil {
			infoLog.Printf("Can not create cpu profile output file: %s", err)
			return
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			infoLog.Printf("Can not start cpu profile: %s", err)
			f.Close()
			return
		}
	}
}

func stopCPUProfile() {
	if cpuProfile != "" {
		pprof.StopCPUProfile() // 把记录的概要信息写到已指定的文件
	}
}

func startMemProfile() {
	if memProfile != "" && memProfileRate > 0 {
		runtime.MemProfileRate = memProfileRate
	}
}

func stopMemProfile() {
	if memProfile != "" {
		f, err := os.Create(memProfile)
		if err != nil {
			infoLog.Printf("Can not create mem profile output file: %s", err)
			return
		}
		if err = pprof.WriteHeapProfile(f); err != nil {
			infoLog.Printf("Can not write %s: %s", memProfile, err)
		}
		f.Close()
	}
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// read profile config path
	// cpuProfile = cfg.Section("PROFILE").Key("cpu_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/cpu.prof")
	// memProfile = cfg.Section("PROFILE").Key("mem_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/mem.out")
	// memProfileRate = cfg.Section("PROFILE").Key("mem_rate").MustInt(512 * 1024)
	// read ssdb config
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	ssdbOperator = util.NewSsdbOperator(ssdbApi, infoLog)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	//startCPUProfile()
	//startMemProfile()
	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	//stopCPUProfile()
	//stopMemProfile()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
