// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"fmt"
	"log"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_relation_store"
	"github.com/golang/protobuf/proto"
)

// Error type
var (
	ErrNotExistInReids = errors.New("not exist in redis")
	ErrNilDbObject     = errors.New("not set object current is nil")
	ErrNotFollowing    = errors.New("not following user")
)

const (
	HashKeyFollowingTs = "update_ts"
)

type SsdbOperator struct {
	ssdbApi *common.SsdbApi
	infoLog *log.Logger
}

func NewSsdbOperator(targetSsdbApi *common.SsdbApi, logger *log.Logger) *SsdbOperator {
	return &SsdbOperator{
		ssdbApi: targetSsdbApi,
		infoLog: logger,
	}
}

// 添加到分组中
func AddToGroupList(groupList []*ht_relation_store.GroupInfo, groupName []byte, updateTs uint64) (newGroupList []*ht_relation_store.GroupInfo, err error) {
	// 首先copy 已经加入的分组
	newGroupList = groupList
	if len(groupList) != 0 {
		strSetName := string(groupName)
		for _, v := range groupList {
			storeName := string(v.GetGroupName())
			if strSetName == storeName {
				return newGroupList, nil // 如果名字已存在直接返回false
			}
		}
	}
	item := &ht_relation_store.GroupInfo{
		GroupName: groupName,
		AddTime:   proto.Uint32(uint32(updateTs)),
	}
	newGroupList = append(newGroupList, item)
	return newGroupList, nil
}

// 从分组中删除
func RemoveFromGroupList(groupList []*ht_relation_store.GroupInfo, groupName []byte, updateTs uint64) (newGroupList []*ht_relation_store.GroupInfo, err error) {
	if len(groupList) == 0 {
		return newGroupList, nil
	}
	strRemoveName := string(groupName)
	for _, v := range groupList {
		storeName := string(v.GetGroupName())
		if strRemoveName != storeName {
			item := &ht_relation_store.GroupInfo{
				GroupName: groupName,
				AddTime:   proto.Uint32(uint32(updateTs)),
			}
			newGroupList = append(newGroupList, item)
		}
	}
	return newGroupList, nil
}

// 添加到分组中
func (this *SsdbOperator) AddUserToGroup(hashName string, uid uint32, groupName []byte, updateTs uint64) (err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return err
	}

	// Step1: 更新following 用户所在分组
	key := fmt.Sprintf("%v", uid)
	value, err := this.ssdbApi.Hget(hashName, key)
	if err != nil {
		this.infoLog.Printf("AddUserToGroup hashName=%s key=%s ssdbApi.Hget return err=%s", hashName, key, err)
		return err
	}
	if value == "" {
		this.infoLog.Printf("AddUserToGroup hashName=%s key=%s value=%s not following",
			hashName,
			key,
			value)
		err = ErrNotFollowing
		return err
	}

	this.infoLog.Printf("AddUserToGroup hashName=%v key=%v valuelen=%v value=%v", hashName, key, len(value), value)
	storeRelation := new(ht_relation_store.StoreRelationData)
	err = proto.Unmarshal([]byte(value), storeRelation)
	if err != nil {
		this.infoLog.Printf("AddUserToGroup proto Unmarshal failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)

		return err
	}
	groupList := storeRelation.GetGroupList()

	newGroupList, err := AddToGroupList(groupList, groupName, updateTs)
	if err != nil {
		this.infoLog.Printf("AddUserToGroup proto Unmarshal failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)
		return err
	}
	storeRelation.GroupList = newGroupList
	strRelation, err := proto.Marshal(storeRelation)
	if err != nil {
		this.infoLog.Printf("AddUserToGroup Failed to proto.Marshal hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)
		return err
	}

	this.infoLog.Printf("AddUserToGroup hashName=%v key=%v new valuelen=%v newValue=%v", hashName, key, len(strRelation), strRelation)
	err = this.ssdbApi.Hset(hashName, key, string(strRelation))
	if err != nil {
		this.infoLog.Printf("AddUserToGroup update following ts failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)
		return err
	}
	// Step2: 更新关注列表的时间戳版本
	err = this.ssdbApi.Hset(hashName, HashKeyFollowingTs, fmt.Sprintf("%v", updateTs))
	if err != nil {
		this.infoLog.Printf("AddUserToGroup update following ts failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)
		return err
	}
	return nil
}

// 从分组中删除
func (this *SsdbOperator) RemoveUserFromGroup(hashName string, uid uint32, groupName []byte, updateTs uint64) (err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return err
	}

	// Step1: 更新following 用户所在分组
	key := fmt.Sprintf("%v", uid)
	value, err := this.ssdbApi.Hget(hashName, key)
	if err != nil {
		this.infoLog.Printf("RemoveUserFromGroup hashName=%v key=%v ssdbApi.Hget return err=%s", hashName, key, err)
		return err
	}
	if value == "" {
		this.infoLog.Printf("RemoveUserFromGroup hashName=%s key=%s value=%s not following",
			hashName,
			key,
			value)
		err = ErrNotFollowing
		return err
	}

	storeRelation := new(ht_relation_store.StoreRelationData)
	err = proto.Unmarshal([]byte(value), storeRelation)
	if err != nil {
		this.infoLog.Printf("RemoveUserFromGroup proto Unmarshal failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)

		return err
	}
	groupList := storeRelation.GetGroupList()
	newGroupList, err := RemoveFromGroupList(groupList, groupName, updateTs)
	if err != nil {
		this.infoLog.Printf("RemoveUserFromGroup proto Unmarshal failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)
		return err
	}
	storeRelation.GroupList = newGroupList
	strRelation, err := proto.Marshal(storeRelation)
	if err != nil {
		this.infoLog.Printf("RemoveUserFromGroup Failed to proto.Marshal hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)
		return err
	}

	err = this.ssdbApi.Hset(hashName, key, strRelation)
	if err != nil {
		this.infoLog.Printf("RemoveUserFromGroup update following ts failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)
		return err
	}
	// Step2: 更新关注列表的时间戳版本
	err = this.ssdbApi.Hset(hashName, HashKeyFollowingTs, fmt.Sprintf("%v", updateTs))
	if err != nil {
		this.infoLog.Printf("RemoveUserFromGroup update following ts failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			groupName,
			updateTs,
			err)
		return err
	}
	return nil
}

// 修改分组的名称
func (this *SsdbOperator) ModifyGroupName(hashName string, uid uint32, oldName []byte, newName []byte, updateTs uint64) (err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return err
	}

	// Step1: 更新following 用户所在分组
	key := fmt.Sprintf("%v", uid)
	value, err := this.ssdbApi.Hget(hashName, key)
	if err != nil {
		this.infoLog.Printf("ModifyGroupName hashName=%v key=%v ssdbApi.Hget return err=%s", hashName, key, err)
		return err
	}
	if value == "" {
		this.infoLog.Printf("RemoveUserFromGroup hashName=%s key=%s value=%s not following",
			hashName,
			key,
			value)
		err = ErrNotFollowing
		return err
	}
	storeRelation := new(ht_relation_store.StoreRelationData)
	err = proto.Unmarshal([]byte(value), storeRelation)
	if err != nil {
		this.infoLog.Printf("ModifyGroupName proto Unmarshal failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			newName,
			updateTs,
			err)

		return err
	}
	// 首先删除oldName
	groupList := storeRelation.GetGroupList()
	newGroupList, err := RemoveFromGroupList(groupList, oldName, updateTs)
	if err != nil {
		this.infoLog.Printf("ModifyGroupName RemoveFromGroupList failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			oldName,
			updateTs,
			err)
		return err
	}
	// 添加newName
	newGroupList, err = AddToGroupList(newGroupList, newName, updateTs)
	if err != nil {
		this.infoLog.Printf("ModifyGroupName AddToGroupList failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			newName,
			updateTs,
			err)
		return err
	}

	storeRelation.GroupList = newGroupList
	strRelation, err := proto.Marshal(storeRelation)
	if err != nil {
		this.infoLog.Printf("ModifyGroupName Failed to proto.Marshal hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			newName,
			updateTs,
			err)
		return err
	}

	err = this.ssdbApi.Hset(hashName, key, strRelation)
	if err != nil {
		this.infoLog.Printf("ModifyGroupName update following ts failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			newName,
			updateTs,
			err)
		return err
	}
	// Step2: 更新关注列表的时间戳版本
	err = this.ssdbApi.Hset(hashName, HashKeyFollowingTs, fmt.Sprintf("%v", updateTs))
	if err != nil {
		this.infoLog.Printf("ModifyGroupName update following ts failed hashName=%s uid=%v groupName=%s updateTs=%v err=%s",
			hashName,
			uid,
			newName,
			updateTs,
			err)
		return err
	}
	return nil
}

func (this *SsdbOperator) HSet(hashName string, key string, value uint64) (err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return err
	}
	err = this.ssdbApi.Hset(hashName, key, fmt.Sprintf("%v", value))
	return err
}
