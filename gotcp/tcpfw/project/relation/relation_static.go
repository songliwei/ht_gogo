package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_relation"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(12750)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)
	uidSlicCfg := cfg.Section("TESTUID").Key("uidSlice").MustString("1946612")
	uidSlic := strings.Split(uidSlicCfg, ",")

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	var head *common.HeadV3
	head = &common.HeadV3{
		Flag:     0xF0,
		Version:  4,
		CryKey:   0,
		TermType: 0,
		Cmd:      8,
		Seq:      1,
		From:     1946612,
		To:       0,
		Len:      0,
		Ret:      0,
	}
	for _, v := range uidSlic {
		uid, err := strconv.ParseUint(v, 10, 32)
		if err != nil {
			infoLog.Printf("strconv.ParseUint uid=%v failed", v)
			continue
		}
		if uid%5 == 0 {
			time.Sleep(1 * time.Second)
		}
		GetFollowerList(conn, head, uint32(uid))
		GetFollowingList(conn, head, uint32(uid))
	}
}

func GetFollowerList(conn *net.TCPConn, head *common.HeadV3, uid uint32) {
	if conn == nil || head == nil || uid == 0 {
		infoLog.Printf("GetFollowerList invalid param conn=%v head=%v uid=%v", conn, head, uid)
		return
	}
	v3Protocol := &common.HeadV3Protocol{}
	head.Cmd = 8
	head.From = uid
	subReqBody := &ht_relation.GetFollowerListReqBody{
		UidList: []uint32{uid},
	}
	var payLoad []byte
	reqBody := new(ht_relation.ReqBody)
	reqBody.GetFollowerListReqbody = subReqBody
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("GetFollowerList proto.Marshal failed uid=%v cmd=%v seq=%v",
			head.From,
			head.Cmd,
			head.Seq)
		return
	}
	head.Len = uint32(common.PacketV3HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("GetFollowerList SerialHeadV3ToSlice failed")
		return
	}
	copy(buf[common.PacketV3HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV3MagicEnd
	// write
	conn.Write(buf)
	// read
	p, err := v3Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket := p.(*common.HeadV3Packet)
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("GetFollowerList resp len=%v cmd=%v\n", rspHead.Len, rspHead.Cmd)
		// protobuf unmarshal
		rspBody := &ht_relation.RspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Printf("GetFollowerList uid=%v unmarshal failed", uid)
		}

		if rspHead.Cmd == 8 {
			subRspBody := rspBody.GetGetFollowerListRspbody()
			followerList := subRspBody.GetFollowerList()
			for _, v := range followerList {
				baseList := v.GetBaseList()
				infoLog.Printf("GetFollowerList follower uid=%v followerSize=%v", v.GetUid(), len(v.GetBaseList()))
				for j, z := range baseList {
					infoLog.Printf("GetFollowerList index=%v followeruid=%v", j, z.GetUid())
				}
			}
		} else {
			infoLog.Printf("GetFollowerList resp cmd=%v not equal 8", rspHead.Cmd)
		}
	}
}

func GetFollowingList(conn *net.TCPConn, head *common.HeadV3, uid uint32) {
	if conn == nil || head == nil || uid == 0 {
		infoLog.Printf("GetFollowingList invalid param conn=%v head=%v uid=%v", conn, head, uid)
		return
	}
	v3Protocol := &common.HeadV3Protocol{}
	head.Cmd = 6
	head.From = uid
	subReqBody := &ht_relation.GetFollowingListReqBody{
		UidList:       []uint32{uid},
		LastTimestamp: []uint32{0},
	}
	var payLoad []byte
	reqBody := new(ht_relation.ReqBody)
	reqBody.GetFollowingListReqbody = subReqBody
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("GetFollowingList proto.Marshal failed uid=%v cmd=%v seq=%v",
			head.From,
			head.Cmd,
			head.Seq)
		return
	}
	head.Len = uint32(common.PacketV3HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("GetFollowingList SerialHeadV3ToSlice failed")
		return
	}
	copy(buf[common.PacketV3HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV3MagicEnd
	// write
	conn.Write(buf)
	// read
	p, err := v3Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket := p.(*common.HeadV3Packet)
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("GetFollowingList resp len=%v cmd=%v\n", rspHead.Len, rspHead.Cmd)
		// protobuf unmarshal
		rspBody := &ht_relation.RspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Printf("GetFollowingList uid=%v unmarshal failed", uid)
		}

		if rspHead.Cmd == 6 {
			subRspBody := rspBody.GetGetFollowingListRspbody()
			followingList := subRspBody.GetFollowingList()
			for _, v := range followingList {
				baseList := v.GetBaseList()
				infoLog.Printf("GetFollowingList following uid=%v followingSize=%v", v.GetUid(), len(v.GetBaseList()))
				for j, z := range baseList {
					infoLog.Printf("GetFollowingList index=%v followinguid=%v", j, z.GetUid())
				}
			}
		} else {
			infoLog.Printf("GetFollowingList resp cmd=%v not equal 6", rspHead.Cmd)
		}
	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
