package main

import (
	"errors"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_accountcache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_basecache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_cacheinterface"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog         *log.Logger
	baseCacheApi    *common.SrvToSrvApiV2
	accountCacheApi *common.SrvToSrvApiV2
)

var (
	ErrInputParam         = errors.New("err param error")
	ErrSendPacketFailed   = errors.New("err send packet failed")
	ErrChangePacketFailed = errors.New("err change packet failed")
	ErrGetHeadFailed      = errors.New("err get packet head failed")
	ErrCacheReturnErr     = errors.New("err cache return err")
	ErrDuplicate          = errors.New("duplicate param")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close() // 关闭连接
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close() // 关闭连接
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		c.Close() // 关闭连接
		return false
	}

	// 统计总的请求量
	attr := "gocainter/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch head.Cmd {
	case uint32(ht_cache_interface.CMD_TYPE_CMD_GET_USER_CACHE):
		go ProcGetUserCache(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_RELOAD_USER_CACHE):
		go ProcRelodUserCache(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_NAME):
		go ProcUpdateUserName(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_SUMMER_TIME_SETTING):
		go ProcUpdateUserSummerTime(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_TIMEZONE):
		go ProcUpdateUserTimeZone(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_GET_USER_NICK_NAME):
		go ProcGetUserNickName(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_PRIVATE):
		go ProcUpdateUserPrivate(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_LEARN_MULTI_LANG):
		go ProcUpdateLearnMultiLang(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_LANG_SETTING):
		go ProcUpdateUserLangSetting(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_PROFILE):
		go ProcUpdateUserProfile(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_PROPERTY):
		go ProcUpdateUserProperty(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_PASSWD):
		go ProcUpdateUserPassWd(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_EMAIL):
		go ProcUpdateUserEmail(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UNREGISTER_USER_ACCOUNT):
		go ProcUnRegisterUserAccount(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_LESSON_VER):
		go ProcUpdateUserLessonVer(c, head, packet.GetBody())
	case uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_WALLET_VER):
		go ProcUpdateUserWalletVer(c, head, packet.GetBody())
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
	}
	return true
}

// 1.proc get user cache
func ProcGetUserCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserCacheRspbody = &ht_cache_interface.GetUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/get_user_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserCacheRspbody = &ht_cache_interface.GetUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserCache GetGetUserCacheReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserCacheRspbody = &ht_cache_interface.GetUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uidList := subReqBody.GetUidList()
	var itermList []*ht_cache_interface.UserInfoCacheIterm
	reqHead := new(common.HeadV2)
	*reqHead = *head
	for _, v := range uidList {
		// STEP1 Update account cache
		reqPayLoad, err := ChangeToGetUserAccountCacheReq(head, subReqBody)
		if err != nil {
			infoLog.Printf("ProcGetUserCache ChangeToGetUserAccountCacheReq() failed uid=%v err=%s", v, err)
			continue
		}

		reqHead.Uid = v // set the get uid
		accountRspPacket, err := SendAndRecvPacketFromAccountCache(reqHead, ht_account_cache.CMD_TYPE_CMD_GET_USER_ACCOUNT_CACHE, reqPayLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserCache SendAndRecvPacketFromBaseCache() failed uid=%v err=%s", v, err)
			attr := "gocainter/query_account_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		accountSubRspPacket := accountRspPacket.GetGetUserAccountCacheRspbody()
		if accountSubRspPacket == nil {
			infoLog.Printf("ProcGetUserCache accountRspPacket.GetGetUserCacheRspbody failed")
			attr := "gocainter/query_account_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		accountCache := accountSubRspPacket.GetAccountCache()
		// STEP2 Update base cache
		reqPayLoad, err = ChangeToGetUserBaseCacheReq(head, subReqBody)
		if err != nil {
			infoLog.Printf("ProcGetUserCache ChangeToGetUserBaseCacheReq() failed uid=%v err=%s", v, err)
			continue
		}
		baseRspPacket, err := SendAndRecvPacketFromBaseCache(reqHead, ht_base_cache.CMD_TYPE_CMD_GET_USER_BASE_CACHE, reqPayLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserCache SendAndRecvPacketFromBaseCache() failed uid=%v err=%s", v, err)
			attr := "gocainter/query_base_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		baseSubRspPacket := baseRspPacket.GetGetUserBaseCacheRspbody()
		if baseSubRspPacket == nil {
			infoLog.Printf("ProcGetUserCache baseRspPacket.GetGetUserCacheRspbody failed")
			attr := "gocainter/query_base_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		baseCache := baseSubRspPacket.GetBaseCache()
		iterm := &ht_cache_interface.UserInfoCacheIterm{
			BaseCache: &ht_cache_interface.UserBaseCache{
				Uid:              proto.Uint32(v),
				UserName:         proto.String(baseCache.GetUserName()),
				UserType:         proto.Uint32(baseCache.GetUserType()),
				NickName:         proto.String(baseCache.GetNickName()),
				Fullpy:           proto.String(baseCache.GetFullpy()),
				ShortFullpy:      proto.String(baseCache.GetShortFullpy()),
				Sex:              proto.Uint32(baseCache.GetSex()),
				Birthday:         proto.String(baseCache.GetBirthday()),
				Signature:        proto.String(baseCache.GetSignature()),
				HeadUrl:          proto.String(baseCache.GetHeadUrl()),
				VoiceUrl:         proto.String(baseCache.GetVoiceUrl()),
				VoiceDuration:    proto.Uint32(baseCache.GetVoiceDuration()),
				Nationality:      proto.String(baseCache.GetNationality()),
				TimeZone:         proto.Uint32(baseCache.GetTimeZone()),
				SummerTime:       proto.Uint32(baseCache.GetSummerTime()),
				TimeZone_48:      proto.Uint32(baseCache.GetTimeZone_48()),
				AllowCount:       proto.Uint32(baseCache.GetAllowCount()),
				NativeLang:       proto.Uint32(baseCache.GetNativeLang()),
				TeachLang2:       proto.Uint32(baseCache.GetTeachLang2()),
				TeachLang2Level:  proto.Uint32(baseCache.GetTeachLang2Level()),
				TeachLang3:       proto.Uint32(baseCache.GetTeachLang3()),
				TeachLang3Level:  proto.Uint32(baseCache.GetTeachLang3Level()),
				LearnLang1:       proto.Uint32(baseCache.GetLearnLang1()),
				LearnLang1Level:  proto.Uint32(baseCache.GetLearnLang1Level()),
				LearnLang2:       proto.Uint32(baseCache.GetLearnLang2()),
				LearnLang2Level:  proto.Uint32(baseCache.GetLearnLang2Level()),
				LearnLang3:       proto.Uint32(baseCache.GetLearnLang3()),
				LearnLang3Level:  proto.Uint32(baseCache.GetLearnLang3Level()),
				LearnLang4:       proto.Uint32(baseCache.GetLearnLang4()),
				LearnLang4Level:  proto.Uint32(baseCache.GetLearnLang4Level()),
				HideLocation:     proto.Uint32(baseCache.GetHideLocation()),
				HideCity:         proto.Uint32(baseCache.GetHideCity()),
				HideAge:          proto.Uint32(baseCache.GetHideAge()),
				HideOnline:       proto.Uint32(baseCache.GetHideOnline()),
				ProfileVer:       proto.Uint64(baseCache.GetProfileVer()),
				FriendVer:        proto.Uint64(baseCache.GetFriendVer()),
				BlackListVer:     proto.Uint64(baseCache.GetBlackListVer()),
				LocationVer:      proto.Uint64(baseCache.GetLocationVer()),
				InviteVer:        proto.Uint64(baseCache.GetInviteVer()),
				LastGetOffMsgVer: proto.Uint64(baseCache.GetLastGetOffMsgVer()),
				HeadModifyVer:    proto.Uint64(baseCache.GetHeadModifyVer()),
				LastLessonVer:    proto.Uint64(baseCache.GetLastLessonVer()),
				WalletVer:        proto.Uint64(baseCache.GetLastWalletVer()),
			},
			AccountCache: &ht_cache_interface.AccountCache{
				UserId:     proto.Uint32(v),
				UserType:   proto.Uint32(accountCache.GetUserType()),
				Cnonce:     accountCache.GetCnonce(),
				PassWd:     accountCache.GetPassWd(),
				Email:      accountCache.GetEmail(),
				Verify:     proto.Uint32(accountCache.GetVerify()),
				RegFrom:    proto.Uint32(accountCache.GetRegFrom()),
				RegTime:    proto.Uint64(accountCache.GetRegTime()),
				ModifyTime: proto.Uint64(accountCache.GetModifyTime()),
			},
		}
		itermList = append(itermList, iterm)
	}
	// return success
	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.GetUserCacheRspbody = &ht_cache_interface.GetUserCacheRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ItermList: itermList,
	}
	return true
}

func ChangeToGetUserBaseCacheReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.GetUserCacheReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToGetUserBaseCacheReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		GetUserBaseCacheReqbody: &ht_base_cache.GetUserBaseCacheReqBody{},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToGetUserBaseCacheReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

func ChangeToGetUserAccountCacheReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.GetUserCacheReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToGetUserAccountCacheReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_account_cache.AccountCacheReqBody{
		GetUserAccountCacheReqbody: &ht_account_cache.GetUserAccountCacheReqBody{},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToGetUserAccountCacheReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 2.proc reload user cache
func ProcRelodUserCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.ReloadUserCacheRspbody = &ht_cache_interface.ReloadUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcRelodUserCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/reload_user_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcRelodUserCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserCacheRspbody = &ht_cache_interface.ReloadUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetReloadUserCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcRelodUserCache GetReloadUserCacheReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserCacheRspbody = &ht_cache_interface.ReloadUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToReloadAccountCacheReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcRelodUserCache ChangeToReloadAccountCacheReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserCacheRspbody = &ht_cache_interface.ReloadUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	ret, err := SendPacketToAccountCache(head, ht_account_cache.CMD_TYPE_CMD_RELOAD_USER_ACCOUNT_CACHE, reqPayLoad)
	if err != nil || ret != uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ProcRelodUserCache SendPacketToAccountCache() failed err=%s ret=%v", err, ret)
		attr := "gocainter/query_account_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserCacheRspbody = &ht_cache_interface.ReloadUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query account cache failed"),
			},
		}
		return false
	}

	// STEP2 Update base cache
	reqPayLoad, err = ChangeToReloadBaseCacheReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcRelodUserCache ChangeToReloadBaseCacheReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserCacheRspbody = &ht_cache_interface.ReloadUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	ret, err = SendPacketToBaseCache(head, ht_base_cache.CMD_TYPE_CMD_RELOAD_USER_BASE_CACHE, reqPayLoad)
	if err != nil || ret != uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ProcRelodUserCache SendPacketToBaseCache() failed err=%s ret=%v", err, ret)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserCacheRspbody = &ht_cache_interface.ReloadUserCacheRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}
	// return success
	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.ReloadUserCacheRspbody = &ht_cache_interface.ReloadUserCacheRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ChangeToReloadBaseCacheReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.ReloadUserCacheReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToReloadBaseCacheReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		ReloadUserBaseCacheReqbody: &ht_base_cache.ReloadUserBaseCacheReqBody{},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToReloadBaseCacheReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

func ChangeToReloadAccountCacheReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.ReloadUserCacheReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToReloadAccountCacheReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_account_cache.AccountCacheReqBody{
		ReloadUserAccountCacheReqbody: &ht_account_cache.ReloadUserAccountCacheReqBody{},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToReloadAccountCacheReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 3.proc update user name
func ProcUpdateUserName(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserNameRspbody = &ht_cache_interface.UpdateUserNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserName invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_name_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserName proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserNameRspbody = &ht_cache_interface.UpdateUserNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserNameReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserName GetUpdateUserNameReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserNameRspbody = &ht_cache_interface.UpdateUserNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateUserNameReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserName ChangeToUpdateUserNameReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserNameRspbody = &ht_cache_interface.UpdateUserNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_NAME, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserName SendPacketToBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		if err == ErrDuplicate {
			result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_DUPLICATE_RECORD)
		} else {
			result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)

		}
		rspBody.UpdateUserNameRspbody = &ht_cache_interface.UpdateUserNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetUpdateUserNameRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcUpdateUserName baseRspPacket.GetUpdateUserNameRspbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserNameRspbody = &ht_cache_interface.UpdateUserNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserNameRspbody = &ht_cache_interface.UpdateUserNameRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ProfileTs: proto.Uint64(baseSubRspPacket.GetProfileTs()),
	}
	return true
}

func ChangeToUpdateUserNameReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserNameReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateUserNameReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserNameReqbody: &ht_base_cache.UpdateUserNameReqBody{
			UserName: proto.String(subReqBody.GetUserName()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateUserNameReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 4.proc update user summer time
func ProcUpdateUserSummerTime(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserSummerTimeRspbody = &ht_cache_interface.UpdateUserSummerTimeRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserSummerTime invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_summertime_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserSummerTime proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_cache_interface.UpdateUserSummerTimeRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserSummerTimeReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateUserSummerTime GetUpdateUserSummerTimeReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_cache_interface.UpdateUserSummerTimeRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateUserSummerTimeReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserSummerTime ChangeToUpdateUserSummerTimeReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_cache_interface.UpdateUserSummerTimeRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_SUMMER_TIME_SETTING, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserSummerTime SendAndRecvPacketFromBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_cache_interface.UpdateUserSummerTimeRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetUpdateUserSummerTimeRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcUpdateUserSummerTime baseRspPacket.GetUpdateUserSummerTimeReqbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_cache_interface.UpdateUserSummerTimeRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserSummerTimeRspbody = &ht_cache_interface.UpdateUserSummerTimeRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ProfileTs: proto.Uint64(baseSubRspPacket.GetProfileTs()),
	}
	return true
}

func ChangeToUpdateUserSummerTimeReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserSummerTimeReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateUserSummerTimeReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserSummerTimeReqbody: &ht_base_cache.UpdateUserSummerTimeReqBody{
			SummerTime: proto.Uint32(subReqBody.GetSummerTime()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateUserSummerTimeReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 5.proc update user time zone
func ProcUpdateUserTimeZone(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserTimeZoneRspbody = &ht_cache_interface.UpdateUserTimeZoneRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserTimeZone invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_timezone_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserTimeZone proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_cache_interface.UpdateUserTimeZoneRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserTimeZoneReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateUserTimeZone GetUpdateUserTimeZoneReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_cache_interface.UpdateUserTimeZoneRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateUserTimeZoneReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserTimeZone ChangeToUpdateUserTimeZoneReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_cache_interface.UpdateUserTimeZoneRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_TIMEZONE, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserTimeZone SendAndRecvPacketFromBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_cache_interface.UpdateUserTimeZoneRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetUpdateUserTimeZoneRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcUpdateUserTimeZone baseRspPacket.GetUpdateUserTimeZoneReqbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_cache_interface.UpdateUserTimeZoneRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserTimeZoneRspbody = &ht_cache_interface.UpdateUserTimeZoneRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ProfileTs: proto.Uint64(baseSubRspPacket.GetProfileTs()),
	}
	return true
}

func ChangeToUpdateUserTimeZoneReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserTimeZoneReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateUserTimeZoneReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserTimeZoneReqbody: &ht_base_cache.UpdateUserTimeZoneReqBody{
			TimeZone: proto.Int32(subReqBody.GetTimeZone()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateUserTimeZoneReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 6.proc get user nick name
func ProcGetUserNickName(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserNickNameRspbody = &ht_cache_interface.GetUserNickNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserNickName invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/get_user_nickname_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserNickName proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserNickNameRspbody = &ht_cache_interface.GetUserNickNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserNickNameReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserNickName GetGetUserNickNameReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserNickNameRspbody = &ht_cache_interface.GetUserNickNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToGetUserNickNameReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserNickName ChangeToGetUserNickNameReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserNickNameRspbody = &ht_cache_interface.GetUserNickNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_GET_USER_NICK_NAME, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcGetUserNickName SendAndRecvPacketFromBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.GetUserNickNameRspbody = &ht_cache_interface.GetUserNickNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetGetUserNickNameRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcGetUserNickName baseRspPacket.GetGetUserNickNameReqbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.GetUserNickNameRspbody = &ht_cache_interface.GetUserNickNameRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.GetUserNickNameRspbody = &ht_cache_interface.GetUserNickNameRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		UserName: proto.String(baseSubRspPacket.GetUserName()),
		NickName: proto.String(baseSubRspPacket.GetNickName()),
	}
	return true
}

func ChangeToGetUserNickNameReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.GetUserNickNameReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToGetUserNickNameReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		GetUserNickNameReqbody: &ht_base_cache.GetUserNickNameReqBody{},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToGetUserNickNameReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 7.proc update user private
func ProcUpdateUserPrivate(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserPrivateRspbody = &ht_cache_interface.UpdateUserPrivateRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserPrivate invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_private_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPrivate proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_cache_interface.UpdateUserPrivateRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserPrivateReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserPrivate GetUpdateUserPrivateRspbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_cache_interface.UpdateUserPrivateRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdatePrivateReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPrivate ChangeToUpdatePrivateReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_cache_interface.UpdateUserPrivateRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_PRIVATE, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPrivate SendAndRecvPacketFromBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_cache_interface.UpdateUserPrivateRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetUpdateUserPrivateRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcUpdateUserPrivate baseRspPacket.GetUpdateUserPrivateRspbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_cache_interface.UpdateUserPrivateRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserPrivateRspbody = &ht_cache_interface.UpdateUserPrivateRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ProfileTs: proto.Uint64(baseSubRspPacket.GetProfileTs()),
	}
	return true
}

func ChangeToUpdatePrivateReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserPrivateReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdatePrivateReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserPrivateReqbody: &ht_base_cache.UpdateUserPrivateReqBody{
			HideLocation: proto.Uint32(subReqBody.GetHideLocation()),
			HideCity:     proto.Uint32(subReqBody.GetHideCity()),
			HideAge:      proto.Uint32(subReqBody.GetHideAge()),
			HideOnline:   proto.Uint32(subReqBody.GetHideOnline()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdatePrivateReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 8.proc update user profile
func ProcUpdateUserProfile(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserProfileRspbody = &ht_cache_interface.UpdateUserProfileRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserProfile invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_profile_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProfile proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_cache_interface.UpdateUserProfileRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserProfileReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserProfile GetUpdateUserProfileReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_cache_interface.UpdateUserProfileRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateProfileReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProfile ChangeToUpdateProfileReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_cache_interface.UpdateUserProfileRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_PROFILE, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProfile SendAndRecvPacketFromBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_cache_interface.UpdateUserProfileRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetUpdateUserProfileRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcUpdateUserProfile baseRspPacket.GetUpdateUserProfileRspbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_cache_interface.UpdateUserProfileRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserProfileRspbody = &ht_cache_interface.UpdateUserProfileRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ProfileTs: proto.Uint64(baseSubRspPacket.GetProfileTs()),
	}
	return true
}

func ChangeToUpdateProfileReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserProfileReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateProfileReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}
	baseInfo := subReqBody.GetBaseInfo()
	langInfo := subReqBody.GetLangInfo()
	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserProfileReqbody: &ht_base_cache.UpdateUserProfileReqBody{
			BaseItems: subReqBody.GetBaseItems(),
			BaseInfo: &ht_base_cache.BaseUserInfo{
				UserId:        proto.Uint32(baseInfo.GetUserId()),
				UserName:      proto.String(baseInfo.GetUserName()),
				UserType:      proto.Uint32(baseInfo.GetUserType()),
				NickName:      proto.String(baseInfo.GetNickName()),
				Fullpy:        proto.String(baseInfo.GetFullpy()),
				ShortFullpy:   proto.String(baseInfo.GetShortFullpy()),
				Sex:           proto.Uint32(baseInfo.GetSex()),
				Birthday:      proto.String(baseInfo.GetBirthday()),
				Signature:     proto.String(baseInfo.GetSignature()),
				HeadUrl:       proto.String(baseInfo.GetHeadUrl()),
				VoiceUrl:      proto.String(baseInfo.GetVoiceUrl()),
				VoiceDuration: proto.Uint32(baseInfo.GetVoiceDuration()),
				Nationality:   proto.String(baseInfo.GetNationality()),
				TimeZone:      proto.Uint32(baseInfo.GetTimeZone()),
				SummerTime:    proto.Uint32(baseInfo.GetSummerTime()),
				TimeZone_48:   proto.Uint32(baseInfo.GetTimeZone_48()),
			},
			LangItems: subReqBody.GetLangItems(),
			LangInfo: &ht_base_cache.LanguageInfo{
				NativeLang:      proto.Uint32(langInfo.GetNativeLang()),
				TeachLang2:      proto.Uint32(langInfo.GetTeachLang2()),
				TeachLang2Level: proto.Uint32(langInfo.GetTeachLang2Level()),
				TeachLang3:      proto.Uint32(langInfo.GetTeachLang3()),
				TeachLang3Level: proto.Uint32(langInfo.GetTeachLang3Level()),
				LearnLang1:      proto.Uint32(langInfo.GetLearnLang1()),
				LearnLang1Level: proto.Uint32(langInfo.GetLearnLang1Level()),
				LearnLang2:      proto.Uint32(langInfo.GetLearnLang2()),
				LearnLang2Level: proto.Uint32(langInfo.GetLearnLang2Level()),
				LearnLang3:      proto.Uint32(langInfo.GetLearnLang3()),
				LearnLang3Level: proto.Uint32(langInfo.GetLearnLang3Level()),
				LearnLang4:      proto.Uint32(langInfo.GetLearnLang4()),
				LearnLang4Level: proto.Uint32(langInfo.GetLearnLang4Level()),
			},
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateProfileReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 9.proc update learn multi language
func ProcUpdateLearnMultiLang(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateLearnMultiLangRspbody = &ht_cache_interface.UpdateLearnMultiLangRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateLearnMultiLang invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_learn_multi_lang_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateLearnMultiLang proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_cache_interface.UpdateLearnMultiLangRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateLearnMultiLangReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateLearnMultiLang GetUpdateLearnMultiLangReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_cache_interface.UpdateLearnMultiLangRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateLearnMultiLangReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateLearnMultiLang ChangeToUpdateLearnMultiLangReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_cache_interface.UpdateLearnMultiLangRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	ret, err := SendPacketToBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_LEARN_MULTI_LANG, reqPayLoad)
	if err != nil || ret != uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ProcUpdateLearnMultiLang SendPacketToBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_cache_interface.UpdateLearnMultiLangRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}
	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateLearnMultiLangRspbody = &ht_cache_interface.UpdateLearnMultiLangRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ChangeToUpdateLearnMultiLangReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateLearnMultiLangReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateLearnMultiLangReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateLearnMultiLangReqbody: &ht_base_cache.UpdateLearnMultiLangReqBody{
			LangCount: proto.Uint32(subReqBody.GetLangCount()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateLearnMultiLangReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 10.proc update user language setting
func ProcUpdateUserLangSetting(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserLangSettingRspbody = &ht_cache_interface.UpdateUserLangSettingRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserLangSetting invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_lang_setting_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLangSetting proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_cache_interface.UpdateUserLangSettingRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserLangSettingReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserLangSetting GetUpdateUserLangSettingReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_cache_interface.UpdateUserLangSettingRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateLangSettingReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLangSetting ChangeToUpdateLangSettingReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_cache_interface.UpdateUserLangSettingRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_LANG_SETTING, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLangSetting SendAndRecvPacketFromBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_cache_interface.UpdateUserLangSettingRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetUpdateUserLangSettingRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcUpdateUserLangSetting baseRspPacket.GetUpdateUserLangSettingRspbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_cache_interface.UpdateUserLangSettingRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserLangSettingRspbody = &ht_cache_interface.UpdateUserLangSettingRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ProfileTs: proto.Uint64(baseSubRspPacket.GetProfileTs()),
	}
	return true
}

func ChangeToUpdateLangSettingReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserLangSettingReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateLangSettingReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}
	langInfo := subReqBody.GetLangInfo()

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserLangSettingReqbody: &ht_base_cache.UpdateUserLangSettingReqBody{
			LangInfo: &ht_base_cache.LanguageInfo{
				NativeLang:      proto.Uint32(langInfo.GetNativeLang()),
				TeachLang2:      proto.Uint32(langInfo.GetTeachLang2()),
				TeachLang2Level: proto.Uint32(langInfo.GetTeachLang2Level()),
				TeachLang3:      proto.Uint32(langInfo.GetTeachLang3()),
				TeachLang3Level: proto.Uint32(langInfo.GetTeachLang3Level()),
				LearnLang1:      proto.Uint32(langInfo.GetLearnLang1()),
				LearnLang1Level: proto.Uint32(langInfo.GetLearnLang1Level()),
				LearnLang2:      proto.Uint32(langInfo.GetLearnLang2()),
				LearnLang2Level: proto.Uint32(langInfo.GetLearnLang2Level()),
				LearnLang3:      proto.Uint32(langInfo.GetLearnLang3()),
				LearnLang3Level: proto.Uint32(langInfo.GetLearnLang3Level()),
				LearnLang4:      proto.Uint32(langInfo.GetLearnLang4()),
				LearnLang4Level: proto.Uint32(langInfo.GetLearnLang4Level()),
			},
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateLangSettingReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 11.proc update user property
func ProcUpdateUserProperty(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserPropertyRspbody = &ht_cache_interface.UpdateUserPropertyRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserProperty invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_property_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProperty proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_cache_interface.UpdateUserPropertyRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserPropertyReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserProperty GetUpdateUserPropertyReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_cache_interface.UpdateUserPropertyRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateUserPropertyReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProperty ChangeToUpdateUserPropertyReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_cache_interface.UpdateUserPropertyRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	ret, err := SendPacketToBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_PROPERTY, reqPayLoad)
	if err != nil || ret != uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ProcUpdateUserProperty SendPacketToBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_cache_interface.UpdateUserPropertyRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}
	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserPropertyRspbody = &ht_cache_interface.UpdateUserPropertyRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ChangeToUpdateUserPropertyReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserPropertyReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateUserPropertyReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserPropertyReqbody: &ht_base_cache.UpdateUserPropertyReqBody{
			ModType:          (ht_base_cache.MOD_PROPERTY_TYPE(subReqBody.GetModType())).Enum(),
			ProfileVer:       proto.Uint64(subReqBody.GetProfileVer()),
			FriendVer:        proto.Uint64(subReqBody.GetFriendVer()),
			BlackListVer:     proto.Uint64(subReqBody.GetBlackListVer()),
			LocationVer:      proto.Uint64(subReqBody.GetLocationVer()),
			InviteVer:        proto.Uint64(subReqBody.GetInviteVer()),
			LastGetOffMsgVer: proto.Uint64(subReqBody.GetLastGetOffMsgVer()),
			HeadModifyVer:    proto.Uint64(subReqBody.GetHeadModifyVer()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateUserPropertyReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 12.proc update user passwd
func ProcUpdateUserPassWd(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserPassWdRspbody = &ht_cache_interface.UpdateUserPassWdRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserPassWd invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_passwd_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPassWd proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_cache_interface.UpdateUserPassWdRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserPassWdReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserPassWd GetUpdateUserPassWdReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_cache_interface.UpdateUserPassWdRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToAccountCacheUpdatePassWdReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPassWd ChangeToAccountCacheUpdatePassWdReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_cache_interface.UpdateUserPassWdRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	ret, err := SendPacketToAccountCache(head, ht_account_cache.CMD_TYPE_CMD_UPDATE_USER_PASSWD, reqPayLoad)
	if err != nil || ret != uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ProcUpdateUserPassWd SendPacketToAccountCache() failed err=%s", err)
		attr := "gocainter/query_account_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_cache_interface.UpdateUserPassWdRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserPassWdRspbody = &ht_cache_interface.UpdateUserPassWdRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ChangeToAccountCacheUpdatePassWdReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserPassWdReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToAccountCacheUpdatePassWdReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_account_cache.AccountCacheReqBody{
		UpdateUserPassWdReqbody: &ht_account_cache.UpdateUserPassWdReqBody{
			PassWd: subReqBody.GetPassWd(),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToAccountCacheUpdatePassWdReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 13.proc update user email
func ProcUpdateUserEmail(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserEmailRspbody = &ht_cache_interface.UpdateUserEmailRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserEmail invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_email_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserEmail proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_cache_interface.UpdateUserEmailRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserEmailReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserEmail GetUpdateUserEmailReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_cache_interface.UpdateUserEmailRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToAccountCacheUpdateEmailReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserEmail ChangeToAccountCacheUpdateEmailReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_cache_interface.UpdateUserEmailRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	ret, err := SendPacketToAccountCache(head, ht_account_cache.CMD_TYPE_CMD_UPDATE_USER_EMAIL, reqPayLoad)
	if err != nil || ret != uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ProcUpdateUserEmail SendPacketToAccountCache() failed err=%s", err)
		attr := "gocainter/query_account_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_cache_interface.UpdateUserEmailRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query account cache failed"),
			},
		}
		return false
	}
	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserEmailRspbody = &ht_cache_interface.UpdateUserEmailRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ChangeToAccountCacheUpdateEmailReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserEmailReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToAccountCacheUpdateEmailReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_account_cache.AccountCacheReqBody{
		UpdateUserEmailReqbody: &ht_account_cache.UpdateUserEmailReqBody{
			Email: subReqBody.GetEmail(),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToAccountCacheUpdateEmailReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 14.proc unregister user account
func ProcUnRegisterUserAccount(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UnregisterAccountRspbody = &ht_cache_interface.UnregisterAccountRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUnRegisterUserAccount invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/unregister_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUnRegisterUserAccount proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_cache_interface.UnregisterAccountRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUnregisterAccountReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUnRegisterUserAccount GetUnregisterAccountReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_cache_interface.UnregisterAccountRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToAccountCacheUnregisterReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUnRegisterUserAccount ChangeToAccountCacheUnregisterReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_cache_interface.UnregisterAccountRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	ret, err := SendPacketToAccountCache(head, ht_account_cache.CMD_TYPE_CMD_UNREGISTER_USER_ACCOUNT, reqPayLoad)
	if err != nil || ret != uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ProcUnRegisterUserAccount SendPacketToAccountCache() failed err=%s ret=%v", err, ret)
		attr := "gocainter/query_account_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UnregisterAccountRspbody = &ht_cache_interface.UnregisterAccountRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query account cache failed"),
			},
		}
		return false
	}

	// STEP2 Update base cache
	reqPayLoad, err = ChangeToBaseCacheUpdateUserPropertyReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUnRegisterUserAccount ChangeToBaseCacheUpdateUserPropertyReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_cache_interface.UnregisterAccountRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	ret, err = SendPacketToBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_PROPERTY, reqPayLoad)
	if err != nil || ret != uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ProcUnRegisterUserAccount SendPacketToBaseCache() failed err=%s ret=%v", err, ret)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UnregisterAccountRspbody = &ht_cache_interface.UnregisterAccountRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}
	// return success
	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UnregisterAccountRspbody = &ht_cache_interface.UnregisterAccountRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ChangeToAccountCacheUnregisterReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UnregisterAccountReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToAccountCacheUnregisterReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_account_cache.AccountCacheReqBody{
		UnregisterAccountReqbody: &ht_account_cache.UnregisterAccountReqBody{
			ReplaceEmail: subReqBody.GetReplaceEmail(),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToAccountCacheUnregisterReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

func ChangeToBaseCacheUpdateUserPropertyReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UnregisterAccountReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToBaseCacheUpdateUserPropertyReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserPropertyReqbody: &ht_base_cache.UpdateUserPropertyReqBody{
			ModType:    (ht_base_cache.MOD_PROPERTY_TYPE_MOD_PROFILE_VER).Enum(),
			ProfileVer: proto.Uint64(uint64(time.Now().Unix())),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToBaseCacheUpdateUserPropertyReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

func SendPacketToAccountCache(reqHead *common.HeadV2, cmd ht_account_cache.CMD_TYPE, reqPayLoad []byte) (ret uint16, err error) {
	if reqHead == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("SendPacketToAccountCache invalid param reqHead=%#v reqPayLoad=%v", *reqHead, reqPayLoad)
		return ret, ErrInputParam
	}
	accountReqHead := new(common.HeadV2)
	*accountReqHead = *reqHead
	accountReqHead.Cmd = uint32(cmd)
	accountRsp, err := accountCacheApi.SendAndRecvPacket(accountReqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("SendPacketToAccountCache SendAndRecvPacket failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	accountPacket, ok := accountRsp.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("SendPacketToAccountCache accountPacket can not change to HeadV2packet")
		return ret, ErrChangePacketFailed
	}
	// 是HeadV2Packet报文
	// head 为一个new出来的对象指针
	rspHead, err := accountPacket.GetHead()
	if err != nil {
		infoLog.Printf("SendPacketToAccountCache Get accountPacket head failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	infoLog.Printf("SendPacketToAccountCache accountPacket uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
	ret = rspHead.Ret
	return ret, nil
}

func SendAndRecvPacketFromAccountCache(reqHead *common.HeadV2, cmd ht_account_cache.CMD_TYPE, reqPayLoad []byte) (rspBody *ht_account_cache.AccountCacheRspBody, err error) {
	if reqHead == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("SendAndRecvPacketFromAccountCache invalid param reqHead=%#v reqPayLoad=%v", *reqHead, reqPayLoad)
		return nil, ErrInputParam
	}
	accountReqHead := new(common.HeadV2)
	*accountReqHead = *reqHead
	accountReqHead.Cmd = uint32(cmd)
	accountRsp, err := accountCacheApi.SendAndRecvPacket(accountReqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromAccountCache SendAndRecvPacket failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}

	accountPacket, ok := accountRsp.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("SendAndRecvPacketFromAccountCache accountPacket can not change to HeadV2packet")
		return nil, ErrChangePacketFailed
	}
	// 是HeadV2Packet报文
	// head 为一个new出来的对象指针
	rspHead, err := accountPacket.GetHead()
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromAccountCache Get accountPacket head failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}
	//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	// infoLog.Printf("SendAndRecvPacketFromAccountCache accountPacket uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
	if rspHead.Ret != uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("SendAndRecvPacketFromAccountCache not success uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
		return nil, ErrCacheReturnErr
	}

	rspPayLoad := accountPacket.GetBody()
	rspBody = new(ht_account_cache.AccountCacheRspBody)
	err = proto.Unmarshal(rspPayLoad, rspBody)
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromAccountCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", rspHead.Uid, rspHead.Cmd, rspHead.Seq)
		return nil, err
	} else {
		return rspBody, nil
	}
}

func SendPacketToBaseCache(reqHead *common.HeadV2, cmd ht_base_cache.CMD_TYPE, reqPayLoad []byte) (ret uint16, err error) {
	if reqHead == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("SendPacketToBaseCache invalid param reqHead=%#v reqPayLoad=%v", *reqHead, reqPayLoad)
		return ret, ErrInputParam
	}
	baseReqHead := new(common.HeadV2)
	*baseReqHead = *reqHead
	baseReqHead.Cmd = uint32(cmd)
	baseRsp, err := baseCacheApi.SendAndRecvPacket(baseReqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("SendPacketToBaseCache SendAndRecvPacket failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	basePacket, ok := baseRsp.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("SendPacketToBaseCache basePacket can not change to HeadV2packet")
		return ret, ErrChangePacketFailed
	}
	// 是HeadV2Packet报文
	// head 为一个new出来的对象指针
	rspHead, err := basePacket.GetHead()
	if err != nil {
		infoLog.Printf("SendPacketToBaseCache Get basePacket head failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	infoLog.Printf("SendPacketToBaseCache basePacket uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
	ret = rspHead.Ret
	return ret, nil
}

func SendAndRecvPacketFromBaseCache(reqHead *common.HeadV2, cmd ht_base_cache.CMD_TYPE, reqPayLoad []byte) (rspBody *ht_base_cache.BaseCacheRspBody, err error) {
	if reqHead == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("SendAndRecvPacketFromBaseCache invalid param reqHead=%#v reqPayLoad=%v", *reqHead, reqPayLoad)
		return nil, ErrInputParam
	}
	baseReqHead := new(common.HeadV2)
	*baseReqHead = *reqHead
	baseReqHead.Cmd = uint32(cmd)
	baseRsp, err := baseCacheApi.SendAndRecvPacket(baseReqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromBaseCache SendAndRecvPacket failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}

	basePacket, ok := baseRsp.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("SendAndRecvPacketFromBaseCache basePacket can not change to HeadV2packet")
		return nil, ErrChangePacketFailed
	}
	// 是HeadV2Packet报文
	// head 为一个new出来的对象指针
	rspHead, err := basePacket.GetHead()
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromBaseCache Get basePacket head failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}
	//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	// infoLog.Printf("SendAndRecvPacketFromBaseCache basePacket uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
	if rspHead.Ret != uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("SendAndRecvPacketFromBaseCache not success uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
		if rspHead.Ret == uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_DUPLICATE_RECORD) {
			return nil, ErrDuplicate
		} else {
			return nil, ErrCacheReturnErr
		}
	}
	rspPayLoad := basePacket.GetBody()
	rspBody = new(ht_base_cache.BaseCacheRspBody)
	err = proto.Unmarshal(rspPayLoad, rspBody)
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromBaseCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", rspHead.Uid, rspHead.Cmd, rspHead.Seq)
		return nil, err
	} else {
		return rspBody, nil
	}
}

// 15.proc update user lesson ver
func ProcUpdateUserLessonVer(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserLessonVerRspbody = &ht_cache_interface.UpdateUserLessonVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserLessonVer invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_lesson_ver"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLessonVer proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserLessonVerRspbody = &ht_cache_interface.UpdateUserLessonVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserLessonVerReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserLessonVer GetUpdateUserNameReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserLessonVerRspbody = &ht_cache_interface.UpdateUserLessonVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateUserLessonVerReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLessonVer ChangeToUpdateUserLessonVerReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserLessonVerRspbody = &ht_cache_interface.UpdateUserLessonVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_CREATE_LESSON_VER, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLessonVer SendPacketToBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		if err == ErrDuplicate {
			result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_DUPLICATE_RECORD)
		} else {
			result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)

		}
		rspBody.UpdateUserLessonVerRspbody = &ht_cache_interface.UpdateUserLessonVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetUpdateUserCreateLessonRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcUpdateUserLessonVer baseRspPacket.GetUpdateUserCreateLessonRspbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserLessonVerRspbody = &ht_cache_interface.UpdateUserLessonVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserLessonVerRspbody = &ht_cache_interface.UpdateUserLessonVerRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ChangeToUpdateUserLessonVerReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserLessonVerReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateUserLessonVerReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserCreateLessonReqbody: &ht_base_cache.UpdateUserCreateLessonReqBody{
			CreateUid: proto.Uint32(subReqBody.GetCreateUid()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateUserLessonVerReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 16.proc update user wallet ver
func ProcUpdateUserWalletVer(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_cache_interface.CacheInterfaceRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserWalletVerRspbody = &ht_cache_interface.UpdateUserWalletVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserLessonVer invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocainter/update_user_wallet_ver"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLessonVer proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_cache_interface.UpdateUserWalletVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserWalletVerReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserLessonVer GetUpdateUserNameReqbody() failed")
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_cache_interface.UpdateUserWalletVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// STEP1 Update account cache
	reqPayLoad, err := ChangeToUpdateUserWalletVerReq(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLessonVer ChangeToUpdateUserWalletVerReq() failed err=%s", err)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_cache_interface.UpdateUserWalletVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change packet failed"),
			},
		}
		return false
	}
	baseRspPacket, err := SendAndRecvPacketFromBaseCache(head, ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_WALLET_VER, reqPayLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLessonVer SendPacketToBaseCache() failed err=%s", err)
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		if err == ErrDuplicate {
			result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_DUPLICATE_RECORD)
		} else {
			result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)

		}
		rspBody.UpdateUserWalletVerRspbody = &ht_cache_interface.UpdateUserWalletVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	baseSubRspPacket := baseRspPacket.GetUpdateUserWalletVerRspbody()
	if baseSubRspPacket == nil {
		infoLog.Printf("ProcUpdateUserLessonVer baseRspPacket.GetUpdateUserWalletVerRspbody failed")
		attr := "gocainter/query_base_cache_failed"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_cache_interface.UpdateUserWalletVerRspBody{
			Status: &ht_cache_interface.CacheInterfaceHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("query base cache failed"),
			},
		}
		return false
	}

	result = uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody.UpdateUserWalletVerRspbody = &ht_cache_interface.UpdateUserWalletVerRspBody{
		Status: &ht_cache_interface.CacheInterfaceHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ChangeToUpdateUserWalletVerReq(reqHead *common.HeadV2, subReqBody *ht_cache_interface.UpdateUserWalletVerReqBody) (reqPayLoad []byte, err error) {
	if reqHead == nil || subReqBody == nil {
		infoLog.Printf("ChangeToUpdateUserWalletVerReq invalid param reqHead=%#v subReqBody=%#v", reqHead, *subReqBody)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		UpdateUserWalletVerReqbody: &ht_base_cache.UpdateUserWalletVerReqBody{
			CreateUid: proto.Uint32(subReqBody.GetCreateUid()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToUpdateUserWalletVerReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_cache_interface.CacheInterfaceRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init master cache
	baseCacheIp := cfg.Section("BASECACHE").Key("ip").MustString("127.0.0.1")
	baseCachePort := cfg.Section("BASECACHE").Key("port").MustString("6379")
	infoLog.Printf("base cache ip=%v port=%v", baseCacheIp, baseCachePort)
	baseCacheApi = common.NewSrvToSrvApiV2(baseCacheIp, baseCachePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	accountCacheIp := cfg.Section("ACCOUNTCACHE").Key("ip").MustString("127.0.0.1")
	accountCachePort := cfg.Section("ACCOUNTCACHE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%v port=%v", accountCacheIp, accountCachePort)
	accountCacheApi = common.NewSrvToSrvApiV2(accountCacheIp, accountCachePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
