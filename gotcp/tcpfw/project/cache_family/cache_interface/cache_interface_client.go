package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_cacheinterface"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	Cmd int `short:"t" long:"cmd" description:"Command type" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	v2Protocol := &common.HeadV2Protocol{}
	var head *common.HeadV2
	head = &common.HeadV2{
		Version:  4,
		Cmd:      1,
		Seq:      1,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      8514895,
	}

	var payLoad []byte
	reqBody := new(ht_cache_interface.CacheInterfaceReqBody)
	switch options.Cmd {
	// 获取用户基本信息
	case 1:
		head.Cmd = 1
		subReqBody := &ht_cache_interface.GetUserCacheReqBody{
			UidList: []uint32{2325928, 5547632},
		}
		reqBody.GetUserCacheReqbody = subReqBody
	case 2:
		head.Cmd = 2
		subReqBody := &ht_cache_interface.ReloadUserCacheReqBody{}
		reqBody.ReloadUserCacheReqbody = subReqBody
	case 3:
		head.Cmd = 3
		subReqBody := &ht_cache_interface.UpdateUserNameReqBody{
			UserName: proto.String("songliwei2017"),
		}
		reqBody.UpdateUserNameReqbody = subReqBody
	case 4:
		head.Cmd = 4
		subReqBody := &ht_cache_interface.UpdateUserSummerTimeReqBody{
			SummerTime: proto.Uint32(5),
		}
		reqBody.UpdateUserSummerTimeReqbody = subReqBody
	case 5:
		head.Cmd = 5
		subReqBody := &ht_cache_interface.UpdateUserTimeZoneReqBody{
			TimeZone: proto.Int32(8),
		}
		reqBody.UpdateUserTimeZoneReqbody = subReqBody
	case 6:
		head.Cmd = 6
		subReqBody := &ht_cache_interface.GetUserNickNameReqBody{}
		reqBody.GetUserNickNameReqbody = subReqBody
	case 7:
		head.Cmd = 7
		subReqBody := &ht_cache_interface.UpdateUserPrivateReqBody{
			HideLocation: proto.Uint32(1),
			HideCity:     proto.Uint32(0),
			HideAge:      proto.Uint32(1),
			HideOnline:   proto.Uint32(0),
		}
		reqBody.UpdateUserPrivateReqbody = subReqBody
	case 8:
		head.Cmd = 8
		subReqBody := &ht_cache_interface.UpdateLearnMultiLangReqBody{
			LangCount: proto.Uint32(1),
		}
		reqBody.UpdateLearnMultiLangReqbody = subReqBody
	case 9:
		head.Cmd = 9
		subReqBody := &ht_cache_interface.UpdateUserLangSettingReqBody{
			LangInfo: &ht_cache_interface.LanguageInfo{
				NativeLang:      proto.Uint32(1),
				TeachLang2:      proto.Uint32(2),
				TeachLang2Level: proto.Uint32(2),
				TeachLang3:      proto.Uint32(0),
				TeachLang3Level: proto.Uint32(0),
				LearnLang1:      proto.Uint32(4),
				LearnLang1Level: proto.Uint32(1),
				LearnLang2:      proto.Uint32(5),
				LearnLang2Level: proto.Uint32(1),
				LearnLang3:      proto.Uint32(0),
				LearnLang3Level: proto.Uint32(0),
				LearnLang4:      proto.Uint32(0),
				LearnLang4Level: proto.Uint32(0),
			},
		}
		reqBody.UpdateUserLangSettingReqbody = subReqBody
	case 10:
		head.Cmd = 10
		subReqBody := &ht_cache_interface.UpdateUserProfileReqBody{
			BaseItems: []uint32{0, 3}, // 0-性别 3-用户昵称
			BaseInfo: &ht_cache_interface.BaseUserInfo{
				UserId:        proto.Uint32(1946612),
				UserName:      proto.String("hello-slw"),
				UserType:      proto.Uint32(0),
				NickName:      proto.String("nickname-slw"),
				Fullpy:        nil,
				ShortFullpy:   nil,
				Sex:           proto.Uint32(1),
				Birthday:      nil,
				Signature:     nil,
				HeadUrl:       nil,
				VoiceUrl:      nil,
				VoiceDuration: nil,
				Nationality:   nil,
				TimeZone:      nil,
				SummerTime:    nil,
				TimeZone_48:   nil,
			},
			LangItems: []uint32{10, 11},
			LangInfo: &ht_cache_interface.LanguageInfo{
				NativeLang:      proto.Uint32(0),
				TeachLang2:      proto.Uint32(0),
				TeachLang2Level: proto.Uint32(0),
				TeachLang3:      proto.Uint32(0),
				TeachLang3Level: proto.Uint32(0),
				LearnLang1:      proto.Uint32(8),
				LearnLang1Level: proto.Uint32(1),
				LearnLang2:      proto.Uint32(7),
				LearnLang2Level: proto.Uint32(2),
				LearnLang3:      proto.Uint32(0),
				LearnLang3Level: proto.Uint32(0),
				LearnLang4:      proto.Uint32(0),
				LearnLang4Level: proto.Uint32(0),
			},
		}
		reqBody.UpdateUserProfileReqbody = subReqBody
	case 11:
		head.Cmd = 11
		subReqBody := &ht_cache_interface.UpdateUserPropertyReqBody{
			ModType:          ht_cache_interface.MOD_PROPERTY_TYPE_MOD_PROFILE_VER.Enum(),
			ProfileVer:       proto.Uint64(140000000),
			FriendVer:        nil,
			BlackListVer:     nil,
			LocationVer:      nil,
			InviteVer:        nil,
			LastGetOffMsgVer: nil,
			HeadModifyVer:    nil,
		}
		reqBody.UpdateUserPropertyReqbody = subReqBody
	case 12:
		head.Cmd = 12
		subReqBody := &ht_cache_interface.UpdateUserPassWdReqBody{
			PassWd: []byte("123456789"),
		}
		reqBody.UpdateUserPassWdReqbody = subReqBody
	case 13:
		head.Cmd = 13
		subReqBody := &ht_cache_interface.UpdateUserEmailReqBody{
			Email: []byte("slw2011@126.com"),
		}
		reqBody.UpdateUserEmailReqbody = subReqBody
	case 14:
		head.Cmd = 14
		subReqBody := &ht_cache_interface.UnregisterAccountReqBody{
			ReplaceEmail: []byte("slw2011@126.com"),
		}
		reqBody.UnregisterAccountReqbody = subReqBody
	default:
		infoLog.Println("UnKnow input cmd =", options.Cmd)
	}

	payLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v cmd=%v seq=%v",
			head.Uid,
			head.Cmd,
			head.Seq)
		return
	}

	head.Len = uint32(common.PacketV2HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV2ToSlice failed")
		return
	}
	copy(buf[common.PacketV2HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV2MagicEnd

	infoLog.Printf("len=%v payLaod=%v\n", len(payLoad), payLoad)
	// write
	conn.Write(buf)
	// read
	p, err := v2Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket, ok := p.(*common.HeadV2Packet)
		if !ok { // 不是HeadV3Packet报文
			infoLog.Printf("packet can not change to HeadV2packet")
			return
		}
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("resp len=%v cmd=%v uid=%v\n", rspHead.Len, rspHead.Cmd, rspHead.Uid)
		rspBody := &ht_cache_interface.CacheInterfaceRspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Println("proto Unmarshal failed")
			return
		}
		switch rspHead.Cmd {
		case 1:
			subRspBody := rspBody.GetGetUserCacheRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetUserBaseCache rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
			itemList := subRspBody.GetItermList()
			for i, v := range itemList {
				infoLog.Printf("GetUserBaseCache index=%v lessonVer=%v",
					i,
					v.GetBaseCache().GetLastLessonVer())
			}
		case 2:
			subRspBody := rspBody.GetReloadUserCacheRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("ReloadUserBaseCache rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 3:
			subRspBody := rspBody.GetUpdateUserNameRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserName rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 4:
			subRspBody := rspBody.GetUpdateUserSummerTimeRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserSummerTime rsp code=%v msg=%s profileTs=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetProfileTs())
		case 5:
			subRspBody := rspBody.GetUpdateUserTimeZoneRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserTimeZone rsp code=%v msg=%s profileTs=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetProfileTs())
		case 6:
			subRspBody := rspBody.GetGetUserNickNameRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetUserNickName rsp code=%v msg=%s userName=%s nickName=%s",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetUserName(),
				subRspBody.GetNickName())
		case 7:
			subRspBody := rspBody.GetUpdateUserPrivateRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s profileTs=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetProfileTs())
		case 8:
			subRspBody := rspBody.GetUpdateLearnMultiLangRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 9:
			subRspBody := rspBody.GetUpdateUserLangSettingRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s profileTs=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetProfileTs())
		case 10:
			subRspBody := rspBody.GetUpdateUserProfileRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s profileTs=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetProfileTs())
		case 11:
			subRspBody := rspBody.GetUpdateUserPropertyRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 12:
			subRspBody := rspBody.GetUpdateUserPassWdRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 13:
			subRspBody := rspBody.GetUpdateUserEmailRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 14:
			subRspBody := rspBody.GetUnregisterAccountRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		default:
			infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
		}

	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
