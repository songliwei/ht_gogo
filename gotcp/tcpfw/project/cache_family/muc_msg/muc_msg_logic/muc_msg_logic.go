package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc_store"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	dbdAgentApi    *common.SrvToSrvApiV2
	redisMasterApi *common.RedisApi
	redisSlaveApi  *common.RedisApi
)

const (
	ProcSlowThreshold = 300000
	AdminUid          = 10000
	HashKeyMsgBase    = 100000000000 //每个人的群消息队列是1000亿条
	MsgCountThreshold = 100
	HashExpirePeriod  = 259200 // 3 * 86400 = 7  day   units:second
	LatestMsgCount    = 10
)

const (
	HashKeyMaxMsgId = "0"
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrInternalErr  = errors.New("internal err")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	// infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gommlogic/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_muc_store.CMD_TYPE_CMD_STORE_MUC_MESSAGE_REQ):
		go ProcStoreMucMsg(c, head, packet)
	case uint32(ht_muc_store.CMD_TYPE_CMD_STORE_MUC_INDEX_REQ):
		go ProcStoreMucMsgIndex(c, head, packet)
	case uint32(ht_muc_store.CMD_TYPE_CMD_BATCH_GET_MUC_MESSAGE_REQ):
		go ProcBatchGetMucMsg(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
		c.Close()
	}
	return true
}

// 1.proc store moment info
func ProcStoreMucMsg(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_muc_store.MucStoreRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStoreMucMsg not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INVALID_PARAM)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStoreMucMsg invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gommlogic/store_muc_msg_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_muc_store.MucStoreReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsg proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStoreMucMessageReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcStoreMucMsg GetStoreMucMessageReqbody() faileduid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsg update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gommlogic/store_muc_msg_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcStoreMucMsg dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gommlogic/store_muc_msg_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcStoreMucMsg dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gommlogic/store_muc_msg_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcStoreMucMsg dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gommlogic/store_muc_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.proc get all chat uid
func ProcStoreMucMsgIndex(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_muc_store.MucStoreRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStoreMucMsgIndex not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INVALID_PARAM)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStoreMucMsgIndex invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gommlogic/store_muc_msg_index_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gommlogic/store_muc_msg_index_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_muc_store.MucStoreReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsgIndex proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStoreMessageIndexReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcStoreMucMsgIndex GetStoreMessageIndexReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsgIndex update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gommlogic/store_muc_msg_index_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcStoreMucMsgIndex dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gommlogic/store_muc_msg_index_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcStoreMucMsgIndex dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gommlogic/store_muc_msg_index_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcStoreMucMsgIndex dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gommlogic/store_muc_msg_index_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 3.proc store moment info
func ProcBatchGetMucMsg(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_muc_store.MucStoreRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBatchGetMucMsg not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INVALID_PARAM)
		rspBody.BatchGetMucMessageRspbody = &ht_muc_store.BatchGetMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetMucMsg invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gommlogic/batch_get_muc_msg_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gommlogic/batch_get_muc_msg_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_muc_store.MucStoreReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetMucMsg proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetMucMessageRspbody = &ht_muc_store.BatchGetMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetMucMessageReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetMucMsg GetBatchGetMucMessageReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.BatchGetMucMessageRspbody = &ht_muc_store.BatchGetMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	opUid := subReqBody.GetOpUid()
	cliSeq := subReqBody.GetCliSeqId()
	infoLog.Printf("ProcBatchGetMucMsg opUid=%v cliSeq=%v", opUid, cliSeq)
	msgIndex, maxSeqId, hasMore, err := GetMucMsgIndexByCliSeq(head, opUid, cliSeq)
	if err != nil {
		infoLog.Printf("ProcBatchGetMucMsg GetMucMsgIndexByCliSeq() failed uid=%v err=%s", opUid, err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetMucMessageRspbody = &ht_muc_store.BatchGetMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get msg index internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcBatchGetMucMsg opUid=%v cliSeq=%v maxSeqId=%v hasMore=%v", opUid, cliSeq, maxSeqId, hasMore)
	if len(msgIndex) == 0 {
		infoLog.Printf("ProcBatchGetMucMsg GetMucMsgIndexByCliSeq() msgIndex empty uid=%v err=%s", opUid, err)
		rspBody.BatchGetMucMessageRspbody = &ht_muc_store.BatchGetMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			MsgList:  nil,
			MaxSeqId: proto.Uint64(maxSeqId),
			HasMore:  proto.Uint32(0),
		}
		return true
	}

	values, err := redisMasterApi.Mget(msgIndex)
	if err != nil {
		infoLog.Printf("ProcBatchGetMucMsg redisMasterApi.Mget failed uid=%v err=%s", opUid, err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.BatchGetMucMessageRspbody = &ht_muc_store.BatchGetMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get msg error"),
			},
		}
		return false
	}

	hasNilValue := HasNilValue(values)
	if hasNilValue { // 某些key 过期从redis中删除了，则从ssdb获取key对应的value
		values, err = MGetKeysFromDbd(head, msgIndex)
		if err != nil {
			infoLog.Printf("ProcBatchGetMucMsg MGetKeysFromDbd failed uid=%v err=%s", opUid, err)
			result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
			rspBody.BatchGetMucMessageRspbody = &ht_muc_store.BatchGetMucMessageRspBody{
				Status: &ht_muc_store.MucStoreHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("get msg error"),
				},
			}
			return false
		}
	}

	// 将消息反序列化拼rspbody
	var msgList []*ht_muc_store.MucMsg
	for _, v := range values {
		if len(v) <= 0 {
			infoLog.Printf("ProcBatchGetMucMsg msg empty=%s continue", v)
			continue
		}
		item := new(ht_muc_store.MucMsg)
		err := proto.Unmarshal([]byte(v), item)
		if err != nil {
			infoLog.Printf("ProcBatchGetMucMsg unmarshal msg failed opUid=%v err=%s", opUid, err)
			continue
		}
		msgList = append(msgList, item)
	}
	result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	var protoHasMore uint32 = 0
	if hasMore {
		protoHasMore = 1
	}
	rspBody.BatchGetMucMessageRspbody = &ht_muc_store.BatchGetMucMessageRspBody{
		Status: &ht_muc_store.MucStoreHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MsgList:  msgList,
		MaxSeqId: proto.Uint64(maxSeqId),
		HasMore:  proto.Uint32(protoHasMore),
	}
	infoLog.Printf("ProcBatchGetMucMsg uid=%v cliSeq=%v get msg count=%v", opUid, cliSeq, len(msgList))
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 从cliSeq 开始取消息，将cliSeq之前的seq全部删除
	err = ClearHashMapByCliSeq(opUid, cliSeq)
	if err != nil {
		infoLog.Printf("ProcBatchGetMucMsg uid=%v cliSeq=%v ClearHashMapByCliSeq err=%s",
			opUid,
			cliSeq,
			err)
	}
	// 删除db中 从cliSeq 开始取消息，将cliSeq之前的seq全部删除
	err = ClearMucMsgIndexByCliSeqInDb(head, opUid, cliSeq)
	if err != nil {
		infoLog.Printf("ProcBatchGetMucMsg uid=%v cliSeq=%v ClearMucMsgIndexByCliSeqInDb failed err=%s",
			opUid,
			cliSeq,
			err)
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gommlogic/batch_get_muc_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}

	return true
}

func ClearHashMapByCliSeq(opUid uint32, cliSeq uint64) (err error) {
	if opUid == 0 || cliSeq == 0 {
		infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v input param error", opUid, cliSeq)
		err = ErrInvalidParam
		return err
	}
	hashName := GetUserHashMapName(opUid)
	ret, err := redisMasterApi.Exists(hashName)
	if err != nil {
		infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v hashMap=%s err=%s",
			opUid,
			cliSeq,
			hashName,
			err)
		return err
	}
	if !ret {
		infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v hashMap=%s not exist",
			opUid,
			cliSeq,
			hashName)
		return nil
	}
	maxMsgId, err := redisMasterApi.Hget(hashName, HashKeyMaxMsgId)
	if err != nil {
		infoLog.Printf("ClearHashMapByCliSeq hashName=%s Hget key=%s return err=%s", hashName, HashKeyMaxMsgId, err)
		return err
	}

	curMaxId, err := strconv.ParseUint(maxMsgId, 10, 64)
	if err != nil {
		infoLog.Printf("ClearHashMapByCliSeq strconv.ParseUint() msgMaxId=%s err=%s", maxMsgId, err)
		return err
	}
	transMaxId := curMaxId + HashKeyMsgBase
	if cliSeq > transMaxId {
		infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v bigger than maxSeq=%v return",
			opUid,
			cliSeq,
			transMaxId)
		return nil
	}

	hashLen, err := redisMasterApi.Hlen(hashName)
	if err != nil {
		infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v err=%s", opUid, cliSeq, err)
		return err
	}
	// infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v maxId=%v hashLen=%v",
	// 	opUid,
	// 	cliSeq,
	// 	transMaxId,
	// 	hashLen)
	hashLen -= 1 // 减去HashKeyMaxMsgId 占用的长度
	if transMaxId < uint64(hashLen) {
		infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v maxId=%v hashLen=%v",
			opUid,
			cliSeq,
			transMaxId,
			hashLen)
		return nil
	}
	minId := transMaxId - uint64(hashLen) + 1 // 包含transMaxId 需要加1
	infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v minId=%v maxId=%v hashLen=%v",
		opUid,
		cliSeq,
		minId,
		transMaxId,
		hashLen)
	// 检查 minId 和 cliSeq 间的关系
	if cliSeq <= minId {
		infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v less than minId=%v maxId=%v",
			opUid,
			cliSeq,
			minId,
			transMaxId)
		return nil
	}
	for index := minId; index <= cliSeq; index += 1 {
		itemKey := fmt.Sprintf("%v", index)
		_, err := redisMasterApi.Hdel(hashName, itemKey)
		if err != nil {
			infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v minId=%v maxId=%v del itemKey=%s err=%s",
				opUid,
				cliSeq,
				minId,
				transMaxId,
				itemKey,
				err)
		} else {
			// infoLog.Printf("ClearHashMapByCliSeq opUid=%v cliSeq=%v minId=%v maxId=%v del itemKey=%s ret=%v",
			// 	opUid,
			// 	cliSeq,
			// 	minId,
			// 	transMaxId,
			// 	itemKey,
			// 	ret)
		}
		_, err = redisSlaveApi.Hdel(hashName, itemKey)
		if err != nil {
			attr := "gommlogic/slave_hdel_failed"
			libcomm.AttrAdd(attr, 1)
			// infoLog.Printf("ClearHashMapByCliSeq slave opUid=%v cliSeq=%v minId=%v maxId=%v del itemKey=%s err=%s",
			// 	opUid,
			// 	cliSeq,
			// 	minId,
			// 	transMaxId,
			// 	itemKey,
			// 	err)
		}
	}
	return nil
}

func HasNilValue(values []string) (ret bool) {
	if len(values) == 0 {
		ret = false
		return ret
	}
	ret = false
	for _, v := range values {
		if len(v) == 0 {
			ret = true
			break
		}
	}
	return ret
}

// 检查slic 是否是全部为空
func IsWholeNilValue(values []string) (ret bool) {
	if len(values) == 0 {
		ret = true
		return ret
	}
	ret = true
	for _, v := range values {
		if len(v) != 0 {
			ret = false
			break
		}
	}
	return ret
}

func GetUserHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#gmsg", uid)
	return hashName
}

func GetMucMsgIndexByCliSeq(head *common.HeadV2, opUid uint32, cliSeq uint64) (msgIndex []string, maxSeqId uint64, hasMore bool, err error) {
	if head == nil || opUid == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetMucMsgIndexByCliSeq opUid=%v cliSeq=%v invalid param", opUid, cliSeq)
		return nil, maxSeqId, hasMore, err
	}
	hashName := GetUserHashMapName(opUid)
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// Step1.get current max msg id
		maxMsgId, err := redisMasterApi.Hget(hashName, HashKeyMaxMsgId)
		if err != nil {
			infoLog.Printf("GetMucMsgIndexByCliSeq hashName=%s Hget key=%s return err=%s", hashName, HashKeyMaxMsgId, err)
			return nil, maxSeqId, hasMore, err
		}
		// 设置过期时间为30天
		err = redisMasterApi.Expire(hashName, HashExpirePeriod)
		if err != nil {
			// add static
			attr := "gommlogic/set_key_expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("GetMucMsgIndexByCliSeq redisMasterApi.Expire key=%s err=%s failed", hashName, err)
		}

		// Stpe2.ullID greate than current max msg id return empty
		curMaxId, err := strconv.ParseUint(maxMsgId, 10, 64)
		if err != nil {
			infoLog.Printf("GetMucMsgIndexByCliSeq strconv.ParseUint() msgMaxId=%s err=%s", maxMsgId, err)
			return nil, maxSeqId, hasMore, err
		}
		transMaxId := curMaxId + HashKeyMsgBase
		if cliSeq > transMaxId {
			// add static
			attr := "gommlogic/cli_seq_over_flow"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("GetMucMsgIndexByCliSeq over flow opUid=%v cliSeq=%v curMaxId=%v",
				opUid,
				cliSeq,
				transMaxId)
			// err = ErrInvalidParam
			// 重置maxSeq = transMaxId
			maxSeqId = transMaxId
			hasMore = false
			return nil, maxSeqId, hasMore, nil
		} else if cliSeq == transMaxId {
			infoLog.Printf("GetMucMsgIndexByCliSeq no msg opUid=%v cliSeq=%v curMaxId=%v",
				opUid,
				cliSeq,
				transMaxId)
			// set maxSeqId and hasMore
			maxSeqId = cliSeq
			hasMore = false
			return nil, maxSeqId, hasMore, nil
		}
		if cliSeq < HashKeyMsgBase {
			infoLog.Printf("GetMucMsgIndexByCliSeq opUid=%v cliSeq=%v err get latest ten msg", opUid, cliSeq)
			if curMaxId < LatestMsgCount {
				cliSeq = HashKeyMsgBase
			} else {
				cliSeq = transMaxId - 10 // 单用户使用0来取群消息时，返回最新的10条消息
			}
			maxSeqId = transMaxId
			hasMore = false
			infoLog.Printf("GetMucMsgIndexByCliSeq opUid=%v cliSeq=%v maxSeqId=%v hasMore=%v",
				opUid,
				cliSeq,
				maxSeqId,
				hasMore)
		}
		// 3.prepare hmget keys
		endSeq := transMaxId
		for {
			if cliSeq+MsgCountThreshold >= transMaxId {
				endSeq = transMaxId
				maxSeqId = transMaxId
				hasMore = false
			} else {
				endSeq = cliSeq + MsgCountThreshold
				maxSeqId = endSeq
				hasMore = true
			}
			var keys []string
			for i := cliSeq + 1; i <= endSeq; i = i + 1 {
				item := fmt.Sprintf("%v", i)
				keys = append(keys, item)
			}
			infoLog.Printf("GetMucMsgIndexByCliSeq opUid=%v cliSeq=%v keySize=%v transMaxId=%v end=%v maxSeqId=%v",
				opUid,
				cliSeq,
				len(keys),
				transMaxId,
				endSeq,
				maxSeqId)
			msgIndex, err := redisMasterApi.Hmget(hashName, keys)
			if err != nil {
				infoLog.Printf("GetMucMsgIndexByCliSeq redisMasterApi.Hmget opUid=%v cliSeq=%v keysLen=%v err=%s",
					opUid,
					cliSeq,
					len(keys),
					err)
				return nil, maxSeqId, hasMore, err
			}
			hasNilValue := HasNilValue(msgIndex)
			if !hasNilValue {
				return msgIndex, maxSeqId, hasMore, err
			} else if IsWholeNilValue(msgIndex) {
				infoLog.Printf("GetMucMsgIndexByCliSeq IsWholeNilValue opUid=%v cliSeq=%v maxSeqId=%v hasMore=%v",
					opUid,
					cliSeq,
					maxSeqId,
					hasMore)
				attr := "gommlogic/get_msg_index_seq_jump"
				libcomm.AttrAdd(attr, 1)
				//如果全部为空将endSeq 赋值给CliSeq
				cliSeq = endSeq
				if cliSeq >= transMaxId {
					// add static
					attr := "gommlogic/cli_seq_over_flow"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("GetMucMsgIndexByCliSeq over flow opUid=%v cliSeq=%v curMaxId=%v",
						opUid,
						cliSeq,
						transMaxId)
					// err = ErrInvalidParam
					// 重置maxSeq = transMaxId
					maxSeqId = transMaxId
					hasMore = false
					return nil, maxSeqId, hasMore, nil
				}
				// 否自继续去
				continue
			} else {
				infoLog.Printf("GetMucMsgIndexByCliSeq redisMasterApi.Exists hashName=%s has nil value delete hashName",
					hashName)
				err = redisMasterApi.Del(hashName)
				if err != nil {
					infoLog.Printf("GetMucMsgIndexByCliSeq hashName=%s delete failed err=%s", hashName, err)
				}
				attr := "gommlogic/get_msg_index_cache_miss"
				libcomm.AttrAdd(attr, 1)
				msgIndex, maxSeqId, hasMore, err = GetMucMsgIndexByCliSeqFromDbd(head, opUid, cliSeq)
				if err != nil {
					infoLog.Printf("GetMucMsgIndexByCliSeq opUid=%v cliSeq=%v hashName=%s err=%s", opUid, cliSeq, hashName, err)
					return nil, maxSeqId, hasMore, err
				}
				return msgIndex, maxSeqId, hasMore, nil
			}
		}
	} else {
		infoLog.Printf("GetMucMsgIndexByCliSeq redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "gommlogic/get_msg_index_cache_miss"
		libcomm.AttrAdd(attr, 1)
		msgIndex, maxSeqId, hasMore, err = GetMucMsgIndexByCliSeqFromDbd(head, opUid, cliSeq)
		if err != nil {
			infoLog.Printf("GetMucMsgIndexByCliSeq opUid=%v cliSeq=%v hashName=%s err=%s", opUid, cliSeq, hashName, err)
			return nil, maxSeqId, hasMore, err
		}
		return msgIndex, maxSeqId, hasMore, nil
	}
}

func GetMucMsgIndexByCliSeqFromDbd(head *common.HeadV2, opUid uint32, cliSeq uint64) (msgIndex []string, maxSeqId uint64, hasMore bool, err error) {
	if head == nil || opUid == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetMucMsgIndexByCliSeqFromDbd opUid=%v cliSeq=%v invalid param", opUid, cliSeq)
		return nil, maxSeqId, hasMore, err
	}
	// Step2 读取Master没有命中则继续读取dbd
	reqhead := new(common.HeadV2)
	if head != nil {
		*reqhead = *head
	}
	reqhead.Cmd = uint32(ht_muc_store.CMD_TYPE_CMD_BATCH_GET_MUC_MSG_INDEX_REQ)
	reqBody := &ht_muc_store.MucStoreReqBody{
		GetMucMessageIndexReqbody: &ht_muc_store.GetMucMessageIndexReqBody{
			OpUid:    proto.Uint32(opUid),
			CliSeqId: proto.Uint64(cliSeq),
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("GetMucMsgIndexByCliSeqFromDbd proto.Marshal failed uid=%v err=%s", opUid, err)
		attr := "gommlogic/get_msg_index_proto_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqhead, reqPayLoad)
	if err != nil {
		infoLog.Printf("GetMucMsgIndexByCliSeqFromDbd cache miss reload from db failed uid=%v err=%s", opUid, err)
		attr := "gommlogic/get_msg_index_dbd_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("GetMucMsgIndexByCliSeqFromDbd dbpacket can not change to HeadV2packet uid=%v", opUid)
		attr := "gommlogic/get_msg_index_inter_err"
		libcomm.AttrAdd(attr, 1)
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("GetMucMsgIndexByCliSeqFromDbd dbpacket Get head failed uid=%v err=%s", opUid, err)
		attr := "gommlogic/get_msg_index_inter_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}
	if dbRespHead.Ret != uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("GetMucMsgIndexByCliSeqFromDbd dbpaket ret=%v uid=%v", dbRespHead.Ret, opUid)
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}

	rspBody := &ht_muc_store.MucStoreRspBody{}
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), rspBody)
	if err != nil {
		return nil, maxSeqId, hasMore, err
	}
	subRspBody := rspBody.GetGetMucMessageIndexRspbody()
	if subRspBody == nil {
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}
	keyValues := subRspBody.GetIdToIndex()
	maxSeqId = subRspBody.GetMaxSeqId()
	protoHasMore := subRspBody.GetHasMore()
	if protoHasMore > 0 {
		hasMore = true
	} else {
		hasMore = false
	}

	if len(keyValues) < 1 {
		infoLog.Printf("GetMucMsgIndexByCliSeqFromDbd uid=%v cliSeq=%v result empty", opUid, cliSeq)
		maxSeqId = cliSeq
		hasMore = false
		return nil, maxSeqId, hasMore, nil
	}
	for index := 1; index < len(keyValues); index = index + 2 {
		msgIndex = append(msgIndex, keyValues[index])
	}
	return msgIndex, maxSeqId, hasMore, nil
}

func MGetKeysFromDbd(head *common.HeadV2, keys []string) (values []string, err error) {
	if head == nil || len(keys) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("MGetKeysFromDbd uid=%v keysLen=%v invalid param", head.Uid, len(keys))
		return nil, err
	}
	// Step2 读取Master没有命中则继续读取dbd
	reqhead := new(common.HeadV2)
	if head != nil {
		*reqhead = *head
	}
	reqhead.Cmd = uint32(ht_muc_store.CMD_TYPE_CMD_MGET_KYES_REQ)
	reqBody := &ht_muc_store.MucStoreReqBody{
		MgetKeysReqbody: &ht_muc_store.MGetKeysReqBody{
			KeyList: keys,
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("MGetKeysFromDbd proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gommlogic/mget_keys_proto_err"
		libcomm.AttrAdd(attr, 1)
		return nil, err
	}
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqhead, reqPayLoad)
	if err != nil {
		infoLog.Printf("MGetKeysFromDbd cache miss reload from db failed uid=%v err=%s", head.Uid, err)
		attr := "gommlogic/mget_keys_dbd_err"
		libcomm.AttrAdd(attr, 1)
		return nil, err
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("MGetKeysFromDbd dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		attr := "gommlogic/mget_keys_inter_err"
		libcomm.AttrAdd(attr, 1)
		err = ErrInternalErr
		return nil, err
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("MGetKeysFromDbd dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		attr := "gommlogic/mget_keys_inter_err"
		libcomm.AttrAdd(attr, 1)
		return nil, err
	}
	if dbRespHead.Ret != uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("MGetKeysFromDbd dbpaket ret=%v uid=%v", dbRespHead.Ret, head.Uid)
		err = ErrInternalErr
		return nil, err
	}

	rspBody := &ht_muc_store.MucStoreRspBody{}
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetMgetKeysRspbody()
	if subRspBody == nil {
		err = ErrInternalErr
		return nil, err
	}
	values = subRspBody.GetValueList()
	return values, nil
}

func ClearMucMsgIndexByCliSeqInDb(head *common.HeadV2, opUid uint32, cliSeq uint64) (err error) {
	if head == nil || opUid == 0 {
		err = ErrInvalidParam
		infoLog.Printf("ClearMucMsgIndexByCliSeqInDb opUid=%v cliSeq=%v invalid param", opUid, cliSeq)
		return err
	}
	// Step2 读取Master没有命中则继续读取dbd
	reqhead := new(common.HeadV2)
	if head != nil {
		*reqhead = *head
	}
	reqhead.Cmd = uint32(ht_muc_store.CMD_TYPE_CMC_CLEAR_MUC_MSG_INDEX_REQ)
	reqBody := &ht_muc_store.MucStoreReqBody{
		ClearMucMessageIndexReqbody: &ht_muc_store.ClearMucMessageIndexReqBody{
			OpUid:    proto.Uint32(opUid),
			CliSeqId: proto.Uint64(cliSeq),
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ClearMucMsgIndexByCliSeqInDb proto.Marshal failed uid=%v err=%s", opUid, err)
		attr := "gommlogic/clear_msg_index_proto_err"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqhead, reqPayLoad)
	if err != nil {
		infoLog.Printf("ClearMucMsgIndexByCliSeqInDb dbdAgentApi.SendAndRecvPacket failed uid=%v err=%s", opUid, err)
		attr := "gommlogic/clear_msg_index_dbd_err"
		libcomm.AttrAdd(attr, 1)
		return err
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ClearMucMsgIndexByCliSeqInDb dbpacket can not change to HeadV2packet uid=%v", opUid)
		attr := "gommlogic/clear_msg_index_inter_err"
		libcomm.AttrAdd(attr, 1)
		err = ErrInternalErr
		return err
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ClearMucMsgIndexByCliSeqInDb dbpacket Get head failed uid=%v err=%s", opUid, err)
		attr := "gommlogic/clear_msg_index_inter_err"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	if dbRespHead.Ret != uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("ClearMucMsgIndexByCliSeqInDb dbpaket ret=%v uid=%v", dbRespHead.Ret, opUid)
		err = ErrInternalErr
		return err
	}

	rspBody := &ht_muc_store.MucStoreRspBody{}
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), rspBody)
	if err != nil {
		return err
	}
	subRspBody := rspBody.GetClearMucMessageIndexRspbody()
	if subRspBody == nil {
		err = ErrInternalErr
		return err
	}
	return nil
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_muc_store.MucStoreRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	redisSlaveIp := cfg.Section("REDISSLAVE").Key("redis_ip").MustString("127.0.0.1")
	redisSlavePort := cfg.Section("REDISSLAVE").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis slave ip=%v port=%v", redisSlaveIp, redisSlavePort)
	redisSlaveApi = common.NewRedisApi(redisSlaveIp + ":" + strconv.Itoa(redisSlavePort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustString("6379")
	infoLog.Printf("cache dbd ip=%v port=%v", dbdIp, dbdPort)
	dbdAgentApi = common.NewSrvToSrvApiV2(dbdIp, dbdPort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
