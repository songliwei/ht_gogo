// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"fmt"
	"log"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
)

// Error type
var (
	ErrNotExistInReids = errors.New("not exist in redis")
	ErrNilDbObject     = errors.New("not set object current is nil")
)

type SsdbOperator struct {
	//	ssdbApi *common.SsdbApi
	redisApi *common.RedisApi
	infoLog  *log.Logger
}

func NewSsdbOperator(targetRedisApi *common.RedisApi, logger *log.Logger) *SsdbOperator {
	return &SsdbOperator{
		redisApi: targetRedisApi,
		infoLog:  logger,
	}
}

// 保存群消息
func (this *SsdbOperator) SetKeyAndValue(key string, value []byte) (err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return err
	}
	err = this.redisApi.Set(key, string(value))
	if err != nil {
		this.infoLog.Printf("SetKeyAndValue failed key=%s vlaue=%s err=%s",
			key,
			value,
			err)
		return err
	} else {
		this.infoLog.Printf("SetKeyAndValue success key=%s valueLen=%v",
			key,
			len(value))
		return nil
	}
}

func (this *SsdbOperator) Hinc(hashName, key string, num int64) (val int64, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return val, err
	}
	val, err = this.redisApi.Hincrby(hashName, key, int(num))
	if err != nil {
		this.infoLog.Printf("Hinc hashName=%s key=%s num=%v err=%s", hashName, key, num, err)
		return val, err
	}
	return val, nil
}

func (this *SsdbOperator) HSet(hashName, key string, value []byte) (err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return err
	}
	err = this.redisApi.Hset(hashName, key, string(value))
	if err != nil {
		this.infoLog.Printf("Hset hashName=%s key=%s value=%s err=%s", hashName, key, value, err)
		return err
	}
	return nil
}

func (this *SsdbOperator) HGet(hashName, key string) (value string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return value, err
	}
	value, err = this.redisApi.Hget(hashName, key)
	if err != nil {
		this.infoLog.Printf("Hget hashName=%s key=%s err=%s", hashName, key, err)
		return value, err
	}
	return value, nil
}

// 这里的HscanArray紧紧用于取(cliSeq, endSeq)之间的所有msg_index 仅用于顺序遍历
// cliSeq 到 endSeq 是递增的 可以使用HMGet 列出所有的键之后迭代即可
func (this *SsdbOperator) HscanArray(hashName string, keyStart, keyEnd uint64, limit uint64) (keys []string, values []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	keyTermianl := keyEnd + 1
	if keyStart+limit < keyEnd {
		keyTermianl = keyStart + limit + 1
		this.infoLog.Printf("HscanArray adjust keyEnd keyStart=%v keyEnd=%v limit=%v keyTerminal=%v",
			keyStart,
			keyEnd,
			limit, keyTermianl)
	}

	this.infoLog.Printf("HscanArray keyStart=%v keyEnd=%v limit=%v keyTermial=%v",
		keyStart,
		keyEnd,
		limit,
		keyTermianl)
	// keyStart 已经取回去(keyStart, keyEnd] 左开右闭区间
	for i := keyStart + 1; i < keyTermianl; i += 1 {
		keys = append(keys, fmt.Sprintf("%v", i))
	}
	this.infoLog.Printf("HscanArray keyStart=%v keyEnd=%v limit=%v keys=%v",
		keyStart,
		keyEnd,
		limit,
		keys)
	values, err = this.redisApi.Hmget(hashName, keys)
	if err != nil {
		this.infoLog.Printf("HscanArray hashName=%s keyStart=%v  keyEnd=%v limit=%v err=%s",
			hashName,
			keyStart,
			keyEnd,
			limit,
			err)
		return nil, nil, err
	}
	return keys, values, nil
}

// 获取指定key 的计数
func (this *SsdbOperator) MultiGet(keys []string) (values []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, err
	}

	//获取指定key的计数
	values, err = this.redisApi.Mget(keys)
	if err != nil {
		this.infoLog.Printf("MultiGet MultiGetSlice failed keys=%v err=%s",
			keys,
			err)
		return nil, err
	}
	return values, nil
}

func (this *SsdbOperator) Hlen(hashName string) (count int64, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return count, err
	}
	count, err = this.redisApi.Hlen(hashName)
	if err != nil {
		this.infoLog.Printf("Hlen hashName=%s err=%s", hashName, err)
		return count, err
	}
	return count, err
}

func (this *SsdbOperator) Hdel(hashName, key string) (result bool, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return result, err
	}
	result, err = this.redisApi.Hdel(hashName, key)
	if err != nil {
		this.infoLog.Printf("Hdel hashName=%s key=%s err=%s", hashName, key, err)
		return result, err
	}
	return result, nil
}
