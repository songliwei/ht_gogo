package main

import (
	"fmt"
	"runtime/pprof"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc_store"
	"github.com/HT_GOGO/gotcp/tcpfw/project/cache_family/muc_msg/muc_msg_dbd/util"

	"github.com/garyburd/redigo/redis"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	"hash/crc64"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"
)

type Callback struct{}

type ClearHashMapTask struct {
	HashName string
	CliSeq   uint64
}

var (
	infoLog             *log.Logger
	ssdbOperator        *util.SsdbOperator
	writeAgentMastreApi *common.SrvToSrvApiV2
	writeAgentSlaveApi  *common.SrvToSrvApiV2
	cpuProfile          string
	memProfile          string
	memProfileRate      int
	clearHashMapChanMap map[int]chan *ClearHashMapTask
	clearHashMapCount   int
	//hashMapLock         sync.Mutex // sync mutex goroutines use reqRecord
)

const (
	ProcSlowThreshold = 300000
	HashMapSizeLimit  = 1000
	HashKeyMsgBase    = 100000000000 //每个人的群消息队列是1000亿条
	MsgCountThreshold = 100
	LatestMsgCount    = 10
)

const (
	HashKeyMaxMsgId = "0"
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gommdbd/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_muc_store.CMD_TYPE_CMD_STORE_MUC_MESSAGE_REQ):
		go ProcStoreMucMsg(c, head, packet)
	case uint32(ht_muc_store.CMD_TYPE_CMD_STORE_MUC_INDEX_REQ):
		go ProcStoreMucMsgIndex(c, head, packet)
	case uint32(ht_muc_store.CMD_TYPE_CMD_BATCH_GET_MUC_MSG_INDEX_REQ):
		go ProcGetMucMsgIndex(c, head, packet)
	case uint32(ht_muc_store.CMD_TYPE_CMD_MGET_KYES_REQ):
		go ProcMGetKeys(c, head, packet)
	case uint32(ht_muc_store.CMD_TYPE_CMC_CLEAR_MUC_MSG_INDEX_REQ):
		go ProcClearMucMsgIndex(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

func GetUserHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#gmsg", uid)
	return hashName
}

func GetMsgKye(roomId, fromId uint32) (key string) {
	timeStamp := time.Now().UnixNano() / 1000000 // change to ms
	key = fmt.Sprintf("%v_%v_%v", roomId, timeStamp, fromId)
	return key
}

// 1.proc store muc message
func ProcStoreMucMsg(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_muc_store.MucStoreRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStoreMucMsg no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INVALID_PARAM)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStoreMucMsg invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gommdbd/store_muc_msg_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_muc_store.MucStoreReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsg proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStoreMucMessageReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcStoreMucMsg GetStoreMucMessageReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcStoreMucMsg uid=%v roomId=%v cmd=%v ",
		subReqBody.GetFromId(),
		subReqBody.GetRoomId(),
		subReqBody.GetCmd())

	mucMsg := &ht_muc_store.MucMsg{
		FromId:  proto.Uint32(subReqBody.GetFromId()),
		RoomId:  proto.Uint32(subReqBody.GetRoomId()),
		Cmd:     proto.Uint32(subReqBody.GetCmd()),
		Format:  proto.Uint32(subReqBody.GetFormat()),
		Content: subReqBody.GetContent(),
		MsgTime: proto.Uint64(uint64(time.Now().UnixNano() / 1000000)),
	}
	mucMsgSlic, err := proto.Marshal(mucMsg)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsg Failed to proto.Marshal uid=%v roomId=%v cmd=%v err=%s",
			subReqBody.GetFromId(),
			subReqBody.GetRoomId(),
			subReqBody.GetCmd(),
			err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	key := GetMsgKye(subReqBody.GetRoomId(), subReqBody.GetFromId())
	err = ssdbOperator.SetKeyAndValue(key, mucMsgSlic)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsg ssdbOperator.SetKeyAndValue failed uid=%v roomId=%v cmd=%v err=%s",
			subReqBody.GetFromId(),
			subReqBody.GetRoomId(),
			subReqBody.GetCmd(),
			err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	rspBody.StoreMucMessageRspbody = &ht_muc_store.StoreMucMessageRspBody{
		Status: &ht_muc_store.MucStoreHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MsgIndex: []byte(key),
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	infoLog.Printf("ProcStoreMucMsg after SendRsp uid=%v roomId=%v cmd=%v ",
		subReqBody.GetFromId(),
		subReqBody.GetRoomId(),
		subReqBody.GetCmd())
	// Step2 notify write agent to update redis
	//双写更新Redis
	setReqBody := &ht_muc_store.MucStoreReqBody{
		SetKeyAndValueReqbody: &ht_muc_store.SetKeyAndValueReqBody{
			Key:   proto.String(key),
			Value: mucMsgSlic,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsg proto.Marshal  SetKeyAndValueReqBody failed uid=%v err=%s", subReqBody.GetFromId(), err)
		attr := "gommdbd/store_muc_msg_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_muc_store.CMD_TYPE_CMD_SET_KEY_AND_VALUE_REQ)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gommdbd/store_muc_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcStoreMucMsg after SetRedisCache uid=%v roomId=%v cmd=%v ",
		subReqBody.GetFromId(),
		subReqBody.GetRoomId(),
		subReqBody.GetCmd())
	return true
}

// 2.proc store muc msg index
func ProcStoreMucMsgIndex(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_muc_store.MucStoreRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStoreMucMsgIndex no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INVALID_PARAM)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStoreMucMsgIndex invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gommdbd/store_muc_msg_index_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_muc_store.MucStoreReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsgIndex proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStoreMessageIndexReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcStoreMucMsgIndex GetStoreMessageIndexReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uidList := subReqBody.GetUidList()
	if len(uidList) == 0 {
		infoLog.Printf("ProcStoreMucMsgIndex uidList empty msgIndex=%s", subReqBody.GetMsgIndex())
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
		rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
		}
		return true
	}

	rspBody.StoreMessageIndexRspbody = &ht_muc_store.StoreMessageIndexRspBody{
		Status: &ht_muc_store.MucStoreHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	infoLog.Printf("ProcStoreMucMsgIndex msgIndex=%s uidList=%v", subReqBody.GetMsgIndex(), uidList)

	//hashMapLock.Lock()
	var hashMapList []*ht_muc_store.HashMapItem
	for i, v := range uidList {
		// infoLog.Printf("ProcStoreMucMsgIndex msgIndex=%s index=%v uid=%v", subReqBody.GetMsgIndex(), i, v)
		// Step1: get current msg id
		hashName := GetUserHashMapName(v)
		val, err := ssdbOperator.Hinc(hashName, HashKeyMaxMsgId, 1)
		// infoLog.Printf("ProcStoreMucMsgIndex hashName=%s val=%v", hashName, val)
		if err != nil || val == -1 {
			// add static
			attr := "gommdbd/inc_user_msg_count_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcStoreMucMsgIndex msgIndex=%s index=%v uid=%v ssdbOperator.Hinc failed err=%s",
				subReqBody.GetMsgIndex(),
				i,
				v,
				err)
			continue
		}
		key := fmt.Sprintf("%v", val+HashKeyMsgBase)
		if key == "99999999999" {
			// add static
			attr := "gommdbd/inc_user_msg_count_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcStoreMucMsgIndex hashMap=%s key=%v err=%s",
				hashName,
				key,
				err)
			continue
		}

		err = ssdbOperator.HSet(hashName, key, subReqBody.GetMsgIndex())
		if err != nil {
			// add static
			attr := "gommdbd/set_user_msg_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcStoreMucMsgIndex msgIndex=%s index=%v uid=%v ssdbOperator.Hset failed err=%s",
				subReqBody.GetMsgIndex(),
				i,
				v,
				err)
			continue
		}
		// infoLog.Printf("ProcStoreMucMsgIndex HSet hashName=%s key=%s", hashName, key)
		hashItem := &ht_muc_store.HashMapItem{
			HashName: proto.String(hashName),
			KeyValuePair: []*ht_muc_store.KeyValuePair{
				&ht_muc_store.KeyValuePair{
					Key:   proto.String(HashKeyMaxMsgId),
					Value: []byte(fmt.Sprintf("%v", val)),
				},
				&ht_muc_store.KeyValuePair{
					Key:   proto.String(key),
					Value: subReqBody.GetMsgIndex(),
				},
			},
		}
		hashMapList = append(hashMapList, hashItem)
	}
	//hashMapLock.Unlock()

	// Step2 notify write agent to update redis
	//双写更新Redis
	setReqBody := &ht_muc_store.MucStoreReqBody{
		BatchSetHashMapReqbody: &ht_muc_store.BatchSetHashMapReqBody{
			HashList: hashMapList,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcStoreMucMsgIndex proto.Marshal BatchSetHashMapReqBody failed uid=%v err=%s", head.Uid, err)
		attr := "gommdbd/store_muc_msg_index_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_muc_store.CMD_TYPE_CMD_BATCH_SET_HASH_MAP_REQ)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gommdbd/store_muc_msg_index_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 检查slic 是否是全部为空
func IsWholeNilValue(values []string) (ret bool) {
	if len(values) == 0 {
		ret = true
		return ret
	}
	ret = true
	for _, v := range values {
		if len(v) != 0 {
			ret = false
			break
		}
	}
	return ret
}

// 3.proc get chat record
func ProcGetMucMsgIndex(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_muc_store.MucStoreRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetMucMsgIndex no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetMucMsgIndex invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gommdbd/get_muc_msg_index_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_muc_store.MucStoreReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetMucMsgIndex proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetMucMessageIndexReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetMucMsgIndex GetGetMucMessageIndexReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	opUid := subReqBody.GetOpUid()
	cliSeq := subReqBody.GetCliSeqId()
	if opUid == 0 {
		infoLog.Printf("ProcGetMucMsgIndex param error failed uid=%v cmd=0x%4x seq=%v opUid=0", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	hashName := GetUserHashMapName(opUid)
	// Step1: get current mas msg id
	strMaxMsgId, err := ssdbOperator.HGet(hashName, HashKeyMaxMsgId)
	if err != nil {
		if err == redis.ErrNil {
			infoLog.Printf("ProcGetMucMsgIndex ssdbOperator.HGet empty set hashName=%s key=%s cliSeq=%v err=%s",
				hashName,
				HashKeyMaxMsgId,
				cliSeq,
				err)
			result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
			rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
				Status: &ht_muc_store.MucStoreHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("get success"),
				},
				IdToIndex: nil,
				MaxSeqId:  proto.Uint64(0), // 不存在hashmap时，直接返回0
				HasMore:   proto.Uint32(0), // 1:还有群消息 0:没有新的群消息
			}
			return true
		} else {
			infoLog.Printf("ProcGetMucMsgIndex ssdbOperator.HGet failed uid=%v opUid=%v cliSeq=%v err=%s", head.Uid, opUid, cliSeq, err)
			result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
				Status: &ht_muc_store.MucStoreHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb internal error"),
				},
			}
			return false
		}
	}
	if strMaxMsgId == "" {
		infoLog.Printf("ProcGetMucMsgIndex strMaxMsgId empty uid=%v opUid=%v cliSeq=%v", head.Uid, opUid, cliSeq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb internal error"),
			},
		}
		return false
	}

	curMaxMsgId, err := strconv.ParseUint(strMaxMsgId, 10, 64)
	if err != nil {
		infoLog.Printf("ProcGetMucMsgIndex strconv.ParseUint failed uid=%v opUid=%v cliSeq=%v err=%s", head.Uid, opUid, cliSeq, err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("strconv internal error"),
			},
		}
		return false
	}
	transMaxMsgId := curMaxMsgId + HashKeyMsgBase
	// Step2: cliSeqId greate than current max msg id return success
	if cliSeq >= transMaxMsgId {
		infoLog.Printf("ProcGetMucMsgIndex opUid=%v cliSeq=%v transMaxMsgId=%v equal or invalid", opUid, cliSeq, transMaxMsgId)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
		rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get success"),
			},
			IdToIndex: nil,
			MaxSeqId:  proto.Uint64(transMaxMsgId),
			HasMore:   proto.Uint32(0), // 1:还有群消息 0:没有新的群消息
		}
		return true
	}
	var protoMaxSeqId uint64 = 0
	var protoHasMore uint32 = 0
	var outKeys, outValues []string
	if cliSeq < HashKeyMsgBase {
		infoLog.Printf("ProcGetMucMsgIndex opUid=%v cliSeq=%v err get latest ten msg", opUid, cliSeq)
		if curMaxMsgId < LatestMsgCount {
			cliSeq = HashKeyMsgBase
		} else {
			cliSeq = transMaxMsgId - 10 // 单用户使用0来取群消息时，返回最新的10条消息
		}
		protoMaxSeqId = transMaxMsgId
		protoHasMore = 0
	}
	endSeq := transMaxMsgId
	for {
		if cliSeq+MsgCountThreshold >= transMaxMsgId {
			endSeq = transMaxMsgId
			protoMaxSeqId = transMaxMsgId
			protoHasMore = 0 // no more message
		} else {
			endSeq = cliSeq + MsgCountThreshold
			protoMaxSeqId = endSeq
			protoHasMore = 1 // 1: has more message
		}
		// Step3: Hscan key-value data
		// keyStart := fmt.Sprintf("%v", cliSeq)
		// keyEnd := fmt.Sprintf("%v", endSeq)
		outKeys, outValues, err = ssdbOperator.HscanArray(hashName, cliSeq, endSeq, MsgCountThreshold)
		if err != nil {
			infoLog.Printf("ProcGetMucMsgIndex ssdbOperator.HscanArray failed uid=%v opUid=%v cliSeq=%v endSeq=%v err=%s",
				head.Uid,
				opUid,
				cliSeq,
				endSeq,
				err)
			result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
				Status: &ht_muc_store.MucStoreHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("HscanArray internal error"),
				},
			}
			return false
		}
		if IsWholeNilValue(outValues) {
			infoLog.Printf("ProcGetMucMsgIndex ssdbOperator.HscanArray uid=%v opUid=%v cliSeq=%v endSeq=%v whole empty",
				head.Uid,
				opUid,
				cliSeq,
				endSeq)
			// 全部都是空的将endSeq 赋值给CliSeq
			cliSeq = endSeq
			// 如果当前cliSeq 需要超过了最大的序号 直接返回
			if cliSeq >= transMaxMsgId {
				infoLog.Printf("ProcGetMucMsgIndex opUid=%v cliSeq=%v transMaxMsgId=%v equal or invalid", opUid, cliSeq, transMaxMsgId)
				result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
				rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
					Status: &ht_muc_store.MucStoreHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("get success"),
					},
					IdToIndex: nil,
					MaxSeqId:  proto.Uint64(transMaxMsgId),
					HasMore:   proto.Uint32(0), // 1:还有群消息 0:没有新的群消息
				}
				return true
			} else {
				// 结束本次循环 继续取下次
				infoLog.Printf("ProcGetMucMsgIndex ssdbOperator.HscanArray uid=%v opUid=%v cliSeq=%v endSeq=%v whole empty move on",
					head.Uid,
					opUid,
					cliSeq,
					endSeq)
				attr := "gommdbd/get_msg_index_seq_jump"
				libcomm.AttrAdd(attr, 1)
				continue
			}
		} else {
			infoLog.Printf("ProcGetMucMsgIndex ssdbOperator.HscanArray uid=%v opUid=%v cliSeq=%v endSeq=%v has msg index",
				head.Uid,
				opUid,
				cliSeq,
				endSeq)
			break
		}
	}

	keyValues := make([]string, 2*len(outValues))
	for i := 0; i < len(keyValues); i += 2 {
		keyValues[i] = outKeys[i/2]
		keyValues[i+1] = outValues[i/2]
	}
	// Step4:answer first
	result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody.GetMucMessageIndexRspbody = &ht_muc_store.GetMucMessageIndexRspBody{
		Status: &ht_muc_store.MucStoreHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		IdToIndex: keyValues,
		MaxSeqId:  proto.Uint64(protoMaxSeqId),
		HasMore:   proto.Uint32(protoHasMore), // 1:还有群消息 0:没有新的群消息
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// update redis cache
	setReqBody := &ht_muc_store.MucStoreReqBody{
		ReloadMsgIndexReqbody: &ht_muc_store.ReloadMsgIndexReqBody{
			OpUid:    proto.Uint32(opUid),
			CliSeqId: proto.Uint64(cliSeq),
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcGetMucMsgIndex proto.Marshal ReloadMsgIndexReqBody failed opUid=%v cliSeq=%v err=%s", opUid, cliSeq, err)
		attr := "gommdbd/get_muc_msg_index_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_muc_store.CMD_TYPE_CMD_RELOAD_MSG_INDEX_REQ)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gommdbd/get_muc_msg_index_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 4.proc get hash map
func ProcMGetKeys(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_muc_store.MucStoreRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcMGetKeys no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INVALID_PARAM)
		rspBody.MgetKeysRspbody = &ht_muc_store.MGetKeysRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcMGetKeys invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gommdbd/mget_key_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_muc_store.MucStoreReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcMGetKeys proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.MgetKeysRspbody = &ht_muc_store.MGetKeysRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetMgetKeysReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcMGetKeys GetMgetKeysReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.MgetKeysRspbody = &ht_muc_store.MGetKeysRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	keys := subReqBody.GetKeyList()
	values, err := ssdbOperator.MultiGet(keys)
	if err != nil {
		infoLog.Printf("ProcMGetKeys ssdbOperator.MultiGet failed uid=%v cmd=0x%4x seq=%v err=%s", head.Uid, head.Cmd, head.Seq, err)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INTERNAL_ERR)
		rspBody.MgetKeysRspbody = &ht_muc_store.MGetKeysRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("MultiGet error"),
			},
		}
		return false
	}

	rspBody.MgetKeysRspbody = &ht_muc_store.MGetKeysRspBody{
		Status: &ht_muc_store.MucStoreHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ValueList: values,
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gommdbd/get_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	// no need to update redis
	return true
}

// 5.porc clear muc message index
func ProcClearMucMsgIndex(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_muc_store.MucStoreRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcClearMucMsgIndex no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INVALID_PARAM)
		rspBody.ClearMucMessageIndexRspbody = &ht_muc_store.ClearMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcClearMucMsgIndex invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gommdbd/clear_muc_msg_index_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_muc_store.MucStoreReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcClearMucMsgIndex proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.ClearMucMessageIndexRspbody = &ht_muc_store.ClearMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetClearMucMessageIndexReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcClearMucMsgIndex GetClearMucMessageIndexReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_PB_ERR)
		rspBody.ClearMucMessageIndexRspbody = &ht_muc_store.ClearMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	opUid := subReqBody.GetOpUid()
	cliSeq := subReqBody.GetCliSeqId()
	if opUid == 0 {
		infoLog.Printf("ProcClearMucMsgIndex param error failed uid=%v cmd=0x%4x seq=%v opUid=0", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ClearMucMessageIndexRspbody = &ht_muc_store.ClearMucMessageIndexRspBody{
			Status: &ht_muc_store.MucStoreHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step4:answer first
	result = uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS)
	rspBody.ClearMucMessageIndexRspbody = &ht_muc_store.ClearMucMessageIndexRspBody{
		Status: &ht_muc_store.MucStoreHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// 将任务放入队列中
	hashName := GetUserHashMapName(opUid)
	task := &ClearHashMapTask{
		HashName: hashName,
		CliSeq:   cliSeq,
	}
	index := int(Crc64(hashName) % uint64(clearHashMapCount))
	infoLog.Printf("ProcClearMucMsgIndex hashName=%s cliSeq=%v index=%v",
		hashName,
		cliSeq,
		index)
	select {
	case clearHashMapChanMap[index] <- task:
		infoLog.Printf("ProcClearMucMsgIndex message hashName=%s index=%v put into chan succ", hashName, index)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gommdbd/clear_muc_msg_index_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func Crc64(input string) uint64 {
	tab := crc64.MakeTable(crc64.ISO)
	return crc64.Checksum([]byte(input), tab)
}

func ClearHashMapTaskHandleLoop(index int) {
	defer func() {
		recover()
	}()
	infoLog.Printf("ClearHashMapTaskHandleLoop index=%v", index)
	for {
		select {
		case task := <-clearHashMapChanMap[index]:
			TaskClearHashMap(task.HashName, task.CliSeq)
		}
	}
}

func TaskClearHashMap(hashName string, cliSeq uint64) {
	infoLog.Printf("TaskClearHashMap hashName=%s cliSeq=%v", hashName, cliSeq)
	if hashName == "" {
		infoLog.Printf("TaskClearHashMap hashName=%s cliSeq=%v invalid param", hashName, cliSeq)
	}
	// Step1: get current mas msg id
	strMaxMsgId, err := ssdbOperator.HGet(hashName, HashKeyMaxMsgId)
	if err != nil || strMaxMsgId == "" {
		infoLog.Printf("TaskClearHashMap ssdbOperator.HGet failed hashName=%s cliSeq=%v err=%s", hashName, cliSeq, err)
		return
	}
	curMaxMsgId, err := strconv.ParseUint(strMaxMsgId, 10, 64)
	if err != nil {
		infoLog.Printf("TaskClearHashMap strconv.ParseUint failed hashName=%s cliSeq=%v err=%s", hashName, cliSeq, err)
		return
	}
	transMaxMsgId := curMaxMsgId + HashKeyMsgBase
	// Step2: cliSeqId greate than current max msg id return success
	if cliSeq >= transMaxMsgId || cliSeq <= HashKeyMsgBase {
		infoLog.Printf("TaskClearHashMap hashName=%s cliSeq=%v transMaxMsgId=%v equal or invalid", hashName, cliSeq, transMaxMsgId)
		return
	}
	// 参数检查完成之后 从cliSeq 开始执行删除 如果出现错误则停止
	for index := cliSeq - 1; index > HashKeyMsgBase; index -= 1 {
		itemKey := fmt.Sprintf("%v", index)
		_, err := ssdbOperator.Hdel(hashName, itemKey)
		if err != nil {
			infoLog.Printf("TaskClearHashMap hashName=%s cliSeq=%v maxId=%v del itemKey=%s err=%s",
				hashName,
				cliSeq,
				transMaxMsgId,
				itemKey,
				err)
			// 出错之后直接退出
			break
		} else {
			// infoLog.Printf("TaskClearHashMap hashName=%s cliSeq=%v maxId=%v del itemKey=%s",
			// 	hashName,
			// 	cliSeq,
			// 	transMaxMsgId,
			// 	itemKey)
		}
	}
	// 正向检测一次是否已经清空了
	for beginSeq := uint64(HashKeyMsgBase + 1); beginSeq < cliSeq; beginSeq += 1 {
		itemKey := fmt.Sprintf("%v", beginSeq)
		_, err := ssdbOperator.Hdel(hashName, itemKey)
		if err != nil {
			infoLog.Printf("TaskClearHashMap hashName=%s cliSeq=%v maxId=%v del itemKey=%s err=%s",
				hashName,
				cliSeq,
				transMaxMsgId,
				itemKey,
				err)
			// 出错之后直接退出
			break
		} else {
			// infoLog.Printf("TaskClearHashMap hashName=%s cliSeq=%v maxId=%v del itemKey=%s",
			// 	hashName,
			// 	cliSeq,
			// 	transMaxMsgId,
			// 	itemKey)
		}
	}

	return
}

func SetRedisCache(head *common.HeadV2, reqPayLoad []byte) {
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}

	redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisSlavePacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentSlaveApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_muc_store.MucStoreRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	// infoLog.Printf("SendRsp cmd=%v ret=%v len=%v remoteAdd=%v", head.Cmd, ret, head.Len, c.GetExtraData())
	infoLog.Printf("SendRsp cmd=%v ret=%v len=%v ", head.Cmd, ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func startCPUProfile() {
	if cpuProfile != "" {
		f, err := os.Create(cpuProfile)
		if err != nil {
			infoLog.Printf("Can not create cpu profile output file: %s", err)
			return
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			infoLog.Printf("Can not start cpu profile: %s", err)
			f.Close()
			return
		}
	}
}

func stopCPUProfile() {
	if cpuProfile != "" {
		pprof.StopCPUProfile() // 把记录的概要信息写到已指定的文件
	}
}

func startMemProfile() {
	if memProfile != "" && memProfileRate > 0 {
		runtime.MemProfileRate = memProfileRate
	}
}

func stopMemProfile() {
	if memProfile != "" {
		f, err := os.Create(memProfile)
		if err != nil {
			infoLog.Printf("Can not create mem profile output file: %s", err)
			return
		}
		if err = pprof.WriteHeapProfile(f); err != nil {
			infoLog.Printf("Can not write %s: %s", memProfile, err)
		}
		f.Close()
	}
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// read profile config path
	cpuProfile = cfg.Section("PROFILE").Key("cpu_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/cpu.prof")
	memProfile = cfg.Section("PROFILE").Key("mem_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/mem.out")
	memProfileRate = cfg.Section("PROFILE").Key("mem_rate").MustInt(512 * 1024)
	// read ssdb config
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	// ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	// ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	// ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	redisApi := common.NewRedisApi(fmt.Sprintf("%s:%v", ssdbHost, ssdbPort))
	// if err != nil {
	// 	infoLog.Printf("common.NewSsdbApi failed err=%s", err)
	// 	checkError(err)
	// 	return
	// }

	//	ssdbOperator = util.NewSsdbOperator(ssdbApi, infoLog)
	ssdbOperator = util.NewSsdbOperator(redisApi, infoLog)

	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%v port=%v", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%v port=%v", slaveIp, slavePort)
	writeAgentSlaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init clear hash map task count
	clearHashMapCount = cfg.Section("CLEAR").Key("count").MustInt(10)
	clearHashMapChanLimit := cfg.Section("CLEAR").Key("length_limit").MustInt(10000)
	clearHashMapChanMap = make(map[int]chan *ClearHashMapTask, clearHashMapCount)
	for i := 0; i < clearHashMapCount; i += 1 {
		clearHashMapChanMap[i] = make(chan *ClearHashMapTask, clearHashMapChanLimit)
	}

	for i := 0; i < clearHashMapCount; i += 1 {
		go ClearHashMapTaskHandleLoop(i)
	}
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	//startCPUProfile()
	//startMemProfile()
	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	//stopCPUProfile()
	//stopMemProfile()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
