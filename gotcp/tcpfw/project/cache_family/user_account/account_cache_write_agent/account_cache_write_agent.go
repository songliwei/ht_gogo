package main

import (
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_accountcache"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	redisMasterApi *common.RedisApi
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
	ExpirePeriod       = 604800 //7 * 86400 = 7 day   units:second
	VERSION_FIELD      = 0
)

const (
	UserTypeField   = "usertype"
	UserCNonceFiled = "cnonce"
	PassWdField     = "passwd"
	EmailField      = "email"
	VerifyField     = "verify"
	RegFromField    = "regfrom"
	RegTimeField    = "regtime"
	ModTimeField    = "modtime"
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close() // 关闭连接
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint6(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close() // 关闭连接
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		c.Close() // 关闭连接
		return false
	}

	// 统计总的请求量
	attr := "goacagent/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch uint32(head.Cmd) {
	case uint32(ht_account_cache.CMD_TYPE_CMD_SET_USER_ACCOUNT_CACHE):
		go ProcSetUserAccountCache(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_SET_USER_PASSWD):
		go ProcSetUserPassWd(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_SET_USER_EMAIL):
		go ProcSetUserEmail(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_SET_UNREGISTER_USER_ACCOUNT):
		go ProcSetUnregisterUser(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_SET_ZSET_LIST):
		go ProcSetZsetList(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_DEL_KEY_IN_REDIS):
		go ProcDelKey(c, head, packet.GetBody())
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

// 1.proc set user account cache
func ProcSetUserAccountCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserAccountCacheRspbody = &ht_account_cache.SetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserAccountCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacagent/set_user_base_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserAccountCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserAccountCacheRspbody = &ht_account_cache.SetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserAccountCacheReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserAccountCache GetSetUserAccountCacheReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserAccountCacheRspbody = &ht_account_cache.SetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := GetHashMapName(head.Uid)
	accountInfo := subReqBody.GetAccountCache()
	if accountInfo == nil {
		infoLog.Println("ProcSetUserAccountCache subReqBody.GetAccountCache() nil uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserAccountCacheRspbody = &ht_account_cache.SetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body nil"),
			},
		}
		return false
	}

	// write into master and slave redis
	var fieldAndValue []string
	// add user type
	fieldAndValue = append(fieldAndValue, UserTypeField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", accountInfo.GetUserType()))

	// add cnonce
	fieldAndValue = append(fieldAndValue, UserCNonceFiled)
	fieldAndValue = append(fieldAndValue, string(accountInfo.GetCnonce()))
	// add passwd
	fieldAndValue = append(fieldAndValue, PassWdField)
	fieldAndValue = append(fieldAndValue, string(accountInfo.GetPassWd()))

	// add email
	fieldAndValue = append(fieldAndValue, EmailField)
	fieldAndValue = append(fieldAndValue, string(accountInfo.GetEmail()))

	// add Verify
	fieldAndValue = append(fieldAndValue, VerifyField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", accountInfo.GetVerify()))

	// add regFrom
	fieldAndValue = append(fieldAndValue, RegFromField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", accountInfo.GetRegFrom()))

	// add regTime
	fieldAndValue = append(fieldAndValue, RegTimeField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", accountInfo.GetRegTime()))

	// add modify time
	fieldAndValue = append(fieldAndValue, ModTimeField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", accountInfo.GetModifyTime()))

	err = redisMasterApi.Hmset(hashName, fieldAndValue)
	if err != nil {
		infoLog.Printf("ProcSetUserAccountCache redisMasterApi.Hmset uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
		rspBody.SetUserAccountCacheRspbody = &ht_account_cache.SetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("redis exec error"),
			},
		}
		attr := "goacagent/set_user_account_cache_redis_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	err = redisMasterApi.Expire(hashName, ExpirePeriod)
	if err != nil {
		infoLog.Printf("ProcSetUserAccountCache redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
		attr := "goacagent/set_key_expire_redis_err"
		libcomm.AttrAdd(attr, 1)
	}

	rspBody.SetUserAccountCacheRspbody = &ht_account_cache.SetUserAccountCacheRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func GetHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#account", uid)
	return hashName
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_account_cache.AccountCacheRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func ProcSetUserPassWd(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserPassWdRspbody = &ht_account_cache.SetUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserPassWd invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacagent/update_user_passwd_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserPassWd proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserPassWdRspbody = &ht_account_cache.SetUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserPassWdReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserPassWd GetSetUserPassWdReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserPassWdRspbody = &ht_account_cache.SetUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserPassWd uid=%v passwd=%s success", head.Uid, subReqBody.GetPassWd())
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		err = redisMasterApi.Hset(hashName, PassWdField, string(subReqBody.GetPassWd()))
		if err != nil {
			infoLog.Printf("ProcSetUserPassWd uid=%v redisMasterApi.Hmset failed err=%s", head.Uid, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserPassWdRspbody = &ht_account_cache.SetUserPassWdRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "goacagent/update_user_passwd_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserPassWd redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "goacagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "goacagent/update_user_passwd_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserPassWd not exist hashname=%s no need update passwd=%s", hashName, subReqBody.GetPassWd())
	}

	rspBody.SetUserPassWdRspbody = &ht_account_cache.SetUserPassWdRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ProcSetUserEmail(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserEmailRspbody = &ht_account_cache.SetUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserEmail invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacagent/set_user_email_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserEmail proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserEmailRspbody = &ht_account_cache.SetUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserEmailReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserEmail GetSetUserEmailReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserEmailRspbody = &ht_account_cache.SetUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserEmail uid=%v email=%s success", head.Uid, subReqBody.GetEmail())
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		err = redisMasterApi.Hset(hashName, EmailField, string(subReqBody.GetEmail()))
		if err != nil {
			infoLog.Printf("ProcSetUserEmail uid=%v redisMasterApi.Hmset failed err=%s", head.Uid, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserEmailRspbody = &ht_account_cache.SetUserEmailRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "goacagent/update_user_summertime_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserEmail redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "goacagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "goacagent/set_user_email_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserEmail not exist hashname=%s no need update email=%s", hashName, subReqBody.GetEmail())
	}

	rspBody.SetUserEmailRspbody = &ht_account_cache.SetUserEmailRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ProcSetUnregisterUser(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUnregisterAccountRspbody = &ht_account_cache.SetUnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUnregisterUser invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacagent/unregister_user_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUnregisterUser proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUnregisterAccountRspbody = &ht_account_cache.SetUnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUnregisterAccountReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUnregisterUser GetSetUnregisterAccountReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUnregisterAccountRspbody = &ht_account_cache.SetUnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUnregisterUser uid=%v success", head.Uid)
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		err = redisMasterApi.Del(hashName)
		if err != nil {
			infoLog.Printf("ProcSetUnregisterUser uid=%v redisMasterApi.Del failed err=%s", head.Uid, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUnregisterAccountRspbody = &ht_account_cache.SetUnregisterAccountRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "goacagent/unregister_user_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
	} else {
		attr := "goacagent/unregister_user_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUnregisterUser not exist hashname=%s no need unregister", hashName)
	}

	rspBody.SetUnregisterAccountRspbody = &ht_account_cache.SetUnregisterAccountRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ProcSetZsetList(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetZsetListRspbody = &ht_account_cache.SetZsetListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetZsetList invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacagent/set_zset_list"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetZsetList proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetZsetListRspbody = &ht_account_cache.SetZsetListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetZsetListReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetZsetList GetSetZsetListRspbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetZsetListRspbody = &ht_account_cache.SetZsetListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	zsetName := subReqBody.GetZsetName()
	keys := subReqBody.GetKeys()
	scores := subReqBody.GetScores()
	infoLog.Printf("ProcSetZsetList zsetName=%s keyLen=%v scoreLen=%v", zsetName, len(keys), len(scores))
	if len(keys) != len(scores) {
		infoLog.Println("ProcSetZsetList zsetName=%s keyLen=%v scoreLen=%v length not equal", zsetName, len(keys), len(scores))
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.SetZsetListRspbody = &ht_account_cache.SetZsetListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	if len(keys) == 0 {
		keys = append(keys, VERSION_FIELD)
		scores = append(scores, uint32(time.Now().Unix()))
	}
	keyScoreSlic := []uint32{}
	for i, v := range keys {
		keyScoreSlic = append(keyScoreSlic, v)
		keyScoreSlic = append(keyScoreSlic, scores[i])
	}
	//设置到redis中
	_, err = redisMasterApi.ZaddSlice(zsetName, keyScoreSlic)
	if err != nil {
		infoLog.Printf("ProcSetZsetList redisMasterApi.ZaddSlice failed zsetName=%s err=%s", zsetName, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
		rspBody.SetZsetListRspbody = &ht_account_cache.SetZsetListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("redis master error"),
			},
		}
		attr := "goacagent/set_zset_redis_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// 设置过期时间
	err = redisMasterApi.Expire(zsetName, ExpirePeriod)
	if err != nil {
		infoLog.Printf("ProcSetZsetList redisMasterApi.Expire uid=%v zsetName=%s failed err=%s", head.Uid, zsetName, err)
		attr := "goacagent/set_key_expire_redis_err"
		libcomm.AttrAdd(attr, 1)
	}

	rspBody.SetZsetListRspbody = &ht_account_cache.SetZsetListRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ProcDelKey(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.DelKeyInRedisRspbody = &ht_account_cache.DelKeyInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelKey invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacagent/del_key_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelKey proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelKeyInRedisRspbody = &ht_account_cache.DelKeyInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelKeyInRedisReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcDelKey GetDelKeyInRedisReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelKeyInRedisRspbody = &ht_account_cache.DelKeyInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	key := subReqBody.GetDelKey()
	infoLog.Printf("ProcDelKey delKey=%s", key)

	//设置到redis中
	err = redisMasterApi.Del(key)
	if err != nil {
		infoLog.Printf("ProcDelKey redisMasterApi.Del failed delKey=%s err=%s", key, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
		rspBody.DelKeyInRedisRspbody = &ht_account_cache.DelKeyInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("redis master error"),
			},
		}
		attr := "goacagent/del_key_redis_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	rspBody.DelKeyInRedisRspbody = &ht_account_cache.DelKeyInRedisRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
