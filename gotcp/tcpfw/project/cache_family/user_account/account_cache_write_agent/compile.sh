#!/bin/bash
CURDIR=`pwd`

go build $CURDIR/account_cache_write_agent.go

cp -f $CURDIR/account_cache_write_agent  /home/ht/goproj/cache_family/user_account/account_cache_write_agent/bin/.

killall account_cache_write_agent
