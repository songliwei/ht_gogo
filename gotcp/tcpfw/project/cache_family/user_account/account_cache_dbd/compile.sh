#!/bin/bash
CURDIR=`pwd`

go build $CURDIR/account_cache_dbd.go

cp -f $CURDIR/account_cache_dbd /home/ht/goproj/cache_family/user_account/account_cache_dbd/bin/.

killall account_cache_dbd
