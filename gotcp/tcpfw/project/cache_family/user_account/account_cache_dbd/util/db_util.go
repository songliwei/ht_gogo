// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

// Error type
var (
	ErrNilDbObject = errors.New("not set object current is nil")
	ErrDbParam     = errors.New("err param error")
)

type AccountStore struct {
	// HT_USER_ACCOUNT
	UserType uint32 //用户类别0-测试 1-内测 2-旧版本用户 3-新环境正式用户 4-后台删除 5-用户删除自己 6-客服 7-正式用户中的测试 8-推广用户
	CNonce   string //用户注册时候产生的随机NONCE
	PassWd   string //加密过的密码 = MD5(PWD+CNONCE)
	Email    string //注册邮箱
	Verify   uint32 //邮箱验证情况:0=未验证,1=已经发送,2=ses回执确认发送,3=ses回执确认失败,4=确认
	RegFrom  uint32 //0=unknown,1=email,2=weibo,3=tweet,4=facebook
	RegTime  int64  //注册时间-readonly
	ModTime  int64  //上一次密码或邮箱修改的时间
}

type StrUserType struct {
	UserId   uint32
	UserType uint32
}

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

func (this *DbUtil) GetAccountInfoFromDb(uid uint32) (accountInfo *AccountStore, err error) {
	if this.db == nil {
		return nil, ErrNilDbObject
	}

	var storeUserType, storeVerify, storeRegFrom, storeRegTime, storeModTime sql.NullInt64
	var storeCNonce, storePassWd, storeEmail sql.NullString
	err = this.db.QueryRow("SELECT USERTYPE, CNONCE, PASSWORD, EMAIL, VERIFY, REG_FROM, UNIX_TIMESTAMP(REGTIME), UNIX_TIMESTAMP(MODTIME) FROM HT_USER_ACCOUNT WHERE USERID = ?",
		uid).Scan(&storeUserType, &storeCNonce, &storePassWd, &storeEmail, &storeVerify,
		&storeRegFrom, &storeRegTime, &storeModTime)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetAccountInfoFromDb not found uid=%v in HT_USER_ACCOUNT err=%s", uid, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetAccountInfoFromDb exec HT_USER_ACCOUNT failed uid=%v, err=%s", uid, err)
		return nil, err
	default:
	}

	var userType, verify, regFrom uint32
	var regTime, modTime int64
	var cNonce, passWd, email string

	if storeUserType.Valid {
		userType = uint32(storeUserType.Int64)
	}

	if storeVerify.Valid {
		verify = uint32(storeVerify.Int64)
	}

	if storeRegFrom.Valid {
		regFrom = uint32(storeRegFrom.Int64)
	}

	if storeRegTime.Valid {
		regTime = storeRegTime.Int64
	}

	if storeModTime.Valid {
		modTime = storeModTime.Int64
	}

	if storeCNonce.Valid {
		cNonce = storeCNonce.String
	}

	if storePassWd.Valid {
		passWd = storePassWd.String
	}

	if storeEmail.Valid {
		email = storeEmail.String
	}

	accountInfo = &AccountStore{
		// HT_USER_ACCOUNT
		UserType: userType,
		CNonce:   cNonce,
		PassWd:   passWd,
		Email:    email,
		Verify:   verify,
		RegFrom:  regFrom,
		RegTime:  regTime,
		ModTime:  modTime,
	}

	return accountInfo, nil
}

func (this *DbUtil) UpdateUserPassWdInDb(uid uint32, passWd string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	if uid == 0 {
		return ErrDbParam
	}

	_, err = this.db.Exec("UPDATE HT_USER_ACCOUNT set PASSWORD=? WHERE USERID=?;",
		passWd,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserPassWdInDb insert faield uid=%v err=%v", uid, err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) UpdateUserEmailInDb(uid uint32, email string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	if uid == 0 {
		return ErrDbParam
	}

	_, err = this.db.Exec("UPDATE HT_USER_ACCOUNT SET EMAIL=? WHERE USERID=?;",
		email,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserEmailInDb update HT_USER_ACCOUNT faield uid=%v err=%v", uid, err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) UnregisterAccountInDb(uid uint32, email string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	if uid == 0 {
		return ErrDbParam
	}

	_, err = this.db.Exec("UPDATE HT_USER_ACCOUNT SET USERTYPE=5, EMAIL=?, MODTIME=UTC_TIMESTAMP() WHERE USERID=?;",
		email,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserEmailInDb update HT_USER_ACCOUNT faield uid=%v err=%v", uid, err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) QueryIllegalUserAccountInDb(updateTime uint64, isInit uint32) (uidSlice []StrUserType, newTime uint64, err error) {
	if this.db == nil {
		return nil, newTime, ErrNilDbObject
	}

	if isInit != 0 {
		// 查询全部过期帐号
		rows, err := this.db.Query("SELECT USERID, USERTYPE, MODTIME FROM HT_USER_ACCOUNT WHERE UNIX_TIMESTAMP(MODTIME) > ? AND USERTYPE >= 4 ORDER BY MODTIME DESC;", updateTime)
		if err != nil {
			return nil, newTime, err
		}
		defer rows.Close()
		firstRow := true
		for rows.Next() {
			var storeUid, storeUserType, storeModTime sql.NullInt64
			if err := rows.Scan(&storeUid, &storeUserType, &storeModTime); err != nil {
				this.infoLog.Printf("QueryIllegalUserAccountInDb rows.Scan failed")
				continue
			}

			var uid, userType uint32
			var modTime int64
			if storeUid.Valid {
				uid = uint32(storeUid.Int64)
			}

			if storeUserType.Valid {
				userType = uint32(storeUserType.Int64)
			}

			if storeModTime.Valid {
				modTime = storeModTime.Int64
			}
			iterm := StrUserType{
				UserId:   uid,
				UserType: userType,
			}
			uidSlice = append(uidSlice, iterm)
			if firstRow {
				newTime = uint64(modTime)
				firstRow = false
			}
		}
	} else {
		// 查询全部过期帐号
		rows, err := this.db.Query("SELECT USERID, USERTYPE, MODTIME FROM HT_USER_ACCOUNT WHERE UNIX_TIMESTAMP(MODTIME) > ? ORDER BY MODTIME DESC;", updateTime)
		if err != nil {
			return nil, newTime, err
		}
		defer rows.Close()
		firstRow := true
		for rows.Next() {
			var storeUid, storeUserType, storeModTime sql.NullInt64
			if err := rows.Scan(&storeUid, &storeUserType, &storeModTime); err != nil {
				this.infoLog.Printf("QueryIllegalUserAccountInDb rows.Scan failed")
				continue
			}

			var uid, userType uint32
			var modTime int64
			if storeUid.Valid {
				uid = uint32(storeUid.Int64)
			}

			if storeUserType.Valid {
				userType = uint32(storeUserType.Int64)
			}

			if storeModTime.Valid {
				modTime = storeModTime.Int64
			}
			iterm := StrUserType{
				UserId:   uid,
				UserType: userType,
			}
			uidSlice = append(uidSlice, iterm)
			if firstRow {
				newTime = uint64(modTime)
				firstRow = false
			}
		}
	}
	return uidSlice, newTime, nil
}

func (this *DbUtil) GetUserBlackListFromDb(reqUid uint32) (keySlic []uint32, scoreSlic []uint32, err error) {
	if this.db == nil {
		return nil, nil, ErrNilDbObject
	}

	rows, err := this.db.Query("SELECT BLACKID, UNIX_TIMESTAMP(TIME) FROM HT_BLACK_LIST WHERE USERID = ? AND FLAG = 1", reqUid)
	if err != nil {
		this.infoLog.Printf("GetUserBlackListFromDb mysql get black list failed err=%s", err)
		return nil, nil, err
	}
	defer rows.Close()
	var storeBlackId, storeTime sql.NullInt64
	for rows.Next() {
		if err := rows.Scan(&storeBlackId, &storeTime); err != nil {
			this.infoLog.Printf("GetUserBlackListFromDb mysql get black list reqUid=%v rows.Scan failed err=%s", reqUid, err)
			continue
		}
		var key, score uint32
		if storeBlackId.Valid {
			key = uint32(storeBlackId.Int64)
		}

		if storeTime.Valid {
			score = uint32(storeTime.Int64)
		}

		keySlic = append(keySlic, key)
		scoreSlic = append(scoreSlic, score)
	}
	return keySlic, scoreSlic, nil
}
