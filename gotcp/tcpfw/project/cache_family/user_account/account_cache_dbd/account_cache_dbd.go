package main

import (
	"database/sql"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_accountcache"
	"github.com/HT_GOGO/gotcp/tcpfw/project/cache_family/user_account/account_cache_dbd/util"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog             *log.Logger
	DbUtil              *util.DbUtil
	writeAgentMastreApi *common.SrvToSrvApiV2
	writeAgentSlaveApi  *common.SrvToSrvApiV2
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close() // 关闭连接
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close() // 关闭连接
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		c.Close() // 关闭连接
		return false
	}

	// 统计总的请求量
	attr := "goacdbd/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch head.Cmd {
	case uint32(ht_account_cache.CMD_TYPE_CMD_GET_USER_ACCOUNT_CACHE):
		go ProcGetUserAccountCache(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_RELOAD_USER_ACCOUNT_CACHE):
		go ProcRelodUserAccountCache(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_UPDATE_USER_PASSWD):
		go ProcUpdateUserPassWd(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_UPDATE_USER_EMAIL):
		go ProcUpdateUserEmail(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_UNREGISTER_USER_ACCOUNT):
		go ProcUnregisterUserAccount(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_GET_BLACK_LIST):
		go ProcGetUserBlackList(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_DEL_BLACK_LIST_IN_REDIS):
		go ProcDelBlackListInRedis(c, head, packet.GetBody())
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

// 1.proc store moment info
func ProcGetUserAccountCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserAccountCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacdbd/get_user_account_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserAccountCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserAccountCacheReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcGetUserAccountCache GetGetUserAccountCacheReqBody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	accountInfo, err := DbUtil.GetAccountInfoFromDb(head.Uid)
	if err != nil {
		infoLog.Printf("ProcGetUserAccountCache GetAccountInfoFromDb() failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_DB_ERR)
		rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("db error"),
			},
		}
		return false
	}

	infoLog.Printf("ProcGetUserAccountCache uid=%v userType=%v CNonce=%s Email=%s verify=%v regFrom=%v",
		head.Uid,
		accountInfo.UserType,
		accountInfo.CNonce,
		accountInfo.Email,
		accountInfo.Verify,
		accountInfo.RegFrom)

	rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		AccountCache: &ht_account_cache.AccountCache{
			UserId:     proto.Uint32(head.Uid),
			UserType:   proto.Uint32(accountInfo.UserType),
			Cnonce:     []byte(accountInfo.CNonce),
			PassWd:     []byte(accountInfo.PassWd),
			Email:      []byte(accountInfo.Email),
			Verify:     proto.Uint32(accountInfo.Verify),
			RegFrom:    proto.Uint32(accountInfo.RegFrom),
			RegTime:    proto.Uint64(uint64(accountInfo.RegTime)),
			ModifyTime: proto.Uint64(uint64(accountInfo.ModTime)),
		},
	}
	return true
}

func GetHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#account", uid)
	return hashName
}

func ProcRelodUserAccountCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcRelodUserAccountCache no need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcRelodUserAccountCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacdbd/reload_user_account_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcRelodUserAccountCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetReloadUserAccountCacheReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcRelodUserAccountCache GetReloadUserAccountCacheReqBody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	accountInfo, err := DbUtil.GetAccountInfoFromDb(head.Uid)
	if err != nil {
		infoLog.Printf("ProcRelodUserAccountCache GetAccountInfoFromDb() failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_DB_ERR)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("db error"),
			},
		}
		attr := "goacdbd/reload_account_cache_db_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	infoLog.Printf("ProcRelodUserAccountCache uid=%v uerType=%v CNonce=%s Email=%s verify=%v regFrom=%v",
		head.Uid,
		accountInfo.UserType,
		accountInfo.CNonce,
		accountInfo.Email,
		accountInfo.Verify,
		accountInfo.RegFrom)

	rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 notify write agent to update redis
	userAccountCache := &ht_account_cache.AccountCache{
		UserId:     proto.Uint32(head.Uid),
		UserType:   proto.Uint32(accountInfo.UserType),
		Cnonce:     []byte(accountInfo.CNonce),
		PassWd:     []byte(accountInfo.PassWd),
		Email:      []byte(accountInfo.Email),
		Verify:     proto.Uint32(accountInfo.Verify),
		RegFrom:    proto.Uint32(accountInfo.RegFrom),
		RegTime:    proto.Uint64(uint64(accountInfo.RegTime)),
		ModifyTime: proto.Uint64(uint64(accountInfo.ModTime)),
	}
	setReqBody := &ht_account_cache.AccountCacheReqBody{
		SetUserAccountCacheReqbody: &ht_account_cache.SetUserAccountCacheReqBody{
			AccountCache: userAccountCache,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcRelodUserAccountCache proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "goacdbd/get_user_account_cache_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_account_cache.CMD_TYPE_CMD_SET_USER_ACCOUNT_CACHE)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	return true
}

func SetRedisCache(head *common.HeadV2, reqPayLoad []byte) {
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}

	redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	if err == nil {
		redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisSlavePacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}
}

func ProcUpdateUserPassWd(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserPassWd no need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserPassWd invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacdbd/update_user_passwd_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPassWd proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserPassWdReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserPassWd GetUpdateUserPassWdReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	err = DbUtil.UpdateUserPassWdInDb(head.Uid, string(subReqBody.GetPassWd()))
	if err != nil {
		infoLog.Printf("ProcUpdateUserPassWd uid=%v passwd=%s UpdateUserPassWdInDb failed err=%s", head.Uid, subReqBody.GetPassWd(), err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_DB_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("update db error"),
			},
		}
		attr := "goacdbd/update_user_passwd_db_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 notify write agent to update redis
	setReqBody := &ht_account_cache.AccountCacheReqBody{
		SetUserPassWdReqbody: &ht_account_cache.SetUserPassWdReqBody{
			PassWd: subReqBody.GetPassWd(),
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPassWd  proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "goacdbd/update_user_passwd_cache_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false

	}
	head.Cmd = uint32(ht_account_cache.CMD_TYPE_CMD_SET_USER_PASSWD)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	return true
}

func ProcUpdateUserEmail(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserEmail no need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserEmail invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacdbd/update_user_email_cout"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserEmail proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserEmailReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserEmail GetUpdateUserEmailReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	err = DbUtil.UpdateUserEmailInDb(head.Uid, string(subReqBody.GetEmail()))
	if err != nil {
		infoLog.Printf("ProcUpdateUserEmail uid=%v emial=%s UpdateUserEmailInDb failed err=%s", head.Uid, subReqBody.GetEmail(), err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_DB_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("update db error"),
			},
		}
		attr := "goacdbd/update_user_email_db_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 update redis
	setReqBody := &ht_account_cache.AccountCacheReqBody{
		SetUserEmailReqbody: &ht_account_cache.SetUserEmailReqBody{
			Email: subReqBody.GetEmail(),
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserEmail proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "goacdbd/update_user_email_cache_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false

	}
	head.Cmd = uint32(ht_account_cache.CMD_TYPE_CMD_SET_USER_EMAIL)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	return true
}

func ProcUnregisterUserAccount(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUnregisterUserAccount no need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUnregisterUserAccount invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goacdbd/unregister_user_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUnregisterUserAccount proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUnregisterAccountReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUnregisterUserAccount GetUnregisterAccountReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	err = DbUtil.UnregisterAccountInDb(head.Uid, string(subReqBody.GetReplaceEmail()))
	if err != nil {
		infoLog.Printf("ProcUnregisterUserAccount uid=%v replacEmail=%s UnregisterAccountInDb failed err=%s", head.Uid, subReqBody.GetReplaceEmail(), err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_DB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("update db error"),
			},
		}
		attr := "goacdbd/unregister_user_db_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 notify write agent to update redis
	setReqBody := &ht_account_cache.AccountCacheReqBody{
		SetUnregisterAccountReqbody: &ht_account_cache.SetUnregisterAccountReqBody{},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcUnregisterUserAccount proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "goacdbd/unregister_user_cache_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false

	}
	head.Cmd = uint32(ht_account_cache.CMD_TYPE_CMD_SET_UNREGISTER_USER_ACCOUNT)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	return true
}

// 6.proc store moment info
func ProcGetUserBlackList(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserBlackList not need call")
		}
	}()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserBlackList invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "goacdbd/get_user_bl_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "goacdbd/get_user_bl"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserBlackList proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserBlackListReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserBlackList GetGetUserBlackListReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetUserId()
	infoLog.Printf("ProcGetUserBlackList reqUid=%v", reqUid)
	keys, scores, err := DbUtil.GetUserBlackListFromDb(reqUid)
	if err != nil {
		infoLog.Printf("ProcGetUserBlackList DbUtil.GetUserBlackListFromDb() failed uid=%v err=%s", reqUid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		BlackList: keys,
	}
	// Step1 answer first
	result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// Step2 update redis
	setReqBody := &ht_account_cache.AccountCacheReqBody{
		SetZsetListReqbody: &ht_account_cache.SetZsetListReqBody{
			ZsetName: proto.String(GetZsetNameName(reqUid)),
			Keys:     keys,
			Scores:   scores,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserBlackList proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "goacdbd/set_user_bl_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false

	}
	head.Cmd = uint32(ht_account_cache.CMD_TYPE_CMD_SET_ZSET_LIST)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	return true
}

func GetZsetNameName(uid uint32) (zsetName string) {
	zsetName = fmt.Sprintf("%v#blacklist", uid)
	return zsetName
}

func ProcDelBlackListInRedis(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcDelBlackListInRedis not need call")
		}
	}()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelBlackListInRedis invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "goacdbd/del_user_bl_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "goacdbd/del_user_bl"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelBlackListInRedis proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelBlakcListInRedisReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelBlackListInRedis GetDelBlakcListInRedisReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetUserId()
	infoLog.Printf("ProcDelBlackListInRedis reqUid=%v", reqUid)
	// Step5:拼成响应返回rsp
	rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
		Status: &ht_account_cache.AccountCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 update redis
	setReqBody := &ht_account_cache.AccountCacheReqBody{
		DelKeyInRedisReqbody: &ht_account_cache.DelKeyInRedisReqBody{
			DelKey: proto.String(GetZsetNameName(reqUid)),
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcDelBlackListInRedis proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "goacdbd/del_key_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false

	}
	head.Cmd = uint32(ht_account_cache.CMD_TYPE_CMD_DEL_KEY_IN_REDIS)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_account_cache.AccountCacheRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	// init dbUtil
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	DbUtil = util.NewDbUtil(db, infoLog)

	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%v port=%v", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%v port=%v", slaveIp, slavePort)
	writeAgentSlaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
