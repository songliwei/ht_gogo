package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_accountcache"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog             *log.Logger
	writeAgentMastreApi *common.SrvToSrvApiV2
	writeAgentslaveApi  *common.SrvToSrvApiV2
	dbdAgentApi         *common.SrvToSrvApiV2
	redisMasterApi      *common.RedisApi
)

const (
	ZRANG_KEY_START = 0
	ZRANG_KEY_STOP  = -1
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
)

const (
	UserTypeField   = "usertype"
	UserCNonceFiled = "cnonce"
	PassWdField     = "passwd"
	EmailField      = "email"
	VerifyField     = "verify"
	RegFromField    = "regfrom"
	RegTimeField    = "regtime"
	ModTimeField    = "modtime"
)

// alias to Util.BaseInfoStore
type AccountInfoStru struct {
	// HT_USER_ACCOUNT
	UserType uint32 //用户类别0-测试 1-内测 2-旧版本用户 3-新环境正式用户 4-后台删除 5-用户删除自己 6-客服 7-正式用户中的测试 8-推广用户
	CNonce   string //用户注册时候产生的随机NONCE
	PassWd   string //加密过的密码 = MD5(PWD+CNONCE)
	Email    string //注册邮箱
	Verify   uint32 //邮箱验证情况:0=未验证,1=已经发送,2=ses回执确认发送,3=ses回执确认失败,4=确认
	RegFrom  uint32 //0=unknown,1=email,2=weibo,3=tweet,4=facebook
	RegTime  uint64 //注册时间-readonly
	ModTime  uint64 //上一次密码或邮箱修改的时间
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close() // 关闭连接
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close() // 关闭连接
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	//	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close() // 关闭连接
		return false
	}

	// 统计总的请求量
	attr := "goaclogic/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_account_cache.CMD_TYPE_CMD_GET_USER_ACCOUNT_CACHE):
		go ProcGetUserAccountCache(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_RELOAD_USER_ACCOUNT_CACHE):
		go ProcRelodUserAccountCache(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_UPDATE_USER_PASSWD):
		go ProcUpdateUserPassWd(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_UPDATE_USER_EMAIL):
		go ProcUpdateUserEmail(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_UNREGISTER_USER_ACCOUNT):
		go ProcUnregisterAccount(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_GET_BLACK_LIST):
		go ProcGetUserBlackList(c, head, packet.GetBody())
	case uint32(ht_account_cache.CMD_TYPE_CMD_DEL_BLACK_LIST_IN_REDIS):
		go ProcDelBlackListInRedis(c, head, packet.GetBody())
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

// 1.proc store moment info
func ProcGetUserAccountCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserAccountCache not need call")
		}
	}()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserAccountCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "goaclogic/get_user_account_cache_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "goaclogic/get_user_account_cache_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserAccountCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserAccountCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserAccountCache GetGetUserAccountCacheReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := GetHashMapName(head.Uid)
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// Step1 存在则读取完成的记录
		var accountInfo AccountInfoStru
		keyValue, err := redisMasterApi.Hgetall(hashName)
		if err != nil {
			infoLog.Printf("ProcGetUserAccountCache redisMasterApi.Hgetall hashName=%s err=%s faield", hashName, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis exec error"),
				},
			}
			attr := "goaclogic/get_user_account_cache_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		for key, value := range keyValue {
			//infoLog.Printf("ProcGetUserAccountCache key=%s value=%s", key, value)
			switch key {
			case UserTypeField:
				tempUserType, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserAccountCache strconv.ParseUint userTyep=%s err=%s failed", value, err)
					continue
				}
				accountInfo.UserType = uint32(tempUserType)
			case UserCNonceFiled:
				accountInfo.CNonce = value
			case PassWdField:
				accountInfo.PassWd = value
			case EmailField:
				accountInfo.Email = value
			case VerifyField:
				tempVerify, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserAccountCache strconv.ParseUint verify=%s err=%s failed", value, err)
					continue
				}
				accountInfo.Verify = uint32(tempVerify)
			case RegFromField:
				tempRegFrom, err := strconv.ParseInt(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserAccountCache strconv.ParseUint regFrom=%s err=%s failed", value, err)
					continue
				}
				accountInfo.RegFrom = uint32(tempRegFrom)
			case RegTimeField:
				tempRegTime, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserAccountCache strconv.ParseUint regtime=%s err=%s failed", value, err)
					continue
				}
				accountInfo.RegTime = uint64(tempRegTime)
			case ModTimeField:
				tempModTime, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserAccountCache strconv.ParseUint modTime=%s err=%s failed", value, err)
					continue
				}
				accountInfo.ModTime = tempModTime
			}
		}

		//infoLog.Printf("ProcGetUserAccountCache uid=%v user_type=%v cnonce=%v passwd=%s email=%s verify=%v regFrom=%v regTime=%v modTime=%v",
		//		head.Uid,
		//		accountInfo.UserType,
		//		accountInfo.CNonce,
		//		accountInfo.PassWd,
		//		accountInfo.Email,
		//		accountInfo.Verify,
		//		accountInfo.RegFrom,
		//		accountInfo.RegTime,
		//		accountInfo.ModTime)

		rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			AccountCache: &ht_account_cache.AccountCache{
				UserId:     proto.Uint32(head.Uid),
				UserType:   proto.Uint32(accountInfo.UserType),
				Cnonce:     []byte(accountInfo.CNonce),
				PassWd:     []byte(accountInfo.PassWd),
				Email:      []byte(accountInfo.Email),
				Verify:     proto.Uint32(accountInfo.Verify),
				RegFrom:    proto.Uint32(accountInfo.RegFrom),
				RegTime:    proto.Uint64(accountInfo.RegTime),
				ModifyTime: proto.Uint64(accountInfo.ModTime),
			},
		}
		return true
	} else {
		infoLog.Printf("ProcGetUserAccountCache redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "goaclogic/get_user_account_cache_miss"
		libcomm.AttrAdd(attr, 1)
		// Step2 读取Master没有命中则继续读取dbd
		dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserAccountCache cache miss reload from db failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "goaclogic/get_account_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
		if !ok { // 不是HeadV2Packet报文
			infoLog.Printf("ProcGetUserAccountCache dbpacket can not change to HeadV2packet uid=%v", head.Uid)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "goaclogic/get_account_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbRespHead, err := dbPacketHeadV2.GetHead()
		if err != nil {
			//SendResp(c, head, uint16(ERR_INVALID_PARAM))
			infoLog.Printf("ProcGetUserAccountCache dbpacket Get head failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserAccountCacheRspbody = &ht_account_cache.GetUserAccountCacheRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "goaclogic/get_account_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		// Step3 读取dbd之后立即返回响应
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
		bNeedCall = false
		SendRspPacket(c, dbPacketHeadV2)
		// Step4 从dbd读取回结果后发送请求到cache_write_agent_master 和 cache_write_agent_slave
		reqPayLoad, err := ChangeGetAccountCacheRspToSetReq(dbRespHead, dbPacketHeadV2.GetBody())
		if err != nil {
			infoLog.Printf("ProcGetUserAccountCache ChangeGetAccountCacheRspToSetReq faild uid=%v err=%s", dbRespHead.Uid, err)
			// bNeedCall already false
			return false
		}
		head.Cmd = uint32(ht_account_cache.CMD_TYPE_CMD_SET_USER_ACCOUNT_CACHE)
		UpdateRedisCache(head, reqPayLoad)
	}
	return true
}

func GetHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#account", uid)
	return hashName
}

func ChangeGetAccountCacheRspToSetReq(rspHead *common.HeadV2, rspPayLoad []byte) (reqPayLoad []byte, err error) {
	if rspHead == nil || len(rspPayLoad) == 0 {
		infoLog.Printf("ChangeGetAccountCacheRspToSetReq rspPayLoad invalid param respHead=%#v rspPayLoad=%v", rspHead, rspPayLoad)
		err = ErrInvalidParam
		return reqPayLoad, err
	}
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	err = proto.Unmarshal(rspPayLoad, rspBody)
	if err != nil {
		infoLog.Printf("ChangeGetAccountCacheRspToSetReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", rspHead.Uid, rspHead.Cmd, rspHead.Seq)
		return reqPayLoad, err
	}
	subRspBody := rspBody.GetGetUserAccountCacheRspbody()
	if subRspBody == nil {
		infoLog.Printf("ChangeGetAccountCacheRspToSetReq GetGetUserAccountCacheRspbody() failed uid=%v", rspHead.Uid)
		err = ErrProtoBuff
		return reqPayLoad, err
	}

	if subRspBody.GetStatus().GetCode() != uint32(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ChangeGetAccountCacheRspToSetReq subRspBody.GetStatus().GetCode()=%v not success uid=%v", subRspBody.GetStatus().GetCode(), rspHead.Uid)
		err = ErrProtoBuff
		return reqPayLoad, err
	}
	reqBody := &ht_account_cache.AccountCacheReqBody{
		SetUserAccountCacheReqbody: &ht_account_cache.SetUserAccountCacheReqBody{
			AccountCache: subRspBody.GetAccountCache(),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v err=%s", rspHead.Uid, err)
	}
	return reqPayLoad, err
}

func UpdateRedisCache(head *common.HeadV2, reqPayLoad []byte) {
	if head == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("UpdateRedisCache input param error")
		return
	}
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("UpdateRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)
			} else {
				infoLog.Printf("UpdateRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)
			}
		} else {
			infoLog.Printf("UpdateRedisCache redisPacket can not change to HeadV2packet")
		}
	} else {
		infoLog.Printf("UpdateRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
	}

	redisPacket, err = writeAgentslaveApi.SendAndRecvPacket(head, reqPayLoad)
	if err == nil {
		redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisSlavePacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("UpdateRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)
			} else {
				infoLog.Printf("UpdateRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)
			}
		} else {
			infoLog.Printf("UpdateRedisCache redisPacket can not change to HeadV2packet")
		}
	} else {
		infoLog.Printf("UpdateRedisCache writeAgentSlaveApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
	}
	return
}

func ProcRelodUserAccountCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcRelodUserAccountCache not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcRelodUserAccountCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goaclogic/reload_user_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcRelodUserAccountCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetReloadUserAccountCacheReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcRelodUserAccountCache GetReloadUserAccountCacheReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcRelodUserAccountCache reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/reload_account_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcRelodUserAccountCache dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/reload_account_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcRelodUserAccountCache dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserAccountCacheRspbody = &ht_account_cache.ReloadUserAccountCacheRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/reload_account_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcRelodUserAccountCache dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

func ProcUpdateUserPassWd(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserPassWd not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserPassWd invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goaclogic/update_user_passwd_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPassWd proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserPassWdReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserPassWd GetUpdateUserPassWdReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPassWd reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/update_user_passwd_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserPassWd dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/update_user_passwd_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserPassWd dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPassWdRspbody = &ht_account_cache.UpdateUserPassWdRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/update_user_passwd_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserPassWd dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

func ProcUpdateUserEmail(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserEmail not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserEmail invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goaclogic/update_user_email_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserEmail proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserEmailReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserEmail GetGetUserBaseCacheReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserEmail reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/update_user_summertime_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserEmail dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/update_user_summertime_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserEmail dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserEmailRspbody = &ht_account_cache.UpdateUserEmailRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/update_user_summertime_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserEmail dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

func ProcUnregisterAccount(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUnregisterAccount not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUnregisterAccount invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "goaclogic/unregister_account_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUnregisterAccount proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUnregisterAccountReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUnregisterAccount GetUnregisterAccountReqbody() failed")
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUnregisterAccount update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/unregister_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUnregisterAccount dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/unregister_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUnregisterAccount dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UnregisterAccountRspbody = &ht_account_cache.UnregisterAccountRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/unregister_account_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUnregisterAccount dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 6.proc get user black list
func ProcGetUserBlackList(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserBlackList not need call")
		}
	}()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserBlackList invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "goaclogic/get_user_bl_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "goaclogic/get_user_bl"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserBlackList proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserBlackListReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserBlackList GetGetUserBlackListReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetUserId()
	infoLog.Printf("ProcGetUserBlackList reqUid=%v", reqUid)
	zsetName := GetZsetNameName(reqUid)
	exists, err := redisMasterApi.Exists(zsetName)
	if err == nil && exists == true {
		// Step1 存在则读取完成的记录
		keys, err := redisMasterApi.ZrangeWithOutScore(zsetName, int64(ZRANG_KEY_START), int64(ZRANG_KEY_STOP))
		if err != nil {
			infoLog.Printf("ProcGetUserBlackList redisMasterApi.ZrangeWithOutScore zsetName=%s err=%s faield", zsetName, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis exec error"),
				},
			}
			attr := "goaclogic/get_user_bl_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		infoLog.Printf("ProcGetUserBlackList uid=%v blackList=%v", reqUid, keys)
		var blackList []uint32
		for _, v := range keys {
			if v == "0" {
				infoLog.Printf("ProcGetUserBlackList uid=%v scripts version filed", reqUid)
				continue
			}
			value, err := strconv.ParseUint(v, 10, 64)
			if err != nil {
				infoLog.Printf("ProcGetUserBlackList uid=%v item=%s strconv.ParseUint err=%s", reqUid, v, err)
				continue
			}
			blackList = append(blackList, uint32(value))
		}
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
		rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			BlackList: blackList,
		}
		return true
	} else {
		infoLog.Printf("ProcGetUserBlackList redisMasterApi.Exists zsetName=%s return err=%s exists=%v",
			zsetName,
			err,
			exists)

		attr := "goaclogic/get_user_bl_cache_miss"
		libcomm.AttrAdd(attr, 1)
		// Step2 读取Master没有命中则继续读取dbd
		dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserBlackList cache miss reload from db failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "goaclogic/get_user_bl_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
		if !ok { // 不是HeadV2Packet报文
			infoLog.Printf("ProcGetUserBlackList dbpacket can not change to HeadV2packet uid=%v", head.Uid)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "goaclogic/get_user_bl_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbRespHead, err := dbPacketHeadV2.GetHead()
		if err != nil {
			//SendResp(c, head, uint16(ERR_INVALID_PARAM))
			infoLog.Printf("ProcGetUserBlackList dbpacket Get head failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBlackListRspbody = &ht_account_cache.GetUserBlackListRspBody{
				Status: &ht_account_cache.AccountCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "goaclogic/get_user_bl_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		infoLog.Printf("ProcGetUserBlackList reqUid=%v dbRespHeadRet=%v", reqUid, dbRespHead.Ret)
		// Step3 读取dbd之后立即返回响应
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
		bNeedCall = false
		SendRspPacket(c, dbPacketHeadV2)
	}
	return true
}

// 7.proc del user black list
func ProcDelBlackListInRedis(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_account_cache.AccountCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcDelBlackListInRedis not need call")
		}
	}()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelBlackListInRedis invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "goaclogic/del_user_bl_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "goaclogic/del_user_bl"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_account_cache.AccountCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelBlackListInRedis proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelBlakcListInRedisReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelBlackListInRedis GetDelBlakcListInRedisReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetUserId()
	infoLog.Printf("ProcDelBlackListInRedis reqUid=%v", reqUid)

	// Step2 读取Master没有命中则继续读取dbd
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcDelBlackListInRedis send to db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/del_user_bl_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcDelBlackListInRedis dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/del_user_bl_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcDelBlackListInRedis dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.DelBlakcListInRedisRspbody = &ht_account_cache.DelBlackListInRedisRspBody{
			Status: &ht_account_cache.AccountCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "goaclogic/del_user_bl_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcDelBlackListInRedis reqUid=%v dbRespHeadRet=%v", reqUid, dbRespHead.Ret)

	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

func GetZsetNameName(uid uint32) (zsetName string) {
	zsetName = fmt.Sprintf("%v#blacklist", uid)
	return zsetName
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_account_cache.AccountCacheRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%s port=%s", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%s port=%s", slaveIp, slavePort)
	writeAgentslaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustString("6379")
	infoLog.Printf("cache dbd ip=%v port=%v", dbdIp, dbdPort)
	dbdAgentApi = common.NewSrvToSrvApiV2(dbdIp, dbdPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
