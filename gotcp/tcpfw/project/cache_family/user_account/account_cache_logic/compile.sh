#!/bin/bash
CURDIR=`pwd`

go build $CURDIR/account_cache_logic.go

cp -f $CURDIR/account_cache_logic /home/ht/goproj/cache_family/user_account/account_cache_logic/bin/.

killall account_cache_logic
