package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_lesson_msg"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	redisMasterApi *common.RedisApi
	ssdbApi        *common.RedisApi
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrSsdbObj      = errors.New("err nil ssdb object")
)

const (
	KeyExpirePeriod  = 1209600      // 7 * 86400 = 30 day   units:second
	HashExpirePeriod = 259200       // 3 * 86400 = 3 day   units:second
	HashKeyMsgBase   = 100000000000 //每个人的群消息队列是1000亿条
	BatchGetCount    = 1000
)

const (
	HashKeyMaxMsgId = "0"
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "golmagent/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch uint32(head.Cmd) {
	case uint32(ht_lesson_msg.CMD_TYPE_CMD_UPDATE_HASH_MAP_REQ):
		go ProcUpdateHashMap(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
		// 无法处理的命令 关闭连接
		c.Close()
	}
	return true
}

func GetLessonHashMapName(roomId uint32, lessonObid []byte) (hashName string) {
	hashName = fmt.Sprintf("%v_%s#gmsg", roomId, lessonObid)
	return hashName
}

// 1.proc update lesson hash map
func ProcUpdateHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_lesson_msg.LessonMsgRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INVALID_PARAM)
		rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "golmagent/update_hashmap_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_lesson_msg.LessonMsgReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateHashMapReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateHashMap GetBatchSetHashMapReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	//获取参数
	hashName := subReqBody.GetHashName()
	keyValuesPair := subReqBody.GetKeyValuePair()
	if len(keyValuesPair) == 0 {
		infoLog.Println("ProcUpdateHashMap param error uid=%v hashLen=%v", head.Uid, len(keyValuesPair))
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
		rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
		}
		return false
	}
	// 先判断整个hashmap是否存在 不存在者加载全部 存在直接添加即可
	exists, err := redisMasterApi.Exists(hashName)
	if err != nil {
		attr := "golmagent/redis_exist_error"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcUpdateHashMap redisMasterApi.Exists hashName=%s keyValuesLen=%v not exist err=%s",
			hashName,
			len(keyValuesPair),
			err)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
		rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	if exists { // 存在
		var kvs []string
		for _, v := range keyValuesPair {
			// Step 1: 获取hashmap 相关参数
			kvs = append(kvs, v.GetKey())
			kvs = append(kvs, string(v.GetValue()))
		}
		// Step 2: 将key 和value 设置到redis 中
		err = redisMasterApi.Hmset(hashName, kvs)
		if err != nil {
			attr := "golmagent/hset_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcUpdateHashMap redisMasterApi.Hset failed hashName=%s kvsLen=%v err=%s",
				hashName,
				len(kvs),
				err)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
			rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	} else { // 不存在
		kvs, err := ReloadHashMap(hashName)
		if err != nil {
			attr := "golmagent/reload_hashmap_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcUpdateHashMap ReloadHashMap failed hashName=%s kvsLen=%v err=%s",
				hashName,
				len(kvs),
				err)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
			rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		// Step 2: 将key 和value 设置到redis 中
		err = redisMasterApi.Hmset(hashName, kvs)
		if err != nil {
			attr := "golmagent/hset_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcUpdateHashMap redisMasterApi.Hset failed hashName=%s kvsLen=%v err=%s",
				hashName,
				len(kvs),
				err)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
			rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}

	// 设置过期时间为30天
	err = redisMasterApi.Expire(hashName, HashExpirePeriod)
	if err != nil {
		// add static
		attr := "golmagent/set_key_expire_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcUpdateHashMap redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
	}

	result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	rspBody.UpdateHashMapRspbody = &ht_lesson_msg.UpdateHashMapRspBody{
		Status: &ht_lesson_msg.LessonMsgHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ReloadHashMap(hashName string) (keyValues []string, err error) {
	if ssdbApi == nil {
		err = ErrSsdbObj
		return nil, err
	}
	strMaxSeqId, err := ssdbApi.Hget(hashName, HashKeyMaxMsgId)
	if err != nil {
		return nil, err
	}
	maxSeq, err := strconv.ParseUint(strMaxSeqId, 10, 64)
	if err != nil {
		return nil, err
	}

	keyStart := uint64(HashKeyMsgBase + 1)
	keyEnd := maxSeq + HashKeyMsgBase
	for {
		var outKeys []string
		for index := 0; index < BatchGetCount && keyStart <= keyEnd; index += 1 {
			outKeys = append(outKeys, fmt.Sprintf("%v", keyStart))
			keyStart += 1
		}
		outValues, err := ssdbApi.Hmget(hashName, outKeys)
		if err != nil {
			infoLog.Printf("ReloadHashMapByCliSeq hashName=%s err=%s", hashName, err)
			return nil, err
		}
		if len(outKeys) != len(outValues) {
			infoLog.Printf("ReloadHashMapByCliSeq outKeysLen=%v outValuesLen=%v not equal", len(outKeys), len(outValues))
			err = ErrInvalidParam
			return nil, err
		}
		for i := 0; i < len(outKeys); i += 1 {
			keyValues = append(keyValues, outKeys[i])
			keyValues = append(keyValues, outValues[i])
		}

		if keyStart > keyEnd {
			infoLog.Printf("ReloadHashMapByCliSeq hashName=%s maxSeq=%v keyStart=%v keyEnd=%v",
				hashName,
				(maxSeq + HashKeyMsgBase),
				keyStart,
				keyEnd)
			break
		}
	}
	// add msg count
	keyValues = append(keyValues, HashKeyMaxMsgId)
	keyValues = append(keyValues, strMaxSeqId)
	return keyValues, nil
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_lesson_msg.LessonMsgRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// read ssdb config
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	// ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	// ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi = common.NewRedisApi(ssdbHost + ":" + strconv.Itoa(ssdbPort))

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
