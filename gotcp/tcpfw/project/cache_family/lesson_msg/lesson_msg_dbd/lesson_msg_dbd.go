package main

import (
	"fmt"
	"sync"

	"github.com/HT_GOGO/gotcp"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_lesson_msg"
	"github.com/HT_GOGO/gotcp/tcpfw/project/cache_family/lesson_msg/lesson_msg_dbd/util"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog             *log.Logger
	ssdbOperator        *util.SsdbOperator
	writeAgentMastreApi *common.SrvToSrvApiV2
	writeAgentSlaveApi  *common.SrvToSrvApiV2
	hashMapLock         sync.Mutex // sync mutex goroutines use reqRecord
)

const (
	ProcSlowThreshold = 300000
	HashMapSizeLimit  = 1000
	HashKeyMsgBase    = 100000000000 //每个人的群消息队列是1000亿条
	MsgCountThreshold = 100
	LatestMsgCount    = 50
)

const (
	HashKeyMaxMsgId = "0"
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v remoteAdd=%v", head.Cmd, head.Seq, head.Uid, c.GetExtraData())
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "golmdbd/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_lesson_msg.CMD_TYPE_CMD_STORE_LESSON_MESSAGE_REQ):
		go ProcStoreLessonMsg(c, head, packet)
	case uint32(ht_lesson_msg.CMD_TYPE_CMD_GET_LESSON_MSG_FROM_DB):
		go ProcGetLessonMsgFromDb(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

func GetLessonHashMapName(roomId uint32, lessonObid []byte) (hashName string) {
	hashName = fmt.Sprintf("%v_%s#gmsg", roomId, lessonObid)
	return hashName
}

// 1.proc store lesson message
func ProcStoreLessonMsg(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_lesson_msg.LessonMsgRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStoreLessonMsg no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INVALID_PARAM)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStoreLessonMsg invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "golmdbd/store_lesson_msg_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_lesson_msg.LessonMsgReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStoreLessonMsg proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStoreLessonMessageReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcStoreLessonMsg GetStoreLessonMessageReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcStoreLessonMsg uid=%v roomId=%v lessonObid=%s cmd=%v ",
		subReqBody.GetFromId(),
		subReqBody.GetRoomId(),
		subReqBody.GetLessonObid(),
		subReqBody.GetCmd())

	lessonMsg := &ht_lesson_msg.LessonMsgStore{
		FromId:     proto.Uint32(subReqBody.GetFromId()),
		RoomId:     proto.Uint32(subReqBody.GetRoomId()),
		LessonObid: subReqBody.GetLessonObid(),
		Cmd:        proto.Uint32(subReqBody.GetCmd()),
		Format:     proto.Uint32(subReqBody.GetFormat()),
		Content:    subReqBody.GetContent(),
		MsgTime:    proto.Uint64(uint64(time.Now().UnixNano() / 1000000)),
	}
	lessonMsgSlic, err := proto.Marshal(lessonMsg)
	if err != nil {
		infoLog.Printf("ProcStoreLessonMsg Failed to proto.Marshal uid=%v roomId=%v lessonObid=%s cmd=%v err=%s",
			subReqBody.GetFromId(),
			subReqBody.GetRoomId(),
			subReqBody.GetLessonObid(),
			subReqBody.GetCmd(),
			err)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	hashName := GetLessonHashMapName(subReqBody.GetRoomId(), subReqBody.GetLessonObid())
	key, err := ssdbOperator.Hinc(hashName, HashKeyMaxMsgId, 1)
	if err != nil {
		infoLog.Printf("ProcStoreLessonMsg ssdbOperator.Hinc failed uid=%v roomId=%v lessonObid=%s cmd=%v err=%s",
			subReqBody.GetFromId(),
			subReqBody.GetRoomId(),
			subReqBody.GetLessonObid(),
			subReqBody.GetCmd(),
			err)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	msgKey := fmt.Sprintf("%v", key+HashKeyMsgBase)
	err = ssdbOperator.HSet(hashName, msgKey, lessonMsgSlic)
	if err != nil {
		infoLog.Printf("ProcStoreLessonMsg ssdbOperator.HSet failed uid=%v roomId=%v lessonObid=%s cmd=%v err=%s",
			subReqBody.GetFromId(),
			subReqBody.GetRoomId(),
			subReqBody.GetLessonObid(),
			subReqBody.GetCmd(),
			err)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
		Status: &ht_lesson_msg.LessonMsgHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MsgIndex: []byte(msgKey),
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	infoLog.Printf("ProcStoreLessonMsg after SendRsp uid=%v roomId=%v cmd=%v msgKey=%s",
		subReqBody.GetFromId(),
		subReqBody.GetRoomId(),
		subReqBody.GetCmd(),
		msgKey)
	// Step2 notify write agent to update redis
	//双写更新Redis
	setReqBody := &ht_lesson_msg.LessonMsgReqBody{
		UpdateHashMapReqbody: &ht_lesson_msg.UpdateHashMapReqBody{
			HashName: proto.String(hashName),
			KeyValuePair: []*ht_lesson_msg.KeyValuePair{
				&ht_lesson_msg.KeyValuePair{
					Key:   proto.String(HashKeyMaxMsgId),
					Value: []byte(fmt.Sprintf("%v", key)),
				},
				&ht_lesson_msg.KeyValuePair{
					Key:   proto.String(msgKey),
					Value: lessonMsgSlic,
				},
			},
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcStoreLessonMsg proto.Marshal  SetKeyAndValueReqBody failed uid=%v err=%s", subReqBody.GetFromId(), err)
		attr := "golmdbd/store_lesson_msg_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_lesson_msg.CMD_TYPE_CMD_UPDATE_HASH_MAP_REQ)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "golmdbd/store_lesson_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcStoreLessonMsg after SetRedisCache uid=%v roomId=%v lessonObid=%s cmd=%v msgKey=%s",
		subReqBody.GetFromId(),
		subReqBody.GetRoomId(),
		subReqBody.GetLessonObid(),
		subReqBody.GetCmd(),
		msgKey)
	return true
}

// 2.proc get lesson message
func ProcGetLessonMsgFromDb(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_lesson_msg.LessonMsgRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetLessonMsgFromDb no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetLessonMsgFromDb invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "golmdbd/get_lesson_msg_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_lesson_msg.LessonMsgReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetLessonMsgFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetLessonMessageFromDbReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetLessonMsgFromDb GetGetLessonMessageFromDbRspbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := subReqBody.GetHashName()
	cliSeq := subReqBody.GetCliSeqId()
	reserve := subReqBody.GetReverse()
	if len(hashName) == 0 {
		infoLog.Printf("ProcGetLessonMsgFromDb param error failed uid=%v cmd=0x%4x seq=%v hashName=%s cliSeq=%v reserve=%v",
			head.Uid,
			head.Cmd,
			head.Seq,
			hashName,
			cliSeq,
			reserve)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	if reserve == 0 { //根据cliSeq 取最新的
		// Step1: get current mas msg id
		strMaxMsgId, err := ssdbOperator.HGet(string(hashName), HashKeyMaxMsgId)
		if err != nil {
			if err == redis.ErrNil {
				infoLog.Printf("ProcGetLessonMsgFromDb ssdbOperator.HGet empty set hashName=%s key=%s cliSeq=%v err=%s",
					hashName,
					HashKeyMaxMsgId,
					cliSeq,
					err)
				result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
				rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
					Status: &ht_lesson_msg.LessonMsgHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("get success"),
					},
					MsgSlic: nil,
					SeqId:   proto.Uint64(0), // 不存在hashmap时，直接返回0
					HasMore: proto.Uint32(0), // 1:还有群消息 0:没有新的群消息
				}
				return true
			} else {
				infoLog.Printf("ProcGetLessonMsgFromDb ssdbOperator.HGet failed uid=%v hashName=%s cliSeq=%v err=%s", head.Uid, hashName, cliSeq, err)
				result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
				rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
					Status: &ht_lesson_msg.LessonMsgHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("ssdb internal error"),
					},
				}
				return false
			}
		}
		if strMaxMsgId == "" {
			infoLog.Printf("ProcGetLessonMsgFromDb strMaxMsgId empty uid=%v hashName=%s cliSeq=%v", head.Uid, hashName, cliSeq)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb internal error"),
				},
			}
			return false
		}
		curMaxMsgId, err := strconv.ParseUint(strMaxMsgId, 10, 64)
		if err != nil {
			infoLog.Printf("ProcGetLessonMsgFromDb strconv.ParseUint failed uid=%v hashName=%s cliSeq=%v err=%s", head.Uid, hashName, cliSeq, err)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("strconv internal error"),
				},
			}
			return false
		}
		// 得到最大的seqid
		transMaxMsgId := curMaxMsgId + HashKeyMsgBase
		// Step2: cliSeqId greate than current max msg id return success
		if cliSeq >= transMaxMsgId {
			infoLog.Printf("ProcGetLessonMsgFromDb hashName=%s cliSeq=%v transMaxMsgId=%v equal or invalid", hashName, cliSeq, transMaxMsgId)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
			rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("get success"),
				},
				MsgSlic: nil,
				SeqId:   proto.Uint64(transMaxMsgId),
				HasMore: proto.Uint32(0), // 1:还有群消息 0:没有新的群消息
			}
			return true
		}
		var protoMaxSeqId uint64 = 0
		var protoHasMore uint32 = 0
		if cliSeq < HashKeyMsgBase {
			infoLog.Printf("ProcGetLessonMsgFromDb hashName=%s cliSeq=%v err get latest ten msg", hashName, cliSeq)
			if curMaxMsgId < LatestMsgCount {
				cliSeq = HashKeyMsgBase
			} else {
				cliSeq = transMaxMsgId - LatestMsgCount // 单用户使用0来取群消息时，返回最新的50条消息
			}
			protoMaxSeqId = transMaxMsgId
			protoHasMore = 0
		}
		endSeq := transMaxMsgId
		if cliSeq+MsgCountThreshold >= transMaxMsgId {
			endSeq = transMaxMsgId
			protoMaxSeqId = transMaxMsgId
			protoHasMore = 0 // no more message
		} else {
			endSeq = cliSeq + MsgCountThreshold
			protoMaxSeqId = endSeq
			protoHasMore = 1 // 1: has more message
		}
		// Step3: Hscan key-value data
		// keyStart := fmt.Sprintf("%v", cliSeq)
		// keyEnd := fmt.Sprintf("%v", endSeq)
		_, outValues, err := ssdbOperator.HscanArray(string(hashName), cliSeq, endSeq, MsgCountThreshold)
		if err != nil {
			infoLog.Printf("ProcGetLessonMsgFromDb ssdbOperator.HscanArray failed uid=%v cliSeq=%v endSeq=%v err=%s",
				head.Uid,
				cliSeq,
				endSeq,
				err)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("HscanArray internal error"),
				},
			}
			return false
		}
		// Step4:answer first
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
		rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			MsgSlic: outValues,
			SeqId:   proto.Uint64(protoMaxSeqId),
			HasMore: proto.Uint32(protoHasMore), // 1:还有群消息 0:没有新的群消息
		}
	} else { // 如果取历史消息时 cliSeq 必须 >= HashKeyMsgBase
		if cliSeq < HashKeyMsgBase {
			infoLog.Printf("ProcGetLessonMsgFromDb param error failed uid=%v cmd=0x%4x seq=%v hashName=%s cliSeq=%v reserve=%v",
				head.Uid,
				head.Cmd,
				head.Seq,
				hashName,
				cliSeq,
				reserve)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
			rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("success"),
				},
				MsgSlic: nil,
				SeqId:   proto.Uint64(HashKeyMsgBase),
				HasMore: proto.Uint32(0),
			}
			return false
		}
		var protoMinSeqId uint64 = 0
		var protoHasMore uint32 = 0
		var beginSeq uint64
		if cliSeq < HashKeyMsgBase+MsgCountThreshold {
			protoMinSeqId = HashKeyMsgBase
			protoHasMore = 0
			beginSeq = HashKeyMsgBase
		} else {
			protoMinSeqId = cliSeq - MsgCountThreshold
			beginSeq = protoMinSeqId
			protoHasMore = 1
		}

		_, outValues, err := ssdbOperator.HscanArray(string(hashName), beginSeq, cliSeq, MsgCountThreshold)
		if err != nil {
			infoLog.Printf("ProcGetLessonMsgFromDb ssdbOperator.HscanArray failed uid=%v hashName=%s beginSeq=%v cliSeq=%v err=%s",
				head.Uid,
				hashName,
				beginSeq,
				cliSeq,
				err)
			result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
				Status: &ht_lesson_msg.LessonMsgHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("HscanArray internal error"),
				},
			}
			return false
		}
		// Step4:answer first
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
		rspBody.GetLessonMessageFromDbRspbody = &ht_lesson_msg.GetLessonMessageFromDbRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			MsgSlic: outValues,
			SeqId:   proto.Uint64(protoMinSeqId),
			HasMore: proto.Uint32(protoHasMore), // 1:还有群消息 0:没有新的群消息
		}
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "golmdbd/get_lesson_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func SetRedisCache(head *common.HeadV2, reqPayLoad []byte) {
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}

	redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisSlavePacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentSlaveApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_lesson_msg.LessonMsgRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp cmd=%v ret=%v len=%v remoteAdd=%v", head.Cmd, ret, head.Len, c.GetExtraData())
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

// func startCPUProfile() {
// 	if cpuProfile != "" {
// 		f, err := os.Create(cpuProfile)
// 		if err != nil {
// 			infoLog.Printf("Can not create cpu profile output file: %s", err)
// 			return
// 		}
// 		if err := pprof.StartCPUProfile(f); err != nil {
// 			infoLog.Printf("Can not start cpu profile: %s", err)
// 			f.Close()
// 			return
// 		}
// 	}
// }

// func stopCPUProfile() {
// 	if cpuProfile != "" {
// 		pprof.StopCPUProfile() // 把记录的概要信息写到已指定的文件
// 	}
// }

// func startMemProfile() {
// 	if memProfile != "" && memProfileRate > 0 {
// 		runtime.MemProfileRate = memProfileRate
// 	}
// }

// func stopMemProfile() {
// 	if memProfile != "" {
// 		f, err := os.Create(memProfile)
// 		if err != nil {
// 			infoLog.Printf("Can not create mem profile output file: %s", err)
// 			return
// 		}
// 		if err = pprof.WriteHeapProfile(f); err != nil {
// 			infoLog.Printf("Can not write %s: %s", memProfile, err)
// 		}
// 		f.Close()
// 	}
// }

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// read profile config path
	// cpuProfile = cfg.Section("PROFILE").Key("cpu_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/cpu.prof")
	// memProfile = cfg.Section("PROFILE").Key("mem_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/mem.out")
	// memProfileRate = cfg.Section("PROFILE").Key("mem_rate").MustInt(512 * 1024)
	// read ssdb config
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	// ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	// ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	// ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	redisApi := common.NewRedisApi(fmt.Sprintf("%s:%v", ssdbHost, ssdbPort))
	// if err != nil {
	// 	infoLog.Printf("common.NewSsdbApi failed err=%s", err)
	// 	checkError(err)
	// 	return
	// }

	//	ssdbOperator = util.NewSsdbOperator(ssdbApi, infoLog)
	ssdbOperator = util.NewSsdbOperator(redisApi, infoLog)

	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%v port=%v", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%v port=%v", slaveIp, slavePort)
	writeAgentSlaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	//startCPUProfile()
	//startMemProfile()
	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	//stopCPUProfile()
	//stopMemProfile()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
