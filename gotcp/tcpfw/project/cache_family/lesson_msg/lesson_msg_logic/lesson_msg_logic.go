package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_lesson_msg"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	dbdAgentApi    *common.SrvToSrvApiV2
	redisMasterApi *common.RedisApi
	redisSlaveApi  *common.RedisApi
)

const (
	ProcSlowThreshold = 300000
	AdminUid          = 10000
	HashKeyMsgBase    = 100000000000 //每个人的群消息队列是1000亿条
	MsgCountThreshold = 100
	HashExpirePeriod  = 259200 // 3 * 86400 = 3  day   units:second
	LatestMsgCount    = 50
)

const (
	HashKeyMaxMsgId = "0"
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrInternalErr  = errors.New("internal err")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	// infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "golmlogic/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_lesson_msg.CMD_TYPE_CMD_STORE_LESSON_MESSAGE_REQ):
		go ProcStoreLessonMsg(c, head, packet)
	case uint32(ht_lesson_msg.CMD_TYPE_CMD_GET_LATEST_LESSON_MESSAGE_REQ):
		go ProcGetLatestLessonMsg(c, head, packet)
	case uint32(ht_lesson_msg.CMD_TYPE_CMD_GET_HISTORY_LESSON_MESSAGE_REQ):
		go ProcGetHistoryLessonMsg(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
		c.Close()
	}
	return true
}

// 1.proc store lesson msg
func ProcStoreLessonMsg(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_lesson_msg.LessonMsgRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcStoreLessonMsg not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INVALID_PARAM)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStoreLessonMsg invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "golmlogic/store_lesson_msg_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_lesson_msg.LessonMsgReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStoreLessonMsg proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStoreLessonMessageReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcStoreLessonMsg GetStoreLessonMessageReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcStoreLessonMsg update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "golmlogic/store_lesson_msg_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcStoreLessonMsg dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "golmlogic/store_lesson_msg_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcStoreLessonMsg dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StoreLessonMessageRspbody = &ht_lesson_msg.StoreLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "golmlogic/store_lesson_msg_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcStoreLessonMsg dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "golmlogic/store_lesson_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.proc get latest lesson msg from db
func ProcGetLatestLessonMsg(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_lesson_msg.LessonMsgRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetLatestLessonMsg not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetLatestLessonMessageRspbody = &ht_lesson_msg.GetLatestLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetLatestLessonMsg invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "golmlogic/get_latest_msg_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "golmlogic/get_latest_msg_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_lesson_msg.LessonMsgReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetLatestLessonMsg proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.GetLatestLessonMessageRspbody = &ht_lesson_msg.GetLatestLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetLatestLessonMessageReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetLatestLessonMsg GetGetLatestLessonMessageReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.GetLatestLessonMessageRspbody = &ht_lesson_msg.GetLatestLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	lessonObid := subReqBody.GetLessonObid()
	cliSeq := subReqBody.GetCliSeqId()
	infoLog.Printf("ProcGetLatestLessonMsg roomId=%v lessonObid=%s cliSeq=%v", roomId, lessonObid, cliSeq)
	msgs, maxSeqId, hasMore, err := GetLatestLessonMsgByCliSeq(head, roomId, lessonObid, cliSeq)
	if err != nil {
		infoLog.Printf("ProcGetLatestLessonMsg GetLatestLessonMsgByCliSeq() failed roomId=%v lessonObid=%s err=%s", roomId, lessonObid, err)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetLatestLessonMessageRspbody = &ht_lesson_msg.GetLatestLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get msg index internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcGetLatestLessonMsg roomId=%v lessonObid=%s cliSeq=%v maxSeqId=%v hasMore=%v", roomId, lessonObid, cliSeq, maxSeqId, hasMore)
	if len(msgs) == 0 {
		infoLog.Printf("ProcGetLatestLessonMsg GetLatestLessonMsgByCliSeq() msgIndex empty roomId=%v lessonObid=%s err=%s", roomId, lessonObid, err)
		rspBody.GetLatestLessonMessageRspbody = &ht_lesson_msg.GetLatestLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			MsgList:    nil,
			RoomId:     proto.Uint32(roomId),
			LessonObid: lessonObid,
			MaxSeqId:   proto.Uint64(maxSeqId),
			HasMore:    proto.Uint32(0),
		}
		return true
	}

	// 将消息反序列化拼rspbody
	var msgList []*ht_lesson_msg.LessonMsgStore
	for _, v := range msgs {
		if len(v) <= 0 {
			infoLog.Printf("ProcGetLatestLessonMsg msg empty=%s continue", v)
			continue
		}
		item := new(ht_lesson_msg.LessonMsgStore)
		err := proto.Unmarshal([]byte(v), item)
		if err != nil {
			infoLog.Printf("ProcGetLatestLessonMsg unmarshal msg failed roomId=%v lessonObid=%s err=%s", roomId, lessonObid, err)
			continue
		}
		msgList = append(msgList, item)
	}
	result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	var protoHasMore uint32 = 0
	if hasMore {
		protoHasMore = 1
	}
	rspBody.GetLatestLessonMessageRspbody = &ht_lesson_msg.GetLatestLessonMessageRspBody{
		Status: &ht_lesson_msg.LessonMsgHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MsgList:    msgList,
		RoomId:     proto.Uint32(roomId),
		LessonObid: lessonObid,
		MaxSeqId:   proto.Uint64(maxSeqId),
		HasMore:    proto.Uint32(protoHasMore),
	}
	infoLog.Printf("ProcGetLatestLessonMsg roomId=%v lessonObid=%s cliSeq=%v get msg count=%v", roomId, lessonObid, cliSeq, len(msgList))
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "golmlogic/get_latest_lesson_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetLatestLessonMsgByCliSeq(head *common.HeadV2, roomId uint32, lessonObid []byte, cliSeq uint64) (msgs []string, maxSeqId uint64, hasMore bool, err error) {
	if head == nil || roomId == 0 || len(lessonObid) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetLatestLessonMsgByCliSeq roomId=%v lessonObid=%s cliSeq=%v invalid param", roomId, lessonObid, cliSeq)
		return nil, maxSeqId, hasMore, err
	}
	hashName := GetLessonHashMapName(roomId, lessonObid)
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// Step1.get current max msg id
		maxMsgId, err := redisMasterApi.Hget(hashName, HashKeyMaxMsgId)
		if err != nil {
			infoLog.Printf("GetLatestLessonMsgByCliSeq hashName=%s Hget key=%s return err=%s", hashName, HashKeyMaxMsgId, err)
			return nil, maxSeqId, hasMore, err
		}
		// 设置过期时间为30天
		err = redisMasterApi.Expire(hashName, HashExpirePeriod)
		if err != nil {
			// add static
			attr := "golmlogic/set_key_expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("GetLatestLessonMsgByCliSeq redisMasterApi.Expire key=%s err=%s failed", hashName, err)
		}

		// Stpe2.ullID greate than current max msg id return empty
		curMaxId, err := strconv.ParseUint(maxMsgId, 10, 64)
		if err != nil {
			infoLog.Printf("GetLatestLessonMsgByCliSeq strconv.ParseUint() msgMaxId=%s err=%s", maxMsgId, err)
			return nil, maxSeqId, hasMore, err
		}
		transMaxId := curMaxId + HashKeyMsgBase
		if cliSeq > transMaxId {
			// add static
			attr := "golmlogic/cli_seq_over_flow"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("GetLatestLessonMsgByCliSeq over flow roomId=%v lessonObid=%s cliSeq=%v curMaxId=%v",
				roomId,
				lessonObid,
				cliSeq,
				transMaxId)
			// err = ErrInvalidParam
			// 重置maxSeq = transMaxId
			maxSeqId = transMaxId
			hasMore = false
			return nil, maxSeqId, hasMore, nil
		} else if cliSeq == transMaxId {
			infoLog.Printf("GetLatestLessonMsgByCliSeq no msg roomId=%v lessonObid=%s cliSeq=%v curMaxId=%v",
				roomId,
				lessonObid,
				cliSeq,
				transMaxId)
			// set maxSeqId and hasMore
			maxSeqId = cliSeq
			hasMore = false
			return nil, maxSeqId, hasMore, nil
		}
		// 如果客户端的序号小余HashKeyMsgBase 比如填0去最新的50条
		if cliSeq < HashKeyMsgBase {
			infoLog.Printf("GetLatestLessonMsgByCliSeq roomId=%v lessonObid=%s cliSeq=%v err get latest 50 msg", roomId, lessonObid, cliSeq)
			if curMaxId < LatestMsgCount {
				cliSeq = HashKeyMsgBase
			} else {
				cliSeq = transMaxId - LatestMsgCount // 单用户使用0来取课程消息时，返回最新的50条消息
			}
			maxSeqId = transMaxId
			hasMore = false
			infoLog.Printf("GetLatestLessonMsgByCliSeq roomId=%v lessonObid=%s cliSeq=%v maxSeqId=%v hasMore=%v",
				roomId,
				lessonObid,
				cliSeq,
				maxSeqId,
				hasMore)
		}
		// 3.prepare hmget keys
		endSeq := transMaxId
		if cliSeq+MsgCountThreshold >= transMaxId {
			endSeq = transMaxId
			maxSeqId = transMaxId
			hasMore = false
		} else {
			endSeq = cliSeq + MsgCountThreshold
			maxSeqId = endSeq
			hasMore = true
		}
		var keys []string
		for i := cliSeq + 1; i <= endSeq; i = i + 1 {
			item := fmt.Sprintf("%v", i)
			keys = append(keys, item)
		}
		infoLog.Printf("GetLatestLessonMsgByCliSeq roomId=%v lessonObid=%s cliSeq=%v keySize=%v keys=%v transMaxId=%v end=%v maxSeqId=%v",
			roomId,
			lessonObid,
			cliSeq,
			len(keys),
			keys,
			transMaxId,
			endSeq,
			maxSeqId)
		msgs, err := redisMasterApi.Hmget(hashName, keys)
		if err != nil {
			infoLog.Printf("GetLatestLessonMsgByCliSeq redisMasterApi.Hmget roomId=%v lessonObid=%s cliSeq=%v keys=%v err=%s",
				roomId,
				lessonObid,
				cliSeq,
				keys,
				err)
			return nil, maxSeqId, hasMore, err
		}
		return msgs, maxSeqId, hasMore, err
	} else {
		infoLog.Printf("GetLatestLessonMsgByCliSeq redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "golmlogic/get_msg_index_cache_miss"
		libcomm.AttrAdd(attr, 1)
		msgs, maxSeqId, hasMore, err = GetLatestLessonMsgByCliSeqFromDbd(head, hashName, cliSeq)
		if err != nil {
			infoLog.Printf("GetLatestLessonMsgByCliSeq roomId=%v lessonObid=%s cliSeq=%v hashName=%s err=%s", roomId, lessonObid, cliSeq, hashName, err)
			return nil, maxSeqId, hasMore, err
		}
		return msgs, maxSeqId, hasMore, nil
	}
}

func GetLatestLessonMsgByCliSeqFromDbd(head *common.HeadV2, hashName string, cliSeq uint64) (msgs []string, maxSeqId uint64, hasMore bool, err error) {
	if head == nil || len(hashName) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetLatestLessonMsgByCliSeqFromDbd hashName=%s cliSeq=%v invalid param", hashName, cliSeq)
		return nil, maxSeqId, hasMore, err
	}
	// Step2 读取Master没有命中则继续读取dbd
	reqhead := new(common.HeadV2)
	if head != nil {
		*reqhead = *head
	}
	reqhead.Cmd = uint32(ht_lesson_msg.CMD_TYPE_CMD_GET_LESSON_MSG_FROM_DB)
	reqBody := &ht_lesson_msg.LessonMsgReqBody{
		GetLessonMessageFromDbReqbody: &ht_lesson_msg.GetLessonMessageFromDbReqBody{
			HashName: []byte(hashName),
			CliSeqId: proto.Uint64(cliSeq),
			Reverse:  proto.Uint32(0), // 0: 取最新 1: 取历史
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("GetLatestLessonMsgByCliSeqFromDbd proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "golmlogic/get_msg_dbd_proto_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqhead, reqPayLoad)
	if err != nil {
		infoLog.Printf("GetLatestLessonMsgByCliSeqFromDbd cache miss reload from db failed uid=%v err=%s", head.Uid, err)
		attr := "golmlogic/get_msg_dbd_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("GetLatestLessonMsgByCliSeqFromDbd dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		attr := "golmlogic/get_msg_dbd_inter_err"
		libcomm.AttrAdd(attr, 1)
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("GetLatestLessonMsgByCliSeqFromDbd dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		attr := "golmlogic/get_msg_dbd_inter_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}
	if dbRespHead.Ret != uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("GetLatestLessonMsgByCliSeqFromDbd dbpaket ret=%v uid=%v", dbRespHead.Ret, head.Uid)
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}

	rspBody := &ht_lesson_msg.LessonMsgRspBody{}
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), rspBody)
	if err != nil {
		return nil, maxSeqId, hasMore, err
	}
	subRspBody := rspBody.GetGetLessonMessageFromDbRspbody()
	if subRspBody == nil {
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}
	msgs = subRspBody.GetMsgSlic()
	maxSeqId = subRspBody.GetSeqId()
	protoHasMore := subRspBody.GetHasMore()
	if protoHasMore > 0 {
		hasMore = true
	} else {
		hasMore = false
	}

	return msgs, maxSeqId, hasMore, nil
}

// 3.proc get history lesson msg from db
func ProcGetHistoryLessonMsg(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_lesson_msg.LessonMsgRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetHistoryLessonMsg not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INVALID_PARAM)
		rspBody.GetHistoryLessonMessageRspbody = &ht_lesson_msg.GetHistoryLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetHistoryLessonMsg invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "golmlogic/get_history_msg_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "golmlogic/get_history_msg_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_lesson_msg.LessonMsgReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetHistoryLessonMsg proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.GetHistoryLessonMessageRspbody = &ht_lesson_msg.GetHistoryLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetHistoryLessonMessageReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetHistoryLessonMsg GetGetHistoryLessonMessageReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_PB_ERR)
		rspBody.GetHistoryLessonMessageRspbody = &ht_lesson_msg.GetHistoryLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	lessonObid := subReqBody.GetLessonObid()
	cliSeq := subReqBody.GetCliSeqId()
	infoLog.Printf("ProcGetHistoryLessonMsg roomId=%v lessonObid=%s cliSeq=%v", roomId, lessonObid, cliSeq)
	msgs, maxSeqId, hasMore, err := GetHistoryLessonMsgByCliSeq(head, roomId, lessonObid, cliSeq)
	if err != nil {
		infoLog.Printf("ProcGetHistoryLessonMsg GetLatestLessonMsgByCliSeq() failed roomId=%v lessonObid=%s err=%s", roomId, lessonObid, err)
		result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetHistoryLessonMessageRspbody = &ht_lesson_msg.GetHistoryLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get msg index internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcGetHistoryLessonMsg roomId=%v lessonObid=%s cliSeq=%v maxSeqId=%v hasMore=%v", roomId, lessonObid, cliSeq, maxSeqId, hasMore)
	if len(msgs) == 0 {
		infoLog.Printf("ProcGetLatestLessonMsg GetLatestLessonMsgByCliSeq() msgIndex empty roomId=%v lessonObid=%s err=%s", roomId, lessonObid, err)
		rspBody.GetHistoryLessonMessageRspbody = &ht_lesson_msg.GetHistoryLessonMessageRspBody{
			Status: &ht_lesson_msg.LessonMsgHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			MsgList:    nil,
			RoomId:     proto.Uint32(roomId),
			LessonObid: lessonObid,
			MinSeqId:   proto.Uint64(maxSeqId),
			HasMore:    proto.Uint32(0),
		}
		return true
	}

	// 将消息反序列化拼rspbody
	var msgList []*ht_lesson_msg.LessonMsgStore
	for _, v := range msgs {
		if len(v) <= 0 {
			infoLog.Printf("ProcGetHistoryLessonMsg msg empty=%s continue", v)
			continue
		}
		item := new(ht_lesson_msg.LessonMsgStore)
		err := proto.Unmarshal([]byte(v), item)
		if err != nil {
			infoLog.Printf("ProcGetHistoryLessonMsg unmarshal msg failed roomId=%v lessonObid=%s err=%s", roomId, lessonObid, err)
			continue
		}
		msgList = append(msgList, item)
	}
	result = uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS)
	var protoHasMore uint32 = 0
	if hasMore {
		protoHasMore = 1
	}
	rspBody.GetHistoryLessonMessageRspbody = &ht_lesson_msg.GetHistoryLessonMessageRspBody{
		Status: &ht_lesson_msg.LessonMsgHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MsgList:    msgList,
		RoomId:     proto.Uint32(roomId),
		LessonObid: lessonObid,
		MinSeqId:   proto.Uint64(maxSeqId),
		HasMore:    proto.Uint32(protoHasMore),
	}
	infoLog.Printf("ProcGetHistoryLessonMsg roomId=%v lessonObid=%s cliSeq=%v get msg count=%v", roomId, lessonObid, cliSeq, len(msgList))
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "golmlogic/get_latest_lesson_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetHistoryLessonMsgByCliSeq(head *common.HeadV2, roomId uint32, lessonObid []byte, cliSeq uint64) (msgs []string, maxSeqId uint64, hasMore bool, err error) {
	if head == nil || roomId == 0 || len(lessonObid) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetHistoryLessonMsgByCliSeq roomId=%v lessonObid=%s cliSeq=%v invalid param", roomId, lessonObid, cliSeq)
		return nil, maxSeqId, hasMore, err
	}
	hashName := GetLessonHashMapName(roomId, lessonObid)
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// 设置过期时间为30天
		err = redisMasterApi.Expire(hashName, HashExpirePeriod)
		if err != nil {
			// add static
			attr := "golmlogic/set_key_expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("GetHistoryLessonMsgByCliSeq redisMasterApi.Expire key=%s err=%s failed", hashName, err)
		}

		// Stpe1.ullID greate than current max msg id return empty
		if cliSeq < HashKeyMsgBase {
			// add static
			attr := "golmlogic/cli_seq_over_flow"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("GetHistoryLessonMsgByCliSeq over flow roomId=%v lessonObid=%s cliSeq=%v",
				roomId,
				lessonObid,
				cliSeq)
			// err = ErrInvalidParam
			// 重置maxSeq = transMaxId
			maxSeqId = HashKeyMsgBase
			hasMore = false
			return nil, maxSeqId, hasMore, nil
		} else if cliSeq < HashKeyMsgBase+MsgCountThreshold {
			infoLog.Printf("GetHistoryLessonMsgByCliSeq remain message less than MsgCountThreshold=%v cliSeq=%v hashName=%s",
				MsgCountThreshold,
				cliSeq,
				hashName)
			// set maxSeqId and hasMore
			maxSeqId = HashKeyMsgBase
			hasMore = false
		} else {
			infoLog.Printf("GetHistoryLessonMsgByCliSeq remain message more than MsgCountThreshold=%v cliSeq=%v hashName=%s",
				MsgCountThreshold,
				cliSeq,
				hashName)
			maxSeqId = cliSeq - MsgCountThreshold
			hasMore = true
		}
		var keys []string
		for i := maxSeqId + 1; i < cliSeq; i = i + 1 {
			item := fmt.Sprintf("%v", i)
			keys = append(keys, item)
		}
		infoLog.Printf("GetHistoryLessonMsgByCliSeq roomId=%v lessonObid=%s cliSeq=%v keySize=%v keys=%v maxSeqId=%v",
			roomId,
			lessonObid,
			cliSeq,
			len(keys),
			keys,
			maxSeqId)
		msgs, err := redisMasterApi.Hmget(hashName, keys)
		if err != nil {
			infoLog.Printf("GetHistoryLessonMsgByCliSeq redisMasterApi.Hmget roomId=%v lessonObid=%s cliSeq=%v keys=%v err=%s",
				roomId,
				lessonObid,
				cliSeq,
				keys,
				err)
			return nil, maxSeqId, hasMore, err
		}
		return msgs, maxSeqId, hasMore, err
	} else {
		infoLog.Printf("GetHistoryLessonMsgByCliSeq redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "golmlogic/get_lesson_msg_cache_miss"
		libcomm.AttrAdd(attr, 1)
		msgs, maxSeqId, hasMore, err = GetHistoryLessonMsgByCliSeqFromDbd(head, hashName, cliSeq)
		if err != nil {
			infoLog.Printf("GetHistoryLessonMsgByCliSeq roomId=%v lessonObid=%s cliSeq=%v hashName=%s err=%s", roomId, lessonObid, cliSeq, hashName, err)
			return nil, maxSeqId, hasMore, err
		}
		return msgs, maxSeqId, hasMore, nil
	}
}

func GetHistoryLessonMsgByCliSeqFromDbd(head *common.HeadV2, hashName string, cliSeq uint64) (msgs []string, maxSeqId uint64, hasMore bool, err error) {
	if head == nil || len(hashName) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetHistoryLessonMsgByCliSeqFromDbd hashName=%s cliSeq=%v invalid param", hashName, cliSeq)
		return nil, maxSeqId, hasMore, err
	}
	// Step2 读取Master没有命中则继续读取dbd
	reqhead := new(common.HeadV2)
	if head != nil {
		*reqhead = *head
	}
	reqhead.Cmd = uint32(ht_lesson_msg.CMD_TYPE_CMD_GET_LESSON_MSG_FROM_DB)
	reqBody := &ht_lesson_msg.LessonMsgReqBody{
		GetLessonMessageFromDbReqbody: &ht_lesson_msg.GetLessonMessageFromDbReqBody{
			HashName: []byte(hashName),
			CliSeqId: proto.Uint64(cliSeq),
			Reverse:  proto.Uint32(1), // 0: 取最新 1: 取历史
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("GetHistoryLessonMsgByCliSeqFromDbd proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "golmlogic/get_msg_dbd_proto_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqhead, reqPayLoad)
	if err != nil {
		infoLog.Printf("GetHistoryLessonMsgByCliSeqFromDbd cache miss reload from db failed uid=%v err=%s", head.Uid, err)
		attr := "golmlogic/get_msg_dbd_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("GetHistoryLessonMsgByCliSeqFromDbd dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		attr := "golmlogic/get_msg_dbd_inter_err"
		libcomm.AttrAdd(attr, 1)
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("GetHistoryLessonMsgByCliSeqFromDbd dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		attr := "golmlogic/get_msg_dbd_inter_err"
		libcomm.AttrAdd(attr, 1)
		return nil, maxSeqId, hasMore, err
	}
	if dbRespHead.Ret != uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("GetHistoryLessonMsgByCliSeqFromDbd dbpaket ret=%v uid=%v", dbRespHead.Ret, head.Uid)
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}

	rspBody := &ht_lesson_msg.LessonMsgRspBody{}
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), rspBody)
	if err != nil {
		return nil, maxSeqId, hasMore, err
	}
	subRspBody := rspBody.GetGetLessonMessageFromDbRspbody()
	if subRspBody == nil {
		err = ErrInternalErr
		return nil, maxSeqId, hasMore, err
	}
	msgs = subRspBody.GetMsgSlic()
	maxSeqId = subRspBody.GetSeqId()
	protoHasMore := subRspBody.GetHasMore()
	if protoHasMore > 0 {
		hasMore = true
	} else {
		hasMore = false
	}

	return msgs, maxSeqId, hasMore, nil
}

func GetLessonHashMapName(roomId uint32, lessonObid []byte) (hashName string) {
	hashName = fmt.Sprintf("%v_%s#gmsg", roomId, lessonObid)
	return hashName
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_lesson_msg.LessonMsgRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	redisSlaveIp := cfg.Section("REDISSLAVE").Key("redis_ip").MustString("127.0.0.1")
	redisSlavePort := cfg.Section("REDISSLAVE").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis slave ip=%v port=%v", redisSlaveIp, redisSlavePort)
	redisSlaveApi = common.NewRedisApi(redisSlaveIp + ":" + strconv.Itoa(redisSlavePort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustString("6379")
	infoLog.Printf("cache dbd ip=%v port=%v", dbdIp, dbdPort)
	dbdAgentApi = common.NewSrvToSrvApiV2(dbdIp, dbdPort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
