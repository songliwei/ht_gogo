package main

import (
	"errors"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_accountcache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_basecache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_user"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog         *log.Logger
	baseCacheApi    *common.SrvToSrvApiV2
	accountCacheApi *common.SrvToSrvApiV2
)

var (
	ErrInputParam         = errors.New("err param error")
	ErrSendPacketFailed   = errors.New("err send packet failed")
	ErrChangePacketFailed = errors.New("err change packet failed")
	ErrGetHeadFailed      = errors.New("err get packet head failed")
	ErrCacheReturnErr     = errors.New("err cache return err")
	ErrDuplicate          = errors.New("duplicate param")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close() // 关闭连接
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close() // 关闭连接
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		c.Close() // 关闭连接
		return false
	}

	// 统计总的请求量
	attr := "gouseradapter/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch head.Cmd {
	case uint32(ht_user.ProtoCmd_CMD_GET_USERINFO):
		go ProcGetUserInfo(c, head, packet.GetBody())
	case uint32(ht_user.ProtoCmd_CMD_MOD_USERINFO):
		go ProcModUserInfo(c, head, packet.GetBody())
	case uint32(ht_user.ProtoCmd_CMD_GET_BLACKLIST):
		go ProcGetBlackList(c, head, packet.GetBody())
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
	}
	return true
}

// 1.proc get user cache
func ProcGetUserInfo(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_user.ResultCode_RET_SUCCESS)
	rspBody := new(ht_user.RspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_user.ResultCode_RET_CACHE_ERR)
		rspBody = &ht_user.RspBody{
			Error: &ht_user.ErrorInfoBody{
				Code: proto.Uint32(uint32(result)),
				Msg:  []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserInfo invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gouseradapter/get_user_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_user.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_user.ResultCode_RET_INTERNAL_ERR)
		rspBody = &ht_user.RspBody{
			Error: &ht_user.ErrorInfoBody{
				Code: proto.Uint32(uint32(result)),
				Msg:  []byte("proto unmarshal failed"),
			},
		}
		return false
	}

	uidList := reqBody.GetUser()
	var itermList []*ht_user.UserInfoBody
	reqHead := new(common.HeadV2)
	*reqHead = *head
	for _, v := range uidList {
		// STEP1 Update account cache
		reqPayLoad, err := ChangeToGetUserBlackListReq(head, v.GetUserID())
		if err != nil {
			infoLog.Printf("ProcGetUserInfo ChangeToGetUserInfoReq() failed uid=%v err=%s", v, err)
			continue
		}

		reqHead.Uid = v.GetUserID() // set the get uid
		accountRspPacket, err := SendAndRecvPacketFromAccountCache(reqHead, ht_account_cache.CMD_TYPE_CMD_GET_BLACK_LIST, reqPayLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserInfo SendAndRecvPacketFromBaseCache() failed uid=%v err=%s", v, err)
			attr := "gouseradapter/query_account_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		accountSubRspPacket := accountRspPacket.GetGetUserBlackListRspbody()
		if accountSubRspPacket == nil {
			infoLog.Printf("ProcGetUserInfo accountRspPacket.GetUserBlackListRspBody failed")
			attr := "gouseradapter/query_account_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		blackList := accountSubRspPacket.GetBlackList()
		// STEP2 Update base cache
		reqPayLoad, err = ChangeToGetUserBaseCacheReq(head, v.GetUserID())
		if err != nil {
			infoLog.Printf("ProcGetUserInfo ChangeToGetUserBaseCacheReq() failed uid=%v err=%s", v, err)
			continue
		}
		baseRspPacket, err := SendAndRecvPacketFromBaseCache(reqHead, ht_base_cache.CMD_TYPE_CMD_GET_USER_BASE_CACHE, reqPayLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserInfo SendAndRecvPacketFromBaseCache() failed uid=%v err=%s", v, err)
			attr := "gouseradapter/query_base_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		baseSubRspPacket := baseRspPacket.GetGetUserBaseCacheRspbody()
		if baseSubRspPacket == nil {
			infoLog.Printf("ProcGetUserInfo baseRspPacket.GetGetUserCacheRspbody failed")
			attr := "gouseradapter/query_base_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		baseCache := baseSubRspPacket.GetBaseCache()

		// 实时计算生日
		birthDay := CalcBirthday(baseCache.GetBirthday())
		iterm := &ht_user.UserInfoBody{
			UserID:       proto.Uint32(v.GetUserID()),
			UserName:     []byte(baseCache.GetUserName()),
			Sex:          proto.Uint32(baseCache.GetSex()),
			Birthday:     proto.Uint64(birthDay),
			National:     []byte(baseCache.GetNationality()),
			NativeLang:   proto.Uint32(baseCache.GetNativeLang()),
			LearnLang1:   proto.Uint32(baseCache.GetLearnLang1()),
			LearnLang2:   proto.Uint32(baseCache.GetLearnLang2()),
			LearnLang3:   proto.Uint32(baseCache.GetLearnLang3()),
			TeachLang2:   proto.Uint32(baseCache.GetTeachLang2()),
			TeachLang3:   proto.Uint32(baseCache.GetTeachLang3()),
			BlackidList:  []uint32(blackList),
			FriendidList: nil,
			FansList:     nil,
			Nickname:     []byte(baseCache.GetNickName()),
			VipExpireTs:  proto.Uint32(0),
		}
		itermList = append(itermList, iterm)
	}
	// return success
	result = uint16(ht_user.ResultCode_RET_SUCCESS)
	rspBody = &ht_user.RspBody{
		Error: &ht_user.ErrorInfoBody{
			Code: proto.Uint32(uint32(result)),
			Msg:  []byte("success"),
		},
		User: itermList,
	}
	return true
}

func ChangeToGetUserBaseCacheReq(reqHead *common.HeadV2, reqUid uint32) (reqPayLoad []byte, err error) {
	if reqHead == nil || reqUid == 0 {
		infoLog.Printf("ChangeToGetUserBaseCacheReq invalid param reqHead=%#v reqUid=%v", reqHead, reqUid)
		err = ErrInputParam
		return nil, err
	}
	reqBody := &ht_base_cache.BaseCacheReqBody{
		GetUserBaseCacheReqbody: &ht_base_cache.GetUserBaseCacheReqBody{},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToGetUserBaseCacheReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

func ChangeToGetUserBlackListReq(reqHead *common.HeadV2, reqUid uint32) (reqPayLoad []byte, err error) {
	if reqHead == nil || reqUid == 0 {
		infoLog.Printf("ChangeToGetUserBlackListReq invalid param reqHead=%#v reqUid=%v", reqHead, reqUid)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_account_cache.AccountCacheReqBody{
		GetUserBlackListReqbody: &ht_account_cache.GetUserBlackListReqBody{
			UserId: proto.Uint32(reqUid),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToGetUserBlackListReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

func CalcBirthday(strBirthday string) (birthDay uint64) {
	if strBirthday == "" {
		infoLog.Printf("CalcBirthday strBirtyDay empty")
		birthDay = 0
		return birthDay
	}

	yearMonthDay := strings.Split(strBirthday, "-")
	if len(yearMonthDay) < 3 {
		infoLog.Printf("CalcBirthday strBirtyDay=%s fromt error", strBirthday)
		birthDay = 0
		return birthDay
	}

	birYear, err := strconv.ParseUint(yearMonthDay[0], 10, 64)
	if err != nil {
		infoLog.Printf("CalcBirthday strBirtyDay=%s strconv.ParseUint err=%s", strBirthday, err)
		birthDay = 0
		return birthDay
	}

	birMonth, err := strconv.ParseUint(yearMonthDay[1], 10, 64)
	if err != nil {
		infoLog.Printf("CalcBirthday strBirtyDay=%s strconv.ParseUint err=%s", strBirthday, err)
		birthDay = 0
		return birthDay
	}

	timeNow := time.Now()
	birthDay = uint64(timeNow.Year()) - birYear

	if uint64(timeNow.Month()) < birMonth {
		if birthDay > 0 {
			birthDay -= 1
		}
	}
	infoLog.Printf("CalcBirthday strBirtyDay=%s calc birthDay=%v", strBirthday, birthDay)
	return birthDay
}

// 2.proc mod user cache
func ProcModUserInfo(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_user.ResultCode_RET_SUCCESS)
	rspBody := new(ht_user.RspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_user.ResultCode_RET_CACHE_ERR)
		rspBody = &ht_user.RspBody{
			Error: &ht_user.ErrorInfoBody{
				Code: proto.Uint32(uint32(result)),
				Msg:  []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModUserInfo invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}
	// add static
	attr := "gouseradapter/mod_user_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_user.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModUserInfo proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_user.ResultCode_RET_INTERNAL_ERR)
		rspBody = &ht_user.RspBody{
			Error: &ht_user.ErrorInfoBody{
				Code: proto.Uint32(uint32(result)),
				Msg:  []byte("proto unmarshal failed"),
			},
		}
		return false
	}

	uidList := reqBody.GetUser()
	reqHead := new(common.HeadV2)
	*reqHead = *head
	for _, v := range uidList {
		// STEP1 Update account cache
		reqPayLoad, err := ChangeToClearUserBlackListReq(head, v.GetUserID())
		if err != nil {
			infoLog.Printf("ProcModUserInfo ChangeToGetUserInfoReq() failed uid=%v err=%s", v, err)
			continue
		}

		reqHead.Uid = v.GetUserID() // set the get uid
		accountRspPacket, err := SendAndRecvPacketFromAccountCache(reqHead, ht_account_cache.CMD_TYPE_CMD_DEL_BLACK_LIST_IN_REDIS, reqPayLoad)
		if err != nil {
			infoLog.Printf("ProcModUserInfo SendAndRecvPacketFromBaseCache() failed uid=%v err=%s", v, err)
			attr := "gouseradapter/query_account_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		accountSubRspPacket := accountRspPacket.GetDelBlakcListInRedisRspbody()
		if accountSubRspPacket == nil {
			infoLog.Printf("ProcModUserInfo accountRspPacket.GetDelBlakcListInRedisReqbody failed")
			attr := "gouseradapter/query_account_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		infoLog.Printf("ProcModUserInfo account code=%v status=%s", accountSubRspPacket.GetStatus().GetCode(), accountSubRspPacket.GetStatus().GetReason())

		// STEP2 Update base cache
		reqPayLoad, err = ChangeToClearUserBaseCacheReq(head, v.GetUserID())
		if err != nil {
			infoLog.Printf("ProcGetUserInfo ChangeToClearUserBaseCacheReq() failed uid=%v err=%s", v, err)
			continue
		}
		baseRspPacket, err := SendAndRecvPacketFromBaseCache(reqHead, ht_base_cache.CMD_TYPE_CMD_DEL_USER_BASE_CACHE, reqPayLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserInfo SendAndRecvPacketFromBaseCache() failed uid=%v err=%s", v, err)
			attr := "gouseradapter/query_base_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		baseSubRspPacket := baseRspPacket.GetDelUserBaseCacheRspbody()
		if baseSubRspPacket == nil {
			infoLog.Printf("ProcGetUserInfo baseRspPacket.GetDelUserBaseCacheRspbody failed")
			attr := "gouseradapter/query_base_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		infoLog.Printf("ProcModUserInfo base code=%v status=%s", baseSubRspPacket.GetStatus().GetCode(), baseSubRspPacket.GetStatus().GetReason())

	}
	return true
}

func ChangeToClearUserBlackListReq(reqHead *common.HeadV2, reqUid uint32) (reqPayLoad []byte, err error) {
	if reqHead == nil || reqUid == 0 {
		infoLog.Printf("ChangeToGetUserBlackListReq invalid param reqHead=%#v reqUid=%v", reqHead, reqUid)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_account_cache.AccountCacheReqBody{
		DelBlakcListInRedisReqbody: &ht_account_cache.DelBlackListInRedisReqBody{
			UserId: proto.Uint32(reqUid),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToGetUserBlackListReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

func ChangeToClearUserBaseCacheReq(reqHead *common.HeadV2, reqUid uint32) (reqPayLoad []byte, err error) {
	if reqHead == nil || reqUid == 0 {
		infoLog.Printf("ChangeToClearUserBaseCacheReq invalid param reqHead=%#v reqUid=%v", reqHead, reqUid)
		err = ErrInputParam
		return nil, err
	}

	reqBody := &ht_base_cache.BaseCacheReqBody{
		DelUserBaseCacheReqbody: &ht_base_cache.DelUserBaseCacheReqBody{
			ReqUid: proto.Uint32(reqUid),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ChangeToClearUserBaseCacheReq proto.Marshal failed uid=%v err=%s", reqHead.Uid, err)
	}
	return reqPayLoad, err
}

// 3.proc get user black cache
func ProcGetBlackList(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_user.ResultCode_RET_SUCCESS)
	rspBody := new(ht_user.RspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_user.ResultCode_RET_CACHE_ERR)
		rspBody = &ht_user.RspBody{
			Error: &ht_user.ErrorInfoBody{
				Code: proto.Uint32(uint32(result)),
				Msg:  []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetBlackList invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gouseradapter/get_user_bl_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_user.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetBlackList proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_user.ResultCode_RET_INTERNAL_ERR)
		rspBody = &ht_user.RspBody{
			Error: &ht_user.ErrorInfoBody{
				Code: proto.Uint32(uint32(result)),
				Msg:  []byte("proto unmarshal failed"),
			},
		}
		return false
	}

	uidList := reqBody.GetUser()
	var itermList []*ht_user.UserInfoBody
	reqHead := new(common.HeadV2)
	*reqHead = *head
	for _, v := range uidList {
		// STEP1 Update account cache
		reqPayLoad, err := ChangeToGetUserBlackListReq(head, v.GetUserID())
		if err != nil {
			infoLog.Printf("ProcGetBlackList ChangeToGetUserInfoReq() failed uid=%v err=%s", v, err)
			continue
		}

		reqHead.Uid = v.GetUserID() // set the get uid
		accountRspPacket, err := SendAndRecvPacketFromAccountCache(reqHead, ht_account_cache.CMD_TYPE_CMD_GET_BLACK_LIST, reqPayLoad)
		if err != nil {
			infoLog.Printf("ProcGetBlackList SendAndRecvPacketFromBaseCache() failed uid=%v err=%s", v, err)
			attr := "gouseradapter/query_account_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		accountSubRspPacket := accountRspPacket.GetGetUserBlackListRspbody()
		if accountSubRspPacket == nil {
			infoLog.Printf("ProcGetBlackList accountRspPacket.GetUserBlackListRspBody failed")
			attr := "gouseradapter/query_account_cache_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		blackList := accountSubRspPacket.GetBlackList()
		iterm := &ht_user.UserInfoBody{
			UserID:       proto.Uint32(v.GetUserID()),
			BlackidList:  []uint32(blackList),
			FriendidList: nil,
			FansList:     nil,
			VipExpireTs:  proto.Uint32(0),
		}
		itermList = append(itermList, iterm)
	}
	// return success
	result = uint16(ht_user.ResultCode_RET_SUCCESS)
	rspBody = &ht_user.RspBody{
		Error: &ht_user.ErrorInfoBody{
			Code: proto.Uint32(uint32(result)),
			Msg:  []byte("success"),
		},
		User: itermList,
	}
	return true
}

func SendPacketToAccountCache(reqHead *common.HeadV2, cmd ht_account_cache.CMD_TYPE, reqPayLoad []byte) (ret uint16, err error) {
	if reqHead == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("SendPacketToAccountCache invalid param reqHead=%#v reqPayLoad=%v", *reqHead, reqPayLoad)
		return ret, ErrInputParam
	}
	accountReqHead := new(common.HeadV2)
	*accountReqHead = *reqHead
	accountReqHead.Cmd = uint32(cmd)
	accountRsp, err := accountCacheApi.SendAndRecvPacket(accountReqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("SendPacketToAccountCache SendAndRecvPacket failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	accountPacket, ok := accountRsp.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("SendPacketToAccountCache accountPacket can not change to HeadV2packet")
		return ret, ErrChangePacketFailed
	}
	// 是HeadV2Packet报文
	// head 为一个new出来的对象指针
	rspHead, err := accountPacket.GetHead()
	if err != nil {
		infoLog.Printf("SendPacketToAccountCache Get accountPacket head failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	infoLog.Printf("SendPacketToAccountCache accountPacket uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
	ret = rspHead.Ret
	return ret, nil
}

func SendAndRecvPacketFromAccountCache(reqHead *common.HeadV2, cmd ht_account_cache.CMD_TYPE, reqPayLoad []byte) (rspBody *ht_account_cache.AccountCacheRspBody, err error) {
	if reqHead == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("SendAndRecvPacketFromAccountCache invalid param reqHead=%#v reqPayLoad=%v", *reqHead, reqPayLoad)
		return nil, ErrInputParam
	}
	accountReqHead := new(common.HeadV2)
	*accountReqHead = *reqHead
	accountReqHead.Cmd = uint32(cmd)
	accountRsp, err := accountCacheApi.SendAndRecvPacket(accountReqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromAccountCache SendAndRecvPacket failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}

	accountPacket, ok := accountRsp.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("SendAndRecvPacketFromAccountCache accountPacket can not change to HeadV2packet")
		return nil, ErrChangePacketFailed
	}
	// 是HeadV2Packet报文
	// head 为一个new出来的对象指针
	rspHead, err := accountPacket.GetHead()
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromAccountCache Get accountPacket head failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}
	//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	// infoLog.Printf("SendAndRecvPacketFromAccountCache accountPacket uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
	if rspHead.Ret != uint16(ht_account_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("SendAndRecvPacketFromAccountCache not success uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
		return nil, ErrCacheReturnErr
	}

	rspPayLoad := accountPacket.GetBody()
	rspBody = new(ht_account_cache.AccountCacheRspBody)
	err = proto.Unmarshal(rspPayLoad, rspBody)
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromAccountCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", rspHead.Uid, rspHead.Cmd, rspHead.Seq)
		return nil, err
	} else {
		return rspBody, nil
	}
}

func SendPacketToBaseCache(reqHead *common.HeadV2, cmd ht_base_cache.CMD_TYPE, reqPayLoad []byte) (ret uint16, err error) {
	if reqHead == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("SendPacketToBaseCache invalid param reqHead=%#v reqPayLoad=%v", *reqHead, reqPayLoad)
		return ret, ErrInputParam
	}
	baseReqHead := new(common.HeadV2)
	*baseReqHead = *reqHead
	baseReqHead.Cmd = uint32(cmd)
	baseRsp, err := baseCacheApi.SendAndRecvPacket(baseReqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("SendPacketToBaseCache SendAndRecvPacket failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	basePacket, ok := baseRsp.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("SendPacketToBaseCache basePacket can not change to HeadV2packet")
		return ret, ErrChangePacketFailed
	}
	// 是HeadV2Packet报文
	// head 为一个new出来的对象指针
	rspHead, err := basePacket.GetHead()
	if err != nil {
		infoLog.Printf("SendPacketToBaseCache Get basePacket head failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	infoLog.Printf("SendPacketToBaseCache basePacket uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
	ret = rspHead.Ret
	return ret, nil
}

func SendAndRecvPacketFromBaseCache(reqHead *common.HeadV2, cmd ht_base_cache.CMD_TYPE, reqPayLoad []byte) (rspBody *ht_base_cache.BaseCacheRspBody, err error) {
	if reqHead == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("SendAndRecvPacketFromBaseCache invalid param reqHead=%#v reqPayLoad=%v", *reqHead, reqPayLoad)
		return nil, ErrInputParam
	}
	baseReqHead := new(common.HeadV2)
	*baseReqHead = *reqHead
	baseReqHead.Cmd = uint32(cmd)
	baseRsp, err := baseCacheApi.SendAndRecvPacket(baseReqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromBaseCache SendAndRecvPacket failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}

	basePacket, ok := baseRsp.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("SendAndRecvPacketFromBaseCache basePacket can not change to HeadV2packet")
		return nil, ErrChangePacketFailed
	}
	// 是HeadV2Packet报文
	// head 为一个new出来的对象指针
	rspHead, err := basePacket.GetHead()
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromBaseCache Get basePacket head failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}
	//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	// infoLog.Printf("SendAndRecvPacketFromBaseCache basePacket uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
	if rspHead.Ret != uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("SendAndRecvPacketFromBaseCache not success uid=%v ret=%v", reqHead.Uid, rspHead.Ret)
		if rspHead.Ret == uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_DUPLICATE_RECORD) {
			return nil, ErrDuplicate
		} else {
			return nil, ErrCacheReturnErr
		}
	}
	rspPayLoad := basePacket.GetBody()
	rspBody = new(ht_base_cache.BaseCacheRspBody)
	err = proto.Unmarshal(rspPayLoad, rspBody)
	if err != nil {
		infoLog.Printf("SendAndRecvPacketFromBaseCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", rspHead.Uid, rspHead.Cmd, rspHead.Seq)
		return nil, err
	} else {
		return rspBody, nil
	}
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_user.RspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init master cache
	baseCacheIp := cfg.Section("BASECACHE").Key("ip").MustString("127.0.0.1")
	baseCachePort := cfg.Section("BASECACHE").Key("port").MustString("6379")
	infoLog.Printf("base cache ip=%v port=%v", baseCacheIp, baseCachePort)
	baseCacheApi = common.NewSrvToSrvApiV2(baseCacheIp, baseCachePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	accountCacheIp := cfg.Section("ACCOUNTCACHE").Key("ip").MustString("127.0.0.1")
	accountCachePort := cfg.Section("ACCOUNTCACHE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%v port=%v", accountCacheIp, accountCachePort)
	accountCacheApi = common.NewSrvToSrvApiV2(accountCacheIp, accountCachePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
