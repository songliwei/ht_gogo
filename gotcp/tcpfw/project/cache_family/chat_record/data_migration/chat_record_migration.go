package main

import (
	// "github.com/bitly/go-simplejson"
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_chatrecord"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()

	// init ssdb
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, 5, 50)
	if err != nil {
		infoLog.Printf("ssdb new pool failed err=%s", err)
		return
	}

	// 遍历 tmp_user_momet_info 表读取每个uid 查询 ssdb 得到moment数和likecoutn
	setBegin := cfg.Section("ENDUID").Key("beginUid").MustInt()
	endUid := cfg.Section("ENDUID").Key("uid").MustInt()

	for beginUid := setBegin; beginUid < endUid; beginUid += 1 {
		infoLog.Printf("beginUid=%v", beginUid)
		rows, err := db.Query("SELECT FROMID, TOID, TEXT, TRANSLATE, IMAG, VOICE, LOCATION, PROFILE, OTHER, TOTAL, UNIX_TIMESTAMP(STARTTIME), UNIX_TIMESTAMP(UPDATETIME) FROM HT_CHAT_RECORD WHERE FROMID = ?", beginUid)
		if err != nil {
			infoLog.Printf("mysql get uid failed err=%s", err)
			return
		}
		defer rows.Close()
		kvs := make(map[string]interface{})
		var storeFromId, storeToId, storeText, storeTranslate, storeImag, storeVoice, storeLocation, storeProfile, storeOther, storeTotal, storeStartTime, storeUpdateTime sql.NullInt64
		for rows.Next() {
			if err := rows.Scan(&storeFromId, &storeToId, &storeText, &storeTranslate, &storeImag, &storeVoice, &storeLocation, &storeProfile, &storeOther, &storeTotal, &storeStartTime, &storeUpdateTime); err != nil {
				infoLog.Printf("mysql get uid=%v rows.Scan failed err=%s", beginUid, err)
				continue
			}
			var fromId, toId, text, translate, image, voice, location, profile, other, total, startTime, updateTime uint32
			if storeFromId.Valid {
				fromId = uint32(storeFromId.Int64)
			}

			if storeToId.Valid {
				toId = uint32(storeToId.Int64)
			}

			if storeText.Valid {
				text = uint32(storeText.Int64)
			}

			if storeTranslate.Valid {
				translate = uint32(storeTranslate.Int64)
			}

			if storeImag.Valid {
				image = uint32(storeImag.Int64)
			}

			if storeVoice.Valid {
				voice = uint32(storeVoice.Int64)
			}

			if storeLocation.Valid {
				location = uint32(storeLocation.Int64)
			}

			if storeProfile.Valid {
				profile = uint32(storeProfile.Int64)
			}

			if storeOther.Valid {
				other = uint32(storeOther.Int64)
			}

			if storeTotal.Valid {
				total = uint32(storeTotal.Int64)
			}

			if storeStartTime.Valid {
				startTime = uint32(storeStartTime.Int64)
			}

			if storeUpdateTime.Valid {
				updateTime = uint32(storeUpdateTime.Int64)
			}
			//infoLog.Printf("fromId=%v toId=%v total=%v", fromId, toId, total)
			if total == 0 {
				infoLog.Printf("continue uid=%v", beginUid)
				continue
			}

			item := &ht_chat_record.ChatRecordItem{
				FromId:        proto.Uint32(fromId),
				ToId:          proto.Uint32(toId),
				TextCount:     proto.Uint32(text),
				TransCount:    proto.Uint32(translate),
				ImagCount:     proto.Uint32(image),
				VoiceCount:    proto.Uint32(voice),
				LocationCount: proto.Uint32(location),
				ProfileCount:  proto.Uint32(profile),
				OtherCount:    proto.Uint32(other),
				TotalCount:    proto.Uint32(total),
				StartTime:     proto.Uint32(startTime),
				UpdateTime:    proto.Uint32(updateTime),
			}

			payLoad, err := proto.Marshal(item)
			if err != nil {
				infoLog.Printf("load item fromId=%v toId=%v text=%v traslate=%v image=%v voice=%v location=%v total=%v starttime=%v updatetime=%v err=%s",
					fromId,
					toId,
					text,
					translate,
					image,
					voice,
					location,
					total,
					startTime,
					updateTime,
					err)
				continue
			}
			key := fmt.Sprintf("%v", toId)
			kvs[key] = string(payLoad)

			// generate speak to me hash map
			speakToMeHashName := GetSpeakToMeHashMapName(toId)
			speakToKey := fmt.Sprintf("%v", fromId)
			err = ssdbApi.Hset(speakToMeHashName, speakToKey, string(payLoad))
			if err != nil {
				infoLog.Printf("ssdbApi.HSet hashName=%s key=%s err=%v", speakToMeHashName, speakToKey, err)
				continue
			}
		}
		if len(kvs) == 0 {
			infoLog.Printf("uid=%v hashmap empty", beginUid)
			continue
		}
		// generate i speak to hash map
		iSpeakToHashName := GetISpeakToHashMapName(uint32(beginUid))
		err = ssdbApi.MultiHset(iSpeakToHashName, kvs)
		if err != nil {
			infoLog.Printf("ssdbApi.MultiHset hashName=%s keyValue=%v err=%s", iSpeakToHashName, kvs, err)
			continue
		}
	}
	return
}

func GetISpeakToHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#ispeakto", uid)
	return hashName
}

func GetSpeakToMeHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#speaktome", uid)
	return hashName
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
