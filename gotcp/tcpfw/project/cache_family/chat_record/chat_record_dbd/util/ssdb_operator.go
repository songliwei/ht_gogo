// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_chatrecord"
	"github.com/golang/protobuf/proto"
)

// Error type
var (
	ErrNotExistInReids = errors.New("not exist in redis")
	ErrNilDbObject     = errors.New("not set object current is nil")
)

type SsdbOperator struct {
	ssdbApi *common.SsdbApi
	infoLog *log.Logger
}

func NewSsdbOperator(targetSsdbApi *common.SsdbApi, logger *log.Logger) *SsdbOperator {
	return &SsdbOperator{
		ssdbApi: targetSsdbApi,
		infoLog: logger,
	}
}

// 增加聊天计数
func (this *SsdbOperator) HincrISpeakToChatRecord(hashName string, fromId, toId uint32, chatType ht_chat_record.E_MSG_TYPE) (newValue int64, err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return newValue, err
	}
	key := fmt.Sprintf("%v", toId)
	ret, err := this.ssdbApi.Hexists(hashName, key)
	if err != nil {
		this.infoLog.Printf("HincrISpeakToChatRecord hashName=%v key=%v ssdbApi.Hexists return err=%s", hashName, key, err)
		return newValue, err
	}
	var valSlice []byte
	if !ret { //不存在则直接设置进去
		item := &ht_chat_record.ChatRecordItem{
			FromId:     proto.Uint32(fromId),
			ToId:       proto.Uint32(toId),
			TotalCount: proto.Uint32(1),
			StartTime:  proto.Uint32(uint32(time.Now().Unix())),
			UpdateTime: proto.Uint32(uint32(time.Now().Unix())),
		}
		switch chatType {
		case ht_chat_record.E_MSG_TYPE_MT_TEXT:
			item.TextCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_TRANSLATE:
			item.TransCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_IMAGE:
			item.ImagCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_VOICE:
			item.VoiceCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_LOCATE:
			item.LocationCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_PROFILE:
			item.ProfileCount = proto.Uint32(1)
		default:
			item.OtherCount = proto.Uint32(1)
		}
		valSlice, err = proto.Marshal(item)
		if err != nil {
			this.infoLog.Printf("HincrISpeakToChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			newValue = 1
			return newValue, err
		}
	} else { //存在则将计数加1
		value, err := this.ssdbApi.Hget(hashName, key)
		if err != nil {
			this.infoLog.Printf("HincrISpeakToChatRecord Hget failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return newValue, err
		}
		item := new(ht_chat_record.ChatRecordItem)
		err = proto.Unmarshal([]byte(value), item)
		if err != nil {
			this.infoLog.Printf("HincrISpeakToChatRecord proto Unmarshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return newValue, err
		}
		item.TotalCount = proto.Uint32(item.GetTotalCount() + 1)
		newValue = int64(item.GetTotalCount())
		item.UpdateTime = proto.Uint32(uint32(time.Now().Unix()))
		switch chatType {
		case ht_chat_record.E_MSG_TYPE_MT_TEXT:
			item.TextCount = proto.Uint32(item.GetTextCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_TRANSLATE:
			item.TransCount = proto.Uint32(item.GetTransCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_IMAGE:
			item.ImagCount = proto.Uint32(item.GetImagCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_VOICE:
			item.VoiceCount = proto.Uint32(item.GetVoiceCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_LOCATE:
			item.LocationCount = proto.Uint32(item.GetLocationCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_PROFILE:
			item.ProfileCount = proto.Uint32(item.GetProfileCount() + 1)
		default:
			item.OtherCount = proto.Uint32(item.GetOtherCount() + 1)
		}

		valSlice, err = proto.Marshal(item)
		if err != nil {
			this.infoLog.Printf("HincrISpeakToChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			newValue = 0
			return newValue, err
		}
	}
	err = this.ssdbApi.Hset(hashName, key, string(valSlice))
	if err != nil {
		this.infoLog.Printf("HincrISpeakToChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
			hashName,
			fromId,
			toId,
			chatType,
			err)
		return newValue, err
	} else {
		this.infoLog.Printf("HincrISpeakToChatRecord hashname=%s from=%v to=%v type=%v newValue=%v",
			hashName,
			fromId,
			toId,
			chatType,
			newValue)
		return newValue, nil
	}
}

func (this *SsdbOperator) HincrSpeakToMeChatRecord(hashName string, fromId, toId uint32, chatType ht_chat_record.E_MSG_TYPE) (newValue int64, err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return newValue, err
	}
	key := fmt.Sprintf("%v", fromId)
	ret, err := this.ssdbApi.Hexists(hashName, key)
	if err != nil {
		this.infoLog.Printf("HincrSpeakToMeChatRecord hashName=%v key=%v ssdbApi.Hexists return err=%s", hashName, key, err)
		return newValue, err
	}
	var valSlice []byte
	if !ret { //不存在则直接设置进去
		item := &ht_chat_record.ChatRecordItem{
			FromId:     proto.Uint32(fromId),
			ToId:       proto.Uint32(toId),
			TotalCount: proto.Uint32(1),
			StartTime:  proto.Uint32(uint32(time.Now().Unix())),
			UpdateTime: proto.Uint32(uint32(time.Now().Unix())),
		}
		switch chatType {
		case ht_chat_record.E_MSG_TYPE_MT_TEXT:
			item.TextCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_TRANSLATE:
			item.TransCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_IMAGE:
			item.ImagCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_VOICE:
			item.VoiceCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_LOCATE:
			item.LocationCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_PROFILE:
			item.ProfileCount = proto.Uint32(1)
		default:
			item.OtherCount = proto.Uint32(1)
		}
		valSlice, err = proto.Marshal(item)
		if err != nil {
			this.infoLog.Printf("HincrSpeakToMeChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			newValue = 1
			return newValue, err
		}
	} else { //存在则将计数加1
		value, err := this.ssdbApi.Hget(hashName, key)
		if err != nil {
			this.infoLog.Printf("HincrSpeakToMeChatRecord Hget failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return newValue, err
		}
		item := new(ht_chat_record.ChatRecordItem)
		err = proto.Unmarshal([]byte(value), item)
		if err != nil {
			this.infoLog.Printf("HincrSpeakToMeChatRecord proto Unmarshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return newValue, err
		}
		item.TotalCount = proto.Uint32(item.GetTotalCount() + 1)
		newValue = int64(item.GetTotalCount())
		item.UpdateTime = proto.Uint32(uint32(time.Now().Unix()))
		switch chatType {
		case ht_chat_record.E_MSG_TYPE_MT_TEXT:
			item.TextCount = proto.Uint32(item.GetTextCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_TRANSLATE:
			item.TransCount = proto.Uint32(item.GetTransCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_IMAGE:
			item.ImagCount = proto.Uint32(item.GetImagCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_VOICE:
			item.VoiceCount = proto.Uint32(item.GetVoiceCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_LOCATE:
			item.LocationCount = proto.Uint32(item.GetLocationCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_PROFILE:
			item.ProfileCount = proto.Uint32(item.GetProfileCount() + 1)
		default:
			item.OtherCount = proto.Uint32(item.GetOtherCount() + 1)
		}

		valSlice, err = proto.Marshal(item)
		if err != nil {
			this.infoLog.Printf("HincrSpeakToMeChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			newValue = 0
			return newValue, err
		}
	}
	err = this.ssdbApi.Hset(hashName, key, string(valSlice))
	if err != nil {
		this.infoLog.Printf("HincrSpeakToMeChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
			hashName,
			fromId,
			toId,
			chatType,
			err)
		return newValue, err
	} else {
		this.infoLog.Printf("HincrSpeakToMeChatRecord hashname=%s from=%v to=%v type=%v newValue=%v",
			hashName,
			fromId,
			toId,
			chatType,
			newValue)
		return newValue, nil
	}
}

// 获取指定key 的计数
func (this *SsdbOperator) HgetChatRecord(hashName string, toIdList []uint32) (ackList []*ht_chat_record.ChatRecordRspItem, err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	keys := make([]string, len(toIdList))
	for i, v := range toIdList {
		this.infoLog.Printf("index=%v toId=%v", i, v)
		keys[i] = fmt.Sprintf("%v", v)
	}
	//获取指定key的计数
	outKeys, outValues, err := this.ssdbApi.MultiHgetSliceArray(hashName, keys)
	if err != nil {
		this.infoLog.Printf("HgetChatRecord MultiHgetSliceArray failed hashname=%s keys=%v err=%s",
			hashName,
			keys,
			err)
		return nil, err
	}

	for i, v := range outValues {
		toId, err := strconv.ParseUint(outKeys[i], 10, 32)
		item := new(ht_chat_record.ChatRecordItem)
		err = proto.Unmarshal(v.Bytes(), item)
		if err != nil {
			this.infoLog.Printf("HgetChatRecord proto Unmarshal failed hashname=%s index=%v value=%v err=%s",
				hashName,
				i,
				v,
				err)
			continue
		}
		rspItem := &ht_chat_record.ChatRecordRspItem{
			ToId:       proto.Uint32(uint32(toId)),
			TotalCount: proto.Uint32(item.GetTotalCount()),
			StartTime:  proto.Uint32(item.GetStartTime()),
		}
		ackList = append(ackList, rspItem)
	}

	return ackList, nil
}

// 获取全部key 的计数
func (this *SsdbOperator) HgetAllChatRecord(hashName string) (keyValues []string, err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	outKeys, outValues, err := this.ssdbApi.MultiHgetAllSlice(hashName)
	if err != nil {
		this.infoLog.Printf("HgetChatRecord MultiHgetSliceArray failed hashname=%s err=%s",
			hashName,
			err)
		return nil, err
	}
	keyValues = make([]string, 2*len(outValues))
	for i := 0; i < len(keyValues); i += 2 {
		keyValues[i] = outKeys[i/2]
		keyValues[i+1] = outValues[i/2].String()
	}
	return keyValues, nil
}
