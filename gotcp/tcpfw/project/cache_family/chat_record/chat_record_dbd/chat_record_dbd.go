package main

import (
	"fmt"
	"runtime/pprof"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_chatrecord"
	"github.com/HT_GOGO/gotcp/tcpfw/project/cache_family/chat_record/chat_record_dbd/util"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog             *log.Logger
	ssdbOperator        *util.SsdbOperator
	writeAgentMastreApi *common.SrvToSrvApiV2
	writeAgentSlaveApi  *common.SrvToSrvApiV2
	cpuProfile          string
	memProfile          string
	memProfileRate      int
)

const (
	ProcSlowThreshold = 300000
	HashMapSizeLimit  = 1000
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gocrdbd/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_chat_record.CMD_TYPE_CMD_INC_CHAT_RECORD):
		go ProcIncChatRecord(c, head, packet)
	case uint32(ht_chat_record.CMD_TYPE_CMD_GET_CHAT_RECORD):
		go ProcGetChatRecord(c, head, packet)
	case uint32(ht_chat_record.CMD_TYPE_CMD_GET_HAHS_LIST):
		go ProcGetHasMap(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

func GetISpeakToHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#ispeakto", uid)
	return hashName
}

func GetSpeakToMeHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#speaktome", uid)
	return hashName
}

// 1.proc inc chat record
func ProcIncChatRecord(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcIncChatRecord no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcIncChatRecord invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocrdbd/inc_chat_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcIncChatRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetIncChatRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcIncChatRecord GetIncChatRecordReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	chatItems := subReqBody.GetChatItems()
	for i, v := range chatItems {
		fromId := v.GetFromId()
		toId := v.GetToId()
		chatType := v.GetType()
		infoLog.Printf("ProcIncChatRecord index=%v from=%v to=%v type=%v", i, fromId, toId, chatType)
		// add i speak to A count
		hashName := GetISpeakToHashMapName(fromId)
		_, err = ssdbOperator.HincrISpeakToChatRecord(hashName, fromId, toId, chatType)
		if err != nil {
			infoLog.Printf("ProcIncChatRecord ssdbOperator.HincrISpeakToChatRecord failed hashName=%v fromId=%v toId=%v chatType=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			attr := "gocrdbd/update_db_chat_count_err"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		// add a recv i message count
		hashName = GetSpeakToMeHashMapName(toId)
		_, err = ssdbOperator.HincrSpeakToMeChatRecord(hashName, fromId, toId, chatType)
		if err != nil {
			infoLog.Printf("ProcIncChatRecord ssdbOperator.HincrSpeakToMeChatRecord failed hashName=%v fromId=%v toId=%v chatType=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			attr := "gocrdbd/update_db_chat_count_err"
			libcomm.AttrAdd(attr, 1)
			continue
		}
	}

	rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
		Status: &ht_chat_record.ChatRecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 notify write agent to update redis
	//双写更新Redis
	SetRedisCache(head, payLoad)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocrdbd/update_db_chat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.proc get chat record
func ProcGetChatRecord(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetChatRecord no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetChatRecord invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocrdbd/get_chat_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetChatRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetChatRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetChatRecord GetGetChatRecordReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	fromId := subReqBody.GetFromId()
	hashName := GetISpeakToHashMapName(fromId)
	toIdList := subReqBody.GetToIdList()

	ackList, err := ssdbOperator.HgetChatRecord(hashName, toIdList)
	if err != nil {
		infoLog.Printf("ProcGetChatRecord ssdbOperator.HgetChatRecord failed fromId=%v toIdList=%v err=%s", fromId, toIdList, err)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
		rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("db error"),
			},
		}
		attr := "gocrdbd/get_chat_record_db_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	infoLog.Printf("ProcGetChatRecord fromId=%v ackList=%v", fromId, ackList)

	rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
		Status: &ht_chat_record.ChatRecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		AckList: ackList,
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 notify write agent to update redis
	// first add i speak to hashmap
	keyValue, err := ssdbOperator.HgetAllChatRecord(hashName)
	if err != nil {
		infoLog.Printf("ProcGetChatRecord ssdbOperator.HgetAllChatRecord hashName=%s err=%s", hashName, err)
		return false
	}
	iSpeakToLen := len(keyValue)
	if iSpeakToLen > HashMapSizeLimit {
		iSpeakToIndex := 0
		// 首先处理长度大于HashMapSizeLimit 部分
		for iSpeakToLen > HashMapSizeLimit {
			infoLog.Printf("ProcGetChatRecord hashName=%s iSpeakToIndex=%v iSpeakToLen=%v", hashName, iSpeakToIndex, iSpeakToLen)
			tempKeyValue := keyValue[iSpeakToIndex : iSpeakToIndex+HashMapSizeLimit]
			iSpeakToIndex += HashMapSizeLimit
			iSpeakToLen -= HashMapSizeLimit
			setItem := &ht_chat_record.SetChatRecordItem{
				HashName: proto.String(hashName),
				KyeValue: tempKeyValue,
			}
			setReqBody := &ht_chat_record.ChatRecordReqBody{
				SetChatRecordReqbody: &ht_chat_record.SetChatRecordReqBody{
					ItemList: []*ht_chat_record.SetChatRecordItem{setItem},
				},
			}
			reqPayLoad, err := proto.Marshal(setReqBody)
			if err != nil {
				infoLog.Printf("ProcGetChatRecord proto.Marshal ispeakto failed uid=%v err=%s", head.Uid, err)
				attr := "gocrdbd/get_chat_record_proto_err"
				libcomm.AttrAdd(attr, 1)
				return false
			}
			head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_SET_CHAT_RECORD)
			//双写更新Redis
			SetRedisCache(head, reqPayLoad)
		}

		// 再处理长度小余HashMapSizeLimit 部分
		setItem := &ht_chat_record.SetChatRecordItem{
			HashName: proto.String(hashName),
			KyeValue: keyValue[iSpeakToIndex:],
		}
		infoLog.Printf("ProcGetChatRecord last proc hashName=%s iSpeakToIndex=%v iSpeakToLen=%v", hashName, iSpeakToIndex, iSpeakToLen)
		setReqBody := &ht_chat_record.ChatRecordReqBody{
			SetChatRecordReqbody: &ht_chat_record.SetChatRecordReqBody{
				ItemList: []*ht_chat_record.SetChatRecordItem{setItem},
			},
		}
		reqPayLoad, err := proto.Marshal(setReqBody)
		if err != nil {
			infoLog.Printf("ProcGetChatRecord proto.Marshal ispeakto failed uid=%v err=%s", head.Uid, err)
			attr := "gocrdbd/get_chat_record_proto_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_SET_CHAT_RECORD)
		//双写更新Redis
		SetRedisCache(head, reqPayLoad)
	} else {
		setItem := &ht_chat_record.SetChatRecordItem{
			HashName: proto.String(hashName),
			KyeValue: keyValue,
		}
		setReqBody := &ht_chat_record.ChatRecordReqBody{
			SetChatRecordReqbody: &ht_chat_record.SetChatRecordReqBody{
				ItemList: []*ht_chat_record.SetChatRecordItem{setItem},
			},
		}
		reqPayLoad, err := proto.Marshal(setReqBody)
		if err != nil {
			infoLog.Printf("ProcGetChatRecord proto.Marshal ispeakto failed uid=%v err=%s", head.Uid, err)
			attr := "gocrdbd/get_chat_record_proto_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_SET_CHAT_RECORD)
		//双写更新Redis
		SetRedisCache(head, reqPayLoad)
	}

	// second add speak to me hashmap
	hashName = GetSpeakToMeHashMapName(fromId)
	keyValue, err = ssdbOperator.HgetAllChatRecord(hashName)
	if err != nil {
		infoLog.Printf("ProcGetChatRecord ssdbOperator.HgetAllChatRecord hashName=%s err=%s", hashName, err)
		return false
	}

	speakToMeLen := len(keyValue)
	if speakToMeLen > HashMapSizeLimit {
		speakToMeIndex := 0
		// 首先处理长度大于HashMapSizeLimit 部分
		for speakToMeLen > HashMapSizeLimit {
			infoLog.Printf("ProcGetChatRecord hashName=%s speakToMeIndex=%v speakToMeLen=%v", hashName, speakToMeIndex, speakToMeLen)
			tempKeyValue := keyValue[speakToMeIndex : speakToMeIndex+HashMapSizeLimit]
			speakToMeIndex += HashMapSizeLimit
			speakToMeLen -= HashMapSizeLimit
			setItem := &ht_chat_record.SetChatRecordItem{
				HashName: proto.String(hashName),
				KyeValue: tempKeyValue,
			}
			setReqBody := &ht_chat_record.ChatRecordReqBody{
				SetChatRecordReqbody: &ht_chat_record.SetChatRecordReqBody{
					ItemList: []*ht_chat_record.SetChatRecordItem{setItem},
				},
			}
			reqPayLoad, err := proto.Marshal(setReqBody)
			if err != nil {
				infoLog.Printf("ProcGetChatRecord proto.Marshal speaktome failed uid=%v err=%s", head.Uid, err)
				attr := "gocrdbd/get_chat_record_proto_err"
				libcomm.AttrAdd(attr, 1)
				return false
			}
			head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_SET_CHAT_RECORD)
			//双写更新Redis
			SetRedisCache(head, reqPayLoad)
		}

		// 再处理长度小余HashMapSizeLimit 部分
		setItem := &ht_chat_record.SetChatRecordItem{
			HashName: proto.String(hashName),
			KyeValue: keyValue[speakToMeIndex:],
		}
		infoLog.Printf("ProcGetChatRecord last proc hashName=%s speakToMeIndex=%v speakToMeLen=%v", hashName, speakToMeIndex, speakToMeLen)
		setReqBody := &ht_chat_record.ChatRecordReqBody{
			SetChatRecordReqbody: &ht_chat_record.SetChatRecordReqBody{
				ItemList: []*ht_chat_record.SetChatRecordItem{setItem},
			},
		}
		reqPayLoad, err := proto.Marshal(setReqBody)
		if err != nil {
			infoLog.Printf("ProcGetChatRecord proto.Marshal speakToMe failed uid=%v err=%s", head.Uid, err)
			attr := "gocrdbd/get_chat_record_proto_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_SET_CHAT_RECORD)
		//双写更新Redis
		SetRedisCache(head, reqPayLoad)
	} else {
		setItem := &ht_chat_record.SetChatRecordItem{
			HashName: proto.String(hashName),
			KyeValue: keyValue,
		}
		setReqBody := &ht_chat_record.ChatRecordReqBody{
			SetChatRecordReqbody: &ht_chat_record.SetChatRecordReqBody{
				ItemList: []*ht_chat_record.SetChatRecordItem{setItem},
			},
		}
		reqPayLoad, err := proto.Marshal(setReqBody)
		if err != nil {
			infoLog.Printf("ProcGetChatRecord proto.Marshal speakToMe failed uid=%v err=%s", head.Uid, err)
			attr := "gocrdbd/get_chat_record_proto_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_SET_CHAT_RECORD)
		//双写更新Redis
		SetRedisCache(head, reqPayLoad)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocrdbd/get_chat_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 3.proc get hash map
func ProcGetHasMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetHasMap no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.GetHashMapRspbody = &ht_chat_record.GetHashMapRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetHasMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocrdbd/get_hash_map_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetHasMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetHashMapRspbody = &ht_chat_record.GetHashMapRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetHashMapReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetHasMap GetGetHashMapRspbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetHashMapRspbody = &ht_chat_record.GetHashMapRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashList := subReqBody.GetHashNamdList()
	if len(hashList) == 0 {
		infoLog.Printf("ProcGetHasMap subReqBody.GetHashNamdList() empty uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INPUT_PARAM_ERR)
		rspBody.GetHashMapRspbody = &ht_chat_record.GetHashMapRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("hash name empty"),
			},
		}
		return false
	}

	var hashMapList []*ht_chat_record.HashMapItem
	for i, v := range hashList {
		infoLog.Printf("ProcGetHasMap index=%v hashName=%s", i, v)
		keyValues, err := ssdbOperator.HgetAllChatRecord(v)
		if err != nil {
			infoLog.Printf("ProcGetHasMap ssdbOperator.HgetAllChatRecord failed fromId=%v hashName=%v err=%s", head.Uid, v, err)
			continue
		}

		item := &ht_chat_record.HashMapItem{
			HashName:  proto.String(v),
			KeyValues: keyValues,
		}
		hashMapList = append(hashMapList, item)
	}

	rspBody.GetHashMapRspbody = &ht_chat_record.GetHashMapRspBody{
		Status: &ht_chat_record.ChatRecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		HashMapList: hashMapList,
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocrdbd/get_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	// no need to update redis
	return true
}

func SetRedisCache(head *common.HeadV2, reqPayLoad []byte) {
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}

	redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisSlavePacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}

	} else {
		infoLog.Printf("SetRedisCache writeAgentSlaveApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_chat_record.ChatRecordRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func startCPUProfile() {
	if cpuProfile != "" {
		f, err := os.Create(cpuProfile)
		if err != nil {
			infoLog.Printf("Can not create cpu profile output file: %s", err)
			return
		}
		if err := pprof.StartCPUProfile(f); err != nil {
			infoLog.Printf("Can not start cpu profile: %s", err)
			f.Close()
			return
		}
	}
}

func stopCPUProfile() {
	if cpuProfile != "" {
		pprof.StopCPUProfile() // 把记录的概要信息写到已指定的文件
	}
}

func startMemProfile() {
	if memProfile != "" && memProfileRate > 0 {
		runtime.MemProfileRate = memProfileRate
	}
}

func stopMemProfile() {
	if memProfile != "" {
		f, err := os.Create(memProfile)
		if err != nil {
			infoLog.Printf("Can not create mem profile output file: %s", err)
			return
		}
		if err = pprof.WriteHeapProfile(f); err != nil {
			infoLog.Printf("Can not write %s: %s", memProfile, err)
		}
		f.Close()
	}
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// read profile config path
	cpuProfile = cfg.Section("PROFILE").Key("cpu_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/cpu.prof")
	memProfile = cfg.Section("PROFILE").Key("mem_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/mem.out")
	memProfileRate = cfg.Section("PROFILE").Key("mem_rate").MustInt(512 * 1024)
	// read ssdb config
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	ssdbOperator = util.NewSsdbOperator(ssdbApi, infoLog)

	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%v port=%v", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%v port=%v", slaveIp, slavePort)
	writeAgentSlaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	//startCPUProfile()
	//startMemProfile()
	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	//stopCPUProfile()
	//stopMemProfile()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
