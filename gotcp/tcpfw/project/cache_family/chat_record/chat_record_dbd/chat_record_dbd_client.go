package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	Cmd int `short:"t" long:"cmd" description:"Command type" optional:"no"`

	TargetMid string `short:"m" long:"mid" description:"Target Mid" optional:"no"`

	Level int `short:"l" long:"level" description:"featured level" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(12750)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	v2Protocol := &common.HeadV2Protocol{}
	var head *common.HeadV2
	head = &common.HeadV2{
		Version:  4,
		Cmd:      0,
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      1946612,
		SysType:  8,
	}

	var payLoad []byte
	reqObj := simplejson.New()
	switch options.Cmd {
	// 创建群聊
	case 101:
		head.Cmd = 101
		reqObj.Set("mid", options.TargetMid)
		condition := simplejson.New()
		national := simplejson.New()
		// 设置指给中国人看
		national.Set("is_all", 1)
		nationalArray := []string{"CN"}
		national.Set("national_array", nationalArray)
		condition.Set("nationality", national)
		// 设置学习语言
		learnLang := simplejson.New()
		learnLang.Set("is_all", 0)
		learnArray := []uint32{}
		learnLang.Set("learnLang", learnArray)
		condition.Set("learn_language", learnLang)
		// 设置教学语言
		teachLang := simplejson.New()
		teachLang.Set("is_all", 0)
		teachArray := []uint32{}
		teachLang.Set("teach_array", teachArray)
		condition.Set("teach_language", teachArray)
		// 设置注册时间
		regTime := simplejson.New()
		regTime.Set("is_all", 0)
		regTime.Set("begin", 0)
		regTime.Set("end", 0)
		condition.Set("regiter_time", regTime)
		// 设置在线时间
		onlineTime := simplejson.New()
		onlineTime.Set("is_all", 0)
		onlineTime.Set("begin", 0)
		onlineTime.Set("end", 0)
		condition.Set("online_time", onlineTime)
		// 设置允许看的地理位置
		loc := simplejson.New()
		loc.Set("is_all", 0)
		loc.Set("long", -73.97)
		loc.Set("lati", 40.77)
		placeWuHan := simplejson.New()
		placeWuHan.Set("country_name", "CN")
		placeWuHan.Set("admin", "GuangDo")
		placeWuHan.Set("locality", "ShenZhen")
		placeArray := []*simplejson.Json{placeWuHan}
		loc.Set("place_array", placeArray)
		condition.Set("location", loc)
		// 设置可以看的年龄
		ageLimit := simplejson.New()
		ageLimit.Set("is_all", 0)
		ageLimit.Set("begin", 0)
		ageLimit.Set("end", 0)
		condition.Set("age", ageLimit)
		reqObj.Set("condition", condition)
		// 设置精选等级
		// reqObj.Set("level", 10)
		reqObj.Set("level", options.Level)

		// 设置展示的分类
		showSlice := []uint32{0, 4, 5}
		reqObj.Set("show_classify", showSlice)
		// 设置展示次数
		reqObj.Set("show_times", 2)
		// 设置生效时间
		validTime := simplejson.New()
		validTime.Set("begin", 1481335810)
		validTime.Set("end", 1483150210)
		reqObj.Set("valid_time", validTime)

	case 102:
		head.Cmd = 102
		reqObj.Set("mid", options.TargetMid)
		reqObj.Set("level", 10)
	case 103:
		head.Cmd = 103
		levleIterm := simplejson.New()
		// 设置已经拉取过的序号
		levleIterm.Set("level", 10)
		levleIterm.Set("index", 3)
		levelSlice := []*simplejson.Json{levleIterm}
		reqObj.Set("level_index", levelSlice)
		// 设置国籍
		reqObj.Set("nationality", "CN")
		learnSlice := []uint32{2, 3, 4}
		teachSlice := []uint32{6, 7, 8}
		// 设置学习语言
		reqObj.Set("learn_lang", learnSlice)
		// 设置教学语言
		reqObj.Set("teach_lang", teachSlice)
		// 设置注册时间
		reqObj.Set("regiter_time", 1451527810)
		// 设置地理位置
		location := simplejson.New()
		location.Set("longitude", 30.60)
		location.Set("latitude", 114.45)
		location.Set("country", "CN")
		location.Set("admin1", "HuBei")
		location.Set("locality", "WuHan")
		reqObj.Set("location", location)
		// 设置生日
		reqObj.Set("birthday", "1987-01-01")
		// 设置展示分类
		showSlice := []uint32{1, 2, 3}
		reqObj.Set("show_classify", showSlice)
		reqObj.Set("get_count", 5)

	default:
		infoLog.Println("UnKnow input cmd =", options.Cmd)
	}

	payLoad, err = reqObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("simplejson.MarshalJSON failed uid=%v cmd=%v seq=%v",
			head.Uid,
			head.Cmd,
			head.Seq)
		return
	}

	head.Len = uint32(common.PacketV2HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV2ToSlice failed")
		return
	}
	copy(buf[common.PacketV2HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV2MagicEnd

	infoLog.Printf("len=%v payLaod=%s\n", len(payLoad), payLoad)
	// write
	conn.Write(buf)
	// read
	p, err := v2Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket := p.(*common.HeadV2Packet)
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("resp len=%v cmd=%v\n", rspHead.Len, rspHead.Cmd)
		// json 反序列化
		switch rspHead.Cmd {
		case 101:
			fallthrough
		case 102:
			rspJson, err := simplejson.NewJson(rspPayLoad)
			if err != nil {
				infoLog.Printf("resp simplejson.NewJson failed err=%s", err)
				return
			}
			code := rspJson.Get("status").Get("code").MustInt(1)
			reason := rspJson.Get("status").Get("reason").MustString()
			infoLog.Printf("recv cmd=%v response code=%v reason=%s", rspHead.Cmd, code, reason)
		case 103:
			rspJson, err := simplejson.NewJson(rspPayLoad)
			if err != nil {
				infoLog.Printf("resp simplejson.NewJson failed err=%s", err)
				return
			}
			code := rspJson.Get("status").Get("code").MustInt(1)
			reason := rspJson.Get("status").Get("reason").MustString()
			infoLog.Printf("recv cmd=%v response code=%v reason=%s", rspHead.Cmd, code, reason)
			infoLog.Printf("recv cmd=%v response=%s", rspHead.Cmd, rspPayLoad)

		default:
			infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
		}
	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
