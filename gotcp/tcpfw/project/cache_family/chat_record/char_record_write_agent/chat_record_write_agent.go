package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_chatrecord"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	redisMasterApi *common.RedisApi
	ssdbApi        *common.SsdbApi
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrSsdbObj      = errors.New("err nil ssdb object")
)

const (
	ExpirePeriod = 259200 //3 * 86400 = 3 day   units:second
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gocragent/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch uint32(head.Cmd) {
	case uint32(ht_chat_record.CMD_TYPE_CMD_INC_CHAT_RECORD):
		go ProcIncChatRecord(c, head, packet)
	case uint32(ht_chat_record.CMD_TYPE_CMD_SET_CHAT_RECORD):
		go ProcSetChatRecord(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
		// 无法处理的命令 关闭连接
		c.Close()
	}
	return true
}

// 1.proc set user account cache
func ProcIncChatRecord(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcIncChatRecord invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocragent/inc_chat_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcIncChatRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetIncChatRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcIncChatRecord GetIncChatRecordReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	itemList := subReqBody.GetChatItems()
	if len(itemList) == 0 {
		infoLog.Println("ProcIncChatRecord subReqBody.GetChatItems() nil uid=%v", head.Uid)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("chat items empty"),
			},
		}
		return false
	}
	for i, v := range itemList {
		fromId := v.GetFromId()
		toId := v.GetToId()
		chatType := v.GetType()
		infoLog.Printf("ProcIncChatRecord index=%v fromId=%v toId=%v chatType=%v",
			i,
			fromId,
			toId,
			chatType)
		// Step 1: 首先判断 I speak to hash map 是否存在，如果不存在则加载，存在则直接计数+1
		hashName := GetISpeakToHashMapName(fromId)
		ret, err := redisMasterApi.Exists(hashName)
		if err != nil {
			attr := "gocragent/inc_chat_record_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcIncChatRecord redisMasterApi.Exists hashName=%s return err=%v",
				hashName,
				err)
			continue
		}
		if !ret { // 如果不存在则加载整个hashmap
			keyValues, err := ReloadChatRecord(hashName)
			if err != nil {
				attr := "gocragent/inc_chat_record_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcIncChatRecord ReloadChatRecord hashName=%s err=%s", hashName, err)
				continue
			}
			err = redisMasterApi.Hmset(hashName, keyValues)
			if err != nil {
				attr := "gocragent/inc_chat_record_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcIncChatRecord redisMasterApi.Hmset hashName=%s err=%s", hashName, err)
				continue
			}

			// 设置过期时间为15天
			err = redisMasterApi.Expire(hashName, ExpirePeriod)
			if err != nil {
				// add static
				attr := "gocragent/set_key_expire_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcIncChatRecord redisMasterApi.Expire key=%s err=%s failed", hashName, err)
			}
		}
		// Step 2: 将支持hash map 的特定key 增加1
		err = HincrISpeakToChatRecord(hashName, fromId, toId, chatType)
		if err != nil {
			attr := "gocragent/inc_chat_record_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcIncChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			continue
		} else {
			infoLog.Printf("HincrChatRecord hashname=%s from=%v to=%v type=%v",
				hashName,
				fromId,
				toId,
				chatType)
		}

		// 设置过期时间为15天
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			// add static
			attr := "gocragent/set_key_expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcIncChatRecord redisMasterApi.Expire key=%s err=%s failed", hashName, err)
		}

		// Step 3: 首先判断hash map 是否存在，如果不存在则加载，存在则直接计数+1
		hashName = GetSpeakToMeHashMapName(toId)
		ret, err = redisMasterApi.Exists(hashName)
		if err != nil {
			attr := "gocragent/inc_chat_record_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcIncChatRecord redisMasterApi.Exists hashName=%s return err=%v",
				hashName,
				err)
			continue
		}
		if !ret { // 如果不存在则加载整个hashmap
			keyValues, err := ReloadChatRecord(hashName)
			if err != nil {
				attr := "gocragent/inc_chat_record_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcIncChatRecord ReloadChatRecord hashName=%s err=%s", hashName, err)
				continue
			}
			err = redisMasterApi.Hmset(hashName, keyValues)
			if err != nil {
				attr := "gocragent/inc_chat_record_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcIncChatRecord redisMasterApi.Hmset hashName=%s err=%s", hashName, err)
				continue
			}
			// 设置过期时间为15天
			err = redisMasterApi.Expire(hashName, ExpirePeriod)
			if err != nil {
				// add static
				attr := "gocragent/set_key_expire_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcIncChatRecord redisMasterApi.Expire key=%s err=%s failed", hashName, err)
			}
		}
		// Step 2: 将支持hash map 的特定key 增加1
		err = HincrSpeakToMeChatRecord(hashName, fromId, toId, chatType)
		if err != nil {
			attr := "gocragent/inc_chat_record_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcIncChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			continue
		} else {
			infoLog.Printf("HincrChatRecord hashname=%s from=%v to=%v type=%v",
				hashName,
				fromId,
				toId,
				chatType)
		}

		// 设置过期时间为15天
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			// add static
			attr := "gocragent/set_key_expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcIncChatRecord redisMasterApi.Expire key=%s err=%s failed", hashName, err)
		}
	}

	rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
		Status: &ht_chat_record.ChatRecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func GetISpeakToHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#ispeakto", uid)
	return hashName
}

func GetSpeakToMeHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#speaktome", uid)
	return hashName
}

func ReloadChatRecord(hashName string) (keyValues []string, err error) {
	if ssdbApi == nil {
		err = ErrSsdbObj
		return nil, err
	}
	outKeys, outValues, err := ssdbApi.MultiHgetAllSlice(hashName)
	if err != nil {
		infoLog.Printf("ReloadChatRecord hashName=%s err=%s", hashName, err)
		return nil, err
	}
	if len(outKeys) != len(outValues) {
		infoLog.Printf("ReloadChatRecord outKeysLen=%v outValuesLen=%v not equal", len(outKeys), len(outValues))
		err = ErrInvalidParam
		return nil, err
	}
	keyValues = make([]string, 2*len(outValues))
	for i := 0; i < len(keyValues); i += 2 {
		keyValues[i] = outKeys[i/2]
		keyValues[i+1] = outValues[i/2].String()
	}
	return keyValues, nil
}

func HincrISpeakToChatRecord(hashName string, fromId, toId uint32, chatType ht_chat_record.E_MSG_TYPE) (err error) {
	if len(hashName) == 0 || fromId == 0 || toId == 0 {
		err = ErrInvalidParam
		return err
	}
	key := fmt.Sprintf("%v", toId)
	ret, err := redisMasterApi.Hexists(hashName, key)
	if err != nil {
		infoLog.Printf("HincrISpeakToChatRecord hashName=%v key=%v redisMasterApi.Hexists return err=%s", hashName, key, err)
		return err
	}
	var valSlice []byte
	if ret == 0 { //不存在则直接设置进去
		item := &ht_chat_record.ChatRecordItem{
			FromId:     proto.Uint32(fromId),
			ToId:       proto.Uint32(toId),
			TotalCount: proto.Uint32(1),
			StartTime:  proto.Uint32(uint32(time.Now().Unix())),
			UpdateTime: proto.Uint32(uint32(time.Now().Unix())),
		}
		switch chatType {
		case ht_chat_record.E_MSG_TYPE_MT_TEXT:
			item.TextCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_TRANSLATE:
			item.TransCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_IMAGE:
			item.ImagCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_VOICE:
			item.VoiceCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_LOCATE:
			item.LocationCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_PROFILE:
			item.ProfileCount = proto.Uint32(1)
		default:
			item.OtherCount = proto.Uint32(1)
		}
		valSlice, err = proto.Marshal(item)
		if err != nil {
			infoLog.Printf("HincrISpeakToChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return err
		}
	} else { //存在则将计数加1
		value, err := redisMasterApi.Hget(hashName, key)
		if err != nil {
			infoLog.Printf("HincrISpeakToChatRecord Hget failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return err
		}
		item := new(ht_chat_record.ChatRecordItem)
		err = proto.Unmarshal([]byte(value), item)
		if err != nil {
			infoLog.Printf("HincrISpeakToChatRecord proto Unmarshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return err
		}
		item.TotalCount = proto.Uint32(item.GetTotalCount() + 1)
		item.UpdateTime = proto.Uint32(uint32(time.Now().Unix()))
		switch chatType {
		case ht_chat_record.E_MSG_TYPE_MT_TEXT:
			item.TextCount = proto.Uint32(item.GetTextCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_TRANSLATE:
			item.TransCount = proto.Uint32(item.GetTransCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_IMAGE:
			item.ImagCount = proto.Uint32(item.GetImagCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_VOICE:
			item.VoiceCount = proto.Uint32(item.GetVoiceCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_LOCATE:
			item.LocationCount = proto.Uint32(item.GetLocationCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_PROFILE:
			item.ProfileCount = proto.Uint32(item.GetProfileCount() + 1)
		default:
			item.OtherCount = proto.Uint32(item.GetOtherCount() + 1)
		}

		valSlice, err = proto.Marshal(item)
		if err != nil {
			infoLog.Printf("HincrISpeakToChatRecord proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return err
		}
	}
	err = redisMasterApi.Hset(hashName, key, string(valSlice))
	if err != nil {
		infoLog.Printf("HincrISpeakToChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
			hashName,
			fromId,
			toId,
			chatType,
			err)
		return err
	} else {
		infoLog.Printf("HincrISpeakToChatRecord hashname=%s from=%v to=%v type=%v ",
			hashName,
			fromId,
			toId,
			chatType)
		return nil
	}
}

func HincrSpeakToMeChatRecord(hashName string, fromId, toId uint32, chatType ht_chat_record.E_MSG_TYPE) (err error) {
	if len(hashName) == 0 || fromId == 0 || toId == 0 {
		err = ErrInvalidParam
		return err
	}
	key := fmt.Sprintf("%v", fromId)
	ret, err := redisMasterApi.Hexists(hashName, key)
	if err != nil {
		infoLog.Printf("HincrSpeakToMeChatRecord hashName=%v key=%v redisMasterApi.Hexists return err=%s", hashName, key, err)
		return err
	}
	var valSlice []byte
	if ret == 0 { //不存在则直接设置进去
		item := &ht_chat_record.ChatRecordItem{
			FromId:     proto.Uint32(fromId),
			ToId:       proto.Uint32(toId),
			TotalCount: proto.Uint32(1),
			StartTime:  proto.Uint32(uint32(time.Now().Unix())),
			UpdateTime: proto.Uint32(uint32(time.Now().Unix())),
		}
		switch chatType {
		case ht_chat_record.E_MSG_TYPE_MT_TEXT:
			item.TextCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_TRANSLATE:
			item.TransCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_IMAGE:
			item.ImagCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_VOICE:
			item.VoiceCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_LOCATE:
			item.LocationCount = proto.Uint32(1)
		case ht_chat_record.E_MSG_TYPE_MT_PROFILE:
			item.ProfileCount = proto.Uint32(1)
		default:
			item.OtherCount = proto.Uint32(1)
		}
		valSlice, err = proto.Marshal(item)
		if err != nil {
			infoLog.Printf("HincrSpeakToMeChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return err
		}
	} else { //存在则将计数加1
		value, err := redisMasterApi.Hget(hashName, key)
		if err != nil {
			infoLog.Printf("HincrSpeakToMeChatRecord Hget failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return err
		}
		item := new(ht_chat_record.ChatRecordItem)
		err = proto.Unmarshal([]byte(value), item)
		if err != nil {
			infoLog.Printf("HincrSpeakToMeChatRecord proto Unmarshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return err
		}
		item.TotalCount = proto.Uint32(item.GetTotalCount() + 1)
		item.UpdateTime = proto.Uint32(uint32(time.Now().Unix()))
		switch chatType {
		case ht_chat_record.E_MSG_TYPE_MT_TEXT:
			item.TextCount = proto.Uint32(item.GetTextCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_TRANSLATE:
			item.TransCount = proto.Uint32(item.GetTransCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_IMAGE:
			item.ImagCount = proto.Uint32(item.GetImagCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_VOICE:
			item.VoiceCount = proto.Uint32(item.GetVoiceCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_LOCATE:
			item.LocationCount = proto.Uint32(item.GetLocationCount() + 1)
		case ht_chat_record.E_MSG_TYPE_MT_PROFILE:
			item.ProfileCount = proto.Uint32(item.GetProfileCount() + 1)
		default:
			item.OtherCount = proto.Uint32(item.GetOtherCount() + 1)
		}

		valSlice, err = proto.Marshal(item)
		if err != nil {
			infoLog.Printf("HincrSpeakToMeChatRecord proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
				hashName,
				fromId,
				toId,
				chatType,
				err)
			return err
		}
	}
	err = redisMasterApi.Hset(hashName, key, string(valSlice))
	if err != nil {
		infoLog.Printf("HincrSpeakToMeChatRecord  proto.Marshal failed hashname=%s from=%v to=%v type=%v err=%s",
			hashName,
			fromId,
			toId,
			chatType,
			err)
		return err
	} else {
		infoLog.Printf("HincrSpeakToMeChatRecord hashname=%s from=%v to=%v type=%v ",
			hashName,
			fromId,
			toId,
			chatType)
		return nil
	}
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_chat_record.ChatRecordRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func ProcSetChatRecord(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.SetChatRecordRspbody = &ht_chat_record.SetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetChatRecord invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocragent/set_chat_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetChatRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.SetChatRecordRspbody = &ht_chat_record.SetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetChatRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSetChatRecord GetSetChatRecordReqbody() uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.SetChatRecordRspbody = &ht_chat_record.SetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 设置过程中每次都已db中的数据为准 执行覆盖写入
	itemList := subReqBody.GetItemList()
	for i, v := range itemList {
		hashName := v.GetHashName()
		keyValues := v.GetKyeValue()
		// infoLog.Printf("ProcSetChatRecord index=%v hashName=%s keyValueLen=%v", i, hashName, len(keyValues))
		if len(keyValues) == 0 {
			infoLog.Printf("ProcSetChatRecord keyValues empty hashName=%s", hashName)
			continue
		}
		err = redisMasterApi.Hmset(hashName, keyValues)
		if err != nil {
			infoLog.Printf("ProcSetChatRecord index=%v hashName=%s Hmset failed err=%s", i, hashName, err)
			attr := "gocragent/set_chat_record_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		// 设置过期时间为15天
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			// add static
			attr := "gocragent/set_key_expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcSetChatRecord redisMasterApi.Expire key=%s err=%s failed", hashName, err)
		}
	}
	rspBody.SetChatRecordRspbody = &ht_chat_record.SetChatRecordRspBody{
		Status: &ht_chat_record.ChatRecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// read ssdb config
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err = common.NewSsdbApi(ssdbHost, ssdbPort, 5, 50)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
