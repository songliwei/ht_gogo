#!/bin/bash
CURDIR=`pwd`

go build $CURDIR/base_cache_write_agent.go

cp -f $CURDIR/base_cache_write_agent  /home/ht/goproj/cache_family/user_base/base_cache_write_agent/bin/.

killall base_cache_write_agent
