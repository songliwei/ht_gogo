package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_chatrecord"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	dbdAgentApi    *common.SrvToSrvApiV2
	redisMasterApi *common.RedisApi
)

const (
	ProcSlowThreshold = 300000
	AdminUid          = 10000
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrInternalErr  = errors.New("internal err")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	// infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gocrlogic/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_chat_record.CMD_TYPE_CMD_INC_CHAT_RECORD):
		go ProcIncChatRecord(c, head, packet)
	case uint32(ht_chat_record.CMD_TYPE_CMD_GET_CHAT_RECORD):
		go ProcGetChatRecord(c, head, packet)
	case uint32(ht_chat_record.CMD_TYPE_CMD_GET_ALL_CHAT_UID):
		go ProcGetAllChatUid(c, head, packet)
	case uint32(ht_chat_record.CMD_TYPE_CMD_GET_ALL_CHAT_INFO):
		go ProcGetAllChatInfo(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
		c.Close()
	}
	return true
}

// 1.proc store moment info
func ProcIncChatRecord(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcIncChatRecord not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcIncChatRecord invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocrlogic/inc_chat_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcIncChatRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetIncChatRecordReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcIncChatRecord GetIncChatRecordReqbody() faileduid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcIncChatRecord update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gocrlogic/inc_chat_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcIncChatRecord dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gocrlogic/inc_chat_recode_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcIncChatRecord dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
		rspBody.IncChatRecordRspbody = &ht_chat_record.IncChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gocrlogic/inc_chat_record_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcIncChatRecord dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocrlogic/inc_chat_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetISpeakToHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#ispeakto", uid)
	return hashName
}

func GetSpeakToMeHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#speaktome", uid)
	return hashName
}

// 2.proc store moment info
func ProcGetChatRecord(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetChatRecord not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetChatRecord invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gocrlogic/get_chat_record_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gocrlogic/get_chat_record_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetChatRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetChatRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetChatRecord GetGetChatRecordReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	fromId := subReqBody.GetFromId()
	toIdList := subReqBody.GetToIdList()
	infoLog.Printf("ProcGetChatRecord fromId=%v toIdList=%v", fromId, toIdList)
	hashName := GetISpeakToHashMapName(fromId)
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// Step1 存在则读取完成的记录
		toIdField := make([]string, len(toIdList))
		for i, v := range toIdList {
			// infoLog.Printf("ProcGetChatRecord hashName=%s index=%v field=%v", hashName, i, v)
			toIdField[i] = fmt.Sprintf("%v", v)
		}

		val, err := redisMasterApi.Hmget(hashName, toIdField)
		if err != nil {
			infoLog.Printf("ProcGetChatRecord hashName=%s hmget return err=%s", hashName, err)
			result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_REDIS_ERR)
			rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
				Status: &ht_chat_record.ChatRecordHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis error"),
				},
			}
			return false
		}

		var ackList []*ht_chat_record.ChatRecordRspItem
		for _, v := range val {
			// infoLog.Printf("ProcGetChatRecord index=%v value=%v", i, v)
			if len(v) == 0 {
				// infoLog.Printf("ProcGetChatRecord hmget index=%v value=%v", i, v)
				continue
			}
			recordItem := new(ht_chat_record.ChatRecordItem)
			err := proto.Unmarshal([]byte(v), recordItem)
			if err != nil {
				infoLog.Printf("ProcGetChatRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
				continue
			}
			if recordItem.GetFromId() != fromId {
				infoLog.Printf("ProcGetChatRecord recordItem.GetFromId()=%v fromId=%v", recordItem.GetFromId(), fromId)
				continue
			}
			iterm := &ht_chat_record.ChatRecordRspItem{
				ToId:       proto.Uint32(recordItem.GetToId()),
				TotalCount: proto.Uint32(recordItem.GetTotalCount()),
				StartTime:  proto.Uint32(recordItem.GetStartTime()),
			}
			ackList = append(ackList, iterm)
		}
		rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			AckList: ackList,
		}

		procTime := packet.CalcProcessTime()
		if procTime > ProcSlowThreshold {
			// add static
			attr := "gocrlogic/get_chat_record_proc_slow"
			libcomm.AttrAdd(attr, 1)
		}
		return true
	} else {
		infoLog.Printf("ProcGetChatRecord redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "gocrlogic/get_chat_record_cache_miss"
		libcomm.AttrAdd(attr, 1)
		// Step2 读取Master没有命中则继续读取dbd
		dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
		if err != nil {
			infoLog.Printf("ProcGetChatRecord cache miss reload from db failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
			rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
				Status: &ht_chat_record.ChatRecordHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gocrlogic/get_account_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
		if !ok { // 不是HeadV2Packet报文
			infoLog.Printf("ProcGetChatRecord dbpacket can not change to HeadV2packet uid=%v", head.Uid)
			result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
			rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
				Status: &ht_chat_record.ChatRecordHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gocrlogic/get_chat_record_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbRespHead, err := dbPacketHeadV2.GetHead()
		if err != nil {
			//SendResp(c, head, uint16(ERR_INVALID_PARAM))
			infoLog.Printf("ProcGetChatRecord dbpacket Get head failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
			rspBody.GetChatRecordRspbody = &ht_chat_record.GetChatRecordRspBody{
				Status: &ht_chat_record.ChatRecordHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gocrlogic/get_chat_record_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		infoLog.Printf("ProcGetChatRecord dbpacket uid=%v ret=%v", fromId, dbRespHead.Ret)
		// Step3 读取dbd之后立即返回响应
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
		bNeedCall = false
		SendRspPacket(c, dbPacketHeadV2)

		procTime := packet.CalcProcessTime()
		if procTime > ProcSlowThreshold {
			// add static
			attr := "gocrlogic/get_chat_record_proc_slow"
			libcomm.AttrAdd(attr, 1)
		}
	}
	return true
}

// 3.proc get all chat uid
func ProcGetAllChatUid(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetAllChatUid not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.GetAllChatUidRspbody = &ht_chat_record.GetAllChatUidRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetAllChatUid invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gocrlogic/get_all_chat_uid_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gocrlogic/get_all_chat_uid_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetAllChatUid proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetAllChatUidRspbody = &ht_chat_record.GetAllChatUidRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetAllChatUidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetAllChatUid GetGetChatRecordReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetAllChatUidRspbody = &ht_chat_record.GetAllChatUidRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	fromId := subReqBody.GetFromId()
	infoLog.Printf("ProcGetAllChatUid fromId=%v", fromId)
	iSpeakToHash := GetISpeakToHashMapName(fromId)
	iSpeakTo, err := GetAllChatUid(head, fromId, iSpeakToHash)
	if err != nil {
		infoLog.Printf("ProcGetAllChatUid GetAllChatUid() failed uid=%v hashName=%s err=%s", fromId, iSpeakToHash, err)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
		rspBody.GetAllChatUidRspbody = &ht_chat_record.GetAllChatUidRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	speakToMeHash := GetSpeakToMeHashMapName(fromId)
	speakToMe, err := GetAllChatUid(head, fromId, speakToMeHash)
	if err != nil {
		infoLog.Printf("ProcGetAllChatUid GetAllChatUid() failed uid=%v hashName=%s err=%s", fromId, iSpeakToHash, err)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
		rspBody.GetAllChatUidRspbody = &ht_chat_record.GetAllChatUidRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody.GetAllChatUidRspbody = &ht_chat_record.GetAllChatUidRspBody{
		Status: &ht_chat_record.ChatRecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ISpeakToSimple: iSpeakTo,
		SSpeakMeSimple: speakToMe,
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocrlogic/get_all_chat_uid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetAllChatUid(head *common.HeadV2, fromId uint32, hashName string) (uidCount []*ht_chat_record.ChatRecordRspItem, err error) {
	if head == nil || fromId == 0 || len(hashName) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetAllChatUid uid=%v hashName=%s invalid param", fromId, hashName)
		return nil, err
	}
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// Step1 存在则读取完成的记录
		keyValue, err := redisMasterApi.Hgetall(hashName)
		if err != nil {
			infoLog.Printf("GetAllChatUid hashName=%s Hgetall return err=%s", hashName, err)
			return nil, err
		}

		for i, v := range keyValue {
			// infoLog.Printf("ProcGetChatRecord index=%v value=%v", i, v)
			if len(v) == 0 {
				infoLog.Printf("GetAllChatUid hgetall key=%s value len zero", i)
				continue
			}
			targetUid, err := strconv.ParseUint(i, 10, 32)
			if err != nil {
				infoLog.Printf("GetAllChatUid uid=%v valud=%s strvonv.PraseUint err=%s", fromId, i, err)
				continue
			}
			recordItem := new(ht_chat_record.ChatRecordItem)
			err = proto.Unmarshal([]byte(v), recordItem)
			if err != nil {
				infoLog.Printf("GetAllChatUid proto Unmarshal failed uid=%v", fromId)
				continue
			}
			if head.Uid != AdminUid && recordItem.GetFromId() != fromId && recordItem.GetToId() != fromId {
				infoLog.Printf("GetAllChatUid recordItem.GetFromId()=%v uid=%v", recordItem.GetFromId(), fromId)
				continue
			}
			iterm := &ht_chat_record.ChatRecordRspItem{
				ToId:       proto.Uint32(uint32(targetUid)),
				TotalCount: proto.Uint32(recordItem.GetTotalCount()),
				StartTime:  proto.Uint32(recordItem.GetStartTime()),
			}
			uidCount = append(uidCount, iterm)
		}
		return uidCount, nil
	} else {
		infoLog.Printf("GetAllChatUid redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "gocrlogic/get_all_chat_uid_cache_miss"
		libcomm.AttrAdd(attr, 1)
		reqHead := new(common.HeadV2)
		*reqHead = *head
		reqHead.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_GET_HAHS_LIST)
		uidCount, err := GetAllChatUidFromDbd(reqHead, fromId, hashName)
		if err != nil {
			infoLog.Printf("GetAllChatUid uid=%v hashName=%s err=%s", fromId, hashName, err)
			return nil, err
		}
		return uidCount, nil
	}
}

func GetAllChatUidFromDbd(head *common.HeadV2, fromId uint32, hashName string) (uidCount []*ht_chat_record.ChatRecordRspItem, err error) {
	if head == nil || fromId == 0 || len(hashName) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetAllChatUidFromDbd uid=%v hashName=%s invalid param", fromId, hashName)
		return nil, err
	}
	keyValues, err := GetHashMapFromDbd(head, hashName)
	if err != nil {
		infoLog.Printf("GetAllChatUidFromDbd uid=%v hashName=%s err=%s", fromId, hashName, err)
		return nil, err
	}
	for i := 0; i < len(keyValues); i += 2 {
		targetUid, err := strconv.ParseUint(keyValues[i], 10, 32)
		if err != nil {
			infoLog.Printf("GetAllChatUidFromDbd uid=%v valud=%s strvonv.PraseUint err=%s", head.Uid, keyValues[i], err)
			continue
		}
		recordItem := new(ht_chat_record.ChatRecordItem)
		err = proto.Unmarshal([]byte(keyValues[i+1]), recordItem)
		if err != nil {
			infoLog.Printf("GetAllChatUidFromDbd proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
			continue
		}
		if head.Uid != AdminUid && recordItem.GetFromId() != head.Uid && recordItem.GetToId() != head.Uid {
			infoLog.Printf("ProcGetChatRecord recordItem.GetFromId()=%v recordItem.GetToId()=%v uid=%v", recordItem.GetFromId(), recordItem.GetToId(), head.Uid)
			continue
		}
		iterm := &ht_chat_record.ChatRecordRspItem{
			ToId:       proto.Uint32(uint32(targetUid)),
			TotalCount: proto.Uint32(recordItem.GetTotalCount()),
			StartTime:  proto.Uint32(recordItem.GetStartTime()),
		}
		uidCount = append(uidCount, iterm)
	}
	return uidCount, nil
}

func GetHashMapFromDbd(head *common.HeadV2, hashName string) (keyValues []string, err error) {
	if head == nil || len(hashName) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetHashMapFromDbd uid=%v hashName=%s invalid param", head.Uid, hashName)
		return nil, err
	}
	// Step2 读取Master没有命中则继续读取dbd
	reqhead := new(common.HeadV2)
	if head != nil {
		*reqhead = *head
	}
	reqhead.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_GET_HAHS_LIST)
	reqBody := &ht_chat_record.ChatRecordReqBody{
		GetHashMapReqbody: &ht_chat_record.GetHashMapReqBody{
			HashNamdList: []string{
				hashName,
			},
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("GetHashMapFromDbd proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gocrlogic/get_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return nil, err
	}
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		infoLog.Printf("GetHashMapFromDbd cache miss reload from db failed uid=%v err=%s", head.Uid, err)
		attr := "gocrlogic/get_hash_map_dbd_err"
		libcomm.AttrAdd(attr, 1)
		return nil, err
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("GetHashMapFromDbd dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		attr := "gocrlogic/get_hash_map_inter_err"
		libcomm.AttrAdd(attr, 1)
		err = ErrInternalErr
		return nil, err
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("GetHashMapFromDbd dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		attr := "gocrlogic/get_hash_map_inter_err"
		libcomm.AttrAdd(attr, 1)
		return nil, err
	}
	if dbRespHead.Ret != uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS) {
		infoLog.Printf("GetHashMapFromDbd dbpaket ret=%v hashmap=%s uid=%v", dbRespHead.Ret, hashName, head.Uid)
		err = ErrInternalErr
		return nil, err
	}

	rspBody := &ht_chat_record.ChatRecordRspBody{}
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetGetHashMapRspbody()
	if subRspBody == nil {
		err = ErrInternalErr
		return nil, err
	}
	if len(subRspBody.GetHashMapList()) < 1 {
		infoLog.Printf("GetHashMapFromDbd uid=%v hashName=%s result empty", head.Uid, hashName)
		err = ErrInternalErr
		return nil, err
	}
	item := (subRspBody.GetHashMapList())[0]
	if item.GetHashName() != hashName {
		infoLog.Printf("GetHashMapFromDbd uid=%v reqHashName=%s rspHashName=%s not equal", head.Uid, hashName, item.GetHashName())
		err = ErrInternalErr
		return nil, err
	}
	keyValues = item.GetKeyValues()
	return keyValues, nil
}

// 4.proc get all chat info
func ProcGetAllChatInfo(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody := new(ht_chat_record.ChatRecordRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetAllChatInfo not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INVALID_PARAM)
		rspBody.GetAllChatInfoRspbody = &ht_chat_record.GetAllChatInfoRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetAllChatInfo invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gocrlogic/get_chat_record_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gocrlogic/get_all_chat_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetAllChatInfo proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetAllChatInfoRspbody = &ht_chat_record.GetAllChatInfoRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetAllChatInfoReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetAllChatInfo GetGetChatRecordReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_PB_ERR)
		rspBody.GetAllChatInfoRspbody = &ht_chat_record.GetAllChatInfoRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	fromId := subReqBody.GetFromId()
	infoLog.Printf("ProcGetAllChatInfo fromId=%v ", fromId)
	iSpeakToHash := GetISpeakToHashMapName(fromId)
	iSpeakTo, err := GetAllChatInfo(head, fromId, iSpeakToHash)
	if err != nil {
		infoLog.Printf("ProcGetAllChatInfo GetAllChatInfo() failed uid=%v hashName=%s err=%s", fromId, iSpeakToHash, err)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
		rspBody.GetAllChatInfoRspbody = &ht_chat_record.GetAllChatInfoRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	speakToMeHash := GetSpeakToMeHashMapName(fromId)
	speakToMe, err := GetAllChatInfo(head, fromId, speakToMeHash)
	if err != nil {
		infoLog.Printf("ProcGetAllChatInfo GetAllChatInfo() failed uid=%v hashName=%s err=%s", fromId, speakToMeHash, err)
		result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_INTERNAL_ERR)
		rspBody.GetAllChatInfoRspbody = &ht_chat_record.GetAllChatInfoRspBody{
			Status: &ht_chat_record.ChatRecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	result = uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS)
	rspBody.GetAllChatInfoRspbody = &ht_chat_record.GetAllChatInfoRspBody{
		Status: &ht_chat_record.ChatRecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ISpeakToDetail: iSpeakTo,
		SSpeakMeDetail: speakToMe,
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocrlogic/get_all_chat_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetAllChatInfo(head *common.HeadV2, fromId uint32, hashName string) (uidInfo []*ht_chat_record.ChatRecordItem, err error) {
	if head == nil || fromId == 0 || len(hashName) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetAllChatInfo uid=%v hashName=%s invalid param", fromId, hashName)
		return nil, err
	}
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// Step1 存在则读取完成的记录
		keyValue, err := redisMasterApi.Hgetall(hashName)
		if err != nil {
			infoLog.Printf("GetAllChatInfo hashName=%s Hgetall return err=%s", hashName, err)
			return nil, err
		}

		for i, v := range keyValue {
			// infoLog.Printf("ProcGetChatRecord index=%v value=%v", i, v)
			if len(v) == 0 {
				infoLog.Printf("GetAllChatInfo hgetall key=%s value len zero", i)
				continue
			}
			// targetUid, err := strconv.ParseUint(i, 10, 32)
			// if err != nil {
			// 	infoLog.Printf("GetAllChatInfo uid=%v valud=%s strvonv.PraseUint err=%s", fromId, i, err)
			// 	continue
			// }
			recordItem := new(ht_chat_record.ChatRecordItem)
			err = proto.Unmarshal([]byte(v), recordItem)
			if err != nil {
				infoLog.Printf("GetAllChatUid proto Unmarshal failed uid=%v", fromId)
				continue
			}
			if head.Uid != AdminUid && recordItem.GetFromId() != fromId && recordItem.GetToId() != fromId {
				infoLog.Printf("GetAllChatUid recordItem.GetFromId()=%v uid=%v", recordItem.GetFromId(), fromId)
				continue
			}
			uidInfo = append(uidInfo, recordItem)
		}
		return uidInfo, nil
	} else {
		infoLog.Printf("GetAllChatInfo redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "gocrlogic/get_all_chat_info_cache_miss"
		libcomm.AttrAdd(attr, 1)
		reqHead := new(common.HeadV2)
		*reqHead = *head
		reqHead.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_GET_HAHS_LIST)
		uidInfo, err := GetAllChatInfoFromDbd(reqHead, fromId, hashName)
		if err != nil {
			infoLog.Printf("GetAllChatInfo uid=%v hashName=%s err=%s", fromId, hashName, err)
			return nil, err
		}
		return uidInfo, nil
	}
}

func GetAllChatInfoFromDbd(head *common.HeadV2, fromId uint32, hashName string) (uidInfo []*ht_chat_record.ChatRecordItem, err error) {
	if head == nil || fromId == 0 || len(hashName) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("GetAllChatInfoFromDbd uid=%v hashName=%s invalid param", fromId, hashName)
		return nil, err
	}

	keyValues, err := GetHashMapFromDbd(head, hashName)
	if err != nil {
		infoLog.Printf("GetAllChatInfoFromDbd uid=%v hashName=%s err=%s", fromId, hashName, err)
		return nil, err
	}
	for i := 0; i < len(keyValues); i += 2 {
		// targetUid, err := strconv.ParseUint(keyValues[i], 10, 32)
		// if err != nil {
		// 	infoLog.Printf("GetAllChatInfoFromDbd uid=%v valud=%s strvonv.PraseUint err=%s", head.Uid, keyValues[i], err)
		// 	continue
		// }
		recordItem := new(ht_chat_record.ChatRecordItem)
		err = proto.Unmarshal([]byte(keyValues[i+1]), recordItem)
		if err != nil {
			infoLog.Printf("GetAllChatInfoFromDbd proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
			continue
		}
		if head.Uid != AdminUid && recordItem.GetFromId() != head.Uid && recordItem.GetToId() != head.Uid {
			infoLog.Printf("GetAllChatInfoFromDbd recordItem.GetFromId()=%v recordItem.GetToId()=%v uid=%v", recordItem.GetFromId(), recordItem.GetToId(), head.Uid)
			continue
		}
		uidInfo = append(uidInfo, recordItem)
	}
	return uidInfo, nil
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_chat_record.ChatRecordRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustString("6379")
	infoLog.Printf("cache dbd ip=%v port=%v", dbdIp, dbdPort)
	dbdAgentApi = common.NewSrvToSrvApiV2(dbdIp, dbdPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
