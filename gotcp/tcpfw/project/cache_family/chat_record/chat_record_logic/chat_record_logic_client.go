package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_chatrecord"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	Cmd int `short:"t" long:"cmd" description:"Command type" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	v2Protocol := &common.HeadV2Protocol{}
	var head *common.HeadV2
	head = &common.HeadV2{
		Version:  4,
		Cmd:      1,
		Seq:      1,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      7843689,
	}

	var payLoad []byte
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	switch options.Cmd {
	// 获取用户基本信息
	case 1:
		head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_INC_CHAT_RECORD)
		subReqBody := &ht_chat_record.IncChatRecordReqBody{
			ChatItems: []*ht_chat_record.IncChatRecordItem{
				&ht_chat_record.IncChatRecordItem{
					FromId: proto.Uint32(8945494),
					ToId:   proto.Uint32(15003),
					Type:   ht_chat_record.E_MSG_TYPE_MT_TEXT.Enum(),
				},
			},
		}
		reqBody.IncChatRecordReqbody = subReqBody
	case 2:
		head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_GET_CHAT_RECORD)
		subReqBody := &ht_chat_record.GetChatRecordReqBody{
			FromId: proto.Uint32(2847240),
			ToIdList: []uint32{
				3529222,
			},
		}
		reqBody.GetChatRecordReqbody = subReqBody
	case 4:
		head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_GET_ALL_CHAT_UID)
		subReqBody := &ht_chat_record.GetAllChatUidReqBody{
			FromId: proto.Uint32(1946612),
		}
		reqBody.GetAllChatUidReqbody = subReqBody
	case 5:
		head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_GET_ALL_CHAT_INFO)
		subReqBody := &ht_chat_record.GetAllChatInfoReqBody{
			FromId: proto.Uint32(8945494),
		}
		reqBody.GetAllChatInfoReqbody = subReqBody
	default:
		infoLog.Println("UnKnow input cmd =", options.Cmd)
	}

	payLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v cmd=%v seq=%v",
			head.Uid,
			head.Cmd,
			head.Seq)
		return
	}

	head.Len = uint32(common.PacketV2HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV2ToSlice failed")
		return
	}
	copy(buf[common.PacketV2HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV2MagicEnd

	infoLog.Printf("len=%v payLaod=%v\n", len(payLoad), payLoad)
	// write
	conn.Write(buf)
	// read
	p, err := v2Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket, ok := p.(*common.HeadV2Packet)
		if !ok { // 不是HeadV3Packet报文
			infoLog.Printf("packet can not change to HeadV2packet")
			return
		}
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("resp len=%v cmd=%v uid=%v\n", rspHead.Len, rspHead.Cmd, rspHead.Uid)
		rspBody := &ht_chat_record.ChatRecordRspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Println("proto Unmarshal failed")
			return
		}
		switch rspHead.Cmd {
		case 1:
			subRspBody := rspBody.GetIncChatRecordRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetIncChatRecordRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 2:
			subRspBody := rspBody.GetGetChatRecordRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetGetChatRecordRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
			ackList := subRspBody.GetAckList()
			for i, v := range ackList {
				infoLog.Printf("GetGetChatRecordRspbody index=%v toId=%v total=%v startTime=%v",
					i,
					v.GetToId(),
					v.GetTotalCount(),
					v.GetStartTime())
			}
		case 4:
			subRspBody := rspBody.GetGetAllChatUidRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetGetAllChatUidRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
			iSpeakToSimple := subRspBody.GetISpeakToSimple()
			for i, v := range iSpeakToSimple {
				infoLog.Printf("GetGetAllChatUidRspbody iSpeakTo index=%v toId=%v total=%v",
					i,
					v.GetToId(),
					v.GetTotalCount())
			}

			speakToMeSimple := subRspBody.GetSSpeakMeSimple()
			for i, v := range speakToMeSimple {
				infoLog.Printf("GetGetAllChatUidRspbody sSpeakToMe index=%v toId=%v total=%v",
					i,
					v.GetToId(),
					v.GetTotalCount())
			}
		case 5:
			subRspBody := rspBody.GetGetAllChatInfoRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetGetAllChatInfoRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
			iSpeakTo := subRspBody.GetISpeakToDetail()
			for i, v := range iSpeakTo {
				infoLog.Printf("GetGetAllChatInfoRspbody iSpeakTo index=%v from=%v to=%v text=%v tran=%v image=%v total=%v",
					i,
					v.GetFromId(),
					v.GetToId(),
					v.GetTextCount(),
					v.GetTransCount(),
					v.GetImagCount(),
					v.GetTotalCount())
			}

			speakToMe := subRspBody.GetSSpeakMeDetail()
			for i, v := range speakToMe {
				infoLog.Printf("GetGetAllChatInfoRspbody speakToMe index=%v from=%v to=%v text=%v tran=%v image=%v total=%v",
					i,
					v.GetFromId(),
					v.GetToId(),
					v.GetTextCount(),
					v.GetTransCount(),
					v.GetImagCount(),
					v.GetTotalCount())
			}

		default:
			infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
		}

	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
