package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"bufio"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_chatrecord"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	RestoreFile string `short:"r" long:"restore" description:"restore uid list file" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" || options.RestoreFile == "" {
		log.Fatalln("Must input config file name")
	}

	// 读取 需要恢复计数的uid列表文件
	fi, err := os.Open(options.RestoreFile)
	if err != nil {
		log.Printf("Error: %s\n", err)
		return
	}
	defer fi.Close()
	br := bufio.NewReader(fi)

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)

	v2Protocol := &common.HeadV2Protocol{}
	var head *common.HeadV2
	head = &common.HeadV2{
		Version:  4,
		Cmd:      1,
		Seq:      1,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      7843689,
	}

	var payLoad []byte
	reqBody := new(ht_chat_record.ChatRecordReqBody)
	head.Cmd = uint32(ht_chat_record.CMD_TYPE_CMD_INC_CHAT_RECORD)
	//for i := 0; i < 1; i += 1 {
	i := 0
	for {
		i += 1
		if i%100 == 0 {
			time.Sleep(1 * time.Second)
		}
		newLine, _, err := br.ReadLine()
		if err == io.EOF {
			break
		}
		strFromAndTo := strings.Split(string(newLine), ",")
		from, _ := strconv.ParseUint(strFromAndTo[0], 10, 64)
		if err != nil {
			infoLog.Printf("strconv.ParseUint from=%s err=%s", strFromAndTo[0], err)
			continue
		}
		to, _ := strconv.ParseUint(strFromAndTo[1], 10, 64)
		if err != nil {
			infoLog.Printf("strconv.ParseUint to=%s err=%s", strFromAndTo[1], err)
			continue
		}
		infoLog.Printf("from=%v to=%v", from, to)
		head.Uid = uint32(from)
		subReqBody := &ht_chat_record.IncChatRecordReqBody{
			ChatItems: []*ht_chat_record.IncChatRecordItem{
				&ht_chat_record.IncChatRecordItem{
					FromId: proto.Uint32(uint32(from)),
					ToId:   proto.Uint32(uint32(to)),
					Type:   ht_chat_record.E_MSG_TYPE_MT_TEXT.Enum(),
				},
			},
		}
		reqBody.IncChatRecordReqbody = subReqBody

		payLoad, err = proto.Marshal(reqBody)
		if err != nil {
			infoLog.Printf("proto.Marshal failed uid=%v cmd=%v seq=%v",
				head.Uid,
				head.Cmd,
				head.Seq)
			continue
		}

		head.Len = uint32(common.PacketV2HeadLen + len(payLoad) + 1) //整个报文长度
		buf := make([]byte, head.Len)
		buf[0] = common.HTV2MagicBegin
		err = common.SerialHeadV2ToSlice(head, buf[1:])
		if err != nil {
			infoLog.Println("SerialHeadV2ToSlice failed")
			continue
		}
		copy(buf[common.PacketV2HeadLen:], payLoad) // return code
		buf[head.Len-1] = common.HTV2MagicEnd

		infoLog.Printf("len=%v payLaod=%v\n", len(payLoad), payLoad)
		// write
		conn.Write(buf)
		// read
		p, err := v2Protocol.ReadPacket(conn)
		if err == nil {
			rspPacket, ok := p.(*common.HeadV2Packet)
			if !ok { // 不是HeadV3Packet报文
				infoLog.Printf("packet can not change to HeadV2packet")
				continue
			}
			rspHead, _ := rspPacket.GetHead()
			rspPayLoad := rspPacket.GetBody()
			infoLog.Printf("resp len=%v cmd=%v uid=%v\n", rspHead.Len, rspHead.Cmd, rspHead.Uid)
			rspBody := &ht_chat_record.ChatRecordRspBody{}
			err = proto.Unmarshal(rspPayLoad, rspBody)
			if err != nil {
				infoLog.Println("proto Unmarshal failed")
				continue
			}
			switch rspHead.Cmd {
			case 1:
				subRspBody := rspBody.GetIncChatRecordRspbody()
				status := subRspBody.GetStatus()
				infoLog.Printf("GetIncChatRecordRspbody rsp code=%v msg=%s",
					status.GetCode(),
					status.GetReason())

			default:
				infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
			}
		} else {
			conn.Close()
			conn, err = net.DialTCP("tcp", nil, tcpAddr)
			checkError(err)
		}
	}
	conn.Close()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
