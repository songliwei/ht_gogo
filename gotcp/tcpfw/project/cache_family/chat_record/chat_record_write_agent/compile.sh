#!/bin/bash
CURDIR=`pwd`

go build $CURDIR/chat_record_write_agent.go

cp -f $CURDIR/chat_record_write_agent /home/ht/goproj/cache_family/chat_record/write_agent/bin/.

killall chat_record_write_agent
