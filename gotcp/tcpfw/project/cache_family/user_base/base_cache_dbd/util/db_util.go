// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_basecache"
	_ "github.com/go-sql-driver/mysql"
)

// Error type
var (
	ErrNilDbObject = errors.New("not set  object current is nil")
	ErrDbParam     = errors.New("err param error")
)

const (
	PMC_SEX         = 0  // 性别
	PMC_BIRTHDAY    = 1  // 生日
	PMC_NATIONALITY = 2  // 国籍检查
	PMC_NICK        = 3  // 用户昵称
	PMC_FULLPY      = 4  // 名称全拼
	PMC_SHORTPY     = 5  // 名称简拼
	PMC_TEXT        = 6  // 文本自我介绍
	PMC_VOICE       = 7  // 语音自我介绍
	PMC_HEADURL     = 8  // 个人头像URL
	PMC_NATIVELANG  = 9  // 用户母语
	PMC_LEARN1      = 10 //
	PMC_LEARN2      = 11 //
	PMC_LEARN3      = 12 //
	PMC_LEARN4      = 13 //
	PMC_TEACH2      = 14 // for multilanguage. The second teach language(NATIVE is the first)
	PMC_TEACH3      = 15
	PMC_COUNT       = 16
)

type BaseInfoStore struct {
	// HT_USER_BASE
	UserName      string //用户名
	UserType      uint32 //用户类别：0-普通用户
	UserNickName  string //用户别名 昵称
	Fullpy        string //用户别名的拼音
	ShortFullpy   string //用户别名的拼音缩写
	Sex           uint32 //性别
	Birthday      string //生日
	Signature     string //签名
	HeadUrl       string //用户头像URL
	VoiceUrl      string //用户语音介绍的URL
	VoiceDuration uint32 //语音介绍录音的时长
	Nationality   string //用户国籍英文简称
	TimeZone      uint8  //用户所在时区(24时区制)
	SummerTime    uint8  //夏令制设置
	TimeZone48    uint8  //用户时区(半时区为单位 -24 到 24)

	// HT_USER_LANGUAGE
	AllowCount      uint32 //可以教学的语言数量(使用权限)
	NativeLang      uint32 //母语 语言语种使用数字代表
	TeachLang2      uint32 //教学语言二
	TeachLang2Level uint32 //教学语言二等级
	TeachLang3      uint32 //教学语言三
	TeachLang3Level uint32 //教学语言三等级
	LearnLang1      uint32 //学习语言一
	LearnLang1Level uint32 //学习语言一等级
	LearnLang2      uint32 //学习语言二
	LearnLang2Level uint32 //学习语言二等级
	LearnLang3      uint32 //学习语言三
	LearnLang3Level uint32 //学习语言三等级
	LearnLang4      uint32 //学习语言四
	LearnLang4Level uint32 //学习语言四等级

	// HT_USER_PRIVATE
	HideLocation uint32 //隐藏定位信息
	HideCity     uint32 //隐藏城市信息
	HideAge      uint32 //隐藏年龄信息
	HideOnline   uint32 //隐藏在线状态

	// HT_USER_PROPERTY PROFILEVERSION
	ProfileVer       uint64 //用户资料版本时间戳
	FriendVer        uint64 //用户好友版本时间戳
	BlackListVer     uint64 //黑名单版本时间戳
	LocationVer      uint64 //用户定位信息版本时间戳
	InviteVer        uint64 //好友邀请版本戳
	LastGetOffMsgVer int64  //最后成功获取的离线时间戳
	HeadModifyVer    int64  //头像最后修改时间
	LastLessonVer    int64  //更新创建课程最后的时间戳
	WalletVer        int64  //发起课程收费之后的展示个人钱包入口
}

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

func (this *DbUtil) GetBaseInfoFromDb(uid uint32) (baseInfo *BaseInfoStore, err error) {
	if this.db == nil {
		return nil, ErrNilDbObject
	}

	var storeUserName, storeUserNickName, storeFullpy, storeBirthday, storeShortFullpy sql.NullString
	var storeUserType, storeSex, storeVoiceDuration, storeTimeZone, storeSummerTime, storeTimeZone48 sql.NullInt64
	var storeSignature, storeHeadUrl, storeVoiceUrl, storeNationality sql.NullString
	err = this.db.QueryRow("SELECT USERNAME, TYPE, NICKNAME, FULLPY, SHORTPY, SEX, BIRTHDAY, SIGNATURE, HEADURL, VOICEURL, "+
		"VOICEDURATION, NATIONALITY, TIMEZONE, SUMMERTIME ,TIMEZONE48 FROM HT_USER_BASE where USERID=?;",
		uid).Scan(&storeUserName, &storeUserType, &storeUserNickName, &storeFullpy, &storeShortFullpy,
		&storeSex, &storeBirthday, &storeSignature, &storeHeadUrl, &storeVoiceUrl,
		&storeVoiceDuration, &storeNationality, &storeTimeZone, &storeSummerTime, &storeTimeZone48)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetBaseInfoFromDb not found uid=%v in HT_USER_BASE err=%s", uid, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetBaseInfoFromDb exec HT_USER_BASE failed [uid=%v, err=%s]", uid, err)
		return nil, err
	default:
	}

	var userName, nickName, fullpy, birtyday, shortFullpy string
	var userType, sex, voiceDuration uint32
	var timeZone, summerTime, timeZone48 uint8
	var signature, headUrl, voiceUrl, nationality string

	if storeUserName.Valid {
		userName = storeUserName.String
	}

	if storeUserType.Valid {
		userType = uint32(storeUserType.Int64)
	}

	if storeUserNickName.Valid {
		nickName = storeUserNickName.String
	}

	if storeFullpy.Valid {
		fullpy = storeFullpy.String
	}

	if storeShortFullpy.Valid {
		shortFullpy = storeShortFullpy.String
	}

	if storeSex.Valid {
		sex = uint32(storeSex.Int64)
	}

	if storeBirthday.Valid {
		birtyday = storeBirthday.String
	}

	if storeSignature.Valid {
		signature = storeSignature.String
	}

	if storeHeadUrl.Valid {
		headUrl = storeHeadUrl.String
	}

	if storeVoiceUrl.Valid {
		voiceUrl = storeVoiceUrl.String
	}

	if storeVoiceDuration.Valid {
		voiceDuration = uint32(storeVoiceDuration.Int64)
	}

	if storeNationality.Valid {
		nationality = storeNationality.String
	}

	if storeTimeZone.Valid {
		timeZone = uint8(storeTimeZone.Int64)
	}

	if storeSummerTime.Valid {
		summerTime = uint8(storeSummerTime.Int64)
	}

	if storeTimeZone48.Valid {
		timeZone48 = uint8(storeTimeZone48.Int64)
	}

	// HT_USER_LANGUAGE
	var storeAllocCount, storeNativ, storeTeach2, storeTeach2Level, storeTeach3, storeTeach3Level, storeLearn1, storeLearn1Level, storeLearn2, storeLearn2Level, storeLearn3, storeLearn3Level, storeLearn4, storeLearn4Level sql.NullInt64
	err = this.db.QueryRow("SELECT ALLOWCOUNT, NATIVELANG, TEACHLANG2, TEACHSKILLLEVEL2, TEACHLANG3, TEACHSKILLLEVEL3, LEARNLANG1, SKILLLEVEL1, LEARNLANG2, SKILLLEVEL2, LEARNLANG3, SKILLLEVEL3, LEARNLANG4, SKILLLEVEL4 FROM HT_USER_LANGUAGE WHERE USERID=?;",
		uid).Scan(&storeAllocCount, &storeNativ, &storeTeach2, &storeTeach2Level, &storeTeach3, &storeTeach3Level, &storeLearn1, &storeLearn1Level, &storeLearn2, &storeLearn2Level, &storeLearn3, &storeLearn3Level, &storeLearn4, &storeLearn4Level)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetBaseInfoFromDb not found uid=%v in HT_USER_LANGUAGE err=%s", uid, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetBaseInfoFromDb exec failed HT_USER_LANGUAGE [uid=%v, err=%s] =", uid, err)
		return nil, err
	default:
	}

	var allocCount, native, teach2, teach2Level, teach3, teach3Level, learn1, learn1Level, learn2, learn2Level, learn3, learn3Level, learn4, learn4Level uint32
	if storeAllocCount.Valid {
		allocCount = uint32(storeAllocCount.Int64)
	}
	if storeNativ.Valid {
		native = uint32(storeNativ.Int64)
	}
	if storeTeach2.Valid {
		teach2 = uint32(storeTeach2.Int64)
	}
	if storeTeach2Level.Valid {
		teach2Level = uint32(storeTeach2Level.Int64)
	}
	if storeTeach3.Valid {
		teach3 = uint32(storeTeach3.Int64)
	}
	if storeTeach3Level.Valid {
		teach3Level = uint32(storeTeach3Level.Int64)
	}
	if storeLearn1.Valid {
		learn1 = uint32(storeLearn1.Int64)
	}
	if storeLearn1Level.Valid {
		learn1Level = uint32(storeLearn1Level.Int64)
	}
	if storeLearn2.Valid {
		learn2 = uint32(storeLearn2.Int64)
	}
	if storeLearn2Level.Valid {
		learn2Level = uint32(storeLearn2Level.Int64)
	}
	if storeLearn3.Valid {
		learn3 = uint32(storeLearn3.Int64)
	}
	if storeLearn3Level.Valid {
		learn3Level = uint32(storeLearn3Level.Int64)
	}
	if storeLearn4.Valid {
		learn4 = uint32(storeLearn4.Int64)
	}
	if storeLearn4Level.Valid {
		learn4Level = uint32(storeLearn4Level.Int64)
	}

	// HT_USER_PROPERTY
	var storeProfileVer, storeFriendVer, storeBlackListVer, storeLocationVer, storeInviteVer, storeLastGetOffMsgVer, storeHeadModifyVer, storeLessonVer, storeWalletVer sql.NullInt64
	var storeHideLocation, storeHideCity, storeHideAge, storeHideOnline sql.NullInt64
	err = this.db.QueryRow("SELECT t1.PROFILEVERSION, t1.FRIENDVERSION, t1.BLACKLISTVERSION, t1.LOCATIONVERSION, t1.INVITEVERSION, UNIX_TIMESTAMP(t1.TSOFFLINEMESSAGE), UNIX_TIMESTAMP(t1.HEADMODTIME), t1.LAST_LESSION_UPDATE, t1.LAST_WALLET_VER, t2.HIDELOCATION, t2.HIDECITY, t2.HIDEAGE, t2.HIDEONLINE FROM HT_USER_PROPERTY AS t1 LEFT JOIN (HT_USER_PRIVATE AS t2) ON t1.USERID = t2.USERID WHERE t1.USERID=?;",
		uid).Scan(&storeProfileVer, &storeFriendVer, &storeBlackListVer, &storeLocationVer, &storeInviteVer, &storeLastGetOffMsgVer, &storeHeadModifyVer, &storeLessonVer, &storeWalletVer, &storeHideLocation, &storeHideCity, &storeHideAge, &storeHideOnline)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetBaseInfoFromDb not found uid=%v in HT_USER_PROPERTY err=%s", uid, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetBaseInfoFromDb exec failed HT_USER_PROPERTY [uid=%v, err=%s] =", uid, err)
		return nil, err
	default:
	}

	var profileVer, friendVer, blackListVer, locationVer, inviteVer, lastGetOffMsgVer, headModifyVer uint64
	var lessonVer, walletVer int64
	var hideLocation, hideCity, hideAge, hideOnline uint32
	if storeProfileVer.Valid {
		profileVer = uint64(storeProfileVer.Int64)
	}
	if storeFriendVer.Valid {
		friendVer = uint64(storeFriendVer.Int64)
	}
	if storeBlackListVer.Valid {
		blackListVer = uint64(storeBlackListVer.Int64)
	}
	if storeLocationVer.Valid {
		locationVer = uint64(storeLocationVer.Int64)
	}
	if storeInviteVer.Valid {
		inviteVer = uint64(storeInviteVer.Int64)
	}
	if storeLastGetOffMsgVer.Valid {
		lastGetOffMsgVer = uint64(storeLastGetOffMsgVer.Int64)
	}
	if storeHeadModifyVer.Valid {
		headModifyVer = uint64(storeHeadModifyVer.Int64)
	}

	if storeLessonVer.Valid {
		lessonVer = storeLessonVer.Int64
	}

	if storeWalletVer.Valid {
		walletVer = storeWalletVer.Int64
	}

	if storeHideLocation.Valid {
		hideLocation = uint32(storeHideLocation.Int64)
	}

	if storeHideCity.Valid {
		hideCity = uint32(storeHideCity.Int64)
	}

	if storeHideAge.Valid {
		hideAge = uint32(storeHideAge.Int64)
	}

	if storeHideOnline.Valid {
		hideOnline = uint32(storeHideOnline.Int64)
	}

	baseInfo = &BaseInfoStore{
		UserName:         userName,
		UserType:         userType,
		UserNickName:     nickName,
		Fullpy:           fullpy,
		ShortFullpy:      shortFullpy,
		Sex:              sex,
		Birthday:         birtyday,
		Signature:        signature,
		HeadUrl:          headUrl,
		VoiceUrl:         voiceUrl,
		VoiceDuration:    voiceDuration,
		Nationality:      nationality,
		TimeZone:         timeZone,
		SummerTime:       summerTime,
		TimeZone48:       timeZone48,
		AllowCount:       allocCount,
		NativeLang:       native,
		TeachLang2:       teach2,
		TeachLang2Level:  teach2Level,
		TeachLang3:       teach3,
		TeachLang3Level:  teach3Level,
		LearnLang1:       learn1,
		LearnLang1Level:  learn1Level,
		LearnLang2:       learn2,
		LearnLang2Level:  learn2Level,
		LearnLang3:       learn3,
		LearnLang3Level:  learn3Level,
		LearnLang4:       learn4,
		LearnLang4Level:  learn4Level,
		ProfileVer:       profileVer,
		FriendVer:        friendVer,
		BlackListVer:     blackListVer,
		LocationVer:      locationVer,
		InviteVer:        inviteVer,
		LastGetOffMsgVer: int64(lastGetOffMsgVer),
		HeadModifyVer:    int64(headModifyVer),
		HideLocation:     hideLocation,
		HideCity:         hideCity,
		HideAge:          hideAge,
		HideOnline:       hideOnline,
		LastLessonVer:    lessonVer,
		WalletVer:        walletVer,
	}

	return baseInfo, nil
}

func (this *DbUtil) UpdateUserNameInDb(uid uint32, name string) (profileTS int64, err error) {
	if this.db == nil {
		return profileTS, ErrNilDbObject
	}

	if uid == 0 {
		return profileTS, ErrDbParam
	}

	_, err = this.db.Exec("UPDATE HT_USER_BASE set USERNAME=? WHERE USERID=?;",
		name,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserNameInDb insert faield uid=%v  err=%v", uid, err)
		return profileTS, err
	}

	profileTS = time.Now().Unix()
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET PROFILEVERSION=? WHERE USERID=?;",
		profileTS,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserSummerTimeInDb update HT_USER_PROPERTY faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	return profileTS, nil
}

func (this *DbUtil) UpdateUserSummerTimeInDb(uid uint32, summerTime uint32) (profileTS int64, err error) {
	if this.db == nil {
		return profileTS, ErrNilDbObject
	}

	if uid == 0 {
		return profileTS, ErrDbParam
	}

	_, err = this.db.Exec("UPDATE HT_USER_BASE SET SUMMERTIME=? WHERE USERID=?;",
		summerTime,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserSummerTimeInDb update HT_USER_BASE faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	profileTS = time.Now().Unix()
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET PROFILEVERSION=? WHERE USERID=?;",
		profileTS,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserSummerTimeInDb update HT_USER_PROPERTY faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	return profileTS, nil
}

func (this *DbUtil) UpdateUserTimeZoneInDb(uid uint32, timeZone int32) (profileTS int64, err error) {
	if this.db == nil {
		return profileTS, ErrNilDbObject
	}

	if uid == 0 {
		return profileTS, ErrDbParam
	}
	// 有符号的时区 -24 至 +24
	_, err = this.db.Exec("UPDATE HT_USER_BASE set TIMEZONE=?, TIMEZONE48=? WHERE USERID=? AND TIMEZONE != ? AND TIMEZONE48 != ?;",
		timeZone/2,
		timeZone,
		uid,
		timeZone/2,
		timeZone)
	if err != nil {
		this.infoLog.Printf("UpdateUserSummerTimeInDb insert faield uid=%v  err=%v", uid, err)
		return profileTS, err
	}

	profileTS = time.Now().Unix()
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET PROFILEVERSION=? WHERE USERID=?;",
		profileTS,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserSummerTimeInDb update HT_USER_PROPERTY faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	return profileTS, nil
}

func (this *DbUtil) GetUserNickNameInDb(uid uint32) (nickName, userName string, err error) {
	if this.db == nil {
		return nickName, userName, ErrNilDbObject
	}
	if uid == 0 {
		return nickName, userName, ErrDbParam
	}
	var storeNickName, storeUserName sql.NullString
	err = this.db.QueryRow("SELECT NICKNAME, USERNAME FROM HT_USER_BASE where USERID=?;", uid).Scan(&storeNickName, &storeUserName)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetUserNickNameInDb not found uid=%v in HT_USER_BASE err=%s", uid, err)
		return nickName, userName, err
	case err != nil:
		this.infoLog.Printf("GetUserNickNameInDb exec failed HT_USER_BASE uid=%v err=%s", uid, err)
		return nickName, userName, err
	default:
	}

	if storeNickName.Valid {
		nickName = storeNickName.String
	}
	if storeUserName.Valid {
		userName = storeUserName.String
	}
	return nickName, userName, nil
}

func (this *DbUtil) UpdateUserPrivateSettingInDb(uid uint32, hideLocation, hideCity, hideAge, hideOnline uint32) (profileTS int64, err error) {
	if this.db == nil {
		return profileTS, ErrNilDbObject
	}

	if uid == 0 {
		return profileTS, ErrDbParam
	}

	_, err = this.db.Exec("UPDATE HT_USER_PRIVATE SET HIDELOCATION=?, HIDECITY=?, HIDEAGE=?, HIDEONLINE=? WHERE USERID=?;",
		hideLocation,
		hideCity,
		hideAge,
		hideOnline,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserPrivateSettingInDb update HT_USER_PRIVATE faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	profileTS = time.Now().Unix()
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET PROFILEVERSION=? WHERE USERID=?;",
		profileTS,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserPrivateSettingInDb update HT_USER_PROPERTY faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	return profileTS, nil
}

func (this *DbUtil) UpdateUserProfileInDb(uid uint32,
	baseItems []uint32,
	baseInfo *ht_base_cache.BaseUserInfo,
	langItems []uint32,
	langInfo *ht_base_cache.LanguageInfo) (profileTS int64, err error) {
	if this.db == nil {
		return profileTS, ErrNilDbObject
	}

	if uid == 0 {
		return profileTS, ErrDbParam
	}

	if len(baseItems) > 0 {
		// STEP1 update HT_USER_BASE
		express, args, err := this.GetSetBaseInfoExpress(baseItems, baseInfo)
		if err != nil {
			this.infoLog.Printf("UpdateUserProfileInDb GetSetBaseInfoExpress return err=%s", err)
			return profileTS, err
		}

		this.infoLog.Printf("UpdateUserProfileInDb GetSetBaseInfoExpress express=%s args=%#v", express, args)
		// append uid
		args = append(args, uid)
		_, err = this.db.Exec("UPDATE HT_USER_BASE SET "+express+" WHERE USERID=?;", args...)
		if err != nil {
			this.infoLog.Printf("UpdateUserProfileInDb update HT_USER_BASE faield uid=%v err=%v", uid, err)
			return profileTS, err
		}
	}
	if len(langItems) > 0 {
		//STEP2 update HT_USER_LANGUAGE
		express, args, err := this.GetSetLangInfoExpress(langItems, langInfo)
		if err != nil {
			this.infoLog.Printf("UpdateUserProfileInDb GetSetLangInfoExpress return err=%s", err)
			return profileTS, err
		}

		this.infoLog.Printf("UpdateUserProfileInDb GetSetLangInfoExpress express=%s args=%#v", express, args)
		// append uid
		args = append(args, uid)
		_, err = this.db.Exec("UPDATE HT_USER_LANGUAGE SET "+express+" WHERE USERID=?;", args...)
		if err != nil {
			this.infoLog.Printf("UpdateUserProfileInDb update HT_USER_LANGUAGE faield uid=%v err=%v", uid, err)
			return profileTS, err
		}
	}

	profileTS = time.Now().Unix()
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET PROFILEVERSION=? WHERE USERID=?;",
		profileTS,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserProfileInDb update HT_USER_PROPERTY faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	return profileTS, nil
}

func (this *DbUtil) GetSetBaseInfoExpress(baseItems []uint32, baseInfo *ht_base_cache.BaseUserInfo) (express string, args []interface{}, err error) {
	if len(baseItems) == 0 {
		return express, nil, nil
	}
	for i, v := range baseItems {
		if i != 0 {
			express += ","
		}
		var itemExpress string
		switch v {
		case PMC_SEX:
			itemExpress = "SEX = ?"
			args = append(args, baseInfo.GetSex())
		case PMC_BIRTHDAY:
			itemExpress = "BIRTHDAY = ?"
			args = append(args, baseInfo.GetBirthday())
		case PMC_NATIONALITY:
			itemExpress = "NATIONALITY = ?"
			args = append(args, baseInfo.GetNationality())
		case PMC_NICK:
			itemExpress = "NICKNAME = ?"
			args = append(args, baseInfo.GetNickName())
		case PMC_FULLPY:
			itemExpress = "FULLPY = ?"
			args = append(args, baseInfo.GetFullpy())
		case PMC_SHORTPY:
			itemExpress = "SHORTPY = ?"
			args = append(args, baseInfo.GetShortFullpy())
		case PMC_TEXT:
			itemExpress = "SIGNATURE = ?"
			args = append(args, baseInfo.GetSignature())
		case PMC_VOICE:
			itemExpress = "VOICEURL = ?, VOICEDURATION = ?"
			args = append(args, baseInfo.GetVoiceUrl())
			args = append(args, baseInfo.GetVoiceDuration())
		case PMC_HEADURL:
			itemExpress = "HEADURL = ?"
			args = append(args, baseInfo.GetHeadUrl())
		default:
			this.infoLog.Printf("GetSetBaseInfoExpress unhandle type=%v", v)
		}
		express += itemExpress
	}
	return express, args, nil
}

func (this *DbUtil) GetSetLangInfoExpress(langItems []uint32, langInfo *ht_base_cache.LanguageInfo) (express string, args []interface{}, err error) {
	if len(langItems) == 0 {
		return express, nil, nil
	}
	for i, v := range langItems {
		if i != 0 {
			express += ","
		}
		var itemExpress string
		switch v {
		case PMC_NATIVELANG:
			itemExpress = "NATIVELANG = ?"
			args = append(args, langInfo.GetNativeLang())
		case PMC_LEARN1:
			itemExpress = "LEARNLANG1 = ?, SKILLLEVEL1 = ? "
			args = append(args, langInfo.GetLearnLang1())
			args = append(args, langInfo.GetLearnLang1Level())
		case PMC_LEARN2:
			itemExpress = "LEARNLANG2 = ?, SKILLLEVEL2 = ? "
			args = append(args, langInfo.GetLearnLang2())
			args = append(args, langInfo.GetLearnLang2Level())
		case PMC_LEARN3:
			itemExpress = "LEARNLANG3 = ?, SKILLLEVEL3 = ? "
			args = append(args, langInfo.GetLearnLang3())
			args = append(args, langInfo.GetLearnLang3Level())
		case PMC_LEARN4:
			itemExpress = "LEARNLANG4 = ?, SKILLLEVEL4 = ? "
			args = append(args, langInfo.GetLearnLang4())
			args = append(args, langInfo.GetLearnLang4Level())
		case PMC_TEACH2:
			itemExpress = "TEACHLANG2 = ?, TEACHSKILLLEVEL2 = ? "
			args = append(args, langInfo.GetTeachLang2())
			args = append(args, langInfo.GetTeachLang2Level())
		case PMC_TEACH3:
			itemExpress = "TEACHLANG3 = ?, TEACHSKILLLEVEL3 = ? "
			args = append(args, langInfo.GetTeachLang3())
			args = append(args, langInfo.GetTeachLang3Level())
		default:
			this.infoLog.Printf("GetSetLangInfoExpress unhandle type=%v", v)
		}
		express += itemExpress
	}
	return express, args, nil
}

func (this *DbUtil) UpdateLearnMultiLangInDb(uid uint32, langCount uint32) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	if uid == 0 {
		return ErrDbParam
	}
	_, err = this.db.Exec("UPDATE HT_USER_LANGUAGE SET ALLOWCOUNT=?, UPDATETIME=UTC_TIMESTAMP() WHERE USERID=?;", langCount, uid)
	if err != nil {
		this.infoLog.Printf("UpdateLearnMultiLangInDb update HT_USER_LANGUAGE faield uid=%v err=%v", uid, err)
		return err
	}

	return nil
}

func (this *DbUtil) UpdateUserLanguageSettingInDb(uid uint32, langInfo *ht_base_cache.LanguageInfo) (profileTS int64, err error) {
	if this.db == nil {
		return profileTS, ErrNilDbObject
	}

	if uid == 0 {
		return profileTS, ErrDbParam
	}

	_, err = this.db.Exec("UPDATE HT_USER_LANGUAGE SET NATIVELANG=?, LEARNLANG1=?, SKILLLEVEL1=?, LEARNLANG2=?,"+
		"SKILLLEVEL2=?, LEARNLANG3=?, SKILLLEVEL3=?, LEARNLANG4=?, SKILLLEVEL4=?,"+
		"TEACHLANG2=?, TEACHSKILLLEVEL2=?, TEACHLANG3=?, TEACHSKILLLEVEL3=?, UPDATETIME=UTC_TIMESTAMP() WHERE USERID=?;",
		langInfo.GetNativeLang(),
		langInfo.GetLearnLang1(),
		langInfo.GetLearnLang1Level(),
		langInfo.GetLearnLang2(),
		langInfo.GetLearnLang2Level(),
		langInfo.GetLearnLang3(),
		langInfo.GetLearnLang3Level(),
		langInfo.GetLearnLang4(),
		langInfo.GetLearnLang4Level(),
		langInfo.GetTeachLang2(),
		langInfo.GetTeachLang2Level(),
		langInfo.GetTeachLang3(),
		langInfo.GetTeachLang3Level(),
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserLanguageSettingInDb update HT_USER_LANGUAGE faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	profileTS = time.Now().Unix()
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET PROFILEVERSION=? WHERE USERID=?;",
		profileTS,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserLanguageSettingInDb update HT_USER_PROPERTY faield uid=%v err=%v", uid, err)
		return profileTS, err
	}
	return profileTS, nil
}

func (this *DbUtil) UpdateUserPropertyInDb(uid uint32, propertyInfo *ht_base_cache.UpdateUserPropertyReqBody) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	if uid == 0 {
		return ErrDbParam
	}

	express, err := this.GetSetPropertyInfoExpress(propertyInfo)
	if err != nil {
		this.infoLog.Printf("UpdateUserPropertyInDb GetSetPropertyInfoExpress return err=%s", err)
		return err
	}
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET "+express+" WHERE USERID=?;", uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserPropertyInDb update HT_USER_PROPERTY faield uid=%v err=%v", uid, err)
		return err
	}
	return nil
}

func (this *DbUtil) GetSetPropertyInfoExpress(propertyInfo *ht_base_cache.UpdateUserPropertyReqBody) (express string, err error) {
	if propertyInfo == nil {
		return express, ErrDbParam
	}
	switch propertyInfo.GetModType() {
	case ht_base_cache.MOD_PROPERTY_TYPE_MOD_PROFILE_VER:
		express = fmt.Sprintf("PROFILEVERSION = %v, UPDATETIME = now()", propertyInfo.GetProfileVer())
	case ht_base_cache.MOD_PROPERTY_TYPE_MOD_FRIEND_VER:
		express = fmt.Sprintf("FRIENDVERSION = %v, UPDATETIME = now()", propertyInfo.GetFriendVer())
	case ht_base_cache.MOD_PROPERTY_TYPE_MOD_BLACK_LIST_VER:
		express = fmt.Sprintf("BLACKLISTVERSION = %v, UPDATETIME = now()", propertyInfo.GetBlackListVer())
	case ht_base_cache.MOD_PROPERTY_TYPE_MOD_LOCATION_VER:
		express = fmt.Sprintf("LOCATIONVERSION = %v, UPDATETIME = now()", propertyInfo.GetLocationVer())
	case ht_base_cache.MOD_PROPERTY_TYPE_MOD_INVITE_VER:
		express = fmt.Sprintf("INVITEVERSION = %v, UPDATETIME = now()", propertyInfo.GetInviteVer())
	case ht_base_cache.MOD_PROPERTY_TYPE_MOD_LAST_GET_OFF_MSG_VER:
		express = fmt.Sprintf("TSOFFLINEMESSAGE = %v, UPDATETIME = now()", propertyInfo.GetLastGetOffMsgVer())
	case ht_base_cache.MOD_PROPERTY_TYPE_MOD_HEAD_VER:
		express = fmt.Sprintf("HEADMODTIME = %v, UPDATETIME = now()", propertyInfo.GetHeadModifyVer())
	default:
		this.infoLog.Printf("GetSetPropertyInfoExpress unhandle type=%v", propertyInfo.GetModType())
	}

	return express, nil
}

// lastVer 的单位是毫秒
func (this *DbUtil) UpdateUserCreateLessonVerInDb(uid uint32) (lastVer int64, err error) {
	if this.db == nil {
		return lastVer, ErrNilDbObject
	}

	if uid == 0 {
		return lastVer, ErrDbParam
	}
	lastVer = time.Now().UnixNano() / 1000000
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET LAST_LESSION_UPDATE = ? WHERE USERID=?;", lastVer, uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserCreateLessonVerInDb update HT_USER_PROPERTY faield uid=%v lastVer=%v err=%v", uid, lastVer, err)
		return lastVer, err
	}
	return lastVer, nil
}

// lastVer 的单位是毫秒
func (this *DbUtil) UpdateUserWalletVerInDb(uid uint32) (lastVer int64, err error) {
	if this.db == nil {
		return lastVer, ErrNilDbObject
	}

	if uid == 0 {
		return lastVer, ErrDbParam
	}
	lastVer = time.Now().UnixNano() / 1000000
	_, err = this.db.Exec("UPDATE HT_USER_PROPERTY SET LAST_WALLET_VER = ? WHERE USERID=?;", lastVer, uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserWalletVerInDb update HT_USER_PROPERTY faield uid=%v lastVer=%v err=%v", uid, lastVer, err)
		return lastVer, err
	}
	return lastVer, nil
}
