package main

import (
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_basecache"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	redisMasterApi *common.RedisApi
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
	ExpirePeriod       = 604800 //7 * 86400 = 30 day   units:second
)

const (
	UserNameField      = "username"
	UserTypeField      = "usertype"
	UserNickNameField  = "nickname"
	FullpyField        = "fullpy"
	ShortFullpyField   = "shortfullpy"
	SexField           = "sex"
	BirthdayField      = "birtyday"
	SignatureField     = "signature"
	HeadUrlField       = "headurl"
	VoiceUrlField      = "voiceurl"
	VoiceDurationField = "voiceduration"
	NationalityField   = "nationality"
	TimeZoneField      = "timezone"
	SummerTimeField    = "summertime"
	TimeZone48Field    = "timezone48"

	// HT_USER_LANGUAGE
	AllowCountField      = "allowcount"
	NativeLangField      = "nativelang"
	TeachLang2Field      = "teachlang2"
	TeachLang2LevelField = "teachlang2level"
	TeachLang3Field      = "teachlang3"
	TeachLang3LevelField = "teachlang3level"
	LearnLang1Field      = "learnlang1"
	LearnLang1LevelField = "learnlang1level"
	LearnLang2Field      = "learnlang2"
	LearnLang2LevelField = "learnlang2level"
	LearnLang3Field      = "learnlang3"
	LearnLang3LevelField = "learnlang3level"
	LearnLang4Field      = "learnlang4"
	LearnLang4LevelField = "learnlang4level"

	// HT_USER_PRIVATE
	HideLocationField = "hidelocation"
	HideCityField     = "hidecity"
	HideAgeField      = "hideage"
	HideOnlineField   = "hideonline"

	// HT_USER_PROPERTY
	ProfileVerField       = "profilever"
	FrinedVerField        = "friendver"
	BlackListVerField     = "blacklistver"
	LocationVerField      = "locationver"
	InviteVerField        = "invitever"
	LastGetOffMsgVerField = "lastgetoffmsgver"
	HeadModifyVerField    = "headmodiryver"
	LastLessonVerField    = "lastlessonver"
	LastWalletVerField    = "lastwalletver"
)

const (
	PMC_SEX         = 0  // 性别
	PMC_BIRTHDAY    = 1  // 生日
	PMC_NATIONALITY = 2  // 国籍检查
	PMC_NICK        = 3  // 用户昵称
	PMC_FULLPY      = 4  // 名称全拼
	PMC_SHORTPY     = 5  // 名称简拼
	PMC_TEXT        = 6  // 文本自我介绍
	PMC_VOICE       = 7  // 语音自我介绍
	PMC_HEADURL     = 8  // 个人头像URL
	PMC_NATIVELANG  = 9  // 用户母语
	PMC_LEARN1      = 10 //
	PMC_LEARN2      = 11 //
	PMC_LEARN3      = 12 //
	PMC_LEARN4      = 13 //
	PMC_TEACH2      = 14 // for multilanguage. The second teach language(NATIVE is the first)
	PMC_TEACH3      = 15
	PMC_COUNT       = 16
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gobagent/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch uint32(head.Cmd) {
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_BASE_CACHE):
		go ProcSetUserBaseCache(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_NAME):
		go ProcSetUserName(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_SUMMER_TIME_SETTING):
		go ProcSetUserSummerTime(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_TIMEZONE):
		go ProcSetUserTimeZone(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_NICK_NAME):
		go ProcSetUserNickName(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_PRIVATE):
		go ProcSetUserPrivate(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_PROFILE):
		go ProcSetUserProfile(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_LEARN_MULTI_LANG):
		go ProcSetLearnMultiLang(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_LANG_SETTING):
		go ProcSetUserLangSetting(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_PROPERTY):
		go ProcSetUserProperty(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_CREATE_LESSON_VER):
		go ProcSetUserLessonVer(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_WALLET_VER):
		go ProcSetUserWalletVer(c, head, packet.GetBody())
	case uint32(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR):
		go ProcDelKey(c, head, packet.GetBody())
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
	}
	return true
}

// 1.proc set base user cache
func ProcSetUserBaseCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserBaseCacheRspbody = &ht_base_cache.SetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserBaseCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_base_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserBaseCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserBaseCacheRspbody = &ht_base_cache.SetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserBaseCacheReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserBaseCache GetSetUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserBaseCacheRspbody = &ht_base_cache.SetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := GetHashMapName(head.Uid)
	baseInfo := subReqBody.GetBaseCache()
	if baseInfo == nil {
		infoLog.Println("ProcSetUserBaseCache subReqBody.GetBaseCache() nil uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserBaseCacheRspbody = &ht_base_cache.SetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body nil"),
			},
		}
		return false
	}

	// write into master and slave redis
	var fieldAndValue []string
	// add user name
	fieldAndValue = append(fieldAndValue, UserNameField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetUserName())

	// add user type
	fieldAndValue = append(fieldAndValue, UserTypeField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetUserType()))

	// add UserNickNameField
	fieldAndValue = append(fieldAndValue, UserNickNameField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetNickName())
	// add fullpy
	fieldAndValue = append(fieldAndValue, FullpyField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetFullpy())

	// add ShortFullpy
	fieldAndValue = append(fieldAndValue, ShortFullpyField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetShortFullpy())

	// add Sex
	fieldAndValue = append(fieldAndValue, SexField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetSex()))

	// add Birthday
	fieldAndValue = append(fieldAndValue, BirthdayField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetBirthday())

	// add Signature
	fieldAndValue = append(fieldAndValue, SignatureField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetSignature())

	// add HeadUrl
	fieldAndValue = append(fieldAndValue, HeadUrlField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetHeadUrl())

	// add VoiceUrl
	fieldAndValue = append(fieldAndValue, VoiceUrlField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetVoiceUrl())

	// add VoiceDuration
	fieldAndValue = append(fieldAndValue, VoiceDurationField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetVoiceDuration()))

	// add Nationality
	fieldAndValue = append(fieldAndValue, NationalityField)
	fieldAndValue = append(fieldAndValue, baseInfo.GetNationality())

	// add TimeZone
	fieldAndValue = append(fieldAndValue, TimeZoneField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetTimeZone()))

	// add SummerTime
	fieldAndValue = append(fieldAndValue, SummerTimeField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetSummerTime()))

	// add TimeZone48
	fieldAndValue = append(fieldAndValue, TimeZone48Field)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetTimeZone_48()))

	// add AllowCount
	fieldAndValue = append(fieldAndValue, AllowCountField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetAllowCount()))

	// add NativeLang
	fieldAndValue = append(fieldAndValue, NativeLangField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetNativeLang()))

	// add TeachLang2
	fieldAndValue = append(fieldAndValue, TeachLang2Field)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetTeachLang2()))
	// add TeachLang2Level
	fieldAndValue = append(fieldAndValue, TeachLang2LevelField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetTeachLang2Level()))
	// add TeachLang3
	fieldAndValue = append(fieldAndValue, TeachLang3Field)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetTeachLang3()))
	// add TeachLang3Level
	fieldAndValue = append(fieldAndValue, TeachLang3LevelField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetTeachLang3Level()))
	// add LearnLang1
	fieldAndValue = append(fieldAndValue, LearnLang1Field)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLearnLang1()))
	// add LearnLang1Level
	fieldAndValue = append(fieldAndValue, LearnLang1LevelField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLearnLang1Level()))

	// add LearnLang2
	fieldAndValue = append(fieldAndValue, LearnLang2Field)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLearnLang2()))

	// add LearnLang2Level
	fieldAndValue = append(fieldAndValue, LearnLang2LevelField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLearnLang2Level()))

	// add LearnLang3
	fieldAndValue = append(fieldAndValue, LearnLang3Field)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLearnLang3()))
	// add LearnLang3Level
	fieldAndValue = append(fieldAndValue, LearnLang3LevelField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLearnLang3Level()))
	// add LearnLang4
	fieldAndValue = append(fieldAndValue, LearnLang4Field)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLearnLang4()))
	// add LearnLang4Level
	fieldAndValue = append(fieldAndValue, LearnLang4LevelField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLearnLang4Level()))

	// add HideLocation
	fieldAndValue = append(fieldAndValue, HideLocationField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetHideLocation()))

	// add HideCity
	fieldAndValue = append(fieldAndValue, HideCityField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetHideCity()))

	// add HideAge
	fieldAndValue = append(fieldAndValue, HideAgeField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetHideAge()))

	// add HideOnline
	fieldAndValue = append(fieldAndValue, HideOnlineField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetHideOnline()))

	// add profile version
	fieldAndValue = append(fieldAndValue, ProfileVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetProfileVer()))

	// add frined version
	fieldAndValue = append(fieldAndValue, FrinedVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetFriendVer()))

	// add balcklist version
	fieldAndValue = append(fieldAndValue, BlackListVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetBlackListVer()))

	// add location version
	fieldAndValue = append(fieldAndValue, LocationVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLocationVer()))

	// add invite version
	fieldAndValue = append(fieldAndValue, InviteVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetInviteVer()))

	// add last get offline msg version
	fieldAndValue = append(fieldAndValue, LastGetOffMsgVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLastGetOffMsgVer()))

	// add head modify version
	fieldAndValue = append(fieldAndValue, HeadModifyVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetHeadModifyVer()))

	// add last lesson version
	fieldAndValue = append(fieldAndValue, LastLessonVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLastLessonVer()))

	// add last wallet version
	fieldAndValue = append(fieldAndValue, LastWalletVerField)
	fieldAndValue = append(fieldAndValue, fmt.Sprintf("%v", baseInfo.GetLastWalletVer()))

	err = redisMasterApi.Hmset(hashName, fieldAndValue)
	if err != nil {
		infoLog.Printf("ProcSetUserBaseCache redisMasterApi.Hmset uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
		rspBody.SetUserBaseCacheRspbody = &ht_base_cache.SetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("redis exec error"),
			},
		}
		attr := "gobagent/set_user_base_cache_redis_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	err = redisMasterApi.Expire(hashName, ExpirePeriod)
	if err != nil {
		infoLog.Printf("ProcSetUserBaseCache redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
		attr := "gobagent/set_key_expire_redis_err"
		libcomm.AttrAdd(attr, 1)
	}

	rspBody.SetUserBaseCacheRspbody = &ht_base_cache.SetUserBaseCacheRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func GetHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#base", uid)
	return hashName
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_base_cache.BaseCacheRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

// 2.proc set user name
func ProcSetUserName(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserNameRspbody = &ht_base_cache.SetUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserName invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_name_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserName proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserNameRspbody = &ht_base_cache.SetUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserNameReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserName GetGetUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserNameRspbody = &ht_base_cache.SetUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserName uid=%v name=%s success", head.Uid, subReqBody.GetUserName())
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		keyValue := []string{
			UserNameField,
			fmt.Sprintf("%v", subReqBody.GetUserName()),
			ProfileVerField,
			fmt.Sprintf("%v", subReqBody.GetProfileTs()),
		}
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserName uid=%v redisMasterApi.Hmset failed userName=%v err=%s", head.Uid, subReqBody.GetUserName(), err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserNameRspbody = &ht_base_cache.SetUserNameRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_name_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserName redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_name_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserName not exist hashname=%s no need update name=%s", hashName, subReqBody.GetUserName())
	}

	rspBody.SetUserNameRspbody = &ht_base_cache.SetUserNameRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 3.proc set user summer time
func ProcSetUserSummerTime(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserSummerTimeRspbody = &ht_base_cache.SetUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserSummerTime invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_summertime_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserSummerTime proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserSummerTimeRspbody = &ht_base_cache.SetUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserSummerTimeReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserSummerTime GetUpdateUserSummerTimeReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserSummerTimeRspbody = &ht_base_cache.SetUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserSummerTime uid=%v summerTime=%v success", head.Uid, subReqBody.GetSummerTime())
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		keyValue := []string{
			SummerTimeField,
			fmt.Sprintf("%v", subReqBody.GetSummerTime()),
			ProfileVerField,
			fmt.Sprintf("%v", subReqBody.GetProfileTs()),
		}
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserSummerTime uid=%v redisMasterApi.Hmset failed summertime=%v err=%s", head.Uid, subReqBody.GetSummerTime(), err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserSummerTimeRspbody = &ht_base_cache.SetUserSummerTimeRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_summertime_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserSummerTime redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}

	} else {
		attr := "gobagent/set_user_summertime_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserSummerTime not exist hashname=%s no need update summerTime=%v", hashName, subReqBody.GetSummerTime())
	}

	rspBody.SetUserSummerTimeRspbody = &ht_base_cache.SetUserSummerTimeRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 4.proc set user timezone
func ProcSetUserTimeZone(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserTimeZoneRspbody = &ht_base_cache.SetUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserTimeZone invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_timezone_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserTimeZone proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserTimeZoneRspbody = &ht_base_cache.SetUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserTimeZoneReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserTimeZone GetGetUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserTimeZoneRspbody = &ht_base_cache.SetUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserTimeZone uid=%v timezone=%v success", head.Uid, subReqBody.GetTimeZone())
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		keyValue := []string{
			TimeZoneField,
			fmt.Sprintf("%v", int(subReqBody.GetTimeZone())/2),
			TimeZone48Field,
			fmt.Sprintf("%v", int(subReqBody.GetTimeZone())),
			ProfileVerField,
			fmt.Sprintf("%v", subReqBody.GetProfileTs()),
		}
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserTimeZone uid=%v redisMasterApi.Hmset failed err=%s", head.Uid, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserTimeZoneRspbody = &ht_base_cache.SetUserTimeZoneRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_timezone_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserTimeZone redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_timezone_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserTimeZone not exist hashname=%s no need update timeZone=%v profileTs=%v", hashName, subReqBody.GetTimeZone(), subReqBody.GetProfileTs())
	}

	rspBody.SetUserTimeZoneRspbody = &ht_base_cache.SetUserTimeZoneRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 5.proc set user nick name
func ProcSetUserNickName(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserNickNameRspbody = &ht_base_cache.SetUserNickNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserNickName invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_nick_name_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserNickName proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserNickNameRspbody = &ht_base_cache.SetUserNickNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserNickNameReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserNickName GetGetUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserNickNameRspbody = &ht_base_cache.SetUserNickNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserNickName uid=%v nickName=%s userName=%s success",
		head.Uid,
		subReqBody.GetNickName(),
		subReqBody.GetUserName())
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		var keyValue []string
		keyValue = append(keyValue, UserNameField)
		keyValue = append(keyValue, string(subReqBody.GetUserName()))
		keyValue = append(keyValue, UserNickNameField)
		keyValue = append(keyValue, string(subReqBody.GetNickName()))
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserNickName uid=%v redisMasterApi.Hmset hashname=%s UserNameField failed err=%s", head.Uid, hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserNickNameRspbody = &ht_base_cache.SetUserNickNameRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_nick_name_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserNickName redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_nick_name_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserNickName not exist hashname=%s no need update userName=%s nickName=%s",
			hashName,
			subReqBody.GetUserName(),
			subReqBody.GetNickName())
	}

	rspBody.SetUserNickNameRspbody = &ht_base_cache.SetUserNickNameRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 6.proc set user private
func ProcSetUserPrivate(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserPrivateRspbody = &ht_base_cache.SetUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserPrivate invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_private_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserPrivate proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserPrivateRspbody = &ht_base_cache.SetUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserPrivateReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserPrivate GetUpdateUserPrivateReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserPrivateRspbody = &ht_base_cache.SetUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserPrivate uid=%v hideLocation=%v hideCity=%v hideAge=%v hideOnline=%v success",
		head.Uid,
		subReqBody.GetHideLocation(),
		subReqBody.GetHideCity(),
		subReqBody.GetHideAge(),
		subReqBody.GetHideOnline())
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		keyValue := []string{
			HideLocationField,
			fmt.Sprintf("%v", subReqBody.GetHideLocation()),
			HideCityField,
			fmt.Sprintf("%v", subReqBody.GetHideCity()),
			HideAgeField,
			fmt.Sprintf("%v", subReqBody.GetHideAge()),
			HideOnlineField,
			fmt.Sprintf("%v", subReqBody.GetHideOnline()),
			ProfileVerField,
			fmt.Sprintf("%v", subReqBody.GetProfileTs()),
		}
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserPrivate uid=%v redisMasterApi.Hmset hasName=%s failed err=%s", head.Uid, hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserPrivateRspbody = &ht_base_cache.SetUserPrivateRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_private_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserPrivate redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_private_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserPrivate not exist hashname=%s no need update hideLocation=%v hideCity=%v hideAge=%v hideOnline=%v profileTs=%v",
			hashName,
			subReqBody.GetHideLocation(),
			subReqBody.GetHideCity(),
			subReqBody.GetHideAge(),
			subReqBody.GetHideOnline(),
			subReqBody.GetProfileTs())
	}

	rspBody.SetUserPrivateRspbody = &ht_base_cache.SetUserPrivateRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 7.proc set user profile
func ProcSetUserProfile(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserProfileRspbody = &ht_base_cache.SetUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserProfile invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_profile_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserProfile proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserProfileRspbody = &ht_base_cache.SetUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserProfileReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserProfile GetUpdateUserPrivateReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserProfileRspbody = &ht_base_cache.SetUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserProfile uid=%v baseItem=%#v baseInfo=%#v langItem=%#v langInfo=%#v success",
		head.Uid,
		subReqBody.GetBaseItems(),
		*(subReqBody.GetBaseInfo()),
		subReqBody.GetLangItems(),
		*(subReqBody.GetLangInfo()))
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		keyValue := GetSetProfileKeyValueSlice(subReqBody.GetBaseItems(), subReqBody.GetBaseInfo(), subReqBody.GetLangItems(), subReqBody.GetLangInfo())
		// add provile ts
		keyValue = append(keyValue, ProfileVerField)
		keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetProfileTs()))
		infoLog.Printf("ProcSetUserProfile hashName=%s keyValue=%#v", hashName, keyValue)
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserProfile uid=%v redisMasterApi.Hmset hasName=%s failed err=%s", head.Uid, hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserProfileRspbody = &ht_base_cache.SetUserProfileRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_profile_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserProfile redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_profile_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserProfile not exist hashname=%s no need update baseItem=%#v baseInfo=%#v langItem=%#v langInfo=%#v profileTs=%v",
			hashName,
			subReqBody.GetBaseItems(),
			*(subReqBody.GetBaseInfo()),
			subReqBody.GetLangItems(),
			*(subReqBody.GetLangInfo()),
			subReqBody.GetProfileTs())
	}

	rspBody.SetUserProfileRspbody = &ht_base_cache.SetUserProfileRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 8.proc set learn multi lang
func ProcSetLearnMultiLang(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetLearnMultiLangRspbody = &ht_base_cache.SetLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetLearnMultiLang invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_learn_multi_lang_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetLearnMultiLang proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetLearnMultiLangRspbody = &ht_base_cache.SetLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetLearnMultiLangReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetLearnMultiLang GetSetLearnMultiLangReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetLearnMultiLangRspbody = &ht_base_cache.SetLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetLearnMultiLang uid=%v langCount=%v success",
		head.Uid,
		subReqBody.GetLangCount())
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		err = redisMasterApi.Hset(hashName, AllowCountField, fmt.Sprintf("%v", subReqBody.GetLangCount()))
		if err != nil {
			infoLog.Printf("ProcSetLearnMultiLang uid=%v redisMasterApi.Hset hashname=%s AllowCountField failed err=%s", head.Uid, hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetLearnMultiLangRspbody = &ht_base_cache.SetLearnMultiLangRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_learn_multi_lang_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetLearnMultiLang redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_learn_multi_lang_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetLearnMultiLang not exist hashname=%s no need update langCount=%v",
			hashName,
			subReqBody.GetLangCount())
	}

	rspBody.SetLearnMultiLangRspbody = &ht_base_cache.SetLearnMultiLangRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 9.proc set user lange setting
func ProcSetUserLangSetting(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserLangSettingRspbody = &ht_base_cache.SetUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserLangSetting invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_lang_setting_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserLangSetting proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserLangSettingRspbody = &ht_base_cache.SetUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserLangSettingReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserLangSetting GetSetUserLangSettingReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserLangSettingRspbody = &ht_base_cache.SetUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserLangSetting uid=%v langInfo=%#v profileTs=%v success",
		head.Uid,
		subReqBody.GetLangInfo(),
		subReqBody.GetProfileTs())
	langInfo := subReqBody.GetLangInfo()
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		var keyValue []string
		// add NativeLang
		keyValue = append(keyValue, NativeLangField)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetNativeLang()))

		// add LearnLang1
		keyValue = append(keyValue, LearnLang1Field)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang1()))
		// add LearnLang1Level
		keyValue = append(keyValue, LearnLang1LevelField)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang1Level()))

		// add LearnLang2
		keyValue = append(keyValue, LearnLang2Field)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang2()))

		// add LearnLang2Level
		keyValue = append(keyValue, LearnLang2LevelField)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang2Level()))

		// add LearnLang3
		keyValue = append(keyValue, LearnLang3Field)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang3()))
		// add LearnLang3Level
		keyValue = append(keyValue, LearnLang3LevelField)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang3Level()))

		// add LearnLang4
		keyValue = append(keyValue, LearnLang4Field)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang4()))
		// add LearnLang4Level
		keyValue = append(keyValue, LearnLang4LevelField)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang4Level()))

		// add TeachLang2
		keyValue = append(keyValue, TeachLang2Field)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetTeachLang2()))
		// add TeachLang2Level
		keyValue = append(keyValue, TeachLang2LevelField)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetTeachLang2Level()))
		// add TeachLang3
		keyValue = append(keyValue, TeachLang3Field)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetTeachLang3()))
		// add TeachLang3Level
		keyValue = append(keyValue, TeachLang3LevelField)
		keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetTeachLang3Level()))

		// add profile ts
		keyValue = append(keyValue, ProfileVerField)
		keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetProfileTs()))
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserLangSetting uid=%v redisMasterApi.Hmset hashname=%s langInfo failed err=%s", head.Uid, hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserLangSettingRspbody = &ht_base_cache.SetUserLangSettingRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_lang_setting_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserLangSetting redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_lang_setting_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserLangSetting not exist hashname=%s no need update langInfo=%#v profileTs=%v",
			hashName,
			subReqBody.GetLangInfo(),
			subReqBody.GetProfileTs())
	}

	rspBody.SetUserLangSettingRspbody = &ht_base_cache.SetUserLangSettingRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 10.proc set user property
func ProcSetUserProperty(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserPropertyRspbody = &ht_base_cache.SetUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserProperty invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_property_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserProperty proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserPropertyRspbody = &ht_base_cache.SetUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserPropertyReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserProperty GetSetUserPropertyReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserPropertyRspbody = &ht_base_cache.SetUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserProperty uid=%v subReqBody=%#v success", head.Uid, *(subReqBody))
	hashName := GetHashMapName(head.Uid)
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		var keyValue []string
		switch subReqBody.GetModType() {
		case ht_base_cache.MOD_PROPERTY_TYPE_MOD_PROFILE_VER:
			// add profile ts
			keyValue = append(keyValue, ProfileVerField)
			keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetProfileVer()))
		case ht_base_cache.MOD_PROPERTY_TYPE_MOD_FRIEND_VER:
			keyValue = append(keyValue, FrinedVerField)
			keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetFriendVer()))
		case ht_base_cache.MOD_PROPERTY_TYPE_MOD_BLACK_LIST_VER:
			keyValue = append(keyValue, BlackListVerField)
			keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetBlackListVer()))
		case ht_base_cache.MOD_PROPERTY_TYPE_MOD_LOCATION_VER:
			keyValue = append(keyValue, LocationVerField)
			keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetLocationVer()))
		case ht_base_cache.MOD_PROPERTY_TYPE_MOD_INVITE_VER:
			keyValue = append(keyValue, InviteVerField)
			keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetInviteVer()))
		case ht_base_cache.MOD_PROPERTY_TYPE_MOD_LAST_GET_OFF_MSG_VER:
			keyValue = append(keyValue, LastGetOffMsgVerField)
			keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetLastGetOffMsgVer()))
		case ht_base_cache.MOD_PROPERTY_TYPE_MOD_HEAD_VER:
			keyValue = append(keyValue, HeadModifyVerField)
			keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetHeadModifyVer()))
		default:
			infoLog.Printf("ProcSetUserProperty unhandle type=%v", subReqBody.GetModType())
		}

		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserProperty uid=%v redisMasterApi.Hmset hashname=%s propertyinfo failed err=%s", head.Uid, hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserPropertyRspbody = &ht_base_cache.SetUserPropertyRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_property_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserProperty redisMasterApi.Expire uid=%v hashName=%s failed err=%s", head.Uid, hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_property_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserProperty not exist hashname=%s no need update subReqBody=%#v profileTs=%v", hashName, *(subReqBody))
	}

	rspBody.SetUserPropertyRspbody = &ht_base_cache.SetUserPropertyRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}
func GetSetProfileKeyValueSlice(baseItems []uint32, baseInfo *ht_base_cache.BaseUserInfo, langItems []uint32, langInfo *ht_base_cache.LanguageInfo) (keyValue []string) {
	if (len(baseItems) == 0) && (len(langItems) == 0) {
		return keyValue
	}
	// append langItems
	baseItems = append(baseItems, langItems...)
	for _, v := range baseItems {
		switch v {
		case PMC_SEX:
			keyValue = append(keyValue, SexField)
			keyValue = append(keyValue, fmt.Sprintf("%v", baseInfo.GetSex()))
		case PMC_BIRTHDAY:
			keyValue = append(keyValue, BirthdayField)
			keyValue = append(keyValue, baseInfo.GetBirthday())
		case PMC_NATIONALITY:
			// add Nationality
			keyValue = append(keyValue, NationalityField)
			keyValue = append(keyValue, baseInfo.GetNationality())
		case PMC_NICK:
			// add UserNickNameField
			keyValue = append(keyValue, UserNickNameField)
			keyValue = append(keyValue, baseInfo.GetNickName())
		case PMC_FULLPY:
			// add fullpy
			keyValue = append(keyValue, FullpyField)
			keyValue = append(keyValue, baseInfo.GetFullpy())
		case PMC_SHORTPY:
			// add ShortFullpy
			keyValue = append(keyValue, ShortFullpyField)
			keyValue = append(keyValue, baseInfo.GetShortFullpy())
		case PMC_TEXT:
			// add Signature
			keyValue = append(keyValue, SignatureField)
			keyValue = append(keyValue, baseInfo.GetSignature())
		case PMC_VOICE:
			// add VoiceUrl
			keyValue = append(keyValue, VoiceUrlField)
			keyValue = append(keyValue, baseInfo.GetVoiceUrl())

			// add VoiceDuration
			keyValue = append(keyValue, VoiceDurationField)
			keyValue = append(keyValue, fmt.Sprintf("%v", baseInfo.GetVoiceDuration()))

		case PMC_HEADURL:
			// add HeadUrl
			keyValue = append(keyValue, HeadUrlField)
			keyValue = append(keyValue, baseInfo.GetHeadUrl())
		case PMC_NATIVELANG:
			// add NativeLang
			keyValue = append(keyValue, NativeLangField)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetNativeLang()))

		case PMC_LEARN1:
			// add LearnLang1
			keyValue = append(keyValue, LearnLang1Field)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang1()))
			// add LearnLang1Level
			keyValue = append(keyValue, LearnLang1LevelField)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang1Level()))

		case PMC_LEARN2:
			// add LearnLang2
			keyValue = append(keyValue, LearnLang2Field)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang2()))

			// add LearnLang2Level
			keyValue = append(keyValue, LearnLang2LevelField)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang2Level()))

		case PMC_LEARN3:
			// add LearnLang3
			keyValue = append(keyValue, LearnLang3Field)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang3()))
			// add LearnLang3Level
			keyValue = append(keyValue, LearnLang3LevelField)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang3Level()))

		case PMC_LEARN4:
			// add LearnLang4
			keyValue = append(keyValue, LearnLang4Field)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang4()))
			// add LearnLang4Level
			keyValue = append(keyValue, LearnLang4LevelField)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetLearnLang4Level()))

		case PMC_TEACH2:
			// add TeachLang2
			keyValue = append(keyValue, TeachLang2Field)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetTeachLang2()))
			// add TeachLang2Level
			keyValue = append(keyValue, TeachLang2LevelField)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetTeachLang2Level()))
		case PMC_TEACH3:
			// add TeachLang3
			keyValue = append(keyValue, TeachLang3Field)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetTeachLang3()))
			// add TeachLang3Level
			keyValue = append(keyValue, TeachLang3LevelField)
			keyValue = append(keyValue, fmt.Sprintf("%v", langInfo.GetTeachLang3Level()))
		default:
			infoLog.Printf("GetSetProfileKeyValueSlice unhandle type=%v", v)
		}
	}
	return keyValue
}

// 11.proc set user lesson ver
func ProcSetUserLessonVer(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserLessonVerRspbody = &ht_base_cache.SetUserLessonVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserLessonVer invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_lesson_ver_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserLessonVer proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserLessonVerRspbody = &ht_base_cache.SetUserLessonVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserLessonVerReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserLessonVer GetSetUserLessonVerReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserLessonVerRspbody = &ht_base_cache.SetUserLessonVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserLessonVer uid=%v reqUid=%v lastVer=%v success", head.Uid, subReqBody.GetReqUid(), subReqBody.GetLastLessonVer())
	hashName := GetHashMapName(subReqBody.GetReqUid())
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		var keyValue []string
		// add last lesson version
		keyValue = append(keyValue, LastLessonVerField)
		keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetLastLessonVer()))
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserLessonVer uid=%v redisMasterApi.Hmset hashname=%s propertyinfo failed err=%s", subReqBody.GetReqUid(), hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserLessonVerRspbody = &ht_base_cache.SetUserLessonVerRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_lesson_ver_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserLessonVer redisMasterApi.Expire uid=%v hashName=%s failed err=%s", subReqBody.GetReqUid(), hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_lesson_ver_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserProperty not exist hashname=%s no need update reqUid=%v lastVer=%v", hashName, subReqBody.GetReqUid(), subReqBody.GetLastLessonVer())
	}

	rspBody.SetUserLessonVerRspbody = &ht_base_cache.SetUserLessonVerRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 12.proc set user wallet ver
func ProcSetUserWalletVer(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.SetUserWalletVerRspbody = &ht_base_cache.SetUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserWalletVer invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/set_user_wallet_ver_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserWalletVer proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserWalletVerRspbody = &ht_base_cache.SetUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserWalletVerReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserWalletVer GetSetUserWalletVerReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.SetUserWalletVerRspbody = &ht_base_cache.SetUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetUserWalletVer uid=%v reqUid=%v lastVer=%v success", head.Uid, subReqBody.GetReqUid(), subReqBody.GetLastWalletVer())
	hashName := GetHashMapName(subReqBody.GetReqUid())
	exist, err := redisMasterApi.Exists(hashName)
	if err == nil && exist == true { //存在则更新
		var keyValue []string
		// add last lesson version
		keyValue = append(keyValue, LastWalletVerField)
		keyValue = append(keyValue, fmt.Sprintf("%v", subReqBody.GetLastWalletVer()))
		err = redisMasterApi.Hmset(hashName, keyValue)
		if err != nil {
			infoLog.Printf("ProcSetUserWalletVer uid=%v redisMasterApi.Hmset hashname=%s propertyinfo failed err=%s", subReqBody.GetReqUid(), hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.SetUserWalletVerRspbody = &ht_base_cache.SetUserWalletVerRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis master error"),
				},
			}
			attr := "gobagent/set_user_wallet_ver_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		err = redisMasterApi.Expire(hashName, ExpirePeriod)
		if err != nil {
			infoLog.Printf("ProcSetUserWalletVer redisMasterApi.Expire uid=%v hashName=%s failed err=%s", subReqBody.GetReqUid(), hashName, err)
			attr := "gobagent/set_key_expire_redis_err"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "gobagent/set_user_wallet_ver_not_exist"
		libcomm.AttrAdd(attr, 1)
		infoLog.Println("ProcSetUserProperty not exist hashname=%s no need update reqUid=%v lastVer=%v", hashName, subReqBody.GetReqUid(), subReqBody.GetLastWalletVer())
	}

	rspBody.SetUserWalletVerRspbody = &ht_base_cache.SetUserWalletVerRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func ProcDelKey(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.DelKeyInRedisRspbody = &ht_base_cache.DelKeyInRedisRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelKey invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobagent/del_key_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelKey proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelKeyInRedisRspbody = &ht_base_cache.DelKeyInRedisRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelKeyInRedisReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcDelKey GetDelKeyInRedisReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelKeyInRedisRspbody = &ht_base_cache.DelKeyInRedisRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	key := subReqBody.GetDelKey()
	infoLog.Printf("ProcDelKey delKey=%s", key)

	//设置到redis中
	err = redisMasterApi.Del(key)
	if err != nil {
		infoLog.Printf("ProcDelKey redisMasterApi.Del failed delKey=%s err=%s", key, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
		rspBody.DelKeyInRedisRspbody = &ht_base_cache.DelKeyInRedisRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("redis master error"),
			},
		}
		attr := "gobagent/del_key_redis_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	rspBody.DelKeyInRedisRspbody = &ht_base_cache.DelKeyInRedisRspBody{
		Status: &ht_base_cache.BaseCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
