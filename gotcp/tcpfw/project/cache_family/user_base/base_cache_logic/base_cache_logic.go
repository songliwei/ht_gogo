package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_basecache"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog             *log.Logger
	writeAgentMastreApi *common.SrvToSrvApiV2
	writeAgentslaveApi  *common.SrvToSrvApiV2
	dbdAgentApi         *common.SrvToSrvApiV2
	redisMasterApi      *common.RedisApi
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
)

const (
	UserNameField      = "username"
	UserTypeField      = "usertype"
	UserNickNameField  = "nickname"
	FullpyField        = "fullpy"
	ShortFullpyField   = "shortfullpy"
	SexField           = "sex"
	BirthdayField      = "birtyday"
	SignatureField     = "signature"
	HeadUrlField       = "headurl"
	VoiceUrlField      = "voiceurl"
	VoiceDurationField = "voiceduration"
	NationalityField   = "nationality"
	TimeZoneField      = "timezone"
	SummerTimeField    = "summertime"
	TimeZone48Field    = "timezone48"

	// HT_USER_LANGUAGE
	AllowCountField      = "allowcount"
	NativeLangField      = "nativelang"
	TeachLang2Field      = "teachlang2"
	TeachLang2LevelField = "teachlang2level"
	TeachLang3Field      = "teachlang3"
	TeachLang3LevelField = "teachlang3level"
	LearnLang1Field      = "learnlang1"
	LearnLang1LevelField = "learnlang1level"
	LearnLang2Field      = "learnlang2"
	LearnLang2LevelField = "learnlang2level"
	LearnLang3Field      = "learnlang3"
	LearnLang3LevelField = "learnlang3level"
	LearnLang4Field      = "learnlang4"
	LearnLang4LevelField = "learnlang4level"

	// HT_USER_PRIVATE
	HideLocationField = "hidelocation"
	HideCityField     = "hidecity"
	HideAgeField      = "hideage"
	HideOnlineField   = "hideonline"

	// HT_USER_PROPERTY
	ProfileVerField       = "profilever"
	FrinedVerField        = "friendver"
	BlackListVerField     = "blacklistver"
	LocationVerField      = "locationver"
	InviteVerField        = "invitever"
	LastGetOffMsgVerField = "lastgetoffmsgver"
	HeadModifyVerField    = "headmodiryver"
	LastLessonVerField    = "lastlessonver"
	LastWalletVerField    = "lastwalletver"
)

// alias to Util.BaseInfoStore
type baseInfoStru struct {
	// HT_USER_BASE
	UserName      string //用户名
	UserType      uint32 //用户类别：0-普通用户
	UserNickName  string //用户别名 昵称
	Fullpy        string //用户别名的拼音
	ShortFullpy   string //用户别名的拼音缩写
	Sex           uint32 //性别
	Birthday      string //生日
	Signature     string //签名
	HeadUrl       string //用户头像URL
	VoiceUrl      string //用户语音介绍的URL
	VoiceDuration uint32 //语音介绍录音的时长
	Nationality   string //用户国籍英文简称
	TimeZone      uint8  //用户所在时区(24时区制)
	SummerTime    uint8  //夏令制设置
	TimeZone48    uint8  //用户时区(半时区为单位 -24 到 24)

	// HT_USER_LANGUAGE
	AllowCount      uint32 //可以教学的语言数量(使用权限)
	NativeLang      uint32 //母语 语言语种使用数字代表
	TeachLang2      uint32 //教学语言二
	TeachLang2Level uint32 //教学语言二等级
	TeachLang3      uint32 //教学语言三
	TeachLang3Level uint32 //教学语言三等级
	LearnLang1      uint32 //学习语言一
	LearnLang1Level uint32 //学习语言一等级
	LearnLang2      uint32 //学习语言二
	LearnLang2Level uint32 //学习语言二等级
	LearnLang3      uint32 //学习语言三
	LearnLang3Level uint32 //学习语言三等级
	LearnLang4      uint32 //学习语言四
	LearnLang4Level uint32 //学习语言四等级

	// HT_USER_PRIVATE
	HideLocation uint32 //隐藏定位信息
	HideCity     uint32 //隐藏城市信息
	HideAge      uint32 //隐藏年龄信息
	HideOnline   uint32 //隐藏在线状态

	// HT_USER_PROPERTY PROFILEVERSION
	ProfileVer       uint64 //用户资料版本时间戳
	FriendVer        uint64 //用户好友版本时间戳
	BlackListVer     uint64 //黑名单版本时间戳
	LocationVer      uint64 //用户定位信息版本时间戳
	InviteVer        uint64 //好友邀请版本戳
	LastGetOffMsgVer int64  //最后成功获取的离线时间戳
	HeadModifyVer    int64  //头像最后修改时间
	LastLessonVer    int64  //创建课程最后更新的时间戳
	WalletVer        int64  //发起课程收费之后的展示个人钱包入口
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close() // 关闭连接
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close() // 关闭连接
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close() // 关闭连接
		return false
	}

	// 统计总的请求量
	attr := "gobalogic/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_base_cache.CMD_TYPE_CMD_GET_USER_BASE_CACHE):
		go ProcGetUserBaseCache(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_RELOAD_USER_BASE_CACHE):
		go ProcRelodUserBaseCache(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_NAME):
		go ProcUpdateUserName(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_SUMMER_TIME_SETTING):
		go ProcUpdateUserSummerTime(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_TIMEZONE):
		go ProcUpdateUserTimeZone(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_GET_USER_NICK_NAME):
		go ProcGetUserNickName(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_PRIVATE):
		go ProcUpdateUserPrivate(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_PROFILE):
		go ProcUpdateUserProfile(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_LEARN_MULTI_LANG):
		go ProcUpdateLearnMultiLang(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_LANG_SETTING):
		go ProcUpdateUserLangSetting(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_PROPERTY):
		go ProcUpdateUserProperty(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_CREATE_LESSON_VER):
		go ProcUpdateCreateLessonVer(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_WALLET_VER):
		go ProcUpdateUserWalletVer(c, head, packet.GetBody())
	case uint32(ht_base_cache.CMD_TYPE_CMD_DEL_USER_BASE_CACHE):
		go ProcDelUserBaseCache(c, head, packet.GetBody())

	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
	}
	return true
}

// 1.proc store moment info
func ProcGetUserBaseCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserBaseCache not need call")
		}
	}()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserBaseCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gobalogic/get_user_base_cache_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gobalogic/get_user_base_cache_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserBaseCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserBaseCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserBaseCache GetGetUserBaseCacheReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := GetHashMapName(head.Uid)
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// Step1 存在则读取完成的记录
		var baseInfo baseInfoStru
		baseInfo.LastLessonVer = 0 // 初始化首先将这个字段置位0 如果后面hash map中有则更新 没有保持为0
		keyValue, err := redisMasterApi.Hgetall(hashName)
		if err != nil {
			infoLog.Printf("ProcGetUserBaseCache redisMasterApi.Hgetall hashName=%s err=%s faield", hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis exec error"),
				},
			}
			attr := "gobalogic/get_user_base_cache_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		for key, value := range keyValue {
			// infoLog.Printf("ProcGetUserBaseCache key=%s value=%s", key, value)
			switch key {
			case UserNameField:
				baseInfo.UserName = value
			case UserTypeField:
				tempUserType, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint userTyep=%s err=%s failed", value, err)
					continue
				}
				baseInfo.UserType = uint32(tempUserType)
			case UserNickNameField:
				baseInfo.UserNickName = value
			case FullpyField:
				baseInfo.Fullpy = value
			case ShortFullpyField:
				baseInfo.ShortFullpy = value
			case SexField:
				tempSex, err := strconv.ParseInt(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint sex=%s err=%s failed", value, err)
					continue
				}
				baseInfo.Sex = uint32(tempSex)
			case BirthdayField:
				baseInfo.Birthday = value
			case SignatureField:
				baseInfo.Signature = value
			case HeadUrlField:
				baseInfo.HeadUrl = value
			case VoiceUrlField:
				baseInfo.VoiceUrl = value
			case VoiceDurationField:
				tempDuration, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint VoiceDuration=%s err=%s failed", value, err)
					continue
				}
				baseInfo.VoiceDuration = uint32(tempDuration)
			case NationalityField:
				baseInfo.Nationality = value
			case TimeZoneField:
				tempTimeZone, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TimeZone=%s err=%s failed", value, err)
					continue
				}
				baseInfo.TimeZone = uint8(tempTimeZone)
			case SummerTimeField:
				tempSummerTime, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint SummerTime=%s err=%s failed", value, err)
					continue
				}
				baseInfo.SummerTime = uint8(tempSummerTime)
			case TimeZone48Field:
				tempTimeZone48, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TimeZone48=%s err=%s failed", value, err)
					continue
				}
				baseInfo.TimeZone48 = uint8(tempTimeZone48)

			case AllowCountField:
				tempAllCount, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TimeZone48=%s err=%s failed", value, err)
					continue
				}
				baseInfo.AllowCount = uint32(tempAllCount)

			case NativeLangField:
				tempNativeLang, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint NativeLang=%s err=%s failed", value, err)
					continue
				}
				baseInfo.NativeLang = uint32(tempNativeLang)

			case TeachLang2Field:
				tempTeachLang2, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TeachLang2=%s err=%s failed", value, err)
					continue
				}
				baseInfo.TeachLang2 = uint32(tempTeachLang2)

			case TeachLang2LevelField:
				tempTeachLang2Level, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TeachLang2Level=%s err=%s failed", value, err)
					continue
				}
				baseInfo.TeachLang2Level = uint32(tempTeachLang2Level)

			case TeachLang3Field:
				tempTeachLang3, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TeachLang3=%s err=%s failed", value, err)
					continue
				}
				baseInfo.TeachLang3 = uint32(tempTeachLang3)
			case TeachLang3LevelField:
				tempTeachLang3Level, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TeachLang3Level=%s err=%s failed", value, err)
					continue
				}
				baseInfo.TeachLang3Level = uint32(tempTeachLang3Level)

			case LearnLang1Field:
				tempLearnLang1, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang1=%s err=%s failed", value, err)
					continue
				}
				baseInfo.LearnLang1 = uint32(tempLearnLang1)

			case LearnLang1LevelField:
				tempLearnLang1Level, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang1Level=%s err=%s failed", value, err)
					continue
				}
				baseInfo.LearnLang1Level = uint32(tempLearnLang1Level)

			case LearnLang2Field:
				tempLearnLang2, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang2=%s err=%s failed", value, err)
					continue
				}
				baseInfo.LearnLang2 = uint32(tempLearnLang2)

			case LearnLang2LevelField:
				tempLearnLang2Level, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang2Level=%s err=%s failed", value, err)
					continue
				}
				baseInfo.LearnLang2Level = uint32(tempLearnLang2Level)

			case LearnLang3Field:
				tempLearnLang3, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang3=%s err=%s failed", value, err)
					continue
				}
				baseInfo.LearnLang3 = uint32(tempLearnLang3)

			case LearnLang3LevelField:
				tempLearnLang3Level, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang3Level=%s err=%s failed", value, err)
					continue
				}
				baseInfo.LearnLang3Level = uint32(tempLearnLang3Level)

			case LearnLang4Field:
				tempLearnLang4, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang4=%s err=%s failed", value, err)
					continue
				}
				baseInfo.LearnLang4 = uint32(tempLearnLang4)

			case LearnLang4LevelField:
				tempLearnLang4Level, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang4Level=%s err=%s failed", value, err)
					continue
				}
				baseInfo.LearnLang4Level = uint32(tempLearnLang4Level)

			case HideLocationField:
				tempHideLocation, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint HideLocation=%s err=%v failed", value, err)
					continue
				}
				baseInfo.HideLocation = uint32(tempHideLocation)
			case HideCityField:
				tempHideCity, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint HideCity=%s err=%v failed", value, err)
					continue
				}
				baseInfo.HideCity = uint32(tempHideCity)

			case HideAgeField:
				tempHideAge, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint HideAge=%s err=%v failed", value, err)
					continue
				}
				baseInfo.HideAge = uint32(tempHideAge)

			case HideOnlineField:
				tempHideOnline, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint HideOnline =%s err=%v failed", value, err)
					continue
				}
				baseInfo.HideOnline = uint32(tempHideOnline)
			case ProfileVerField:
				tempProfileVer, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint ProfileVer=%s err=%v failed", value, err)
					continue
				}
				baseInfo.ProfileVer = uint64(tempProfileVer)
			case FrinedVerField:
				tempFriendVer, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint FrinedVer =%s err=%v failed", value, err)
					continue
				}
				baseInfo.FriendVer = uint64(tempFriendVer)
			case BlackListVerField:
				tempBlackListVer, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint BlackListVer=%s err=%v failed", value, err)
					continue
				}
				baseInfo.BlackListVer = uint64(tempBlackListVer)
			case LocationVerField:
				tempLocationVer, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LocationVer=%s err=%v failed", value, err)
					continue
				}
				baseInfo.LocationVer = uint64(tempLocationVer)
			case InviteVerField:
				tempInviteVer, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint InviteVer=%s err=%v failed", value, err)
					continue
				}
				baseInfo.InviteVer = uint64(tempInviteVer)
			case LastGetOffMsgVerField:
				tempLastGetOffMsgVer, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LastGetOffMsgVer=%s err=%v failed", value, err)
					continue
				}
				baseInfo.LastGetOffMsgVer = int64(tempLastGetOffMsgVer)
			case HeadModifyVerField:
				tempHeadModifyVer, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint HeadModifyVer=%s err=%v failed", value, err)
					continue
				}
				baseInfo.HeadModifyVer = int64(tempHeadModifyVer)
			case LastLessonVerField:
				tempLessonVer, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LastLessonVerField=%s err=%v failed", value, err)
					continue
				}
				baseInfo.LastLessonVer = int64(tempLessonVer)

			case LastWalletVerField:
				tempWalletVer, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LastWalletVerField=%s err=%v failed", value, err)
					continue
				}
				baseInfo.WalletVer = int64(tempWalletVer)
			}
		}

		//infoLog.Printf("ProcGetUserBaseCache uid=%v name=%s user_type=%v nickName=%v sex=%v National=%s NativeLang=%v",
		//	head.Uid,
		//	baseInfo.UserName,
		//	baseInfo.UserType,
		//	baseInfo.UserNickName,
		//	baseInfo.Sex,
		//	baseInfo.Nationality,
		//	baseInfo.NativeLang)

		rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			BaseCache: &ht_base_cache.UserBaseCache{
				Uid:              proto.Uint32(head.Uid),
				UserName:         proto.String(baseInfo.UserName),
				UserType:         proto.Uint32(baseInfo.UserType),
				NickName:         proto.String(baseInfo.UserNickName),
				Fullpy:           proto.String(baseInfo.Fullpy),
				ShortFullpy:      proto.String(baseInfo.ShortFullpy),
				Sex:              proto.Uint32(baseInfo.Sex),
				Birthday:         proto.String(baseInfo.Birthday),
				Signature:        proto.String(baseInfo.Signature),
				HeadUrl:          proto.String(baseInfo.HeadUrl),
				VoiceUrl:         proto.String(baseInfo.VoiceUrl),
				VoiceDuration:    proto.Uint32(baseInfo.VoiceDuration),
				Nationality:      proto.String(baseInfo.Nationality),
				TimeZone:         proto.Uint32(uint32(baseInfo.TimeZone)),
				SummerTime:       proto.Uint32(uint32(baseInfo.SummerTime)),
				TimeZone_48:      proto.Uint32(uint32(baseInfo.TimeZone48)),
				AllowCount:       proto.Uint32(baseInfo.AllowCount),
				NativeLang:       proto.Uint32(baseInfo.NativeLang),
				TeachLang2:       proto.Uint32(baseInfo.TeachLang2),
				TeachLang2Level:  proto.Uint32(baseInfo.TeachLang2Level),
				TeachLang3:       proto.Uint32(baseInfo.TeachLang3),
				TeachLang3Level:  proto.Uint32(baseInfo.TeachLang3Level),
				LearnLang1:       proto.Uint32(baseInfo.LearnLang1),
				LearnLang1Level:  proto.Uint32(baseInfo.LearnLang1Level),
				LearnLang2:       proto.Uint32(baseInfo.LearnLang2),
				LearnLang2Level:  proto.Uint32(baseInfo.LearnLang2Level),
				LearnLang3:       proto.Uint32(baseInfo.LearnLang3),
				LearnLang3Level:  proto.Uint32(baseInfo.LearnLang3Level),
				LearnLang4:       proto.Uint32(baseInfo.LearnLang4),
				LearnLang4Level:  proto.Uint32(baseInfo.LearnLang4Level),
				HideLocation:     proto.Uint32(baseInfo.HideLocation),
				HideCity:         proto.Uint32(baseInfo.HideCity),
				HideAge:          proto.Uint32(baseInfo.HideAge),
				HideOnline:       proto.Uint32(baseInfo.HideOnline),
				ProfileVer:       proto.Uint64(baseInfo.ProfileVer),
				FriendVer:        proto.Uint64(baseInfo.FriendVer),
				BlackListVer:     proto.Uint64(baseInfo.BlackListVer),
				LocationVer:      proto.Uint64(baseInfo.LocationVer),
				InviteVer:        proto.Uint64(baseInfo.InviteVer),
				LastGetOffMsgVer: proto.Uint64(uint64(baseInfo.LastGetOffMsgVer)),
				HeadModifyVer:    proto.Uint64(uint64(baseInfo.HeadModifyVer)),
				LastLessonVer:    proto.Uint64(uint64(baseInfo.LastLessonVer)),
				LastWalletVer:    proto.Uint64(uint64(baseInfo.WalletVer)),
			},
		}
		return true
	} else {
		infoLog.Printf("ProcGetUserBaseCache redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "gobalogic/get_user_base_cache_miss"
		libcomm.AttrAdd(attr, 1)
		// Step2 读取Master没有命中则继续读取dbd
		dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserBaseCache cache miss reload from db failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobalogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
		if !ok { // 不是HeadV2Packet报文
			infoLog.Printf("ProcGetUserBaseCache dbpacket can not change to HeadV2packet uid=%v", head.Uid)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobalogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbRespHead, err := dbPacketHeadV2.GetHead()
		if err != nil {
			//SendResp(c, head, uint16(ERR_INVALID_PARAM))
			infoLog.Printf("ProcGetUserBaseCache dbpacket Get head failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobalogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		// Step3 读取dbd之后立即返回响应
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
		bNeedCall = false
		SendRspPacket(c, dbPacketHeadV2)
		// Step4 从dbd读取回结果后发送请求到cache_write_agent_master 和 cache_write_agent_slave
		reqPayLoad, err := ChangeGetBaseCacheRspToSetReq(dbRespHead, dbPacketHeadV2.GetBody())
		if err != nil {
			infoLog.Printf("ProcGetUserBaseCache ChangeGetRspToSetReq faild uid=%v err=%s", dbRespHead.Uid, err)
			// bNeedCall already false
			return false
		}
		head.Cmd = uint32(ht_base_cache.CMD_TYPE_CMD_SET_USER_BASE_CACHE)
		UpdateRedisCache(head, reqPayLoad)
	}
	return true
}

func GetHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#base", uid)
	return hashName
}

func ChangeGetBaseCacheRspToSetReq(rspHead *common.HeadV2, rspPayLoad []byte) (reqPayLoad []byte, err error) {
	if rspHead == nil || len(rspPayLoad) == 0 {
		infoLog.Printf("ChangeGetRspToSetReq rspPayLoad invalid param respHead=%#v rspPayLoad=%v", rspHead, rspPayLoad)
		err = ErrInvalidParam
		return reqPayLoad, err
	}
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	err = proto.Unmarshal(rspPayLoad, rspBody)
	if err != nil {
		infoLog.Printf("ChangeGetRspToSetReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", rspHead.Uid, rspHead.Cmd, rspHead.Seq)
		return reqPayLoad, err
	}
	subRspBody := rspBody.GetGetUserBaseCacheRspbody()
	if subRspBody == nil {
		infoLog.Printf("ChangeGetRspToSetReq GetGetUserBaseCacheRspbody() failed uid=%v", rspHead.Uid)
		err = ErrProtoBuff
		return reqPayLoad, err
	}

	if subRspBody.GetStatus().GetCode() != uint32(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ChangeGetRspToSetReq subRspBody.GetStatus().GetCode()=%v not success uid=%v", subRspBody.GetStatus().GetCode(), rspHead.Uid)
		err = ErrProtoBuff
		return reqPayLoad, err
	}
	reqBody := &ht_base_cache.BaseCacheReqBody{
		SetUserBaseCacheReqbody: &ht_base_cache.SetUserBaseCacheReqBody{
			BaseCache: subRspBody.GetBaseCache(),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v err=%s", rspHead.Uid, err)
	}
	return reqPayLoad, err
}

func UpdateRedisCache(head *common.HeadV2, reqPayLoad []byte) {
	if head == nil || len(reqPayLoad) == 0 {
		infoLog.Printf("UpdateRedisCache input param error")
		return
	}
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("UpdateRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)
			} else {
				infoLog.Printf("UpdateRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)
			}
		} else {
			infoLog.Printf("UpdateRedisCache redisPacket can not change to HeadV2packet")
		}
	} else {
		infoLog.Printf("UpdateRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
	}

	redisPacket, err = writeAgentslaveApi.SendAndRecvPacket(head, reqPayLoad)
	if err == nil {
		redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisSlavePacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("UpdateRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)
			} else {
				infoLog.Printf("UpdateRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)
			}
		} else {
			infoLog.Printf("UpdateRedisCache redisPacket can not change to HeadV2packet")
		}
	} else {
		infoLog.Printf("UpdateRedisCache writeAgentSlaveApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
	}
	return
}

// 2.proc reload user base cache
func ProcRelodUserBaseCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcRelodUserBaseCache not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcRelodUserBaseCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/reload_user_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcRelodUserBaseCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetReloadUserBaseCacheReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcRelodUserBaseCache GetReloadUserBaseCacheReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcRelodUserBaseCache reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/reload_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcRelodUserBaseCache dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/reload_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcRelodUserBaseCache dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/reload_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcRelodUserBaseCache dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 3.proc update user name
func ProcUpdateUserName(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserName not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserName invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_user_name_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserName proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserNameReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserName GetGetUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserName reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_name_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserName dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_name_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserName dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_name_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserName dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 4.proc update user summer time
func ProcUpdateUserSummerTime(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserSummerTime not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserSummerTimeRspbody = &ht_base_cache.UpdateUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserSummerTime invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_user_summertime_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserSummerTime proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_base_cache.UpdateUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserSummerTimeReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserSummerTime GetGetUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_base_cache.UpdateUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserSummerTime reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_base_cache.UpdateUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_summertime_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserSummerTime dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_base_cache.UpdateUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_summertime_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserSummerTime dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserSummerTimeRspbody = &ht_base_cache.UpdateUserSummerTimeRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_summertime_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserSummerTime dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 5.proc update user time zone
func ProcUpdateUserTimeZone(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserTimeZone not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserTimeZoneRspbody = &ht_base_cache.UpdateUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserTimeZone invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_user_timezone_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserTimeZone proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_base_cache.UpdateUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserTimeZoneReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserTimeZone GetGetUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_base_cache.UpdateUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserTimeZone update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_base_cache.UpdateUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_timezone_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserTimeZone dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_base_cache.UpdateUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_timezone_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserTimeZone dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserTimeZoneRspbody = &ht_base_cache.UpdateUserTimeZoneRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_timezone_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserTimeZone dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 6.proc get user nick name
func ProcGetUserNickName(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserNickName not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserNickNameRspbody = &ht_base_cache.GetUserNickNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserNickName invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/get_user_nick_name_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserNickName proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserNickNameRspbody = &ht_base_cache.GetUserNickNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserNickNameReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcGetUserNickName GetGetUserNickNameReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserNickNameRspbody = &ht_base_cache.GetUserNickNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := GetHashMapName(head.Uid)
	exists, err := redisMasterApi.Exists(hashName)
	if err == nil && exists == true {
		// Step1 存在则读取完成的记录
		field := []string{
			UserNameField,
			UserNickNameField,
		}
		values, err := redisMasterApi.Hmget(hashName, field)
		valueLen := len(values)
		if err != nil || valueLen != 2 {
			infoLog.Printf("ProcGetUserNickName redisMasterApi.Hmget hashName=%s valueLen=%v err=%s faield", hashName, valueLen, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.GetUserNickNameRspbody = &ht_base_cache.GetUserNickNameRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis exec error"),
				},
			}
			attr := "gobalogic/get_user_nick_name_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		infoLog.Printf("ProcGetUserNickName uid=%v userName=%s nickName=%s",
			head.Uid,
			values[0],
			values[1])

		rspBody.GetUserNickNameRspbody = &ht_base_cache.GetUserNickNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			UserName: proto.String(values[0]),
			NickName: proto.String(values[1]),
		}
		return true
	} else {
		infoLog.Printf("ProcGetUserNickName redisMasterApi.Exists hashName=%s return err=%s exists=%v",
			hashName,
			err,
			exists)

		attr := "gobalogic/get_user_nick_name_cache_miss"
		libcomm.AttrAdd(attr, 1)
		// Step2 读取Master没有命中则继续读取dbd
		dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserNickName cache miss reload from db failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserNickNameRspbody = &ht_base_cache.GetUserNickNameRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobalogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
		if !ok { // 不是HeadV2Packet报文
			infoLog.Printf("ProcGetUserNickName dbpacket can not change to HeadV2packet uid=%v", head.Uid)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserNickNameRspbody = &ht_base_cache.GetUserNickNameRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobalogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbRespHead, err := dbPacketHeadV2.GetHead()
		if err != nil {
			//SendResp(c, head, uint16(ERR_INVALID_PARAM))
			infoLog.Printf("ProcGetUserNickName dbpacket Get head failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserNickNameRspbody = &ht_base_cache.GetUserNickNameRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobalogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		// Step3 读取dbd之后立即返回响应
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
		bNeedCall = false
		SendRspPacket(c, dbPacketHeadV2)
		// Step4 从dbd读取回结果后发送请求到cache_write_agent_master 和 cache_write_agent_slave
		reqPayLoad, err := ChangeGetNickNameRspToSetNickNameReq(dbRespHead, dbPacketHeadV2.GetBody())
		if err != nil {
			infoLog.Printf("ProcGetUserNickName ChangeGetNickNameRspToSetNickNameReq faild uid=%v err=%s", dbRespHead.Uid, err)
			// bNeedCall already false
			return false
		}
		head.Cmd = uint32(ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_NICK_NAME)
		UpdateRedisCache(head, reqPayLoad)
	}
	return true
}

func ChangeGetNickNameRspToSetNickNameReq(rspHead *common.HeadV2, rspPayLoad []byte) (reqPayLoad []byte, err error) {
	if rspHead == nil || len(rspPayLoad) == 0 {
		infoLog.Printf("ChangeGetNickNameRspToSetNickNameReq rspPayLoad invalid param respHead=%#v rspPayLoad=%v", rspHead, rspPayLoad)
		err = ErrInvalidParam
		return reqPayLoad, err
	}
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	err = proto.Unmarshal(rspPayLoad, rspBody)
	if err != nil {
		infoLog.Printf("ChangeGetNickNameRspToSetNickNameReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", rspHead.Uid, rspHead.Cmd, rspHead.Seq)
		return reqPayLoad, err
	}
	subRspBody := rspBody.GetGetUserNickNameRspbody()
	if subRspBody == nil {
		infoLog.Printf("ChangeGetNickNameRspToSetNickNameReq GetGetUserBaseCacheRspbody() failed uid=%v", rspHead.Uid)
		err = ErrProtoBuff
		return reqPayLoad, err
	}

	if subRspBody.GetStatus().GetCode() != uint32(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		infoLog.Printf("ChangeGetNickNameRspToSetNickNameReq subRspBody.GetStatus().GetCode()=%v not success uid=%v", subRspBody.GetStatus().GetCode(), rspHead.Uid)
		err = ErrProtoBuff
		return reqPayLoad, err
	}
	reqBody := &ht_base_cache.BaseCacheReqBody{
		SetUserNickNameReqbody: &ht_base_cache.SetUserNickNameReqBody{
			UserName: proto.String(subRspBody.GetUserName()),
			NickName: proto.String(subRspBody.GetNickName()),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v err=%s", rspHead.Uid, err)
	}
	return reqPayLoad, err
}

// 7.proc update user private
func ProcUpdateUserPrivate(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserPrivate not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserPrivateRspbody = &ht_base_cache.UpdateUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserPrivate invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_user_private_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPrivate proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_base_cache.UpdateUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserPrivateReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserPrivate GetUpdateUserPrivateReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_base_cache.UpdateUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserPrivate reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_base_cache.UpdateUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_private_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserPrivate dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_base_cache.UpdateUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_private_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserPrivate dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPrivateRspbody = &ht_base_cache.UpdateUserPrivateRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_private_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserPrivate dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 8.proc update user profile
func ProcUpdateUserProfile(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserProfile not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserProfileRspbody = &ht_base_cache.UpdateUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserProfile invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_user_profile_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProfile proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_base_cache.UpdateUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserProfileReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserProfile GetUpdateUserProfileReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_base_cache.UpdateUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProfile reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_base_cache.UpdateUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_profile_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserProfile dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_base_cache.UpdateUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_profile_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserProfile dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserProfileRspbody = &ht_base_cache.UpdateUserProfileRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_profile_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserProfile dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 9.proc update learn multi lang
func ProcUpdateLearnMultiLang(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateLearnMultiLang not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateLearnMultiLangRspbody = &ht_base_cache.UpdateLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateLearnMultiLang invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_learn_multi_lang_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateLearnMultiLang proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_base_cache.UpdateLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateLearnMultiLangReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateLearnMultiLang GetUpdateLearnMultiLangReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_base_cache.UpdateLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateLearnMultiLang reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_base_cache.UpdateLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_learn_multi_lang_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateLearnMultiLang dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_base_cache.UpdateLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_learn_multi_lang_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateLearnMultiLang dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateLearnMultiLangRspbody = &ht_base_cache.UpdateLearnMultiLangRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_learn_multi_lang_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateLearnMultiLang dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 10.proc update user language sertting
func ProcUpdateUserLangSetting(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserLangSetting not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserLangSettingRspbody = &ht_base_cache.UpdateUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserLangSetting invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_user_lang_setting_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLangSetting proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_base_cache.UpdateUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserLangSettingReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserLangSetting GetUpdateUserPrivateReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_base_cache.UpdateUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserLangSetting reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_base_cache.UpdateUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_lang_setting_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserLangSetting dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_base_cache.UpdateUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_lang_setting_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserLangSetting dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserLangSettingRspbody = &ht_base_cache.UpdateUserLangSettingRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_lang_setting_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserLangSetting dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 11.proc update user property
func ProcUpdateUserProperty(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserProperty not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserPropertyRspbody = &ht_base_cache.UpdateUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserProperty invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_user_property_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProperty proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_base_cache.UpdateUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserPropertyReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserProperty GetUpdateUserPropertyReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_base_cache.UpdateUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserProperty reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_base_cache.UpdateUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_property_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserProperty dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_base_cache.UpdateUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_usr_property_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserProperty dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserPropertyRspbody = &ht_base_cache.UpdateUserPropertyRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_property_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserProperty dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 12.proc update user create lesson version
func ProcUpdateCreateLessonVer(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateCreateLessonVer not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserCreateLessonRspbody = &ht_base_cache.UpdateUserCreateLessonRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateCreateLessonVer invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_create_lesson_ver"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateCreateLessonVer proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserCreateLessonRspbody = &ht_base_cache.UpdateUserCreateLessonRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserCreateLessonReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateCreateLessonVer GetUpdateUserCreateLessonReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserCreateLessonRspbody = &ht_base_cache.UpdateUserCreateLessonRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateCreateLessonVer reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserCreateLessonRspbody = &ht_base_cache.UpdateUserCreateLessonRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_lesson_ver_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateCreateLessonVer dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserCreateLessonRspbody = &ht_base_cache.UpdateUserCreateLessonRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_usr_lesson_ver_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateCreateLessonVer dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserCreateLessonRspbody = &ht_base_cache.UpdateUserCreateLessonRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_lesson_ver_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateCreateLessonVer dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 13.proc update user wallet version
func ProcUpdateUserWalletVer(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserWalletVer not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserWalletVerRspbody = &ht_base_cache.UpdateUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserWalletVer invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/update_usr_wallet_ver"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserWalletVer proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_base_cache.UpdateUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserWalletVerReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserWalletVer GetUpdateUserWalletVerReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_base_cache.UpdateUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserWalletVer reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_base_cache.UpdateUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_wallet_ver_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserWalletVer dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_base_cache.UpdateUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_usr_wallet_ver_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserWalletVer dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.UpdateUserWalletVerRspbody = &ht_base_cache.UpdateUserWalletVerRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/update_user_wallet_ver_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcUpdateUserWalletVer dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

// 14.proc del user base cache
func ProcDelUserBaseCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcDelUserBaseCache not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.DelUserBaseCacheRspbody = &ht_base_cache.DelUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelUserBaseCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobalogic/del_base_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelUserBaseCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelUserBaseCacheRspbody = &ht_base_cache.DelUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelUserBaseCacheReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcDelUserBaseCache GetDelUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.DelUserBaseCacheRspbody = &ht_base_cache.DelUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcDelUserBaseCache reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.DelUserBaseCacheRspbody = &ht_base_cache.DelUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/del_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcDelUserBaseCache dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.DelUserBaseCacheRspbody = &ht_base_cache.DelUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/del_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcDelUserBaseCache dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.DelUserBaseCacheRspbody = &ht_base_cache.DelUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobalogic/del_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcDelUserBaseCache dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_base_cache.BaseCacheRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%s port=%s", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%s port=%s", slaveIp, slavePort)
	writeAgentslaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustString("6379")
	infoLog.Printf("cache dbd ip=%v port=%v", dbdIp, dbdPort)
	dbdAgentApi = common.NewSrvToSrvApiV2(dbdIp, dbdPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
