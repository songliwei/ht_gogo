package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_basecache"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	Cmd int `short:"t" long:"cmd" description:"Command type" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	v2Protocol := &common.HeadV2Protocol{}
	var head *common.HeadV2
	head = &common.HeadV2{
		Version:  4,
		Cmd:      1,
		Seq:      1,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      1946612,
	}

	var payLoad []byte
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	switch options.Cmd {
	// 获取用户基本信息
	case 1:
		head.Cmd = 1
		subReqBody := &ht_base_cache.GetUserBaseCacheReqBody{}
		reqBody.GetUserBaseCacheReqbody = subReqBody
	case 3:
		head.Cmd = 3
		subReqBody := &ht_base_cache.ReloadUserBaseCacheReqBody{}
		reqBody.ReloadUserBaseCacheReqbody = subReqBody
	case 4:
		head.Cmd = 4
		subReqBody := &ht_base_cache.UpdateUserNameReqBody{
			UserName: proto.String("songlw"),
		}
		reqBody.UpdateUserNameReqbody = subReqBody
	case 6:
		head.Cmd = 6
		subReqBody := &ht_base_cache.UpdateUserSummerTimeReqBody{
			SummerTime: proto.Uint32(2),
		}
		reqBody.UpdateUserSummerTimeReqbody = subReqBody
	case 8:
		head.Cmd = 8
		subReqBody := &ht_base_cache.UpdateUserTimeZoneReqBody{
			TimeZone: proto.Uint32(4),
		}
		reqBody.UpdateUserTimeZoneReqbody = subReqBody
	case 10:
		head.Cmd = 10
		subReqBody := &ht_base_cache.GetUserNickNameReqBody{}
		reqBody.GetUserNickNameReqbody = subReqBody
	case 13:
		head.Cmd = 13
		subReqBody := &ht_base_cache.UpdateUserPrivateReqBody{
			HideLocation: proto.Uint32(1),
			HideCity:     proto.Uint32(1),
			HideAge:      proto.Uint32(1),
			HideOnline:   proto.Uint32(0),
		}
		reqBody.UpdateUserPrivateReqbody = subReqBody
	default:
		infoLog.Println("UnKnow input cmd =", options.Cmd)
	}

	payLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v cmd=%v seq=%v",
			head.Uid,
			head.Cmd,
			head.Seq)
		return
	}

	head.Len = uint32(common.PacketV2HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV2ToSlice failed")
		return
	}
	copy(buf[common.PacketV2HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV2MagicEnd

	infoLog.Printf("len=%v payLaod=%v\n", len(payLoad), payLoad)
	// write
	conn.Write(buf)
	// read
	p, err := v2Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket, ok := p.(*common.HeadV2Packet)
		if !ok { // 不是HeadV3Packet报文
			infoLog.Printf("packet can not change to HeadV2packet")
			return
		}
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("resp len=%v cmd=%v uid=%v\n", rspHead.Len, rspHead.Cmd, rspHead.Uid)
		rspBody := &ht_base_cache.BaseCacheRspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Println("proto Unmarshal failed")
			return
		}
		switch rspHead.Cmd {
		case 1:
			subRspBody := rspBody.GetGetUserBaseCacheRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetUserBaseCache rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
			baseInfo := subRspBody.GetBaseCache()
			infoLog.Printf("GetUserBaseCache=%#v", *baseInfo)
		case 3:
			subRspBody := rspBody.GetReloadUserBaseCacheRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("ReloadUserBaseCache rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 4:
			subRspBody := rspBody.GetUpdateUserNameRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserName rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 6:
			subRspBody := rspBody.GetUpdateUserSummerTimeRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserSummerTime rsp code=%v msg=%s profileTs=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetProfileTs())
		case 8:
			subRspBody := rspBody.GetUpdateUserTimeZoneRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserTimeZone rsp code=%v msg=%s profileTs=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetProfileTs())
		case 10:
			subRspBody := rspBody.GetGetUserNickNameRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetUserNickName rsp code=%v msg=%s userName=%s nickName=%s",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetUserName(),
				subRspBody.GetNickName())
		case 13:
			subRspBody := rspBody.GetUpdateUserPrivateRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateUserPrivate rsp code=%v msg=%s profileTs=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetProfileTs())
		default:
			infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
		}

	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
