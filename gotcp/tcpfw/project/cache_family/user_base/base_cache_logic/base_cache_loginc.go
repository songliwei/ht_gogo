package main

import (
	"fmt"

	"github.com/HT_GOGO/gotcp"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/seefan/gossdb"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/project/moment/util"
	mgo "gopkg.in/mgo.v2"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog             *log.Logger
	writeAgentMastreApi *SrvToSrvApiV2
	writeAgentslaveApi  *SrvToSrvApiV2
	dbdAgentApi         *SrvToSrvApiV2
	redisMasterApi      *common.RedisApi
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
)

// alias to Util.BaseInfoStore
type baseInfoStru struct {
	// HT_USER_BASE
	UserName      string //用户名
	UserType      uint32 //用户类别：0-普通用户
	UserNickName  string //用户别名 昵称
	Fullpy        string //用户别名的拼音
	ShortFullpy   string //用户别名的拼音缩写
	Sex           uint32 //性别
	Birthday      uint32 //生日
	Signature     string //签名
	HeadUrl       string //用户头像URL
	VoiceUrl      string //用户语音介绍的URL
	VoiceDuration uint32 //语音介绍录音的时长
	Nationality   string //用户国籍英文简称
	TimeZone      uint8  //用户所在时区(24时区制)
	SummerTime    uint8  //夏令制设置
	TimeZone48    uint8  //用户时区(半时区为单位 -24 到 24)

	// HT_USER_LANGUAGE
	AllowCount uint32 //可以教学的语言数量(使用权限)
	NativeLang uint32 //母语 语言语种使用数字代表
	TeachLang2 uint32 //教学语言二
	TeachLang3 uint32 //教学语言三
	LearnLang1 uint32 //学习语言一
	LearnLang2 uint32 //学习语言二
	LearnLang3 uint32 //学习语言三
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		return false
	}

	// 统计总的请求量
	attr := "gobaselogic/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch int(head.Cmd) {
	case ht_base_cache.CMD_TYPE_CMD_GET_USER_BASE_CACHE:
		go ProcGetUserBaseCache(c, head, packet.GetBody())
	case ht_base_cache.CMD_TYPE_CMD_RELOAD_USER_BASE_CACHE:
		go ProcRelodUserBaseCache(c, head, packet.GetBody())
	case ht_base_cache.CMD_TYPE_CMD_UPDATE_USER_NAME:
		go ProcUpdateUserName(c, head, packet.GetBody())
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

// 1.proc store moment info
func ProcGetUserBaseCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall = true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserBaseCache not need call")
		}
	}()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserBaseCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gobaselogic/get_user_base_cache_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gobaselogic/get_user_base_cache_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err = proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserBaseCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserBaseCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserBaseCache GetGetUserBaseCacheReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := GetHashMapName(head.Uid)
	err, ret := redisMasterApi.Exists(hashName)
	if err == nil || result == true {
		// Step1 存在则读取完成的记录
		var baseInfo baseInfoStru
		keyValue, err := redisMasterApi.Hgetall(hashName)
		if err != nil {
			infoLog.Printf("ProcGetUserBaseCache redisMasterApi.Hgetall hashName=%s err=%s faield", hashName, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_REDIS_ERR)
			rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("redis exec error"),
				},
			}
			attr := "gobaselogic/get_user_base_cache_redis_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		for key, value := range keyValue {
			infoLog.Printf("ProcGetUserBaseCache key=%s value=%s", key, value)
			switch key {
			case UserNameField:
				baseInfo.UserName = value
			case UserTypeField:
				tempUserType, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint userTyep=%s err=%s failed", value, err)
					break
				}
				baseInfo.UserType = uint32(tempUserType)
			case UserNickNameField:
				baseInfo.UserNickName = value
			case FullpyField:
				baseInfo.Fullpy = value
			case ShortFullpyField:
				baseInfo.ShortFullpy = value
			case SexField:
				tempSex, err := strconv.ParseInt(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint sex=%s err=%s failed", value, err)
					break
				}
				baseInfo.Sex = uint32(tempSex)
			case BirthdayField:
				tempBirthday, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint birthDay=%s err=%s failed", value, err)
					break
				}
				baseInfo.Birthday = uint32(tempBirthday)
			case SignatureField:
				baseInfo.Signature = value
			case HeadUrlField:
				baseInfo.HeadUrl = value
			case VoiceUrlField:
				baseInfo.VoiceUrl = value
			case VoiceDurationField:
				tempDuration, err := strconv.ParseUint(value, 10, 64)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint VoiceDuration=%s err=%s failed", value, err)
					break
				}
				baseInfo.VoiceDuration = tempDuration
			case NationalityField:
				baseInfo.Nationality = value
			case TimeZoneField:
				tempTimeZone, err := strconv.ParseInt(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TimeZone=%s err=%s failed", value, err)
					break
				}
				baseInfo.TimeZone = uint8(tempTimeZone)
			case SummerTimeField:
				tempSummerTime, err := strconv.ParseInt(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint SummerTime=%s err=%s failed", value, err)
					break
				}
				baseInfo.SummerTime = tempSummerTime
			case TimeZone48Field:
				tempTimeZone48, err := strconv.ParseInt(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TimeZone48=%s err=%s failed", value, err)
					break
				}
				baseInfo.TimeZone48 = tempTimeZone48

			case AllowCountField:
				tempAllCount, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TimeZone48=%s err=%s failed", value, err)
					break
				}
				baseInfo.AllowCount = tempAllCount

			case NativeLangField:
				tempNativeLang, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint NativeLang=%s err=%s failed", value, err)
					break
				}
				baseInfo.NativeLang = tempNativeLang

			case TeachLang2Field:
				tempTeachLang2, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TeachLang2=%s err=%s failed", value, err)
					break
				}
				baseInfo.TeachLang2 = tempTeachLang2

			case TeachLang3Field:
				tempTeachLang3, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint TeachLang3=%s err=%s failed", value, err)
					break
				}
				baseInfo.TeachLang3 = tempTeachLang3

			case LearnLang1Field:
				tempLearnLang1, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang1=%s err=%s failed", value, err)
					break
				}
				baseInfo.LearnLang1 = tempLearnLang1

			case LearnLang2Field:
				tempLearnLang2, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang2=%s err=%s failed", value, err)
					break
				}
				baseInfo.LearnLang2 = tempLearnLang2

			case LearnLang3Field:
				tempLearnLang3, err := strconv.ParseUint(value, 10, 32)
				if err != nil {
					infoLog.Printf("ProcGetUserBaseCache strconv.ParseUint LearnLang3=%s err=%s failed", value, err)
					break
				}
				baseInfo.LearnLang3 = tempLearnLang3
			}
		}

		infoLog.Printf("ProcGetUserBaseCache uid=%v name=%s user_type=%v nickName=%v sex=%v National=%s NativeLang=%v",
			head.Uid,
			baseInfo.UserName,
			baseInfo.UserType,
			baseInfo.NickName,
			baseInfo.Sex,
			baseInfo.Nationality,
			baseInfo.NativeLang)

		rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			BaseCache: &ht_base_cache.UserBaseCache{
				Uid:           proto.Uint32(head.Uid),
				UserName:      proto.String(baseInfo.UserName),
				UserType:      proto.Uint32(baseInfo.UserType),
				NickName:      proto.String(baseInfo.UserNickName),
				Fullpy:        proto.String(baseInfo.Fullpy),
				ShortFullpy:   proto.String(baseInfo.ShortFullpy),
				Sex:           proto.Uint32(baseInfo.Sex),
				Birthday:      proto.Uint32(baseInfo.Birthday),
				Signature:     proto.String(baseInfo.Signature),
				HeadUrl:       proto.String(baseInfo.HeadUrl),
				VoiceUrl:      proto.String(baseInfo.VoiceUrl),
				VoiceDuration: proto.String(baseInfo.VoiceDuration),
				Nationality:   proto.String(baseInfo.Nationality),
				TimeZone:      proto.Uint32(baseInfo.TimeZone),
				SummerTime:    proto.Uint32(baseInfo.SummerTime),
				TimeZone_48:   proto.Uint32(baseInfo.TimeZone48),
				AllowCount:    proto.Uint32(baseInfo.AllowCount),
				NativeLang:    proto.Uint32(baseInfo.NativeLang),
				TeachLang2:    proto.Uint32(baseInfo.TeachLang2),
				TeachLang3:    proto.Uint32(baseInfo.TeachLang3),
				LearnLang1:    proto.Uint32(baseInfo.LearnLang1),
				LearnLang2:    proto.Uint32(baseInfo.LearnLang2),
				LearnLang3:    proto.Uint32(baseInfo.LearnLang3),
			},
		}
		return true
	} else {
		infoLog.Printf("ProcGetUserBaseCache redisMasterApi.Exists hashName=%s return err=%s result=%v",
			hashName,
			err,
			ret)

		attr := "gobaselogic/get_user_base_cache_miss"
		libcomm.AttrAdd(attr, 1)
		// Step2 读取Master没有命中则继续读取dbd
		dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
		if err != nil {
			infoLog.Printf("ProcGetUserBaseCache cache miss reload from db failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobaselogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
		if !ok { // 不是HeadV2Packet报文
			infoLog.Printf("ProcGetUserBaseCache dbpacket can not change to HeadV2packet uid=%v", head.Uid)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobaselogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}

		dbRespHead, err := dbPacketHeadV2.GetHead()
		if err != nil {
			//SendResp(c, head, uint16(ERR_INVALID_PARAM))
			infoLog.Printf("ProcGetUserBaseCache dbpacket Get head failed uid=%v err=%s", head.Uid, err)
			result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
			rspBody.GetUserBaseCacheRspbody = &ht_base_cache.GetUserBaseCacheRspBody{
				Status: &ht_base_cache.BaseCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			attr := "gobaselogic/get_base_cache_inter_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		// Step3 读取dbd之后立即返回响应
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
		bNeedCall = false
		SendRspPacket(c, dbPacketHeadV2)
		// Step4 从dbd读取回结果后发送请求到cache_write_agent_master 和 cache_write_agent_slave
		reqPayLoad, err := ChangeGetRspToSetReq(dbRespHead, dbPacketHeadV2.GetBody())
		if err != nil {
			infoLog.Printf("ProcGetUserBaseCache ChangeGetRspToSetReq faild uid=%v err=%s", dbRespHead.Uid, err)
			// bNeedCall already false
			return false
		}
		redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
		if err == nil {
			redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
			if ok { // 是HeadV2Packet报文
				// head 为一个new出来的对象指针
				redisHead, err := redisMasterPacket.GetHead()
				if err == nil {
					//SendResp(c, head, uint16(ERR_INVALID_PARAM))
					infoLog.Printf("ProcGetUserBaseCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)
				} else {
					infoLog.Printf("ProcGetUserBaseCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)
				}
			} else {
				infoLog.Printf("ProcGetUserBaseCache redisPacket can not change to HeadV2packet")
			}
		} else {
			infoLog.Printf("ProcGetUserBaseCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
		}

		redisPacket, err = writeAgentslaveApi.SendAndRecvPacket(head, reqPayLoad)
		if err == nil {
			redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
			if ok { // 是HeadV2Packet报文
				// head 为一个new出来的对象指针
				redisHead, err = redisSlavePacket.GetHead()
				if err == nil {
					//SendResp(c, head, uint16(ERR_INVALID_PARAM))
					infoLog.Printf("ProcGetUserBaseCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)
				} else {
					infoLog.Printf("ProcGetUserBaseCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)
				}
			} else {
				infoLog.Printf("ProcGetUserBaseCache redisPacket can not change to HeadV2packet")
			}
		} else {
			infoLog.Printf("ProcGetUserBaseCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
		}

	}
	return true
}

func GetHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#base", uid)
}

func ChangeGetRspToSetReq(rspHead *HeadV2, rspPayLoad []byte) (reqPayLoad []byte, err error) {
	if respHead == nil || len(rspPayLoad) == 0 {
		infoLog.Printf("ChangeGetRspToSetReq rspPayLoad invalid param respHead=%#v rspPayLoad=%v", rspHead, rspPayLoad)
		err = ErrInvalidParam
		return reqPayLoad, err
	}
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	err = proto.Unmarshal(rspPayLoad, rspBody)
	if err != nil {
		infoLog.Printf("ChangeGetRspToSetReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", rspHead.Uid, rspHead.Cmd, rspHead.Seq)
		return reqPayLoad, err
	}
	subRspBody := rspBody.GetGetUserBaseCacheRspbody()
	if subReqBody == nil {
		infoLog.Printf("ChangeGetRspToSetReq GetGetUserBaseCacheRspbody() failed uid=%v", rspHead.Uid)
		err = ErrProtoBuff
		return reqPayLoad, err
	}

	if subRspBody.GetStatus().GetCode() != ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS {
		infoLog.Printf("ChangeGetRspToSetReq subRspBody.GetStatus().GetCode()=%v not success uid=%v", subRspBody.GetStatus().GetCode(), rspHead.Uid)
		err = ErrProtoBuff
		return reqPayLoad, err
	}
	reqBody := &ht_base_cache.BaseCacheReqBody{
		SetUserBaseCacheReqbody: &ht_base_cache.SetUserBaseCacheReqBody{
			BaseCache: subRspBody.GetBaseCache(),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v err=%s", rspHead.Uid, err)
	}
	return reqPayLoad, err
}

func ProcRelodUserBaseCache(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall = true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcRelodUserBaseCache not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcRelodUserBaseCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobaselogic/reload_user_cache_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err = proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcRelodUserBaseCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetReloadUserBaseCacheReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcRelodUserBaseCache GetReloadUserBaseCacheReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcRelodUserBaseCache reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobaselogic/reload_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcRelodUserBaseCache dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobaselogic/reload_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcRelodUserBaseCache dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobaselogic/reload_base_cache_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// Step3 读取dbd之后立即返回响应
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

func ProcUpdateUserName(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	// parse packet
	result := uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	rspBody := new(ht_base_cache.BaseCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateUserName not need call")
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INVALID_PARAM)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateUserName invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gobaselogic/update_user_name_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_base_cache.BaseCacheReqBody)
	err = proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateUserName proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateUserNameReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateUserName GetGetUserBaseCacheReqbody() failed")
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_PB_ERR)
		rspBody.UpdateUserNameRspbody = &ht_base_cache.UpdateUserNameRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcUpdateUserName reload from db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobaselogic/update_user_name_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcUpdateUserName dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobaselogic/update_user_name_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateUserName dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR)
		rspBody.ReloadUserBaseCacheRspbody = &ht_base_cache.ReloadUserBaseCacheRspBody{
			Status: &ht_base_cache.BaseCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gobaselogic/update_user_name_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// Step3 读取dbd之后立即返回响应
	result = uint16(ht_base_cache.CACHE_RET_CODE_CACHE_RET_SUCCESS)
	bNeedCall = false
	SendRspPacket(c, dbPacketHeadV2)
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_base_cache.BaseCacheRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	rspHead.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	rspHead.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, rspHead.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(rspHead, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[rspHead.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, rspHead.Len)
	resp := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(resp, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustInt(6379)
	infoLog.Printf("cache master ip=%v port=%v", redisIp, redisPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(ip, port, 1, 1, common.HeadV2Protocol, 100)
	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustInt(6379)
	infoLog.Printf("cache slave ip=%v port=%v", redisIp, redisPort)
	writeAgentslaveApi = common.NewSrvToSrvApiV2(ip, port, 1, 1, common.HeadV2Protocol, 100)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustInt(6379)
	infoLog.Printf("cache dbd ip=%v port=%v", redisIp, redisPort)
	dbdAgentApi = common.NewSrvToSrvApiV2(ip, port, 1, 1, common.HeadV2Protocol, 100)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
