package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"sync"
	"syscall"
	"time"
	"unicode/utf8"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/aiwuTech/xinge"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/jessevdk/go-flags"
	"github.com/nsqio/go-nsq"
	"gopkg.in/ini.v1"
)

var wg *sync.WaitGroup
var infoLog *log.Logger

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

const (
	TYPE_ACTIVITY = 1
	TYPE_URL      = 2
	TYPE_INTENT   = 3
	TYPE_PACKAGE  = 4
	MAX_WORD_LEN  = 30
)

const (
	AccessId  = "2100093818"
	AccessKey = "A39V82PU7WMC"
	SecretKey = "42deaa73626e172176fbe6a27e730de5"
)

const (
	P2P_CHAT         = "CHAT"
	MUC_CHAT         = "MUC"
	HELLO_TALK_TITLE = "HelloTalk"
)

const (
	GCM_PUSH_NOPREVIEW     = "no_preview"
	GCM_PUSH_TEXT          = "text"
	GCM_PUSH_VOICE         = "voice"
	GCM_PUSH_IMAGE         = "image"
	GCM_PUSH_LOCATION      = "location"
	GCM_PUSH_INTRODUCE     = "introduce"
	GCM_PUSH_CORRECT       = "correction"
	GCM_PUSH_STICKER       = "sticker"
	GCM_PUSH_DOODLE        = "doodle"
	GCM_PUSH_INVITE        = "friend_invite"
	GCM_PUSH_LX            = "language_exchange"
	GCM_PUSH_LX_REPLY      = "language_exchange_reply" // Reply Code "Declined" "Accepted" "Terminated"
	GCM_PUSH_GIFT          = "gift"
	GCM_PUSH_CALL_INCOMING = "call_incoming"
	GCM_PUSH_CALL_CANCEL   = "call_cancel"
	GCM_PUSH_CALL_MISS     = "call_miss"
	GCM_PUSH_ACCEPT_INVITE = "accept_invite"
	GCM_PUSH_VIDEO         = "video" // add video push
	GCM_PUSH_GVOIP         = "gvoip"
	GCM_PUSH_LINK          = "message_preview_example_no"
	GCM_PUSH_CARD          = "card"

	// 2016-08-26 add by songliwei
	GCM_PUSH_FOLLOW              = "s_has_followed_you"
	GCM_PUSH_REPLY_YOUR_COMMENT  = "s_replied_your_comment"
	GCM_PUSH_COMMENTED_YOUR_POST = "s_commented_your_post"
	GCM_PUSH_CORRECTED_YOUR_POST = "s_corrected_your_post"
	GCM_PUSH_POST_MNT            = "s_post_mnt"
	GCM_PUSH_CREATE_WHITE_BOARD  = "s_create_white_board"
	GCM_PUSH_NEW_MSG_NOTIFY      = "s_new_msg_notify"
	GMC_PUSH_GROUP_LESSON        = "s_group_lesson"
	GCM_PUSH_START_CHARGE        = "s_start_charge"
)

const (
	GCM_PUSH_NOPREVIEW_CHINESE   = "你有一条新消息"
	GCM_PUSH_VOICE_CHINESE       = "[语音信息]"
	GCM_PUSH_IMAGE_CHINESE       = "[图片]"
	GCM_PUSH_LOCATION_CHINESE    = "[位置]"
	GCM_PUSH_INTRODUCE_CHINESE   = "%s 的名片"
	GCM_PUSH_CORRECT_CHINESE     = "[修改句子错处]"
	GCM_PUSH_STICKER_CHINESE     = "[贴图表情]"
	GCM_PUSH_DOODLE_CHINESE      = "[涂鸦]"
	GCM_PUSH_GIFT_CHINESE        = "来自 %s 的礼物 "
	GCM_PUSH_CALL_CANCEL_CHINESE = "已取消通话"
	GCM_PUSH_CALL_MISS_CHINESE   = "未接来电"
	GCM_PUSH_VIDEO_CHINESE       = "[视频]" // add video push
	GCM_PUSH_GVOIP_CHINESE       = "%s: 我发起了群组通话"
	GCM_PUSH_LINK_CHINESE        = "你有一条新消息"
	GCM_PUSH_CARD_CHINESE        = "[贺卡]"

	// 2016-08-26 add by songliwei
	GCM_PUSH_FOLLOW_CHINESE              = "%s关注了你"
	GCM_PUSH_REPLY_YOUR_COMMENT_CHINESE  = "%s回复了你的评论"
	GCM_PUSH_COMMENTED_YOUR_POST_CHINESE = "%s评论了你的贴文"
	GCM_PUSH_CORRECTED_YOUR_POST_CHINESE = "%s修改了你的贴文"
	GCM_PUSH_POST_MNT_CHINESE            = "%s: 更新了动态"
	GCM_PUSH_CREATE_WHITE_BOARD_CHINESE  = "%s: 发起了群组课程"
	GCM_PUSH_NEW_MSG_NOTIFY_CHINESE      = "你有一条新消息"
	GMC_PUSH_GROUP_LESSON_CHINESE        = "%s正在上课中"
	AT_ME_CHINESE                        = "[@了我]"
	AT_ALL_ENGLISH                       = "[@all]"
	AT_ALL_CHINESE                       = "@所有人"
)

var options Options
var parser = flags.NewParser(&options, flags.Default)

var (
	xingeClient = xinge.NewClient(AccessId, 300, AccessKey, SecretKey)
)

func MessageHandle(message *nsq.Message) error {
	// log.Printf("MessageHandle Got a message: %v", message)
	// 统计总的请求量
	attr := "xinge/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	wg.Add(1)
	go func() {
		PostData(message)
		wg.Done()
	}()

	return nil
}

func Utf8StrLen(targetStr string) (count int) {
	count = utf8.RuneCountInString(targetStr)
	return count
}

func SubString(str string, begin, length int) (substr string) {
	// 将字符串的转换成[]rune
	rs := []rune(str)
	lth := len(rs)

	// 简单的越界判断
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length
	if end > lth {
		end = lth
	}

	// 返回子串
	return string(rs[begin:end])
}

func PostData(message *nsq.Message) error {
	// log.Printf("PostData enter msgId=%v timestamp=%v body=%s", message.ID, message.Timestamp, message.Body)
	// 然后进行下一步无处理
	rootObj, err := simplejson.NewJson(message.Body)
	if err != nil {
		log.Printf("PostData simplejson new packet error", err)
		return err
	}

	// log.Printf("ProcData rootObj=%#v", rootObj)
	intPushType := rootObj.Get("int_push_type").MustInt(0)
	activityAttr := &xinge.ActivityAttr{IF: 0, PF: 0}
	browser := &xinge.Browser{Confirm: 1}
	packetName := &xinge.Package{Confirm: 1}
	action := &xinge.AndroidAction{ActionType: TYPE_ACTIVITY,
		AtyAttr:     activityAttr,
		Browser:     browser,
		PackageName: packetName}
	// 自定义字段
	strPushType := rootObj.Get("msg_type").MustString("text")
	// 对于不需要的推送直接丢弃掉
	if strPushType == GCM_PUSH_INVITE ||
		strPushType == GCM_PUSH_LX ||
		strPushType == GCM_PUSH_LX_REPLY ||
		strPushType == GCM_PUSH_CALL_INCOMING ||
		strPushType == GCM_PUSH_ACCEPT_INVITE {
		log.Printf("pushType=%s rootObj=%#v drop", strPushType, rootObj)
		attr := "xinge/drop_push_count"
		libcomm.AttrAdd(attr, 1)
		return nil
	}
	prevContent := rootObj.Get("content").MustString("")
	trimContent := strings.Trim(prevContent, "\u0000")
	newPush := rootObj.Get("new_push").MustInt(0)
	title := strings.Trim(rootObj.Get("title").MustString(""), "\u0000")
	prevMsgId := rootObj.Get("msg_id").MustString("")
	trimMsgId := strings.Trim(prevMsgId, "\u0000")
	customObj := map[string]interface{}{}
	var textVal, voiceVal, pictureVal, videoVal uint8
	if strPushType == GCM_PUSH_TEXT {
		textVal = 1
	} else if strPushType == GCM_PUSH_VOICE {
		voiceVal = 1
	} else if strPushType == GCM_PUSH_IMAGE {
		pictureVal = 1
	} else if strPushType == GCM_PUSH_VIDEO {
		videoVal = 1
	}
	fromId := uint32(rootObj.Get("from_id").MustInt64(0))
	toId := uint32(rootObj.Get("to_id").MustInt64(0))
	if newPush > 0 {
		log.Printf("PostData pushType=%s newPush=%v", strPushType, newPush)
		switch strPushType {
		case GCM_PUSH_NOPREVIEW:
			title = HELLO_TALK_TITLE
			trimContent = GCM_PUSH_NOPREVIEW_CHINESE
		case GCM_PUSH_TEXT:
		case GCM_PUSH_VOICE:
			trimContent = GCM_PUSH_VOICE_CHINESE
		case GCM_PUSH_IMAGE:
			trimContent = GCM_PUSH_IMAGE_CHINESE
		case GCM_PUSH_LOCATION:
			trimContent = GCM_PUSH_LOCATION_CHINESE
		case GCM_PUSH_INTRODUCE:
			trimContent = fmt.Sprintf(GCM_PUSH_INTRODUCE_CHINESE, title)
		case GCM_PUSH_CORRECT:
			trimContent = GCM_PUSH_CORRECT_CHINESE
		case GCM_PUSH_STICKER:
			trimContent = GCM_PUSH_STICKER_CHINESE
		case GCM_PUSH_DOODLE:
			trimContent = GCM_PUSH_DOODLE_CHINESE
		case GCM_PUSH_GIFT:
			trimContent = fmt.Sprintf(GCM_PUSH_GIFT_CHINESE, title)
		case GCM_PUSH_CALL_CANCEL:
			trimContent = GCM_PUSH_CALL_CANCEL_CHINESE
		case GCM_PUSH_CALL_MISS:
			trimContent = GCM_PUSH_CALL_MISS_CHINESE
		case GCM_PUSH_VIDEO:
			trimContent = GCM_PUSH_VIDEO_CHINESE
		case GCM_PUSH_GVOIP:
			trimContent = fmt.Sprintf(GCM_PUSH_GVOIP_CHINESE, title)
			title = HELLO_TALK_TITLE
		case GCM_PUSH_LINK:
			trimContent = GCM_PUSH_LINK_CHINESE
			title = HELLO_TALK_TITLE

		case GCM_PUSH_CARD:
			trimContent = GCM_PUSH_CARD_CHINESE
		case GCM_PUSH_FOLLOW:
			trimContent = fmt.Sprintf(GCM_PUSH_FOLLOW_CHINESE, title)
			title = HELLO_TALK_TITLE
		case GCM_PUSH_REPLY_YOUR_COMMENT:
			trimContent = fmt.Sprintf(GCM_PUSH_REPLY_YOUR_COMMENT_CHINESE, title)
			title = HELLO_TALK_TITLE
		case GCM_PUSH_COMMENTED_YOUR_POST:
			trimContent = fmt.Sprintf(GCM_PUSH_COMMENTED_YOUR_POST_CHINESE, title)
			title = HELLO_TALK_TITLE
		case GCM_PUSH_CORRECTED_YOUR_POST:
			trimContent = fmt.Sprintf(GCM_PUSH_CORRECTED_YOUR_POST_CHINESE, title)
			title = HELLO_TALK_TITLE
		case GCM_PUSH_POST_MNT:
			trimContent = fmt.Sprintf(GCM_PUSH_POST_MNT_CHINESE, title)
			title = HELLO_TALK_TITLE
		case GCM_PUSH_NEW_MSG_NOTIFY:
			trimContent = GCM_PUSH_NEW_MSG_NOTIFY_CHINESE
			title = HELLO_TALK_TITLE
		case GMC_PUSH_GROUP_LESSON:
			trimContent = fmt.Sprintf(GMC_PUSH_GROUP_LESSON_CHINESE, title)
		default:
			trimContent = GCM_PUSH_NOPREVIEW_CHINESE
			title = HELLO_TALK_TITLE
			log.Printf("strPushType=%s unhandle", strPushType)

		}

		// add for new version
		customObj["sender"] = strings.Trim(rootObj.Get("title").MustString(""), "\u0000")
		customObj["message"] = strings.Trim(rootObj.Get("content").MustString(""), "\u0000")
		if strPushType == GCM_PUSH_POST_MNT {
			customObj["content"] = strings.Trim(rootObj.Get("content").MustString(""), "\u0000")
		}

		customObj["chat_type"] = rootObj.Get("chat_type").MustInt(0)
		customObj["sound"] = rootObj.Get("sound").MustInt(1)
		customObj["lights"] = rootObj.Get("lights").MustInt(1)
		customObj["vibrate"] = rootObj.Get("sound").MustInt(0)
		customObj["type"] = strPushType
		if strPushType == GCM_PUSH_INTRODUCE {
			customObj["user"] = strings.Trim(rootObj.Get("content").MustString(""), "\u0000")
		}
		customObj["msg_id"] = strings.Trim(rootObj.Get("msg_id").MustString(""), "\u0000")
		customObj["from_id"] = fromId
		customObj["to_id"] = toId
		customObj["actionid"] = uint32(rootObj.Get("actionid").MustInt64(0))
		byAt := uint32(rootObj.Get("byAt").MustInt64(0))
		customObj["byAt"] = byAt

		if Utf8StrLen(trimContent) > MAX_WORD_LEN {
			log.Printf("strPushType=%s content=%s exceed max word lengh", strPushType, trimContent)
			trimContent = SubString(trimContent, 0, MAX_WORD_LEN) + "..."
		}
		trimContent = strings.Replace(trimContent, AT_ALL_ENGLISH, AT_ALL_CHINESE, -1)
		if byAt == 1 {
			trimContent = AT_ME_CHINESE + trimContent
		}
	} else {
		customObj["chat_type"] = rootObj.Get("chat_type").MustInt(0)
		customObj["sound"] = rootObj.Get("sound").MustInt(1)
		customObj["lights"] = rootObj.Get("lights").MustInt(1)
		customObj["vibrate"] = rootObj.Get("sound").MustInt(0)
		customObj["type"] = strPushType
		if strPushType == GCM_PUSH_INTRODUCE {
			customObj["user"] = trimContent
		}
		if strPushType == GCM_PUSH_POST_MNT {
			customObj["content"] = strings.Trim(rootObj.Get("push_param").MustString(""), "\u0000")
		}
		customObj["msg_id"] = trimMsgId
		customObj["from_id"] = fromId
		customObj["to_id"] = toId
		customObj["actionid"] = uint32(rootObj.Get("actionid").MustInt64(0))
		customObj["byAt"] = uint32(rootObj.Get("byAt").MustInt64(0))
	}

	// log.Printf("procData customObj=%#v fromId=%v token=%s", customObj, uint32(rootObj.Get("from_id").MustInt64(0)), rootObj.Get("token").MustString(""))

	xingeMessage := &xinge.AndroidMessage{
		Title:         title,
		Content:       trimContent,
		AcceptTime:    []*xinge.AcceptTime{},
		NotifyId:      0,
		BuilderId:     1,
		Ring:          byte(rootObj.Get("sound").MustInt(1)),
		Vibrate:       1,
		Lights:        byte(rootObj.Get("lights").MustInt(1)),
		Clearable:     1,
		IconType:      0,
		StyleId:       1,
		Action:        action,
		CustomContent: customObj,
	}

	log.Printf("xingeMessage=%#v", *xingeMessage)
	reqPush := &xinge.ReqPush{
		PushType:    xinge.PushType_single_device,
		DeviceToken: rootObj.Get("token").MustString(""),
		MessageType: xinge.MessageType_notify,
		// MessageType:  xinge.MessageType_passthrough,
		Message:      xingeMessage,
		ExpireTime:   300,
		SendTime:     time.Now(),
		MultiPkgType: xinge.MultiPkg_aid,
		PushEnv:      xinge.PushEnv_android,
		PlatformType: xinge.Platform_android,
		LoopTimes:    2,
		LoopInterval: 7,
		Cli:          xingeClient,
	}

	err = reqPush.Push()
	if err != nil {
		// 统计信鸽失败量
		attr := "xinge/total_push_failed_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PostData reqPush.Push failed toUid=%v pushType=%s trimContentLen=%v trimMsgId=%v message.Body=%s err=%s ",
			rootObj.Get("to_id").MustInt64(0),
			strPushType,
			len(trimContent),
			len(trimMsgId),
			message.Body,
			err)
	} else {
		// 统计信鸽成功量
		attr := "xinge/total_push_success_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PostData reqPush.Push success")
	}
	WriteStaticLog(fromId, toId, intPushType, 1, textVal, voiceVal, pictureVal, videoVal)
	return nil
}

func WriteStaticLog(fromId, toId uint32, pushType int, terminalType, text, voice, picture, video uint8) {
	contentObj := simplejson.New()
	contentObj.Set("text", text)
	contentObj.Set("voice", voice)
	contentObj.Set("picture", picture)
	contentObj.Set("video", video)
	logObj := simplejson.New()
	logObj.Set("fromid", fromId)
	logObj.Set("toid", toId)
	logObj.Set("type", pushType)
	logObj.Set("terminal", terminalType)
	logObj.Set("content", contentObj)
	logObj.Set("createtime", time.Now().Unix())
	logSlic, err := logObj.MarshalJSON()
	if err != nil {
		log.Printf("WriteStaticLog fromId=%v toId=%v pushType=%v terminalType=%v type=%v voice=%v picture=%v video=%v err=%s",
			fromId,
			toId,
			pushType,
			terminalType,
			pushType,
			voice,
			picture,
			video,
			err)
		return
	}
	infoLog.Printf("%s", logSlic)
}
func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}
	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	wg = &sync.WaitGroup{}
	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
