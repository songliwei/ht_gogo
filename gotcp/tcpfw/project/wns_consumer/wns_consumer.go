package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_wns_api"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"github.com/nsqio/go-nsq"
	"gopkg.in/ini.v1"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

const (
	APPID         = 201254
	WNSTAG        = "hellotalk_tag"
	APPSECRITID   = "AKIDkXYplGJ7s24fNvdKK6ELiaBrnrhRL7Dp"
	APPSECRITKEY  = "YRR4nliGfnYCfqPhns4ZdmRuxgNTIRCb"
	WNSPOSTAPIURL = "http://wns.api.qcloud.com/api/send_msg_bin"
	//WNSPOSTAPIURL  = "http://wnslog.qcloud.com/api/send_msg_bin"
	RETRYTHRESHOLD = 3
)

const (
	ALLPLANT     = 0
	IOSPLANT     = 1
	ANDROIDPLANT = 2
)

const (
	BOTHSEND   = 0 //默认0在线离线都会发
	ONLYONLINE = 1 //表示只发当前在线用户，
)

type WnsRspDetial struct {
	Ret int    `json:"ret" binding:"required"`
	Wid uint64 `json:"wid" binding:"required"`
}

type WnsRspData struct {
	Errno  int            `json:"errno" binding:"required"`
	Msg    string         `json:"msg",omitempty`
	Detial []WnsRspDetial `json:"detail,omitempty"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

func MessageHandle(message *nsq.Message) error {
	// log.Printf("MessageHandle Got a messageid=%v", message.ID)
	go func() {
		WnsPushHandle(message)
	}()

	return nil
}

func GetWnsSign(appId uint32, timeStamp int64) (sing string) {
	plainText := fmt.Sprintf("%v&%v", appId, timeStamp)
	h := hmac.New(sha1.New, []byte(APPSECRITKEY))
	h.Write([]byte(plainText))
	sing = base64.StdEncoding.EncodeToString(h.Sum(nil))
	// log.Printf("GetWnsSign appId=%v timeStamp=%v sing=%s", appId, timeStamp, sing)
	return sing
}

func DoBytesPost(url string, data []byte) (outBody []byte, err error) {
	body := bytes.NewReader(data)
	request, err := http.NewRequest(http.MethodPost, url, body)
	if err != nil {
		log.Printf("DoBytesPost http.NewRequest,[err=%s][url=%s]", err, url)
		return outBody, err
	}
	request.Header.Set("Content-Length", fmt.Sprintf("%v", len(data)))
	request.Header.Set("Content-Type", "application/octet-stream")
	request.Header.Set("Accept", "*/*")
	request.Header.Set("Connection", "Keep-Alive")
	var resp *http.Response
	resp, err = http.DefaultClient.Do(request)
	if err != nil {
		log.Printf("http.Do failed,[err=%s][url=%s]", err, url)
		return outBody, err
	}
	defer resp.Body.Close()
	outBody, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("http.Do failed,[err=%s][url=%s]", err, url)
	}
	return outBody, err
}

//	{
//		wid:xxxx,
//		uid:xxxx,
//		timestamp:xxxxx,
//		tag:"xxxx",
//		content:"xxxxxx",
//	}
//
// expire: 0 永不过期  消息有效期，单位秒，表示从现在起经过该时间之后该消息将被丢弃
func WnsPushHandle(message *nsq.Message) error {
	// log.Printf("WnsPushHandle enter msgId=%s timestamp=%v bodyLen=%v", message.ID, message.Timestamp, len(message.Body))
	// 然后进行下一步无处理
	rootObj, err := simplejson.NewJson(message.Body)
	if err != nil {
		log.Printf("WnsPushHandle simplejson new packet error=%s", err)
		return err
	}
	// log.Printf("WnsPushHandle rootObj=%#v", rootObj)
	wid := rootObj.Get("wid").MustUint64(0)
	uid := rootObj.Get("uid").MustUint64(0)
	prevTimeStamp := rootObj.Get("timestamp").MustInt64(0)
	tag := rootObj.Get("tag").MustString("")
	content := rootObj.Get("content").MustString()
	decContent, err := base64.StdEncoding.DecodeString(content)
	if err != nil {
		log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s content=%x base64.StdEncoding.DecodeString err=%s",
			wid,
			uid,
			prevTimeStamp,
			tag,
			content,
			err)
		return err
	}
	log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s decContentLen=%v",
		wid,
		uid,
		prevTimeStamp,
		tag,
		len(decContent))

	curTimeStamp := time.Now().Unix()
	sign := GetWnsSign(APPID, curTimeStamp)
	sendMsgReq := &WNS_API.SendMsgReq{
		Comm: &WNS_API.RequestComm{
			Appid:     proto.Uint32(APPID),
			Timestamp: proto.Uint32(uint32(curTimeStamp)),
			SecretId:  proto.String(APPSECRITID),
			Sign:      proto.String(sign),
		},
		Wid:               proto.Uint64(wid),
		Uid:               nil,
		MsgTag:            proto.String(tag),
		Content:           decContent,
		Aps:               nil,
		Expire:            proto.Uint32(0),
		Plat:              proto.Uint32(ALLPLANT),
		OnlyCurrentOnline: proto.Bool(true),
	}
	sendSlic, err := proto.Marshal(sendMsgReq)
	if err != nil {
		log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s content=%s proto.Marshal failed err=%s",
			wid,
			uid,
			prevTimeStamp,
			tag,
			content,
			err)
		return err
	}
	// log.Printf("sendSlic=%x", sendSlic)
	postSucc := false
	for i := 0; i < RETRYTHRESHOLD; i += 1 {
		// resp, err := http.Post(WNSPOSTAPIURL,
		// 	"application/octet-stream",
		// 	strings.NewReader(string(sendSlic)))
		body, err := DoBytesPost(WNSPOSTAPIURL, sendSlic)
		// Step1: post failed
		if err != nil {
			log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s content=%s post err=%s",
				wid,
				uid,
				prevTimeStamp,
				tag,
				content,
				err)
			// 统计总的请求量
			attr := "wns_consumer/channel_post_failed_count"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		if len(body) == 0 {
			log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s content=%s body empty",
				wid,
				uid,
				prevTimeStamp,
				tag,
				content)
			continue
		}
		// log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s contentLen=%v bodyLen=%v",
		// 	wid,
		// 	uid,
		// 	prevTimeStamp,
		// 	tag,
		// 	len(content),
		// 	len(body))

		// try pb
		sendMsgRsp := new(WNS_API.SendMsgRsp)
		err = proto.Unmarshal(body, sendMsgRsp)
		// Step4: resp new json failed
		if err != nil {
			log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s contentLen=%v proto unmarshal error=%s",
				wid,
				uid,
				prevTimeStamp,
				tag,
				len(content),
				err)
			continue
		}
		log.Printf("WnsPushHandle sendMsgRsp={ResponseComm:{Code=%v, Msg=%s}}",
			sendMsgRsp.GetComm().GetCode(),
			sendMsgRsp.GetComm().GetMsg())
		errno := sendMsgRsp.GetComm().GetCode()
		if errno == 0 && len(sendMsgRsp.GetDetail()) > 0 {
			detailSlic := sendMsgRsp.GetDetail()
			if detailSlic[0].GetRet() == 0 {
				postSucc = true
				log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s contentLen=%v post success ret=%v wid=%v onlineStat=%v",
					wid,
					uid,
					prevTimeStamp,
					tag,
					len(content),
					detailSlic[0].GetRet(),
					detailSlic[0].GetWid(),
					detailSlic[0].GetStatus())
				// 统计总的请求量
				attr := "wns_consumer/channel_post_success_count"
				libcomm.AttrAdd(attr, 1)
				break
			} else {
				log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s content=%s post failed ret=%v wid=%v onlineStat=%v",
					wid,
					uid,
					prevTimeStamp,
					tag,
					content,
					detailSlic[0].GetRet(),
					detailSlic[0].GetWid(),
					detailSlic[0].GetStatus())
				// 统计总的请求量
				attr := "wns_consumer/channel_post_failed_count"
				libcomm.AttrAdd(attr, 1)
				continue
			}
		} else {
			log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s content=%s post failed code=%v",
				wid,
				uid,
				prevTimeStamp,
				tag,
				content,
				errno)
			// 统计总的请求量
			attr := "wns_consumer/channel_post_failed_count"
			libcomm.AttrAdd(attr, 1)
		}
	}
	if postSucc {
		log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s contentLen=%v post success",
			wid,
			uid,
			prevTimeStamp,
			tag,
			len(content))
		// 统计总的请求量
		attr := "wns_consumer/post_success_count"
		libcomm.AttrAdd(attr, 1)
	} else {
		log.Printf("WnsPushHandle wid=%v uid=%v prevTimeStamp=%v tag=%s contentLen=%v post failed",
			wid,
			uid,
			prevTimeStamp,
			tag,
			len(content))
		// 统计总的请求量
		attr := "wns_consumer/post_failed_count"
		libcomm.AttrAdd(attr, 1)
	}
	return nil
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}
	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)
}
