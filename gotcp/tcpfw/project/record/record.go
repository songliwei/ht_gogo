package main

import (
	"errors"
	"fmt"
	"path/filepath"
	"sync"

	"github.com/HT_GOGO/gotcp"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_record"
	webcommon "github.com/HT_GOGO/gotcp/webapi/common"

	"log"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

type RecordStr struct {
	Pid       int
	RecordDri string
	Wg        sync.WaitGroup
}

var (
	infoLog              *log.Logger
	groupLessonApi       *common.GroupLessonApi
	channelIdToRecordPid map[string]*RecordStr
	recordFileDir        string
	idleThreshold        string
	OSSBukect            string = "ht-blog"
	OSSRegion            string = "oss-cn-hongkong.aliyuncs.com"
	OSSAppID             string
	OSSAppKey            string
	OSSPrefix            string
)

const (
	APP_ID            = "9101377F20E84760B827B18B84CE58CF"
	RECORD_BIN        = "/mydata/local_src/Agora_Recording_SDK_for_Linux_FULL/samples/release/bin/recorder"
	RECORD_UID        = "10000"
	APPLITE_DIR       = "/mydata/local_src/Agora_Recording_SDK_for_Linux_FULL/bin"
	ISAUDIOONLY       = "1"
	ProcSlowThreshold = 300000
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrInternalErr  = errors.New("internal err")
	ErrInvalidRsp   = errors.New("err invalid respones")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gopur/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_record.RECORD_CMD_TYPE_CMD_START_NEW_RECORD):
		go ProcStartRecord(c, head, packet)
	case uint32(ht_record.RECORD_CMD_TYPE_CMD_END_RECORD):
		go ProcEndRecord(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
		c.Close()
	}
	return true
}

// 1.proc start a new record
func ProcStartRecord(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_record.RECORD_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_record.RecordRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_record.RECORD_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.StartNewRecordRspbody = &ht_record.StartNewRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcStartRecord invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gorecord/start_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_record.RecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcStartRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_record.RECORD_RET_CODE_RET_PB_ERR)
		rspBody.StartNewRecordRspbody = &ht_record.StartNewRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetStartNewRecordReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcStartRecord GetStartNewRecordReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_record.RECORD_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.StartNewRecordRspbody = &ht_record.StartNewRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	roomId := subReqBody.GetRoomId()
	channelId := string(subReqBody.GetChannelId())
	infoLog.Printf("ProcStartRecord uid=%v roomId=%v channelId=%s",
		head.Uid,
		roomId,
		channelId)
	// Step1 param check
	if roomId == 0 || len(channelId) == 0 {
		infoLog.Printf("ProcStartRecord invalid param uid=%v cmd=0x%4x seq=%v roomId=%v channelId=%s",
			head.Uid,
			head.Cmd,
			head.Seq,
			roomId,
			channelId)
		result = uint16(ht_record.RECORD_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.StartNewRecordRspbody = &ht_record.StartNewRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: begin record
	go BeginRecord(channelId, ISAUDIOONLY, idleThreshold)
	if err != nil {
		attr := "gorecord/start_new_record_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcStartRecord internel error uid=%v cmd=0x%4x seq=%v roomId=%v channelId=%s",
			head.Uid,
			head.Cmd,
			head.Seq,
			roomId,
			channelId)
		result = uint16(ht_record.RECORD_RET_CODE_RET_INTERNAL_ERR)
		rspBody.StartNewRecordRspbody = &ht_record.StartNewRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	result = uint16(ht_record.RECORD_RET_CODE_RET_SUCCESS)
	rspBody.StartNewRecordRspbody = &ht_record.StartNewRecordRspBody{
		Status: &ht_record.RecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		attr := "gorecord/start_new_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcStartRecord uid=%v roomId=%v channelId=%s procTime=%v",
			head.Uid,
			roomId,
			channelId,
			procTime)
	}
	return true
}

// isAudioOnly: 1 audio only 0:not audio only
func BeginRecord(channelId string, isAudioOnly string, idleTime string) {
	if len(channelId) == 0 {
		infoLog.Printf("BeginRecord channelId=%s isAudioOnly=%s idleTime=%s input error",
			channelId,
			isAudioOnly,
			idleThreshold)
		return
	}

	cmd := exec.Command(RECORD_BIN,
		"--appId", APP_ID,
		"--uid", RECORD_UID,
		"--channel", channelId,
		"--appliteDir", APPLITE_DIR,
		"--recordFileRootDir", recordFileDir,
		"--isAudioOnly", isAudioOnly,
		"--isMixingEnabled", "1",
		"--idle", idleTime)

	err := cmd.Start()
	timeNow := time.Now()
	timeNow = timeNow.UTC()
	if err != nil {
		infoLog.Printf("BeginRecord channelId=%s isAudioOnly=%s idleTime=%s cmd.Start err=%s",
			channelId,
			isAudioOnly,
			idleTime,
			err)
		return
	}

	recordChannelDir := fmt.Sprintf("%s/%4d%02d%02d/%s_%02d%02d%02d",
		recordFileDir,
		timeNow.Year(),
		timeNow.Month(),
		timeNow.Day(),
		channelId,
		timeNow.Hour(),
		timeNow.Minute(),
		timeNow.Second())
	infoLog.Printf("BeginRecord channelId=%s isAudioOnly=%s idleTime=%s pid=%v recordChannelDir=%s",
		channelId,
		isAudioOnly,
		idleTime,
		cmd.Process.Pid,
		recordChannelDir)
	tempRecordStr := RecordStr{
		Pid:       cmd.Process.Pid,
		RecordDri: recordChannelDir,
		Wg:        sync.WaitGroup{},
	}
	tempRecordStr.Wg.Add(1)
	channelIdToRecordPid[channelId] = &tempRecordStr
	if err := cmd.Wait(); err != nil {
		infoLog.Printf("BeginRecord channelId=%s isAudioOnly=%s idleTime=%s wait return err=%s",
			channelId,
			isAudioOnly,
			idleTime,
			err.Error())
		tempRecordStr.Wg.Done()
		return

	}
	tempRecordStr.Wg.Done()
	return
}

func EndRecord(channelId string) (err error) {
	if len(channelId) == 0 {
		infoLog.Printf("EndRecord channelId=%s input error", channelId)
		err = ErrInvalidParam
		return err
	}
	if record, ok := channelIdToRecordPid[channelId]; ok {
		cmd := exec.Command("kill", "-9", fmt.Sprintf("%v", record.Pid))
		// Run starts the specified command and waits for it to complete
		// Start starts the specified command but does not wait for it to complete
		// we need it compile
		err = cmd.Run()
		if err != nil {
			infoLog.Printf("EndRecord channelId=%s pid=%v", channelId, record.Pid)
			return err
		}
		infoLog.Printf("EndRecord channelId=%s kill -9 pid=%v", channelId, record.Pid)
		//等待录像进程退出都执行完
		record.Wg.Wait()
	} else {
		infoLog.Printf("EndRecord channelId=%s not found channelIdToRecordPid=%v",
			channelId,
			channelIdToRecordPid)
	}
	return nil
}

// 2.proc record end
func ProcEndRecord(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_record.RECORD_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_record.RecordRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcEndRecord no need call from=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_record.RECORD_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.EndRecordRspbody = &ht_record.EndRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcEndRecord invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gorecord/end_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_record.RecordReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcEndRecord proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_record.RECORD_RET_CODE_RET_PB_ERR)
		rspBody.EndRecordRspbody = &ht_record.EndRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetEndRecordReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcEndRecord GetEndRecordReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_record.RECORD_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.EndRecordRspbody = &ht_record.EndRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	roomId := subReqBody.GetRoomId()
	channelId := string(subReqBody.GetChannelId())
	recordObid := subReqBody.GetRecordObid()
	infoLog.Printf("ProcEndRecord uid=%v roomId=%v channelId=%s recordObid=%s",
		head.Uid,
		roomId,
		channelId,
		recordObid)
	if roomId == 0 || len(channelId) == 0 || len(recordObid) == 0 {
		infoLog.Printf("ProcEndRecord invalid param uid=%v cmd=0x%4x seq=%v roomId=%v channelId=%s recordObid=%s",
			head.Uid,
			head.Cmd,
			head.Seq,
			roomId,
			channelId,
			recordObid)
		result = uint16(ht_record.RECORD_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.EndRecordRspbody = &ht_record.EndRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	err = EndRecord(channelId)
	if err != nil {
		attr := "gorecord/end_record_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcEndRecord internel error uid=%v cmd=0x%4x seq=%v roomId=%v channelId=%s",
			head.Uid,
			head.Cmd,
			head.Seq,
			roomId,
			channelId)
		result = uint16(ht_record.RECORD_RET_CODE_RET_INTERNAL_ERR)
		rspBody.EndRecordRspbody = &ht_record.EndRecordRspBody{
			Status: &ht_record.RecordHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	result = uint16(ht_record.RECORD_RET_CODE_RET_SUCCESS)
	rspBody.EndRecordRspbody = &ht_record.EndRecordRspBody{
		Status: &ht_record.RecordHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("commit success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// 开始处理视频录像的上传工作
	// 录像完成后睡眠5秒再上传
	time.Sleep(5 * time.Second)

	if record, ok := channelIdToRecordPid[channelId]; ok {
		infoLog.Printf("ProcEndRecord roomId=%v channelId=%s pid=%v recordDir=%s",
			roomId,
			channelId,
			record.Pid,
			record.RecordDri)
		urlSlic, err := UploadToOSS(record.RecordDri, roomId)
		delete(channelIdToRecordPid, channelId)
		if err != nil || len(urlSlic) == 0 {
			infoLog.Printf("ProcEndRecord roomId=%v channelId=%s recordObid=%s UploadToOSS failed err=%s",
				roomId,
				channelId,
				recordObid,
				err)
			attr := "gorecord/upload_to_oss_failed"
			libcomm.AttrAdd(attr, 1)
		} else {
			err = groupLessonApi.UpdateGroupLessonRecordUrl(roomId, recordObid, []byte(urlSlic[0]))
			if err != nil {
				infoLog.Printf("ProcEndRecord roomId=%v channelId=%s recordObid=%s UpdateGroupLessonRecordUrl err=%s",
					roomId,
					channelId,
					recordObid,
					err)
				attr := "gorecord/update_record_url_failed"
				libcomm.AttrAdd(attr, 1)
			}
		}
	} else {
		infoLog.Printf("ProcEndRecord roomId=%v channelId=%s not found",
			roomId,
			channelId)
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		attr := "gorecord/end_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcEndRecord uid=%v roomId=%v channelId=%v procTime=%v",
			head.Uid,
			roomId,
			channelId,
			procTime)
	}
	return true
}

func UploadToOSS(recordFileDir string, roomId uint32) (urlSlic []string, err error) {
	// Step1: param check
	if len(recordFileDir) == 0 {
		infoLog.Printf("UploadToOSS recordFileDir empty")
		err = ErrInvalidParam
		return urlSlic, err
	}
	infoLog.Printf("UploadToOSS recordFileDir=%s", recordFileDir)
	// Step2: search aac file name
	list, err := filepath.Glob(recordFileDir + "/*.aac")
	if err != nil {
		infoLog.Printf("UploadToOSS filepath.Glob search aac file failed recordFileDir=%s err=%s",
			recordFileDir,
			err)
		return urlSlic, err
	}
	for _, v := range list {
		infoLog.Printf("UploadToOSS recordFileDir=%s fileName=%s ", recordFileDir, v)
		endPoint := OSSRegion
		AccessKeyId := OSSAppID
		AccessKeySecret := OSSAppKey
		Bucket := OSSBukect

		client, err := oss.New(endPoint, AccessKeyId, AccessKeySecret)
		if err != nil {
			attr := "gorecord/new_oss_client_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("UploadToOSS recordFileDir=%s fileName=%s oss.New err=%s",
				recordFileDir,
				v,
				err)
			continue
		}

		bucket, err := client.Bucket(Bucket)
		if err != nil {
			attr := "gorecord/new_oss_bucket_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("UploadToOSS recordFileDir=%s fileName=%s client.Bucket err=%s",
				recordFileDir,
				v,
				err)
			continue
		}
		//file 1 2 3, //
		//命名规则 bai/{fileType}/{imagecount}/9.jpg
		fileExt := webcommon.GetExt(v)
		md5, err := webcommon.Md5File(v)
		if err != nil {
			infoLog.Printf("UploadToOSS recordFileDir=%s fileName=%s webcommon.Md5File err=%s",
				recordFileDir,
				v,
				err)
			continue
		}
		infoLog.Printf("UploadToOSS recordFileDir=%s fileName=%s md5=%s fileExt=%s",
			recordFileDir,
			v,
			md5,
			fileExt)

		prefix := OSSPrefix
		srcFileObj := prefix + fmt.Sprintf("%v", roomId) + "/m_" + md5 + fileExt
		infoLog.Printf("UploadToOSS localFile=%s srcFileObj=%s", v, srcFileObj)
		err = bucket.PutObjectFromFile(srcFileObj, v)
		if err != nil {
			attr := "gorecord/upload_to_oss_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("UploadToOSS recordFileDir=%s fileName=%s srcFileObj=%s err=%s",
				recordFileDir,
				v,
				srcFileObj,
				err)
			continue
		}
		fullName := OSSBukect + "." + OSSRegion + "/" + srcFileObj
		urlSlic = append(urlSlic, fullName)
	}
	infoLog.Printf("UploadToOSS roomId=%v recordFileDir=%s urlSlic=%v", roomId, recordFileDir, urlSlic)
	return urlSlic, nil
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_record.RecordRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	channelIdToRecordPid = make(map[string]*RecordStr)
	recordFileDir = cfg.Section("RECORD_FILE_DIR").Key("dir").MustString("/mydata/record_file")
	idleThreshold = cfg.Section("IDLE_THRESHOLD").Key("value").MustString("300")
	infoLog.Printf("recordFileDir=%s idleThreshold=%s", recordFileDir, idleThreshold)

	// 读取group lesson 配置
	groupLessonIp := cfg.Section("GROUPLESSON").Key("ip").MustString("127.0.0.1")
	groupLessonPort := cfg.Section("GROUPLESSON").Key("port").MustString("0")
	groupLessonConnLimit := cfg.Section("GROUPLESSON").Key("pool_limit").MustInt(1000)
	infoLog.Printf("group lesson server ip=%v port=%v connLimit=%v", groupLessonIp, groupLessonPort, groupLessonConnLimit)
	groupLessonApi = common.NewGroupLessonApi(groupLessonIp, groupLessonPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, groupLessonConnLimit)

	//
	OSSBukect = cfg.Section("SERVICE_CONFIG").Key("oss_bucket").MustString("ht-blog")
	OSSRegion = cfg.Section("SERVICE_CONFIG").Key("oss_region").MustString("oss-cn-hongkong.aliyuncs.com")
	OSSAppID = cfg.Section("SERVICE_CONFIG").Key("oss_appid").MustString("ULuE3LK66Y6ZlEWS")
	OSSAppKey = cfg.Section("SERVICE_CONFIG").Key("oss_appkey").MustString("JTFyxX7TW9K1TSVMwglj272CxfvwUC")
	OSSPrefix = cfg.Section("SERVICE_CONFIG").Key("oss_prefix").MustString("groupclass/")
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
