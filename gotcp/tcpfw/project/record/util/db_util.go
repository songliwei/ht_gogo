// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

// Error type
var (
	ErrNilDbObject = errors.New("not set  object current is nil")
	ErrDbParam     = errors.New("err param error")
)

const (
	HelloTalkBid             = "com.helloTalk.helloTalk"
	ProductMonthAuto         = "com.hellotalk.monthauto"
	ProductOneMonthAuto1     = "com.hellotalk.onemonth1auto"
	ProductOneMonthAutoRenew = "com.hellotalk.1monthautorenew"
	ProductOneMonthAuto      = "com.hellotalk.onemonthauto"
	ProductYearAuto          = "com.hellotalk.yearauto"
	ProductOneMonthSubPlan   = "com.hellotalk.onemonthsubscriptionplan"
	ProductOneMonthSubPlan2  = "com.hellotalk.onemonthsubscriptionplan2"
	ProductOneYearSubPlan    = "com.hellotalk.oneyearsubscriptionplan"
	ProductOneYearSubPlan2   = "com.hellotalk.oneyearsubscriptionplan2"
	ProductOneYearAuto2      = "com.hellotalk.yearauto2"
	ProductOneMonthSub       = "com.hellotalk.onemonthsubscription"
	ProductOneMonthSub2      = "com.hellotalk.onemonthsubscription2"
	ProductOnMonthFreeTrial  = "com.hellotalk.monthauto_freetrial"
	ProductOneYearSub        = "com.hellotalk.oneyearsubscription"
	ProductOneYearSub2       = "com.hellotalk.oneyearsubscription2"
	ProductOneYearFreeTrial  = "com.hellotalk.yearauto2_freetrial"
	ProductOneMonthAutoB     = "com.hellotalk.onemonthauto.b"
	ProductSuperOneMonthAuto = "com.hellotalk.super1monthauto"
	ProductOneMonthAutoC     = "com.hellotalk.onemonthauto.c"
	ProductOneYearAutoB      = "com.hellotalk.yearauto.b"
	ProductSuperOneYearAuto  = "com.hellotalk.super1yearauto"
	ProductOneYearAutoC      = "com.hellotalk.yearauto.c"
)

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

func (this *DbUtil) WriteAutoRenewRecordApple(userId, status, clientOrServer uint32,
	transactionId, productId, originPurchDate, originTransactionId, purchDateGMT, purchTs, expireDate, expireTs, receiptData, verifyRsp, currency, payMoney string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	itemCode := GetItemCode(productId)
	_, err = this.db.Exec("insert into HT_AUTORENEW_RECORD_APPLE set TRANSACTION_ID=?, USER_ID=?, PRODUCT_ID=?, ORIGIN_ID=?, ORIGIN_DATE=?, PURCHDATE_GMT=?, PURCHDATE_TS=?, EXPIRES_GMT=?, EXPIRES_TS=?, RECEIPT_DATA=?, VERIFY_DATA=?, VERIFY_STATUS=?, CLIENT_OR_SRV=?, UPDATE_TIME=UTC_TIMESTAMP(), CURRENY=?, PAY_MONEY=?, ITEMCODE=?;",
		transactionId,
		userId,
		productId,
		originTransactionId,
		originPurchDate,
		purchDateGMT,
		purchTs,
		expireDate,
		expireTs,
		receiptData,
		verifyRsp,
		status,
		clientOrServer,
		currency,
		payMoney,
		itemCode)
	if err != nil {
		this.infoLog.Printf("WriteAutoRenewRecordApple insert faield transactionId=%s userid=%v productId=%v verifyRsp=%s err=%v",
			transactionId,
			userId,
			productId,
			verifyRsp,
			err)
		return err
	} else {
		return nil
	}
}

func GetItemCode(productId string) (itemCode uint32) {
	itemCode = 0
	if productId == ProductMonthAuto ||
		productId == ProductOneMonthAuto1 ||
		productId == ProductOneMonthAutoRenew ||
		productId == ProductOneMonthAuto {
		itemCode = 2
	} else if productId == ProductOneMonthSubPlan ||
		productId == ProductOneMonthSubPlan2 {
		itemCode = 7 // PRODUCT_ONE_MONTH_SUB_PLAN 7 高价格 1个月自动续费会员
	} else if productId == ProductOneMonthSub ||
		productId == ProductOneMonthSub2 ||
		productId == ProductOnMonthFreeTrial {
		itemCode = 14
	} else if productId == ProductYearAuto {
		itemCode = 5
	} else if productId == ProductOneYearSubPlan ||
		productId == ProductOneYearSubPlan2 ||
		productId == ProductOneYearAuto2 {
		itemCode = 8 // PRODUCT_ONE_YEAR_SUB_PLAN 8 高价格 1年自动续费会员
	} else if productId == ProductOneYearSub ||
		productId == ProductOneYearSub2 ||
		productId == ProductOneYearFreeTrial {
		itemCode = 15 //  1年试用
	} else if productId == ProductOneMonthAutoB ||
		productId == ProductSuperOneMonthAuto {
		itemCode = 22
	} else if productId == ProductOneMonthAutoC {
		itemCode = 23
	} else if productId == ProductOneYearAutoB ||
		productId == ProductSuperOneYearAuto {
		itemCode = 24
	} else if productId == ProductOneYearAutoC {
		itemCode = 25
	}
	return itemCode
}

func (this *DbUtil) AppleStoreGetUserIdByRepeateCommitOrderId(orderId string) (uid uint32, err error) {
	if this.db == nil || orderId == "" {
		err = ErrDbParam
		return uid, err
	}
	var storeUserId sql.NullInt64
	err = this.db.QueryRow("SELECT USER_ID FROM HT_AUTORENEW_RECORD_APPLE WHERE TRANSACTION_ID = ?",
		orderId).Scan(&storeUserId)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("AppleStoreGetUserIdByRepeateCommitOrderId not found orderId=%s in HT_AUTORENEW_RECORD_APPLE err=%s", orderId, err)
		return uid, err
	case err != nil:
		this.infoLog.Printf("AppleStoreGetUserIdByRepeateCommitOrderId exec HT_AUTORENEW_RECORD_APPLE failed orderId=%v, err=%s", orderId, err)
		return uid, err
	default:
	}

	if storeUserId.Valid {
		uid = uint32(storeUserId.Int64)
	}
	return uid, nil
}

func (this *DbUtil) WriteAppStorePurchaseRecord(userId, buyType, itemCode, verifyStatus, jailBroken uint32,
	productId, transactionId, currency, payMoney, storeArea, purchDate, purchDateGMT, receiptData, verifyRsp string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	if verifyStatus == 0 {
		_, err = this.db.Exec("insert into HT_PURCHRECORD_APPSTORE set TRNSACTIONID = ?, USERID = ?, ITEMCODE = ?, QUANTITY = 1, CURRENCY = ?, PAYMONEY = ?, STOREAREA = ?, RECEIPT = ?, PURCHDATE_PST =?, PURCHDATE_GMT = ?, BUYTYPE = ?, JAILBROKEN = ?, VERIFYSTATUS = ?, VERIFYDATA = ?;",
			transactionId,
			userId,
			itemCode,
			currency,
			payMoney,
			storeArea,
			receiptData,
			purchDate,
			purchDateGMT,
			buyType,
			jailBroken,
			verifyStatus,
			verifyRsp)
		if err != nil {
			this.infoLog.Printf("WriteAppStorePurchaseRecord insert faield transactionId=%s userid=%v productId=%v verifyRsp=%s err=%v",
				transactionId,
				userId,
				productId,
				verifyRsp,
				err)
			return err
		} else {
			return nil
		}
	} else {
		_, err = this.db.Exec("insert into HT_PURCHRECORD_APPSTORE set TRNSACTIONID = ?, USERID = ?, ITEMCODE = ?, QUANTITY = 1, CURRENCY = ?, PAYMONEY = ?, STOREAREA = ?, RECEIPT = ?, PURCHDATE_PST =?, PURCHDATE_GMT = ?, BUYTYPE = ?, JAILBROKEN = ?, VERIFYSTATUS = ?, VERIFYDATA = ?, VERIFYTIME = UTC_TIMESTAMP();",
			transactionId,
			userId,
			itemCode,
			currency,
			payMoney,
			storeArea,
			receiptData,
			purchDate,
			purchDateGMT,
			buyType,
			jailBroken,
			verifyStatus,
			verifyRsp)
		if err != nil {
			this.infoLog.Printf("WriteAppStorePurchaseRecord insert faield transactionId=%s userid=%v productId=%v verifyRsp=%s err=%v",
				transactionId,
				userId,
				productId,
				verifyRsp,
				err)
			return err
		} else {
			return nil
		}
	}
}

func (this *DbUtil) WriteAutoRenewRecordGoogle(userId, status uint32, startTimeMillis, expiryTimeMillis int64, orderId, productId, country, payMonmey, token, response string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}
	itemCode := GetItemCode(productId)
	_, err = this.db.Exec("insert into HT_AUTORENEW_RECORD_GOOGLE set ORDERID= ?, USERID=?, PRODUCTID=?, COUNTRY=?, PAYMONEY=?, PURCHDATE=from_unixtime(?), INITTIMESTAMP=?, UTILDATE=from_unixtime(?), UTILTIMESTAMP=?, STATE=?, PURCHTOKEN=?, RESPONSE=?, UPDATETIME=UTC_TIMESTAMP(), ITEMCODE=?;",
		orderId,
		userId,
		productId,
		country,
		payMonmey,
		startTimeMillis/1000, // change ms to second
		startTimeMillis,
		expiryTimeMillis/1000, // change ms to second
		expiryTimeMillis,
		status,
		token,
		response,
		itemCode)
	if err != nil {
		this.infoLog.Printf("WriteAutoRenewRecordGoogle insert faield  userid=%v productId=%v verifyRsp=%s err=%v",
			userId,
			productId,
			response,
			err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) GooglePlayGetUserIdByRepeateCommitOrderId(orderId string) (uid uint32, err error) {
	if this.db == nil || orderId == "" {
		err = ErrDbParam
		return uid, err
	}
	var storeUserId sql.NullInt64
	err = this.db.QueryRow("SELECT USERID FROM HT_AUTORENEW_RECORD_GOOGLE WHERE ORDERID = ?;",
		orderId).Scan(&storeUserId)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GooglePlayGetUserIdByRepeateCommitOrderId not found orderId=%s in HT_AUTORENEW_RECORD_GOOGLE err=%s", orderId, err)
		return uid, err
	case err != nil:
		this.infoLog.Printf("GooglePlayGetUserIdByRepeateCommitOrderId exec HT_AUTORENEW_RECORD_GOOGLE failed orderId=%v, err=%s", orderId, err)
		return uid, err
	default:
	}

	if storeUserId.Valid {
		uid = uint32(storeUserId.Int64)
	}
	return uid, nil
}

func (this *DbUtil) WriteGooglePlayPurchaseRecord(userId, itemCode, verifyStatus uint32, purchaseTimeStamp uint64,
	productId, orderId, country, currency, payMoney, purchaseToken, verifyRsp string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	_, err = this.db.Exec("insert into HT_PURCHRECORD_PALY set ORDERID = ?, USERID = ?, ITEMCODE = ?, PRODUCTID =?, CURRENCY =? , PAYMONEY = ?, COUNTRY = ?, PURCHDATE = UTC_TIMESTAMP(), PURCHTIME_TS = ?, PURCHTOKEN = ?, STATE = ?,VERIFYRESPONSE= ?, UPDATETIME = UTC_TIMESTAMP();",
		orderId,
		userId,
		itemCode,
		productId,
		currency,
		payMoney,
		country,
		purchaseTimeStamp,
		purchaseToken,
		verifyStatus,
		verifyRsp)
	if err != nil {
		this.infoLog.Printf("WriteGooglePlayPurchaseRecord insert faield userid=%v productId=%v verifyRsp=%s err=%v",
			userId,
			productId,
			verifyRsp,
			err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) WriteAliPayPurchaseRecord(userId, itemCode uint32,
	tradeNo, purchDate, partner, seller, subject, success, totalFee string) (err error) {
	if this.db == nil || len(subject) == 0 || itemCode == 0 {
		return ErrNilDbObject
	}

	_, err = this.db.Exec("insert into HT_PURCHRECORD_ALIPAY set USERID = ?, TRADENO = ?, ITEMCODE = ?, PURCHASEDATE = ?, PARTNER = ?, SELLER = ?, SUBJECT = ?, TOTALFEE = ?, SUCCESS = ?, UPDATETIME = UTC_TIMESTAMP();",
		userId,
		tradeNo,
		itemCode,
		purchDate,
		partner,
		seller,
		subject,
		totalFee,
		success)
	if err != nil {
		this.infoLog.Printf("WriteAliPayPurchaseRecord insert faield userid=%v itemcode=%v subject=%s err=%v",
			userId,
			itemCode,
			subject,
			err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) WriteWeChatPurchaseRecord(userId, itemCode uint32,
	tradeNo, purchDate, partner, seller, subject, success, totalFee, checkResp string) (err error) {
	if this.db == nil || len(subject) == 0 || itemCode == 0 {
		return ErrNilDbObject
	}

	_, err = this.db.Exec("insert into HT_PURCHRECORD_WECHATPAY set USERID = ?, TRADENO = ?, ITEMCODE = ?, PURCHASEDATE = ?, PARTNER = ?, SELLER = ?, SUBJECT = ?, TOTALFEE = ?, SUCCESS = ?, CHECKRESPONSE = ?, UPDATETIME = UTC_TIMESTAMP();",
		userId,
		tradeNo,
		itemCode,
		purchDate,
		partner,
		seller,
		subject,
		totalFee,
		success,
		checkResp)
	if err != nil {
		this.infoLog.Printf("WriteWeChatPurchaseRecord insert faield userid=%v itemcode=%v subject=%s err=%v",
			userId,
			itemCode,
			subject,
			err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) GetUserVIPInfo(userId uint32) (lastIterm, vipYear, vipType uint8, expiredTS uint64, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return 0, 0, 0, 0, err
	}
	if userId == 0 {
		err = ErrDbParam
		return 0, 0, 0, 0, err
	}

	err = this.db.QueryRow("select EXPIRETIME,LASTPURCHITEM,VIPYEAR,VIPTYPE from HT_PURCHASE_TRANSLATE where USERID = ?;", userId).Scan(&expiredTS, &lastIterm, &vipYear, &vipType)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetUserVIPInfo not found uid=%v", userId)
		break
	case err != nil:
		this.infoLog.Printf("GetUserVIPInfo exec failed uid=%v, err=%s", userId, err)
		break
	default:
		this.infoLog.Printf("GetUserVIPInfo uid=%v expiredTs=%v lastIterm=%v vipYear=%v vipType=%v", userId, expiredTS, lastIterm, vipYear, vipType)
	}
	return lastIterm, vipYear, vipType, expiredTS, nil
}

func (this *DbUtil) UpdateUserVIPInfo(userId, itermCode uint32, vipYear, vipType uint8, expiredTimestamp uint64) (err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return err
	}

	if userId == 0 || itermCode == 0 {
		return ErrDbParam
	}

	_, err = this.db.Exec("insert into HT_PURCHASE_TRANSLATE set USERID=?, CREATETIME=UTC_TIMESTAMP(), EXPIRETIME=?, "+
		"LASTPURCHITEM=?, VIPYEAR=?, VIPTYPE=?, UPDATETIME=UTC_TIMESTAMP() on duplicate key update "+
		"LASTPURCHITEM=?, EXPIRETIME=?, VIPYEAR=?, VIPTYPE=?, UPDATETIME=UTC_TIMESTAMP();",
		userId,
		expiredTimestamp,
		itermCode,
		vipYear,
		vipType,
		itermCode,
		expiredTimestamp,
		vipYear,
		vipType)
	if err != nil {
		this.infoLog.Printf("UpdateUserVIPInfo insert faield userId=%v expireTimeStamp=%v itermCode=%v vipYear=%v vipType=%v err=%s",
			userId,
			expiredTimestamp,
			itermCode,
			vipYear,
			vipType,
			err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) WriteUserSendPresentRecordInDB(orderId string, userId, toId, presentIterm, payType uint32) (err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return err
	}

	if userId == 0 || presentIterm == 0 {
		return ErrDbParam
	}
	_, err = this.db.Exec("insert into HT_PRESENTSEND_RECORD set ORDERID = ?, USERID = ?, TOID = ?, ITEMCODE = ?, PAYTYPE = ?, SENDTIME = UTC_TIMESTAMP();",
		orderId,
		userId,
		toId,
		presentIterm,
		payType)
	if err != nil {
		this.infoLog.Printf("WriteUserSendPresentRecordInDB insert faield orderId=%s userId=%v toId=%v presentItem=%v payType=%v err=%s",
			orderId,
			userId,
			toId,
			presentIterm,
			payType,
			err)
		return err
	} else {
		return nil
	}
}
