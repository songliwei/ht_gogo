package main

import (
	"log"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strconv"

	flags "github.com/jessevdk/go-flags"
	ini "gopkg.in/ini.v1"
)

var (
	infoLog *log.Logger
)

func hello(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("success"))
	len := req.ContentLength
	body := make([]byte, len)
	req.Body.Read(body)
	infoLog.Printf("bodyLen=%v body=%s", len, body)
	decodeParam, err := url.QueryUnescape(string(body))
	if err != nil {
		infoLog.Printf("url.QueryUnescape return err=%s", err)
		return
	}
	infoLog.Printf("url.QueryUnescape decodeParam=%s", decodeParam)
}
func say(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("success"))
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	listenAddr := serverIp + ":" + strconv.Itoa(serverPort)

	http.HandleFunc("/goapi/alipay", hello)
	// http.Handle("/handle", http.HandlerFunc(say))
	http.ListenAndServe(listenAddr, nil)
	select {} //阻塞进程
}
