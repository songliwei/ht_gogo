package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	simplejson "github.com/bitly/go-simplejson"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_thirdpay"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_user"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"github.com/nsqio/go-nsq"
	"gopkg.in/ini.v1"
)

var (
	wg             *sync.WaitGroup
	mcApi          *common.MemcacheApi
	userCacheApi   *common.UserCacheApi
	db             *sql.DB
	globalProducer *nsq.Producer
	mucApi         *common.MucApi
	nsqNotifyTopic string
	imServerPool   map[string]*common.ImServerApiV2
)

var (
	ErrNilOrder       = errors.New("err nil order obj")
	ErrNilDbObject    = errors.New("err nil db obj")
	ErrNilMucObject   = errors.New("err nil MucApi obj")
	ErrPurchaseFailed = errors.New("err purchase failed")
	ErrInputParamErr  = errors.New("err input param err")
	ErrOnlineIpErr    = errors.New("err online state ip not exist")
)

const (
	TradeSuccess = "TRADE_SUCCESS"
)

const (
	YEAR_NORMAL = 0 //'0-非年会员 1-年会员'
	YEAR_VIP    = 1
)

const (
	DEFAULT_USER = 200002
	TRYTIMES     = 3
)

const (
	//0-客户端提交 1-服务端后台验证（自动续费）
	CLIENT_COMMIT = 0
	SERVER_COMMIT = 1
)

const (
	E_NOT_VIP       = 0   // 非会员
	E_DAYS_VIP      = 1   // 按天计数的会员(旧版本购买的月、年会员)
	E_MONTHAUTO_VIP = 2   // 月自动续费的会员
	E_YEARAUTO_VIP  = 3   // 年自动续费的会员
	E_LIFETIME_VIP  = 100 // 终生会员 (50年后过期)
)

const (
	PAYTYPE_APPSTORE   = 1
	PAYTYPE_GOOGLEPLAY = 2
	PAYTYPE_PAYPAL     = 3
	PAYTYPE_ALIPAY     = 4
	PAYTYPE_WECHATPAY  = 5
)

const (
	PRODUCT_ONE_MONTH = 1
	// "com.hellotalk.onemonth1"
	PRODUCT_ONE_MONTHAUTO = 2
	// "com.hellotalk.onemonthauto"
	// or GooglePlay="com.hellotalk.onemonth1auto"
	// 2016-07-16 android use "com.hellotalk.1monthautorenew"
	// 2016-08-08  iOS use "com.hellotalk.monthauto"
	PRODUCT_ONE_YEAR = 3
	// "com.hellotalk.oneyear"
	PRODUCT_THREE_MONTH = 4
	// "com.hellotalk.3months"

	PRODUCT_YEAR_AUTO = 5
	// "com.hellotalk.yearauto"
	PRODUCT_LIFT_TIME = 6
	// "com.hellotalk.lifetime"

	// product id for upgrade vip price
	PRODUCT_ONE_MONTH_SUB_PLAN = 7
	// iOS:"com.hellotalk.onemonthsubscriptionplan"
	// And:"com.hellotalk.onemonthsubscriptionplan2"
	PRODUCT_ONE_YEAR_SUB_PLAN = 8
	// iOS:"com.hellotalk.oneyearsubscriptionplan"
	// iOS:"com.hellotalk.yearauto2"
	// And:"com.hellotalk.oneyearsubscriptionplan2"
	PRODUCT_LIFE_TIME_SUB_PLAN = 9
	// "com.hellotalk.lifetimeplan"

	PRODUCT_GIFT_ONEMONTH = 11
	// "com.hellotalk.Gift1M" or GooglePlay="com.hellotalk.g1m"
	PRODUCT_GIFT_THREEMONTH = 12
	// "com.hellotalk.Gift3M" or GooglePlay="com.hellotalk.g3m"
	PRODUCT_GIFT_ONEYEAR = 13
	// "com.hellotalk.Gift1Y" or GooglePlay="com.hellotalk.g1y"

	// product id for trial membership
	PRODUCT_ONE_MONTH_SUB = 14
	// iOS:"com.hellotalk.onemonthsubscription"
	// iOS:"com.hellotalk.monthauto_freetrial"
	// And:"com.hellotalk.onemonthsubscription2"
	PRODUCT_ONE_YEAR_SUB = 15
	// iOS:"com.hellotalk.oneyearsubscription"
	// iOS:"com.hellotalk.yearauto2_freetrial"
	// And:"com.hellotalk.oneyearsubscription2"
	PRODUCT_ONE_MULITLANG = 21
	// "com.hellotalk.1moreLang"
	PRODUCT_ONE_MONTH_AUTO_B = 22
	// "com.hellotalk.onemonthauto.b"
	// iOS:"com.hellotalk.super1monthauto"
	PRODUCT_ONE_MONTH_AUTO_C = 23
	// "com.hellotalk.onemonthauto.c"
	PRODUCT_YEAR_AUTO_B = 24
	// "com.hellotalk.yearauto.b"
	// iOS:"com.hellotalk.super1yearauto"
	PRODUCT_YEAR_AUTO_C = 25
	// "com.hellotalk.yearauto.c"
	PRODUCT_LIFE_TIME_B = 26
	// "com.hellotalk.lifetime.b"
	// iOS:"com.hellotalk.superlifetime"
	PRODUCT_LIFE_TIME_C = 27
	// "com.hellotalk.lifetime.c"
	PRODUCT_ITEM_END = 28
	// product item end
)

const (
	RetSuccess      = "SUCCESS"
	LegelTimeLenght = 14 //格式为yyyyMMddHHmmss
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

type AliPayOrder struct {
	partner          string
	discount         string
	payMentType      string
	subject          string // 购买商品类型
	tradeNo          string
	buyerEmail       string
	gmtCreateTime    string
	quatity          string
	outTradeNo       string
	sellerId         string
	tradeStatus      string
	isTotalFeeAdjust string
	totalFee         string
	gmtPaymentTime   string
	sellerEmail      string
	price            string
	buyId            string
	useCoupon        string
	userId           uint32
	createTime       string
}

// Convert uint to net.IP http://www.outofmemory.cn
func inet_ntoa(ipnr int64) net.IP {
	var bytes [4]byte
	bytes[0] = byte((ipnr >> 24) & 0xFF)
	bytes[1] = byte((ipnr >> 16) & 0xFF)
	bytes[2] = byte((ipnr >> 8) & 0xFF)
	bytes[3] = byte(ipnr & 0xFF)

	return net.IPv4(bytes[3], bytes[2], bytes[1], bytes[0])
}

// Convert net.IP to int64 ,  http://www.outofmemory.cn
func inet_aton(ipnr net.IP) int64 {
	bits := strings.Split(ipnr.String(), ".")

	b0, _ := strconv.Atoi(bits[0])
	b1, _ := strconv.Atoi(bits[1])
	b2, _ := strconv.Atoi(bits[2])
	b3, _ := strconv.Atoi(bits[3])

	var sum int64

	sum += int64(b0) << 24
	sum += int64(b1) << 16
	sum += int64(b2) << 8
	sum += int64(b3)

	return sum
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

func MessageHandle(message *nsq.Message) error {
	log.Printf("MessageHandle Got a message message.Body=%s", message.Body)
	wg.Add(1)
	go func() {
		ProcData(message)
		wg.Done()
	}()

	return nil
}

// 队列中的 json
// {
//   "PARTNER": "2088301827259517",
//   "DISCOUNT": "0.00",
//   "PAYMENT_TYPE": "1",
//   "SUBJECT": "1 Year VIP 会员",
//   "TRADE_NO": "2017083121001004990250701260",
//   "BUYER_EMAIL": "slw2012@yeah.net",
//   "GMT_CREATE": "2017-08-31 11:36:38",
//   "QUANTITY": "1",
//   "OUT_TRADE_NO": "oneyear_2325928_cb7d2202946ad200i63x8w63s1504150586223496",
//   "SELLER_ID": "2088301827259517",
//   "TRADE_STATUS": "TRADE_SUCCESS",
//   "IS_TOTAL_FEE_ADJUST": "N",
//   "TOTAL_FEE": "0.01",
//   "GMT_PAYMENT": "2017-08-31 11:36:39",
//   "SELLER_EMAIL": "kkstyle668@gmail.com",
//   "PRICE": "0.01",
//   "BUYER_ID": "2088502862637991",
//   "USE_COUPON": "N",
//   "USERID": 2325928,
//   "SIGN": "PKqFjba5C7uUOdod+ni8lornaDyDm5heiLjk1hyqqhSpbNuEE9WJVzdtbJ0hWAALhOzcm7aGNFROT7f2+kiL2fDnP5h5iLwCy4Pl1OB6ZskyR+atLf4tOgU44UXegCuY59goEz1gsrhsiM2mY9IjoVjnX28ssmv7U6W0EBSjgxg=",
//   "CREATETIME": "2017-08-31 03:36:39"
// }
func ProcData(message *nsq.Message) error {
	// 统计总的请求量
	attr := "alipay/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	log.Printf("ProcData enter msgId=%v timestamp=%v body=%s", message.ID, message.Timestamp, message.Body)
	// 然后进行下一步无处理
	rootObj, err := simplejson.NewJson(message.Body)
	if err != nil {
		log.Printf("ProcData simplejson new packet error", err)
		return err
	}
	log.Printf("ProcData rootObj=%#v", rootObj)
	// 只处理 "TRADE_STATUS": "TRADE_SUCCESS" 的订单
	tradeStat := rootObj.Get("TRADE_STATUS").MustString("")
	if tradeStat == TradeSuccess {
		aliOrder := &AliPayOrder{
			partner:          rootObj.Get("PARTNER").MustString(""),
			discount:         rootObj.Get("DISCOUNT").MustString(""),
			payMentType:      rootObj.Get("PAYMENT_TYPE").MustString(""),
			subject:          rootObj.Get("SUBJECT").MustString(""),
			tradeNo:          rootObj.Get("TRADE_NO").MustString(""),
			buyerEmail:       rootObj.Get("BUYER_EMAIL").MustString(""),
			gmtCreateTime:    rootObj.Get("GMT_CREATE").MustString(""),
			quatity:          rootObj.Get("QUANTITY").MustString("0"),
			outTradeNo:       rootObj.Get("OUT_TRADE_NO").MustString(""),
			sellerId:         rootObj.Get("SELLER_ID").MustString(""),
			tradeStatus:      tradeStat,
			isTotalFeeAdjust: rootObj.Get("IS_TOTAL_FEE_ADJUST").MustString(""),
			totalFee:         rootObj.Get("TOTAL_FEE").MustString(""),
			gmtPaymentTime:   rootObj.Get("GMT_PAYMENT").MustInt64(""),
			sellerEmail:      rootObj.Get("SELLER_EMAIL").MustString(""),
			price:            rootObj.Get("PRICE").MustString(""),
			buyId:            rootObj.Get("BUYER_ID").MustString(""),
			useCoupon:        rootObj.Get("USE_COUPON").MustString(""),
			userId:           uint32(rootObj.Get("USERID").MustInt64(0)),
			createTime:       rootObj.Get("CREATETIME").MustString(""),
		}

		err = ProcAliOrder(aliOrder)
		if err != nil {
			log.Printf("ProcData call ProcAliOrder err=%s", err)
			return err
		}
	} else {
		log.Printf("ProcData call back tradeStat=%s", tradeStat)
	}

	return nil
}

func ProcAliOrder(order *AliPayOrder) (err error) {
	if order == nil {
		log.Printf("ProcAliOrder nil order or uid")
		return ErrNilOrder
	}

	if order.userId == 0 {
		log.Printf("ProcAliOrder uid=%v invalid param", order.userId)
		return ErrInputParamErr
	}

	// step1: 将订单写入数据库 如果返回错误(订单重复 客户端如果提前提交，然后收到微信后台回调则会产生重复、写入sql失败等)则不添修改会员信息
	tryCount := 0
	for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
		err = StorePurchaseOrderInDB(order)
		if err != nil {
			log.Printf("ProcAliOrder exec StorePurchaseOrderInDB error=%s", err.Error())
			time.Sleep(10 * time.Second) // 休眠10秒
			continue
		}
		break
	}

	if tryCount == 0 {
		attr := "alipay/store_purchase_order_in_db_failed"
		libcomm.AttrAdd(attr, 1)
		log.Printf("ProcAliOrder exec StorePurchaseOrderInDB order=%#v failed", *order)
		return
	}

	if order.returnCode != RetSuccess || order.resultCode != RetSuccess {
		log.Printf("ProcAliOrder purchase failed returnCode=%v resultCode=%s", order.returnCode, order.resultCode)
		err = ErrPurchaseFailed
		attr := "alipay/purchase_failed"
		libcomm.AttrAdd(attr, 1)
		return err
	}

	// step2: 更新会员创建群成员数限制
	userId := order.attach.toId
	err = UpdateMucRoomMemberLimit(userId)
	if err != nil {
		log.Printf("ProcAliOrder UpdateMucRoomMemberLimit failed uid=%v err=%v", userId, err)
	}
	// step3: 根据订单详情修改会员过期天数
	var vipExpiredBefore, vipExpiredAfter uint64
	var vipYearOut, vipTypeOut uint8
	tryCount = 0
	for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
		vipExpiredBefore, vipExpiredAfter, vipYearOut, vipTypeOut, err = UpdateVipExpiredTime(userId,
			order.attach.itermCode,
			0,
			0)
		if err != nil {
			log.Printf("ProcAliOrder UpdateVipExpiredTime failed uid=%v err=%v", userId, err)
			time.Sleep(10 * time.Second) // 休眠10秒
			continue
		}
		break
	}
	if tryCount == 0 {
		attr := "alipay/update_vip_expired_time_failed"
		libcomm.AttrAdd(attr, 1)
		log.Printf("ProcAliOrder UpdateVipExpiredTime failed uid=%v tryCount=%v", userId, tryCount)
		return
	}

	// step4: 如果发起者和接收者不是同一个人则为赠送礼物 直接插入数据库
	if order.attach.fromId != userId {
		err = WriteUserSendPresentRecordInDB(order.outTradeNo,
			order.attach.fromId,
			userId,
			order.attach.itermCode,
			PAYTYPE_WECHATPAY)
		if err != nil {
			log.Printf("ProcAliOrder WriteUserSendPresentRecordInDB failed tradeno=%s from=%v to=%v itermCode=%v paytype=%v",
				order.outTradeNo,
				order.attach.fromId,
				userId,
				order.attach.itermCode,
				PAYTYPE_WECHATPAY)
		}
	}
	tryCount = 0
	for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
		// step5: 插入购买记录
		err = WritePurchaseHistoryToDB(order.outTradeNo,
			order.attach.productId,
			order.attach.itermCode,
			0,
			order.attach.fromId,
			order.attach.toId,
			PAYTYPE_WECHATPAY,
			SERVER_COMMIT,
			order.feeType,
			order.totalFee,
			order.totalFee,
			WeChatTimeToSqlTime(order.timeEnd),
			vipExpiredBefore,
			vipExpiredAfter)
		if err != nil {
			log.Printf("ProcAliOrder WritePurchaseHistoryToDB exec failed tradeNo=%v productId=%v itermCode=%v from=%v to=%v",
				order.outTradeNo,
				order.attach.productId,
				order.attach.itermCode,
				order.attach.fromId,
				order.attach.toId)
			time.Sleep(10 * time.Second) // 休眠10秒
			continue
		}
		break
	}
	if tryCount == 0 {
		attr := "alipay/write_purchase_history_to_db_failed"
		libcomm.AttrAdd(attr, 1)
		log.Printf("ProcAliOrder WritePurchaseHistoryToDB failed uid=%v tryCount=%v", userId, tryCount)
		// 不返回继续执行
	}

	// step6: 发送请求到QIMServer(需要查询用户在哪台服务器上)实时更新会员信息
	reqBody := new(ht_thirdpay.ThirdPayReqBody)
	reqBody.UpdateUserVipInfoReqbody = &ht_thirdpay.UpdateUserVipInfoReqBody{
		FromId:           proto.Uint32(order.attach.fromId),
		ToId:             proto.Uint32(userId),
		VipExpiredBefore: proto.Uint64(vipExpiredBefore),
		VipExpiredAfter:  proto.Uint64(vipExpiredAfter),
		VipYear:          proto.Uint32(uint32(vipYearOut)),
		VipType:          proto.Uint32(uint32(vipTypeOut)),
		ItermCode:        proto.Uint32(order.attach.itermCode),
		PayType:          proto.Uint32(PAYTYPE_WECHATPAY),
		TotalFee:         []byte(order.totalFee),
		OutTradeNo:       []byte(order.outTradeNo),
		ProductId:        []byte(order.attach.productId),
		PurchDate:        []byte(WeChatTimeToSqlTime(order.timeEnd)),
	}

	err = SendPacketToIMServer(userId, reqBody)
	if err != nil {
		log.Printf("ProcAliOrder SendPacketToIMServer failed tradeno=%s from=%v to=%v itermCode=%v paytype=%v",
			order.outTradeNo,
			order.attach.fromId,
			userId,
			order.attach.itermCode,
			PAYTYPE_WECHATPAY)
	}

	// step7: 通知usercache清除用户缓存
	userCacheReqBody := new(ht_user.ReqBody)
	userCacheReqBody.User = []*ht_user.UserInfoBody{&ht_user.UserInfoBody{
		UserID: proto.Uint32(userId),
	},
	}
	err = SendPacketToUserCache(userId, userCacheReqBody)
	if err != nil {
		log.Printf("ProcAliOrder SendPacketToUserCache failed tradeno=%s from=%v to=%v itermCode=%v paytype=%v",
			order.outTradeNo,
			order.attach.fromId,
			userId,
			order.attach.itermCode,
			PAYTYPE_WECHATPAY)
	}
	return nil
}

// SUBJECT varchar(64) DEFAULT '' COMMENT '商品名称' 回调的时候没有填写如果后续客户端也提交上来，需要更新此字段
func StorePurchaseOrderInDB(order *AliPayOrder) (err error) {
	if db == nil {
		log.Printf("StorePurchaseOrderInDB nil db object")
		err = ErrNilDbObject
		return err
	}
	r, err := db.Exec("insert into HT_PURCHRECORD_ALIPAY(TRADENO,USERID,ITEMCODE,PURCHASEDATE,PARTNER,SELLER,SUBJECT,TOTALFEE,SUCCESS,UPDATETIME) value"+
		"(?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP())",
		order.tradeNo,
		order.userId,
		order.,
		WeChatTimeToSqlTime(order.timeEnd),
		order.mchId,
		order.mchId,
		order.totalFee,
		order.returnCode,
		order.resultCode)
	if err != nil {
		log.Printf("StorePurchaseOrderInDB insert faield err=%s", err.Error())
		return err
	} else {
		id, err := r.LastInsertId()
		if err != nil {
			log.Printf("StorePurchaseOrderInDB r.LastInsertId failed err=%s", err)
		}
		log.Printf("StorePurchaseOrderInDb insert success order=%#v lastInsertId=%v", order, id)
	}
	return nil
}

// 支付完成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见
func WeChatTimeToSqlTime(weChatTime string) (outTime string) {
	if len(weChatTime) < LegelTimeLenght {
		log.Printf("WeChatTimeToSqlTime illegal time=%s", weChatTime)
		return outTime
	}
	b := []byte(weChatTime)
	outTime = string(b[0:4]) + "-" + string(b[4:6]) + "-" + string(b[6:8]) + " " + string(b[8:10]) + ":" + string(b[10:12]) + ":" + string(b[12:14])
	log.Printf("outTime=%s", outTime)
	return outTime
}

// 更新会员创建群成员数限制
func UpdateMucRoomMemberLimit(opUid uint32) (err error) {
	if mucApi == nil {
		err = ErrNilMucObject
		return err
	}
	reqBody := new(ht_muc.MucReqBody)
	reqBody.UpdateRoomMemberLimitReqbody = new(ht_muc.UpdateRoomMemberLimitReqBody)
	subMucReqBody := reqBody.GetUpdateRoomMemberLimitReqbody()
	subMucReqBody.OpUid = proto.Uint32(opUid)

	head := &common.HeadV3{Flag: uint8(common.CServToServ),
		Version:  common.CVerMmedia,
		CryKey:   uint8(common.CNoneKey),
		TermType: uint8(0),
		Cmd:      uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_UPDATE_ROOM_MEMBER_LIMIT),
		Seq:      0,
		From:     opUid,
		To:       opUid,
		Len:      0,
	}

	reqSlice, err := proto.Marshal(reqBody)
	if err != nil {
		log.Printf("UpdateMucRoomMemberLimit proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return err
	}

	ret, err := mucApi.SendPacket(head, reqSlice)
	if err != nil {
		log.Printf("UpdateMucRoomMemberLimit mucApi.SendPacket failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return err
	}
	if ret == 0 {
		log.Printf("UpdateMucRoomMemberLimit mucApi.SendPacket success uid=%v ret=%v", opUid, ret)
	} else {
		log.Printf("UpdateMucRoomMemberLimit mucApi.SendPacket failed uid=%v ret=%v", opUid, ret)
	}
	return nil
}

// 根据订单信息修改会员的过期天数
func UpdateVipExpiredTime(userId, itermCode uint32, utilTimestamp uint64, purchPeriod uint64) (vipExpiredBefore, vipExpiredAfter uint64, vipYearOut, vipTypeOut uint8, err error) {
	// 购买会员成功更新会员天数
	log.Printf("UpdateVipExpiredTime userId=%v itermCode=%v utilTimestamp=%v purchPeroid=%v",
		userId,
		itermCode,
		utilTimestamp,
		purchPeriod)

	lastIterm, vipYear, vipType, expiredTS, err := GetUserVIPInfo(userId)
	if err != nil {
		log.Printf("UpdateVipExpiredTime exec GetUserVIPInfo failed userUid=%v", userId)
		return 0, 0, 0, 0, err
	}
	log.Printf("UpdateVipExpiredTime lastIterm=%v vipYear=%v vipType=%v expiredTS=%v", lastIterm, vipYear, vipType, expiredTS)
	vipExpiredBefore = expiredTS

	timeStampNow := uint64(time.Now().Unix())
	for {
		// 购买的是年会员
		if itermCode == PRODUCT_ONE_YEAR ||
			itermCode == PRODUCT_GIFT_ONEYEAR ||
			itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN || // 高价格年费会员
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C { // 年费试用会员
			vipYear = YEAR_VIP
			break
		}

		// 以前是年会员 现在还没有过期 依旧是年会员
		if vipYear == YEAR_VIP && expiredTS > timeStampNow {
			vipYear = YEAR_VIP
			break
		}

		vipYear = YEAR_NORMAL
		break
	}

	// 原来的会员是否已经过期
	// 判断会员类型 终生会员 > 自动续费会员 > 剩余天数会员
	if expiredTS >= timeStampNow { // 会员没有过期
		// 未过期的有顺序判断
		if itermCode == PRODUCT_LIFT_TIME ||
			itermCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
			itermCode == PRODUCT_LIFE_TIME_B ||
			itermCode == PRODUCT_LIFE_TIME_C ||
			vipType == E_LIFETIME_VIP {
			vipType = E_LIFETIME_VIP
		} else if itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C ||
			vipType == E_YEARAUTO_VIP {
			vipType = E_YEARAUTO_VIP
		} else if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C ||
			vipType == E_MONTHAUTO_VIP {
			vipType = E_MONTHAUTO_VIP
		} else {
			vipType = E_DAYS_VIP
		}
	} else {
		// 已经过期的以当前购买的为准
		if itermCode == PRODUCT_LIFT_TIME ||
			itermCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
			itermCode == PRODUCT_LIFE_TIME_B ||
			itermCode == PRODUCT_LIFE_TIME_C {
			vipType = E_LIFETIME_VIP
		} else if itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C {
			vipType = E_YEARAUTO_VIP
		} else if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C {
			vipType = E_MONTHAUTO_VIP
		} else {
			vipType = E_DAYS_VIP
		}
	}
	// 更改到期时间戳
	countSecond := CountPostponeSeconds(itermCode)
	log.Printf("UpdateVipExpiredTime countSecond=%v purchPeriod=%v", countSecond, purchPeriod)
	if expiredTS < timeStampNow {
		// 已经过期或是第一次购买的 月费试用会员和年费试用会员都是自动续费类型
		if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C {
			// 自动续费的以续费过期时间为准
			expiredTS = utilTimestamp
		} else {
			// 其他的认为在当前时间往后延长
			expiredTS = timeStampNow + countSecond
		}
	} else {
		if itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_YEAR_SUB {
			// 对于月费试用会员和年费试用会员 将过期时间修改成 当前时间 + 订单里面的持续时间 不再写死会员的过期时间
			expiredTS += purchPeriod
		} else {
			// 还没有过期则在原来的到期时间戳上延长
			expiredTS += countSecond
		}
	}

	log.Printf("UpdateVipExpiredTime uid=%v lastIterm=%v vipYear=%v vipType=%v expiredTS=%v",
		userId,
		lastIterm,
		vipYear,
		vipType,
		expiredTS)
	err = UpdateUserVIPInfo(userId, itermCode, vipYear, vipType, expiredTS)
	if err != nil {
		log.Printf("UpdateVipExpiredTime UpdateUserVIPInfo failed lastIterm=%v vipYear=%v vipType=%v expiredTS=%v err=%s",
			lastIterm,
			vipYear,
			vipType,
			expiredTS,
			err)
	} else {
		log.Printf("UpdateVipExpiredTime UpdateUserVIPInfo ok lastIterm=%v vipYear=%v vipType=%v expiredTS=%v",
			lastIterm,
			vipYear,
			vipType,
			expiredTS)
	}

	// 更新成功返回
	vipExpiredAfter = expiredTS
	vipYearOut = vipYear
	vipTypeOut = vipType
	return vipExpiredBefore, vipExpiredAfter, vipYearOut, vipTypeOut, nil

}

func GetUserVIPInfo(userId uint32) (lastIterm, vipYear, vipType uint8, expiredTS uint64, err error) {
	if db == nil {
		err = ErrNilDbObject
		return 0, 0, 0, 0, err
	}
	if userId == 0 {
		err = ErrInputParamErr
		return 0, 0, 0, 0, err
	}

	err = db.QueryRow("select EXPIRETIME,LASTPURCHITEM,VIPYEAR,VIPTYPE from HT_PURCHASE_TRANSLATE where USERID = ?;", userId).Scan(&expiredTS, &lastIterm, &vipYear, &vipType)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("GetUserVIPInfo not found uid=%v", userId)
		break
	case err != nil:
		log.Printf("GetUserVIPInfo exec failed uid=%v, err=%s", userId, err)
		break
	default:
		log.Printf("GetUserVIPInfo uid=%v expiredTs=%v lastIterm=%v vipYear=%v vipType=%v", userId, expiredTS, lastIterm, vipYear, vipType)
	}
	return lastIterm, vipYear, vipType, expiredTS, nil
}

func CountPostponeSeconds(productCode uint32) (outSecond uint64) {
	if productCode == PRODUCT_ONE_MONTH ||
		productCode == PRODUCT_ONE_MONTHAUTO ||
		productCode == PRODUCT_GIFT_ONEMONTH ||
		productCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
		productCode == PRODUCT_ONE_MONTH_AUTO_B ||
		productCode == PRODUCT_ONE_MONTH_AUTO_C {
		outSecond = 31 * 24 * 3600
	} else if productCode == PRODUCT_THREE_MONTH ||
		productCode == PRODUCT_GIFT_THREEMONTH {
		outSecond = 92 * 24 * 3600
	} else if productCode == PRODUCT_ONE_YEAR ||
		productCode == PRODUCT_GIFT_ONEYEAR ||
		productCode == PRODUCT_YEAR_AUTO ||
		productCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
		productCode == PRODUCT_YEAR_AUTO_B ||
		productCode == PRODUCT_YEAR_AUTO_C {
		outSecond = 365 * 24 * 3600
	} else if productCode == PRODUCT_LIFT_TIME ||
		productCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
		productCode == PRODUCT_LIFE_TIME_B ||
		productCode == PRODUCT_LIFE_TIME_C {
		// 终生会员做50年处理
		outSecond = 50 * 365 * 24 * 3600
	} else if productCode == PRODUCT_ONE_MONTH_SUB {
		outSecond = 7 * 24 * 3600 // 月会员试用 返回的天数UpdateVipExpiredTime没有使用
	} else if productCode == PRODUCT_ONE_YEAR_SUB {
		outSecond = 31 * 24 * 3600 // 年会员试用 返回的天数UpdateVipExpiredTime没有使用
	}
	return outSecond
}

func UpdateUserVIPInfo(userId, itermCode uint32, vipYear, vipType uint8, expiredTimestamp uint64) (err error) {
	if db == nil {
		err = ErrNilDbObject
		return err
	}

	if userId == 0 || itermCode == 0 {
		return ErrInputParamErr
	}

	_, err = db.Exec("insert into HT_PURCHASE_TRANSLATE set USERID=?, CREATETIME=UTC_TIMESTAMP(), EXPIRETIME=?, "+
		"LASTPURCHITEM=?, VIPYEAR=?, VIPTYPE=?, UPDATETIME=UTC_TIMESTAMP() on duplicate key update "+
		"LASTPURCHITEM=?, EXPIRETIME=?, VIPYEAR=?, VIPTYPE=?, UPDATETIME=UTC_TIMESTAMP();",
		userId,
		expiredTimestamp,
		itermCode,
		vipYear,
		vipType,
		itermCode,
		expiredTimestamp,
		vipYear,
		vipType)
	if err != nil {
		log.Printf("UpdateUserVIPInfo insert faield userId=%v expireTimeStamp=%v itermCode=%v vipYear=%v vipType=%v err=%s",
			userId,
			expiredTimestamp,
			itermCode,
			vipYear,
			vipType,
			err)
		return err
	} else {
		return nil
	}
}

func WriteUserSendPresentRecordInDB(orderId string, userId, toId, presentIterm, payType uint32) (err error) {
	if db == nil {
		err = ErrNilDbObject
		return err
	}

	if userId == 0 || presentIterm == 0 {
		return ErrInputParamErr
	}
	_, err = db.Exec("insert into HT_PRESENTSEND_RECORD set ORDERID = ?, USERID = ?, TOID = ?, ITEMCODE = ?, PAYTYPE = ?, SENDTIME = UTC_TIMESTAMP();",
		orderId,
		userId,
		toId,
		presentIterm,
		payType)
	if err != nil {
		log.Printf("WriteUserSendPresentRecordInDB insert faield orderId=%s userId=%v toId=%v presentItem=%v payType=%v err=%s",
			orderId,
			userId,
			toId,
			presentIterm,
			payType,
			err)
		return err
	} else {
		return nil
	}
}

func WritePurchaseHistoryToDB(orderId, productId string,
	itemCode, giftDays, userId, toId, payType, clientOrServer uint32,
	currency, payMoney, formatMoney, buyTime string,
	utilBefore, utilAfter uint64) (err error) {

	log.Printf("WritePurchaseHistoryToDB orderId=%s productId=%s itemCode=%v giftDays=%v userId=%v toId=%v payType=%v clientOrServer=%v currency=%s payMoney=%s formatMoney=%s buyTime=%s utilBefore=%v utilAfter=%v",
		orderId,
		productId,
		itemCode,
		giftDays,
		userId,
		toId,
		payType,
		clientOrServer,
		currency,
		payMoney,
		formatMoney,
		buyTime,
		utilBefore,
		utilAfter)
	if db == nil {
		err = ErrNilDbObject
		return err
	}

	if len(orderId) == 0 || len(productId) == 0 || itemCode == 0 || userId == 0 || toId == 0 || payType == 0 {
		log.Printf("WritePurchaseHistoryToDB invalid param orderId=%s productId=%s itemCode=%v userId=%v toId=%v payType=%v",
			orderId,
			productId,
			itemCode,
			userId,
			toId,
			payType)
		return ErrInputParamErr
	}
	_, err = db.Exec("insert into HT_PURCHASE_HISTORY set ORDERID=?, ITEMCODE=?, PRODUCTID=?, GIFTDAYS=?,"+
		"USERID=?, TOID=?, CURRENCY=?, PAYMONEY=?, FORMATMONEY=?, PAYTYPE=?, BUYTIME=?,"+
		"UTILBEFORE=?, UTILAFTER=?, UPDATETIME=UTC_TIMESTAMP(), CLIENT_OR_SRV=?;",
		orderId,
		itemCode,
		productId,
		giftDays,
		userId,
		toId,
		currency,
		payMoney,
		formatMoney,
		payType,
		buyTime,
		utilBefore,
		utilAfter,
		clientOrServer)
	if err != nil {
		log.Printf("WritePurchaseHistoryToDB insert failed orderId=%s productId=%s itemCode=%v userId=%v toId=%v payType=%v",
			orderId,
			productId,
			itemCode,
			userId,
			toId,
			payType)
		return err
	} else {
		return nil
	}
}

func SendPacketToIMServer(userId uint32, reqBody *ht_thirdpay.ThirdPayReqBody) (err error) {
	// 查询用户的在线状态
	onlineStat, err := mcApi.GetUserOnlineStat(userId)
	if err != nil {
		log.Printf("SendPacketToIMServer user online failed uid=%v err=%v", userId, err)
		fromId := reqBody.GetUpdateUserVipInfoReqbody().GetFromId()
		// 查询用户在线失败则查询发起方在线
		onlineStat, err = mcApi.GetUserOnlineStat(fromId)
		if err != nil {
			log.Printf("SendPacketToIMServer Get user online failed get default user uid=%v err=%v default=%v", fromId, err, DEFAULT_USER)
			// 查询发起方在线失败查询默认用户在线(经常活跃 mc 中一定存在在线)
			onlineStat, err = mcApi.GetUserOnlineStat(DEFAULT_USER)
			if err != nil {
				log.Printf("SendPacketToIMServer Get default user online failed get default user uid=%v err=%v", DEFAULT_USER, err)
				return err
			}
		}
	}
	head := &common.HeadV3{Flag: uint8(common.CServToServ),
		Version:  common.CVerMmedia,
		CryKey:   uint8(common.CNoneKey),
		TermType: onlineStat.ClientType,
		Cmd:      uint16(ht_thirdpay.THIRDPAY_CMD_TYPE_GO_CMD_UPDATE_UPDATE_VIP_INOF),
		Seq:      0,
		From:     reqBody.GetUpdateUserVipInfoReqbody().GetFromId(),
		To:       reqBody.GetUpdateUserVipInfoReqbody().GetToId(),
		Len:      0,
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		log.Printf("SendPacketToIMServer proto marshal user=%v to=%v err=%v",
			userId,
			reqBody.GetUpdateUserVipInfoReqbody().GetToId(),
			err)
		return err
	}
	err = SendPacketToIMServerRelabile(onlineStat, head, payLoad)
	if err != nil {
		log.Printf("SendPacketToIMServer exec SendPacketToIMServerRelabile failed user=%v to=%v err=%v",
			userId,
			reqBody.GetUpdateUserVipInfoReqbody().GetToId(),
			err)
		return err
	}
	return nil
}

func SendPacketToIMServerRelabile(stat *common.UserState, head *common.HeadV3, payLoad []byte) (err error) {
	svrIp := inet_ntoa(int64(stat.SvrIp)).String()
	log.Printf("SendPacketToIMServerRelabile svrIP=%s oriIP=%v", svrIp, stat.SvrIp)
	v, ok := imServerPool[svrIp]
	if !ok { // 不存在直接打印日志返回错误
		log.Printf("SendPacketToIMServerRelabile not exist IM ip=%v imServerPool=%v", svrIp, imServerPool)
		return ErrOnlineIpErr
	}

	tryCount := 2
	for tryCount > 0 {
		ret, err := v.SendPacket(head, payLoad)
		if err == nil && ret == 0 {
			log.Printf("SendPacketToIMServerRelabile send succ from=%v to=%v seq=%v cmd=%v", head.From, head.To, head.Seq, head.Cmd)
			return nil
		} else {
			log.Printf("SendPacketToIMServerRelabile failed from=%v to=%v seq=%v cmd=%v err=%v ret=%v", head.From, head.To, head.Seq, head.Cmd, err, ret)
		}
		tryCount--
	}
	return common.ErrReachMaxTryCount
}

func SendPacketToUserCache(userId uint32, reqBody *ht_user.ReqBody) (err error) {
	head := &common.HeadV2{
		Version:  common.CVerMmedia,
		Cmd:      uint32(ht_user.ProtoCmd_CMD_MOD_USERINFO),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      userId,
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		log.Printf("SendPacketToUserCache proto marshal user=%v err=%s",
			userId,
			err)
		return err
	}
	ret, err := userCacheApi.SendPacket(head, payLoad)
	if err != nil {
		log.Printf("SendPacketToUserCache userCacheApi.SendPacket failed userId=%v err=%s", userId, err)
		return err
	}
	log.Printf("SendPacketToUserCache userCacheApi.SendPacket ret=%v", ret)
	return nil
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	// 读取 Memcache Ip and port
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustInt(11211)
	log.Printf("memcache ip=%v port=%v", mcIp, mcPort)
	mcApi = new(common.MemcacheApi)
	mcApi.Init(mcIp + ":" + strconv.Itoa(mcPort))

	// 读取muc 配置
	mucIp := cfg.Section("MUC").Key("muc_ip").MustString("127.0.0.1")
	mucPort := cfg.Section("MUC").Key("muc_port").MustString("0")
	mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	log.Printf("muc server ip=%v port=%v connLimit=%v", mucIp, mucPort, mucConnLimit)
	mucApi = common.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, mucConnLimit)

	// 读取user cache 配置
	userCacheIp := cfg.Section("USERCACHE").Key("user_cache_ip").MustString("127.0.0.1")
	userCachePort := cfg.Section("USERCACHE").Key("user_cache_port").MustString("0")
	userCacheConnLimit := cfg.Section("USERCACHE").Key("pool_limit").MustInt(1000)
	log.Printf("user cache server ip=%v port=%v connLimit=%v", userCacheIp, userCachePort, userCacheConnLimit)
	userCacheApi = common.NewUserCacheApi(userCacheIp, userCachePort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, userCacheConnLimit)

	// 读取IMServer 配置
	imCount := cfg.Section("IMSERVER").Key("imserver_cnt").MustInt(2)
	imConnLimit := cfg.Section("IMSERVER").Key("pool_limit").MustInt(1000)
	imServerPool = make(map[string]*common.ImServerApiV2, imCount)
	log.Printf("IMServer Count=%v", imCount)
	for i := 0; i < imCount; i++ {
		ipKey := "imserver_ip_" + strconv.Itoa(i)
		ipOnlineKye := "imserver_ip_online_" + strconv.Itoa(i)
		portKey := "imserver_port_" + strconv.Itoa(i)
		imIp := cfg.Section("IMSERVER").Key(ipKey).MustString("127.0.0.1")
		imIpOnline := cfg.Section("IMSERVER").Key(ipOnlineKye).MustString("127.0.0.1")
		imPort := cfg.Section("IMSERVER").Key(portKey).MustString("18380")

		log.Printf("im server ip=%v ip_online=%v port=%v", imIp, imIpOnline, imPort)
		imServerPool[imIpOnline] = common.NewImServerApiV2(imIp, imPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, imConnLimit)
	}
	log.Printf("imServerPool=%v", imServerPool)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	log.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		log.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("wechat_pay")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch1")

	// init ansp consumter
	wg = &sync.WaitGroup{}
	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
