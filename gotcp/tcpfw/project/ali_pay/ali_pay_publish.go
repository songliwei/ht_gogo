package main

import (
	"fmt"
	"log"

	simplejson "github.com/bitly/go-simplejson"
	"github.com/nsqio/go-nsq"
)

func main() {
	config := nsq.NewConfig()
	w, _ := nsq.NewProducer("127.0.0.1:4150", config)
	msgObj := simplejson.New()
	msgObj.Set("bank_type", "CFT")
	msgObj.Set("mch_id", "1233820102")
	msgObj.Set("sign", "0E7043CED9A0E2E53522B79F70FFD120")
	msgObj.Set("total_fee", "1")
	msgObj.Set("trade_type", "APP")
	msgObj.Set("transaction_id", "4003762001201701095751108279")
	msgObj.Set("nonce_str", "56EAF25A3ACF494CBCB7A8C94187048D")
	msgObj.Set("fee_type", "CNY")
	msgObj.Set("is_subscribe", "N")
	msgObj.Set("time_end", "20170109172744")
	msgObj.Set("appid", "wxd061f34c48968028")
	msgObj.Set("cash_fee", "1")
	msgObj.Set("result_code", "SUCCESS")
	msgObj.Set("openid", "omehiuAV-4KQH-rOmoSMpF0rzo2s")
	msgObj.Set("return_code", "SUCCESS")
	msgObj.Set("out_trade_no", "3months_1946612_df789ab0b34afe12")

	// attach
	attach := simplejson.New()
	attach.Set("from_id", 1946612)
	attach.Set("to_id", "1946612")
	attach.Set("itemcode", 4)
	attach.Set("ostype", 1)
	attach.Set("version", "2.2.8")
	attach.Set("product_id", "com.hellotalk.3months")

	msgObj.Set("attach", attach)

	message, err := msgObj.MarshalJSON()
	if err != nil {
		fmt.Printf("MarshalJSON failed")
		return
	}
	err = w.Publish("wechat_pay", message)
	if err != nil {
		log.Panic("Could not connect")
	}
	fmt.Printf("Publish success")
	w.Stop()
}
