// author Jordanlu
// 举报行为通信协议结构体
package util

import (
	"github.com/HT_GOGO/gotcp/tcpfw/common/msg"
)

const (
	// 举报类型
	REP_REPORT_TYPE_BLACKPULL    = 0   // 拉黑用户
	REP_REPORT_TYPE_REPORTPERSON = 1   // 拉黑举报用户
	REP_REPORT_TYPE_REPORTIMAGE  = 2   // 举报图片
	REP_REPORT_TYPE_REPORTMOMENT = 3   // 举报信息流
	REP_REPORT_TYPE_UNKNOWREPORT = 255 // 不明举报类型

	// 举报/拉黑入口
	REP_ENTRACE_UNKNOWN                   = 0  // 未知类型
	REP_ENTRACE_CHATLIST_TO_PROFILE       = 1  // 聊天列表进入profile进行拉黑/举报
	REP_ENTRACE_CHATPAGE_TO_PROFILE       = 2  // 聊天界面进入profile进行拉黑/举报
	REP_ENTRACE_SINGLE_CHAT_SETTING       = 3  // 单人聊天设置进行拉黑/举报
	REP_ENTRACE_MUC_CHAT_SETTING          = 4  // 群聊聊天设置进行拉黑/举报
	REP_ENTRACE_SEARCHLIST_TO_PROFILE     = 5  // 搜索列表进入profile进行拉黑/举报
	REP_ENTRACE_FRIENDLIST_TO_PROFILE     = 6  // 好友列表进入profile进行拉黑/举报
	REP_ENTRACE_MONENTLIST_TO_PROFILE     = 7  // 动态列表进入profile进行拉黑/举报
	REP_ENTRACE_MONENT_COMMENT_TO_PROFILE = 8  // 帖子评论列表进入profile进行拉黑/举报
	REP_ENTRACE_MONENT_NOTIFY_TO_PROFILE  = 9  // 信息流通知列表进入profile进行拉黑/举报
	REP_ENTRACE_LIKELIST_TO_PROFILE       = 10 // 赞列表进入profile进行拉黑/举报

	// 举报理由
	REP_REASON_OTHER             = 0 // 其他
	REP_REASON_ASK_PIC_CONTACT   = 1 // 无理由要求照片/个人联系
	REP_REASON_LANG_NOT_MATCH    = 2 // 语言不匹配
	REP_REASON_HARASS            = 3 // 纠缠骚扰
	REP_REASON_PORNOGRAPHIC      = 4 // 色情不雅
	REP_REASON_IMPURE_MOTIVE     = 5 // 动机不良-寻找女友/男友
	REP_REASON_PROFANITY         = 6 // 粗言秽语
	REP_REASON_JUNK_MSG          = 7 // 垃圾信息
	REP_REASON_RELIGION_POLITICS = 8 // 宗教或政治

)

// 拉黑请求包结构
type BlackPullReq struct {
	UserId        uint32       `json:"user_id" binding:"omitempty"`
	ReportId      uint32       `json:"report_id" binding:"omitempty"`
	ReportConfirm uint32       `json:"report_confirm" binding:"omitempty"`
	ReportType    uint32       `json:"report_type" binding:"omitempty"`
	ReportEntrace uint32       `json:"report_entrace" binding:"omitempty"`
	ReportContent string       `json:"report_content" binding:"omitempty"`
	ReportReason  string       `json:"report_reason" binding:"omitempty"`
	ReportMsg     []*msg.IMMsg `json:"report_msg" binding:"required"`
}

func UnCompressStr(compressedStr string) (re []byte, err error) {
	compressBuff := bytes.NewBuffer(compressedStr)
	r, err := zlib.NewReader(compressBuff)
	defer r.Close()
	if err != nil {
		infoLog.Printf("UnCompressStr zlib.NewReader failed err=%v", err)
		return re, err
	}
	// 解压缩
	unCompressSlice, err := ioutil.ReadAll(r)
	if err != nil {
		infoLog.Printf("UnCompressStr zlib. unCompress failed")
		return re, err
	}
	re = unCompressSlice
	return re, nil
}
