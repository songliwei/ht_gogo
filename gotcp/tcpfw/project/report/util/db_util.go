// author Jordanlu
package util

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gansidui/gotcp/libcomm"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
)

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

func (this *DbUtil) AddToBlacklist(userId, blackId uint32) error {
	if userId == 0 || blackId == 0 {
		this.infoLog.Printf("AddToBlacklist param invalid userId=%d blackId=%d", userId, blackId)
		return errors.New("param invalid")
	}

	// 这里抛弃使用 replace语句,因为replace可能会引起一些问题详情见：https://stackoverflow.com/questions/9168928/what-are-practical-differences-between-replace-and-insert-on-duplicate-ke
	re, err := this.db.Exec("INSERT INTO HT_BLACK_LIST (USERID, BLACKID, FLAG, TIME) VALUES (?,?,1,UTC_TIMESTAMP()) ON DUPLICATE KEY UPDATE ;", userId, blackId)
	if err != nil {
		this.infoLog.Printf("AddToBlacklist param invalid userId=%d blackId=%d", userId, blackId)

	}
}
