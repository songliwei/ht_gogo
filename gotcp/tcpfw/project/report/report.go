package main

import (
	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/project/report/util"
	webcommon "github.com/HT_GOGO/gotcp/webapi/common"

	"bytes"
	"compress/zlib"
	"encoding/json"
	"fmt"
)

type Callback struct{}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV3packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v from=%v", head.Cmd, head.Seq, head.From)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		return false
	}

	attr := "goreport/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	// infoLog.Printf("OnMessage trans cmd=%v", ht_wallet.WALLET_CMD_TYPE(head.Cmd))
	switch ht_report.CMD_TYPE(head.Cmd) {
	case ht_report.CMD_TYPE_CMD_BLACK_PULL_REQ:
		go ProcBlackPull(c, head, reqBody)
	}
}

func GenBlackPullRetPack(result uint32, reason string) ([]byte, error) {
	rspBody.BlackPullRspBody = &ht_report.BlackPullRspBody{
		Status: &ht_report.ReportHeader{
			Code:   proto.Uint32(result),
			Reason: []byte(reason),
		},
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("GenBlackPullRetPack proto.Marshal failed err=%s", err)
	}

	return rspPayLoad, err
}

func ProcBlackPull(c *gotcp.Conn, head *common.HeadV2, reqBody *ht_report.ReprotReqBody) (rspPayLoad []byte, err error) {
	// parse packet
	result := uint16(ht_report.REPORT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_report.ReportRspBody)
	// 这个是有没有后续要处理的。
	// bNeedCall := true
	// defer func() {
	// 	if bNeedCall {
	// 		SendRsp(c, head, rspBody, result)
	// 	} else {
	// 		infoLog.Printf("ProcBlackPull not need call")
	// 	}
	// }()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint32(ht_report.REPORT_RET_CODE_RET_INVALID_PARAM)
		infoLog.Printf("ProcBlackPull invalid param from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		return GenBlackPullRetPack(result, "invalid param")
	}

	// add static
	libcomm.AttrAdd("goreport/nbpr_req_cnt", 1)
	reqBody := new(ht_report.ReportReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBlackPull proto Unmarshal failed from=%v cmd=0x%4x seq=%v", head.From, head.Cmd, head.Seq)
		result = uint32(ht_report.REPORT_RET_CODE_RET_PB_ERR)
		return GenBlackPullRetPack(result, "proto unmarshal failed")
	}

	subReqBody := reqBody.GetBlackPullReqbody()
	CompressJson := subReqBody.GetCompressJson()
	// 解压内容
	unCompressSlice, err := util.UnCompressStr(CompressJson)
	if err != nil {
		infoLog.Printf("ProcBlackPull zlib. unCompress failed")
		result = uint32(ht_report.REPORT_RET_CODE_RET_INTERNAL_ERR)
		return GenBlackPullRetPack(result, "Internal error")
	}
	infoLog.Printf("ProcBlackPull unCompressSlice string : %s", string(unCompressSlice))

	// 解析请求包
	req := util.BlackPullReq{}
	if err = json.Unmarshal(unCompressSlice, &req); err != nil {
		infoLog.Printf("ProcBlackPull Unmarshal fail : %v", err)
		result = uint32(ht_report.REPORT_RET_CODE_RET_INTERNAL_ERR)
		return GenBlackPullRetPack(result, "Internal error")
	}

	// 下面的是翻译C++的代码

	// 检查参数
	if req.UserId == 0 || req.ReportId == 0 || req.ReportConfirm > 1 || req.ReportType > util.REP_REPORT_TYPE_REPORTMOMENT {
		libcomm.AttrAdd("goreport/nbpr_input_err_cnt", 1)
		infoLog.Printf("ProcBlackPull invalid param userid=%d, reportId=%d, comfirm=%d, reprotType=%d", req.UserId, req.ReportId, req.ReportConfirm, req.ReportType)
		result = uint32(ht_report.REPORT_RET_CODE_RET_INVALID_PARAM)
		return GenBlackPullRetPack(result, "Black pull invalid param")
	}
	// 如果是举报拉黑某人，又没有确认举报
	if req.ReportConfirm == 0 && (req.ReportType == util.REP_REPORT_TYPE_BLACKPULL || req.ReportType == util.REP_REPORT_TYPE_REPORTPERSON) {
		isFollowerEachOther := false
		// 不是互相关注的才进下一步
		if !isFollowerEachOther {
			// 查找聊天数量
			nSpeakToMe := 1
			if nSpeakToMe < 2 {
				libcomm.AttrAdd("goreport/nbpr_is_confirm_cnt", 1)
				result = uint32(ht_report.REPORT_RET_CODE_RET_NEED_COMFIRM)
				return GenBlackPullRetPack(result, "Is confirm to report?")
			}
		}
	}
	// 检查举报次数是否超过每天限制
	if util.IsReachReportLimit(req.UserId) {
		infoLog.Printf("ProcBlackPull IsReachReportLimit true userid=%d", req.UserId)
		result = uint32(ht_report.REPORT_RET_CODE_RET_OVER_LIMIT)
		return GenBlackPullRetPack(result, "Can't report stranger any more.")
	}
	// 写入数据库
	err = dbUtil.AddToBlacklist(req.UserId, req.ReportId)
	if err != nil {
		infoLog.Printf("ProcBlackPull AddToBlacklist fail err=%d", err)
		result = uint32(ht_report.REPORT_RET_CODE_RET_INTERNAL_ERR)
		return GenBlackPullRetPack(result, "Internal error")
	}

	return true
}

var (
	infoLog *log.Logger
	options webcommon.Options
	dbUtil  *util.DbUtil
)

func main() {
	// 加载配置文件
	cfg, err := webcommon.InitLogAndOption(&options, &infoLog)
	webcommon.CheckError(err)

	// 自身服务的配置
	srvIP := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("0.0.0.0")
	srvPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustString("9889")

	// MYSQL
	dbHost := cfg.Section("MYSQL").Key("ip").MustString("0.0.0.0")
	dbPort := cfg.Section("MYSQL").Key("port").MustString("3306")
	dbUser := cfg.Section("MYSQL").Key("username").MustString("")
	dbPass := cfg.Section("MYSQL").Key("password").MustString("")
	dbName := cfg.Section("MYSQL").Key("database").MustString("HT_IMDB")
	dbChar := cfg.Section("MYSQL").Key("charset").MustString("utf8mb4")

	infoLog.Printf("Config server socket=%s:%s", srvIP, srvPort)
	// 创建服务
	tcpAddr, err := net.ResolveTCPAddr("tcp4", srvIP+":"+srvPort)
	webcommon.CheckError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	webcommon.CheckError(err)

	// 建立Mysql连接
	mysqlDSN := dbUser + ":" + dbPass + "@" + "tcp(" + dbHost + ":" + dbPort + ")/" + dbName + "?charset=" + dbChar + "&timeout=90s&interpolateParams=true"
	infoLog.Printf("Connecting to Mysql: %s:%s username=%s, password=%s, database=%s, charset=%s ",
		dbHost,
		dbPort,
		dbUser,
		dbPass,
		dbName,
		dbChar)

	db, err := sql.Open("mysql", mysqlDSN)
	webcommon.CheckError(err)
	dbUtil = util.NewDbUtil(db, infoLog)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
