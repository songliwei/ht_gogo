package org

import (
	"database/sql"
	"errors"
	"fmt"
	apicommon "github.com/HT_GOGO/gotcp/webapi/common"
	sphinx "github.com/lilien1010/sphinx"
	"log"
	//"math"
	"strconv"
	"strings"
)

const (
	STRGOOGLE           = "google"
	LOCATIONTOPLACEHMAP = "location#placeid"
	MULTILANGNAMEHMAP   = "country#lang"
	MULTIPLACEINFOHMAP  = "place#lang"

	KTTLInterval = 2592000 // TTL life time = 30*24*3600 second , 自己看自己的

	ViewKTTLInterval = 1296000 // TTL life time = 30*24*3600 second ，自己看别的，别人的缓存周期

	GOOGLE_API_OK    = 200
	UNKOWN_RESULT    = 500
	ZERO_RESULT      = 501
	INVALID_REQUEST  = 502
	OVER_QUERY_LIMIT = 503
)

//SphinxDbAPI handle sphinx
//Conn exposes a set of callbacks for the various events that occur on a connection
type GeoDbAPI struct {
	SphinxClient *sphinx.Client
	//MyInfo       *UserResult
	infoLog        *log.Logger
	Masterdb       *sql.DB
	queryIndex     string
	GOOGLE_API_KEY string
}

var (
	ErrInputParam = errors.New("err param error")
)

var MultiLangCountryMap map[string]map[string]string

var LanguageShortName map[string]string = map[string]string{"Chinese": "zh",
	"English":    "en",
	"Japanese":   "ja",
	"Korean":     "ko",
	"Spanish":    "es",
	"Portugues":  "pt",
	"Portuguese": "pt",
	"French":     "fr",
	"German":     "de",
	"Italian":    "it",
	"Russian":    "ru",
	"Arabic":     "ar",
	"Chinese_yy": "zh-TW"}

var AllNonEnglishLBSLang = []string{"zh", "zh-TW", "ar", "ru", "it", "de", "fr", "pt", "es", "ko", "ja"}

type TagPlaceInformation struct {
	Country      string `json:"country" binding:"omitempty"`
	FullCountry  string `json:"full_country" binding:"omitempty"`
	Admin1       string `json:"admin1" binding:"omitempty"`
	Admin2       string `json:"admin2" binding:"omitempty"`
	Admin3       string `json:"admin3" binding:"omitempty"`
	Locality     string `json:"locality" binding:"omitempty"`
	SubLocality  string `json:"subLocality" binding:"omitempty"`
	Neighborhood string `json:"neighborhood" binding:"omitempty"`
	//	PlaceId      uint32 `json:"placeid" binding:"omitempty"`

	//以这个字段作为显示在客户端的描述
	City string `json:"city" binding:"omitempty"`
}

type TagLocationInfo struct {
	Uid         uint32              `json:"userid" binding:"omitempty"`
	Ostype      uint8               `json:"ostype" binding:"omitempty"`
	Allowed     uint8               `json:"allowed" binding:"omitempty"`
	TimeStamp   uint64              `json:"timestamp" binding:"omitempty"`
	PlaceId     uint32              `json:"placeid" binding:"omitempty"`
	CityPlaceId uint32              `json:"cityplaceid" binding:"omitempty"`
	Latitude    string              `json:"latitude" binding:"omitempty"`
	Longtitude  string              `json:"longtitude" binding:"omitempty"`
	Source      string              `json:"source" binding:"omitempty"`
	PlaceInfo   TagPlaceInformation `json:"place_info" binding:"omitempty"`

	IsNewPlace int `json:"is_new_place" binding:"omitempty"`
}

//用户提交新定位到服务端，同时更新事件到NSQ
//type=[user_update_location,new_locatioin_pace]
//无论是用户更新定位还是提交了新定位 都用 TagPlaceInfo 来记录信息
type EventForLBSInfo struct {
	Type string           `json:"type" binding:"required"` //
	Data *TagLocationInfo `json:"data" binding:"required"`
}

//提交到 新的 队列
type UserEvent struct {
	Cmd      string           `json:"cmd" binding:"required"`
	Info     *EventForLBSInfo `json:"info" binding:"required"`
	Userid   uint32           `json:"user_id" binding:"required"`
	ServerTs uint64           `json:"server_ts" binding:"required"`
	Ostype   uint8            `json:"os_type" binding:"required"`
}

//提交到浏览器要处理的任务结构
type LBSGoogleTaskInfo struct {
	Id         uint32 `json:"id" binding:"required"` //
	Lang       string `json:"lang" binding:"required"`
	Coordinate string `json:"lo" binding:"required"`
	Type       string `json:"type" binding:"required"`
}

//google one result
type GoogleResultItem struct {
	LongName  string   `json:"long_name" binding:"required"`
	ShortName string   `json:"short_name" binding:"required"`
	Types     []string `json:"types" binding:"required"`
}

func (this *GoogleResultItem) IsTypeMatch(Type string) bool {

	for _, v := range this.Types {
		if v == Type {
			return true
		}
	}

	return false
}

//google
type LBSGoogleResult struct {
	AddressComponents []GoogleResultItem `json:"address_components" binding:"required"`
	FormattedAddress  string             `json:"formatted_address" binding:"omitempty"`
	GPlaceId          string             `json:"place_id" binding:"omitempty"`
	types             []string           `json:"types" binding:"required"`
}

type LBSGoogleResultBody struct {
	Results []LBSGoogleResult `json:"results" binding:"omitempty"`
	Status  string            `json:"status" binding:"required"`
	Error   string            `json:"error_message" binding:"omitempty"`
}

/*

	UNKOWN_RESULT    = 500
	ZERO_RESULT      = 501
	INVALID_REQUEST  = 502
	OVER_QUERY_LIMIT = 503
*/
func (this *LBSGoogleResultBody) GetTagPlaceInformation() (uint32, *TagPlaceInformation) {

	if len(this.Results) == 0 {

		if this.Status == "ZERO_RESULT" {
			return ZERO_RESULT, nil
		}

		if this.Status == "INVALID_REQUEST" {
			return INVALID_REQUEST, nil
		}

		if this.Status == "OVER_QUERY_LIMIT" {
			return OVER_QUERY_LIMIT, nil
		}

		return UNKOWN_RESULT, nil
	}

	res := &TagPlaceInformation{}

	Components := this.Results[0]

	for _, v := range Components.AddressComponents {

		if v.IsTypeMatch("country") {
			res.Country = v.ShortName
		} else if v.IsTypeMatch("administrative_area_level_1") {
			res.Admin1 = v.LongName
		} else if v.IsTypeMatch("administrative_area_level_2") {
			res.Admin2 = v.LongName
		} else if v.IsTypeMatch("administrative_area_level_3") {
			res.Admin3 = v.LongName
		} else if v.IsTypeMatch("locality") {
			res.Locality = v.LongName
		} else if v.IsTypeMatch("sublocality") {
			res.SubLocality = v.LongName
		} else if v.IsTypeMatch("neighborhood") {
			res.Neighborhood = v.LongName
		}
	}

	return GOOGLE_API_OK, res
}

func MatchLanguageShortName(languageType string) (shortName string) {
	shortName, ok := LanguageShortName[languageType]
	if !ok {
		shortName = "en"
		return
	}
	return
}

func (placeInfo *TagPlaceInformation) GetProvinceAndCity() (Province string, cityName string) {

	if placeInfo.Country == "HK" || placeInfo.Country == "SG" || placeInfo.Country == "IL" {
		if placeInfo.Admin1 != "" && placeInfo.Neighborhood != "" {
			return placeInfo.Admin1, placeInfo.Neighborhood
		}
	}

	if placeInfo.Admin1 != "" && placeInfo.Locality != "" {
		return placeInfo.Admin1, placeInfo.Locality
	}

	if placeInfo.Admin1 != "" && placeInfo.SubLocality != "" {
		return placeInfo.Admin1, placeInfo.SubLocality
	}

	if placeInfo.Admin1 != "" && placeInfo.Neighborhood != "" {
		return placeInfo.Admin1, placeInfo.Neighborhood
	}

	if placeInfo.Admin1 != "" && placeInfo.Admin3 != "" {
		return placeInfo.Admin1, placeInfo.Admin3
	}

	if placeInfo.Admin1 != "" && placeInfo.Admin2 != "" {
		return placeInfo.Admin1, placeInfo.Admin2
	}

	if placeInfo.Admin2 != "" && placeInfo.Locality != "" {
		return placeInfo.Admin2, placeInfo.Locality
	}

	if placeInfo.Admin2 != "" && placeInfo.SubLocality != "" {
		return placeInfo.Admin2, placeInfo.SubLocality
	}

	return
}

func (placeInfo *TagPlaceInformation) ReformCountryName(languageType string) {

	multiCountryName := GetMultiCountryName(placeInfo.Country, languageType)
	if len(multiCountryName) > 0 {
		placeInfo.FullCountry = (multiCountryName)
	} else {
		placeInfo.FullCountry = placeInfo.Country
	}

}

func (placeInfo *TagPlaceInformation) ReformLocationCityName() (cityName string) {

	if placeInfo.Country == "HK" ||
		placeInfo.Country == "TW" ||
		placeInfo.Country == "SG" ||
		placeInfo.Country == "IL" {
		//小地区特殊处理
		if len(placeInfo.Admin1) > 0 {
			return placeInfo.Admin1
		}

		if len(placeInfo.Admin2) > 0 {
			return placeInfo.Admin2
		}

		if len(placeInfo.Admin3) > 0 {
			return placeInfo.Admin3
		}
		if len(placeInfo.SubLocality) > 0 {
			return placeInfo.SubLocality
		}
		if len(placeInfo.Neighborhood) > 0 {
			return placeInfo.Neighborhood
		}
	}

	if len(placeInfo.Locality) > 0 {
		return placeInfo.Locality
	}
	if len(placeInfo.SubLocality) > 0 {
		return placeInfo.SubLocality
	}
	if len(placeInfo.Neighborhood) > 0 {
		return placeInfo.Neighborhood
	}
	if len(placeInfo.Admin3) > 0 {
		return placeInfo.Admin3
	}
	if len(placeInfo.Admin2) > 0 {
		return placeInfo.Admin2
	}

	if len(placeInfo.Admin1) > 0 {
		return placeInfo.Admin1
	}
	return
}

func (PlaceInfo *TagPlaceInformation) SetPlace(country, Admin1, Admin2, Admin3, Locality, SubLocality, Neighborhood string) {
	PlaceInfo.Admin1 = Admin1
	PlaceInfo.Admin2 = Admin2
	PlaceInfo.Admin3 = Admin3
	PlaceInfo.Country = country
	PlaceInfo.Locality = Locality
	PlaceInfo.SubLocality = SubLocality
	PlaceInfo.Neighborhood = Neighborhood
}

func (PlaceInfo *TagPlaceInformation) GetCacheString() string {
	value := PlaceInfo.Country + "#" + PlaceInfo.Admin1 + "#" + PlaceInfo.Admin2 + "#" + PlaceInfo.Admin3 + "#" + PlaceInfo.Locality + "#" + PlaceInfo.SubLocality + "#" + PlaceInfo.Neighborhood

	return value
}

func (loca *TagLocationInfo) GetUserCacheString() string {

	cityid := loca.CityPlaceId

	if cityid == 0 {
		cityid = loca.PlaceId
	}

	value := fmt.Sprintf("%d#%d#%s#%s#%s#%s#%s#%s#%s#%s#%s#%d#%d", loca.Uid,
		loca.Allowed,
		loca.Latitude,
		loca.Longtitude,
		loca.PlaceInfo.Country,
		loca.PlaceInfo.Admin1,
		loca.PlaceInfo.Admin2,
		loca.PlaceInfo.Admin3,
		loca.PlaceInfo.Locality,
		loca.PlaceInfo.SubLocality,
		loca.PlaceInfo.Neighborhood,
		loca.TimeStamp,
		cityid)

	return value
}

func (locationInfo *TagLocationInfo) IsValid() bool {
	if locationInfo.Allowed != 1 {
		return true
	}
	if (len(locationInfo.Latitude) == 0) ||
		(len(locationInfo.Longtitude) == 0) ||
		(strings.Compare(("0"), (locationInfo.Latitude)) == 0) ||
		(strings.Compare(("1"), (locationInfo.Latitude)) == 0) ||
		(strings.Compare(("0.0"), (locationInfo.Latitude)) == 0) ||
		(strings.Compare(("1.1"), (locationInfo.Latitude)) == 0) ||
		(strings.Compare(("0"), (locationInfo.Longtitude)) == 0) ||
		(strings.Compare(("1"), (locationInfo.Longtitude)) == 0) ||
		(strings.Compare(("0.0"), (locationInfo.Longtitude)) == 0) ||
		(strings.Compare(("1.1"), (locationInfo.Longtitude)) == 0) ||
		(strings.Compare((locationInfo.Latitude), (locationInfo.Longtitude)) == 0) {
		return false
	}

	_, err1 := strconv.ParseFloat(locationInfo.Latitude, 64)
	_, err2 := strconv.ParseFloat(locationInfo.Longtitude, 64)

	if err1 != nil || err2 != nil {
		return false
	}

	return true
}

func GetLocationPlaceIdKey(placeInfo *TagPlaceInformation) string {
	return fmt.Sprintf("#%s#%s#%s#%s#%s#%s#%s",
		placeInfo.Country,
		placeInfo.Admin1,
		placeInfo.Admin2,
		placeInfo.Admin3,
		placeInfo.Locality,
		placeInfo.SubLocality,
		placeInfo.Neighborhood)
}

func GetMultiCountryNameKey(countryName string, languageType string) string {
	return fmt.Sprintf("#%s#%s", countryName, languageType)
}

func GetMultiPlaceInfoKey(placeId int64, shortName string) string {
	return fmt.Sprintf("%v#%s", placeId, shortName)
}

func GetUserPlaceKey(uid uint32) string {
	return fmt.Sprintf("%v#place", uid)
}

//把#分隔的定位细信息到转到结构体,同时告诉redis 要不要刷新
func StrLocationToTagLocationInfo(uid uint32, strLocation string, locationInfo *TagLocationInfo) (needUpdate bool, err error) {

	// 解析位置 地址位置使用'#'分隔
	fieldArry := strings.Split(strLocation, "#")
	if len(fieldArry) < 12 { // 一共12个字段
		return false, fmt.Errorf("bad format  size=", strLocation)
	}

	if uid == 0 {
		if uid, err := strconv.Atoi(fieldArry[0]); err != nil {
			return false, fmt.Errorf(" Conv uid err =", err.Error())
		} else {
			locationInfo.Uid = uint32(uid)
		}
	} else {
		locationInfo.Uid = uid

	}

	if allow, err := strconv.Atoi(fieldArry[1]); err != nil {
		return false, fmt.Errorf("Conv Allowed err =", err.Error())
	} else {
		locationInfo.Allowed = uint8(allow)
	}

	if fieldArry[3] != "" {
		for i, v := range fieldArry {
			// infoLog.Printf("index=%v value=%s", i, v)
			switch i { // 前面已经处理了index=0、1的元素
			case 0: // does not need the value
			case 1: // does not need the value

			case 2:
				locationInfo.Latitude = (v)
			case 3:
				locationInfo.Longtitude = (v)
			case 4:
				locationInfo.PlaceInfo.Country = (v)
			case 5:
				locationInfo.PlaceInfo.Admin1 = (v)
			case 6:
				locationInfo.PlaceInfo.Admin2 = (v)
			case 7:
				locationInfo.PlaceInfo.Admin3 = (v)
			case 8:
				locationInfo.PlaceInfo.Locality = (v)
			case 9:
				locationInfo.PlaceInfo.SubLocality = (v)
			case 10:
				locationInfo.PlaceInfo.Neighborhood = (v)
			default:

			}
		}
	}

	if len(fieldArry) == 13 {
		cityId, _ := strconv.Atoi(fieldArry[12])
		locationInfo.CityPlaceId = uint32(cityId)
	}

	if ts, err := strconv.Atoi(fieldArry[11]); err != nil {
		return false, fmt.Errorf("Conv TimeStamp err =", err.Error())
	} else {
		locationInfo.TimeStamp = uint64(ts)
	}

	//只有12行 说明内存要刷新了，添加 cityplaceid
	if len(fieldArry) == 12 {
		return true, nil
	}
	return false, nil
}

func InitMultiLangCountryMapForDb(db *sql.DB, infoLog *log.Logger) error {

	allLangs := []string{}
	for k, _ := range LanguageShortName {
		allLangs = append(allLangs, k)
	}
	var CountryName, Chinese, English, Japanese, Korean, Spanish, Portugues, French, German, Italian, Russian, Arabic, Chinese_yy string
	formatString := "SELECT CountryName,Chinese, English, Japanese, Korean, Spanish, Portugues, French, German, Italian, Russian, Arabic, Chinese_yy  from HT_MULTI_COUNTRY"
	rows, err := db.Query(formatString)

	if err != nil {
		return fmt.Errorf("InitMultiLangCountryMapForDb err=%s", err.Error())
	}
	MultiLangCountryMap = map[string]map[string]string{}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&CountryName, &Chinese, &English, &Japanese, &Korean, &Spanish, &Portugues, &French, &German, &Italian, &Russian, &Arabic, &Chinese_yy)

		if err != nil {
			return fmt.Errorf("InitMultiLangCountryMapForDb Scan err=%s", err.Error())
		} else {

			// infoLog.Println("InitMultiLangCountryMapForDb country=", CountryName, Chinese, English, Japanese, Korean, Spanish, Portugues, French, German, Italian, Russian, Arabic, Chinese_yy)
			MultiLangCountryMap[CountryName] = map[string]string{
				"Chinese":    Chinese,
				"English":    English,
				"Japanese":   Japanese,
				"Korean":     Korean,
				"Spanish":    Spanish,
				"Portugues":  Portugues,
				"French":     French,
				"German":     German,
				"Italian":    Italian,
				"Russian":    Russian,
				"Arabic":     Arabic,
				"Chinese_yy": Chinese_yy,
			}
		}

	}
	return nil
}

func GetMultiCountryName(CountryName, LangType string) string {

	val, ok := MultiLangCountryMap[CountryName]
	if ok {
		name, ok1 := val[LangType]
		if ok1 {
			return name
		} else {
			name, ok3 := val["English"]
			if ok3 {
				return name
			}
		}
	}

	return CountryName

}

func NewGeoDbAPI(destIP string, destPort int, queryIndex string, infoLog *log.Logger, Masterdb *sql.DB, GOOGLE_API_KEY string) *GeoDbAPI {

	sc := sphinx.NewClient().SetServer(destIP, destPort).SetConnectTimeout(20000).SetConnectDuration(1000).SetRetries(2, 1)

	if err := sc.Error(); err != nil {
		infoLog.Println("NewGeoDbAPI() SphinxClient NewClient fail,err=", err)
	}

	if err := sc.Open(); err != nil {
		infoLog.Println("NewGeoDbAPI() SphinxClient Open fail,err=", err)
	}

	if err := sc.GetDb(20000/1000, 4, 1); err != nil {
		// return nil, err
		infoLog.Println("NewGeoDbAPI() SphinxClient GetDb fail,err=", err)

	}

	sc.SetMaxQueryTime(3)

	return &GeoDbAPI{
		SphinxClient:   sc,
		queryIndex:     queryIndex,
		infoLog:        infoLog,
		Masterdb:       Masterdb,
		GOOGLE_API_KEY: GOOGLE_API_KEY,
	}
}

func (this *GeoDbAPI) GetCoordinateFromGoogleApi(Uid uint32, Latitude, Longitute, Lang string, reqSource string) (uint32, *TagPlaceInformation, error) {

	url := fmt.Sprintf("https://maps.googleapis.com/maps/api/geocode/json?sensor=true&language=%s&latlng=%s,%s&key=%s", Lang, Latitude, Longitute, this.GOOGLE_API_KEY)

	timeout := int(8)

	googleRes := &LBSGoogleResultBody{}

	err := apicommon.GetJsonByHTTPAPI(url, timeout, &googleRes)

	if err != nil {
		this.infoLog.Println("GetCoordinateFromGoogleApi fail, err=", err, Uid, Latitude, Longitute, Lang)
		return 0, nil, err
	}

	this.infoLog.Printf("GetCoordinateFromGoogleApi request uid=%d,lat=%s,lng=%s,lang=%s,reqSource=%s, res=%#s", Uid, Latitude, Longitute, Lang, reqSource, googleRes)

	this.infoLog.Println("GetCoordinateFromGoogleApi fail, err=", err)

	code, loca := googleRes.GetTagPlaceInformation()

	if code == GOOGLE_API_OK {
		return code, loca, nil
	} else {

		return 0, nil, fmt.Errorf("GetCoordinateFromGoogleApi return error,code=%d,reqSource=%s", code, reqSource)
	}

}

//更新DB里面的lbs信息
func (this *GeoDbAPI) UpdateLocationInDb(locationInfo *TagLocationInfo) (err error) {
	if locationInfo == nil {
		this.infoLog.Println("UpdateLocationInDb input nil")
		err = ErrInputParam
		return err
	}
	r, err := this.Masterdb.Exec("INSERT INTO HT_USER_LOCATION2 (USERID, ALLOWED, LATITUDE, LONGITUDE,"+
		"PLACEID, COUNTRY, ADMINISTRATIVE1, ADMINISTRATIVE2, ADMINISTRATIVE3, LOCALITY, "+
		"SUBLOCALITY, NEIGHBORHOOD,CITYPLACEID, UPDATETIME) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, FROM_UNIXTIME(?))"+
		"ON DUPLICATE KEY UPDATE ALLOWED=VALUES(ALLOWED),LATITUDE=VALUES(LATITUDE),LONGITUDE=VALUES(LONGITUDE),"+
		"PLACEID=VALUES(PLACEID),COUNTRY=VALUES(COUNTRY),ADMINISTRATIVE1=VALUES(ADMINISTRATIVE1),ADMINISTRATIVE2=VALUES(ADMINISTRATIVE2),"+
		"ADMINISTRATIVE3=VALUES(ADMINISTRATIVE3),LOCALITY=VALUES(LOCALITY), SUBLOCALITY=VALUES(SUBLOCALITY),"+
		"NEIGHBORHOOD=VALUES(NEIGHBORHOOD), CITYPLACEID=VALUES(CITYPLACEID), UPDATETIME=VALUES(UPDATETIME)",
		locationInfo.Uid,
		locationInfo.Allowed,
		locationInfo.Latitude,
		locationInfo.Longtitude,
		locationInfo.PlaceId,
		locationInfo.PlaceInfo.Country,
		locationInfo.PlaceInfo.Admin1,
		locationInfo.PlaceInfo.Admin2,
		locationInfo.PlaceInfo.Admin3,
		locationInfo.PlaceInfo.Locality,
		locationInfo.PlaceInfo.SubLocality,
		locationInfo.PlaceInfo.Neighborhood,
		locationInfo.CityPlaceId,
		locationInfo.TimeStamp)
	if err != nil {
		this.infoLog.Println("UpdateLocationInDb() insert faield err =", err, locationInfo.Uid)
		return err
	} else {
		affectRow, err := r.RowsAffected()
		if err != nil {
			this.infoLog.Println("UpdateLocationInDb() affectRow faied err =", err, locationInfo.Uid)
		}
		this.infoLog.Printf("UpdateLocationInDb() HT_USER_LOCATION2 affectRow=%d,uid=%d,Placeid=%d,Cityplaceid=%d", affectRow, locationInfo.Uid, locationInfo.PlaceId, locationInfo.CityPlaceId)
		// 更新HT_USER_LOCATION2 成功 继续更新HT_USER_PROPERTY
		_, err = this.Masterdb.Exec("update HT_USER_PROPERTY set LOCATIONVERSION = ? where USERID = ?", locationInfo.TimeStamp, locationInfo.Uid)
		if err != nil {
			this.infoLog.Println("UpdateLocationInDb() update HT_USER_PROPERTY failed err =", err, locationInfo.Uid)
			return err
		}
		return nil
	}
}

func (this *GeoDbAPI) GetCityPlaceIdFromDb(PlaceId uint32, Uid uint32, PlaceInfo *TagPlaceInformation) (CityPlaceid uint32) {

	StrCN := PlaceInfo.Country
	Admin1 := PlaceInfo.Admin1
	Admin2 := PlaceInfo.Admin2
	Admin3 := PlaceInfo.Admin3
	Locality := PlaceInfo.Locality
	SubLocality := PlaceInfo.SubLocality
	Neighborhood := PlaceInfo.Neighborhood
	QuerySql := "SELECT PLACEID FROM GEO_LOCATION_CITY_EN WHERE COUNTRY=? AND PROVINCE=? AND CITY=? AND CITY!=''"

	if Admin1 != "" && Locality != "" {

		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin1, Locality).Scan(&CityPlaceid)

		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get placeid=%d,Uid=%d,cityplaceid=%d,level1", PlaceId, Uid, CityPlaceid)
			return
		}

	}

	if StrCN == "SG" && Neighborhood != "" {

		err := this.Masterdb.QueryRow(QuerySql, StrCN, Locality, Neighborhood).Scan(&CityPlaceid)

		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get placeid=%d,Uid=%d,cityplaceid=%d,levelsg", PlaceId, Uid, CityPlaceid)
			return
		}

	}

	if StrCN == "KR" && Locality != "" {

		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin1, Locality).Scan(&CityPlaceid)

		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get placeid=%d,Uid=%d,cityplaceid=%d,levelkr", PlaceId, Uid, CityPlaceid)
			return
		}

	}

	if Locality == "" && SubLocality == "" && Admin1 != "" && Neighborhood != "" {
		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin1, Neighborhood).Scan(&CityPlaceid)
		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get  placeid=%d,Uid=%d,cityplaceid=%d,level1_ne", PlaceId, Uid, CityPlaceid)
			return
		}
	}

	if Admin1 != "" && Admin3 != "" && Locality == "" && SubLocality == "" && Neighborhood == "" {
		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin1, Admin3).Scan(&CityPlaceid)
		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get  placeid=%d,Uid=%d,cityplaceid=%d,level1_3", PlaceId, Uid, CityPlaceid)
			return
		}
	}

	if Admin1 == "" && Admin2 != "" && Admin3 != "" && Locality == "" && SubLocality == "" && Neighborhood == "" {
		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin2, Admin3).Scan(&CityPlaceid)
		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get  placeid=%d,Uid=%d,cityplaceid=%d,level2_3", PlaceId, Uid, CityPlaceid)
			return
		}
	}

	if Admin1 != "" && Admin3 != "" {
		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin1, Admin3).Scan(&CityPlaceid)
		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get placeid=%d,Uid=%d,cityplaceid=%d,level4", PlaceId, Uid, CityPlaceid)
			return
		}
	}

	if Locality == "" && Admin1 != "" && Admin2 != "" && Admin3 == "" {
		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin1, Admin2).Scan(&CityPlaceid)
		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get  placeid=%d,Uid=%d,cityplaceid=%d,leveltr", PlaceId, Uid, CityPlaceid)
			return
		}
	}

	if Locality != "" {
		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin2, Locality).Scan(&CityPlaceid)

		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get placeid=%d,Uid=%d,cityplaceid=%d,level2", PlaceId, Uid, CityPlaceid)
			return
		}
	}

	if SubLocality != "" {
		err := this.Masterdb.QueryRow(QuerySql, StrCN, Admin1, SubLocality).Scan(&CityPlaceid)

		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get placeid=%d,Uid=%d,cityplaceid=%d,level3", PlaceId, Uid, CityPlaceid)
			return
		}

		err = this.Masterdb.QueryRow(QuerySql, StrCN, Admin2, SubLocality).Scan(&CityPlaceid)
		if err == nil && CityPlaceid > 0 {
			this.infoLog.Printf("GetCityPlaceIdFromDb() get  placeid=%d,Uid=%d,cityplaceid=%d,level5", PlaceId, Uid, CityPlaceid)
			return
		}
	}

	this.infoLog.Printf("GetCityPlaceIdFromDb() get cityplaceid fail,err=empty PlaceId=%v Uid=%v country=%s"+
		" admin1=%s admin2=%s admin3=%s locality=%s subloc=%s neibhood=%s",
		PlaceId,
		Uid,
		StrCN,
		Admin1,
		Admin2,
		Admin3,
		Locality,
		SubLocality,
		Neighborhood)

	return
}

func (this *GeoDbAPI) GetPlaceInfoFromSphinx(Uid uint32, Latitude, Longitute string, distance uint32) (placeid uint32, PlaceInfo *TagPlaceInformation, err error) {

	latFloat, err1 := strconv.ParseFloat(Latitude, 64)
	lngFloat, err2 := strconv.ParseFloat(Longitute, 64)

	if err1 != nil || err2 != nil {
		return 0, nil, fmt.Errorf("bad location %s,%s", Latitude, Longitute)
	}

	latRad := apicommon.Deg2rad(latFloat)
	lngRad := apicommon.Deg2rad(lngFloat)

	query := fmt.Sprintf("SELECT placeid,COUNTRY,ADMINISTRATIVE1,ADMINISTRATIVE2,ADMINISTRATIVE3,LOCALITY,SUBLOCALITY,NEIGHBORHOOD,"+
		"GEODIST(%v, %v, latitude, longitude) as distance FROM %s WHERE distance < %d ORDER BY distance ASC LIMIT 0,1",
		latRad,
		lngRad,
		this.queryIndex,
		distance)

	rows, err := this.SphinxClient.DB.Query(query)

	if err != nil {
		return 0, nil, err
	}

	var StrCN, Admin1, Admin2, Admin3, Locality, SubLocality, Neighborhood string
	var relDistance float64
	placeidMaps := map[uint32]int{}

	for rows.Next() {

		if err := rows.Scan(&placeid, &StrCN, &Admin1, &Admin2, &Admin3, &Locality, &SubLocality, &Neighborhood, &relDistance); err != nil {
			this.infoLog.Printf("GetPlaceInfoFromSphinx() rows.Scan failed")
			return 0, nil, err
		}

		_, ok := placeidMaps[placeid]

		if ok {
			placeidMaps[placeid]++
		} else {
			placeidMaps[placeid] = 1
		}
	}
	rows.Close()

	this.infoLog.Printf("GetCityPlaceIdFromDb() get  place from sphinx,Uid=%d PlaceId=%v country=%s"+
		" admin1=%s admin2=%s admin3=%s locality=%s subloc=%s neibhood=%s distance=%v",
		Uid,
		placeid,
		StrCN,
		Admin1,
		Admin2,
		Admin3,
		Locality,
		SubLocality,
		Neighborhood, relDistance)

	placeInfo := &TagPlaceInformation{}
	placeInfo.SetPlace(StrCN, Admin1, Admin2, Admin3, Locality, SubLocality, Neighborhood)

	return placeid, placeInfo, nil
}
