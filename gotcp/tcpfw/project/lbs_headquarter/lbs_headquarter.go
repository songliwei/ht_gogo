package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/project/lbs_headquarter/org"
	apicommon "github.com/HT_GOGO/gotcp/webapi/common"
	_ "github.com/go-sql-driver/mysql"
	sphinx "github.com/lilien1010/sphinx" //经过改造的sphinx go driver
	"github.com/nsqio/go-nsq"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	// "strconv"
)

var (
	GlobalRecvMsgPackChan chan *org.EventForLBSInfo
	infoLog               *log.Logger
	SphinxClient          *sphinx.Client
	SphinxIndex           string
	UserCacheRedis        *common.RedisApi
	lbsCacheRedis         *common.RedisApi

	GeoDbClient *org.GeoDbAPI

	nsqUserUpdateInfoPublisher *nsq.Producer
	nsqUserUpdateInfoTopic     string

	Masterdb *sql.DB

	options        apicommon.Options
	GOOGLE_API_KEY string
)

const (
	PROJECT_NAME = "lbs_headquarter"
)

//{"cmd":"update_location","info":{"data":"20180306/7a355535c13924ab0_84646.jpg","type":"update_location"},"os_type":0,"server_ts":1520393441,"user_id":8022781}

func MessageHandle(message *nsq.Message, idx int) (uint32, error) {

	info := &org.EventForLBSInfo{}
	err := json.Unmarshal(message.Body, info) // JSON to Struct

	if err != nil {
		infoLog.Printf("PostData err=%s", err.Error())
		return 500, err
	}

	infoLog.Printf("MessageHandle() start chanIdx=%d Type=%v,Data=%#v", idx, info.Type, info.Data)

	if info.Type == "user_update_location" {
		HandleForUserUpdateLocation(info.Type, info.Data)

	} else if info.Type == "new_locatioin_pace" {
		HandleForNewPlaceAdded(info.Type, info.Data)
	}

	libcomm.ProAttrAdd(PROJECT_NAME, info.Type, 1)

	return 0, nil

}

//用户更新城市
// 1：查询CITYPLACEID 信息
// 2：更新经纬度信息和城市 SPHINX
// 3：如果是IP定位 使用浏览器任务队列处理
func HandleForUserUpdateLocation(Type string, Locationinfo *org.TagLocationInfo) {

	if len(Locationinfo.Latitude) <= 3 {
		infoLog.Printf("HandleForUserUpdateLocation Latitude error ", Locationinfo.Latitude)
		return
	}

	if Locationinfo.PlaceId == 0 {
		infoLog.Printf("HandleForUserUpdateLocation() empty placeid=%#v", Locationinfo.PlaceId)
		return
	}

	curVersion := uint64(apicommon.GetMilliNow()/1000) + 1

	CityPlaceId := Locationinfo.CityPlaceId

	isNeedToUpdateCify := false

	if Locationinfo.Source == "ipcity" || Locationinfo.PlaceInfo.Country == "" {

		code, PlaceInfo, err := GeoDbClient.GetCoordinateFromGoogleApi(Locationinfo.Uid, Locationinfo.Latitude, Locationinfo.Longtitude, "en", "ipcity")

		if PlaceInfo != nil && err == nil && code == org.GOOGLE_API_OK {

			CityPlaceId = GeoDbClient.GetCityPlaceIdFromDb(Locationinfo.PlaceId, Locationinfo.Uid, PlaceInfo)

			if PlaceInfo.GetCacheString() != Locationinfo.PlaceInfo.GetCacheString() {
				isNeedToUpdateCify = true
				//google result 覆盖
				Locationinfo.PlaceInfo = *PlaceInfo
			} else {
				libcomm.ProAttrAdd(PROJECT_NAME, "google_result_not_match", 1)
			}

			infoLog.Printf("HandleForUserUpdateLocation CityPlaceId from google OK, userid=%d,source=%s", Locationinfo.Uid, Locationinfo.Source)

		} else {

			if code == org.ZERO_RESULT {
				_, err := Masterdb.Exec("UPDATE HT_USER_LOCATION2 SET ALLOWED=0,CITYPLACEID=?,PLACEID=?,UPDATETIME=FROM_UNIXTIME(?) WHERE USERID = ?", CityPlaceId, CityPlaceId, curVersion, Locationinfo.Uid)
				if err != nil {
					infoLog.Println("HandleForUserUpdateLocation update HT_USER_LOCATION2 failed err =", err, Locationinfo.Uid)
				}
			}

			infoLog.Printf("HandleForUserUpdateLocation  googleapi fail, err=%v,userid=%d,source=%s,code=%d", err, Locationinfo.Uid, Locationinfo.Source, code)
		}

	} else {

		if CityPlaceId == 0 {
			CityPlaceId = GeoDbClient.GetCityPlaceIdFromDb(Locationinfo.PlaceId, Locationinfo.Uid, &Locationinfo.PlaceInfo)
		} else {
			CityPlaceId = Locationinfo.CityPlaceId
		}
	}

	Locationinfo.CityPlaceId = CityPlaceId

	if isNeedToUpdateCify {

		// Locationinfo.PlaceId = CityPlaceId

		Locationinfo.TimeStamp = curVersion

		// 更新HT_USER_LOCATION2 和 HT_USER_PROPERTY表
		err := GeoDbClient.UpdateLocationInDb(Locationinfo)

		if err != nil {
			infoLog.Printf("HandleForUserUpdateLocation() UpdateLocationInDb failed uid=%v err=%s", Locationinfo.Uid, err)
		}

		//更新lbs内存数据
		value := Locationinfo.GetUserCacheString()

		strKey := org.GetUserPlaceKey(Locationinfo.Uid)
		err = lbsCacheRedis.Setex(strKey, value, org.KTTLInterval)
		if err != nil {
			infoLog.Printf("HandleForUserUpdateLocation() Uid=%d,key=%s value=%s faield err=%v", Locationinfo.Uid, strKey, value, err)
		} else {
			infoLog.Printf("HandleForUserUpdateLocation() Uid=%d, SETEX key=%s value=%s succ", Locationinfo.Uid, strKey, value)
		}

	}

	if Locationinfo.Source == org.STRGOOGLE {

		r, err := Masterdb.Exec("INSERT INTO GEO_MAP (latitude,longitude,placeid,status,country,updatetime) value"+
			"(?, ?, ?, ?, ?,  NOW())",
			Locationinfo.Latitude,
			Locationinfo.Longtitude,
			Locationinfo.PlaceId,
			0,
			Locationinfo.PlaceInfo.Country)

		if err != nil {
			infoLog.Printf("HandleForUserUpdateLocation insert GEO_MAP err=%s,placeid=%d,lang=%s,place=%#v", err.Error(), Locationinfo.PlaceId)
		} else {
			affectRow, _ := r.RowsAffected()
			infoLog.Printf("HandleForUserUpdateLocation insert GEO_MAP affectRow=%d,lat=%s,lng=%s,placeid=%d",
				affectRow,
				Locationinfo.Latitude,
				Locationinfo.Longtitude,
				Locationinfo.PlaceId)
		}
	}

	Locationinfo.PlaceInfo.City = Locationinfo.PlaceInfo.ReformLocationCityName()

	PublishLBSEventToUserUpdateNsq(Locationinfo.Uid, &org.EventForLBSInfo{
		Type: Type,
		Data: Locationinfo,
	})
}

//将用户更新定位的信息发布到nsq
func PublishLBSEventToUserUpdateNsq(Userid uint32, info *org.EventForLBSInfo) (uint32, error) {

	event := org.UserEvent{
		Userid:   Userid,
		Cmd:      "user_update_location",
		Info:     info,
		ServerTs: apicommon.GetMilliNow() / 1000,
		Ostype:   uint8(info.Data.Ostype),
	}

	buff, err := json.Marshal(event)

	if err != nil {
		return 0, err
	}

	err = nsqUserUpdateInfoPublisher.Publish(nsqUserUpdateInfoTopic, buff)

	if err != nil {
		libcomm.ProAttrAdd(PROJECT_NAME, "to_user_updateinfo_nsq_failed", 1)

		infoLog.Printf("PublishLBSEventToUserUpdateNsq() fail Uid=%d err=%s", Userid, err.Error())

		return 0, err
	} else {
		infoLog.Printf("PublishLBSEventToUserUpdateNsq() Uid=%d OK,info=%#v", Userid, info.Data)
	}

	return 0, err
}

//有新的城市来
//1：插入数据到 HT_MULTILANG_PLACE
//2：插入任务到redis队列
func HandleForNewPlaceAdded(Type string, Locationinfo *org.TagLocationInfo) {

	if Locationinfo.PlaceId == 0 {
		infoLog.Printf("HandleForNewPlaceAdded() PlaceId=%d", Locationinfo.PlaceId)
		return
	}

	canAddToCity := false
	for _, v := range org.AllNonEnglishLBSLang {

		code, placeInfo, err := GeoDbClient.GetCoordinateFromGoogleApi(Locationinfo.Uid, Locationinfo.Latitude, Locationinfo.Longtitude, v, "multilang")

		if code == org.GOOGLE_API_OK && placeInfo != nil && err == nil {
			libcomm.ProAttrAdd(PROJECT_NAME, "google_maps_api_ok", 1)
			canAddToCity = true
			PlaceInfoToMultiDbAndCache(Locationinfo.PlaceId, Locationinfo.Latitude, Locationinfo.Longtitude, v, 900, placeInfo)
		} else {
			libcomm.ProAttrAdd(PROJECT_NAME, "google_maps_api_fail", 1)
			infoLog.Println("HandleForNewPlaceAdded GetCoordinateFromGoogleApi err=", err.Error(), Locationinfo.PlaceId)
		}

		if code == org.ZERO_RESULT {
			PlaceInfoToMultiDbAndCache(Locationinfo.PlaceId, Locationinfo.Latitude, Locationinfo.Longtitude, v, -1, placeInfo)
			infoLog.Println("HandleForNewPlaceAdded org.ZERO_RESULT stop,fail ", Locationinfo.PlaceId)
			break
		}

	}

	Province, City := Locationinfo.PlaceInfo.GetProvinceAndCity()

	//CityPlaceId GEO_LOCATION_CITY_EN
	if canAddToCity && Province != "" && City != "" {

		r, err := Masterdb.Exec("INSERT INTO GEO_LOCATION_CITY_EN (LANGTYPE,COUNTRY,PROVINCE,CITY,PLACEID,STATE,UPDATETIME) value"+
			"(?, ?, ?, ?, ?, ?,  NOW())",
			"en",
			Locationinfo.PlaceInfo.Country,
			Province,
			City, Locationinfo.PlaceId, 0)
		if err != nil {
			infoLog.Printf("HandleForNewPlaceAdded insert GEO_LOCATION_CITY_EN err=%s,placeid=%d,pro=%s,city=%s", err.Error(), Locationinfo.PlaceId, Province, City)
		} else {

			affectRow, _ := r.RowsAffected()
			infoLog.Printf("HandleForNewPlaceAdded insert GEO_LOCATION_CITY_EN affectRow=%d,,placeid=%d,pro=%s,city=%s",
				affectRow,
				Locationinfo.PlaceId,
				Province,
				City)
		}

	}

	return
}

//将多语言城市更新到数据库
func PlaceInfoToMultiDbAndCache(PlaceId uint32, Latitude, Longitute, Lang string, StateCode int, Place *org.TagPlaceInformation) {

	_, err := Masterdb.Exec("insert into HT_MULTILANG_PLACE (PLACEID,LANGTYPE,STATE,COUNTRY,ADMINISTRATIVE1,ADMINISTRATIVE2,ADMINISTRATIVE3,"+
		"LOCALITY,SUBLOCALITY,NEIGHBORHOOD,LATITUDE,LONGITUDE,UPDATETIME) value"+
		"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP())",
		PlaceId,
		Lang,
		StateCode,
		Place.Country,
		Place.Admin1,
		Place.Admin2,
		Place.Admin3,
		Place.Locality,
		Place.SubLocality,
		Place.Neighborhood,
		Latitude,
		Longitute)

	if err != nil {
		infoLog.Println("PlaceInfoToMultiDbAndCache insert faield err=%s,placeid=%d,lang=%s,place=%#v", err.Error(), PlaceId, Lang, Place)
	}

	// 查询db 得到MultiPlaceInfo 后重新将插入redis
	field := org.GetMultiPlaceInfoKey(int64(PlaceId), Lang)
	value := Place.GetCacheString()
	err = lbsCacheRedis.Hset(org.MULTIPLACEINFOHMAP, field, value)
	if err != nil {
		infoLog.Printf("PlaceInfoToMultiDbAndCache HSET placeid#lang field=%s value=%v faield err=%v", field, value, err)
	} else {
		infoLog.Printf("PlaceInfoToMultiDbAndCache HSET palceid#lang field=%s value=%v succ", field, value)
	}
}

//下面的两个函数消费redis 地理位置更新队列
func NewPlaceQueueWorker(message *string) (code uint32, err error) {

	return 0, nil
}

//更新LOCATION2表，同时查询 CITYPLACECID更新到  SPHINX和 REDIS
func UserPlaceUpdateGeoWorker(message *string) (code uint32, err error) {

	return 0, nil
}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := apicommon.InitLogAndOption(&options, &infoLog)

	// init redis pool
	GOOGLE_API_KEY = cfg.Section("SERVICE_CONFIG").Key("GOOGLE_API_KEY").MustString("127.0.0.1")

	// redisIp := cfg.Section("USER_CACHE_REDIS").Key("ip").MustString("127.0.0.1")
	// redisPort := cfg.Section("USER_CACHE_REDIS").Key("port").MustString("6379")
	// infoLog.Printf("redis ip=%v port=%v", redisIp, redisPort)
	// fmt.Printf("lbsQueueRedis ip=%v port=%v", redisIp, redisPort)
	// UserCacheRedis = common.NewRedisApi(redisIp + ":" + redisPort)

	redisIp := cfg.Section("LBS_CACHE_REDIS").Key("ip").MustString("127.0.0.1")
	redisPort := cfg.Section("LBS_CACHE_REDIS").Key("port").MustString("6379")
	fmt.Printf("lbsCacheRedis ip=%v port=%v", redisIp, redisPort)
	lbsCacheRedis = common.NewRedisApi(redisIp + ":" + redisPort)

	Masterdb, err = apicommon.InitMySqlFromSection(cfg, infoLog, "MASTER_MYSQL", 2, 1)
	apicommon.CheckError(err)
	defer Masterdb.Close()

	sphinx_host := cfg.Section("SERVICE_CONFIG").Key("sphinx_host").MustString("127.0.0.1")
	sphinx_port := cfg.Section("SERVICE_CONFIG").Key("sphinx_port").MustInt(9312)
	sphinx_location_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_location_index").MustString("location")
	GeoDbClient = org.NewGeoDbAPI(sphinx_host, sphinx_port, sphinx_location_index, infoLog, Masterdb, GOOGLE_API_KEY)

	code, res, err := GeoDbClient.GetCoordinateFromGoogleApi(22222, "22.489461", "113.92035", "en", "test")
	infoLog.Println("GetCoordinateFromGoogleApi res =", res, ",code=", code)

	nsq_host := cfg.Section("USER_UPDATEINFO_NSQ").Key("host").MustString("10.144.89.163:4150")
	nsqUserUpdateInfoTopic = cfg.Section("USER_UPDATEINFO_NSQ").Key("topic").MustString("profile_update")

	nsqconfig := nsq.NewConfig()
	nsqUserUpdateInfoPublisher, err = nsq.NewProducer(nsq_host, nsqconfig)
	apicommon.CheckError(err)
	defer nsqUserUpdateInfoPublisher.Stop()

	//从redis 里面更新数据
	//lbsQueueRedis.StartLoop("list_multilang_place_data", 10, 3, NewPlaceQueueWorker)
	//lbsQueueRedis.StartLoop("list_multilang_user_data", 10, 3, UserPlaceUpdateGeoWorker)

	attr := "lbs_headquarter/total_count"
	nsqWorker := apicommon.NewNsqLoopHandlerFromIni(cfg, "NSQ_CONSUMER", attr, infoLog)
	err = nsqWorker.Start(MessageHandle)
	apicommon.CheckError(err)

	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
