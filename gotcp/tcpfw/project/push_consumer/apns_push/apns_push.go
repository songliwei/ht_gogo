package main

import (
	"crypto/tls"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	simplejson "github.com/bitly/go-simplejson"
	"github.com/gansidui/gotcp/libcomm"
	"github.com/gansidui/gotcp/tcpfw/common"
	"github.com/jessevdk/go-flags"
	"github.com/nsqio/go-nsq"
	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/certificate"
	"gopkg.in/ini.v1"
)

var wg *sync.WaitGroup
var clientManager *apns2.ClientManager
var myCert tls.Certificate
var mcApi *common.MemcacheApi
var infoLog *log.Logger

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

const (
	//CerPassWd = "yike@helloTalk"
	CerPassWd = "123456"
	TopicId   = "com.helloTalk.helloTalk"
)

const (
	PUSH_PARAM_CALLINCOMING  = "voip_call_incoming"
	PUSH_PARAM_CALLIMISS     = "voip_call_miss"
	PUSH_PARAM_CALLICANCEL   = "voip_call_cancel"
	PUSH_PARAM_LX_ACCEPT     = "language_exchange_accept"
	PUSH_PARAM_LX_DECLINED   = "language_exchange_declined"
	PUSH_PARAM_LX_TERMINATED = "language_exchange_terminated"
	PUSH_PARAM_LX_CANCLED    = "language_exchange_cancled"
)

const (
	APNS_PUSH_LANGUAGE_EXCHANGE = "Language Exchange Request"
	APNS_PUSH_LX_ACCEPT         = "Accepted Language Exchange"
	APNS_PUSH_LX_DECLIND        = "Declined Language Exchange"
	APNS_PUSH_LX_TERMINATED     = "Terminated Language Exchange"
	APNS_PUSH_LX_CANCLED        = "Cancled Language Exchange"

	APNS_PUSH_LANGUAGE_EXCHANGE_V2 = "Language Exchange Request push"
	APNS_PUSH_LX_ACCEPT_V2         = "%@: Exchange request agreed push"
	APNS_PUSH_LX_DECLIND_V2        = "Request Declined push"
	APNS_PUSH_LX_TERMINATED_V2     = "Terminate Exchange push"
	APNS_PUSH_LX_CANCLED_V2        = "Exchange Request Canceled push"

	APNS_PUSH_CALLINCOMING = "Incoming Call"
	APNS_PUSH_CALLMISS     = "Call Canceled"
	APNS_PUSH_CALLCANCEL   = "Call Missed"
)

const (
	PT_TEXT              = 0
	PT_VOICE             = 1
	PT_PHOTO             = 2
	PT_INDRODUCE         = 3
	PT_LOCATION          = 4
	PT_FRIEND_INVITE     = 5
	PT_LANGUAGE_EXCHANGE = 6
	PT_CORRECT_SENTENCE  = 7
	PT_STICKERS          = 8
	PT_DOODLE            = 9
	PT_GIFT              = 10
	PT_VOIP              = 11
	PT_INVITE_ACCEPT     = 12
	PT_VIDEO             = 13

	PT_GVOIP               = 15
	PT_LINK                = 16
	PT_CARD                = 17
	PT_FOLLOW              = 18
	PT_REPLY_YOUR_COMMENT  = 19
	PT_COMMENTED_YOUR_POST = 20
	PT_CORRECTED_YOUR_POST = 21
	PT_MOMENT_LIKE         = 22
	PT_POST_MNT            = 23
	PT_WHITE_BOARD         = 24
	PT_NEW_MSG_NOTIFY      = 25
	PT_GROUP_LESSON        = 26
	PT_START_CHANGE        = 27
)

const (
	NOT_BEEN_AT = 0 //default not at
	BEEN_AT     = 1 //
)

const (
	CTP2P = 0
	CTMUC = 1
)

var options Options
var parser = flags.NewParser(&options, flags.Default)

func MessageHandle(message *nsq.Message) error {
	// 统计apns总量
	attr := "apns/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	//log.Printf("MessageHandle Got a message: %s", message.Body)
	wg.Add(1)
	go func() {
		PostData(message)
		wg.Done()
	}()

	return nil
}

func PostData(message *nsq.Message) error {
	// log.Printf("PostData enter msgId=%v timestamp=%v body=%s", message.ID, message.Timestamp, message.Body)
	// log.Printf("PostData enter msgId=%v timestamp=%v", message.ID, message.Timestamp)
	// 然后进行下一步无处理
	rootObj, err := simplejson.NewJson(message.Body)
	if err != nil {
		log.Printf("PostData simplejson new packet error", err)
		return err
	}
	// log.Printf("PostData rootObj=%#v", rootObj)
	fromId := rootObj.Get("from_id").MustInt(0)
	token := rootObj.Get("token").MustString("")
	pushType := rootObj.Get("push_type").MustInt(PT_TEXT)
	preview := rootObj.Get("preview").MustInt(0)
	pushParam := rootObj.Get("push_param").MustString("")
	pushParam = strings.Trim(pushParam, "\u0000")
	nickName := rootObj.Get("nick_name").MustString("")
	nickName = strings.Trim(nickName, "\u0000")
	byAt := rootObj.Get("by_at").MustInt(NOT_BEEN_AT)
	toId := rootObj.Get("to_id").MustUint64(0)

	var textVal, voiceVal, pictureVal, videoVal uint8
	needRichNotify := false
	if pushType == PT_VOICE || pushType == PT_PHOTO {
		onlineStat, err := mcApi.GetUserOnlineStat(uint32(toId))
		if err == nil {
			if onlineStat.ClientType == common.CClientTyepIOS && onlineStat.Version > common.CVerSion245 {
				needRichNotify = true
				log.Printf("PostData GetUserOnlineStat uid=%v version=%v need rich notification", toId, onlineStat.Version)
			}
		} else {
			log.Printf("PostData Get get user online failed uid=%v err=%v", toId, err)
		}
	}

	alertObj := simplejson.New()
	var alertString string
	if preview == 0 &&
		pushType != PT_REPLY_YOUR_COMMENT &&
		pushType != PT_COMMENTED_YOUR_POST &&
		pushType != PT_CORRECTED_YOUR_POST &&
		pushType != PT_POST_MNT {
		alertObj.Set("loc-key", "Message Preview Example No")
	} else {
		switch pushType {
		case PT_TEXT:
			textVal = 1
			if byAt == BEEN_AT {
				alertObj.Set("loc-key", "Someone @ me")
				alertObj.Set("loc-args", []interface{}{})
			} else {
				alertString = nickName + ": " + pushParam
			}
		case PT_PHOTO:
			pictureVal = 1
			alertObj.Set("loc-key", "%@: Photo")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_VOICE:
			voiceVal = 1
			alertObj.Set("loc-key", "%@: Voice Message")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_INDRODUCE:
			alertObj.Set("loc-key", "%1$@: %2$@'s cantact card push")
			alertObj.Set("loc-args", []interface{}{nickName, pushParam})
		case PT_LOCATION:
			alertObj.Set("loc-key", "%@: Location")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_FRIEND_INVITE:
			alertObj.Set("loc-key", "%@ wants to add you")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_INVITE_ACCEPT:
			alertObj.Set("loc-key", "%@ accepts your request")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_LANGUAGE_EXCHANGE:
			if pushParam == APNS_PUSH_LANGUAGE_EXCHANGE ||
				pushParam == APNS_PUSH_LX_ACCEPT ||
				pushParam == APNS_PUSH_LX_DECLIND ||
				pushParam == APNS_PUSH_LX_TERMINATED ||
				pushParam == APNS_PUSH_LX_CANCLED {
				alertString = nickName + ": " + pushParam
			} else {
				alertObj.Set("loc-key", pushParam)
				alertObj.Set("loc-args", []interface{}{nickName})
			}
		case PT_CORRECT_SENTENCE:
			alertObj.Set("loc-key", "%@: Correct Sentences")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_DOODLE:
			alertObj.Set("loc-key", "%@: Doodle")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_STICKERS:
			alertObj.Set("loc-key", "%@: Stickers")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_GIFT:
			alertObj.Set("loc-key", "Gift from %@")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_VOIP:
			alertObj.Set("loc-key", "%@: "+pushParam)
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_VIDEO:
			videoVal = 1
			alertObj.Set("loc-key", "%@: Video")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_GVOIP:
			alertObj.Set("loc-key", "Start group call push")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_LINK:
			alertObj.Set("loc-key", "Message Preview Example No")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_CARD:
			alertObj.Set("loc-key", "%@: Card")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_FOLLOW:
			alertObj.Set("loc-key", "%@ has followed you")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_REPLY_YOUR_COMMENT:
			alertObj.Set("loc-key", "%@ replied to your comment")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_COMMENTED_YOUR_POST:
			alertObj.Set("loc-key", "%@ commented your post")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_CORRECTED_YOUR_POST:
			alertObj.Set("loc-key", "%@ corrected your post")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_POST_MNT:
			alertObj.Set("loc-key", "%@: published a new moment")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_WHITE_BOARD:
			alertObj.Set("loc-key", "%@ is having a class")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_GROUP_LESSON:
			alertObj.Set("loc-key", "%@ is having a class")
			alertObj.Set("loc-args", []interface{}{nickName})
		case PT_START_CHANGE:
			alertObj.Set("loc-key", "Course payment")
			// alertObj.Set("loc-args", []interface{}{nickName})
		default:
			log.Printf("UnHandle pushType=%v", pushType)
		}
	}
	apsObj := simplejson.New()
	// add alert obj
	if len(alertString) != 0 {
		apsObj.Set("alert", alertString)
	} else {
		apsObj.Set("alert", alertObj)
	}
	// add badge
	badgeNum := rootObj.Get("badge_num").MustInt(0)
	if badgeNum <= 0 {
		badgeNum = 1
	}
	apsObj.Set("badge", badgeNum)
	//add sound
	apsObj.Set("sound", rootObj.Get("sound").MustString("default"))

	// 2017-10-13 添加category字段
	if pushType == PT_TEXT ||
		pushType == PT_INDRODUCE ||
		pushType == PT_LOCATION ||
		pushType == PT_CORRECT_SENTENCE ||
		pushType == PT_STICKERS ||
		pushType == PT_DOODLE ||
		pushType == PT_GIFT ||
		pushType == PT_VOIP ||
		pushType == PT_VIDEO ||
		pushType == PT_GVOIP ||
		pushType == PT_LINK ||
		pushType == PT_CARD {
		apsObj.Set("category", "category_message")
	} else if pushType == PT_POST_MNT {
		apsObj.Set("category", "category_moment")
		apsObj.Set("mutable-content", 1)
	} else if pushType == PT_VOICE ||
		pushType == PT_PHOTO {
		apsObj.Set("category", "category_message_custom")
		apsObj.Set("mutable-content", 1)
	}

	pushMsgObj := simplejson.New()
	// add aps param
	pushMsgObj.Set("aps", apsObj)
	pushMsgObj.Set("push_type", pushType)
	// userid: for the Client UI Jump
	if pushType != PT_FRIEND_INVITE && pushType != PT_VOIP {
		chatType := rootObj.Get("chat_type").MustInt(CTP2P)
		if chatType == CTMUC {
			pushMsgObj.Set("roomid", fromId)
		} else {
			pushMsgObj.Set("userid", fromId)
		}
	}

	// moment add
	if pushType == PT_REPLY_YOUR_COMMENT ||
		pushType == PT_COMMENTED_YOUR_POST ||
		pushType == PT_CORRECTED_YOUR_POST {
		pushMsgObj.Set("notifyType", "MntCmtNotify")
	} else if pushType == PT_FOLLOW {
		pushMsgObj.Set("notifyType", "FollowingtNotify")
	} else if pushType == PT_POST_MNT {
		pushMsgObj.Set("notifyType", "MntNotify")
		// 反序列化
		log.Printf("PostData MntNotify toId=%v param=%s", rootObj.Get("from_id").MustInt(0), pushParam)
		paramObj, err := simplejson.NewJson([]byte(pushParam))
		if err == nil {
			pushMsgObj.Set("mid", paramObj.Get("mid").MustString(""))
			text := paramObj.Get("text").MustString("")
			voiceUrl := paramObj.Get("voice").Get("url").MustString("")
			voiceDuration := paramObj.Get("voice").Get("duration").MustInt(0)
			imageUrl := paramObj.Get("image").MustString("")
			if text != "" {
				textVal = 1
				pushMsgObj.Set("text", text)
			}
			if voiceUrl != "" {
				voiceVal = 1
				voiceObj := simplejson.New()
				voiceObj.Set("url", voiceUrl)
				voiceObj.Set("duration", voiceDuration)
				pushMsgObj.Set("audio", voiceObj)
			} else if imageUrl != "" {
				pictureVal = 1
				pushMsgObj.Set("image", imageUrl)
			}
		} else {
			attr := "apns/new_json_failed"
			libcomm.AttrAdd(attr, 1)
			log.Printf("PostData MntNotify param simpleJson.New failed pushParam=%s err=%s", pushParam, err)
		}
	} else if needRichNotify && pushType == PT_PHOTO {
		paramObj, err := simplejson.NewJson([]byte(pushParam))
		if err == nil {
			imageUrl := paramObj.Get("url").MustString("")
			log.Printf("PostData pushType=PT_PHOTO url=%s", imageUrl)
			pushMsgObj.Set("image", imageUrl)
		} else {
			attr := "apns/new_json_failed"
			libcomm.AttrAdd(attr, 1)
			log.Printf("PostData MntNotify param simpleJson.New failed pushParam=%s err=%s", pushParam, err)
		}
	} else if needRichNotify && pushType == PT_VOICE {
		paramObj, err := simplejson.NewJson([]byte(pushParam))
		log.Printf("PostData pushType=PT_VOICE pushParam=%s", pushParam)
		if err == nil {
			audioObj := simplejson.New()
			audioObj.Set("duration", paramObj.Get("duration").MustInt(0))
			audioObj.Set("url", paramObj.Get("url").MustString(""))
			pushMsgObj.Set("audio", audioObj)
		} else {
			attr := "apns/new_json_failed"
			libcomm.AttrAdd(attr, 1)
			log.Printf("PostData MntNotify param simpleJson.New failed pushParam=%s err=%s", pushParam, err)
		}
	}
	//add action id
	rootObj.Set("actionid", rootObj.Get("action_id").MustInt(0))
	pushSlice, err := pushMsgObj.MarshalJSON()
	if err != nil {
		log.Printf("PostData MarshalJSON failed")
		return nil
	}

	log.Printf("PostData push message=%s", pushSlice)
	notification := &apns2.Notification{}
	notification.DeviceToken = token
	notification.Topic = TopicId
	notification.Payload = pushSlice // See Payload section below

	client := clientManager.Get(myCert).Production()
	defer clientManager.Add(client)
	res, err := client.Push(notification)
	log.Printf("ProcData client.Push ret = %#v", res)
	if err == nil && res.StatusCode == apns2.StatusSent {
		// 统计apns成功总量
		attr := "apns/total_push_succ_count"
		libcomm.AttrAdd(attr, 1)
	} else {
		// 统计apns失败总量
		attr := "apns/total_push_failed_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("postData client.Push failed uid=%v token=%s res.StatusCode=%v reason=%s err=%v",
			rootObj.Get("to_id").MustInt(0),
			rootObj.Get("token").MustString(""),
			res.StatusCode,
			res.Reason,
			err)
	}
	// 添加一条日志用于统计各种推送类型的量
	WriteStaticLog(uint32(fromId), uint32(toId), pushType, 0, textVal, voiceVal, pictureVal, videoVal)
	return nil
}

func WriteStaticLog(fromId, toId uint32, pushType int, terminalType, text, voice, picture, video uint8) {
	contentObj := simplejson.New()
	contentObj.Set("text", text)
	contentObj.Set("voice", voice)
	contentObj.Set("picture", picture)
	contentObj.Set("video", video)
	logObj := simplejson.New()
	logObj.Set("fromid", fromId)
	logObj.Set("toid", toId)
	logObj.Set("type", pushType)
	logObj.Set("terminal", terminalType)
	logObj.Set("content", contentObj)
	logObj.Set("createtime", time.Now().Unix())
	logSlic, err := logObj.MarshalJSON()
	if err != nil {
		log.Printf("WriteStaticLog fromId=%v toId=%v pushType=%v terminalType=%v type=%v voice=%v picture=%v video=%v err=%s",
			fromId,
			toId,
			pushType,
			terminalType,
			pushType,
			voice,
			picture,
			video,
			err)
		return
	}
	infoLog.Printf("%s", logSlic)
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}
	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取 Memcache Ip and port
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustInt(11211)
	log.Printf("memcache ip=%v port=%v", mcIp, mcPort)
	mcApi = new(common.MemcacheApi)
	mcApi.Init(mcIp + ":" + strconv.Itoa(mcPort))

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")
	cerPath := cfg.Section("CER").Key("path").MustString("./cer.p12")
	myCert, err = certificate.FromP12File(cerPath, CerPassWd)
	if err != nil {
		log.Fatal("Get Cert error:", err)
	}
	clientManager = apns2.NewClientManager()

	// init ansp consumter
	wg = &sync.WaitGroup{}
	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
