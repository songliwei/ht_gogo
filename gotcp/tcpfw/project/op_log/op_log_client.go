package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_purchase"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	Cmd int `short:"t" long:"cmd" description:"Command type" optional:"no"`

	Type int `short:"p" long:"type" description:"pay type" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	v2Protocol := &common.HeadV2Protocol{}
	var head *common.HeadV2
	head = &common.HeadV2{
		Version:  4,
		Cmd:      1,
		Seq:      1,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      0,
	}

	var payLoad []byte
	reqBody := new(ht_purchase.PurchaseReqBody)
	switch options.Cmd {
	// 获取用户基本信息
	case 1:
		head.Cmd = uint32(ht_purchase.CMD_TYPE_CMD_COMMIT_PURCHASE_ORDER_REQ)
		payType := ht_purchase.PAY_TYPE_PAY_TYPE_APP_STORE
		var subReqBody *ht_purchase.CommitPurchaseOrderReqBody
		switch options.Type {
		case 1:
			payType = ht_purchase.PAY_TYPE_PAY_TYPE_APP_STORE
			subReqBody = &ht_purchase.CommitPurchaseOrderReqBody{
				PayType:  payType.Enum(),
				UserId:   proto.Uint32(6882004),
				ToId:     proto.Uint32(6882004),
				ItemCode: proto.Uint32(2),
				AppleOrder: &ht_purchase.AppStoreOrder{
					ProductId:       []byte("com.hellotalk.monthauto"),
					StoreArea:       []byte("US"),
					TransactionId:   []byte("330000227220152"),
					PurchaseDateGmt: []byte("2017-05-15 15:16:36 Etc/GMT"),
					PurchaseDate:    []byte("2017-05-15 08:16:36 America/Los_Angeles"),
					ReceiptData:     []byte("ewoJInNpZ25hdHVyZSIgPSAiQTBiR0c4MldTckEwV0QyOWlPdFMwMnE5MjFZaU1yU3A2dDFWZDBEeTl4clpZVmtVRXNHVGJvazRhbDVIZ0UzSEhPQkxqRElydkltaW9YdS9JdmtjVHVoOTFNdzQzSjZuOHJyYlVwdC9HUk9vR3k0QzF3NDVTQ1lqV3loRFZoenU5MTF2V0RnOUhsbkI2MVVuV3UvaXAzNm8xa0FVTWRHS2dicjkveG1KTER6NlJFK0VONW0wZzdJYjU5Ri9aN0Z4V1kzZWl5cnRIWTJ4d3hxVFpRQ0lxQUt1M3V1L1hFa29Wbm96bTAxSjN0ZS8zaldNSXZROFdzbnFwMi9OWUMweXhaUUpNODNDL1VwRmhpQ0tpYXlYdytLMkcrSHp0ZVhSR1BubFZDK2dvelVOVTN1ZGliRVFGSHllZnJpZm93ZWluM2lITDFJOU4zNkluTjNuUi9TNU9hY0FBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ERTNMVEExTFRFMUlEQTRPakUyT2pNNElFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5CMWNtTm9ZWE5sTFdSaGRHVXRiWE1pSUQwZ0lqRTBPVFE0TmpFek9UWXdNREFpT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdWeUlpQTlJQ0l6WldJME1EVTBOak5pTVRaaVlqTXlNRGM0WWpFeVptTXlOelZpTjJaallUVm1aVFF4Tm1Zeklqc0tDU0p2Y21sbmFXNWhiQzEwY21GdWMyRmpkR2x2YmkxcFpDSWdQU0FpTXpNd01EQXdNakkzTWpJd01UVXlJanNLQ1NKbGVIQnBjbVZ6TFdSaGRHVWlJRDBnSWpFME9UYzFNemszT1RZd01EQWlPd29KSW1Gd2NDMXBkR1Z0TFdsa0lpQTlJQ0kxTlRjeE16QTFOVGdpT3dvSkluUnlZVzV6WVdOMGFXOXVMV2xrSWlBOUlDSXpNekF3TURBeU1qY3lNakF4TlRJaU93b0pJbkYxWVc1MGFYUjVJaUE5SUNJeElqc0tDU0ozWldJdGIzSmtaWEl0YkdsdVpTMXBkR1Z0TFdsa0lpQTlJQ0l6TXpBd01EQXdOREV4T0RjeE1EQWlPd29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdSaGRHVXRiWE1pSUQwZ0lqRTBPVFE0TmpFek9UZ3dNREFpT3dvSkluVnVhWEYxWlMxMlpXNWtiM0l0YVdSbGJuUnBabWxsY2lJZ1BTQWlNVFF4UXpRMlF6UXRORVZGTlMwME9ETTJMVGcxTmtZdFF6TTNSakEzTmpjeU1VUTNJanNLQ1NKbGVIQnBjbVZ6TFdSaGRHVXRabTl5YldGMGRHVmtMWEJ6ZENJZ1BTQWlNakF4Tnkwd05pMHhOU0F3T0RveE5qb3pOaUJCYldWeWFXTmhMMHh2YzE5QmJtZGxiR1Z6SWpzS0NTSnBkR1Z0TFdsa0lpQTlJQ0l4TVRReE56YzRNams1SWpzS0NTSmxlSEJwY21WekxXUmhkR1V0Wm05eWJXRjBkR1ZrSWlBOUlDSXlNREUzTFRBMkxURTFJREUxT2pFMk9qTTJJRVYwWXk5SFRWUWlPd29KSW5CeWIyUjFZM1F0YVdRaUlEMGdJbU52YlM1b1pXeHNiM1JoYkdzdWJXOXVkR2hoZFhSdklqc0tDU0p3ZFhKamFHRnpaUzFrWVhSbElpQTlJQ0l5TURFM0xUQTFMVEUxSURFMU9qRTJPak0ySUVWMFl5OUhUVlFpT3dvSkltOXlhV2RwYm1Gc0xYQjFjbU5vWVhObExXUmhkR1VpSUQwZ0lqSXdNVGN0TURVdE1UVWdNVFU2TVRZNk16Z2dSWFJqTDBkTlZDSTdDZ2tpY0hWeVkyaGhjMlV0WkdGMFpTMXdjM1FpSUQwZ0lqSXdNVGN0TURVdE1UVWdNRGc2TVRZNk16WWdRVzFsY21sallTOU1iM05mUVc1blpXeGxjeUk3Q2draVltbGtJaUE5SUNKamIyMHVhR1ZzYkc5VVlXeHJMbWhsYkd4dlZHRnNheUk3Q2draVluWnljeUlnUFNBaU5DSTdDbjA9IjsKCSJwb2QiID0gIjMzIjsKCSJzaWduaW5nLXN0YXR1cyIgPSAiMCI7Cn0"),
					Currency:        []byte("GBP"),
					PayMoney:        []byte("2.99"),
					Jailbroken:      proto.Uint32(0),
				},
			}
		case 2:
			payType = ht_purchase.PAY_TYPE_PAY_TYPE_GOOGLE_PLAY
			head.Uid = 8419703
			subReqBody = &ht_purchase.CommitPurchaseOrderReqBody{
				PayType:  payType.Enum(),
				UserId:   proto.Uint32(8419703),
				ToId:     proto.Uint32(8419703),
				ItemCode: proto.Uint32(22),
				GoogleOrder: &ht_purchase.GoogleOrder{
					OrderId:         []byte("GPA.3392-7758-0848-08727"),
					ProductId:       []byte("com.hellotalk.onemonthauto.b"),
					PurchaseTime:    proto.Uint32(1505567488),
					PurchaseState:   proto.Uint32(0),
					PurchaseCountry: []byte("KRW"),
					Currency:        []byte("KRW"),
					PayMoney:        []byte("₩6,200"),
					PurchaseToken:   []byte("poikenkjcimdbhchhdfgcecl.AO-J1OwGy37BgEgKhgZOcvpHqnIH9QEAM03mTvtbZQSgmjGnHlPv4t3U45SLy719-z9x8Vqz_bmzJxw0pEQXoMpvncU95jc3gggV4MMoK6rn_t8USRXFd5h-VE5P9L-Ah2b3h2l6FbSK"),
				},
			}
		case 3:
			payType = ht_purchase.PAY_TYPE_PAY_TYPE_ALI_PAY
		case 4:
			payType = ht_purchase.PAY_TYPE_PAY_TYPE_WECHAT_PAY
			subReqBody = &ht_purchase.CommitPurchaseOrderReqBody{
				PayType:  payType.Enum(),
				UserId:   proto.Uint32(3249314),
				ToId:     proto.Uint32(3249317),
				ItemCode: proto.Uint32(12),
				WechatOrder: &ht_purchase.WeCharOrder{
					PurchaseDate: []byte("2017-06-19 03:50:03"),
					TradeNo:      []byte("3249314_14978586025888"),
					Partner:      []byte("1233820001"),
					Seller:       []byte("1233820001"),
					Subject:      []byte("com.hellotalk.monthauto"),
					Success:      []byte("true"),
					TotalFee:     []byte("0.01"),
				},
			}
		}

		reqBody.CommitPurchaseOrderReqbody = subReqBody

	default:
		infoLog.Println("UnKnow input cmd =", options.Cmd)
	}

	payLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v cmd=%v seq=%v",
			head.Uid,
			head.Cmd,
			head.Seq)
		return
	}

	head.Len = uint32(common.PacketV2HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV2ToSlice failed")
		return
	}
	copy(buf[common.PacketV2HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV2MagicEnd

	infoLog.Printf("len=%v payLaod=%v\n", len(payLoad), payLoad)
	// write
	conn.Write(buf)
	// read
	p, err := v2Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket, ok := p.(*common.HeadV2Packet)
		if !ok { // 不是HeadV3Packet报文
			infoLog.Printf("packet can not change to HeadV2packet")
			return
		}
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("resp len=%v cmd=%v uid=%v\n", rspHead.Len, rspHead.Cmd, rspHead.Uid)
		rspBody := &ht_purchase.PurchaseRspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Println("proto Unmarshal failed")
			return
		}
		switch rspHead.Cmd {
		case 1:
			subRspBody := rspBody.GetCommitPurchaseOrderRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetCommitPurchaseOrderRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())

		default:
			infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
		}

	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
