package main

import (
	"database/sql"

	"github.com/HT_GOGO/gotcp/tcpfw/project/op_log/util"
	"github.com/golang/protobuf/proto"
	nsq "github.com/nsqio/go-nsq"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_oplog"

	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog       *log.Logger
	DbUtil        *util.DbUtil
	messageChan   chan *nsq.Message
	consumerCount int
	closeChan     chan struct{} // close chanel
)

const (
	ACCOUNT = "rrchen123"
	PASSWD  = "HTalk*#rrchen156"
)

func MessageHandle(message *nsq.Message) error {
	// 统计gcm处理总量
	attr := "gooplog/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	// log.Printf("MessageHandle Got a message: %v", message)
	select {
	case messageChan <- message:
		infoLog.Printf("ProcData message Id=%s timestamp=%v put into chan succ", message.ID, message.Timestamp)
	case <-closeChan: // 进程退出
		infoLog.Printf("MessageHandle gorutine exit")
		return nil
	}
	return nil
}

func DoOpLogTask(index int) {
	defer func() {
		recover()
	}()
	infoLog.Printf("DoOpLogTask index=%v", index)
	for {
		select {
		case opTask := <-messageChan:
			headV2Packet := common.NewHeadV2Packet(opTask.Body)
			// head 为一个new出来的对象指针
			head, err := headV2Packet.GetHead()
			if err != nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("DoOpLogTask Get head failed err=%s", err)
				continue
			}

			infoLog.Printf("DoOpLogTask:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
			_, err = headV2Packet.CheckPacketV2Valid()
			if err != nil {
				infoLog.Printf("DoOpLogTask Invalid packet err=%s", err)
				continue
			}
			switch head.Cmd {
			case uint32(ht_oplog.CMD_TYPE_CMD_COMMIT_GROUP_LESSON_OP_REQ):
				ProcCommitGroupLessonOpLog(head, headV2Packet.GetBody())
			default:
				infoLog.Printf("DoOpLogTask UnHandle Cmd=%v", head.Cmd)
			}
		case <-closeChan:
			return
		}
	}
}

// 1.proc store moment info
func ProcCommitGroupLessonOpLog(head *common.HeadV2, payLoad []byte) bool {
	if head == nil || len(payLoad) == 0 {
		infoLog.Printf("ProcCommitGroupLessonOpLog invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gooplog/handle_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_oplog.OpLogReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcCommitGroupLessonOpLog proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}
	subReqBody := reqBody.GetCommitGroupLessonOpReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcCommitGroupLessonOpLog GetCommitGroupLessonOpReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}
	reqUid := subReqBody.GetReqUid()
	inTime := subReqBody.GetInTime()
	outTime := subReqBody.GetOutTime()
	groupLessonObid := subReqBody.GetGroupLessonObid()
	createrUid := subReqBody.GetCreaterUid()
	isTeacher := subReqBody.GetIsTeacher()
	recordObid := subReqBody.GetRecordObid()

	infoLog.Printf("ProcCommitGroupLessonOpLog reqUid=%v inTime=%v outTime=%v groupLessonObid=%s createrUid=%v isTeacher=%v recordObid=%s",
		reqUid,
		inTime,
		outTime,
		groupLessonObid,
		createrUid,
		isTeacher,
		recordObid)

	if reqUid == 0 ||
		inTime == 0 ||
		outTime == 0 ||
		len(groupLessonObid) == 0 ||
		createrUid == 0 ||
		len(recordObid) == 0 {
		infoLog.Printf("ProcCommitGroupLessonOpLog reqUid=%v inTime=%v outTime=%v groupLessonObid=%s createrUid=%v isTeacher=%v recordObid=%s input param error",
			reqUid,
			inTime,
			outTime,
			groupLessonObid,
			createrUid,
			isTeacher,
			recordObid)
		return false
	}

	err = DbUtil.WriteGroupLessonOpLog(reqUid, createrUid, isTeacher, inTime, outTime, groupLessonObid, recordObid)
	if err != nil {
		attr := "gooplog/update_db_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcCommitGroupLessonOpLog DbUtil.WriteGroupLessonOpLog failed uid=%v err=%v", reqUid, err)
		return false

	}

	return true
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	checkError(err)
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	checkError(err)

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mysql
	mysqlHost := cfg.Section("LOGMYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("LOGMYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("LOGMYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("LOGMYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("LOGMYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("LOGMYSQL host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", ACCOUNT+":"+PASSWD+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	checkError(err)
	DbUtil = util.NewDbUtil(db, infoLog)

	// nsq 配置
	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")
	messageChanLen := cfg.Section("MESSAGE").Key("chanLen").MustInt(10000)
	consumerCount = cfg.Section("MESSAGE").Key("consumerCount").MustInt(10)
	messageChan = make(chan *nsq.Message, messageChanLen)
	closeChan = make(chan struct{})
	infoLog.Printf("message chan length=%v consumerCount=%v", messageChanLen, consumerCount)
	for i := 0; i < consumerCount; i += 1 {
		go DoOpLogTask(i)
	}

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	checkError(err)

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)
	close(closeChan)
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
