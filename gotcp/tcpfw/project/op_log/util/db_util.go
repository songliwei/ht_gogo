// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

// Error type
var (
	ErrNilDbObject  = errors.New("not set  object current is nil")
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrInternalErr  = errors.New("internal err")
	ErrInvalidRsp   = errors.New("err invalid respones")
)

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

func (this *DbUtil) WriteGroupLessonOpLog(reqUid, createrUid, isTeacher uint32,
	inTime, outTime uint64,
	groupLessonObid, recordObid string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	_, err = this.db.Exec("insert into LOG_CLASSROOM_RECORD set userid=?, in_time=?, out_time=?, lesson_groupid=?, lesson_createrid=?, if_teacher=?, lesson_recordid=?, updatetime=UTC_TIMESTAMP();",
		reqUid,
		inTime,
		outTime,
		groupLessonObid,
		createrUid,
		isTeacher,
		recordObid)
	if err != nil {
		this.infoLog.Printf("WriteGroupLessonOpLog insert faield reqUid=%v createrUid=%v isTeacher=%v inTime=%v outTime=%v groupLessonObid=%s recordObid=%s err=%v",
			reqUid,
			createrUid,
			isTeacher,
			inTime,
			outTime,
			groupLessonObid,
			recordObid,
			err)
		return err
	} else {
		return nil
	}
}
