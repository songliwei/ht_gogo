package main

import (
	"errors"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/gansidui/gotcp/libcomm"
	"github.com/jessevdk/go-flags"
	"github.com/shirou/gopsutil/process"
	"gopkg.in/ini.v1"
)

const (
	kCheckInterval = 60 //  second
	ExecCmd        = "pidof"
	MemTarget      = "RSS"
	CpuPercent     = "CPUP"
)

var (
	infoLog          *log.Logger
	sleepSecond      int
	cpuCheckInternal int
)

var (
	ErrInputParam = errors.New("err param error")
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// sleep second
	sleepSecond = cfg.Section("SLEEP").Key("sleep_second").MustInt(60)
	cpuCheckInternal = cfg.Section("CPU").Key("check_second").MustInt(1)

	// process config
	processCount := cfg.Section("PROCESS").Key("process_cnt").MustInt(1)
	infoLog.Printf("process_cnt=%v", processCount)
	procNameToPidMap := map[string]int32{}
	for i := 0; i < processCount; i++ {
		processKey := "process_" + strconv.Itoa(i)
		processValue := cfg.Section("PROCESS").Key(processKey).MustString("")
		if processValue != "" {
			procNameToPidMap[processValue] = 0
		}
	}

	for {
		for i, v := range procNameToPidMap {
			infoLog.Printf("processName=%v pid=%v", i, v)
			cmd := exec.Command(ExecCmd, i)
			buf, err := cmd.Output()
			if err != nil {
				infoLog.Printf("exec cmd=%s err=%s continue", ExecCmd, err)
				continue
			}
			pidStr := string(buf)
			pidStr = strings.Trim(pidStr, "\n")
			pid, err := strconv.Atoi(pidStr)
			if err != nil {
				infoLog.Printf("processName=%v processPid=%s err=%v continue", i, pidStr, err)
				continue
			}
			if pid == 0 {
				infoLog.Printf("processName=%v processPid=%s pid=%v continue", i, pidStr, pid)
				continue
			}

			infoLog.Printf("exec cmd=%s process=%s pid=%v", ExecCmd, i, pid)
			procNameToPidMap[i] = int32(pid)
			processInfo, err := process.NewProcess(int32(pid))
			if err != nil {
				infoLog.Printf("process.NewProcess pid=%v err=%s", pid, err)
				continue
			}

			// Memory
			memStat, err := processInfo.MemoryInfo()
			if err != nil {
				infoLog.Printf("processInfo.MemoryInfo pid=%v err=%s", pid, err)
				continue
			}
			infoLog.Printf("pid=%v RSS=%v VMS=%v Swap=%v", pid, memStat.RSS, memStat.VMS, memStat.Swap)
			attr := i + "/" + MemTarget
			statValue := int32(memStat.RSS / 1048576)
			libcomm.AttrSet(attr, statValue) // MByte

			// Cpu
			cpuPercent, err := processInfo.Percent(time.Duration(cpuCheckInternal) * time.Second)
			if err != nil {
				infoLog.Printf("processInfo.Percent processName=%v pid=%v err=%s continue", i, pid, err)
				continue
			}
			infoLog.Printf("processInfo.Percent processName=%v pid=%v cpuPercent=%v", i, pid, cpuPercent)
			attr = i + "/" + CpuPercent
			cpuValue := int32(cpuPercent)
			libcomm.AttrSet(attr, cpuValue) // cpu percent
		}

		// sleep 60 second
		time.Sleep(time.Duration(sleepSecond) * time.Second)
	}

}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
