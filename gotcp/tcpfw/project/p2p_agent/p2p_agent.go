package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/libcrypto"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_p2p"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

var (
	// ErrNilDbObject    = errors.New("err nil db obj")
	ErrNilParam       = errors.New("err nil param obj")
	ErrInputParamErr  = errors.New("err input param err")
	ErrOnlineIpErr    = errors.New("err online state ip not exist")
	ErrSendToMucFaild = errors.New("err send packet to muc failed")
)

const (
	CT_P2P = 0
	CT_MUC = 1
)

const (
	CNotBeenAt = 0
	CBeenAt    = 1
)

const (
	CMD_MOMENT_NOTIFY        = 0x6101
	CMD_FOLLOWER_NOTIFY      = 0x320A
	CMD_S2S_MESSAGE_PUSH     = 0x8027
	CMD_S2S_MESSAGE_PUSH_ACK = 0x8028
)

const (
	NICKNAME_LEN    = 128
	CONTENT_LEN     = 1024
	NORMALDEC_LEN   = 128
	SERVER_COMM_KEY = "lp$5F@nfN0Oh8I*5"
)

type Callback struct{}

var (
	infoLog    *log.Logger
	mcApi      *common.MemcacheApi
	imServer   map[string]*common.ImServerApiV2
	pcImServer map[string]*common.ImServerApiV2
	offlineApi *common.OfflineApiV2
	sendSeq    uint16
)

// Convert uint to net.IP http://www.outofmemory.cn
func inet_ntoa(ipnr int64) net.IP {
	var bytes [4]byte
	bytes[0] = byte((ipnr >> 24) & 0xFF)
	bytes[1] = byte((ipnr >> 16) & 0xFF)
	bytes[2] = byte((ipnr >> 8) & 0xFF)
	bytes[3] = byte(ipnr & 0xFF)

	return net.IPv4(bytes[3], bytes[2], bytes[1], bytes[0])
}

// Convert net.IP to int64 ,  http://www.outofmemory.cn
func inet_aton(ipnr net.IP) int64 {
	bits := strings.Split(ipnr.String(), ".")

	b0, _ := strconv.Atoi(bits[0])
	b1, _ := strconv.Atoi(bits[1])
	b2, _ := strconv.Atoi(bits[2])
	b3, _ := strconv.Atoi(bits[3])

	var sum int64

	sum += int64(b0) << 24
	sum += int64(b1) << 16
	sum += int64(b2) << 8
	sum += int64(b3)

	return sum
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV3packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed err=%s", err)
		return false
	}

	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	reqBody := &ht_p2p.P2PMsgBody{}
	err = proto.Unmarshal(packet.GetBody(), reqBody)
	if err != nil {
		infoLog.Printf("OnMessage proto Unmarshal failed")
		return false
	}
	// 统计总的请求量
	attr := "gop2p/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	go ProcData(c, head, reqBody)
	return true
}

func SendResp(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16) bool {
	if reqHead.Cmd == uint16(CMD_MOMENT_NOTIFY) {
		infoLog.Printf("SendResp cmd=%v no need respon", reqHead.Cmd)
	} else {
		head := new(common.HeadV3)
		if reqHead != nil {
			*head = *reqHead
		}

		head.Len = uint32(common.EmptyPacktV3Len)
		head.Ret = ret
		buf := make([]byte, head.Len)
		buf[0] = common.HTV3MagicBegin
		err := common.SerialHeadV3ToSlice(head, buf[1:])
		if err != nil {
			infoLog.Println("SerialHeadV3ToSlice failed")
			return false
		}

		buf[head.Len-1] = common.HTV3MagicEnd
		resp := common.NewHeadV3Packet(buf)
		c.AsyncWritePacket(resp, time.Second)
	}
	return true
}

func ProcData(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_p2p.P2PMsgBody) bool {
	if head == nil || reqBody == nil {
		infoLog.Printf("ProcData invalid param head==nil or reqBody==nil")
		return false
	}
	infoLog.Printf("from=%v to=%v seq=%v cmd=0x%4x toType=%v", head.From, head.To, head.Seq, head.Cmd, reqBody.GetToType())
	switch reqBody.GetToType() {
	case ht_p2p.TO_CLIENT_TYPE_SEND_TO_MOBILE:
		ProcToMobile(c, head, reqBody)
	case ht_p2p.TO_CLIENT_TYPE_SEND_TO_PC:
		ProcToPc(c, head, reqBody)
	case ht_p2p.TO_CLIENT_TYPE_SEND_TO_ALL:
		ProcToMobile(c, head, reqBody)
		ProcToPcWithOutAck(head, reqBody)
	}
	return true
}

func ProcToMobile(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_p2p.P2PMsgBody) bool {
	result := uint16(ht_p2p.RET_CODE_RET_SUCCESS)
	defer func() {
		SendResp(c, head, result)
	}()

	stat, err := mcApi.GetUserOnlineStat(head.To)
	if err != nil {
		attr := "gop2p/get_mc_failed_count"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcToMobile Get user stat failed from=%v uid=%v seq=%v cmd=0x%4x", head.From, head.To, head.Seq, head.Cmd)
		result = uint16(ht_p2p.RET_CODE_RET_INTERNAL_ERR)
		return false
	}

	infoLog.Printf("ProcToMobile recv req to=%v uid=%v clientType=%v stat=%v SvrIp=%v updateTs=%v version=%v port=%v wid=%v usertype=%v",
		head.To,
		stat.Uid,
		stat.ClientType,
		stat.OnlineStat,
		stat.SvrIp,
		stat.UpdateTs,
		stat.Version,
		stat.Port,
		stat.Wid,
		stat.UserType)

	if stat.OnlineStat == common.ST_ONLINE ||
		reqBody.GetJustOnline() == 2 { // 在线或者直接转发
		svrIp := inet_ntoa(int64(stat.SvrIp)).String()
		infoLog.Printf("svrIP=%s oriIP=%v", svrIp, stat.SvrIp)
		v, ok := imServer[svrIp]
		if !ok { // 不存在直接打印日志返回错误
			result = uint16(ht_p2p.RET_CODE_RET_INTERNAL_ERR)
			attr := "gop2p/get_im_srv_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("not exist IM ip=%v imServer=%v", svrIp, imServer)
			return false
		}
		ret, err := v.SendPacket(head, reqBody.P2PData)
		if err != nil {
			attr := "gop2p/send_packet_to_im_failed"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_p2p.RET_CODE_RET_INTERNAL_ERR)
			infoLog.Printf("Send packet to IM failed ret=%v err=%s", ret, err)
			return false
		}
		result = uint16(ht_p2p.RET_CODE_RET_SUCCESS)
		infoLog.Printf("recv im ack ret=%v", ret)
	} else { // 不在线首先判断是否丢弃
		justOnline := reqBody.GetJustOnline()
		if justOnline == 1 { // 丢弃
			attr := "gop2p/drop_just_online_count"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_p2p.RET_CODE_RET_SUCCESS)
			infoLog.Printf("ProcToMobile Drop p2p packet cmd=%v from=%v to=%v", head.Cmd, head.From, head.To)
		} else {
			if reqBody.GetPushInfo() != nil { // push info 不为空
				push, err := BuildPushPacket(stat.ClientType, head.From, head.To, reqBody.GetPushInfo())
				if err != nil {
					attr := "gop2p/build_push_packet_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ProcToMobile build Push packet faield just save offline from=%v uid=%v seq=%v cmd=0x%4x err=%s",
						head.From,
						head.To,
						head.Seq,
						head.Cmd,
						err)
					ret, err := offlineApi.SendPacketWithHeadV3(head, reqBody.GetP2PData(), nil)
					if err != nil {
						attr := "gop2p/save_offline_failed_count"
						libcomm.AttrAdd(attr, 1)
						result = uint16(ht_p2p.RET_CODE_RET_INTERNAL_ERR)
						infoLog.Printf("ProcToMobile save offline faield ret=%v from=%v uid=%v seq=%v cmd=0x%4x err=%s",
							ret,
							head.From,
							head.To,
							head.Seq,
							head.Cmd,
							err)
					} else {
						result = ret
						infoLog.Printf("ProcToMobile save offline success ret=%v from=%v uid=%v seq=%v cmd=0x%4x",
							ret,
							head.From,
							head.To,
							head.Seq,
							head.Cmd)
					}
				} else {
					ret, err := offlineApi.SendPacketWithHeadV3(head, reqBody.GetP2PData(), push)
					if err != nil {
						attr := "gop2p/save_offline_failed_count"
						libcomm.AttrAdd(attr, 1)
						result = uint16(ht_p2p.RET_CODE_RET_INTERNAL_ERR)
						infoLog.Printf("ProcToMobile save offline faield ret=%v from=%v uid=%v seq=%v cmd=0x%4x err=%s",
							ret,
							head.From,
							head.To,
							head.Seq,
							head.Cmd,
							err)
					} else {
						result = ret
						infoLog.Printf("ProcToMobile save offline success ret=%v from=%v uid=%v seq=%v cmd=0x%4x",
							ret,
							head.From,
							head.To,
							head.Seq,
							head.Cmd)
					}
				}
			} else { // push info 为空
				ret, err := offlineApi.SendPacketWithHeadV3(head, reqBody.GetP2PData(), nil)
				if err != nil {
					attr := "gop2p/save_offline_failed_count"
					libcomm.AttrAdd(attr, 1)
					result = uint16(ht_p2p.RET_CODE_RET_INTERNAL_ERR)
					infoLog.Printf("ProcToMobile save offline faield ret=%v from=%v uid=%v seq=%v cmd=0x%4x err=%s",
						ret,
						head.From,
						head.To,
						head.Seq,
						head.Cmd,
						err)
				} else {
					result = ret
					infoLog.Printf("ProcToMobile save offline success ret=%v from=%v uid=%v seq=%v cmd=0x%4x",
						ret,
						head.From,
						head.To,
						head.Seq,
						head.Cmd)
				}
			}
		}
	}
	return true
}

func BuildPushPacket(terminalType uint8, fromId, toId uint32, pushInfo *ht_p2p.PushInfo) (outPacket []byte, err error) {
	infoLog.Printf("BuildPushPacket fromId=%v toId=%v terminalType=%v pushType=%v nickName=%s sound=%s",
		fromId,
		toId,
		terminalType,
		pushInfo.GetPushType(),
		pushInfo.GetNickName(),
		pushInfo.GetPushSound())
	if fromId == 0 || toId == 0 || terminalType > 1 {
		infoLog.Printf("BuildPushPacket invalid param from=%v to=%v terminalType=%v", fromId, toId, terminalType)
		err = ErrInputParamErr
		return nil, err
	}
	// VOIP推送的默认参数
	var roomId uint32 = 0
	var chatType uint8 = uint8(CT_P2P)
	var pushType uint8 = uint8(pushInfo.GetPushType())
	var sound uint8 = uint8(pushInfo.GetPushSound())
	var lights uint8 = uint8(1)

	limitNickeName := make([]byte, NICKNAME_LEN)
	copy(limitNickeName, pushInfo.GetNickName())

	limitPushContent := make([]byte, CONTENT_LEN)
	copy(limitPushContent, pushInfo.GetContent())

	limitMsgId := make([]byte, CONTENT_LEN)
	msgId := fmt.Sprintf("%v", time.Now().Unix())
	copy(limitMsgId, []byte(msgId))

	var packetPayLoad []byte
	common.MarshalUint8(terminalType, &packetPayLoad)
	common.MarshalUint8(chatType, &packetPayLoad)
	common.MarshalUint32(fromId, &packetPayLoad)
	common.MarshalUint32(toId, &packetPayLoad)
	common.MarshalUint32(roomId, &packetPayLoad)
	common.MarshalUint8(pushType, &packetPayLoad)
	common.MarshalSlice(limitNickeName, &packetPayLoad)
	common.MarshalSlice(limitPushContent, &packetPayLoad)
	common.MarshalUint8(sound, &packetPayLoad)
	common.MarshalUint8(lights, &packetPayLoad)
	common.MarshalSlice(limitMsgId, &packetPayLoad)
	var actionId uint32
	var byAt uint8 = uint8(CNotBeenAt)
	common.MarshalUint32(actionId, &packetPayLoad)
	common.MarshalUint8(byAt, &packetPayLoad)

	head := &common.XTHead{
		Flag:     common.CServToServ,
		Version:  common.CVerMmedia,
		CryKey:   common.CServKey,
		TermType: terminalType,
		Cmd:      CMD_S2S_MESSAGE_PUSH,
		Seq:      GetPacketSeq(),
		From:     fromId,
		To:       toId,
		Len:      0,
	}
	// 使用Server key 加密
	cryptoText := libcrypto.TEAEncrypt(string(packetPayLoad), SERVER_COMM_KEY)
	infoLog.Printf("BuildPushPacket cryptoText len=%v", len(cryptoText))

	head.Len = uint32(len(cryptoText)) //
	outPacket = make([]byte, common.XTHeadLen+head.Len)
	err = common.SerialXTHeadToSlice(head, outPacket[:])
	if err != nil {
		infoLog.Println("BuildPushPacket SerialXTHeadToSlice failed")
		return nil, err
	}
	copy(outPacket[common.XTHeadLen:], []byte(cryptoText)) // return code
	return outPacket, nil
}

func GetPacketSeq() (packetSeq uint16) {
	sendSeq++
	return sendSeq
}

func SendPacketToPcRelabile(stat *common.PcState, head *common.HeadV3, payLoad []byte) (err error) {
	if stat.OnlineStat != common.ST_ONLINE {
		infoLog.Printf("SendPacketToPcRelabile online stat=%v error", stat.OnlineStat)
		return ErrInputParamErr
	}

	svrIp := inet_ntoa(int64(stat.SvrIp)).String()
	infoLog.Printf("SendPacketToPcRelabile svrIP=%s oriIP=%v", svrIp, stat.SvrIp)
	v, ok := pcImServer[svrIp]
	if !ok { // 不存在直接打印日志返回错误
		infoLog.Printf("SendPacketToPcRelabile not exist IM ip=%v imServer=%#v", svrIp, pcImServer)
		return ErrOnlineIpErr
	}

	tryCount := 2
	for tryCount > 0 {
		ret, err := v.SendPacket(head, payLoad)
		if err == nil && ret == 0 {
			infoLog.Printf("SendPacketToPcRelabile send succ from=%v to=%v seq=%v cmd=%v", head.From, head.To, head.Seq, head.Cmd)
			return nil
		} else {
			infoLog.Printf("SendPacketToPcRelabile failed from=%v to=%v seq=%v cmd=%v err=%v ret=%v", head.From, head.To, head.Seq, head.Cmd, err, ret)
		}
		tryCount--
	}
	return common.ErrReachMaxTryCount
}

func ProcToPc(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_p2p.P2PMsgBody) bool {
	result := uint16(ht_p2p.RET_CODE_RET_SUCCESS)
	defer func() {
		SendResp(c, head, result)
	}()

	pcStat, err := mcApi.GetPcOnlineStat(head.To)
	if err != nil {
		attr := "gop2p/get_pc_stat_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcToPc Get pc stat failed from=%v uid=%v seq=%v cmd=0x%4x", head.From, head.To, head.Seq, head.Cmd)
		result = uint16(ht_p2p.RET_CODE_RET_INTERNAL_ERR)
		return false
	}
	// PC 不在线不用发送直接返回
	if pcStat.OnlineStat != common.ST_ONLINE {
		infoLog.Printf("ProcToPc from=%v to=%v seq=%v cmd=0x%x toId=%v not online stat=%v",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			head.To,
			pcStat.OnlineStat)
		result = uint16(ht_p2p.RET_CODE_RET_SUCCESS)
		return true
	}
	infoLog.Printf("ProcToPc recv req to=%v uid=%v clientType=%v stat=%v SvrIp=%v updateTs=%v version=%v port=%v usertype=%v",
		head.To,
		pcStat.Uid,
		pcStat.ClientType,
		pcStat.OnlineStat,
		pcStat.SvrIp,
		pcStat.UpdateTs,
		pcStat.Version,
		pcStat.Port,
		pcStat.UserType)

	rebuildHeader := *head
	rebuildHeader.CryKey = uint8(common.CNoneKey)

	err = SendPacketToPcRelabile(pcStat, &rebuildHeader, reqBody.P2PData)
	if err != nil {
		infoLog.Printf("BroadcastMucMessageToPc SendPacketToIMServerRelabile failed from=%v to=%v seq=%v",
			rebuildHeader.From,
			rebuildHeader.To,
			rebuildHeader.Seq)
	}
	return true
}

func ProcToPcWithOutAck(head *common.HeadV3, reqBody *ht_p2p.P2PMsgBody) bool {
	pcStat, err := mcApi.GetPcOnlineStat(head.To)
	if err != nil {
		attr := "gop2p/get_pc_stat_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcToPc Get pc stat failed from=%v uid=%v seq=%v cmd=0x%4x", head.From, head.To, head.Seq, head.Cmd)
		return false
	}
	// PC 不在线不用发送直接返回
	if pcStat.OnlineStat != common.ST_ONLINE {
		infoLog.Printf("ProcToPc from=%v to=%v seq=%v cmd=0x%x toId=%v not online stat=%v",
			head.From,
			head.To,
			head.Seq,
			head.Cmd,
			head.To,
			pcStat.OnlineStat)
		return true
	}
	infoLog.Printf("ProcToPc recv req to=%v uid=%v clientType=%v stat=%v SvrIp=%v updateTs=%v version=%v port=%v usertype=%v",
		head.To,
		pcStat.Uid,
		pcStat.ClientType,
		pcStat.OnlineStat,
		pcStat.SvrIp,
		pcStat.UpdateTs,
		pcStat.Version,
		pcStat.Port,
		pcStat.UserType)

	rebuildHeader := *head
	rebuildHeader.CryKey = uint8(common.CNoneKey)

	err = SendPacketToPcRelabile(pcStat, &rebuildHeader, reqBody.P2PData)
	if err != nil {
		infoLog.Printf("BroadcastMucMessageToPc SendPacketToIMServerRelabile failed from=%v to=%v seq=%v",
			rebuildHeader.From,
			rebuildHeader.To,
			rebuildHeader.Seq)
	}
	return true
}

// func buildPushContent(head *common.HeadV3, pushInfo *ht_p2p.PushInfo) (push []byte, err error) {
// 	newHead := &common.HeadV3{From: head.From, To: head.To, Cmd: uint16(ht_push.CMD_TYPE_CMD_S2S_MESSAGE_PUSH), SysType: uint16(ht_push.SYS_TYPE_SYS_VOIP_SERVER)}
// 	reqBody := new(ht_push.ReqBody)
// 	reqBody.PushMsgReqbody = new(ht_push.PushMsgReqBody)
// 	subReqBody := reqBody.GetPushMsgReqbody()
// 	subReqBody.ChatType = new(uint32)
// 	*(subReqBody.ChatType) = 0

// 	subReqBody.RoomId = new(uint32)
// 	*(subReqBody.RoomId) = 0

// 	subReqBody.PushType = new(uint32)
// 	*(subReqBody.PushType) = pushInfo.GetPushType()

// 	subReqBody.Nickname = make([]byte, len(pushInfo.GetNickName()))
// 	copy(subReqBody.Nickname, pushInfo.GetNickName())

// 	subReqBody.Content = make([]byte, len(pushInfo.GetContent()))
// 	copy(subReqBody.Content, pushInfo.GetContent())

// 	subReqBody.Sound = new(uint32)
// 	*(subReqBody.Sound) = 1

// 	subReqBody.Light = new(uint32)
// 	*(subReqBody.Light) = 1

// 	strTime := time.Now().String()
// 	subReqBody.MessageId = make([]byte, len(strTime))
// 	copy(subReqBody.MessageId, []byte(strTime))

// 	s, err := proto.Marshal(reqBody)
// 	if err != nil {
// 		return nil, err
// 	}

// 	newHead.Len = uint32(common.PacketV3HeadLen) + uint32(len(s)) + 1
// 	push = make([]byte, newHead.Len)
// 	push[0] = common.HTV3MagicBegin
// 	err = common.SerialHeadV3ToSlice(newHead, push[1:])
// 	if err != nil {
// 		return nil, err
// 	}
// 	copy(push[common.PacketV3HeadLen:], s)
// 	push[newHead.Len-1] = common.HTV3MagicEnd
// 	return push, nil
// }

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init send seq
	sendSeq = 0
	// 读取 Memcache Ip and port
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustInt(11211)
	infoLog.Printf("memcache ip=%v port=%v", mcIp, mcPort)
	mcApi = new(common.MemcacheApi)
	mcApi.Init(mcIp + ":" + strconv.Itoa(mcPort))

	// 读取offline 配置
	offlineIp := cfg.Section("OFFLINE").Key("offline_ip").MustString("127.0.0.1")
	offlinePort := cfg.Section("OFFLINE").Key("offline_port").MustString("0")
	infoLog.Printf("offline server ip=%v port=%v", offlineIp, offlinePort)
	offlineApi = common.NewOfflineApiV2(offlineIp, offlinePort, time.Minute, time.Minute, &common.HeadV2Protocol{}, 1000)

	// 读取IMServer 配置
	imCount := cfg.Section("IMSERVER").Key("imserver_cnt").MustInt(0)
	imConnLimit := cfg.Section("IMSERVER").Key("pool_limit").MustInt(1000)
	imServer = make(map[string]*common.ImServerApiV2, imCount)
	infoLog.Printf("IMServer Count=%v", imCount)
	for i := 0; i < imCount; i++ {
		ipKey := "imserver_ip_" + strconv.Itoa(i)
		ipOnlineKye := "imserver_ip_online_" + strconv.Itoa(i)
		portKey := "imserver_port_" + strconv.Itoa(i)
		imIp := cfg.Section("IMSERVER").Key(ipKey).MustString("127.0.0.1")
		imIpOnline := cfg.Section("IMSERVER").Key(ipOnlineKye).MustString("127.0.0.1")
		imPort := cfg.Section("IMSERVER").Key(portKey).MustString("28080")
		infoLog.Printf("im server ip=%v ip_online=%v port=%v", imIp, imIpOnline, imPort)
		imServer[imIpOnline] = common.NewImServerApiV2(imIp, imPort, 2*time.Second, 2*time.Second, &common.HeadV3Protocol{}, imConnLimit)
	}

	// 读取PC 端IMServer 配置
	pcImCount := cfg.Section("PCIMSERVER").Key("pc_imserver_cnt").MustInt(1)
	pcImConnLimit := cfg.Section("PCIMSERVER").Key("pc_pool_limit").MustInt(1000)
	pcImServer = make(map[string]*common.ImServerApiV2, pcImCount)
	infoLog.Printf("PCIMServer Count=%v", pcImCount)
	for i := 0; i < pcImCount; i++ {
		pcIpKey := "pc_imserver_ip_" + strconv.Itoa(i)
		pcIpOnlineKye := "pc_imserver_ip_online_" + strconv.Itoa(i)
		pcPortKey := "pc_imserver_port_" + strconv.Itoa(i)
		pcImIp := cfg.Section("PCIMSERVER").Key(pcIpKey).MustString("127.0.0.1")
		pcImIpOnline := cfg.Section("PCIMSERVER").Key(pcIpOnlineKye).MustString("127.0.0.1")
		pcImPort := cfg.Section("PCIMSERVER").Key(pcPortKey).MustString("18380")
		infoLog.Printf("pc im server ip=%v ip_online=%v port=%v", pcImIp, pcImIpOnline, pcImPort)
		pcImServer[pcImIpOnline] = common.NewImServerApiV2(pcImIp, pcImPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, pcImConnLimit)
	}

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}
	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
