package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/libcrypto"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	simplejson "github.com/bitly/go-simplejson"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	nsq "github.com/nsqio/go-nsq"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	mcApi          *common.MemcacheApi
	db             *sql.DB
	globalProducer *nsq.Producer
	nsqAPNSTopic   string
	nsqGCMTopic    string
	nsqXinGeTopic  string
	nsqGeTuiTopic  string
	globalRemark   map[string]string
	remarkLock     sync.Mutex
	relationApi    *common.RelationApi
	userCacheApi   *common.UserCacheApi
)

// Error type
var (
	ErrNilMcObject      = errors.New("nil memcache object")
	ErrNilDbObject      = errors.New("not set object current is nil")
	ErrParam            = errors.New("err input param")
	ErrNotOpenPush      = errors.New("err not open push")
	ErrNULLPushToken    = errors.New("err push token is empty")
	ErrInvalidPushToken = errors.New("err invalid push token")
	ErrNotFoundRemark   = errors.New("err not found remark")
)

const (
	OFFICE_BEGIN_UID      = 10000
	APNS_TOKEN_LEN        = 64
	ONCE_GETRELATIONLINES = 1000000
)
const (
	CTP2P = 0
	CTMUC = 1
)

const (
	CMD_S2S_MARKNAMECHANGED_NOTIFY     = 0x8025
	CMD_S2S_MARKNAMECHANGED_NOTIFY_ACK = 0x8026
	CMD_S2S_MESSAGE_PUSH               = 0x8027
	CMD_S2S_MESSAGE_PUSH_ACK           = 0x8028
)

const (
	RET_SUCCESS            = 0
	ERR_SYSERR_START       = 100
	ERR_SERVER_BUSY        = 100
	ERR_INTERNAL_ERROR     = 101
	ERR_UNFORMATTED_PACKET = 102
	ERR_NO_ACCESS          = 103
	ERR_INVALID_CLIENT     = 104
	ERR_INVALID_SESSION    = 105
	ERR_INVALID_PARAM      = 106
)

const (
	SERVER_COMM_KEY    = "lp$5F@nfN0Oh8I*5"
	PUSH_SOUND_CALLING = "voipcall.caf"
	PUSH_SOUND_DEFAULT = "default"
	INVAlID_TOKEN      = "Invalid_Token"
)

const (
	APNS_PUSH_SOUND_CALLING  = "voipcall.caf"
	APNS_PUSH_SOUND_DEFAULT  = "default"
	PUSH_PARAM_CALLINCOMING  = "voip_call_incoming"
	PUSH_PARAM_CALLIMISS     = "voip_call_miss"
	PUSH_PARAM_CALLICANCEL   = "voip_call_cancel"
	PUSH_PARAM_LX_ACCEPT     = "language_exchange_accept"
	PUSH_PARAM_LX_DECLINED   = "language_exchange_declined"
	PUSH_PARAM_LX_TERMINATED = "language_exchange_terminated"
	PUSH_PARAM_LX_CANCLED    = "language_exchange_cancled"

	APNS_PUSH_LANGUAGE_EXCHANGE_V2 = "Language Exchange Request push"
	APNS_PUSH_LX_ACCEPT_V2         = "%@: Exchange request agreed push"
	APNS_PUSH_LX_DECLIND_V2        = "Request Declined push"
	APNS_PUSH_LX_TERMINATED_V2     = "Terminate Exchange push"
	APNS_PUSH_LX_CANCLED_V2        = "Exchange Request Canceled push"
	APNS_PUSH_CALLINCOMING         = "Incoming Call"
	APNS_PUSH_CALLMISS             = "Call Canceled"
	APNS_PUSH_CALLCANCEL           = "Call Missed"
)

// GCM push
const (
	GCM_PUSH_NOPREVIEW     = "no_preview"
	GCM_PUSH_TEXT          = "text"
	GCM_PUSH_VOICE         = "voice"
	GCM_PUSH_IMAGE         = "image"
	GCM_PUSH_LOCATION      = "location"
	GCM_PUSH_INTRODUCE     = "introduce"
	GCM_PUSH_CORRECT       = "correction"
	GCM_PUSH_STICKER       = "sticker"
	GCM_PUSH_DOODLE        = "doodle"
	GCM_PUSH_INVITE        = "friend_invite"
	GCM_PUSH_LX            = "language_exchange"
	GCM_PUSH_LX_REPLY      = "language_exchange_reply" // Reply Code "Declined" "Accepted" "Terminated"
	GCM_PUSH_GIFT          = "gift"
	GCM_PUSH_CALL_INCOMING = "call_incoming"
	GCM_PUSH_CALL_CANCEL   = "call_cancel"
	GCM_PUSH_CALL_MISS     = "call_miss"
	GCM_PUSH_ACCEPT_INVITE = "accept_invite"
	GCM_PUSH_VIDEO         = "video" // add video push
	GCM_PUSH_GVOIP         = "gvoip"
	GCM_PUSH_LINK          = "message_preview_example_no"
	GCM_PUSH_CARD          = "card"

	// 2016-08-26 add by songliwei
	GCM_PUSH_FOLLOW              = "s_has_followed_you"
	GCM_PUSH_REPLY_YOUR_COMMENT  = "s_replied_your_comment"
	GCM_PUSH_COMMENTED_YOUR_POST = "s_commented_your_post"
	GCM_PUSH_CORRECTED_YOUR_POST = "s_corrected_your_post"
	GCM_PUSH_POST_MNT            = "s_post_mnt"

	GCM_PUSH_CREATE_WHITE_BOARD = "s_create_white_board"
	GCM_PUSH_NEW_MSG_NOTIFY     = "s_new_msg_notify"
	GMC_PUSH_GROUP_LESSON       = "s_group_lesson"
	GCM_PUSH_START_CHARGE       = "s_start_charge"
)

const (
	PUSH_TEXT                    = 0
	PUSH_VOICE                   = 1
	PUSH_IMAGE                   = 2
	PUSH_INTRODUCE               = 3
	PUSH_LOCATION                = 4
	PUSH_FRIEND_INVITE           = 5
	PUSH_LANGUAGE_EXCHANGE       = 6
	PUSH_LANGUAGE_EXCHANGE_REPLY = 7
	PUSH_CORRECT_SENTENCE        = 8
	PUSH_STICKERS                = 9
	PUSH_DOODLE                  = 10
	PUSH_GIFT                    = 11
	PUSH_VOIP                    = 12
	PUSH_ACCEPT_INVITE           = 13
	PUSH_VIDEO                   = 14
	PUSH_GVOIP                   = 15 // group voip
	PUSH_LINK                    = 16
	PUSH_CARD                    = 17
	PUSH_FOLLOW                  = 18
	PUSH_REPLY_YOUR_COMMENT      = 19
	PUSH_COMMENTED_YOUR_POST     = 20
	PUSH_CORRECTED_YOUR_POST     = 21
	PUSH_MOMENT_LIKE             = 22
	PUSH_POST_MOMENT             = 23
	PUSH_WHITE_BOARD             = 24
	PUSH_NEW_MSG_NOTIFY          = 25
	PUSH_GROUP_LESSON            = 26
	PUSH_START_CHARGE            = 27
	PUSH_UNKNOW                  = 100
)

const (
	PT_TEXT              = 0
	PT_VOICE             = 1
	PT_PHOTO             = 2
	PT_INDRODUCE         = 3
	PT_LOCATION          = 4
	PT_FRIEND_INVITE     = 5
	PT_LANGUAGE_EXCHANGE = 6
	PT_CORRECT_SENTENCE  = 7
	PT_STICKERS          = 8
	PT_DOODLE            = 9
	PT_GIFT              = 10
	PT_VOIP              = 11
	PT_INVITE_ACCEPT     = 12
	PT_VIDEO             = 13

	PT_GVOIP               = 15
	PT_LINK                = 16
	PT_CARD                = 17
	PT_FOLLOW              = 18
	PT_REPLY_YOUR_COMMENT  = 19
	PT_COMMENTED_YOUR_POST = 20
	PT_CORRECTED_YOUR_POST = 21
	PT_MOMENT_LIKE         = 22
	PT_POST_MNT            = 23
	PT_WHITE_BOARD         = 24
	PT_NEW_MSG_NOTIFY      = 25
	PT_GROUP_LESSON        = 26
	PT_START_CHANGE        = 27
	PT_UNKNOW              = 100
)

type PushMessage struct {
	terminalType uint8
	chatType     uint8
	fromId       uint32
	toId         uint32
	roomId       uint32
	pushType     uint8
	nickName     []byte
	pushContent  []byte
	sound        uint8
	lights       uint8
	messageId    []byte
	actionId     uint32
	byAt         uint8
}

type SimplePushSetting struct {
	token    []byte
	preview  uint8
	dndTime  bool
	badge    uint16
	pushType uint8
}

type StoreUserPushSetting struct {
	uid       uint32
	pushToken []byte
	dndSet    uint8
	dndStart  uint8
	dndEnd    uint8
	timeZone  uint8
	alert     uint8
	preview   uint8
	pushType  uint8
	follow    uint8
	moment    uint8
}

type ChangeRemarkName struct {
	From       uint32
	To         uint32
	RemarkName []byte
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	log.Println("OnConnect:", addr)
	return true
}

// all push packet need not response
func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.XTHeadPacket)
	if !ok { // 不是XTHeadPacket报文
		log.Println("OnMessage packet can not change to xtpacket")
		return false
	}

	head, err := packet.GetHead()
	if err != nil {
		log.Printf("OnMessage Get head failed err=%s", err)
		return false
	}
	attr := "push_producer/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	//log.Printf("OnMessage:[%#v] len=%v payLoad=%v\n", head, len(packet.GetBody()), packet.GetBody())

	log.Printf("OnMessage:from=%v to=%v cmd=0x%4x seq=%v terminaltype=%v len=%v\n",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		head.TermType,
		len(packet.GetBody()))
	// log.Printf("OnMessage:head=[%#v] len=%v\n", *head, len(packet.GetBody()))
	_, err = packet.CheckXTPacketValid()
	if err != nil {
		// SendResp(c, head, uint8(ERR_INVALID_PARAM))
		log.Printf("Invalid packet err=%s", err)
		return false
	}

	switch head.Cmd {
	case CMD_S2S_MESSAGE_PUSH:
		go ProcMessagePush(c, head, packet)
	case CMD_S2S_MARKNAMECHANGED_NOTIFY:
		go ProcChangeRemarkName(c, head, packet)
	default:
		log.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
	}
	return true
}

func SendResp(c *gotcp.Conn, reqHead *common.XTHead, ret uint8) bool {
	if c == nil || reqHead == nil {
		log.Printf("SendResp nil conn=%v reqhead=%v", c, reqHead)
		return false
	}

	head := new(common.XTHead)
	if reqHead != nil {
		*head = *reqHead
	}

	head.Cmd = reqHead.Cmd + 1 // ack cmd = req cmd + 1
	head.Len = 1               // sizeof(uint8)
	buf := make([]byte, common.XTHeadLen+head.Len)
	err := common.SerialXTHeadToSlice(head, buf[:])
	if err != nil {
		log.Printf("SendResp SerialXTHeadToSlice failed")
		return false
	}
	buf[common.XTHeadLen] = ret // return code
	resp := common.NewXTHeadPacket(buf)
	c.AsyncWritePacket(resp, time.Second)
	return true
}

func SendRespWithPayLoad(c *gotcp.Conn, reqHead *common.XTHead, payLoad []byte) bool {
	if c == nil || reqHead == nil {
		log.Printf("SendRespWithPayLoad nil conn=%v reqhead=%v", c, reqHead)
		return false
	}

	head := new(common.XTHead)
	if reqHead != nil {
		*head = *reqHead
	}

	head.Cmd = reqHead.Cmd + 1      // ack cmd = req cmd + 1
	head.Len = uint32(len(payLoad)) //
	buf := make([]byte, common.XTHeadLen+head.Len)
	err := common.SerialXTHeadToSlice(head, buf[:])
	if err != nil {
		log.Println("SerialXTHeadToSlice failed")
		return false
	}
	copy(buf[common.XTHeadLen:], payLoad) // return code
	resp := common.NewXTHeadPacket(buf)
	c.AsyncWritePacket(resp, time.Second)
	return true
}
func ProcMessagePush(c *gotcp.Conn, reqHead *common.XTHead, packet *common.XTHeadPacket) bool {
	if c == nil || reqHead == nil || packet == nil {
		log.Printf("ProcMessagePush invalid param c=%v reqHead=%v packetlen=%v", c, reqHead, len(packet.GetBody()))
		return false
	}
	// 统计push总量
	attr := "push_producer/total_push_message_count"
	libcomm.AttrAdd(attr, 1)

	var body []byte
	if reqHead.CryKey == common.CServKey {
		// 使用Server key 加密
		plainText := libcrypto.TEADecrypt(string(packet.GetBody()), SERVER_COMM_KEY)
		// log.Printf("ProcMessagePush plainText len=%v", len(plainText))
		body = []byte(plainText)
	} else if reqHead.CryKey == common.CNoneKey {
		body = packet.GetBody()
	} else {
		log.Printf("ProcMessagePush UnHandle CryKey=%v", reqHead.CryKey)
		return false
	}

	message := PushMessage{
		terminalType: common.UnMarshalUint8(&body),
		chatType:     common.UnMarshalUint8(&body),
		fromId:       common.UnMarshalUint32(&body),
		toId:         common.UnMarshalUint32(&body),
		roomId:       common.UnMarshalUint32(&body),
		pushType:     common.UnMarshalUint8(&body),
		nickName:     common.UnMarshalSlice(&body),
		pushContent:  common.UnMarshalSlice(&body),
		sound:        common.UnMarshalUint8(&body),
		lights:       common.UnMarshalUint8(&body),
		messageId:    common.UnMarshalSlice(&body),
		actionId:     common.UnMarshalUint32(&body),
		byAt:         common.UnMarshalUint8(&body),
	}
	trimNickName := strings.Trim(string(message.nickName), "\u0000")
	if len(trimNickName) == 0 {
		message.nickName = nil
	} else {
		message.nickName = []byte(trimNickName)
	}
	// log.Printf("ProcMessagePush trim nickName=%s nickNameLen=%v", message.nickName, len(message.nickName))
	// // 首先查询PC是否在线 如果PC在线则不往手机发送推送
	// pcStat, err := mcApi.GetPcOnlineStat(message.toId)
	// if err != nil {
	// 	log.Printf("ProcMessagePush mcApi.GetPcOnlineStat toId=%v err=%s", message.toId, err)
	// } else {
	// 	if pcStat.OnlineStat == common.ST_ONLINE &&
	// 		message.pushType != PUSH_VOIP &&
	// 		message.pushType != PUSH_VIDEO &&
	// 		message.pushType != PUSH_GVOIP &&
	// 		message.pushType != PUSH_FOLLOW &&
	// 		message.pushType != PT_REPLY_YOUR_COMMENT &&
	// 		message.pushType != PT_COMMENTED_YOUR_POST &&
	// 		message.pushType != PT_CORRECTED_YOUR_POST &&
	// 		message.pushType != PT_MOMENT_LIKE &&
	// 		message.pushType != PT_POST_MNT {
	// 		log.Printf("ProcMessagePush mcApi.GetPcOnlineStat toId=%v PC is online continue", message.toId)
	// 		return true
	// 	}
	// }
	// log.Printf("ProcMessagePush PushMessage=%#v", message)
	pushSetting, err := GetPushSetting(message.toId, message.terminalType, message.pushType)
	if err != nil {
		log.Printf("ProcMessagePush get push setting failed err=%v fromId=%v toId=%v terminalTyep=%v pushType=%v",
			err,
			message.fromId,
			message.toId,
			message.terminalType,
			message.pushType)
		return false
	}

	log.Printf("pushSetting fromId=%v toId=%v terminalTyep=%v pushType=%v token=%s preview=%v dndTime=%v badge=%v pushType=%v userName=%s",
		message.fromId,
		message.toId,
		message.terminalType,
		message.pushType,
		pushSetting.token,
		pushSetting.preview,
		pushSetting.dndTime,
		pushSetting.badge,
		pushSetting.pushType,
		message.nickName)
	// add static
	attr = fmt.Sprintf("push_producer/push_%v_message_count", message.pushType)
	libcomm.AttrAdd(attr, 1)

	var finalRemarName []byte
	if reqHead.From != 0 && reqHead.To != 0 && reqHead.From != 10000 && reqHead.To != 10000 {
		remarkName, err := relationApi.QueryRemarkName(reqHead.To, reqHead.From)
		trimRemarkName := []byte(strings.Trim(string(remarkName), "\u0000"))
		if len(trimRemarkName) == 0 {
			trimRemarkName = nil
		}
		// log.Printf("pushSetting fromId=%v toId=%v terminalTyep=%v pushType=%v relation remarkname=%s",
		// 	message.fromId,
		// 	message.toId,
		// 	message.terminalType,
		// 	message.pushType,
		// 	remarkName)
		if err != nil {
			attr := "push_producer/query_relation_remark_failed"
			libcomm.AttrAdd(attr, 1)
			log.Printf("ProcMessagePush query relationApi failed from=%v to=%v seq=%v cmd=0x%4x err=%s",
				reqHead.From,
				reqHead.To,
				reqHead.Seq,
				reqHead.Cmd,
				err)
		} else { // 查询成功
			// if trimRemarkName == nil && (message.nickName == nil || message.pushType == PUSH_FOLLOW) { // 查新成功当relation中的remarname 仍然为空继续查询UserCache
			if trimRemarkName == nil && (message.nickName == nil) { // 查新成功当relation中的remarname 仍然为空继续查询UserCache
				nickName, userName, err := userCacheApi.QueryNickNameAndUserName(reqHead.From, reqHead.From)
				// log.Printf("ProcMessagePush query fromId=%v toId=%v terminalTyep=%v pushType=%v nickName=%s userName=%s",
				// 	message.fromId,
				// 	message.toId,
				// 	message.terminalType,
				// 	message.pushType,
				// 	nickName,
				// 	userName)
				if err != nil {
					attr := "push_producer/query_user_nickname_failed"
					libcomm.AttrAdd(attr, 1)
					log.Printf("ProcMessagePush query usercache failed from=%v to=%v seq=%v cmd=0x%4x err=%s",
						reqHead.From,
						reqHead.To,
						reqHead.Seq,
						reqHead.Cmd,
						err)
				} else { // 查询usercache成功
					finalRemarName = nickName
					if finalRemarName == nil && userName == nil {
						log.Printf("ProcMessagePush from=%v to=%v seq=%v cmd=0x%4x nickName and userName empty",
							reqHead.From,
							reqHead.To,
							reqHead.Seq,
							reqHead.Cmd)
					} else if finalRemarName == nil {
						finalRemarName = userName
					}
				}
			} else {
				finalRemarName = trimRemarkName
			}
		}
	}
	/*有备注名使用备注名，否则使用昵称*/
	finalRemarName = []byte(strings.Trim(string(finalRemarName), "\u0000"))
	if len(finalRemarName) != 0 {
		message.nickName = finalRemarName
		log.Printf("ProcMessagePush from=%v to=%v seq=%v cmd=0x%4x use remark name=%s",
			reqHead.From,
			reqHead.To,
			reqHead.Seq,
			reqHead.Cmd,
			finalRemarName)
	}

	if message.terminalType == common.CClientTyepIOS {
		err = PushAPNSMessage(&message, pushSetting)
		if err != nil {
			log.Printf("ProcMessagePush PushAPNSMessage failed err=%s", err)
		}
	} else {
		// 0:GCM 1:xinge 2:getui
		if pushSetting.pushType == 1 {
			err = PushXinGeMessage(&message, pushSetting)
			if err != nil {
				log.Printf("ProcMessagePush PushXinGeMessage failed err=%s", err)
			}
		} else if pushSetting.pushType == 2 {
			err = PushGeTuiMessage(&message, pushSetting)
			if err != nil {
				log.Printf("ProcMessagePush PushGeTuiMessage failed err=%s", err)
			}
		} else {
			err = PushGCMMessage(&message, pushSetting)
			if err != nil {
				log.Printf("ProcMessagePush PushGCMMessage failed err=%s", err)
			}
		}
	}
	return true
}

func GetPushSetting(uid uint32, terminal, pushType uint8) (pushSetting *SimplePushSetting, err error) {
	if mcApi == nil {
		log.Printf("GetPushSetting nil mcApi")
		return nil, ErrNilMcObject
	}
	pushSetting = new(SimplePushSetting)
	// push badge
	if terminal == uint8(common.CClientTyepIOS) {
		badge, err := mcApi.GetPushBadge(uid)
		if err != nil {
			log.Printf("GetPushSetting mcApi.GetPushBadge failed uid=%v err=%v", uid, err)
			badge = 0 // get mc failed set badge zero
		}
		pushSetting.badge = badge
	}
	pushConfig, err := mcApi.GetPushSetting(uid)
	if err != nil {
		log.Printf("GetPushSetting McApi.GetPushSetting load from db failed uid=%v err=%v", uid, err)
		attr := "push_producer/get_mc_failed_load_from_db_count"
		libcomm.AttrAdd(attr, 1)
		storePushConfig, err := ReloadUserPushSettingByUid(uid)
		if err != nil {
			log.Printf("GetPushSetting ReloadUserPushSettingByUid faild uid=%v err=%v", uid, err)
			attr := "push_producer/get_mc_failed_load_from_db_failed_count"
			libcomm.AttrAdd(attr, 1)
			return nil, err
		}
		pushConfig.Uid = storePushConfig.uid
		pushConfig.PushToken = storePushConfig.pushToken
		pushConfig.DndSet = storePushConfig.dndSet
		pushConfig.DndStart = storePushConfig.dndStart
		pushConfig.DndEnd = storePushConfig.dndEnd
		pushConfig.TimeZone = storePushConfig.timeZone
		pushConfig.Alert = storePushConfig.alert
		pushConfig.Preview = storePushConfig.preview
		pushConfig.PushType = storePushConfig.pushType
		pushConfig.Follow = storePushConfig.follow
		pushConfig.Moment = storePushConfig.moment

		err = mcApi.SetPushSetting(&pushConfig)
		if err != nil {
			log.Printf("GetPushSetting mcApi.SetPushSetting()exec failed uid=%v err=%v", pushConfig.Uid, err)
		} else {
			// log.Printf("GetPushSetting mcApi.SetPushSetting success pushConfig=%#v", pushConfig)
		}
	} else {
		// log.Printf("GetPushSetting mcApi.GetPushSetting success pushConfig=%#v", pushConfig)
	}
	// whether open push
	if pushConfig.Alert == 0 {
		// log.Printf("GetPushSetting close push pushConfig=%#v", pushConfig)
		log.Printf("GetPushSetting close push")
		return nil, ErrNotOpenPush
	} else if pushConfig.Follow == 0 && pushType == PUSH_FOLLOW { // 1:on 0:off
		// log.Printf("GetPushSetting close follow notification pushConfig=%#v", pushConfig)
		log.Printf("GetPushSetting close follow notification")
		return nil, ErrNotOpenPush
	} else if pushConfig.Moment == 0 && (pushType == PUSH_REPLY_YOUR_COMMENT ||
		pushType == PUSH_COMMENTED_YOUR_POST ||
		pushType == PUSH_CORRECTED_YOUR_POST) { //1:on 0:off
		// log.Printf("GetPushSetting close moment notification pushConfig=%#v", pushConfig)
		log.Printf("GetPushSetting close moment notification")
		return nil, ErrNotOpenPush
	}
	pushSetting.preview = pushConfig.Preview
	if len(pushConfig.PushToken) == 0 {
		attr := "push_producer/null_token_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("GetPushSetting push token is empty uid=%v terminal=%v pushConfig=%#v", uid, terminal, pushConfig)
		return nil, ErrNULLPushToken
	}
	// token 合法性检验
	if terminal == common.CClientTyepIOS {
		if !CheckAPNSToken(pushConfig.PushToken) {
			attr := "push_producer/invalid_apns_token_count"
			libcomm.AttrAdd(attr, 1)
			log.Printf("GetPushSetting invalid apns token uid=%v token=%s", uid, pushConfig.PushToken)
			return nil, ErrInvalidPushToken
		}
		pushSetting.token = pushConfig.PushToken
	} else {
		if len(pushConfig.PushToken) <= 0 || strings.HasPrefix(string(pushConfig.PushToken), INVAlID_TOKEN) {
			attr := "push_producer/invalid_android_token_count"
			libcomm.AttrAdd(attr, 1)
			log.Printf("GetPushSetting invalid android token uid=%v pushConfig=%#v", uid, pushConfig)
			return nil, ErrInvalidPushToken
		}
		pushSetting.token = pushConfig.PushToken
		// pushSetting.isXinGe = false
		// pushtype 为1:信鸽 0:GCM
		pushSetting.pushType = pushConfig.PushType
	}
	// 是否在免打扰时间内
	if pushConfig.DndSet == 1 {
		localHour := GetLocalHour(pushConfig.TimeZone)
		pushSetting.dndTime = BetweenDNDTime(uint8(localHour), pushConfig.DndStart, pushConfig.DndEnd)
	} else {
		pushSetting.dndTime = false
	}
	return pushSetting, nil
}

func CheckAPNSToken(pushToken []byte) (result bool) {
	tokenLen := len(pushToken)
	if tokenLen != APNS_TOKEN_LEN {
		return false
	}
	for _, v := range pushToken {
		if !((v >= '0' && v <= '9') || (v >= 'a' && v <= 'f')) {
			return false
		}
	}
	return true
}

func GetLocalHour(timeZone uint8) (localHour uint16) {
	// timeZone 是可能小余0 的需要将无符号转成有符号数字
	realyZone := int8(timeZone)
	// zone 里面记录的是小时需要将小时转换成秒
	localZone := time.FixedZone("UTC", int(realyZone)*3600)
	timeNow := time.Now()
	timeLocal := timeNow.In(localZone)
	localHour = uint16(timeLocal.Hour())
	return localHour
}

func BetweenDNDTime(localHour, startHour, endHour uint8) (ret bool) {
	if startHour <= endHour {
		ret = localHour >= startHour && localHour <= endHour
	} else {
		ret = !(localHour >= endHour && localHour <= startHour)
	}
	return ret
}

func ReloadUserPushSettingByUid(uid uint32) (pushSetting *StoreUserPushSetting, err error) {
	if db == nil || uid < OFFICE_BEGIN_UID {
		return nil, ErrParam
	}

	pushSetting = new(StoreUserPushSetting)

	var storeUid sql.NullInt64
	var storeToken sql.NullString
	var storeTimeZone, storeDndSet, storeDndStart, storeDndEnd, storeAlert, storePreview, storePushType, storeFollow, storeMoment sql.NullInt64
	err = db.QueryRow("SELECT t1.USERID, t1.APNSTOKEN, t2.TIMEZONE, t3.DNDSET, t3.DNDSTART, t3.DNDEND, t3.NOTIFYALERT, t3.NOTIFYPREVIEW, t1.PUSHTYPE, t3.NOTIFY_FOLLOW, t3.NOTIFY_MOMENT FROM HT_USER_TERMINAL AS t1 LEFT JOIN (HT_USER_BASE AS t2, HT_USER_SETTING AS t3) ON (t2.USERID = t1.USERID and t3.USERID = t1.USERID) WHERE t1.USERID=?", uid).Scan(&storeUid, &storeToken, &storeTimeZone, &storeDndSet, &storeDndStart, &storeDndEnd, &storeAlert, &storePreview, &storePushType, &storeFollow, &storeMoment)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("ReloadUserPushSettingByUid not found uid=%v", uid)
		return nil, err
	case err != nil:
		log.Printf("ReloadUserPushSettingByUid exec failed uid=%v err=%v", uid, err)
		return nil, err
	default:
	}

	if storeUid.Valid {
		pushSetting.uid = uint32(storeUid.Int64)
	}
	if storeToken.Valid {
		pushSetting.pushToken = []byte(storeToken.String)
	}
	if storeTimeZone.Valid {
		pushSetting.timeZone = uint8(storeTimeZone.Int64)
	}
	if storeDndSet.Valid {
		pushSetting.dndSet = uint8(storeDndSet.Int64)
	}
	if storeDndStart.Valid {
		pushSetting.dndStart = uint8(storeDndStart.Int64)
	}
	if storeDndEnd.Valid {
		pushSetting.dndEnd = uint8(storeDndEnd.Int64)
	}
	if storeAlert.Valid {
		pushSetting.alert = uint8(storeAlert.Int64)
	}
	if storePreview.Valid {
		pushSetting.preview = uint8(storePreview.Int64)
	}
	if storePushType.Valid {
		pushSetting.pushType = uint8(storePushType.Int64)
	}
	if storeFollow.Valid {
		pushSetting.follow = uint8(storeFollow.Int64)
	}
	if storeMoment.Valid {
		pushSetting.moment = uint8(storeMoment.Int64)
	}
	log.Printf("ReloadUserPushSettingByUid pushSetting=%#v", pushSetting)

	return pushSetting, nil
}

func PushAPNSMessage(message *PushMessage, pushSetting *SimplePushSetting) (err error) {
	if message == nil || pushSetting == nil {
		log.Printf("PushAPNSMessage input param err message=%v, pushSetting=%v", message, pushSetting)
		return ErrParam
	}
	// 统计apns总量
	attr := "push_producer/apns_total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	var soundStr string
	if !pushSetting.dndTime && message.sound == 1 {
		if message.pushType == uint8(PUSH_VOIP) && strings.HasPrefix(string(message.pushContent), PUSH_PARAM_CALLINCOMING) {
			soundStr = PUSH_SOUND_CALLING
		} else {
			soundStr = PUSH_SOUND_DEFAULT
		}
	}
	var senderName string = string(message.nickName)
	// remarkName, err := FindRemarkName(message.fromId, message.toId)
	// if err == nil && len(remarkName) > 0 {
	// 	senderName = remarkName
	// }
	var fromId uint32
	if message.chatType == uint8(CTMUC) {
		fromId = message.roomId
	} else {
		fromId = message.fromId
	}

	pushType := PT_TEXT
	pushParam := message.pushContent
	switch int(message.pushType) {
	case PUSH_TEXT:
		pushType = PT_TEXT
	case PUSH_VOICE:
		pushType = PT_VOICE
	case PUSH_IMAGE:
		pushType = PT_PHOTO
	case PUSH_INTRODUCE:
		pushType = PT_INDRODUCE
	case PUSH_LOCATION:
		pushType = PT_LOCATION
	case PUSH_FRIEND_INVITE:
		pushType = PT_FRIEND_INVITE
	case PUSH_LANGUAGE_EXCHANGE, PUSH_LANGUAGE_EXCHANGE_REPLY:
		pushType = PT_LANGUAGE_EXCHANGE
		outParma := GetLanguageExchagePushParam(message)
		pushParam = []byte(outParma)
	case PUSH_CORRECT_SENTENCE:
		pushType = PT_CORRECT_SENTENCE
	case PUSH_STICKERS:
		pushType = PT_STICKERS
	case PUSH_DOODLE:
		pushType = PT_DOODLE
	case PUSH_GIFT:
		pushType = PT_GIFT
	case PUSH_VOIP:
		pushType = PT_VOIP
		pushCont := string(message.pushContent)
		log.Printf("pushCont=%s", pushCont)
		if strings.HasPrefix(pushCont, PUSH_PARAM_CALLINCOMING) {
			pushParam = []byte(APNS_PUSH_CALLINCOMING)
		} else if strings.HasPrefix(pushCont, PUSH_PARAM_CALLICANCEL) {
			pushParam = []byte(APNS_PUSH_CALLCANCEL)
		} else {
			pushParam = []byte(APNS_PUSH_CALLMISS)
		}
	case PUSH_ACCEPT_INVITE:
		pushType = PT_INVITE_ACCEPT
	case PUSH_VIDEO:
		pushType = PT_VIDEO
	case PUSH_GVOIP:
		pushType = PT_GVOIP
	case PUSH_LINK:
		pushType = PT_LINK
	case PUSH_CARD:
		pushType = PT_CARD
	case PUSH_FOLLOW:
		pushType = PT_FOLLOW
	case PUSH_REPLY_YOUR_COMMENT:
		pushType = PT_REPLY_YOUR_COMMENT
	case PUSH_COMMENTED_YOUR_POST:
		pushType = PT_COMMENTED_YOUR_POST
	case PUSH_CORRECTED_YOUR_POST:
		pushType = PT_CORRECTED_YOUR_POST
	case PUSH_POST_MOMENT:
		pushType = PT_POST_MNT
	case PUSH_WHITE_BOARD:
		pushType = PT_WHITE_BOARD
	case PUSH_NEW_MSG_NOTIFY:
		pushType = PT_NEW_MSG_NOTIFY
	case PUSH_GROUP_LESSON:
		pushType = PT_GROUP_LESSON
	case PUSH_START_CHARGE:
		pushType = PT_START_CHANGE
	default:
		log.Printf("PushAPNSMessage unknow type=%d", message.pushType)
		return nil
	}

	msgObj := simplejson.New()
	msgObj.Set("chat_type", message.chatType)
	msgObj.Set("from_id", fromId)
	msgObj.Set("to_id", message.toId)
	msgObj.Set("push_type", pushType)
	msgObj.Set("preview", pushSetting.preview)
	msgObj.Set("token", string(pushSetting.token))
	msgObj.Set("badge_num", pushSetting.badge+1)
	msgObj.Set("sound", soundStr)
	msgObj.Set("nick_name", senderName)
	msgObj.Set("push_param", string(pushParam))
	msgObj.Set("msg_id", string(message.messageId))
	msgObj.Set("expired", 0)
	msgObj.Set("action_id", message.actionId)
	msgObj.Set("by_at", message.byAt)

	msgSlice, err := msgObj.MarshalJSON()
	if err != nil {
		log.Printf("PushAPNSMessage msgObj.MarshalJson failed from=%v to=%v chatType=%v pushType=%v",
			message.fromId,
			message.toId,
			message.chatType,
			message.pushType)
		return
	}

	err = globalProducer.Publish(nsqAPNSTopic, msgSlice)
	if err != nil {
		// 统计apns失败总量
		attr := "push_producer/apns_total_failed_req_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PushAPNSMessage Could not connect from=%v to=%v chatType=%v pushType=%v",
			message.fromId,
			message.toId,
			message.chatType,
			message.pushType)
	} else {
		// 统计apns成功的总量
		attr := "push_producer/apns_total_succ_req_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PushAPNSMessage Publish success from=%v to=%v chatType=%v pushType=%v",
			fromId,
			message.toId,
			message.chatType,
			pushType)
	}

	if message.pushType != PT_VOIP {
		UpdateAPNSBadge(message.toId, pushSetting.badge+1)
	} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_CALLINCOMING) { // VOIP只有呼叫推送才Badge+1
		UpdateAPNSBadge(message.toId, pushSetting.badge+1)
	}
	return err
}

func GetLanguageExchagePushParam(message *PushMessage) (outParam string) {
	if message.pushType != PUSH_LANGUAGE_EXCHANGE ||
		message.pushType != PUSH_LANGUAGE_EXCHANGE_REPLY {
		log.Printf("GetLanguageExchagePushParam invalid tyep=%v", message.pushType)
		return
	}
	if message.pushType == PUSH_LANGUAGE_EXCHANGE {
		outParam = APNS_PUSH_LANGUAGE_EXCHANGE_V2
	} else if message.pushType == PUSH_LANGUAGE_EXCHANGE_REPLY {
		if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_ACCEPT) {
			outParam = APNS_PUSH_LX_ACCEPT_V2
		} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_DECLINED) {
			outParam = APNS_PUSH_LX_DECLIND_V2
		} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_CANCLED) {
			outParam = APNS_PUSH_LX_CANCLED_V2
		} else {
			outParam = APNS_PUSH_LX_TERMINATED_V2
		}
	}
	return outParam
}

func UpdateAPNSBadge(uid uint32, badge uint16) (err error) {
	if uid == 0 {
		err = ErrParam
		log.Printf("UpdateAPNSBadge err param uid=%v badge=%v", uid, badge)
		return err
	}
	err = mcApi.SetPushBadge(uid, badge)
	if err != nil {
		log.Printf("UpdateAPNSBadge uid=%v badge=%v err=%s faild", uid, badge, err)
	}
	return err
}

func PushXinGeMessage(message *PushMessage, pushSetting *SimplePushSetting) (err error) {
	// Sender Nickname
	senderName := string(message.nickName)
	// remarkName, err := FindRemarkName(message.fromId, message.toId)
	// if err == nil && len(remarkName) > 0 {
	// 	senderName = remarkName
	// }
	// 统计xinge总量
	attr := "push_producer/xinge_total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	//fromId
	var fromId uint32
	if message.chatType == uint8(CTMUC) {
		fromId = message.roomId
	} else {
		fromId = message.fromId
	}

	// Sound control
	sound := 1
	if pushSetting.dndTime || message.sound == 0 {
		sound = 0
	}

	// XINGE Push type
	pushType := GCM_PUSH_NOPREVIEW
	var replyCode string
	if pushSetting.preview == 1 {
		switch message.pushType {
		case PUSH_TEXT:
			pushType = GCM_PUSH_TEXT
		case PUSH_VOICE:
			pushType = GCM_PUSH_VOICE
		case PUSH_IMAGE:
			pushType = GCM_PUSH_IMAGE
		case PUSH_INTRODUCE:
			pushType = GCM_PUSH_INTRODUCE
		case PUSH_LOCATION:
			pushType = GCM_PUSH_LOCATION
		case PUSH_FRIEND_INVITE:
			pushType = GCM_PUSH_INVITE
		case PUSH_LANGUAGE_EXCHANGE:
			pushType = GCM_PUSH_LX
		case PUSH_LANGUAGE_EXCHANGE_REPLY:
			{
				pushType = GCM_PUSH_LX_REPLY
				// Reply Code
				if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_ACCEPT) {
					replyCode = "Accepted"
				} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_DECLINED) {
					replyCode = "Declined"
				} else {
					replyCode = "Terminated"
				}
				message.pushContent = nil
			}
		case PUSH_CORRECT_SENTENCE:
			pushType = GCM_PUSH_CORRECT
		case PUSH_STICKERS:
			pushType = GCM_PUSH_STICKER
		case PUSH_DOODLE:
			pushType = GCM_PUSH_DOODLE
		case PUSH_GIFT:
			pushType = GCM_PUSH_GIFT
		case PUSH_VOIP:
			{
				// VOIP Type
				if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_CALLINCOMING) {
					pushType = GCM_PUSH_CALL_INCOMING
				} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_CALLICANCEL) {
					pushType = GCM_PUSH_CALL_CANCEL
				} else {
					pushType = GCM_PUSH_CALL_MISS
				}
				message.pushContent = nil
			}
		case PUSH_VIDEO:
			pushType = GCM_PUSH_VIDEO
		case PUSH_GVOIP:
			pushType = GCM_PUSH_GVOIP
		case PUSH_LINK:
			pushType = GCM_PUSH_LINK
		case PUSH_CARD:
			pushType = GCM_PUSH_CARD
		case PUSH_FOLLOW:
			pushType = GCM_PUSH_FOLLOW
		case PUSH_REPLY_YOUR_COMMENT:
			pushType = GCM_PUSH_REPLY_YOUR_COMMENT
		case PUSH_COMMENTED_YOUR_POST:
			pushType = GCM_PUSH_COMMENTED_YOUR_POST
		case PUSH_CORRECTED_YOUR_POST:
			pushType = GCM_PUSH_CORRECTED_YOUR_POST
		case PUSH_POST_MOMENT:
			pushType = GCM_PUSH_POST_MNT
		case PUSH_WHITE_BOARD:
			pushType = GCM_PUSH_CREATE_WHITE_BOARD
		case PUSH_NEW_MSG_NOTIFY:
			pushType = GCM_PUSH_NEW_MSG_NOTIFY
		case PUSH_GROUP_LESSON:
			pushType = GMC_PUSH_GROUP_LESSON
		case PUSH_START_CHARGE:
			pushType = GCM_PUSH_START_CHARGE
		default:
			log.Printf("PushXinGeMessage unknow fromId=%v toId=%v chatType=%v pushType=%v",
				message.fromId,
				message.toId,
				message.chatType,
				message.pushType)
		}
	}
	// get to clinet version
	newPush := 0
	onlineStat, err := mcApi.GetUserOnlineStat(message.toId)
	if err != nil {
		log.Printf("PushXinGeMessage mcApi.GetPcOnlineStat toId=%v err=%s", message.toId, err)
	} else {
		if onlineStat.Version >= common.CVerSion253 {
			newPush = 1
		}
	}

	msgObj := simplejson.New()
	msgObj.Set("chat_type", message.chatType)
	msgObj.Set("token", string(pushSetting.token))
	msgObj.Set("title", senderName)
	msgObj.Set("content", string(message.pushContent))
	msgObj.Set("msg_type", pushType)
	msgObj.Set("sound", sound)
	msgObj.Set("lights", sound)
	msgObj.Set("from_id", fromId)
	msgObj.Set("msg_id", string(message.messageId))
	msgObj.Set("to_id", message.toId)
	msgObj.Set("reply_code", replyCode)
	msgObj.Set("actionid", message.actionId)
	msgObj.Set("byAt", message.byAt)
	msgObj.Set("int_push_type", message.pushType)
	msgObj.Set("new_push", newPush)
	msgSlice, err := msgObj.MarshalJSON()
	if err != nil {
		log.Printf("PushXinGeMessage msgObj.MarshalJSON failed from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
		return
	}
	err = globalProducer.Publish(nsqXinGeTopic, msgSlice)
	if err != nil {
		// 统计xinge总量
		attr := "push_producer/xinge_total_failed_req_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PushXinGeMessage Could not connect from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
	} else {
		// 统计xinge总量
		attr := "push_producer/xinge_total_succ_req_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PushXinGeMessage Publish success from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
	}
	return err
}

func PushGeTuiMessage(message *PushMessage, pushSetting *SimplePushSetting) (err error) {
	if message == nil || pushSetting == nil {
		log.Printf("PushGeTuiMessage input param err message=%v, pushSetting=%v", message, pushSetting)
		return ErrParam
	}

	// 统计apns总量
	attr := "push_producer/getui_total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	// sender nickname
	var senderName string = string(message.nickName)
	// remarkName, err := FindRemarkName(message.fromId, message.toId)
	// if err == nil && len(remarkName) > 0 {
	// 	senderName = remarkName
	// }

	//fromId
	var fromId uint32
	if message.chatType == uint8(CTMUC) {
		fromId = message.roomId
	} else {
		fromId = message.fromId
	}

	// Sound control
	sound := 1
	if pushSetting.dndTime || message.sound == 0 {
		sound = 0
	}

	// GCM PushType
	pushType := GCM_PUSH_NOPREVIEW
	pushParam := string(message.pushContent)

	if pushSetting.preview == 1 {
		switch message.pushType {
		case PUSH_TEXT:
			pushType = GCM_PUSH_TEXT
		case PUSH_VOICE:
			pushType = GCM_PUSH_VOICE
		case PUSH_IMAGE:
			pushType = GCM_PUSH_IMAGE
		case PUSH_INTRODUCE:
			pushType = GCM_PUSH_INTRODUCE
		case PUSH_LOCATION:
			pushType = GCM_PUSH_LOCATION
		case PUSH_FRIEND_INVITE:
			pushType = GCM_PUSH_INVITE
		case PUSH_LANGUAGE_EXCHANGE:
			pushType = GCM_PUSH_LX
		case PUSH_LANGUAGE_EXCHANGE_REPLY:
			pushType = GCM_PUSH_LX_REPLY
			// Reply Code
			if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_ACCEPT) {
				pushParam = "Accepted"
			} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_DECLINED) {
				pushParam = "Declined"
			} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_CANCLED) {
				pushParam = "Cancled"
			} else {
				pushParam = "Terminated"
			}
		case PUSH_CORRECT_SENTENCE:
			pushType = GCM_PUSH_CORRECT
		case PUSH_STICKERS:
			pushType = GCM_PUSH_STICKER
		case PUSH_DOODLE:
			pushType = GCM_PUSH_DOODLE
		case PUSH_GIFT:
			pushType = GCM_PUSH_GIFT
		case PUSH_VOIP:
			// VOIP Type
			if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_CALLINCOMING) {
				pushType = GCM_PUSH_CALL_INCOMING
			} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_CALLICANCEL) {
				pushType = GCM_PUSH_CALL_CANCEL
			} else {
				pushType = GCM_PUSH_CALL_MISS
			}
		case PUSH_ACCEPT_INVITE:
			pushType = GCM_PUSH_ACCEPT_INVITE
		case PUSH_VIDEO:
			pushType = GCM_PUSH_VIDEO
		case PUSH_GVOIP:
			pushType = GCM_PUSH_GVOIP
		case PUSH_LINK:
			pushType = GCM_PUSH_LINK
		case PUSH_CARD:
			pushType = GCM_PUSH_CARD
		case PUSH_FOLLOW:
			pushType = GCM_PUSH_FOLLOW
		case PUSH_REPLY_YOUR_COMMENT:
			pushType = GCM_PUSH_REPLY_YOUR_COMMENT
		case PUSH_COMMENTED_YOUR_POST:
			pushType = GCM_PUSH_COMMENTED_YOUR_POST
		case PUSH_CORRECTED_YOUR_POST:
			pushType = GCM_PUSH_CORRECTED_YOUR_POST
		case PUSH_POST_MOMENT:
			pushType = GCM_PUSH_POST_MNT
		case PUSH_WHITE_BOARD:
			pushType = GCM_PUSH_CREATE_WHITE_BOARD
		case PUSH_NEW_MSG_NOTIFY:
			pushType = GCM_PUSH_NEW_MSG_NOTIFY
		case PUSH_GROUP_LESSON:
			pushType = GMC_PUSH_GROUP_LESSON
		case PUSH_START_CHARGE:
			pushType = GCM_PUSH_START_CHARGE
		default:
			break
		}
	}
	msgObj := simplejson.New()
	msgObj.Set("token", string(pushSetting.token))
	msgObj.Set("push_type", pushType)
	msgObj.Set("from_id", fromId)
	msgObj.Set("sender", senderName)
	msgObj.Set("push_param", string(pushParam))
	msgObj.Set("msg_id", string(message.messageId))
	msgObj.Set("sound", sound)
	msgObj.Set("to_id", message.toId)
	msgObj.Set("action_id", message.actionId)
	msgObj.Set("by_at", message.byAt)
	msgObj.Set("chat_type", message.chatType)
	msgObj.Set("int_push_type", message.pushType)
	msgSlice, err := msgObj.MarshalJSON()
	if err != nil {
		log.Printf("PushGeTuiMessage msgObj.MarshalJSON failed from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
		return
	}
	err = globalProducer.Publish(nsqGeTuiTopic, msgSlice)
	if err != nil {
		// 统计apns总量
		attr := "push_producer/getui_total_failed_req_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PushGeTuiMessage Could not connect from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
	} else {
		// 统计apns总量
		attr := "push_producer/getui_total_succ_req_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PushGeTuiMessage Publish success from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
	}

	return err
}

func PushGCMMessage(message *PushMessage, pushSetting *SimplePushSetting) (err error) {
	if message == nil || pushSetting == nil {
		log.Printf("PushGCMMessage input param err message=%v, pushSetting=%v", message, pushSetting)
		return ErrParam
	}

	// 统计apns总量
	attr := "push_producer/gcm_total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	// sender nickname
	var senderName string = string(message.nickName)
	// remarkName, err := FindRemarkName(message.fromId, message.toId)
	// if err == nil && len(remarkName) > 0 {
	// 	senderName = remarkName
	// }

	//fromId
	var fromId uint32
	if message.chatType == uint8(CTMUC) {
		fromId = message.roomId
	} else {
		fromId = message.fromId
	}

	// Sound control
	sound := 1
	if pushSetting.dndTime || message.sound == 0 {
		sound = 0
	}

	// GCM PushType
	pushType := GCM_PUSH_NOPREVIEW
	pushParam := string(message.pushContent)

	if pushSetting.preview == 1 {
		switch message.pushType {
		case PUSH_TEXT:
			pushType = GCM_PUSH_TEXT
		case PUSH_VOICE:
			pushType = GCM_PUSH_VOICE
		case PUSH_IMAGE:
			pushType = GCM_PUSH_IMAGE
		case PUSH_INTRODUCE:
			pushType = GCM_PUSH_INTRODUCE
		case PUSH_LOCATION:
			pushType = GCM_PUSH_LOCATION
		case PUSH_FRIEND_INVITE:
			pushType = GCM_PUSH_INVITE
		case PUSH_LANGUAGE_EXCHANGE:
			pushType = GCM_PUSH_LX
		case PUSH_LANGUAGE_EXCHANGE_REPLY:
			pushType = GCM_PUSH_LX_REPLY
			// Reply Code
			if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_ACCEPT) {
				pushParam = "Accepted"
			} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_DECLINED) {
				pushParam = "Declined"
			} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_LX_CANCLED) {
				pushParam = "Cancled"
			} else {
				pushParam = "Terminated"
			}
		case PUSH_CORRECT_SENTENCE:
			pushType = GCM_PUSH_CORRECT
		case PUSH_STICKERS:
			pushType = GCM_PUSH_STICKER
		case PUSH_DOODLE:
			pushType = GCM_PUSH_DOODLE
		case PUSH_GIFT:
			pushType = GCM_PUSH_GIFT
		case PUSH_VOIP:
			// VOIP Type
			if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_CALLINCOMING) {
				pushType = GCM_PUSH_CALL_INCOMING
			} else if strings.HasPrefix(string(message.pushContent), PUSH_PARAM_CALLICANCEL) {
				pushType = GCM_PUSH_CALL_CANCEL
			} else {
				pushType = GCM_PUSH_CALL_MISS
			}
		case PUSH_ACCEPT_INVITE:
			pushType = GCM_PUSH_ACCEPT_INVITE
		case PUSH_VIDEO:
			pushType = GCM_PUSH_VIDEO
		case PUSH_GVOIP:
			pushType = GCM_PUSH_GVOIP
		case PUSH_LINK:
			pushType = GCM_PUSH_LINK
		case PUSH_CARD:
			pushType = GCM_PUSH_CARD
		case PUSH_FOLLOW:
			pushType = GCM_PUSH_FOLLOW
		case PUSH_REPLY_YOUR_COMMENT:
			pushType = GCM_PUSH_REPLY_YOUR_COMMENT
		case PUSH_COMMENTED_YOUR_POST:
			pushType = GCM_PUSH_COMMENTED_YOUR_POST
		case PUSH_CORRECTED_YOUR_POST:
			pushType = GCM_PUSH_CORRECTED_YOUR_POST
		case PUSH_POST_MOMENT:
			pushType = GCM_PUSH_POST_MNT
		case PUSH_WHITE_BOARD:
			pushType = GCM_PUSH_CREATE_WHITE_BOARD
		case PUSH_NEW_MSG_NOTIFY:
			pushType = GCM_PUSH_NEW_MSG_NOTIFY
		case PUSH_GROUP_LESSON:
			pushType = GMC_PUSH_GROUP_LESSON
		case PUSH_START_CHARGE:
			pushType = GCM_PUSH_START_CHARGE
		default:
			break
		}
	}
	msgObj := simplejson.New()
	msgObj.Set("registration_id", string(pushSetting.token))
	msgObj.Set("push_type", pushType)
	msgObj.Set("from_id", fromId)
	msgObj.Set("sender", senderName)
	msgObj.Set("push_param", string(pushParam))
	msgObj.Set("msg_id", string(message.messageId))
	msgObj.Set("sound", sound)
	msgObj.Set("to_id", message.toId)
	msgObj.Set("action_id", message.actionId)
	msgObj.Set("by_at", message.byAt)
	msgObj.Set("chat_type", message.chatType)
	msgObj.Set("int_push_type", message.pushType)
	msgSlice, err := msgObj.MarshalJSON()
	if err != nil {
		log.Printf("PushGCMMessage msgObj.MarshalJSON failed from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
		return
	}
	err = globalProducer.Publish(nsqGCMTopic, msgSlice)
	if err != nil {
		// 统计apns总量
		attr := "push_producer/gcm_total_failed_req_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PushGCMMessage Could not connect from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
	} else {
		// 统计apns总量
		attr := "push_producer/gcm_total_succ_req_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("PushGCMMessage Publish success from=%v to=%v chatType=%v pushType=%s",
			fromId,
			message.toId,
			message.chatType,
			pushType)
	}

	return err
}

func ProcChangeRemarkName(c *gotcp.Conn, reqHead *common.XTHead, packet *common.XTHeadPacket) bool {
	if c == nil || reqHead == nil || packet == nil {
		log.Printf("ProcChangeRemarkName invalid param c=%v reqHead=%v packetlen=%v", c, reqHead, len(packet.GetBody()))
		return false
	}
	// 统计push总量
	attr := "push_producer/total_change_remarkname_req_count"
	libcomm.AttrAdd(attr, 1)

	var body []byte
	if reqHead.CryKey == common.CServKey {
		// 使用Server key 加密
		plainText := libcrypto.TEADecrypt(string(packet.GetBody()), SERVER_COMM_KEY)
		log.Printf("ProcChangeRemarkName plainText len=%v", len(plainText))
		body = []byte(plainText)
	} else if reqHead.CryKey == common.CNoneKey {
		body = packet.GetBody()
	} else {
		log.Printf("ProcChangeRemarkName UnHandle CryKey=%v", reqHead.CryKey)
		return false
	}
	changeReq := ChangeRemarkName{
		From:       common.UnMarshalUint32(&body),
		To:         common.UnMarshalUint32(&body),
		RemarkName: common.UnMarshalSlice(&body),
	}
	err := UpdateRemarkName(changeReq.From, changeReq.To, changeReq.RemarkName)
	if err != nil {
		log.Printf("ProcChangeRemarkName exec UpdateRemarkName failed from=%v to=%v name=%s",
			changeReq.From,
			changeReq.To,
			changeReq.RemarkName)
	}
	return true
}

func UpdateRemarkName(from, to uint32, name []byte) (err error) {
	if from == 0 || to == 0 || len(name) == 0 {
		log.Printf("UpdateRemarkName input param err from=%v to=%v name=%v",
			from,
			to,
			name)
		err = ErrParam
		return err
	}
	key := GetRemarkNameKey(from, to)
	remarkLock.Lock()
	defer remarkLock.Unlock()
	if v, ok := globalRemark[key]; ok {
		log.Printf("UpdateRemarkName key=%s prevName=%s nowName=%s",
			key,
			v,
			name)
	} else {
		log.Printf("UpdateRemarkName insert key=%s prevName=%s nowName=%s",
			key,
			v,
			name)
	}
	globalRemark[key] = string(name)
	return nil
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	log.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	// 读取 Memcache Ip and port
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustInt(11211)
	log.Printf("memcache ip=%v port=%v", mcIp, mcPort)
	mcApi = new(common.MemcacheApi)
	mcApi.Init(mcIp + ":" + strconv.Itoa(mcPort))

	// nsq producer config
	nsqUrl := cfg.Section("NSQ").Key("url").MustString("127.0.0.1:4150")
	config := nsq.NewConfig()
	globalProducer, err = nsq.NewProducer(nsqUrl, config)
	if err != nil {
		log.Fatalf("main nsq.NewProcucer failed nsqUrl=%s", nsqUrl)
	}
	nsqAPNSTopic = cfg.Section("APNS").Key("topic").MustString("apns")
	nsqGCMTopic = cfg.Section("GCM").Key("topic").MustString("gcm")
	nsqXinGeTopic = cfg.Section("XinGe").Key("topic").MustString("xinge")
	nsqGeTuiTopic = cfg.Section("GeTui").Key("topic").MustString("getui")

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	log.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		log.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	relationIp := cfg.Section("RELATION").Key("ip").MustString("127.0.0.1")
	relationPort := cfg.Section("RELATION").Key("port").MustString("8890")
	relationConnLimit := cfg.Section("RELATION").Key("pool_limit").MustInt(1000)
	log.Printf("relation server ip=%v port=%v connLimit=%v", relationIp, relationPort, relationConnLimit)
	relationApi = common.NewRelationApi(relationIp, relationPort, 2*time.Second, 2*time.Second, &common.HeadV3Protocol{}, relationConnLimit)

	userCacheIp := cfg.Section("USERCACHE").Key("ip").MustString("127.0.0.1")
	userCachePort := cfg.Section("USERCACHE").Key("port").MustString("8890")
	userCacheConnLimit := cfg.Section("USERCACHE").Key("pool_limit").MustInt(1000)
	log.Printf("usercache server ip=%v port=%v connLimit=%v", userCacheIp, userCachePort, userCacheConnLimit)
	userCacheApi = common.NewUserCacheApi(userCacheIp, userCachePort, 2*time.Second, 2*time.Second, &common.HeadV2Protocol{}, userCacheConnLimit)
	// load all remark from db
	globalRemark = map[string]string{}
	// err = LoadRemarNameFromDB()
	// if err != nil {
	// 	log.Printf("main LoadRemarNameFromDB failed err=%s", err)
	// }

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	log.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	serverConfig := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}
	srv := gotcp.NewServer(serverConfig, &Callback{}, &common.XTHeadProtocol{})

	// starts service
	go srv.Start(listener, time.Second)
	log.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	db.Close()
}

func LoadRemarNameFromDB() (err error) {
	if db == nil {
		err = ErrNilDbObject
		log.Printf("LoadRemarNameFromDB nil db object")
		return err
	}
	remarkLock.Lock()
	defer remarkLock.Unlock()
	var startLine uint32
	for {
		getLines, err := GetAllFriendRemark(startLine, ONCE_GETRELATIONLINES, &globalRemark)
		if err != nil {
			log.Printf("LoadRemarNameFromDB exec GetAllFriendRemark failed err=%s", err)
			return err
		}
		startLine += getLines
		log.Printf("LoadRemarNameFromDB get lines=%v", getLines)
		if getLines == 0 {
			log.Printf("LoadRemarNameFromDB get 0 lines return")
			break
		}
	}
	return nil
}

func GetAllFriendRemark(startLine, onceGetLines uint32, mapFriendRemark *map[string]string) (updateLines uint32, err error) {
	if db == nil {
		log.Printf("GetAllFriendRemark nil db object")
		return 0, ErrNilDbObject
	}
	var emptyRemark string
	rows, err := db.Query("select USERID,FRIENDID,FRIENDREMARKNAME from HT_FRIEND_RELATION where FRIENDREMARKNAME is not null and FRIENDREMARKNAME <>? limit ?, ?;", emptyRemark, startLine, onceGetLines)
	if err != nil {
		return 0, err
	}
	updateLines = 0
	defer rows.Close()
	for rows.Next() {
		var from, to uint32
		var remarkName string
		if err := rows.Scan(&from, &to, &remarkName); err != nil {
			log.Printf("GetAllFriendRemark rows.Scan failed")
			continue
		}
		if len(remarkName) <= 0 {
			log.Printf("GetAllFriendRemark empty remark name from=%v to=%v", from, to)
			continue
		}

		log.Printf("GetAllFriendRemark DEBUG from=%v to=%v remark=%s", from, to, remarkName)
		key := GetRemarkNameKey(from, to)
		(*mapFriendRemark)[key] = remarkName
		updateLines++
	}
	return updateLines, nil
}

// mapRemarkName 中存储的是 from 给to 备注的备注名
// 在to 给 from 发送推送时 需要将to的nickname 修改成备注名
func GetRemarkNameKey(from, to uint32) (key string) {
	key = fmt.Sprintf("%v-%v", from, to)
	return key
}

func FindRemarkName(from, to uint32) (remarkName string, err error) {
	// 需要颠倒from, to 的传参顺序因为是发送消息到to 所以需要看to 给from的备注名
	key := GetRemarkNameKey(to, from)
	remarkLock.Lock()
	defer remarkLock.Unlock()
	if v, ok := globalRemark[key]; ok {
		remarkName = v
		log.Printf("FindRemarkName to=%v from=%v remarkName=%s", from, to, remarkName)
	} else {
		err = ErrNotFoundRemark
		log.Printf("FindRemarkName to=%v from=%v not found", from, to)
	}
	return remarkName, err
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
