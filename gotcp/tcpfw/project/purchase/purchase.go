package main

import (
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"google.golang.org/api/googleapi"

	"github.com/HT_GOGO/gotcp"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_purchase"
	"github.com/HT_GOGO/gotcp/tcpfw/project/purchase/util"
	"github.com/dchest/uniuri"
	"github.com/dogenzaka/go-iap/appstore"
	"github.com/dogenzaka/go-iap/playstore"
	"github.com/streadway/amqp"
	"gopkg.in/chanxuehong/wechat.v2/mch/core"
	"gopkg.in/chanxuehong/wechat.v2/mch/pay"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog         *log.Logger
	mucApi          *common.MucApi
	appstoreConfig  appstore.Config
	appstoreClient  appstore.Client
	jsonKey         []byte
	playStoreClient playstore.Client
	weChatClient    *core.Client
	DbUtil          *util.DbUtil
	rabbitMQUrl     string
)

const (
	ProcSlowThreshold = 300000
	AdminUid          = 10000
	ExpireReceiptCode = 21006
	VerifySuccessCode = 0
)

// 与数据库中STATE的定义一致
const (
	E_STATE_INIT    = 0 // 未验证
	E_STATE_SUCC    = 1 // 验证成功
	E_STATE_INVALID = 2 // 验证失败
	E_STATE_EXPIRED = 3 // 验证过期
)

const (
	YEAR_NORMAL = 0 //'0-非年会员 1-年会员'
	YEAR_VIP    = 1
)

const (
	E_NOT_VIP       = 0   // 非会员
	E_DAYS_VIP      = 1   // 按天计数的会员(旧版本购买的月、年会员)
	E_MONTHAUTO_VIP = 2   // 月自动续费的会员
	E_YEARAUTO_VIP  = 3   // 年自动续费的会员
	E_LIFETIME_VIP  = 100 // 终生会员 (50年后过期)
)

const (
	ClientCommit = 0
	ServerCommit = 1
)

const (
	TypePurchase = 0 //0-购买
	TypeRenew    = 1 //1-续订
	TRYTIMES     = 3
)

const (
	PURCHASE_PWD                 = "f2e40870103240f7b485b6f186fa141a"
	PACKAGE_NAME                 = "com.hellotalk"
	WECHAT_PAY_APPID             = "wxd061f34c48968028"
	WECHAT_PAY_COMMERCIAL_TENANT = "1233820102"
	WECHAT_PAY_SIGN_KEY          = "B3A32EF18D804B1BA5B8CA07BEBCBE3E"
)

const (
	PRODUCT_ONE_MONTH = 1
	// "com.hellotalk.onemonth1"
	PRODUCT_ONE_MONTHAUTO = 2
	// "com.hellotalk.onemonthauto"
	// or GooglePlay="com.hellotalk.onemonth1auto"
	// 2016-07-16 android use "com.hellotalk.1monthautorenew"
	// 2016-08-08  iOS use "com.hellotalk.monthauto"
	PRODUCT_ONE_YEAR = 3
	// "com.hellotalk.oneyear"
	PRODUCT_THREE_MONTH = 4
	// "com.hellotalk.3months"
	PRODUCT_YEAR_AUTO = 5
	// "com.hellotalk.yearauto"
	PRODUCT_LIFT_TIME = 6
	// "com.hellotalk.lifetime"
	// product id for upgrade vip price
	PRODUCT_ONE_MONTH_SUB_PLAN = 7
	// iOS:"com.hellotalk.onemonthsubscriptionplan"
	// And:"com.hellotalk.onemonthsubscriptionplan2"
	PRODUCT_ONE_YEAR_SUB_PLAN = 8
	// iOS:"com.hellotalk.oneyearsubscriptionplan"
	// iOS:"com.hellotalk.yearauto2"
	// And:"com.hellotalk.oneyearsubscriptionplan2"
	PRODUCT_LIFE_TIME_SUB_PLAN = 9
	// "com.hellotalk.lifetimeplan"

	PRODUCT_GIFT_ONEMONTH = 11
	// "com.hellotalk.Gift1M" or GooglePlay="com.hellotalk.g1m"
	PRODUCT_GIFT_THREEMONTH = 12
	// "com.hellotalk.Gift3M" or GooglePlay="com.hellotalk.g3m"
	PRODUCT_GIFT_ONEYEAR = 13
	// "com.hellotalk.Gift1Y" or GooglePlay="com.hellotalk.g1y"
	// product id for trial membership
	PRODUCT_ONE_MONTH_SUB = 14
	// iOS:"com.hellotalk.onemonthsubscription"
	// iOS:"com.hellotalk.monthauto_freetrial"
	// And:"com.hellotalk.onemonthsubscription2"
	PRODUCT_ONE_YEAR_SUB = 15
	// iOS:"com.hellotalk.oneyearsubscription"
	// iOS:"com.hellotalk.yearauto2_freetrial"
	// And:"com.hellotalk.oneyearsubscription2"
	PRODUCT_ONE_MULITLANG = 21
	// "com.hellotalk.1moreLang"
	PRODUCT_ONE_MONTH_AUTO_B = 22
	// "com.hellotalk.onemonthauto.b"
	// iOS:"com.hellotalk.super1monthauto"
	PRODUCT_ONE_MONTH_AUTO_C = 23
	// "com.hellotalk.onemonthauto.c"
	PRODUCT_YEAR_AUTO_B = 24
	// "com.hellotalk.yearauto.b"
	// iOS:"com.hellotalk.super1yearauto"
	PRODUCT_YEAR_AUTO_C = 25
	// "com.hellotalk.yearauto.c"
	PRODUCT_LIFE_TIME_B = 26
	// "com.hellotalk.lifetime.b"
	// iOS:"com.hellotalk.super1lifetime"
	PRODUCT_LIFE_TIME_C = 27
	// "com.hellotalk.lifetime.c"
	PRODUCT_ITEM_END = 28
	// product item end
)

const (
	RB_AUTORENEW_EXPIRED = "autorenew_expired"
	RB_PURCHASE_NOTIFY   = "purchase_item"
	RABBIT_SUBMIT        = "user_submit"
	EXCHANGENAME         = "push-msg"
	ROUTINGKEY           = "push-r-1"
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrInternalErr  = errors.New("internal err")
	ErrInvalidRsp   = errors.New("err invalid respones")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gopur/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_purchase.CMD_TYPE_CMD_COMMIT_PURCHASE_ORDER_REQ):
		go ProcCommitPurchaseOrder(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
		c.Close()
	}
	return true
}

// 1.proc store moment info
func ProcCommitPurchaseOrder(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_purchase.PURCHASE_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_purchase.PurchaseRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcCommitPurchaseOrder no need call uid=%v cmd=%v seq=%v", head.Uid, head.Cmd, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_PARAM)
		rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
			Status: &ht_purchase.PurchaseHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcCommitPurchaseOrder invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gopur/commit_purchase_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_purchase.PurchaseReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcCommitPurchaseOrder proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_PB_ERR)
		rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
			Status: &ht_purchase.PurchaseHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetCommitPurchaseOrderReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcCommitPurchaseOrder GetCommitPurchaseOrderReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_PARAM)
		rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
			Status: &ht_purchase.PurchaseHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	payType := subReqBody.GetPayType()
	userId := subReqBody.GetUserId()
	toId := subReqBody.GetToId()
	itemCode := subReqBody.GetItemCode()
	infoLog.Printf("ProcCommitPurchaseOrder payType=%v userId=%v toId=%v itemCode=%v",
		payType,
		userId,
		toId,
		itemCode)
	if userId == 0 ||
		toId == 0 ||
		itemCode == 0 ||
		itemCode >= PRODUCT_ITEM_END ||
		uint32(payType) == 0 ||
		payType < ht_purchase.PAY_TYPE_PAY_TYPE_APP_STORE ||
		payType > ht_purchase.PAY_TYPE_PAY_TYPE_WECHAT_PAY {
		infoLog.Printf("ProcCommitPurchaseOrder invalid param uid=%v cmd=0x%4x seq=%v payType=%v userId=%v toId=%v itemCode=%v",
			head.Uid,
			head.Cmd,
			head.Seq,
			payType,
			userId,
			toId,
			itemCode)
		result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_PARAM)
		rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
			Status: &ht_purchase.PurchaseHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}

	historyRecord := &util.PurchaseHistory{
		UserId:   userId,
		ToId:     toId,
		ItemCode: itemCode,
		PayType:  uint8(payType),
	}
	orderType := ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
	var orderId string
	var utilTimeStampRenew uint64  // 自动续费的到期时间戳(自动续费才有,以秒为单位)
	var pruchasePeriodRenew uint64 // 订单的持续时间=订单过期时间戳-订单购买时间戳(自动续费才有,以秒为单位))
	switch payType {
	case ht_purchase.PAY_TYPE_PAY_TYPE_APP_STORE:
		appleOrder := subReqBody.GetAppleOrder()
		orderId = string(appleOrder.GetTransactionId())
		outTransactionId := string(appleOrder.GetTransactionId())
		infoLog.Printf("ProcCommitPurchaseOrder appleOrder product_id=%s store_area=%s transaction_id=%s purchase_date_gmt=%s purchase_date=%s currency=%s pay_money=%s jailbroken=%v",
			appleOrder.GetProductId(),
			appleOrder.GetStoreArea(),
			appleOrder.GetTransactionId(),
			appleOrder.GetPurchaseDateGmt(),
			appleOrder.GetPurchaseDate(),
			appleOrder.GetCurrency(),
			appleOrder.GetPayMoney(),
			appleOrder.GetJailbroken())
		if itemCode != EnumProductCode(string(appleOrder.GetProductId())) {
			infoLog.Printf("ProcCommitPurchaseOrder invalid param uid=%v cmd=0x%4x seq=%v payType=%v userId=%v toId=%v itemCode=%v",
				head.Uid,
				head.Cmd,
				head.Seq,
				payType,
				userId,
				toId,
				itemCode)
			result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_PARAM)
			rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
				Status: &ht_purchase.PurchaseHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("invalid param"),
				},
			}
			return false
		}
		// 自动续费的逻辑处理
		if itemCode == PRODUCT_ONE_MONTHAUTO ||
			itemCode == PRODUCT_YEAR_AUTO ||
			itemCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itemCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itemCode == PRODUCT_ONE_MONTH_SUB || // 月费会员试用
			itemCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itemCode == PRODUCT_ONE_MONTH_AUTO_C ||
			itemCode == PRODUCT_ONE_YEAR_SUB ||
			itemCode == PRODUCT_YEAR_AUTO_B ||
			itemCode == PRODUCT_YEAR_AUTO_C {
			orderType, utilTimeStampRenew, pruchasePeriodRenew, outTransactionId, err = CheckAutoRenewApple(userId, appleOrder.GetReceiptData(), string(appleOrder.GetCurrency()), string(appleOrder.GetPayMoney()))
			infoLog.Printf("ProcCommitPurchaseOrder appstore orderType=%v utilTimeStamp=%v purchasePeriod=%v outTransactionId=%v err=%s", orderType, utilTimeStampRenew, pruchasePeriodRenew, outTransactionId, err)
			if err != nil {
				result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INTERNAL_ERR)
				rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
					Status: &ht_purchase.PurchaseHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("internal error"),
					},
				}
				return false
			}
		} else {
			orderType, err = CheckPurchaseOrderAppStore(userId, itemCode, appleOrder)
			if err != nil {
				infoLog.Printf("ProcCommitPurchaseOrder appstore orderType=%v productId=%s transactionId=%s err=%s", orderType, appleOrder.GetProductId(), appleOrder.GetTransactionId(), err)
				result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INTERNAL_ERR)
				rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
					Status: &ht_purchase.PurchaseHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("internal error"),
					},
				}
				return false
			}
		}

		historyRecord.OrderId = outTransactionId
		historyRecord.ProductId = string(appleOrder.GetProductId())
		historyRecord.Currency = string(appleOrder.GetCurrency())
		historyRecord.PayMoney = string(appleOrder.GetPayMoney())
		historyRecord.BuyTime = string(appleOrder.GetPurchaseDateGmt())
	case ht_purchase.PAY_TYPE_PAY_TYPE_GOOGLE_PLAY:
		googleOrder := subReqBody.GetGoogleOrder()
		orderId = string(googleOrder.GetOrderId())
		infoLog.Printf("ProcCommitPurchaseOrder google order_id=%s product_id=%s purchse_time=%v purchase_stat=%v purchase_country=%s currency=%s pay_money=%s purchasetoken=%s",
			googleOrder.GetOrderId(),
			googleOrder.GetProductId(),
			googleOrder.GetPurchaseTime(),
			googleOrder.GetPurchaseState(),
			googleOrder.GetPurchaseCountry(),
			googleOrder.GetCurrency(),
			googleOrder.GetPayMoney(),
			googleOrder.GetPurchaseToken())
		if (itemCode != 7) && itemCode != EnumProductCode(string(googleOrder.GetProductId())) {
			infoLog.Printf("ProcCommitPurchaseOrder google play invalid param uid=%v cmd=0x%4x seq=%v payType=%v userId=%v toId=%v itemCode=%v",
				head.Uid,
				head.Cmd,
				head.Seq,
				payType,
				userId,
				toId,
				itemCode)
			result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_PARAM)
			rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
				Status: &ht_purchase.PurchaseHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("invalid param"),
				},
			}
			return false
		}

		// 自动续费的逻辑处理
		if itemCode == PRODUCT_ONE_MONTHAUTO ||
			itemCode == PRODUCT_YEAR_AUTO ||
			itemCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itemCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itemCode == PRODUCT_ONE_MONTH_SUB || // 月费会员试用
			itemCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itemCode == PRODUCT_ONE_MONTH_AUTO_C ||
			itemCode == PRODUCT_ONE_YEAR_SUB ||
			itemCode == PRODUCT_YEAR_AUTO_B ||
			itemCode == PRODUCT_YEAR_AUTO_C {
			// 自动续费的逻辑处理
			orderType, utilTimeStampRenew, pruchasePeriodRenew, err = CheckAutoRenewGoogle(userId, googleOrder)
			infoLog.Printf("ProcCommitPurchaseOrder GooglePlay orderType=%v utilTimeStamp=%v purchasePeriod=%v err=%s", orderType, utilTimeStampRenew, pruchasePeriodRenew, err)
			if err != nil {
				result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INTERNAL_ERR)
				rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
					Status: &ht_purchase.PurchaseHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("internal error"),
					},
				}
				return false
			}
			if orderType == ht_purchase.ORDER_TYPE_E_EXPIRED_ORDER && utilTimeStampRenew > uint64(time.Now().Unix()) {
				infoLog.Printf("ProcCommitPurchaseOrder GooglePlay uid=%v toId=%v orderType=%v utilTimeStamp=%v purchasePeriod=%v fix order type",
					userId,
					toId,
					orderType,
					utilTimeStampRenew,
					pruchasePeriodRenew)
				orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
			}
		} else {
			orderType, err = CheckPurchaseOrderGooglePlay(userId, itemCode, googleOrder)
			if err != nil {
				infoLog.Printf("ProcCommitPurchaseOrder GooglePlay orderType=%v orderId=%s err=%s", orderType, googleOrder.GetOrderId(), err)
				result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INTERNAL_ERR)
				rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
					Status: &ht_purchase.PurchaseHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("internal error"),
					},
				}
				return false
			}
		}
		timeStamp := time.Now().Unix()
		timeStampStr := TimeStampToSqlTime(timeStamp)
		historyRecord.OrderId = string(googleOrder.GetOrderId())
		historyRecord.ProductId = string(googleOrder.GetProductId())
		historyRecord.Currency = string(googleOrder.GetCurrency())
		historyRecord.PayMoney = string(googleOrder.GetPayMoney())
		historyRecord.BuyTime = timeStampStr
	case ht_purchase.PAY_TYPE_PAY_TYPE_ALI_PAY:
		aliOrder := subReqBody.GetAliOrder()
		infoLog.Printf("ProcCommitPurchaseOrder aliOrder purchase_date=%s trade_no=%s partner=%v seller=%v subject=%s success=%s total_fee=%s",
			aliOrder.GetPurchaseDate(),
			aliOrder.GetTradeNo(),
			aliOrder.GetPartner(),
			aliOrder.GetSeller(),
			aliOrder.GetSubject(),
			aliOrder.GetSuccess(),
			aliOrder.GetTotalFee())
		if itemCode != EnumProductCode(string(aliOrder.GetSubject())) {
			infoLog.Println("ProcCommitPurchaseOrder aliOrder play invalid param uid=%v cmd=0x%4x seq=%v payType=%v userId=%v toId=%v itemCode=%v",
				head.Uid,
				head.Cmd,
				head.Seq,
				payType,
				userId,
				toId,
				itemCode)
			result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_PARAM)
			rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
				Status: &ht_purchase.PurchaseHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("invalid param"),
				},
			}
			return false
		}
		orderType, err = CheckPurchaseOrderAlipay(userId, itemCode, aliOrder)
		if err != nil {
			infoLog.Printf("ProcCommitPurchaseOrder AliPay orderType=%v orderId=%s err=%s", orderType, itemCode, err)
			result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INTERNAL_ERR)
			rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
				Status: &ht_purchase.PurchaseHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		orderId = string(aliOrder.GetTradeNo())
		historyRecord.OrderId = string(aliOrder.GetTradeNo())
		historyRecord.ProductId = string(aliOrder.GetSubject())
		historyRecord.Currency = "CNY"
		historyRecord.PayMoney = string(aliOrder.GetTotalFee())
		historyRecord.BuyTime = string(aliOrder.GetPurchaseDate())
	case ht_purchase.PAY_TYPE_PAY_TYPE_WECHAT_PAY:
		weChatOrder := subReqBody.GetWechatOrder()
		infoLog.Printf("ProcCommitPurchaseOrder weChatOrder purchase_date=%s trade_no=%s partner=%s seller=%s subject=%s success=%s total_fee=%s",
			weChatOrder.GetPurchaseDate(),
			weChatOrder.GetTradeNo(),
			weChatOrder.GetPartner(),
			weChatOrder.GetSeller(),
			weChatOrder.GetSubject(),
			weChatOrder.GetSuccess(),
			weChatOrder.GetTotalFee())
		if itemCode != EnumProductCode(string(weChatOrder.GetSubject())) {
			infoLog.Println("ProcCommitPurchaseOrder weChatOrder play invalid param uid=%v cmd=0x%4x seq=%v payType=%v userId=%v toId=%v itemCode=%v",
				head.Uid,
				head.Cmd,
				head.Seq,
				payType,
				userId,
				toId,
				itemCode)
			result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_PARAM)
			rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
				Status: &ht_purchase.PurchaseHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("invalid param"),
				},
			}
			return false
		}
		orderType, err = CheckPurchaseOrderWeChatPay(userId, itemCode, weChatOrder)
		if err != nil {
			infoLog.Printf("ProcCommitPurchaseOrder weChat orderType=%v orderId=%s err=%s", orderType, itemCode, err)
			result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INTERNAL_ERR)
			rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
				Status: &ht_purchase.PurchaseHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}

		orderId = string(weChatOrder.GetTradeNo())
		historyRecord.OrderId = string(weChatOrder.GetTradeNo())
		historyRecord.ProductId = string(weChatOrder.GetSubject())
		historyRecord.Currency = "CNY"
		historyRecord.PayMoney = string(weChatOrder.GetTotalFee())
		historyRecord.BuyTime = string(weChatOrder.GetPurchaseDate())
	default:
		infoLog.Printf("ProcCommitPurchaseOrder unkonw payType=%v userId=%v", payType, userId)
	}

	if orderType == ht_purchase.ORDER_TYPE_E_FAKE_ORDER {
		infoLog.Printf("ProcCommitPurchaseOrder userId=%v orderType=%v orderId=%s err=%s", userId, orderType, itemCode, err)
		result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_ORDER)
		rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
			Status: &ht_purchase.PurchaseHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid order"),
			},
		}
		return false
	} else if orderType == ht_purchase.ORDER_TYPE_E_EXPIRED_ORDER {
		infoLog.Printf("ProcCommitPurchaseOrder userId=%v orderType=%v itemCode=%v err=%s", userId, orderType, itemCode, err)
		result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_EXPIRE_ORDER)
		rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
			Status: &ht_purchase.PurchaseHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("expire order"),
			},
		}
		return false
	}

	var bRepeatCommit bool
	if orderType == ht_purchase.ORDER_TYPE_E_REPEAT_ORDER {
		bRepeatCommit = true
		infoLog.Printf("ProcCommitPurchaseOrder payType=%v userId=%v toId=%v itemCode=%v repeatCommit=%v",
			payType,
			userId,
			toId,
			itemCode,
			bRepeatCommit)
	}

	var vipExpiredBefore, vipExpiredAfter uint64
	var vipYearOut, vipTypeOut uint8
	if itemCode < PRODUCT_ITEM_END && itemCode != PRODUCT_ONE_MULITLANG {
		if orderType != ht_purchase.ORDER_TYPE_E_REPEAT_ORDER {
			// step2: 更新会员创建群成员数限制
			err = UpdateMucRoomMemberLimit(toId)
			if err != nil {
				infoLog.Printf("ProcCommitPurchaseOrder UpdateMucRoomMemberLimit failed uid=%v err=%v", toId, err)
			}
		}
		// step3: 根据订单详情修改会员过期天数
		tryCount := 0
		for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
			vipExpiredBefore, vipExpiredAfter, vipYearOut, vipTypeOut, err = UpdateVipExpiredTime(toId,
				itemCode,
				utilTimeStampRenew,
				pruchasePeriodRenew,
				bRepeatCommit)
			if err != nil {
				log.Printf("ProcCommitPurchaseOrder UpdateVipExpiredTime failed uid=%v err=%v", toId, err)
				time.Sleep(5 * time.Second) // 休眠5秒再重试
				continue
			}
			break
		}
		if tryCount == 0 {
			attr := "gopur/update_vip_expired_time_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcCommitPurchaseOrder UpdateVipExpiredTime failed uid=%v tryCount=%v", userId, tryCount)
			result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INTERNAL_ERR)
			rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
				Status: &ht_purchase.PurchaseHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	} else if itemCode == PRODUCT_ONE_MULITLANG {
		infoLog.Printf("ProcCommitPurchaseOrder QIMServer need do something")
	} else {
		infoLog.Printf("ProcCommitPurchaseOrder orderType=%v itemCode=%s unhandle", orderType, itemCode)
		result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_INVALID_PARAM)
		rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
			Status: &ht_purchase.PurchaseHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid item code"),
			},
		}
		return false
	}

	// history purchase order
	historyRecord.UtilBefore = vipExpiredBefore
	historyRecord.UtilAfter = vipExpiredAfter
	historyRecord.ClientOrServer = ClientCommit // 0: client 1:server

	// step4: 如果发起者和接收者不是同一个人则为赠送礼物 直接插入数据库
	if userId != toId && orderType != ht_purchase.ORDER_TYPE_E_REPEAT_ORDER {
		err = DbUtil.WriteUserSendPresentRecordInDB(orderId,
			userId,
			toId,
			itemCode,
			uint32(payType))
		if err != nil {
			infoLog.Printf("ProcCommitPurchaseOrder WriteUserSendPresentRecordInDB failed tradeno=%s from=%v to=%v itermCode=%v paytype=%v",
				orderId,
				userId,
				toId,
				itemCode,
				payType)
		}
	}

	rspBody.CommitPurchaseOrderRspbody = &ht_purchase.CommitPurchaseOrderRspBody{
		Status: &ht_purchase.PurchaseHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("commit success"),
		},
		OrderType:  orderType.Enum(),
		VipTsAfter: proto.Uint64(vipExpiredAfter),
		VipYear:    proto.Uint32(uint32(vipYearOut)),
		VipType:    proto.Uint32(uint32(vipTypeOut)),
		HistoryOrder: &ht_purchase.PurchaseHistory{
			OrderId:     []byte(historyRecord.OrderId),
			ItemCode:    proto.Uint32(historyRecord.ItemCode),
			ProductId:   []byte(historyRecord.ProductId),
			GiftDays:    proto.Uint32(historyRecord.GiftDays),
			UserId:      proto.Uint32(historyRecord.UserId),
			ToId:        proto.Uint32(historyRecord.ToId),
			Currency:    []byte(historyRecord.Currency),
			PayMoney:    []byte(historyRecord.PayMoney),
			PayType:     proto.Uint32(uint32(historyRecord.PayType)),
			BuyTime:     []byte(historyRecord.BuyTime),
			UtilBefore:  proto.Uint64(historyRecord.UtilBefore),
			UtilAfter:   proto.Uint64(historyRecord.UtilAfter),
			ClientOrSrv: proto.Uint32(uint32(historyRecord.ClientOrServer)),
			RetryTime:   proto.Uint32(uint32(historyRecord.RetryTime)),
		},
	}
	// 返回响应
	result = uint16(ht_purchase.PURCHASE_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	if orderType == ht_purchase.ORDER_TYPE_E_NORMAL_ORDER {
		// 将历史记录写入数据库中
		err = WritePurchaseHistoryRelabile(historyRecord)
		if err != nil {
			infoLog.Printf("ProcCommitPurchaseOrder userId=%v itemCode=%v payType=%v WritePurchaseHistoryRelabile err=%s",
				userId,
				itemCode,
				payType,
				err)
		}
		// STEP5: 发RabbitMQ通知
		err = NotifyPurchaseInformation(toId, uint8(subReqBody.GetTerminalType()), itemCode, uint32(payType), 1, 0, userId, historyRecord.OrderId, historyRecord.UtilAfter)
		if err != nil {
			attr := "gopur/notify_mq_purchase_info_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcCommitPurchaseOrder NotifyPurchaseInformation faile UserId=%v itemCode=%v orderId=%s utilAfter=%v",
				userId,
				itemCode,
				historyRecord.OrderId,
				historyRecord.UtilAfter)
		}
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		attr := "gopur/inc_chat_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcCommitPurchaseOrder userId=%v itemCode=%v payType=%v procTime=%v", userId, itemCode, payType, procTime)
	}
	return true
}

func EnumProductCode(productId string) (productCode uint32) {
	if productId == "com.hellotalk.onemonth1" {
		productCode = PRODUCT_ONE_MONTH
	} else if productId == "com.hellotalk.onemonthauto" {
		productCode = PRODUCT_ONE_MONTHAUTO
	} else if productId == "com.hellotalk.onemonthsubscriptionplan" ||
		productId == "com.hellotalk.onemonthsubscriptionplan2" {
		productCode = PRODUCT_ONE_MONTH_SUB_PLAN
	} else if productId == "com.hellotalk.onemonth1auto" ||
		productId == "com.hellotalk.1monthautorenew" ||
		productId == "com.hellotalk.monthauto" {
		productCode = PRODUCT_ONE_MONTHAUTO
	} else if productId == "com.hellotalk.oneyear" ||
		productId == "com.hellotalk.oneyeardiscount" {
		productCode = PRODUCT_ONE_YEAR
	} else if productId == "com.hellotalk.3months" {
		productCode = PRODUCT_THREE_MONTH
	} else if productId == "com.hellotalk.Gift1M" {
		productCode = PRODUCT_GIFT_ONEMONTH
	} else if productId == "com.hellotalk.g1m" {
		productCode = PRODUCT_GIFT_ONEMONTH
	} else if productId == "com.hellotalk.Gift3M" {
		productCode = PRODUCT_GIFT_THREEMONTH
	} else if productId == "com.hellotalk.g3m" {
		productCode = PRODUCT_GIFT_THREEMONTH
	} else if productId == "com.hellotalk.Gift1Y" {
		productCode = PRODUCT_GIFT_ONEYEAR
	} else if productId == "com.hellotalk.g1y" {
		productCode = PRODUCT_GIFT_ONEYEAR
	} else if productId == "com.hellotalk.1morelang" ||
		productId == "com.hellotalk.1moreLang2" {
		productCode = PRODUCT_ONE_MULITLANG
	} else if productId == "com.hellotalk.yearauto" {
		productCode = PRODUCT_YEAR_AUTO
	} else if productId == "com.hellotalk.oneyearsubscriptionplan" ||
		productId == "com.hellotalk.oneyearsubscriptionplan2" ||
		productId == "com.hellotalk.yearauto2" {
		productCode = PRODUCT_ONE_YEAR_SUB_PLAN
	} else if productId == "com.hellotalk.lifetime" {
		productCode = PRODUCT_LIFT_TIME
	} else if productId == "com.hellotalk.lifetimeplan" {
		productCode = PRODUCT_LIFE_TIME_SUB_PLAN
	} else if productId == "com.hellotalk.onemonthsubscription" ||
		productId == "com.hellotalk.onemonthsubscription2" ||
		productId == "com.hellotalk.monthauto_freetrial" {
		productCode = PRODUCT_ONE_MONTH_SUB
	} else if productId == "com.hellotalk.oneyearsubscription" ||
		productId == "com.hellotalk.oneyearsubscription2" ||
		productId == "com.hellotalk.yearauto2_freetrial" {
		productCode = PRODUCT_ONE_YEAR_SUB
	} else if productId == "com.hellotalk.onemonthauto.b" ||
		productId == "com.hellotalk.super1monthauto" {
		productCode = PRODUCT_ONE_MONTH_AUTO_B
	} else if productId == "com.hellotalk.onemonthauto.c" {
		productCode = PRODUCT_ONE_MONTH_AUTO_C
	} else if productId == "com.hellotalk.yearauto.b" ||
		productId == "com.hellotalk.super1yearauto" {
		productCode = PRODUCT_YEAR_AUTO_B
	} else if productId == "com.hellotalk.yearauto.c" {
		productCode = PRODUCT_YEAR_AUTO_C
	} else if productId == "com.hellotalk.lifetime.b" ||
		productId == "com.hellotalk.super1lifetime" {
		productCode = PRODUCT_LIFE_TIME_B
	} else if productId == "com.hellotalk.lifetime.c" {
		productCode = PRODUCT_LIFE_TIME_C
	}
	return productCode
}

func CheckAutoRenewApple(userId uint32, receiptData []byte, currency, payMoney string) (orderType ht_purchase.ORDER_TYPE, utilTimeStamp, purchasePeriod uint64, outTransactionId string, err error) {
	if userId == 0 || len(receiptData) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("CheckAutoRenewApple userId=%v receipts=%v currency=%s payMoney=%s", userId, receiptData, currency, payMoney)
		return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
	}
	// base64Str := base64.StdEncoding.EncodeToString(receiptData)
	req := appstore.IAPRequest{
		ReceiptData: string(receiptData),
		Password:    PURCHASE_PWD,
	}
	resp := &appstore.IAPResponse{}
	err = appstoreClient.Verify(req, resp)
	if err != nil {
		infoLog.Printf("CheckAutoRenewApple userId=%v receipts=%v currency=%s payMoney=%s err=%s", userId, receiptData, currency, payMoney, err)
		return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
	}
	infoLog.Printf("CheckAutoRenewApple resp=%#v", resp)
	// copy the transaction
	receipt := resp.Receipt
	latestReceiptInfo := resp.LatestReceiptInfo
	if resp.Status == ExpireReceiptCode {
		latestReceiptInfo = resp.LatestExpireReceiptInfo
	}
	outTransactionId = string(latestReceiptInfo.TransactionID)
	verifyRspStr, err := GenAppStoreAutoRenewVerifyRspStr(resp)
	if err != nil {
		attr := "gopur/appstore_json_rsp_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("CheckAutoRenewApple GenAppStoreAutoRenewVerifyRspStr failed rsp=%#v err=%s", resp, err)
	}
	if string(latestReceiptInfo.Bid) != util.HelloTalkBid ||
		(string(latestReceiptInfo.ProductID) != util.ProductMonthAuto &&
			string(latestReceiptInfo.ProductID) != util.ProductYearAuto &&
			string(latestReceiptInfo.ProductID) != util.ProductOneMonthSubPlan &&
			string(latestReceiptInfo.ProductID) != util.ProductOneYearSubPlan &&
			string(latestReceiptInfo.ProductID) != util.ProductOneYearAuto2 &&
			string(latestReceiptInfo.ProductID) != util.ProductOneMonthSub &&
			string(latestReceiptInfo.ProductID) != util.ProductOnMonthFreeTrial &&
			string(latestReceiptInfo.ProductID) != util.ProductOneYearSub &&
			string(latestReceiptInfo.ProductID) != util.ProductOneYearFreeTrial &&
			string(latestReceiptInfo.ProductID) != util.ProductOneMonthAutoB &&
			string(latestReceiptInfo.ProductID) != util.ProductOneMonthAuto &&
			string(latestReceiptInfo.ProductID) != util.ProductSuperOneMonthAuto &&
			string(latestReceiptInfo.ProductID) != util.ProductOneMonthAutoC &&
			string(latestReceiptInfo.ProductID) != util.ProductOneYearAutoB &&
			string(latestReceiptInfo.ProductID) != util.ProductSuperOneYearAuto &&
			string(latestReceiptInfo.ProductID) != util.ProductOneYearAutoC) {
		// add static
		attr := "gopur/appstore_faker_order"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("CheckAutoRenewApple faker order uid=%v bid=%s pid=%s", userId, latestReceiptInfo.Bid, latestReceiptInfo.ProductID)
		orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
		err = DbUtil.WriteAutoRenewRecordApple(
			userId,
			E_STATE_INVALID,
			0,
			latestReceiptInfo.TransactionID,
			latestReceiptInfo.ProductID,
			latestReceiptInfo.OriginalPurchaseDate.OriginalPurchaseDate,
			latestReceiptInfo.OriginalTransactionID,
			latestReceiptInfo.PurchaseDate.PurchaseDate,
			latestReceiptInfo.PurchaseDateMS,
			latestReceiptInfo.ExpiresDateFormatted,
			latestReceiptInfo.ExpiresDate.ExpiresDate,
			resp.LatestReceipt,
			verifyRspStr,
			currency,
			payMoney)
		if err != nil {
			attr := "gopur/appstore_write_auto_renew_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("CheckAutoRenewApple WriteAutoRenewRecordApple failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s err=%s",
				userId,
				resp.Status,
				latestReceiptInfo.TransactionID,
				latestReceiptInfo.PurchaseDate,
				err)
		}
		return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
	}

	// 判断验证结果
	if resp.Status == ExpireReceiptCode { // 续订过期
		orderType = ht_purchase.ORDER_TYPE_E_EXPIRED_ORDER
		err = DbUtil.WriteAutoRenewRecordApple(
			userId,
			E_STATE_EXPIRED,
			0,
			latestReceiptInfo.TransactionID,
			latestReceiptInfo.ProductID,
			latestReceiptInfo.OriginalPurchaseDate.OriginalPurchaseDate,
			latestReceiptInfo.OriginalTransactionID,
			latestReceiptInfo.PurchaseDate.PurchaseDate,
			latestReceiptInfo.PurchaseDateMS,
			latestReceiptInfo.ExpiresDateFormatted,
			latestReceiptInfo.ExpiresDate.ExpiresDate,
			resp.LatestReceipt,
			verifyRspStr,
			currency,
			payMoney)
		if err != nil {
			mysqlerr, ok := err.(*mysql.MySQLError)
			if ok && mysqlerr.Number == 1062 {
				infoLog.Printf("CheckAutoRenewApple WriteAutoRenewRecordApple deuplicate order uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					userId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
			} else {
				attr := "gopur/appstore_write_auto_renew_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("CheckAutoRenewApple WriteAutoRenewRecordApple failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					userId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
				return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
			}
		}
		return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
	} else if resp.Status == VerifySuccessCode {
		receiptExpireTs, err := strconv.ParseUint(receipt.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("CheckAutoRenewApple strconv.ParseUint receipt.ExpiresDate=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				receipt.ExpiresDate,
				userId,
				receiptData,
				currency,
				payMoney,
				err)
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
		}

		latestExpireTs, err := strconv.ParseUint(latestReceiptInfo.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("CheckAutoRenewApple strconv.ParseUint latestReceiptInfo.ExpiresDate=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				latestReceiptInfo.ExpiresDate,
				userId,
				receiptData,
				currency,
				payMoney,
				err)
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
		}

		latestPurchTs, err := strconv.ParseUint(latestReceiptInfo.PurchaseDateMS, 10, 64)
		if err != nil {
			infoLog.Printf("CheckAutoRenewApple strconv.ParseUint latestReceiptInfo.PurchaseDateMS=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				latestReceiptInfo.PurchaseDateMS,
				userId,
				receiptData,
				currency,
				payMoney,
				err)
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
		}
		if receiptExpireTs == latestExpireTs {
			// auto renew not change
			orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
			utilTimeStamp = latestExpireTs / 1000 // chage ms to second
			purchasePeriod = (latestExpireTs - latestPurchTs) / 1000
			infoLog.Printf("check Renew Receipt is not change. uid=%v utilTimeStamp=%v purchasePeriod=%v expireDate=%s",
				userId,
				utilTimeStamp,
				purchasePeriod,
				latestReceiptInfo.ExpiresDateFormatted)
		} else {
			orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
			utilTimeStamp = latestExpireTs / 1000 // change ms to second
			purchasePeriod = (latestExpireTs - latestPurchTs) / 1000
			infoLog.Printf("Renew is successfully. uid=%v utilTimeStamp=%v purchasePeriod=%v expireDate=%s",
				userId,
				utilTimeStamp,
				purchasePeriod,
				latestReceiptInfo.ExpiresDateFormatted)
		}
		err = DbUtil.WriteAutoRenewRecordApple(
			userId,
			E_STATE_SUCC,
			0,
			latestReceiptInfo.TransactionID,
			latestReceiptInfo.ProductID,
			latestReceiptInfo.OriginalPurchaseDate.OriginalPurchaseDate,
			latestReceiptInfo.OriginalTransactionID,
			latestReceiptInfo.PurchaseDate.PurchaseDate,
			latestReceiptInfo.PurchaseDateMS,
			latestReceiptInfo.ExpiresDateFormatted,
			latestReceiptInfo.ExpiresDate.ExpiresDate,
			resp.LatestReceipt,
			verifyRspStr,
			currency,
			payMoney)
		if err != nil {
			mysqlerr, ok := err.(*mysql.MySQLError)
			if ok && mysqlerr.Number == 1062 {
				orderType = ht_purchase.ORDER_TYPE_E_REPEAT_ORDER
				infoLog.Printf("CheckAutoRenewApple Repeat AutoRenew deuplicate order uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					userId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
				lastUid, err := DbUtil.AppleStoreGetUserIdByRepeateCommitOrderId(string(latestReceiptInfo.TransactionID))
				if err == nil {
					if lastUid != userId {
						orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
						infoLog.Printf("CheckAutoRenewApple purchase uid=%v lastUid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s",
							userId,
							lastUid,
							resp.Status,
							latestReceiptInfo.TransactionID,
							latestReceiptInfo.PurchaseDate,
							verifyRspStr)
					}
					return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
				} else {
					infoLog.Printf("CheckAutoRenewApple DbUtil.AppleStoreGetUserIdByRepeateCommitOrderId uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
						userId,
						resp.Status,
						latestReceiptInfo.TransactionID,
						latestReceiptInfo.PurchaseDate,
						verifyRspStr,
						err)
					return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
				}
			} else {
				attr := "gopur/appstore_write_auto_renew_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("CheckAutoRenewApple WriteAutoRenewRecordApple failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					userId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
				return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
			}
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
		} else {
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
		}
	}
	return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
}

func GenAppStoreAutoRenewVerifyRspStr(rsp *appstore.IAPResponse) (verifyRspStr string, err error) {
	if rsp == nil {
		err = ErrInvalidParam
		return verifyRspStr, err
	}
	rspReceipt := rsp.Receipt

	receipt := simplejson.New()
	receipt.Set("expires_date_formatted", rspReceipt.ExpiresDateFormatted)
	receipt.Set("original_purchase_date_pst", rspReceipt.OriginalPurchaseDatePST)
	receipt.Set("unique_identifier", rspReceipt.UniqueIdentifier)
	receipt.Set("original_transaction_id", rspReceipt.OriginalTransactionID)
	receipt.Set("expires_date", rspReceipt.ExpiresDate)
	receipt.Set("app_item_id", rspReceipt.AppItemID)
	receipt.Set("transaction_id", rspReceipt.TransactionID)
	receipt.Set("quantity", rspReceipt.Quantity)
	receipt.Set("expires_date_formatted_pst", rspReceipt.ExpiresDateFormattedPST)
	receipt.Set("product_id", rspReceipt.ProductID)
	receipt.Set("bvrs", rspReceipt.Bvrs)
	receipt.Set("unique_vendor_identifier", rspReceipt.UniqueVendorIdentifier)
	receipt.Set("web_order_line_item_id", rspReceipt.WebOrderLineItemID)
	receipt.Set("original_purchase_date_ms", rspReceipt.OriginalPurchaseDateMS)
	receipt.Set("version_external_identifier", rspReceipt.VersionExternalIdentifier)
	receipt.Set("bid", rspReceipt.Bid)
	receipt.Set("purchase_date_ms", rspReceipt.PurchaseDateMS)
	receipt.Set("purchase_date", rspReceipt.PurchaseDate)
	receipt.Set("purchase_date_pst", rspReceipt.PurchaseDatePST)
	receipt.Set("original_purchase_date", rspReceipt.OriginalPurchaseDate)
	receipt.Set("item_id", rspReceipt.ItemId)

	rspJsonObj := simplejson.New()
	rspJsonObj.Set("receipt", receipt)
	rspJsonObj.Set("status", rsp.Status)
	rspJsonObj.Set("latest_receipt", rsp.LatestReceipt)

	latestReceiptInfo := simplejson.New()
	if rsp.Status == ExpireReceiptCode {
		rspLatestReceiptInfo := rsp.LatestExpireReceiptInfo
		latestReceiptInfo.Set("original_purchase_date_pst", rspLatestReceiptInfo.OriginalPurchaseDatePST)
		latestReceiptInfo.Set("unique_identifier", rspLatestReceiptInfo.UniqueIdentifier)
		latestReceiptInfo.Set("original_transaction_id", rspLatestReceiptInfo.OriginalTransactionID)
		latestReceiptInfo.Set("expires_date", rspLatestReceiptInfo.ExpiresDate)
		latestReceiptInfo.Set("app_item_id", rspLatestReceiptInfo.AppItemID)
		latestReceiptInfo.Set("transaction_id", rspLatestReceiptInfo.TransactionID)
		latestReceiptInfo.Set("quantity", rspLatestReceiptInfo.Quantity)
		latestReceiptInfo.Set("product_id", rspLatestReceiptInfo.ProductID)
		latestReceiptInfo.Set("bvrs", rspLatestReceiptInfo.Bvrs)
		latestReceiptInfo.Set("bid", rspLatestReceiptInfo.Bid)
		latestReceiptInfo.Set("unique_vendor_identifier", rspLatestReceiptInfo.UniqueVendorIdentifier)
		latestReceiptInfo.Set("web_order_line_item_id", rspLatestReceiptInfo.WebOrderLineItemID)
		latestReceiptInfo.Set("original_purchase_date_ms", rspLatestReceiptInfo.OriginalPurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted", rspLatestReceiptInfo.ExpiresDateFormatted)
		latestReceiptInfo.Set("purchase_date", rspLatestReceiptInfo.PurchaseDate)
		latestReceiptInfo.Set("purchase_date_ms", rspLatestReceiptInfo.PurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted_pst", rspLatestReceiptInfo.ExpiresDateFormattedPST)
		latestReceiptInfo.Set("purchase_date_pst", rspLatestReceiptInfo.PurchaseDatePST)
		latestReceiptInfo.Set("original_purchase_date", rspLatestReceiptInfo.OriginalPurchaseDate)
		latestReceiptInfo.Set("item_id", rspLatestReceiptInfo.ItemId)
		rspJsonObj.Set("latest_expired_receipt_info", latestReceiptInfo)
	} else {
		rspLatestReceiptInfo := rsp.LatestReceiptInfo
		latestReceiptInfo.Set("original_purchase_date_pst", rspLatestReceiptInfo.OriginalPurchaseDatePST)
		latestReceiptInfo.Set("unique_identifier", rspLatestReceiptInfo.UniqueIdentifier)
		latestReceiptInfo.Set("original_transaction_id", rspLatestReceiptInfo.OriginalTransactionID)
		latestReceiptInfo.Set("expires_date", rspLatestReceiptInfo.ExpiresDate)
		latestReceiptInfo.Set("app_item_id", rspLatestReceiptInfo.AppItemID)
		latestReceiptInfo.Set("transaction_id", rspLatestReceiptInfo.TransactionID)
		latestReceiptInfo.Set("quantity", rspLatestReceiptInfo.Quantity)
		latestReceiptInfo.Set("product_id", rspLatestReceiptInfo.ProductID)
		latestReceiptInfo.Set("bvrs", rspLatestReceiptInfo.Bvrs)
		latestReceiptInfo.Set("bid", rspLatestReceiptInfo.Bid)
		latestReceiptInfo.Set("unique_vendor_identifier", rspLatestReceiptInfo.UniqueVendorIdentifier)
		latestReceiptInfo.Set("web_order_line_item_id", rspLatestReceiptInfo.WebOrderLineItemID)
		latestReceiptInfo.Set("original_purchase_date_ms", rspLatestReceiptInfo.OriginalPurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted", rspLatestReceiptInfo.ExpiresDateFormatted)
		latestReceiptInfo.Set("purchase_date", rspLatestReceiptInfo.PurchaseDate)
		latestReceiptInfo.Set("purchase_date_ms", rspLatestReceiptInfo.PurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted_pst", rspLatestReceiptInfo.ExpiresDateFormattedPST)
		latestReceiptInfo.Set("purchase_date_pst", rspLatestReceiptInfo.PurchaseDatePST)
		latestReceiptInfo.Set("original_purchase_date", rspLatestReceiptInfo.OriginalPurchaseDate)
		latestReceiptInfo.Set("item_id", rspLatestReceiptInfo.ItemId)
		rspJsonObj.Set("latest_receipt_info", latestReceiptInfo)
	}

	verifyRspSlic, err := rspJsonObj.Encode()
	verifyRspStr = string(verifyRspSlic)
	return verifyRspStr, err
}

func CheckPurchaseOrderAppStore(userId, itemCode uint32, appstoreOrder *ht_purchase.AppStoreOrder) (orderType ht_purchase.ORDER_TYPE, err error) {
	if userId == 0 || len(appstoreOrder.GetReceiptData()) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("CheckPurchaseOrderAppStore userId=%v appstoreOrder=%#v", userId, *appstoreOrder)
		return orderType, err
	}
	req := appstore.IAPRequest{
		ReceiptData: string(appstoreOrder.GetReceiptData()),
		Password:    PURCHASE_PWD,
	}
	resp := &appstore.IAPResponse{}
	err = appstoreClient.Verify(req, resp)
	if err != nil {
		infoLog.Printf("CheckPurchaseOrderAppStore userId=%v appstoreOrder=%#v err=%s", userId, *appstoreOrder, err)
		return orderType, err
	}
	infoLog.Printf("CheckPurchaseOrderAppStore resp=%#v", resp)
	// copy the transaction
	var verifyStat uint32 = E_STATE_INVALID
	orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
	if resp.Status != VerifySuccessCode {
		verifyStat = E_STATE_INVALID
	}

	productId := string(appstoreOrder.GetProductId())
	receipt := resp.Receipt
	if productId == receipt.ProductID && receipt.Bid == util.HelloTalkBid {
		verifyStat = E_STATE_SUCC // 验证成功
	} else {
		verifyStat = E_STATE_INVALID // 验证失败
	}

	verifyRspStr, err := GenAppStoreVerifyRspStr(resp)
	if err != nil {
		attr := "gopur/appstore_json_rsp_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("CheckPurchaseOrderAppStore GenAppStoreVerifyRspStr failed rsp=%#v err=%s", resp, err)
	}
	err = DbUtil.WriteAppStorePurchaseRecord(userId,
		TypePurchase,
		itemCode,
		verifyStat,
		appstoreOrder.GetJailbroken(),
		string(appstoreOrder.GetProductId()),
		string(appstoreOrder.GetTransactionId()),
		string(appstoreOrder.GetCurrency()),
		string(appstoreOrder.GetPayMoney()),
		string(appstoreOrder.GetStoreArea()),
		string(appstoreOrder.GetPurchaseDate()),
		string(appstoreOrder.GetPurchaseDateGmt()),
		string(appstoreOrder.GetReceiptData()),
		verifyRspStr)
	if err != nil {
		mysqlerr, ok := err.(*mysql.MySQLError)
		if ok && mysqlerr.Number == 1062 {
			orderType = ht_purchase.ORDER_TYPE_E_REPEAT_ORDER
			infoLog.Printf("CheckPurchaseOrderAppStore WriteAppStorePurchaseRecord deuplicate order uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
				userId,
				resp.Status,
				receipt.TransactionID,
				receipt.PurchaseDate,
				verifyRspStr,
				err)
		} else {
			attr := "gopur/appstore_write_purch_order_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("CheckPurchaseOrderAppStore WriteAppStorePurchaseRecord failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
				userId,
				resp.Status,
				appstoreOrder.GetTransactionId(),
				appstoreOrder.GetPurchaseDate(),
				verifyRspStr,
				err)
			return orderType, err
		}
	} else {
		orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
	}

	if verifyStat == E_STATE_INVALID {
		orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
	}
	return orderType, nil
}

func GenAppStoreVerifyRspStr(rsp *appstore.IAPResponse) (verifyRspStr string, err error) {
	if rsp == nil {
		err = ErrInvalidParam
		return verifyRspStr, err
	}
	rspReceipt := rsp.Receipt
	receipt := simplejson.New()
	receipt.Set("original_purchase_date_pst", rspReceipt.OriginalPurchaseDatePST)
	receipt.Set("unique_identifier", rspReceipt.UniqueIdentifier)
	receipt.Set("original_transaction_id", rspReceipt.OriginalTransactionID)
	receipt.Set("app_item_id", rspReceipt.AppItemID)
	receipt.Set("transaction_id", rspReceipt.TransactionID)
	receipt.Set("quantity", rspReceipt.Quantity)
	receipt.Set("product_id", rspReceipt.ProductID)
	receipt.Set("bvrs", rspReceipt.Bvrs)
	receipt.Set("unique_vendor_identifier", rspReceipt.UniqueVendorIdentifier)
	receipt.Set("original_purchase_date_ms", rspReceipt.OriginalPurchaseDateMS)
	receipt.Set("version_external_identifier", rspReceipt.VersionExternalIdentifier)
	receipt.Set("bid", rspReceipt.Bid)
	receipt.Set("purchase_date_ms", rspReceipt.PurchaseDateMS)
	receipt.Set("purchase_date", rspReceipt.PurchaseDate)
	receipt.Set("purchase_date_pst", rspReceipt.PurchaseDatePST)
	receipt.Set("original_purchase_date", rspReceipt.OriginalPurchaseDate)
	receipt.Set("item_id", rspReceipt.ItemId)

	rspJsonObj := simplejson.New()
	rspJsonObj.Set("receipt", receipt)
	rspJsonObj.Set("status", rsp.Status)

	verifyRspSlic, err := rspJsonObj.Encode()
	verifyRspStr = string(verifyRspSlic)
	return verifyRspStr, err
}

func CheckAutoRenewGoogle(userId uint32, googleOrder *ht_purchase.GoogleOrder) (orderType ht_purchase.ORDER_TYPE, utilTimeStamp, purchasePeriod uint64, err error) {
	if userId == 0 || googleOrder == nil || len(googleOrder.GetPurchaseToken()) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("CheckAutoRenewGoogle userId=%v product_id=%s purchase_token=%s", userId, googleOrder.GetProductId(), googleOrder.GetPurchaseToken())
		return orderType, utilTimeStamp, purchasePeriod, err
	}
	infoLog.Printf("CheckAutoRenewGoogle order_id=%s purchase_token=%s", googleOrder.GetOrderId(), googleOrder.GetPurchaseToken())
	var verifyStat uint32 = E_STATE_INVALID
	orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
	autoRenewing := false
	var startTimeMillis, expiryTimeMillis int64
	var respStr string
	resp, err := playStoreClient.VerifySubscription(PACKAGE_NAME, string(googleOrder.GetProductId()), string(googleOrder.GetPurchaseToken()))
	if err != nil {
		googleerr, ok := err.(*googleapi.Error)
		if ok && googleerr.Code == 400 { //Invalid value
			infoLog.Printf("CheckAutoRenewGoogle fake order uid=%v code=%v message=%s err=%s", userId, googleerr.Code, googleerr.Message, err)
			verifyStat = E_STATE_INVALID
			respStr, err = GenGooglePlayErrStr(googleerr)
			if err != nil {
				infoLog.Printf("CheckAutoRenewGoogle GenGooglePlayErrStr return uid=%v code=%v message=%s err=%s", userId, googleerr.Code, googleerr.Message, err)
				// add static
				attr := "gopur/google_err_marshal_failed"
				libcomm.AttrAdd(attr, 1)
			}
		} else {
			// 400是假购买的返回码 如果不是400 则可能是鉴权失败等问题 立即报警
			infoLog.Printf("CheckAutoRenewGoogle userId=%v product_id=%s purchase_token=%s err=%s", userId, googleOrder.GetProductId(), googleOrder.GetPurchaseToken(), err)
			return orderType, utilTimeStamp, purchasePeriod, err
		}
	} else {
		// 订单校验成功
		infoLog.Printf("CheckAutoRenewGoogle resp=%#v", *resp)
		autoRenewing = resp.AutoRenewing
		if autoRenewing {
			verifyStat = E_STATE_SUCC
		} else {
			verifyStat = E_STATE_EXPIRED
		}
		startTimeMillis = resp.StartTimeMillis
		expiryTimeMillis = resp.ExpiryTimeMillis
		infoLog.Printf("CheckAutoRenewGoogle verify ok start=%v expier=%v autoRenewing=%v verifyStat=%v",
			startTimeMillis,
			expiryTimeMillis,
			autoRenewing,
			verifyStat)
		respSlic, err := resp.MarshalJSON()
		if err != nil {
			infoLog.Printf("CheckAutoRenewGoogle verify ok start=%v expier=%v autoRenewing=%v verifyStat=%v",
				startTimeMillis,
				expiryTimeMillis,
				autoRenewing,
				verifyStat)
			// add static
			attr := "gopur/google_resp_marshal_failed"
			libcomm.AttrAdd(attr, 1)
		}
		respStr = string(respSlic)
	}

	// store into mysql
	err = DbUtil.WriteAutoRenewRecordGoogle(userId,
		verifyStat,
		startTimeMillis,
		expiryTimeMillis,
		string(googleOrder.GetOrderId()),
		string(googleOrder.GetProductId()),
		string(googleOrder.GetPurchaseCountry()),
		string(googleOrder.GetPayMoney()),
		string(googleOrder.GetPurchaseToken()),
		respStr)
	if err != nil {
		mysqlerr, ok := err.(*mysql.MySQLError)
		if ok && mysqlerr.Number == 1062 {
			orderType = ht_purchase.ORDER_TYPE_E_REPEAT_ORDER
			infoLog.Printf("CheckAutoRenewGoogle Repeat AutoRenew deuplicate order uid=%v resp.AutoRenew=%v orderId=%s respStr=%s err=%s",
				userId,
				autoRenewing,
				googleOrder.GetOrderId(),
				respStr,
				err)
			if verifyStat == E_STATE_SUCC || verifyStat == E_STATE_EXPIRED {
				lastUid, err := DbUtil.GooglePlayGetUserIdByRepeateCommitOrderId(string(googleOrder.GetOrderId()))
				if err == nil {
					if lastUid != userId {
						orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
						infoLog.Printf("CheckAutoRenewGoogle fake order purchase uid=%v lastUid=%v resp.AutoRenew=%v orderId=%s verifyRspStr=%s",
							userId,
							lastUid,
							resp.AutoRenewing,
							googleOrder.GetOrderId(),
							respStr)
					}
				} else {
					infoLog.Printf("CheckAutoRenewGoogle DbUtil.GetUserIdByRepeateCommitOrderId uid=%v resp.AutoRenew=%v orderId=%s verifyRspStr=%s err=%s",
						userId,
						resp.AutoRenewing,
						googleOrder.GetProductId(),
						respStr,
						err)
					return orderType, utilTimeStamp, purchasePeriod, err
				}
			}
		} else {
			return orderType, utilTimeStamp, purchasePeriod, err
		}
	} else {
		orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
		utilTimeStamp = uint64(expiryTimeMillis / 1000) // change ms to second
		purchasePeriod = uint64((expiryTimeMillis - startTimeMillis) / 1000)
	}

	if verifyStat == E_STATE_INVALID {
		orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
	} else if verifyStat == E_STATE_EXPIRED {
		orderType = ht_purchase.ORDER_TYPE_E_EXPIRED_ORDER
	}
	infoLog.Printf("CheckAutoRenewGoogle userId=%v orderId=%s verifyStat=%v orderType=%v utilTimStamp=%v purchasePeriod=%v",
		userId,
		googleOrder.GetOrderId(),
		verifyStat,
		orderType,
		utilTimeStamp,
		purchasePeriod)
	return orderType, utilTimeStamp, purchasePeriod, nil
}

func GenGooglePlayErrStr(rspErr *googleapi.Error) (verifyRspStr string, err error) {
	if rspErr == nil {
		err = ErrInvalidParam
		return verifyRspStr, err
	}
	errObj := simplejson.New()
	errObj.Set("code", rspErr.Code)
	errObj.Set("message", rspErr.Message)

	var errorItem []*simplejson.Json
	for _, v := range rspErr.Errors {
		item := simplejson.New()
		item.Set("reason", v.Reason)
		item.Set("message", v.Message)
		errorItem = append(errorItem, item)
	}
	errObj.Set("errors", errorItem)
	rspJsonObj := simplejson.New()
	rspJsonObj.Set("error", errObj)
	verifyRspSlic, err := rspJsonObj.Encode()
	verifyRspStr = string(verifyRspSlic)
	return verifyRspStr, err
}

func CheckPurchaseOrderGooglePlay(userId, itemCode uint32, googleOrder *ht_purchase.GoogleOrder) (orderType ht_purchase.ORDER_TYPE, err error) {
	if userId == 0 || googleOrder == nil || len(googleOrder.GetPurchaseToken()) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("CheckPurchaseOrderGooglePlay userId=%v product_id=%s purchase_token=%s", userId, googleOrder.GetProductId(), googleOrder.GetPurchaseToken())
		return orderType, err
	}
	infoLog.Printf("CheckPurchaseOrderGooglePlay order_id=%s purchase_token=%s", googleOrder.GetOrderId(), googleOrder.GetPurchaseToken())
	var verifyStat uint32 = E_STATE_INVALID
	orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
	var respStr string
	resp, err := playStoreClient.VerifyProduct(PACKAGE_NAME, string(googleOrder.GetProductId()), string(googleOrder.GetPurchaseToken()))
	if err != nil {
		googleerr, ok := err.(*googleapi.Error)
		if ok && googleerr.Code == 400 { //Invalid value
			infoLog.Printf("CheckPurchaseOrderGooglePlay fake order uid=%v code=%v message=%s err=%s", userId, googleerr.Code, googleerr.Message, err)
			verifyStat = E_STATE_INVALID
			respStr, err = GenGooglePlayErrStr(googleerr)
			if err != nil {
				infoLog.Printf("CheckPurchaseOrderGooglePlay GenGooglePlayErrStr return uid=%v code=%v message=%s err=%s", userId, googleerr.Code, googleerr.Message, err)
				// add static
				attr := "gopur/google_err_marshal_failed"
				libcomm.AttrAdd(attr, 1)
			}
		} else {
			// 400是假购买的返回码 如果不是400 则可能是鉴权失败等问题 立即报警
			infoLog.Printf("CheckPurchaseOrderGooglePlay userId=%v product_id=%s purchase_token=%s err=%s", userId, googleOrder.GetProductId(), googleOrder.GetPurchaseToken(), err)
			return orderType, err
		}
	} else {
		// 订单校验成功
		infoLog.Printf("CheckPurchaseOrderGooglePlay resp=%#v", *resp)
		purchaseState := resp.PurchaseState
		if purchaseState == 0 {
			verifyStat = E_STATE_SUCC
		} else {
			verifyStat = E_STATE_EXPIRED
		}
		infoLog.Printf("CheckPurchaseOrderGooglePlay verify ok userId=%v purchaseStat=%v verifyStat=%v",
			userId,
			purchaseState,
			verifyStat)
		respSlic, err := resp.MarshalJSON()
		if err != nil {
			infoLog.Printf("CheckPurchaseOrderGooglePlay verify ok userId=%v purchaseStat=%v verifyStat=%v err=%s",
				userId,
				purchaseState,
				verifyStat,
				err)
			// add static
			attr := "gopur/google_resp_marshal_failed"
			libcomm.AttrAdd(attr, 1)
		}
		respStr = string(respSlic)
	}

	// store into mysql
	err = DbUtil.WriteGooglePlayPurchaseRecord(userId,
		itemCode,
		verifyStat,
		uint64(googleOrder.GetPurchaseTime()),
		string(googleOrder.GetProductId()),
		string(googleOrder.GetOrderId()),
		string(googleOrder.GetPurchaseCountry()),
		string(googleOrder.GetCurrency()),
		string(googleOrder.GetPayMoney()),
		string(googleOrder.GetPurchaseToken()),
		respStr)
	if err != nil {
		mysqlerr, ok := err.(*mysql.MySQLError)
		if ok && mysqlerr.Number == 1062 {
			orderType = ht_purchase.ORDER_TYPE_E_REPEAT_ORDER
			infoLog.Printf("CheckPurchaseOrderGooglePlay Repeat deuplicate order userId=%v orderId=%s verifyStat=%v err=%s",
				userId,
				googleOrder.GetOrderId(),
				respStr,
				err)
		} else {
			return orderType, err
		}
	} else {
		orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
	}

	if verifyStat == E_STATE_INVALID {
		orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
	}
	infoLog.Printf("CheckPurchaseOrderGooglePlay userId=%v orderId=%s verifyStat=%v orderType=%v",
		userId,
		googleOrder.GetOrderId(),
		verifyStat,
		orderType)
	return orderType, nil
}

func TimeStampToSqlTime(ts int64) (outTime string) {
	tm := time.Unix(ts, 0)
	tm = tm.UTC()
	outTime = fmt.Sprintf("%4d-%02d-%02d %02d:%02d:%02d", tm.Year(), tm.Month(), tm.Day(), tm.Hour(), tm.Minute(), tm.Second())
	return outTime
}

func CheckPurchaseOrderAlipay(userId, itemCode uint32, aliOrder *ht_purchase.AliOrder) (orderType ht_purchase.ORDER_TYPE, err error) {
	if userId == 0 || aliOrder == nil || len(aliOrder.GetSubject()) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("CheckPurchaseOrderAlipay userId=%v productId=%s", userId, aliOrder.GetSubject())
		return orderType, err
	}
	err = DbUtil.WriteAliPayPurchaseRecord(
		userId,
		itemCode,
		string(aliOrder.GetTradeNo()),
		string(aliOrder.GetPurchaseDate()),
		string(aliOrder.GetPartner()),
		string(aliOrder.GetSeller()),
		string(aliOrder.GetSubject()),
		string(aliOrder.GetSuccess()),
		string(aliOrder.GetTotalFee()))
	if err != nil {
		mysqlerr, ok := err.(*mysql.MySQLError)
		if ok && mysqlerr.Number == 1062 {
			orderType = ht_purchase.ORDER_TYPE_E_REPEAT_ORDER
			infoLog.Printf("CheckPurchaseOrderAlipay Repeat deuplicate order userId=%v itemCode=%v subject=%s err=%s",
				userId,
				itemCode,
				aliOrder.GetSubject(),
				err)
		} else {
			return orderType, err
		}
	} else {
		orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
	}

	infoLog.Printf("CheckPurchaseOrderAlipay userId=%v itemCode=%v subject=%s orderType=%v",
		userId,
		itemCode,
		aliOrder.GetSubject(),
		orderType)
	return orderType, nil
}

func CheckPurchaseOrderWeChatPay(userId, itemCode uint32, weChatOrder *ht_purchase.WeCharOrder) (orderType ht_purchase.ORDER_TYPE, err error) {
	if userId == 0 || weChatOrder == nil || len(weChatOrder.GetSubject()) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("CheckPurchaseOrderWeChatPay userId=%v productId=%s", userId, weChatOrder.GetSubject())
		return orderType, err
	}

	noceStr := uniuri.New()
	signParam := map[string]string{
		"appid":        WECHAT_PAY_APPID,
		"mch_id":       WECHAT_PAY_COMMERCIAL_TENANT,
		"out_trade_no": string(weChatOrder.GetTradeNo()),
		"nonce_str":    noceStr,
	}

	sign := core.Sign(signParam, WECHAT_PAY_SIGN_KEY, nil)

	reqParam := map[string]string{
		"appid":        WECHAT_PAY_APPID,
		"mch_id":       WECHAT_PAY_COMMERCIAL_TENANT,
		"out_trade_no": string(weChatOrder.GetTradeNo()),
		"nonce_str":    noceStr,
		"sign":         sign,
	}

	verifyStat := E_STATE_INVALID
	var respStr string
	resp, err := pay.OrderQuery(weChatClient, reqParam)
	if err != nil {
		weChaterr, ok := err.(*core.Error)
		if ok { //Invalid value
			infoLog.Printf("CheckPurchaseOrderWeChatPay fake order uid=%v code=%v message=%s err=%s", userId, weChaterr.ReturnCode, weChaterr.ReturnMsg, err)
			verifyStat = E_STATE_INVALID
			respStr = weChaterr.Error() // no need return
		} else {
			infoLog.Printf("CheckPurchaseOrderWeChatPay userId=%v productid=%s trandeNo=%s err=%s", userId, weChatOrder.GetSubject(), weChatOrder.GetTradeNo(), err)
			return orderType, err
		}
	} else {
		verifyStat = E_STATE_SUCC
		infoLog.Printf("CheckPurchaseOrderWeChatPay success userId=%v productId=%s trandeNo=%s resp=%#v", userId, weChatOrder.GetSubject(), weChatOrder.GetTradeNo(), resp)
		respStr, err = GenWeChatVerifyRspStr(&resp)
		if err != nil {
			infoLog.Printf("CheckPurchaseOrderWeChatPay userId=%v productId=%s trandeNo=%s err=%s", userId, weChatOrder.GetSubject(), weChatOrder.GetTradeNo(), err)
		}
	}

	orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER

	err = DbUtil.WriteWeChatPurchaseRecord(
		userId,
		itemCode,
		string(weChatOrder.GetTradeNo()),
		string(weChatOrder.GetPurchaseDate()),
		string(weChatOrder.GetPartner()),
		string(weChatOrder.GetSeller()),
		string(weChatOrder.GetSubject()),
		string(weChatOrder.GetSuccess()),
		string(weChatOrder.GetTotalFee()),
		respStr)
	if err != nil {
		mysqlerr, ok := err.(*mysql.MySQLError)
		if ok && mysqlerr.Number == 1062 {
			orderType = ht_purchase.ORDER_TYPE_E_REPEAT_ORDER
			infoLog.Printf("CheckPurchaseOrderWeChatPay Repeat deuplicate order userId=%v itemCode=%v subject=%s err=%s",
				userId,
				itemCode,
				weChatOrder.GetSubject(),
				err)
		} else {
			return orderType, err
		}
	} else {
		orderType = ht_purchase.ORDER_TYPE_E_NORMAL_ORDER
	}

	//重置为2的订单为假购买
	if verifyStat == E_STATE_INVALID {
		orderType = ht_purchase.ORDER_TYPE_E_FAKE_ORDER
	}
	infoLog.Printf("CheckPurchaseOrderWeChatPay userId=%v itemCode=%v subject=%s orderType=%v",
		userId,
		itemCode,
		weChatOrder.GetSubject(),
		orderType)
	return orderType, nil
}

func GenWeChatVerifyRspStr(rsp *map[string]string) (verifyRspStr string, err error) {
	if rsp == nil || len(*rsp) == 0 {
		err = ErrInvalidParam
		return verifyRspStr, err
	}
	rspJsonObj := simplejson.New()
	for i, v := range *rsp {
		rspJsonObj.Set(i, v)
	}
	verifyRspSlic, err := rspJsonObj.Encode()
	verifyRspStr = string(verifyRspSlic)
	return verifyRspStr, err
}

// 更新会员创建群成员数限制
func UpdateMucRoomMemberLimit(opUid uint32) (err error) {
	if mucApi == nil {
		err = ErrInvalidParam
		return err
	}
	reqBody := new(ht_muc.MucReqBody)
	reqBody.UpdateRoomMemberLimitReqbody = new(ht_muc.UpdateRoomMemberLimitReqBody)
	subMucReqBody := reqBody.GetUpdateRoomMemberLimitReqbody()
	subMucReqBody.OpUid = proto.Uint32(opUid)

	head := &common.HeadV3{Flag: uint8(common.CServToServ),
		Version:  common.CVerMmedia,
		CryKey:   uint8(common.CNoneKey),
		TermType: uint8(0),
		Cmd:      uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_UPDATE_ROOM_MEMBER_LIMIT),
		Seq:      0,
		From:     opUid,
		To:       opUid,
		Len:      0,
	}

	reqSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("UpdateMucRoomMemberLimit proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return err
	}

	ret, err := mucApi.SendPacket(head, reqSlice)
	if err != nil {
		infoLog.Printf("UpdateMucRoomMemberLimit mucApi.SendPacket failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return err
	}
	if ret == 0 {
		infoLog.Printf("UpdateMucRoomMemberLimit mucApi.SendPacket success uid=%v ret=%v", opUid, ret)
	} else {
		infoLog.Printf("UpdateMucRoomMemberLimit mucApi.SendPacket failed uid=%v ret=%v", opUid, ret)
	}
	return nil
}

// 根据订单信息修改会员的过期天数
func UpdateVipExpiredTime(userId, itermCode uint32, utilTimestamp uint64, purchPeriod uint64, repeatCommit bool) (vipExpiredBefore, vipExpiredAfter uint64, vipYearOut, vipTypeOut uint8, err error) {
	// 购买会员成功更新会员天数
	infoLog.Printf("UpdateVipExpiredTime userId=%v itermCode=%v utilTimestamp=%v purchPeroid=%v repeatcommit=%v",
		userId,
		itermCode,
		utilTimestamp,
		purchPeriod,
		repeatCommit)

	lastIterm, vipYear, vipType, expiredTS, err := DbUtil.GetUserVIPInfo(userId)
	if err != nil {
		infoLog.Printf("UpdateVipExpiredTime exec GetUserVIPInfo failed userUid=%v", userId)
		return 0, 0, 0, 0, err
	}
	infoLog.Printf("UpdateVipExpiredTime lastIterm=%v vipYear=%v vipType=%v expiredTS=%v", lastIterm, vipYear, vipType, expiredTS)
	vipExpiredBefore = expiredTS

	if repeatCommit == true {
		vipYearOut = vipYear
		vipExpiredAfter = expiredTS
		vipTypeOut = vipType
		return vipExpiredBefore, vipExpiredAfter, vipYearOut, vipTypeOut, nil
	}

	timeStampNow := uint64(time.Now().Unix())
	for {
		// 购买的是年会员
		if itermCode == PRODUCT_ONE_YEAR ||
			itermCode == PRODUCT_GIFT_ONEYEAR ||
			itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN || // 高价格年费会员
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C { // 年费试用会员
			vipYear = YEAR_VIP
			break
		}

		// 以前是年会员 现在还没有过期 依旧是年会员
		if vipYear == YEAR_VIP && expiredTS > timeStampNow {
			vipYear = YEAR_VIP
			break
		}

		vipYear = YEAR_NORMAL
		break
	}

	// 原来的会员是否已经过期
	// 判断会员类型 终生会员 > 自动续费会员 > 剩余天数会员
	if expiredTS >= timeStampNow { // 会员没有过期
		// 未过期的有顺序判断
		if itermCode == PRODUCT_LIFT_TIME ||
			itermCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
			itermCode == PRODUCT_LIFE_TIME_B ||
			itermCode == PRODUCT_LIFE_TIME_C ||
			vipType == E_LIFETIME_VIP {
			vipType = E_LIFETIME_VIP
		} else if itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C ||
			vipType == E_YEARAUTO_VIP {
			vipType = E_YEARAUTO_VIP
		} else if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C ||
			vipType == E_MONTHAUTO_VIP {
			vipType = E_MONTHAUTO_VIP
		} else {
			vipType = E_DAYS_VIP
		}
	} else {
		// 已经过期的以当前购买的为准
		if itermCode == PRODUCT_LIFT_TIME ||
			itermCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
			itermCode == PRODUCT_LIFE_TIME_B ||
			itermCode == PRODUCT_LIFE_TIME_C {
			vipType = E_LIFETIME_VIP
		} else if itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C {
			vipType = E_YEARAUTO_VIP
		} else if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C {
			vipType = E_MONTHAUTO_VIP
		} else {
			vipType = E_DAYS_VIP
		}
	}
	// 更改到期时间戳
	countSecond := CountPostponeSeconds(itermCode)
	infoLog.Printf("UpdateVipExpiredTime countSecond=%v purchPeriod=%v", countSecond, purchPeriod)
	if expiredTS < timeStampNow {
		// 已经过期或是第一次购买的 月费试用会员和年费试用会员都是自动续费类型
		if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C {
			// 自动续费的以续费过期时间为准
			expiredTS = utilTimestamp
		} else {
			// 其他的认为在当前时间往后延长
			expiredTS = timeStampNow + countSecond
		}
	} else {
		if itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_YEAR_SUB {
			// 对于月费试用会员和年费试用会员 将过期时间修改成 当前时间 + 订单里面的持续时间 不再写死会员的过期时间
			expiredTS += purchPeriod
		} else {
			// 还没有过期则在原来的到期时间戳上延长
			expiredTS += countSecond
		}
	}

	infoLog.Printf("UpdateVipExpiredTime uid=%v lastIterm=%v vipYear=%v vipType=%v expiredTS=%v",
		userId,
		lastIterm,
		vipYear,
		vipType,
		expiredTS)
	err = DbUtil.UpdateUserVIPInfo(userId, itermCode, vipYear, vipType, expiredTS)
	if err != nil {
		infoLog.Printf("UpdateVipExpiredTime UpdateUserVIPInfo failed lastIterm=%v vipYear=%v vipType=%v expiredTS=%v err=%s",
			lastIterm,
			vipYear,
			vipType,
			expiredTS,
			err)
	} else {
		infoLog.Printf("UpdateVipExpiredTime UpdateUserVIPInfo ok lastIterm=%v vipYear=%v vipType=%v expiredTS=%v",
			lastIterm,
			vipYear,
			vipType,
			expiredTS)
	}

	// 更新成功返回
	vipExpiredAfter = expiredTS
	vipYearOut = vipYear
	vipTypeOut = vipType
	return vipExpiredBefore, vipExpiredAfter, vipYearOut, vipTypeOut, nil
}

func CountPostponeSeconds(productCode uint32) (outSecond uint64) {
	if productCode == PRODUCT_ONE_MONTH ||
		productCode == PRODUCT_ONE_MONTHAUTO ||
		productCode == PRODUCT_GIFT_ONEMONTH ||
		productCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
		productCode == PRODUCT_ONE_MONTH_AUTO_B ||
		productCode == PRODUCT_ONE_MONTH_AUTO_C {
		outSecond = 31 * 24 * 3600
	} else if productCode == PRODUCT_THREE_MONTH ||
		productCode == PRODUCT_GIFT_THREEMONTH {
		outSecond = 92 * 24 * 3600
	} else if productCode == PRODUCT_ONE_YEAR ||
		productCode == PRODUCT_GIFT_ONEYEAR ||
		productCode == PRODUCT_YEAR_AUTO ||
		productCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
		productCode == PRODUCT_YEAR_AUTO_B ||
		productCode == PRODUCT_YEAR_AUTO_C {
		outSecond = 365 * 24 * 3600
	} else if productCode == PRODUCT_LIFT_TIME ||
		productCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
		productCode == PRODUCT_LIFE_TIME_B ||
		productCode == PRODUCT_LIFE_TIME_C {
		// 终生会员做50年处理
		outSecond = 50 * 365 * 24 * 3600
	} else if productCode == PRODUCT_ONE_MONTH_SUB {
		outSecond = 7 * 24 * 3600 // 月会员试用 返回的天数UpdateVipExpiredTime没有使用
	} else if productCode == PRODUCT_ONE_YEAR_SUB {
		outSecond = 31 * 24 * 3600 // 年会员试用 返回的天数UpdateVipExpiredTime没有使用
	}
	return outSecond
}

func WritePurchaseHistoryRelabile(historyOrder *util.PurchaseHistory) (err error) {
	tryCount := 0
	for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
		err = DbUtil.WritePurchaseHistory(historyOrder)
		if err != nil {
			infoLog.Printf("WritePurchaseHistoryRelabile DbUtil.WritePurchaseHistory failed uid=%v itemCode=%v err=%v", historyOrder.UserId, historyOrder.ItemCode, err)
			time.Sleep(5 * time.Second) // 休眠5秒再重试
			continue
		}
		break
	}
	if tryCount == 0 {
		attr := "gopur/wirte_purchase_history_order_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("WritePurchaseHistoryRelabile DbUtil.WritePurchaseHistory failed uid=%v   itemCode=%v tryCount=%v", historyOrder.UserId, historyOrder.ItemCode, tryCount)
	}
	return err
}

func NotifyPurchaseInformation(toUid uint32, termianlType uint8, itemCode, platform, verifyState, clientOrServer, fromId uint32, orderId string, expireTs uint64) (err error) {
	// build notification
	dataObj := simplejson.New()
	dataObj.Set("item", itemCode)
	dataObj.Set("platform", platform)
	dataObj.Set("verify_success", verifyState)
	dataObj.Set("order_id", orderId)
	dataObj.Set("client_or_server", clientOrServer)
	dataObj.Set("purch_uid", fromId)
	dataObj.Set("expire_ts", expireTs)

	infoObj := simplejson.New()
	infoObj.Set("type", RB_PURCHASE_NOTIFY)
	infoObj.Set("data", dataObj)

	rootObject := BuildCommonObject(RABBIT_SUBMIT, toUid, termianlType)
	rootObject.Set("info", infoObj)
	strSlice, err := rootObject.MarshalJSON()
	if err != nil {
		infoLog.Printf("NotifyPurchaseInformation simpleJson MarshalJSON failed toUid=%v terminaltype=%v err=%s",
			toUid,
			termianlType,
			err)
		return err
	}
	infoLog.Printf("NotifyPurchaseInformation publish slice=%s", strSlice)
	err = RabbitMQPublish(rabbitMQUrl, EXCHANGENAME, "direct", ROUTINGKEY, string(strSlice), true)
	if err != nil {
		infoLog.Printf("NotifyPurchaseInformation publish strSlice=%s failed", strSlice)
	} else {
		infoLog.Printf("NotifyPurchaseInformation publish strSlice=%s success", strSlice)
	}
	return err
}

func BuildCommonObject(cmd string, uid uint32, terminalType uint8) (rootObject *simplejson.Json) {
	rootObject = simplejson.New()
	rootObject.Set("cmd", cmd)
	rootObject.Set("user_id", uid)
	rootObject.Set("os_type", terminalType)
	rootObject.Set("server_ts", time.Now().Unix())
	return rootObject
}

func RabbitMQPublish(amqpURI, exchange, exchangeType, routingKey, body string, reliable bool) error {

	// This function dials, connects, declares, publishes, and tears down,
	// all in one go. In a real service, you probably want to maintain a
	// long-lived connection as state, and publish against that.

	// infoLog.Printf("dialing %q", amqpURI)
	connection, err := amqp.Dial(amqpURI)
	if err != nil {
		return fmt.Errorf("Dial: %s", err)
	}
	defer connection.Close()

	channel, err := connection.Channel()
	if err != nil {
		return fmt.Errorf("Channel: %s", err)
	}

	//Printf("got Channel, declaring %q Exchange (%q)", exchangeType, exchange)
	if err := channel.ExchangeDeclare(
		exchange,     // name
		exchangeType, // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return fmt.Errorf("Exchange Declare: %s", err)
	}

	// Reliable publisher confirms require confirm.select support from the
	// connection.
	if reliable {
		infoLog.Printf("enabling publishing confirms.")
		if err := channel.Confirm(false); err != nil {
			return fmt.Errorf("Channel could not be put into confirm mode: %s", err)
		}

		confirms := channel.NotifyPublish(make(chan amqp.Confirmation, 1))

		defer confirmOne(confirms)
	}

	// infoLog.Printf("declared Exchange, publishing %dB body (%q)", len(body), body)
	if err = channel.Publish(
		exchange,   // publish to an exchange
		routingKey, // routing to 0 or more queues
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "text/plain",
			ContentEncoding: "",
			Body:            []byte(body),
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
			// a bunch of application/implementation-specific fields
		},
	); err != nil {
		return fmt.Errorf("Exchange Publish: %s", err)
	}

	return nil
}

// One would typically keep a channel of publishings, a sequence number, and a
// set of unacknowledged sequence numbers and loop until the publishing channel
// is closed.
func confirmOne(confirms <-chan amqp.Confirmation) {
	infoLog.Printf("waiting for confirmation of one publishing")

	if confirmed := <-confirms; confirmed.Ack {
		infoLog.Printf("confirmed delivery with delivery tag: %d", confirmed.DeliveryTag)
	} else {
		infoLog.Printf("failed delivery of delivery tag: %d", confirmed.DeliveryTag)
	}
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_purchase.PurchaseRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// read appstore config
	productEnv := cfg.Section("APPSTORE").Key("product").MustBool(true)
	appTimeOut := cfg.Section("APPSTORE").Key("time_out").MustInt(2)
	infoLog.Printf("APPSTORE product=%v time_out=%v", productEnv, appTimeOut)
	appstoreConfig = appstore.Config{
		IsProduction: productEnv,
		TimeOut:      time.Second * time.Duration(appTimeOut),
	}
	appstoreClient = appstore.NewWithConfig(appstoreConfig)

	// You need to prepare a public key for your Android app's in app billing
	// at https://console.developers.google.com.
	jsonFileName := cfg.Section("GOOGLE").Key("file_name").MustString("jsonKey.json")
	jsonKey, err = ioutil.ReadFile(jsonFileName)
	if err != nil {
		log.Fatal(err)
		return
	}
	playStoreClient, err = playstore.New(jsonKey)
	if err != nil {
		log.Fatal(err)
		return
	}
	googleTimeOut := cfg.Section("GOOGLE").Key("time_out").MustInt(2)
	infoLog.Printf("GOOGLE jsonFileName=%s time_out=%v", jsonFileName, googleTimeOut)
	playstore.SetTimeout(time.Second * time.Duration(googleTimeOut))

	// weChat Client
	weChatTimeOut := cfg.Section("WECHAT").Key("time_out").MustInt(2)
	httpClient := &http.Client{
		Timeout: time.Second * time.Duration(weChatTimeOut),
	}
	weChatClient = core.NewClient(WECHAT_PAY_APPID, WECHAT_PAY_COMMERCIAL_TENANT, WECHAT_PAY_SIGN_KEY, httpClient)

	// 读取RabbitMQ的配置
	rabbitMQUrl = cfg.Section("RABBITMQ").Key("mq_url").MustString("amqp://pushServer:123456@10.243.134.240:5672/pushHost")
	infoLog.Printf("RabbitMQ URL=%s", rabbitMQUrl)

	// 读取muc 配置
	mucIp := cfg.Section("MUC").Key("muc_ip").MustString("127.0.0.1")
	mucPort := cfg.Section("MUC").Key("muc_port").MustString("0")
	mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	infoLog.Printf("muc server ip=%v port=%v connLimit=%v", mucIp, mucPort, mucConnLimit)
	mucApi = common.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, mucConnLimit)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	DbUtil = util.NewDbUtil(db, infoLog)
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
