// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"log"
	"strings"
	// "time"

	_ "github.com/go-sql-driver/mysql"
)

// Error type
var (
	ErrNilDbObject = errors.New("not set  object current is nil")
	ErrDbParam     = errors.New("err param error")
)

const (
	HelloTalkBid                  = "com.helloTalk.helloTalk"
	ProductMonthAuto              = "com.hellotalk.monthauto"
	ProductOneMonthAuto1          = "com.hellotalk.onemonth1auto"
	ProductOneMonthAutoRenew      = "com.hellotalk.1monthautorenew"
	ProductOneMonthAuto           = "com.hellotalk.onemonthauto"
	ProductYearAuto               = "com.hellotalk.yearauto"
	ProductOneMonthSubPlan        = "com.hellotalk.onemonthsubscriptionplan"
	ProductOneMonthSubPlan2       = "com.hellotalk.com.hellotalk.onemonthsubscriptionplan2"
	ProductOneYearSubPlan         = "com.hellotalk.oneyearsubscriptionplan"
	ProductOneYearSubPlan2        = "com.hellotalk.oneyearsubscriptionplan2"
	ProductOneYearAuto2           = "com.hellotalk.yearauto2"
	ProductOneMonthSub            = "com.hellotalk.onemonthsubscription"
	ProductOneMonthSub2           = "com.hellotalk.onemonthsubscription2"
	ProductOnMonthFreeTrial       = "com.hellotalk.monthauto_freetrial"
	ProductOneYearSub             = "com.hellotalk.oneyearsubscription"
	ProductOneYearSub2            = "com.hellotalk.oneyearsubscription2"
	ProductOneYearFreeTrial       = "com.hellotalk.yearauto2_freetrial"
	ProductOneMonthAutoB          = "com.hellotalk.onemonthauto.b"
	ProductSuperOneMonthAuto      = "com.hellotalk.super1monthauto"
	ProductOneMonthAutoC          = "com.hellotalk.onemonthauto.c"
	ProductOneYearAutoB           = "com.hellotalk.yearauto.b"
	ProductSuperOneYearAuto       = "com.hellotalk.super1yearauto"
	ProductOneYearAutoC           = "com.hellotalk.yearauto.c"
	ProductOpenEnglishOneYearAuto = "com.hellotalk.openenglish1year"
)

const (
	// 与数据库中STATE的定义一致
	E_STATE_INIT    = 0 // 未验证
	E_STATE_SUCC    = 1 // 验证成功
	E_STATE_INVALID = 2 // 验证失败
	E_STATE_EXPIRED = 3 // 验证过期
)

const (
	PAY_TYPE_APPSTORE   = 1
	PAY_TYPE_GOOGLEPLAY = 2
	PAY_TYPE_PAYPAL     = 3
	PAY_TYPE_ALIPAY     = 4
	PAY_TYPE_WECHATPAY  = 5
)

type AppleRenewItem struct {
	UserId          uint32
	TransactionId   string
	ProductId       string
	LatestReceipt   string
	ExpireTimeStamp uint64
	Status          uint32
	Currency        string
	PayMoney        string
}

type PurchaseHistory struct {
	OrderId        string
	ItemCode       uint32
	ProductId      string
	GiftDays       uint32
	UserId         uint32
	ToId           uint32
	Currency       string
	PayMoney       string
	PayType        uint8
	BuyTime        string
	UtilBefore     uint64
	UtilAfter      uint64
	ClientOrServer uint8
	RetryTime      uint8
}

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

// verify_status '0-未验证 1-成功 2-失败 3-续订过期'
func (this *DbUtil) GetAppleAutoRenewTransactionId() (originTransaction []string, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return nil, err
	}

	// 查询正在续费中的订单
	rows, err := this.db.Query("SELECT DISTINCT origin_id FROM HT_AUTORENEW_PRODUCT_RECORD_APPLE WHERE verify_status=1;")
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var item string
		if err := rows.Scan(&item); err != nil {
			this.infoLog.Printf("GetAppleAutoRenewTransactionId HT_AUTORENEW_PRODUCT_RECORD_APPLE rows.Scan failed err=%s", err)
			continue
		}
		originTransaction = append(originTransaction, item)
	}
	return originTransaction, nil
}

func (this *DbUtil) GetAppleAutoRenewBillingOrder() (originTransaction []string, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return nil, err
	}

	rows, err := this.db.Query("SELECT DISTINCT origin_id FROM HT_AUTORENEW_PRODUCT_RECORD_APPLE WHERE verify_status = 3;")
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var item string
		if err := rows.Scan(&item); err != nil {
			this.infoLog.Printf("GetAppleAutoRenewBillingOrder HT_AUTORENEW_PRODUCT_RECORD_APPLE rows.Scan failed err=%s", err)
			continue
		}
		originTransaction = append(originTransaction, item)
	}
	return originTransaction, nil
}

func (this *DbUtil) GetAppleTransactionByOriginId(originId string) (renewItem AppleRenewItem, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return renewItem, err
	}
	var storeTransactionId, storeProductId, storeLatestReceipt, storeCurrency, storePayMoney sql.NullString
	var storeUserId, storeExpredTS, storeStatus sql.NullInt64
	err = this.db.QueryRow("SELECT transaction_id, user_id, product_id, expires_ts, receipt_data, verify_status, currency, pay_money FROM HT_AUTORENEW_PRODUCT_RECORD_APPLE WHERE origin_id = ? ORDER BY purchdate_ts DESC;",
		originId).Scan(&storeTransactionId, &storeUserId, &storeProductId, &storeExpredTS, &storeLatestReceipt,
		&storeStatus, &storeCurrency, &storePayMoney)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetAppleTransactionByOriginId not found originId=%s in HT_AUTORENEW_PRODUCT_RECORD_APPLE err=%s", originId, err)
		return renewItem, err
	case err != nil:
		this.infoLog.Printf("GetAppleTransactionByOriginId exec HT_AUTORENEW_PRODUCT_RECORD_APPLE failed originId=%s, err=%s", originId, err)
		return renewItem, err
	default:
	}
	if storeTransactionId.Valid {
		renewItem.TransactionId = storeTransactionId.String
	}
	if storeProductId.Valid {
		renewItem.ProductId = storeProductId.String
	}
	if storeLatestReceipt.Valid {
		renewItem.LatestReceipt = storeLatestReceipt.String
	}
	if storeCurrency.Valid {
		renewItem.Currency = storeCurrency.String
	}
	if storePayMoney.Valid {
		renewItem.PayMoney = storePayMoney.String
	}
	if storeUserId.Valid {
		renewItem.UserId = uint32(storeUserId.Int64)
	}
	if storeExpredTS.Valid {
		renewItem.ExpireTimeStamp = uint64(storeExpredTS.Int64)
	}
	if storeStatus.Valid {
		renewItem.Status = uint32(storeStatus.Int64)
	}
	return renewItem, nil
}

func (this *DbUtil) WriteAutoRenewRecordApple(userId, itemCode, status, clientOrServer uint32,
	transactionId, productId, originPurchDate, originTransactionId, purchDateGMT, purchTs, expireDate, expireTs, receiptData, verifyRsp, currency, payMoney string) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}
	_, err = this.db.Exec("insert into HT_AUTORENEW_PRODUCT_RECORD_APPLE set transaction_id=?, user_id=?, product_id=?, origin_id=?, origin_date=?, purchdate_gmt=?, purchdate_ts=?, expire_gmt=?, expires_ts=?, receipt_data=?, verify_data=?, verify_status=?, client_or_srv=?, update_time=UTC_TIMESTAMP(), currency=?, pay_money=?, item_code=?;",
		transactionId,
		userId,
		productId,
		originTransactionId,
		originPurchDate,
		purchDateGMT,
		purchTs,
		expireDate,
		expireTs,
		receiptData,
		verifyRsp,
		status,
		clientOrServer,
		currency,
		payMoney,
		itemCode)
	if err != nil {
		this.infoLog.Printf("WriteAutoRenewRecordApple insert faield transactionId=%s userid=%v productId=%v verifyRsp=%s err=%v",
			transactionId,
			userId,
			productId,
			verifyRsp,
			err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) GetUserPurchaseInfo(userId uint32) (lastIterm, expiredTS uint64, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return lastIterm, expiredTS, err
	}
	if userId == 0 {
		err = ErrDbParam
		return lastIterm, expiredTS, err
	}

	err = this.db.QueryRow("select expire_time, last_purchase_item FROM HT_PURCHASE_PRODUCT where user_id = ?;", userId).Scan(&expiredTS, &lastIterm)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetUserPurchaseInfo not found uid=%v", userId)
		break
	case err != nil:
		this.infoLog.Printf("GetUserPurchaseInfo exec failed uid=%v, err=%s", userId, err)
		break
	default:
		this.infoLog.Printf("GetUserPurchaseInfo uid=%v expiredTs=%v lastIterm=%v", userId, expiredTS, lastIterm)
	}
	return lastIterm, expiredTS, nil
}

func (this *DbUtil) UpdateUserPurchaseInfo(userId, itermCode uint32, expiredTimestamp uint64) (err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return err
	}

	if userId == 0 || itermCode == 0 {
		return ErrDbParam
	}

	_, err = this.db.Exec("insert into HT_PURCHASE_PRODUCT set user_id=?, create_time=UTC_TIMESTAMP(), expire_time=?, "+
		"last_purchase_item=?, update_time=UTC_TIMESTAMP() on duplicate key update "+
		"last_purchase_item=?, expire_time=?, update_time=UTC_TIMESTAMP();",
		userId,
		expiredTimestamp,
		itermCode,
		itermCode,
		expiredTimestamp)
	if err != nil {
		this.infoLog.Printf("UpdateUserPurchaseInfo insert faield userId=%v expireTimeStamp=%v itermCode=%v err=%s",
			userId,
			expiredTimestamp,
			itermCode,
			err)
		return err
	} else {
		return nil
	}
}

// 货币特殊情况处理方法： 去掉多余的小数点  欧元(欧洲一些国家的)的,变成.
// 1、欧元  EUR  4,99 €    ,表示小数点  .表示千
// 2、沙特阿拉伯里尔 SAR  ر.س.‏ 22.99   有几个小数点
// 3、秘魯索爾 PEN  S/.20.99    有几个小数点
// 4、波兰  PLN  23,99 zł  和欧元一样
// 5、丹麦 DKK   39,00 kr.  和欧元一样  还有几个小数点
// 6、埃及镑  EGP  43,99 EGP  和欧元一样
// 7、巴西雷亞爾  BRL  R$20,99  和欧元一样
// 8、摩洛哥迪拉姆 MAD  47,99 MAD   欧元一样
// 9、俄國盧布   RUB   409,00 ₽    409,00 ₽   和欧元一样
// 10、土耳其里拉  TRY    ₺12,99  和欧元一样
// 11、USD 4,90 $  6,19 US$  ??
// 12、南非蘭特 ZAR   R229,99
// 13、阿聯酋迪拉姆  AED  22,99 AED   欧元一样
// 14、瑞典克朗   SEK     65,00 kr
// 15、捷克克朗   CZK     149,99 Kč
// 16、乌克兰Hryvnia    UAH   149,99 грн.
// 17、挪威克羅鈉  NOK    kr 79,00
// 18、卡塔爾利雅  QAR   55,00 QAR
// 19、越南盾    VND   42.000 ₫    ₫ 339.000  // 处理不一样!!!!!!!!!! .代表千位符
// 注意：中国的人民币是CNY

func IsNumberChar(r rune) bool {
	if (r >= '0' && r <= '9') || (r == '.' || r == ',') {
		return false
	} else {
		return true
	}
}

func FormatCurrecyMoney(payMoney string) (formatMoney string, err error) {
	if payMoney == "" || len(payMoney) > 24 {
		err = ErrDbParam
		return formatMoney, err
	}

	trimMoney := strings.TrimFunc(payMoney, IsNumberChar)
	// 去除开头和结尾的.
	// 结尾有多个.的情况会有问题 开头有多个会去掉
	trimLeft := strings.TrimLeft(trimMoney, ".")
	trimRight := strings.TrimRight(trimLeft, ".")

	// 替换类似欧元格式中的,为. 以及去除千位符,
	// 没考虑超过1000欧元的情况  2.016,99 €
	// 也没考虑超过100万金额的情况  ₩8,007,000
	// 越南盾的情况 42.000 ₫    ₫ 339.000
	commaIndex := strings.LastIndex(trimRight, ",")
	dotIndex := strings.LastIndex(trimRight, ".")
	if commaIndex == -1 {
		// 只在没有，的金额中处理越南盾
		// 根据小数点的位置判断
		if dotIndex == -1 {
			formatMoney = trimRight
		} else if dotIndex+2 == len(trimRight)-1 {
			// 正常的小数点
			formatMoney = trimMoney
		} else if dotIndex+3 == len(trimRight)-1 {
			// 小数点代表千位符 越南盾  去除掉
			formatMoney = strings.TrimLeft(trimRight, ".")
		} else {
			formatMoney = trimRight
		}
	} else if (commaIndex + 2) == (len(trimRight) - 1) {
		formatMoney = strings.Replace(trimRight, ",", ".", -1)
	} else if (commaIndex + 3) == (len(trimRight) - 1) {
		// 去掉真正的千位符,
		formatMoney = strings.TrimLeft(trimRight, ",")
	} else {
		formatMoney = trimRight
	}
	return formatMoney, nil
}

func (this *DbUtil) WritePurchaseHistory(historyOrder *PurchaseHistory) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}
	var formatMoney string
	// 格式化	GooglePlay 提交上来的金额数据
	if historyOrder.PayType == PAY_TYPE_GOOGLEPLAY {
		formatMoney, err = FormatCurrecyMoney(historyOrder.PayMoney)
		if err != nil {
			this.infoLog.Printf("WritePurchaseHistory uid=%v itemCode=%v payMoney=%s", historyOrder.UserId, historyOrder.ItemCode, historyOrder.PayMoney)
		}
	} else {
		formatMoney = historyOrder.PayMoney
	}
	_, err = this.db.Exec("insert into HT_PURCHASE_HISTORY set ORDERID=?, ITEMCODE=?, PRODUCTID=?, GIFTDAYS=?, USERID=?, TOID=?, CURRENCY=?, PAYMONEY=?, FORMATMONEY=?, PAYTYPE=?, BUYTIME=?, UTILBEFORE=?, UTILAFTER=?, UPDATETIME=UTC_TIMESTAMP(), CLIENT_OR_SRV=?;",
		historyOrder.OrderId,
		historyOrder.ItemCode,
		historyOrder.ProductId,
		historyOrder.GiftDays,
		historyOrder.UserId,
		historyOrder.ToId,
		historyOrder.Currency,
		historyOrder.PayMoney,
		formatMoney,
		historyOrder.PayType,
		historyOrder.BuyTime,
		historyOrder.UtilBefore,
		historyOrder.UtilAfter,
		historyOrder.ClientOrServer)
	if err != nil {
		this.infoLog.Printf("WritePurchaseHistory uid=%v itemCode=%v payMoney=%s err=%s", historyOrder.UserId, historyOrder.ItemCode, historyOrder.PayMoney, err)
		return err
	} else {
		return nil
	}
}

func (this *DbUtil) UpdateAppleRenewExpiredTransaction(transactionId, verifyResponse string, verifyStat uint32) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}
	_, err = this.db.Exec("UPDATE HT_AUTORENEW_PRODUCT_RECORD_APPLE SET verify_status=?, verify_data=?, update_time=UTC_TIMESTAMP(),client_or_srv=1 WHERE transaction_id=?;",
		verifyStat,
		verifyResponse,
		transactionId)
	if err != nil {
		this.infoLog.Printf("UpdateAppleRenewExpiredTransaction verifyResponse=%s transactionId=%s  err=%s", verifyResponse, transactionId, err)
		return err
	} else {
		return nil
	}
}
