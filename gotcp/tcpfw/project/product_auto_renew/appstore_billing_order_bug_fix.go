package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net"
	"os/signal"
	"strings"
	"sync"
	"syscall"

	simplejson "github.com/bitly/go-simplejson"
	"github.com/dogenzaka/go-iap/appstore"
	"github.com/dogenzaka/go-iap/playstore"
	"github.com/golang/protobuf/proto"
	"github.com/streadway/amqp"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_thirdpay"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_user"
	"github.com/HT_GOGO/gotcp/tcpfw/project/vip_auto_renew/util"

	"os"
	"runtime"
	"strconv"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	ErrInputParam  = errors.New("invalid intput param")
	ErrFakeOrder   = errors.New("fake order")
	ErrOnlineIpErr = errors.New("err online state ip not exist")
)

var (
	infoLog             *log.Logger
	DbUtil              *util.DbUtil
	appstoreClient      appstore.Client
	jsonKey             []byte
	playStoreClient     playstore.Client
	mcApi               *common.MemcacheApi
	userCacheApi        *common.UserCacheApi
	appleCheckChan      chan util.AppleRenewItem
	closeChan           chan struct{} // close chanel
	googleCheckChan     chan util.GoogleRenewItem
	waitGroup           *sync.WaitGroup // wait for all goroutines
	rabbitMQUrl         string
	checkInterval       int
	imServerPool        map[string]*common.ImServerApiV2
	googlePendTimeStamp uint64
	validStep           int64
)

const (
	PURCHASE_PWD                 = "f2e40870103240f7b485b6f186fa141a"
	PACKAGE_NAME                 = "com.hellotalk"
	WECHAT_PAY_APPID             = "wxd061f34c48968028"
	WECHAT_PAY_COMMERCIAL_TENANT = "1233820102"
	WECHAT_PAY_SIGN_KEY          = "B3A32EF18D804B1BA5B8CA07BEBCBE3E"
)

const (
	TypePurchase = 0 //0-购买
	TypeRenew    = 1 //1-续订
	TRYTIMES     = 3
)

const (
	PaymentPending  = 0
	PaymentReceived = 1
	FreeTrial       = 2
)

const (
	DEFAULT_USER          = 200002
	ExpireReceiptCode     = 21006
	VerifySuccessCode     = 0
	AppStoreCheckDate     = 5 * 24 * 3600
	AppStoreBillingPeriod = 61 * 24 * 3600 // second App Store may attempt to renew a subscription for up to 60 days
)

const (
	YEAR_NORMAL = 0 //'0-非年会员 1-年会员'
	YEAR_VIP    = 1
)

const (
	E_NOT_VIP       = 0   // 非会员
	E_DAYS_VIP      = 1   // 按天计数的会员(旧版本购买的月、年会员)
	E_MONTHAUTO_VIP = 2   // 月自动续费的会员
	E_YEARAUTO_VIP  = 3   // 年自动续费的会员
	E_LIFETIME_VIP  = 100 // 终生会员 (50年后过期)
)

const (
	PRODUCT_ONE_MONTH = 1
	// "com.hellotalk.onemonth1"
	PRODUCT_ONE_MONTHAUTO = 2
	// "com.hellotalk.onemonthauto"
	// or GooglePlay="com.hellotalk.onemonth1auto"
	// 2016-07-16 android use "com.hellotalk.1monthautorenew"
	// 2016-08-08  iOS use "com.hellotalk.monthauto"
	PRODUCT_ONE_YEAR = 3
	// "com.hellotalk.oneyear"
	PRODUCT_THREE_MONTH = 4
	// "com.hellotalk.3months"
	PRODUCT_YEAR_AUTO = 5
	// "com.hellotalk.yearauto"
	PRODUCT_LIFT_TIME = 6
	// "com.hellotalk.lifetime"
	// product id for upgrade vip price
	PRODUCT_ONE_MONTH_SUB_PLAN = 7
	// iOS:"com.hellotalk.onemonthsubscriptionplan"
	// And:"com.hellotalk.onemonthsubscriptionplan2"
	PRODUCT_ONE_YEAR_SUB_PLAN = 8
	// iOS:"com.hellotalk.oneyearsubscriptionplan"
	// iOS:"com.hellotalk.yearauto2"
	// And:"com.hellotalk.oneyearsubscriptionplan2"
	PRODUCT_LIFE_TIME_SUB_PLAN = 9
	// "com.hellotalk.lifetimeplan"

	PRODUCT_GIFT_ONEMONTH = 11
	// "com.hellotalk.Gift1M" or GooglePlay="com.hellotalk.g1m"
	PRODUCT_GIFT_THREEMONTH = 12
	// "com.hellotalk.Gift3M" or GooglePlay="com.hellotalk.g3m"
	PRODUCT_GIFT_ONEYEAR = 13
	// "com.hellotalk.Gift1Y" or GooglePlay="com.hellotalk.g1y"
	// product id for trial membership
	PRODUCT_ONE_MONTH_SUB = 14
	// iOS:"com.hellotalk.onemonthsubscription"
	// iOS:"com.hellotalk.monthauto_freetrial"
	// And:"com.hellotalk.onemonthsubscription2"
	PRODUCT_ONE_YEAR_SUB = 15
	// iOS:"com.hellotalk.oneyearsubscription"
	// iOS:"com.hellotalk.yearauto2_freetrial"
	// And:"com.hellotalk.oneyearsubscription2"
	PRODUCT_ONE_MULITLANG = 21
	// "com.hellotalk.1moreLang"
	PRODUCT_ONE_MONTH_AUTO_B = 22
	// "com.hellotalk.onemonthauto.b"
	// iOS:"com.hellotalk.super1monthauto"
	PRODUCT_ONE_MONTH_AUTO_C = 23
	// "com.hellotalk.onemonthauto.c"
	PRODUCT_YEAR_AUTO_B = 24
	// "com.hellotalk.yearauto.b"
	// iOS:"com.hellotalk.super1yearauto"
	PRODUCT_YEAR_AUTO_C = 25
	// "com.hellotalk.yearauto.c"
	PRODUCT_LIFE_TIME_B = 26
	// "com.hellotalk.lifetime.b"
	// iOS:"com.hellotalk.superlifetime"
	PRODUCT_LIFE_TIME_C = 27
	// "com.hellotalk.lifetime.c"
	PRODUCT_ITEM_END = 28
	// product item end
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)

const (
	CheckBeginTimestamp = 1473696000 // 2016-09-13 00:00:00 → 1473696000 以09-13为起始时间戳
	Client_Version_220  = 131584
)

const (
	AppStoreIsStillTry = "1"
	AppStoreHasStop    = "0"
)

// 与数据库中STATE的定义一致
const (
	E_STATE_INIT        = 0 // 未验证
	E_STATE_SUCC        = 1 // 验证成功
	E_STATE_INVALID     = 2 // 验证失败
	E_STATE_EXPIRED     = 3 // 订单续费失败，验证过期，但是仍然处于尝试续费中
	E_STATE_END_BILLING = 4 // 验证过期 不在尝试续费
)

const (
	RB_AUTORENEW_EXPIRED = "autorenew_expired"
	RB_PURCHASE_NOTIFY   = "purchase_item"
	RABBIT_SUBMIT        = "user_submit"
	EXCHANGENAME         = "push-msg"
	ROUTINGKEY           = "push-r-1"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

// Convert uint to net.IP http://www.outofmemory.cn
func inet_ntoa(ipnr int64) net.IP {
	var bytes [4]byte
	bytes[0] = byte((ipnr >> 24) & 0xFF)
	bytes[1] = byte((ipnr >> 16) & 0xFF)
	bytes[2] = byte((ipnr >> 8) & 0xFF)
	bytes[3] = byte(ipnr & 0xFF)

	return net.IPv4(bytes[3], bytes[2], bytes[1], bytes[0])
}

// Convert net.IP to int64 ,  http://www.outofmemory.cn
func inet_aton(ipnr net.IP) int64 {
	bits := strings.Split(ipnr.String(), ".")

	b0, _ := strconv.Atoi(bits[0])
	b1, _ := strconv.Atoi(bits[1])
	b2, _ := strconv.Atoi(bits[2])
	b3, _ := strconv.Atoi(bits[3])

	var sum int64

	sum += int64(b0) << 24
	sum += int64(b1) << 16
	sum += int64(b2) << 8
	sum += int64(b3)
	return sum
}

func NotifyAutoRenewExpired(uid uint32, termianlType uint8, itemCode, platform uint32, orderId string, expireTs uint32) (err error) {
	// build notification
	dataObj := simplejson.New()
	dataObj.Set("item", itemCode)
	dataObj.Set("platform", platform)
	dataObj.Set("order_id", orderId)
	dataObj.Set("expire_ts", expireTs)

	infoObj := simplejson.New()
	infoObj.Set("type", RB_AUTORENEW_EXPIRED)
	infoObj.Set("data", dataObj)

	rootObject := BuildCommonObject(RABBIT_SUBMIT, uid, termianlType)
	rootObject.Set("info", infoObj)
	strSlice, err := rootObject.MarshalJSON()
	if err != nil {
		infoLog.Printf("NotifyAutoRenewExpired simpleJson MarshalJSON failed uid=%v terminaltype=%v err=%s",
			uid,
			termianlType,
			err)
		return err
	}
	infoLog.Printf("NotifyAutoRenewExpired publish slice=%s", strSlice)
	err = RabbitMQPublish(rabbitMQUrl, EXCHANGENAME, "direct", ROUTINGKEY, string(strSlice), true)
	if err != nil {
		infoLog.Printf("NotifyAutoRenewExpired publish strSlice=%s failed", strSlice)
	} else {
		infoLog.Printf("NotifyAutoRenewExpired publish strSlice=%s success", strSlice)
	}
	return err
}

func NotifyPurchaseInformation(toUid uint32, termianlType uint8, itemCode, platform, verifyState, clientOrServer, fromId uint32, orderId string, expireTs uint64) (err error) {
	// build notification
	dataObj := simplejson.New()
	dataObj.Set("item", itemCode)
	dataObj.Set("platform", platform)
	dataObj.Set("verify_success", verifyState)
	dataObj.Set("order_id", orderId)
	dataObj.Set("client_or_server", clientOrServer)
	dataObj.Set("purch_uid", fromId)
	dataObj.Set("expire_ts", expireTs)

	infoObj := simplejson.New()
	infoObj.Set("type", RB_PURCHASE_NOTIFY)
	infoObj.Set("data", dataObj)

	rootObject := BuildCommonObject(RABBIT_SUBMIT, toUid, termianlType)
	rootObject.Set("info", infoObj)
	strSlice, err := rootObject.MarshalJSON()
	if err != nil {
		infoLog.Printf("NotifyPurchaseInformation simpleJson MarshalJSON failed toUid=%v terminaltype=%v err=%s",
			toUid,
			termianlType,
			err)
		return err
	}
	infoLog.Printf("NotifyPurchaseInformation publish slice=%s", strSlice)
	err = RabbitMQPublish(rabbitMQUrl, EXCHANGENAME, "direct", ROUTINGKEY, string(strSlice), true)
	if err != nil {
		infoLog.Printf("NotifyPurchaseInformation publish strSlice=%s failed", strSlice)
	} else {
		infoLog.Printf("NotifyPurchaseInformation publish strSlice=%s success", strSlice)
	}
	return err
}

func BuildCommonObject(cmd string, uid uint32, terminalType uint8) (rootObject *simplejson.Json) {
	rootObject = simplejson.New()
	rootObject.Set("cmd", cmd)
	rootObject.Set("user_id", uid)
	rootObject.Set("os_type", terminalType)
	rootObject.Set("server_ts", time.Now().Unix())
	return rootObject
}

func RabbitMQPublish(amqpURI, exchange, exchangeType, routingKey, body string, reliable bool) error {

	// This function dials, connects, declares, publishes, and tears down,
	// all in one go. In a real service, you probably want to maintain a
	// long-lived connection as state, and publish against that.

	// infoLog.Printf("dialing %q", amqpURI)
	connection, err := amqp.Dial(amqpURI)
	if err != nil {
		return fmt.Errorf("Dial: %s", err)
	}
	defer connection.Close()

	channel, err := connection.Channel()
	if err != nil {
		return fmt.Errorf("Channel: %s", err)
	}

	//Printf("got Channel, declaring %q Exchange (%q)", exchangeType, exchange)
	if err := channel.ExchangeDeclare(
		exchange,     // name
		exchangeType, // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return fmt.Errorf("Exchange Declare: %s", err)
	}

	// Reliable publisher confirms require confirm.select support from the
	// connection.
	if reliable {
		infoLog.Printf("enabling publishing confirms.")
		if err := channel.Confirm(false); err != nil {
			return fmt.Errorf("Channel could not be put into confirm mode: %s", err)
		}

		confirms := channel.NotifyPublish(make(chan amqp.Confirmation, 1))

		defer confirmOne(confirms)
	}

	// infoLog.Printf("declared Exchange, publishing %dB body (%q)", len(body), body)
	if err = channel.Publish(
		exchange,   // publish to an exchange
		routingKey, // routing to 0 or more queues
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "text/plain",
			ContentEncoding: "",
			Body:            []byte(body),
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
			// a bunch of application/implementation-specific fields
		},
	); err != nil {
		return fmt.Errorf("Exchange Publish: %s", err)
	}

	return nil
}

// One would typically keep a channel of publishings, a sequence number, and a
// set of unacknowledged sequence numbers and loop until the publishing channel
// is closed.
func confirmOne(confirms <-chan amqp.Confirmation) {
	infoLog.Printf("waiting for confirmation of one publishing")

	if confirmed := <-confirms; confirmed.Ack {
		infoLog.Printf("confirmed delivery with delivery tag: %d", confirmed.DeliveryTag)
	} else {
		infoLog.Printf("failed delivery of delivery tag: %d", confirmed.DeliveryTag)
	}
}

// step7: 通知usercache清除用户缓存
func SendPacketToUserCache(userId uint32) (err error) {
	head := &common.HeadV2{
		Version:  common.CVerMmedia,
		Cmd:      uint32(ht_user.ProtoCmd_CMD_MOD_USERINFO),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      userId,
	}

	userCacheReqBody := new(ht_user.ReqBody)
	userCacheReqBody.User = []*ht_user.UserInfoBody{&ht_user.UserInfoBody{
		UserID: proto.Uint32(userId),
	},
	}

	payLoad, err := proto.Marshal(userCacheReqBody)
	if err != nil {
		infoLog.Printf("SendPacketToUserCache proto marshal user=%v err=%s",
			userId,
			err)
		return err
	}
	ret, err := userCacheApi.SendPacket(head, payLoad)
	if err != nil {
		attr := "govipcheck/update_user_cache_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("SendPacketToUserCache userCacheApi.SendPacket failed userId=%v err=%s", userId, err)
		return err
	}
	infoLog.Printf("SendPacketToUserCache userCacheApi.SendPacket ret=%v", ret)
	return nil
}

func CheckAppleNeedRenewTransaction() {
	// add billing order
	billingOrders, err := DbUtil.GetAppleAutoRenewBillingOrder()
	if err != nil {
		infoLog.Printf("CheckAppleNeedRenewTransaction DbUtil.GetAppleAutoRenewBillingOrder err=%s", err)
		// 统计总的请求量
		attr := "goautorenew/apple_get_billing_order_failed"
		libcomm.AttrAdd(attr, 1)
		return
	}
	infoLog.Printf("CheckAppleNeedRenewTransaction DbUtil.GetAppleAutoRenewBillingOrder size=%v", len(billingOrders))
	for _, v := range billingOrders {
		appleItem, err := DbUtil.GetAppleTransactionByOriginId(v)
		if err != nil {
			infoLog.Printf("CheckAppleNeedRenewTransaction DbUtil.GetAppleTransactionByOriginId err=%s", err)
			// 统计总的请求量
			attr := "goautorenew/apple_get_order_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		dataString := TimeStampToDate(int64(appleItem.ExpireTimeStamp) / 1000)
		// 验证失败或过期的订单不需要再验证
		if appleItem.Status == E_STATE_EXPIRED {
			infoLog.Printf("CheckAppleNeedRenewTransaction BILLING_VERIFY_TRANS uid=%v tran_id=%s stat=%v exp_ts=%v exp_date=%s",
				appleItem.UserId,
				appleItem.TransactionId,
				appleItem.Status,
				appleItem.ExpireTimeStamp,
				dataString)
			select {
			case appleCheckChan <- appleItem:
				infoLog.Printf("CheckAppleNeedRenewTransaction put into check chan uid=%v tran_id=%s stat=%v exp_ts=%v exp_date=%s",
					appleItem.UserId,
					appleItem.TransactionId,
					appleItem.Status,
					appleItem.ExpireTimeStamp,
					dataString)
			case <-closeChan: // 进程退出
				infoLog.Printf("CheckAppleNeedRenewTransaction gorutine exit")
				return
			}
		}
	}
}

func TimeStampToDate(timeStamp int64) (dataString string) {
	// 设置时区为8时区
	var realyZone int = 8
	// zone 里面记录的是小时需要将小时转换成秒
	localZone := time.FixedZone("UTC", int(realyZone)*3600)
	timeNow := time.Unix(timeStamp, 0)
	timeLocal := timeNow.In(localZone)
	dataString = timeLocal.Format("2006-01-02 15:04:05")
	return dataString
}

func CheckAppleStoreOrder() {
	for {
		select {
		case appleItem := <-appleCheckChan:
			err := AppleStoreVerifyAutoRenew(&appleItem)
			if err != nil {
				infoLog.Printf("CheckAppleStoreOrder userId=%v transactionId=%s productId=%s expireTs=%v status=%v",
					appleItem.UserId,
					appleItem.TransactionId,
					appleItem.ProductId,
					appleItem.ExpireTimeStamp,
					appleItem.Status)
			}
		case <-closeChan:
			infoLog.Printf("CheckGooogleNeedRenewOrder gorutine exit")
			return
		}
	}
}

func AppleStoreVerifyAutoRenew(appleItem *util.AppleRenewItem) (err error) {
	if appleItem == nil {
		err = ErrInputParam
		infoLog.Printf("AppleStoreVerifyAutoRenew invalid input param")
		return err
	}

	req := appstore.IAPRequest{
		ReceiptData: string(appleItem.LatestReceipt),
		Password:    PURCHASE_PWD,
	}
	resp := &appstore.IAPResponse{}
	err = appstoreClient.Verify(req, resp)
	if err != nil {
		infoLog.Printf("AppleStoreVerifyAutoRenew userId=%v receipts=%v currency=%s payMoney=%s err=%s", appleItem.UserId, appleItem.LatestReceipt, appleItem.Currency, appleItem.PayMoney, err)
		return err
	}
	// infoLog.Printf("AppleStoreVerifyAutoRenew uid=%v appleItem=%#v resp=%#v",
	// 	appleItem.UserId,
	// 	*appleItem,
	// 	resp)
	// copy the transaction
	receipt := resp.Receipt
	latestReceiptInfo := resp.LatestReceiptInfo
	if resp.Status == ExpireReceiptCode {
		latestReceiptInfo = resp.LatestExpireReceiptInfo
	}

	verifyRspStr, err := GenAppStoreAutoRenewVerifyRspStr(resp)
	if err != nil {
		attr := "goautorenew/appstore_json_rsp_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("AppleStoreVerifyAutoRenew GenAppStoreAutoRenewVerifyRspStr failed rsp=%#v err=%s", resp, err)
	}
	if latestReceiptInfo.Bid != util.HelloTalkBid ||
		latestReceiptInfo.ProductID != appleItem.ProductId {
		infoLog.Printf("AppleStoreVerifyAutoRenew Faker Order userId=%v bid=%s productId=%s", appleItem.UserId, latestReceiptInfo.Bid, latestReceiptInfo.ProductID)
		err = ErrFakeOrder
		return err
	}
	itemCode := EnumProductCode(appleItem.ProductId)
	infoLog.Printf("AppleStoreVerifyAutoRenew userId=%v itemCode=%v", appleItem.UserId, itemCode)
	// 判断验证结果
	if resp.Status == ExpireReceiptCode { // 续订过期
		infoLog.Printf("AppleStoreVerifyAutoRenew check Renew Receipt expired status=21006 uid=%v transactionId=%s expireDate=%s",
			appleItem.UserId,
			appleItem.TransactionId,
			latestReceiptInfo.ExpiresDate)

		latestExpireDate, err := strconv.ParseUint(latestReceiptInfo.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("CheckAutoRenewApple strconv.ParseUint failed uid=%v productId=%s err=%s", appleItem.UserId, latestReceiptInfo.TransactionID, err)
		}
		timeStampNow := time.Now().Unix()
		var verifyStat uint32 = E_STATE_EXPIRED
		infoLog.Printf("AppleStoreVerifyAutoRenew userId=%v itemCode=%v verifyStat=%v isInBilling=%s expireDate=%v",
			appleItem.UserId,
			itemCode,
			resp.Status,
			resp.IsInBillingRetryPeriod,
			latestExpireDate)
		if (resp.IsInBillingRetryPeriod == AppStoreHasStop) ||
			(timeStampNow-int64(latestExpireDate/1000)) > int64(AppStoreBillingPeriod) {
			verifyStat = E_STATE_END_BILLING
			infoLog.Printf("AppleStoreVerifyAutoRenew userId=%v itemCode=%v verifyStat=%v isInBilling=%s expireDate=%v set E_STATE_END_BILLING",
				appleItem.UserId,
				itemCode,
				resp.Status,
				resp.IsInBillingRetryPeriod,
				latestExpireDate)
		}
		err = DbUtil.WriteAutoRenewRecordApple(
			appleItem.UserId,
			verifyStat,
			1,
			latestReceiptInfo.TransactionID,
			latestReceiptInfo.ProductID,
			latestReceiptInfo.OriginalPurchaseDate.OriginalPurchaseDate,
			latestReceiptInfo.OriginalTransactionID,
			latestReceiptInfo.PurchaseDate.PurchaseDate,
			latestReceiptInfo.PurchaseDateMS,
			latestReceiptInfo.ExpiresDateFormatted,
			latestReceiptInfo.ExpiresDate.ExpiresDate,
			resp.LatestReceipt,
			verifyRspStr,
			appleItem.Currency,
			appleItem.PayMoney)
		if err != nil {
			mysqlerr, ok := err.(*mysql.MySQLError)
			if ok && mysqlerr.Number == 1062 {
				// 应为过期的验证不会产生新的交易ID
				// 如果是过期的记录需要更新交易的验证状态为3(避免每次都会做续费验证)
				infoLog.Printf("AppleStoreVerifyAutoRenew WriteAutoRenewRecordApple deuplicate order uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					appleItem.UserId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
				err = DbUtil.UpdateRenewExpiredTransaction(latestReceiptInfo.TransactionID, verifyRspStr, verifyStat)
				if err != nil {
					attr := "goautorenew/appstore_update_renew_expire_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("AppleStoreVerifyAutoRenew UpdateRenewExpiredTransaction failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
						appleItem.UserId,
						resp.Status,
						latestReceiptInfo.TransactionID,
						latestReceiptInfo.PurchaseDate,
						verifyRspStr,
						err)
				} else {
					infoLog.Printf("AppleStoreVerifyAutoRenew UpdateRenewExpiredTransaction success uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
						appleItem.UserId,
						resp.Status,
						latestReceiptInfo.TransactionID,
						latestReceiptInfo.PurchaseDate,
						verifyRspStr,
						err)
				}
			} else {
				attr := "goautorenew/appstore_write_auto_renew_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("AppleStoreVerifyAutoRenew WriteAutoRenewRecordApple failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					appleItem.UserId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
			}
		} else { // 写成功了
			infoLog.Printf("AppleStoreVerifyAutoRenew WriteAutoRenewRecordApple success status=21006 uid=%v transactionId=%s",
				appleItem.UserId,
				latestReceiptInfo.TransactionID)
		}
		if (verifyStat == E_STATE_END_BILLING) ||
			(timeStampNow-int64(latestExpireDate/1000) > int64(AppStoreBillingPeriod)) {
			// 发RabbitMQ通知
			err = NotifyAutoRenewExpired(appleItem.UserId, 0, itemCode, 1, latestReceiptInfo.TransactionID, uint32(latestExpireDate/1000))
			if err != nil {
				infoLog.Printf("CheckAutoRenewApple NotifyAutoRenewExpired failed uid=%v itemCode=%v productId=%v err=%s",
					appleItem.UserId,
					itemCode,
					appleItem.ProductId,
					err)
			}
		}
		return err
	} else if resp.Status == VerifySuccessCode { // 校验成功
		receiptExpireTs, err := strconv.ParseUint(receipt.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("AppleStoreVerifyAutoRenew strconv.ParseUint receipt.ExpiresDate=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				receipt.ExpiresDate,
				appleItem.UserId,
				verifyRspStr,
				appleItem.Currency,
				appleItem.PayMoney,
				err)
			return err
		}

		latestExpireTs, err := strconv.ParseUint(latestReceiptInfo.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("AppleStoreVerifyAutoRenew strconv.ParseUint latestReceiptInfo.ExpiresDate=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				latestReceiptInfo.ExpiresDate,
				appleItem.UserId,
				verifyRspStr,
				appleItem.Currency,
				appleItem.PayMoney,
				err)
			return err
		}

		latestPurchTs, err := strconv.ParseUint(latestReceiptInfo.PurchaseDateMS, 10, 64)
		if err != nil {
			infoLog.Printf("AppleStoreVerifyAutoRenew strconv.ParseUint latestReceiptInfo.PurchaseDateMS=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				latestReceiptInfo.PurchaseDateMS,
				appleItem.UserId,
				verifyRspStr,
				appleItem.Currency,
				appleItem.PayMoney,
				err)
			return err
		}
		utilTimeStamp := latestExpireTs / 1000 // chage ms to second
		purchasePeriod := (latestExpireTs - latestPurchTs) / 1000
		if receiptExpireTs == latestExpireTs {
			// auto renew not change
			// 秒的时间戳一样 可能毫秒的时间戳不一样 （iOS回执数据里可能出现的） pippo 2016-09-14
			infoLog.Printf("AppleStoreVerifyAutoRenew check Renew Receipt is not change. uid=%v utilTimeStamp=%v purchasePeriod=%v expireDate=%s",
				appleItem.UserId,
				utilTimeStamp,
				purchasePeriod,
				latestReceiptInfo.ExpiresDateFormatted)
			return nil // 自动续费的认为是重复的订单  就是没有续费操作  插入数据库也会失败
		} else {
			infoLog.Printf("AppleStoreVerifyAutoRenew Renew is successfully. uid=%v utilTimeStamp=%v purchasePeriod=%v expireDate=%s",
				appleItem.UserId,
				utilTimeStamp,
				purchasePeriod,
				latestReceiptInfo.ExpiresDateFormatted)
		}
		// STEP1: 写购买续订记录
		err = DbUtil.WriteAutoRenewRecordApple(
			appleItem.UserId,
			E_STATE_SUCC,
			1,
			latestReceiptInfo.TransactionID,
			latestReceiptInfo.ProductID,
			latestReceiptInfo.OriginalPurchaseDate.OriginalPurchaseDate,
			latestReceiptInfo.OriginalTransactionID,
			latestReceiptInfo.PurchaseDate.PurchaseDate,
			latestReceiptInfo.PurchaseDateMS,
			latestReceiptInfo.ExpiresDateFormatted,
			latestReceiptInfo.ExpiresDate.ExpiresDate,
			resp.LatestReceipt,
			verifyRspStr,
			appleItem.Currency,
			appleItem.PayMoney)
		if err != nil {
			mysqlerr, ok := err.(*mysql.MySQLError)
			if ok && mysqlerr.Number == 1062 {
				infoLog.Printf("AppleStoreVerifyAutoRenew Repeat AutoRenew deuplicate order uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					appleItem.UserId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
			} else {
				attr := "goautorenew/appstore_write_auto_renew_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("AppleStoreVerifyAutoRenew WriteAutoRenewRecordApple failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					appleItem.UserId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
			}
			return err
		} else {
			// STEP2: 续费成功 需要更新会员的过期时间
			var vipExpiredBefore, vipExpiredAfter uint64
			var vipYearOut, vipTypeOut uint8
			// step3: 根据订单详情修改会员过期天数
			tryCount := 0
			for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
				vipExpiredBefore, vipExpiredAfter, vipYearOut, vipTypeOut, err = UpdateVipExpiredTime(
					appleItem.UserId,
					itemCode,
					utilTimeStamp,
					purchasePeriod,
					false)
				if err != nil {
					infoLog.Printf("AppleStoreVerifyAutoRenew UpdateVipExpiredTime failed uid=%v err=%v", appleItem.UserId, err)
					time.Sleep(5 * time.Second) // 休眠5秒再重试
					continue
				} else {
					infoLog.Printf("AppleStoreVerifyAutoRenew uid=%v vipExpiredBefore=%v vipExpiredAfter=%v vipYearOut=%v vipTypeOut=%v",
						appleItem.UserId,
						vipExpiredBefore,
						vipExpiredAfter,
						vipYearOut,
						vipTypeOut)
				}
				break
			}
			if tryCount == 0 {
				attr := "goautorenew/update_vip_expired_time_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("AppleStoreVerifyAutoRenew UpdateVipExpiredTime failed uid=%v tryCount=%v", appleItem.UserId, tryCount)
			}
			// STEP3: updae vip expires success send notify to IM
			if tryCount > 0 {
				err = SendAutoRenewSuccessNotify(appleItem.UserId, itemCode, vipExpiredAfter)
				if err != nil {
					infoLog.Printf("AppleStoreVerifyAutoRenew UpdateVipExpiredTime failed uid=%v itemCode=%v vipExpireTs=%v", appleItem.UserId, itemCode, vipExpiredAfter)
				}
			}
			latestPurchaseMS, err := strconv.ParseUint(latestReceiptInfo.PurchaseDateMS, 10, 64)
			if err != nil {
				infoLog.Printf("AppleStoreVerifyAutoRenew strconv.ParseUint failed uid=%v itemCode=%v purchaseMs=%s", appleItem.UserId, itemCode, latestReceiptInfo.PurchaseDateMS)
			}
			// STEP4: 写购买历史记录只有续费成功才写购买历史
			historyOrder := &util.PurchaseHistory{
				OrderId:        latestReceiptInfo.TransactionID,
				ItemCode:       itemCode,
				ProductId:      appleItem.ProductId,
				GiftDays:       0,
				UserId:         appleItem.UserId,
				ToId:           appleItem.UserId,
				Currency:       appleItem.Currency,
				PayMoney:       appleItem.PayMoney,
				PayType:        util.PAY_TYPE_APPSTORE,
				BuyTime:        TimeStampToDate(int64(latestPurchaseMS) / 1000),
				UtilBefore:     vipExpiredBefore,
				UtilAfter:      vipExpiredAfter,
				ClientOrServer: 1,
				RetryTime:      0,
			}
			tryCount = 0
			for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
				err = DbUtil.WritePurchaseHistory(historyOrder)
				if err != nil {
					infoLog.Printf("AppleStoreVerifyAutoRenew DbUtil.WritePurchaseHistory failed uid=%v err=%v", appleItem.UserId, err)
					time.Sleep(5 * time.Second) // 休眠5秒再重试
					continue
				}
				break
			}
			if tryCount == 0 {
				attr := "goautorenew/wirte_purchase_history_order_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("AppleStoreVerifyAutoRenew DbUtil.WritePurchaseHistory failed uid=%v tryCount=%v", appleItem.UserId, tryCount)
			}

			// STEP5: 发RabbitMQ通知
			err = NotifyPurchaseInformation(appleItem.UserId, 0, itemCode, 1, 1, 1, appleItem.UserId, historyOrder.OrderId, historyOrder.UtilAfter)
			if err != nil {
				attr := "goautorenew/notify_mq_purchase_info_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("AppleStoreVerifyAutoRenew NotifyPurchaseInformation faile UserId=%v itemCode=%v orderId=%s utilAfter=%v",
					appleItem.UserId,
					itemCode,
					historyOrder.OrderId,
					historyOrder.UtilAfter)
			}
			// STEP6: 更新UseCache
			err = SendPacketToUserCache(appleItem.UserId)
			if err != nil {
				attr := "goautorenew/update_user_cache_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("AppleStoreVerifyAutoRenew SendPacketToUserCache failed uid=%v", appleItem.UserId)
			}
		}
	}
	return err
}

func SendAutoRenewSuccessNotify(uid, itemCode uint32, vipExpireTs uint64) (err error) {
	if uid == 0 || itemCode == 0 || vipExpireTs == 0 {
		infoLog.Printf("SendAutoRenewSuccessNotify invalid param uid=%v itemCode=%v vipExpire=%v", uid, itemCode, vipExpireTs)
		err = ErrInputParam
		return err
	}
	// step6: 发送请求到QIMServer(需要查询用户在哪台服务器上)实时更新会员信息
	reqBody := new(ht_thirdpay.ThirdPayReqBody)
	reqBody.AutoRenewSuccessReqbody = &ht_thirdpay.AutoRenewSuccessReqBody{
		Uid:         proto.Uint32(uid),
		ItemCode:    proto.Uint32(itemCode),
		VipExpireTs: proto.Uint64(vipExpireTs),
	}
	err = SendPacketToIMServer(uid, reqBody)
	if err != nil {
		attr := "goautorenew/send_packet_to_im_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("SendAutoRenewSuccessNotify SendPacketToIMServer failed uid=%v itermCode=%v vipExpireTs=%v",
			uid,
			itemCode,
			vipExpireTs)
	}
	return err
}

func SendPacketToIMServer(userId uint32, reqBody *ht_thirdpay.ThirdPayReqBody) (err error) {
	// 查询用户的在线状态
	onlineStat, err := mcApi.GetUserOnlineStat(userId)
	if err != nil {
		infoLog.Printf("SendPacketToIMServer user online failed uid=%v err=%v", userId, err)
		fromId := reqBody.GetUpdateUserVipInfoReqbody().GetFromId()
		// 查询用户在线失败则查询发起方在线
		onlineStat, err = mcApi.GetUserOnlineStat(fromId)
		if err != nil {
			infoLog.Printf("SendPacketToIMServer Get user online failed get default user uid=%v err=%v default=%v", fromId, err, DEFAULT_USER)
			// 查询发起方在线失败查询默认用户在线(经常活跃 mc 中一定存在在线)
			onlineStat, err = mcApi.GetUserOnlineStat(DEFAULT_USER)
			if err != nil {
				infoLog.Printf("SendPacketToIMServer Get default user online failed get default user uid=%v err=%v", DEFAULT_USER, err)
				return err
			}
		}
	}
	head := &common.HeadV3{Flag: uint8(common.CServToServ),
		Version:  common.CVerMmedia,
		CryKey:   uint8(common.CNoneKey),
		TermType: onlineStat.ClientType,
		Cmd:      uint16(ht_thirdpay.THIRDPAY_CMD_TYPE_GO_CMD_AUTO_RENEW_SUCCESS),
		Seq:      0,
		From:     0,
		To:       userId,
		Len:      0,
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("SendPacketToIMServer proto marshal user=%v err=%v",
			userId,
			err)
		return err
	}
	err = SendPacketToIMServerRelabile(onlineStat, head, payLoad)
	if err != nil {
		infoLog.Printf("SendPacketToIMServer exec SendPacketToIMServerRelabile failed user=%v err=%v",
			userId,
			err)
		return err
	}
	return nil
}

func SendPendingOrderToIMServer(userId uint32) (err error) {
	// 查询用户的在线状态
	onlineStat, err := mcApi.GetUserOnlineStat(userId)
	if err != nil {
		infoLog.Printf("SendPendingOrderToIMServer user online failed uid=%v err=%v", userId, err)
		// 查询发起方在线失败查询默认用户在线(经常活跃 mc 中一定存在在线)
		onlineStat, err = mcApi.GetUserOnlineStat(DEFAULT_USER)
		if err != nil {
			infoLog.Printf("SendPendingOrderToIMServer Get default user online failed get default user uid=%v err=%v", DEFAULT_USER, err)
			return err
		}
	}
	head := &common.HeadV3{Flag: uint8(common.CServToServ),
		Version:  common.CVerMmedia,
		CryKey:   uint8(common.CNoneKey),
		TermType: onlineStat.ClientType,
		Cmd:      uint16(ht_thirdpay.THIRDPAY_CMD_TYPE_GO_CMD_AUTO_RENEQ_PENDIGN),
		Seq:      0,
		From:     0,
		To:       userId,
		Len:      0,
	}

	reqBody := &ht_thirdpay.ThirdPayReqBody{
		AutoRenewPendingOrderReqbody: &ht_thirdpay.AutoRenewPendingOrderReqBody{
			Uid: proto.Uint32(userId),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("SendPendingOrderToIMServer proto marshal user=%v err=%v",
			userId,
			err)
		return err
	}
	err = SendPacketToIMServerRelabile(onlineStat, head, payLoad)
	if err != nil {
		infoLog.Printf("SendPendingOrderToIMServer exec SendPacketToIMServerRelabile failed user=%v err=%v",
			userId,
			err)
		return err
	}
	infoLog.Printf("SendPendingOrderToIMServer success uid=%v", userId)
	return nil
}

func SendPacketToIMServerRelabile(stat *common.UserState, head *common.HeadV3, payLoad []byte) (err error) {
	svrIp := inet_ntoa(int64(stat.SvrIp)).String()
	infoLog.Printf("SendPacketToIMServerRelabile svrIP=%s oriIP=%v", svrIp, stat.SvrIp)
	v, ok := imServerPool[svrIp]
	if !ok { // 不存在直接打印日志返回错误
		infoLog.Printf("SendPacketToIMServerRelabile not exist IM ip=%v imServerPool=%v", svrIp, imServerPool)
		return ErrOnlineIpErr
	}

	tryCount := 2
	for tryCount > 0 {
		ret, err := v.SendPacket(head, payLoad)
		if err == nil && ret == 0 {
			infoLog.Printf("SendPacketToIMServerRelabile send succ from=%v to=%v seq=%v cmd=%v", head.From, head.To, head.Seq, head.Cmd)
			return nil
		} else {
			infoLog.Printf("SendPacketToIMServerRelabile failed from=%v to=%v seq=%v cmd=%v err=%v ret=%v", head.From, head.To, head.Seq, head.Cmd, err, ret)
		}
		tryCount--
	}
	return common.ErrReachMaxTryCount
}

func EnumProductCode(productId string) (productCode uint32) {
	if productId == "com.hellotalk.onemonth1" {
		productCode = PRODUCT_ONE_MONTH
	} else if productId == "com.hellotalk.onemonthauto" {
		productCode = PRODUCT_ONE_MONTHAUTO
	} else if productId == "com.hellotalk.onemonthsubscriptionplan" ||
		productId == "com.hellotalk.onemonthsubscriptionplan2" {
		productCode = PRODUCT_ONE_MONTH_SUB_PLAN
	} else if productId == "com.hellotalk.onemonth1auto" ||
		productId == "com.hellotalk.1monthautorenew" ||
		productId == "com.hellotalk.monthauto" {
		productCode = PRODUCT_ONE_MONTHAUTO
	} else if productId == "com.hellotalk.oneyear" ||
		productId == "com.hellotalk.oneyeardiscount" {
		productCode = PRODUCT_ONE_YEAR
	} else if productId == "com.hellotalk.3months" {
		productCode = PRODUCT_THREE_MONTH
	} else if productId == "com.hellotalk.Gift1M" {
		productCode = PRODUCT_GIFT_ONEMONTH
	} else if productId == "com.hellotalk.g1m" {
		productCode = PRODUCT_GIFT_ONEMONTH
	} else if productId == "com.hellotalk.Gift3M" {
		productCode = PRODUCT_GIFT_THREEMONTH
	} else if productId == "com.hellotalk.g3m" {
		productCode = PRODUCT_GIFT_THREEMONTH
	} else if productId == "com.hellotalk.Gift1Y" {
		productCode = PRODUCT_GIFT_ONEYEAR
	} else if productId == "com.hellotalk.g1y" {
		productCode = PRODUCT_GIFT_ONEYEAR
	} else if productId == "com.hellotalk.1morelang" ||
		productId == "com.hellotalk.1moreLang2" {
		productCode = PRODUCT_ONE_MULITLANG
	} else if productId == "com.hellotalk.yearauto" {
		productCode = PRODUCT_YEAR_AUTO
	} else if productId == "com.hellotalk.oneyearsubscriptionplan" ||
		productId == "com.hellotalk.oneyearsubscriptionplan2" ||
		productId == "com.hellotalk.yearauto2" {
		productCode = PRODUCT_ONE_YEAR_SUB_PLAN
	} else if productId == "com.hellotalk.lifetime" {
		productCode = PRODUCT_LIFT_TIME
	} else if productId == "com.hellotalk.lifetimeplan" {
		productCode = PRODUCT_LIFE_TIME_SUB_PLAN
	} else if productId == "com.hellotalk.onemonthsubscription" ||
		productId == "com.hellotalk.onemonthsubscription2" ||
		productId == "com.hellotalk.monthauto_freetrial" {
		productCode = PRODUCT_ONE_MONTH_SUB
	} else if productId == "com.hellotalk.oneyearsubscription" ||
		productId == "com.hellotalk.oneyearsubscription2" ||
		productId == "com.hellotalk.yearauto2_freetrial" {
		productCode = PRODUCT_ONE_YEAR_SUB
	} else if productId == "com.hellotalk.onemonthauto.b" ||
		productId == "com.hellotalk.super1monthauto" {
		productCode = PRODUCT_ONE_MONTH_AUTO_B
	} else if productId == "com.hellotalk.onemonthauto.c" {
		productCode = PRODUCT_ONE_MONTH_AUTO_C
	} else if productId == "com.hellotalk.yearauto.b" ||
		productId == "com.hellotalk.super1yearauto" {
		productCode = PRODUCT_YEAR_AUTO_B
	} else if productId == "com.hellotalk.yearauto.c" {
		productCode = PRODUCT_YEAR_AUTO_C
	} else if productId == "com.hellotalk.lifetime.b" ||
		productId == "com.hellotalk.superlifetime" {
		productCode = PRODUCT_LIFE_TIME_B
	} else if productId == "com.hellotalk.lifetime.c" {
		productCode = PRODUCT_LIFE_TIME_C
	}
	return productCode
}

func GenAppStoreAutoRenewVerifyRspStr(rsp *appstore.IAPResponse) (verifyRspStr string, err error) {
	if rsp == nil {
		err = ErrInputParam
		return verifyRspStr, err
	}
	rspReceipt := rsp.Receipt

	receipt := simplejson.New()
	receipt.Set("expires_date_formatted", rspReceipt.ExpiresDateFormatted)
	receipt.Set("original_purchase_date_pst", rspReceipt.OriginalPurchaseDatePST)
	receipt.Set("unique_identifier", rspReceipt.UniqueIdentifier)
	receipt.Set("original_transaction_id", rspReceipt.OriginalTransactionID)
	receipt.Set("expires_date", rspReceipt.ExpiresDate)
	receipt.Set("app_item_id", rspReceipt.AppItemID)
	receipt.Set("transaction_id", rspReceipt.TransactionID)
	receipt.Set("quantity", rspReceipt.Quantity)
	receipt.Set("expires_date_formatted_pst", rspReceipt.ExpiresDateFormattedPST)
	receipt.Set("product_id", rspReceipt.ProductID)
	receipt.Set("bvrs", rspReceipt.Bvrs)
	receipt.Set("unique_vendor_identifier", rspReceipt.UniqueVendorIdentifier)
	receipt.Set("web_order_line_item_id", rspReceipt.WebOrderLineItemID)
	receipt.Set("original_purchase_date_ms", rspReceipt.OriginalPurchaseDateMS)
	receipt.Set("version_external_identifier", rspReceipt.VersionExternalIdentifier)
	receipt.Set("bid", rspReceipt.Bid)
	receipt.Set("purchase_date_ms", rspReceipt.PurchaseDateMS)
	receipt.Set("purchase_date", rspReceipt.PurchaseDate)
	receipt.Set("purchase_date_pst", rspReceipt.PurchaseDatePST)
	receipt.Set("original_purchase_date", rspReceipt.OriginalPurchaseDate)
	receipt.Set("item_id", rspReceipt.ItemId)

	rspJsonObj := simplejson.New()
	rspJsonObj.Set("receipt", receipt)
	rspJsonObj.Set("status", rsp.Status)
	rspJsonObj.Set("latest_receipt", rsp.LatestReceipt)

	latestReceiptInfo := simplejson.New()
	if rsp.Status == ExpireReceiptCode {
		rspLatestReceiptInfo := rsp.LatestExpireReceiptInfo
		latestReceiptInfo.Set("original_purchase_date_pst", rspLatestReceiptInfo.OriginalPurchaseDatePST)
		latestReceiptInfo.Set("unique_identifier", rspLatestReceiptInfo.UniqueIdentifier)
		latestReceiptInfo.Set("original_transaction_id", rspLatestReceiptInfo.OriginalTransactionID)
		latestReceiptInfo.Set("expires_date", rspLatestReceiptInfo.ExpiresDate)
		latestReceiptInfo.Set("app_item_id", rspLatestReceiptInfo.AppItemID)
		latestReceiptInfo.Set("transaction_id", rspLatestReceiptInfo.TransactionID)
		latestReceiptInfo.Set("quantity", rspLatestReceiptInfo.Quantity)
		latestReceiptInfo.Set("product_id", rspLatestReceiptInfo.ProductID)
		latestReceiptInfo.Set("bvrs", rspLatestReceiptInfo.Bvrs)
		latestReceiptInfo.Set("bid", rspLatestReceiptInfo.Bid)
		latestReceiptInfo.Set("unique_vendor_identifier", rspLatestReceiptInfo.UniqueVendorIdentifier)
		latestReceiptInfo.Set("web_order_line_item_id", rspLatestReceiptInfo.WebOrderLineItemID)
		latestReceiptInfo.Set("original_purchase_date_ms", rspLatestReceiptInfo.OriginalPurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted", rspLatestReceiptInfo.ExpiresDateFormatted)
		latestReceiptInfo.Set("purchase_date", rspLatestReceiptInfo.PurchaseDate)
		latestReceiptInfo.Set("purchase_date_ms", rspLatestReceiptInfo.PurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted_pst", rspLatestReceiptInfo.ExpiresDateFormattedPST)
		latestReceiptInfo.Set("purchase_date_pst", rspLatestReceiptInfo.PurchaseDatePST)
		latestReceiptInfo.Set("original_purchase_date", rspLatestReceiptInfo.OriginalPurchaseDate)
		latestReceiptInfo.Set("item_id", rspLatestReceiptInfo.ItemId)
		rspJsonObj.Set("latest_expired_receipt_info", latestReceiptInfo)
	} else {
		rspLatestReceiptInfo := rsp.LatestReceiptInfo
		latestReceiptInfo.Set("original_purchase_date_pst", rspLatestReceiptInfo.OriginalPurchaseDatePST)
		latestReceiptInfo.Set("unique_identifier", rspLatestReceiptInfo.UniqueIdentifier)
		latestReceiptInfo.Set("original_transaction_id", rspLatestReceiptInfo.OriginalTransactionID)
		latestReceiptInfo.Set("expires_date", rspLatestReceiptInfo.ExpiresDate)
		latestReceiptInfo.Set("app_item_id", rspLatestReceiptInfo.AppItemID)
		latestReceiptInfo.Set("transaction_id", rspLatestReceiptInfo.TransactionID)
		latestReceiptInfo.Set("quantity", rspLatestReceiptInfo.Quantity)
		latestReceiptInfo.Set("product_id", rspLatestReceiptInfo.ProductID)
		latestReceiptInfo.Set("bvrs", rspLatestReceiptInfo.Bvrs)
		latestReceiptInfo.Set("bid", rspLatestReceiptInfo.Bid)
		latestReceiptInfo.Set("unique_vendor_identifier", rspLatestReceiptInfo.UniqueVendorIdentifier)
		latestReceiptInfo.Set("web_order_line_item_id", rspLatestReceiptInfo.WebOrderLineItemID)
		latestReceiptInfo.Set("original_purchase_date_ms", rspLatestReceiptInfo.OriginalPurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted", rspLatestReceiptInfo.ExpiresDateFormatted)
		latestReceiptInfo.Set("purchase_date", rspLatestReceiptInfo.PurchaseDate)
		latestReceiptInfo.Set("purchase_date_ms", rspLatestReceiptInfo.PurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted_pst", rspLatestReceiptInfo.ExpiresDateFormattedPST)
		latestReceiptInfo.Set("purchase_date_pst", rspLatestReceiptInfo.PurchaseDatePST)
		latestReceiptInfo.Set("original_purchase_date", rspLatestReceiptInfo.OriginalPurchaseDate)
		latestReceiptInfo.Set("item_id", rspLatestReceiptInfo.ItemId)
		rspJsonObj.Set("latest_receipt_info", latestReceiptInfo)
	}

	verifyRspSlic, err := rspJsonObj.Encode()
	verifyRspStr = string(verifyRspSlic)
	return verifyRspStr, err
}

// 根据订单信息修改会员的过期天数
func UpdateVipExpiredTime(userId, itermCode uint32, utilTimestamp uint64, purchPeriod uint64, repeatCommit bool) (vipExpiredBefore, vipExpiredAfter uint64, vipYearOut, vipTypeOut uint8, err error) {
	// 购买会员成功更新会员天数
	infoLog.Printf("UpdateVipExpiredTime userId=%v itermCode=%v utilTimestamp=%v purchPeroid=%v repeatcommit=%v",
		userId,
		itermCode,
		utilTimestamp,
		purchPeriod,
		repeatCommit)

	lastIterm, vipYear, vipType, expiredTS, err := DbUtil.GetUserVIPInfo(userId)
	if err != nil {
		infoLog.Printf("UpdateVipExpiredTime exec GetUserVIPInfo failed userUid=%v", userId)
		return 0, 0, 0, 0, err
	}
	infoLog.Printf("UpdateVipExpiredTime lastIterm=%v vipYear=%v vipType=%v expiredTS=%v", lastIterm, vipYear, vipType, expiredTS)
	vipExpiredBefore = expiredTS

	if repeatCommit == true {
		vipYearOut = vipYear
		vipExpiredAfter = expiredTS
		vipTypeOut = vipType
		return vipExpiredBefore, vipExpiredAfter, vipYearOut, vipTypeOut, nil
	}

	timeStampNow := uint64(time.Now().Unix())
	for {
		// 购买的是年会员
		if itermCode == PRODUCT_ONE_YEAR ||
			itermCode == PRODUCT_GIFT_ONEYEAR ||
			itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN || // 高价格年费会员
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C { // 年费试用会员
			vipYear = YEAR_VIP
			break
		}

		// 以前是年会员 现在还没有过期 依旧是年会员
		if vipYear == YEAR_VIP && expiredTS > timeStampNow {
			vipYear = YEAR_VIP
			break
		}

		vipYear = YEAR_NORMAL
		break
	}

	// 原来的会员是否已经过期
	// 判断会员类型 终生会员 > 自动续费会员 > 剩余天数会员
	if expiredTS >= timeStampNow { // 会员没有过期
		// 未过期的有顺序判断
		if itermCode == PRODUCT_LIFT_TIME ||
			itermCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
			itermCode == PRODUCT_LIFE_TIME_B ||
			itermCode == PRODUCT_LIFE_TIME_C ||
			vipType == E_LIFETIME_VIP {
			vipType = E_LIFETIME_VIP
		} else if itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C ||
			vipType == E_YEARAUTO_VIP {
			vipType = E_YEARAUTO_VIP
		} else if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C ||
			vipType == E_MONTHAUTO_VIP {
			vipType = E_MONTHAUTO_VIP
		} else {
			vipType = E_DAYS_VIP
		}
	} else {
		// 已经过期的以当前购买的为准
		if itermCode == PRODUCT_LIFT_TIME ||
			itermCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
			itermCode == PRODUCT_LIFE_TIME_B ||
			itermCode == PRODUCT_LIFE_TIME_C {
			vipType = E_LIFETIME_VIP
		} else if itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C {
			vipType = E_YEARAUTO_VIP
		} else if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C {
			vipType = E_MONTHAUTO_VIP
		} else {
			vipType = E_DAYS_VIP
		}
	}
	// 更改到期时间戳
	countSecond := CountPostponeSeconds(itermCode)
	infoLog.Printf("UpdateVipExpiredTime countSecond=%v purchPeriod=%v", countSecond, purchPeriod)
	if expiredTS < timeStampNow {
		// 已经过期或是第一次购买的 月费试用会员和年费试用会员都是自动续费类型
		if itermCode == PRODUCT_ONE_MONTHAUTO ||
			itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
			itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
			itermCode == PRODUCT_ONE_MONTH_AUTO_C {
			// 自动续费的以续费过期时间为准
			expiredTS = utilTimestamp
		} else {
			// 其他的认为在当前时间往后延长
			expiredTS = timeStampNow + countSecond
		}
	} else {
		if itermCode == PRODUCT_ONE_MONTH_SUB ||
			itermCode == PRODUCT_ONE_YEAR_SUB {
			// 对于月费试用会员和年费试用会员 将过期时间修改成 当前时间 + 订单里面的持续时间 不再写死会员的过期时间
			expiredTS += purchPeriod
		} else {
			// 还没有过期则在原来的到期时间戳上延长
			expiredTS += countSecond
		}
	}

	infoLog.Printf("UpdateVipExpiredTime uid=%v lastIterm=%v vipYear=%v vipType=%v expiredTS=%v",
		userId,
		lastIterm,
		vipYear,
		vipType,
		expiredTS)
	err = DbUtil.UpdateUserVIPInfo(userId, itermCode, vipYear, vipType, expiredTS)
	if err != nil {
		infoLog.Printf("UpdateVipExpiredTime UpdateUserVIPInfo failed lastIterm=%v vipYear=%v vipType=%v expiredTS=%v err=%s",
			lastIterm,
			vipYear,
			vipType,
			expiredTS,
			err)
	} else {
		infoLog.Printf("UpdateVipExpiredTime UpdateUserVIPInfo ok lastIterm=%v vipYear=%v vipType=%v expiredTS=%v",
			lastIterm,
			vipYear,
			vipType,
			expiredTS)
	}

	// 更新成功返回
	vipExpiredAfter = expiredTS
	vipYearOut = vipYear
	vipTypeOut = vipType
	infoLog.Printf("UpdateVipExpiredTime vipExpiredBefore=%v vipExpiredAfter=%v vipYearOut=%v vipTypeOut=%v",
		vipExpiredBefore,
		vipExpiredAfter,
		vipYearOut,
		vipTypeOut)
	return vipExpiredBefore, vipExpiredAfter, vipYearOut, vipTypeOut, nil
}

func UpdateAutoTypeVipExpiredTime(userId, itermCode uint32, utilTimestamp, purchasePeriod uint64) (err error) {
	// 购买会员成功更新会员天数
	infoLog.Printf("UpdateAutoTypeVipExpiredTime userId=%v itermCode=%v utilTimestamp=%v purchPeroid=%v",
		userId,
		itermCode,
		utilTimestamp,
		purchasePeriod)
	if itermCode != PRODUCT_ONE_MONTHAUTO &&
		itermCode != PRODUCT_YEAR_AUTO &&
		itermCode != PRODUCT_ONE_MONTH_SUB_PLAN &&
		itermCode != PRODUCT_ONE_YEAR_SUB_PLAN &&
		itermCode != PRODUCT_ONE_MONTH_SUB &&
		itermCode != PRODUCT_ONE_YEAR_SUB &&
		itermCode != PRODUCT_ONE_MONTH_AUTO_B &&
		itermCode != PRODUCT_ONE_MONTH_AUTO_C &&
		itermCode != PRODUCT_YEAR_AUTO_B &&
		itermCode != PRODUCT_YEAR_AUTO_C {
		infoLog.Printf("UpdateAutoTypeVipExpiredTime trace userId=%v itermCode=%v utilTimestamp=%v purchasePeriod=%v not auto order",
			userId,
			itermCode,
			utilTimestamp,
			purchasePeriod)
		err = ErrInputParam
		return err
	}

	lastIterm, vipYear, vipType, expiredTS, err := DbUtil.GetUserVIPInfo(userId)
	if err != nil {
		infoLog.Printf("UpdateAutoTypeVipExpiredTime exec GetUserVIPInfo failed userUid=%v", userId)
		return err
	}
	infoLog.Printf("UpdateAutoTypeVipExpiredTime lastIterm=%v vipYear=%v vipType=%v expiredTS=%v", lastIterm, vipYear, vipType, expiredTS)
	timeStampNow := uint64(time.Now().Unix())
	// 原来的会员是否已经过期 如果会员没有过期或者订单内的过期时间戳小余当前时间则不需要更新过期时间
	isYetExpired := false
	if expiredTS < timeStampNow {
		isYetExpired = true
	}
	if !isYetExpired || utilTimestamp < timeStampNow {
		infoLog.Printf("UpdateAutoTypeVipExpiredTime vip info uid=%v lastItem=%v vipYear=%v ts=%v utilts=%v no need update",
			userId,
			lastIterm,
			vipYear,
			expiredTS,
			utilTimestamp)
		return nil
	}

	for {
		// 购买的是年会员
		if itermCode == PRODUCT_ONE_YEAR ||
			itermCode == PRODUCT_GIFT_ONEYEAR ||
			itermCode == PRODUCT_YEAR_AUTO ||
			itermCode == PRODUCT_ONE_YEAR_SUB_PLAN || // 高价格年费会员
			itermCode == PRODUCT_ONE_YEAR_SUB ||
			itermCode == PRODUCT_YEAR_AUTO_B ||
			itermCode == PRODUCT_YEAR_AUTO_C { // 年费试用会员
			vipYear = YEAR_VIP
			break
		}

		// 以前是年会员 现在还没有过期 依旧是年会员
		if vipYear == YEAR_VIP && expiredTS > timeStampNow {
			vipYear = YEAR_VIP
			break
		}

		vipYear = YEAR_NORMAL
		break
	}

	// 已经过期的以当前购买的为准
	if itermCode == PRODUCT_LIFT_TIME ||
		itermCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
		itermCode == PRODUCT_LIFE_TIME_B ||
		itermCode == PRODUCT_LIFE_TIME_C {
		vipType = E_LIFETIME_VIP
	} else if itermCode == PRODUCT_YEAR_AUTO ||
		itermCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
		itermCode == PRODUCT_ONE_YEAR_SUB ||
		itermCode == PRODUCT_YEAR_AUTO_B ||
		itermCode == PRODUCT_YEAR_AUTO_C {
		vipType = E_YEARAUTO_VIP
	} else if itermCode == PRODUCT_ONE_MONTHAUTO ||
		itermCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
		itermCode == PRODUCT_ONE_MONTH_SUB ||
		itermCode == PRODUCT_ONE_MONTH_AUTO_B ||
		itermCode == PRODUCT_ONE_MONTH_AUTO_C {
		vipType = E_MONTHAUTO_VIP
	} else {
		vipType = E_DAYS_VIP
	}

	infoLog.Printf("UpdateAutoTypeVipExpiredTime uid=%v lastIterm=%v vipYear=%v vipType=%v expiredTS=%v",
		userId,
		lastIterm,
		vipYear,
		vipType,
		expiredTS)
	expiredTS = utilTimestamp
	err = DbUtil.UpdateUserVIPInfo(userId, itermCode, vipYear, vipType, expiredTS)
	if err != nil {
		infoLog.Printf("UpdateAutoTypeVipExpiredTime UpdateUserVIPInfo failed lastIterm=%v vipYear=%v vipType=%v expiredTS=%v err=%s",
			lastIterm,
			vipYear,
			vipType,
			expiredTS,
			err)
	} else {
		infoLog.Printf("UpdateAutoTypeVipExpiredTime UpdateUserVIPInfo ok lastIterm=%v vipYear=%v vipType=%v expiredTS=%v",
			lastIterm,
			vipYear,
			vipType,
			expiredTS)
	}
	return nil
}

func CountPostponeSeconds(productCode uint32) (outSecond uint64) {
	if productCode == PRODUCT_ONE_MONTH ||
		productCode == PRODUCT_ONE_MONTHAUTO ||
		productCode == PRODUCT_GIFT_ONEMONTH ||
		productCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
		productCode == PRODUCT_ONE_MONTH_AUTO_B ||
		productCode == PRODUCT_ONE_MONTH_AUTO_C {
		outSecond = 31 * 24 * 3600
	} else if productCode == PRODUCT_THREE_MONTH ||
		productCode == PRODUCT_GIFT_THREEMONTH {
		outSecond = 92 * 24 * 3600
	} else if productCode == PRODUCT_ONE_YEAR ||
		productCode == PRODUCT_GIFT_ONEYEAR ||
		productCode == PRODUCT_YEAR_AUTO ||
		productCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
		productCode == PRODUCT_YEAR_AUTO_B ||
		productCode == PRODUCT_YEAR_AUTO_C {
		outSecond = 365 * 24 * 3600
	} else if productCode == PRODUCT_LIFT_TIME ||
		productCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
		productCode == PRODUCT_LIFE_TIME_B ||
		productCode == PRODUCT_LIFE_TIME_C {
		// 终生会员做50年处理
		outSecond = 50 * 365 * 24 * 3600
	} else if productCode == PRODUCT_ONE_MONTH_SUB {
		outSecond = 7 * 24 * 3600 // 月会员试用 返回的天数UpdateVipExpiredTime没有使用
	} else if productCode == PRODUCT_ONE_YEAR_SUB {
		outSecond = 31 * 24 * 3600 // 年会员试用 返回的天数UpdateVipExpiredTime没有使用
	}
	return outSecond
}

func asyncDo(fn func(), wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		fn()
		wg.Done()
	}()
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		infoLog.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		infoLog.Fatalln("Must input config file name")
	}

	// infoLog.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		infoLog.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.infoLog")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		infoLog.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取IMServer 配置
	imCount := cfg.Section("IMSERVER").Key("imserver_cnt").MustInt(2)
	imConnLimit := cfg.Section("IMSERVER").Key("pool_limit").MustInt(1000)
	imServerPool = make(map[string]*common.ImServerApiV2, imCount)
	infoLog.Printf("IMServer Count=%v", imCount)
	for i := 0; i < imCount; i++ {
		ipKey := "imserver_ip_" + strconv.Itoa(i)
		ipOnlineKye := "imserver_ip_online_" + strconv.Itoa(i)
		portKey := "imserver_port_" + strconv.Itoa(i)
		imIp := cfg.Section("IMSERVER").Key(ipKey).MustString("127.0.0.1")
		imIpOnline := cfg.Section("IMSERVER").Key(ipOnlineKye).MustString("127.0.0.1")
		imPort := cfg.Section("IMSERVER").Key(portKey).MustString("18380")

		infoLog.Printf("im server ip=%v ip_online=%v port=%v", imIp, imIpOnline, imPort)
		imServerPool[imIpOnline] = common.NewImServerApiV2(imIp, imPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, imConnLimit)
	}
	infoLog.Printf("imServerPool=%v", imServerPool)

	// init dbUtil
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	DbUtil = util.NewDbUtil(db, infoLog)

	// 读取 Memcache Ip and port
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustInt(11211)
	infoLog.Printf("memcache ip=%v port=%v", mcIp, mcPort)
	mcApi = new(common.MemcacheApi)
	mcApi.Init(mcIp + ":" + strconv.Itoa(mcPort))

	// 读取user cache 配置
	userIp := cfg.Section("USERCACHE").Key("user_cache_ip").MustString("127.0.0.1")
	userPort := cfg.Section("USERCACHE").Key("user_cache_port").MustString("26000")
	userCacheConnLimit := cfg.Section("USERCACHE").Key("pool_limit").MustInt(1000)
	infoLog.Printf("user cache server ip=%v port=%v connLimit=%v", userIp, userPort, userCacheConnLimit)
	userCacheApi = common.NewUserCacheApi(userIp, userPort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, userCacheConnLimit)

	// 读取RabbitMQ的配置
	rabbitMQUrl = cfg.Section("RABBITMQ").Key("mq_url").MustString("amqp://pushServer:123456@10.243.134.240:5672/pushHost")
	infoLog.Printf("RabbitMQ URL=%s", rabbitMQUrl)

	appleChanLimit := cfg.Section("CHANLIMIT").Key("apple_chan").MustInt(1000)
	infoLog.Printf("ChanLimit apple_chan=%v", appleChanLimit)
	appleCheckChan = make(chan util.AppleRenewItem, appleChanLimit)
	closeChan = make(chan struct{})

	// check interval
	checkInterval = cfg.Section("CHECKINTER").Key("period").MustInt(300)
	waitGroup = &sync.WaitGroup{}

	// read appstore config
	productEnv := cfg.Section("APPSTORE").Key("product").MustBool(true)
	appTimeOut := cfg.Section("APPSTORE").Key("time_out").MustInt(2)
	infoLog.Printf("APPSTORE product=%v time_out=%v", productEnv, appTimeOut)
	appstoreConfig := appstore.Config{
		IsProduction: productEnv,
		TimeOut:      time.Second * time.Duration(appTimeOut),
	}
	appstoreClient = appstore.NewWithConfig(appstoreConfig)

	asyncDo(CheckAppleNeedRenewTransaction, waitGroup)
	asyncDo(CheckAppleStoreOrder, waitGroup)
	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)
	close(closeChan)
	return
}

func checkError(err error) {
	if err != nil {
		infoLog.Fatal(err)
	}
}
