package main

import (
	"crypto/md5"
	"database/sql"
	"fmt"
	"strings"

	//simplejson "github.com/bitly/go-simplejson"
	"encoding/json"

	"github.com/HT_GOGO/gotcp"
	nsq "github.com/nsqio/go-nsq"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_group_lesson"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_wallet"
	"github.com/HT_GOGO/gotcp/tcpfw/project/group_lesson/util"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

const (
	ProcSlowThreshold = 300000
	WhiteBoardName    = "whiteboard"
	RecordName        = "record"
	HangUpName        = "hangup"
)

type Callback struct{}

var (
	infoLog *log.Logger
	db      *sql.DB
	//ssdb        *gossdb.Connectors
	mongoSess          *mgo.Session
	DbUtil             *util.DbUtil
	groupLessonManager *util.GroupLessonManager
	mucApi             *common.MucApi
	voipApi            *common.VoipApi
	lessonMsgStoreApi  *common.LessonMsgStoreApi
	agoraRecordApi     *common.AgoraRecordApi
	userInfoCacheApi   *common.UserInfoCacheApi
	walletApi          *common.WalletApi
	filePath           string
)

func SubString(str string, begin, length int) (substr string) {
	// 将字符串的转换成[]rune
	rs := []rune(str)
	lth := len(rs)

	// 简单的越界判断
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length
	if end > lth {
		end = lth
	}

	// 返回子串
	return string(rs[begin:end])
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV3packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", head, len(packet.GetBody()))
	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}
	// 统计总的请求量
	attr := "grouplesson/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch head.Cmd {
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_CREATE_PERSONAL_LESSON_REQ):
		go ProcCreatePersonalLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_UPDATE_PERSONAL_LESSON_REQ):
		go ProcUpdatePersonalLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_DEL_PERSONAL_LESSON_REQ):
		go ProcDelPersonalLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_BATCH_GET_PERSONAL_LESSON_REQ):
		go ProcGetPersonalLessonByObid(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_CREATE_GROUP_LESSON_REQ):
		go ProcCreateGroupLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_UPDATE_GROUP_LESSON_REQ):
		go ProcUpdateGroupLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_DEL_GROUP_LESSON_REQ):
		go ProcDelGroupLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_BATCH_GET_GROUP_LESSON_REQ):
		go ProcGetGroupLessonByObid(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_BEGIN_GROUP_LESSON_REQ):
		go ProcBeginGroupLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_END_GROUP_LESSON_REQ):
		go ProcEndGroupLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_REQ_JOIN_GROUP_LESSON_REQ):
		go ProcJoinGroupLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GROUP_LESSON_PING_REQ):
		go ProcGroupLessonHeartBeat(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_REQ_LEAVE_GROUP_LESSON_REQ):
		go ProcLeaveGroupLesson(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GROUP_LESSON_MESSAGE_REQ):
		go ProcGroupLessonMessage(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_CHANGE_COURSEWARE_REQ):
		go ProcGroupLessonChangeCourseware(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_UPDATE_LESSON_RECORD_URL_REQ):
		go ProcUpdateGroupLessonRecordUrl(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_LESSON_BY_OBID):
		go ProcGetLessonByObid(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_LATEST_LESSON_MSG_REQ):
		go ProcGetLatestGroupLessonMsg(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_BATCH_GET_GROUP_LESSON_DETAIL_REQ):
		go ProcBatchGetGroupLessonDetailInfo(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_DEL_RECORD_REQ):
		go ProcDelRecord(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_QUERY_RECORD_STAT_REQ):
		go ProcQueryRecordStat(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_RECORD_BY_OBID_REQ):
		go ProcGetRecordByObid(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_LESSON_JSON_BY_OBID):
		go ProcGetGroupLessonJsonByObid(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_DEL_GROUP_LESSON_BY_UID):
		go ProcDelGroupLessonByUid(c, head, packet)
	case uint16(ht_group_lesson.GL_CMD_TYPE_CMD_QUERY_GROUP_LESSON_STAT):
		go ProcQueryGroupLessonStat(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
	}
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV3, rspBody *ht_group_lesson.GroupLessonRspBody, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendCreatRoomResp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 1.Proc create personal lesson
func ProcCreatePersonalLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcCreatePersonalLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.CreatePersonalLessonRspbody = &ht_group_lesson.CreatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcCreatePersonalLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/creat_personal_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcCreatePersonalLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.CreatePersonalLessonRspbody = &ht_group_lesson.CreatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetCreatePersonalLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcCreatePersonalLesson GetCreatePersonalLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.CreatePersonalLessonRspbody = &ht_group_lesson.CreatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	lessonItem := subReqBody.GetLessonItem()
	createrUid := lessonItem.GetCreaterUid()
	md5sum := md5.Sum(payLoad)
	var md5slice []byte = md5sum[:]
	md5Str := fmt.Sprintf("%x", string(md5slice))
	infoLog.Printf("ProcCreatePersonalLesson createrUid=%v md5=%s title=%s abstract=%s teacherAbstract=%s coursewareUrl=%s coverUrl=%s",
		createrUid,
		md5Str,
		lessonItem.GetLessonTitle(),
		lessonItem.GetLessonAbstract(),
		lessonItem.GetTeacherAbstract(),
		lessonItem.GetLessonCoursewareUrl(),
		lessonItem.GetCoverUrl())

	// Step1: 输入参数检查
	if createrUid == 0 ||
		len(lessonItem.GetLessonTitle()) == 0 ||
		len(lessonItem.GetLessonAbstract()) == 0 {
		infoLog.Printf("ProcCreatePersonalLesson invalid param createrUid=%v title=%s titleLen=%v abstract=%s teacherAbstract=%s coursewareUrl=%s coverUrl=%s",
			createrUid,
			lessonItem.GetLessonTitle(),
			len(lessonItem.GetLessonTitle()),
			lessonItem.GetLessonAbstract(),
			lessonItem.GetTeacherAbstract(),
			lessonItem.GetLessonCoursewareUrl(),
			lessonItem.GetCoverUrl())
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.CreatePersonalLessonRspbody = &ht_group_lesson.CreatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 创建个人课程
	obid, err := groupLessonManager.CreatePersonalLesson(
		createrUid,
		md5Str,
		lessonItem.GetLessonTitle(),
		lessonItem.GetLessonAbstract(),
		lessonItem.GetTeacherAbstract(),
		lessonItem.GetLessonCoursewareUrl(),
		lessonItem.GetCoverUrl())
	if err != nil {
		infoLog.Printf("ProcCreatePersonalLesson groupLessonManager.CreatePersonalLesson createUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s failed err=%s",
			createrUid,
			lessonItem.GetLessonTitle(),
			lessonItem.GetLessonAbstract(),
			lessonItem.GetTeacherAbstract(),
			lessonItem.GetLessonCoursewareUrl(),
			lessonItem.GetCoverUrl(),
			err)
		if lerr, ok := err.(*mgo.LastError); ok && lerr.Code == 11000 &&
			strings.Contains(lerr.Err, "E11000") {
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_REPEAT_ADD
			infoLog.Printf("ProcCreatePersonalLesson groupLessonManager.CreatePersonalLesson createUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s failed err=%#v repeat add",
				createrUid,
				lessonItem.GetLessonTitle(),
				lessonItem.GetLessonAbstract(),
				lessonItem.GetTeacherAbstract(),
				lessonItem.GetLessonCoursewareUrl(),
				lessonItem.GetCoverUrl(),
				*lerr)
		} else {
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		}

		rspBody.CreatePersonalLessonRspbody = &ht_group_lesson.CreatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcCreatePersonalLesson groupLessonManager.CreatePersonalLesson createUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s",
		createrUid,
		lessonItem.GetLessonTitle(),
		lessonItem.GetLessonAbstract(),
		lessonItem.GetTeacherAbstract(),
		lessonItem.GetLessonCoursewareUrl(),
		lessonItem.GetCoverUrl())

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.CreatePersonalLessonRspbody = &ht_group_lesson.CreatePersonalLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Obid: []byte(obid),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// 调用user_info_cache 接口更新用户创建群聊的时间戳
	err = userInfoCacheApi.UpdateUserLessonVer(createrUid)
	if err != nil {
		infoLog.Printf("ProcCreatePersonalLesson createUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s UpdateUserLessonVer failed err=%s",
			createrUid,
			lessonItem.GetLessonTitle(),
			lessonItem.GetLessonAbstract(),
			lessonItem.GetTeacherAbstract(),
			lessonItem.GetLessonCoursewareUrl(),
			lessonItem.GetCoverUrl(),
			err)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/create_personal_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.Proc update personal lesson
func ProcUpdatePersonalLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcUpdatePersonalLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdatePersonalLessonRspbody = &ht_group_lesson.UpdatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdatePersonalLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/update_personal_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdatePersonalLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.UpdatePersonalLessonRspbody = &ht_group_lesson.UpdatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdatePersonalLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdatePersonalLesson GetUpdatePersonalLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.UpdatePersonalLessonRspbody = &ht_group_lesson.UpdatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	lessonItem := subReqBody.GetLessonItem()
	obid := lessonItem.GetPersonalObid()
	infoLog.Printf("ProcUpdatePersonalLesson reqUid=%v obid=%s title=%s abstract=%s teacherAbstract=%s coursewareUrl=%s coverUrl=%s",
		reqUid,
		obid,
		lessonItem.GetLessonTitle(),
		lessonItem.GetLessonAbstract(),
		lessonItem.GetTeacherAbstract(),
		lessonItem.GetLessonCoursewareUrl(),
		lessonItem.GetCoverUrl())

	// Step1: 输入参数检查
	if reqUid == 0 ||
		len(obid) == 0 ||
		len(lessonItem.GetLessonTitle()) == 0 {
		infoLog.Printf("ProcUpdatePersonalLesson invalid param reqUid=%v obid=%s title=%s abstract=%s teacherAbstract=%s coursewareUrl=%s coverUrl=%s",
			reqUid,
			obid,
			lessonItem.GetLessonTitle(),
			lessonItem.GetLessonAbstract(),
			lessonItem.GetTeacherAbstract(),
			lessonItem.GetLessonCoursewareUrl(),
			lessonItem.GetCoverUrl())
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdatePersonalLessonRspbody = &ht_group_lesson.UpdatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2:更新个人课程
	err = groupLessonManager.UpdatePersonalLesson(
		reqUid,
		obid,
		lessonItem.GetLessonTitle(),
		lessonItem.GetLessonAbstract(),
		lessonItem.GetTeacherAbstract(),
		lessonItem.GetLessonCoursewareUrl(),
		lessonItem.GetCoverUrl())
	if err != nil {
		infoLog.Printf("ProcUpdatePersonalLesson groupLessonManager.UpdatePersonalLesson reqUid=%v obid=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s failed err=%s",
			reqUid,
			obid,
			lessonItem.GetLessonTitle(),
			lessonItem.GetLessonAbstract(),
			lessonItem.GetTeacherAbstract(),
			lessonItem.GetLessonCoursewareUrl(),
			lessonItem.GetCoverUrl(),
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdatePersonalLessonRspbody = &ht_group_lesson.UpdatePersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcUpdatePersonalLesson groupLessonManager.UpdatePersonalLesson reqUid=%v obid=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s lessonCoverUrl=%s",
		reqUid,
		obid,
		lessonItem.GetLessonTitle(),
		lessonItem.GetLessonAbstract(),
		lessonItem.GetTeacherAbstract(),
		lessonItem.GetLessonCoursewareUrl(),
		lessonItem.GetCoverUrl())

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.UpdatePersonalLessonRspbody = &ht_group_lesson.UpdatePersonalLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/update_personal_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 3.Proc del personal lesson
func ProcDelPersonalLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcDelPersonalLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.DelPersonalLessonRspbody = &ht_group_lesson.DelPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelPersonalLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/del_personal_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelPersonalLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.DelPersonalLessonRspbody = &ht_group_lesson.DelPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelPersonalLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelPersonalLesson GetDelPersonalLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.DelPersonalLessonRspbody = &ht_group_lesson.DelPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	obid := subReqBody.GetObid()
	infoLog.Printf("ProcDelPersonalLesson reqUid=%v obid=%s",
		reqUid,
		obid)

	// Step1: 输入参数检查
	if reqUid == 0 ||
		len(obid) == 0 {
		infoLog.Printf("ProcDelPersonalLesson invalid param reqUid=%v obid=%s",
			reqUid,
			obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.DelPersonalLessonRspbody = &ht_group_lesson.DelPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}

	// 删除个人课程之前 查询个人课程所运用的群聊 依次结束群聊中的课程收费
	groupLessonSlice, err := groupLessonManager.GetGroupLessonSlicByPersonalLessonObid(reqUid, obid)
	if err != nil {
		infoLog.Printf("ProcDelPersonalLesson reqUid=%v err=%s GetGroupLessonSlicByPersonalLessonObid failed",
			reqUid,
			err)
	} else {
		infoLog.Printf("ProcDelPersonalLesson reqUid=%v GetGroupLessonSlicByPersonalLessonObid success groupLessonSliceLen =%v",
			reqUid,
			len(groupLessonSlice))
		if len(groupLessonSlice) != 0 {
			var lessonList []*ht_wallet.StopLessonCharginItem
			for _, v := range groupLessonSlice {
				item := &ht_wallet.StopLessonCharginItem{
					GroupLessonObid: proto.String(v.GroupLessonObid),
					RoomId:          proto.Uint32(v.RoomId),
				}
				lessonList = append(lessonList, item)
			}
			err = walletApi.BatchStopLessonCharging(reqUid, lessonList)
			if err != nil {
				infoLog.Printf("ProcDelPersonalLesson reqUid=%v groupLessonSlice=%v BatchStopLessonCharging failed err=%s",
					reqUid,
					groupLessonSlice,
					err)
			} else {
				infoLog.Printf("ProcDelPersonalLesson reqUid=%v groupLessonSlice=%v BatchStopLessonCharging success",
					reqUid,
					groupLessonSlice)
			}
		}
	}

	// Step3: 删除个人课程需要判断当前是否有群正在上这个课如果有群正在上课 返回特定的返回码通知操作者
	err = groupLessonManager.DelPersonlLesson(reqUid, obid)
	if err != nil {
		infoLog.Printf("ProcDelPersonalLesson groupLessonManager.DelPersonlLesson reqUid=%v obid=%s failed err=%s",
			reqUid,
			obid,
			err)
		if err == util.ErrPersonalLessonIsLessoning {
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_LESSON_IS_IN_USE
			rspBody.DelPersonalLessonRspbody = &ht_group_lesson.DelPersonalLessonRspBody{
				Status: &ht_group_lesson.GroupLessonHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("personal lesson is using"),
				},
			}
			return false
		} else {
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
			rspBody.DelPersonalLessonRspbody = &ht_group_lesson.DelPersonalLessonRspBody{
				Status: &ht_group_lesson.GroupLessonHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}

	infoLog.Printf("OK ProcDelPersonalLesson groupLessonManager.DelPersonlLesson reqUid=%v obid=%s", reqUid, obid)

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.DelPersonalLessonRspbody = &ht_group_lesson.DelPersonalLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/del_personal_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetBeginHexString() string {
	timeBegin := time.Date(3000, time.Month(12), 12, 0, 0, 0, 0, time.Local)
	objId := bson.NewObjectIdWithTime(timeBegin)
	return objId.Hex()
}

// 4.Proc batch get personal lesson
func ProcGetPersonalLessonByObid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetPersonalLessonByObid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.BatchGetPersonalLessonRspbody = &ht_group_lesson.BatchGetPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetPersonalLessonByObid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/get_personal_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetPersonalLessonByObid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.BatchGetPersonalLessonRspbody = &ht_group_lesson.BatchGetPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetPersonalLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetPersonalLessonByObid GetBatchGetPersonalLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.BatchGetPersonalLessonRspbody = &ht_group_lesson.BatchGetPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	obid := subReqBody.GetCliObid()
	infoLog.Printf("ProcGetPersonalLessonByObid reqUid=%v cliObid=%s",
		reqUid,
		obid)

	if string(obid) == "0" {
		obid = []byte(GetBeginHexString())
		infoLog.Printf("ProcGetPersonalLessonByObid reqUid=%v cliObid=%s", reqUid, obid)
	}
	// Step1: 输入参数检查
	if reqUid == 0 ||
		len(obid) == 0 {
		infoLog.Printf("ProcGetPersonalLessonByObid invalid param reqUid=%v obid=%s",
			reqUid,
			obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.BatchGetPersonalLessonRspbody = &ht_group_lesson.BatchGetPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 删除个人课程
	lessonList, minObid, err := groupLessonManager.GetPersonalLessonByObid(reqUid, obid)
	if err != nil {
		infoLog.Printf("ProcGetPersonalLessonByObid groupLessonManager.GetPersonalLessonByObid reqUid=%v obid=%s failed err=%s",
			reqUid,
			obid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.BatchGetPersonalLessonRspbody = &ht_group_lesson.BatchGetPersonalLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcGetPersonalLessonByObid groupLessonManager.GetPersonalLessonByObid reqUid=%v obid=%s minObid=%s lessonList=%#v", reqUid, obid, minObid, lessonList)
	personLessonList := []*ht_group_lesson.PersonalLessonItem{}
	for _, v := range lessonList {
		item := &ht_group_lesson.PersonalLessonItem{
			PersonalObid:        []byte(v.Id.Hex()),
			CreaterUid:          proto.Uint32(v.CreaterUid),
			LessonTitle:         []byte(v.LessonTitle),
			LessonAbstract:      []byte(v.LessonAbstract),
			TeacherAbstract:     []byte(v.TeacherAbstract),
			LessonCoursewareUrl: []byte(v.LessonCoursewareUrl),
			CoverUrl:            []byte(v.CoverUrl),
			RoomIdList:          v.ApplyRoomId,
		}
		personLessonList = append(personLessonList, item)
	}
	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.BatchGetPersonalLessonRspbody = &ht_group_lesson.BatchGetPersonalLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MinObid:    minObid,
		LessonList: personLessonList,
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/get_personal_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 5.Proc create group lesson
func ProcCreateGroupLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcCreateGroupLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.CreateGroupLessonRspbody = &ht_group_lesson.CreateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcCreateGroupLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/_group_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcCreateGroupLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.CreateGroupLessonRspbody = &ht_group_lesson.CreateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetCreateGroupLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcCreateGroupLesson GetCreateGroupLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.CreateGroupLessonRspbody = &ht_group_lesson.CreateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	lessonItem := subReqBody.GetGroupLesson()
	createrUid := lessonItem.GetCreateUid()
	infoLog.Printf("ProcCreateGroupLesson createrUid=%v roomId=%v personalLessonObid=%s lessonTime=%s",
		createrUid,
		lessonItem.GetRoomId(),
		lessonItem.GetPersonalLessonObid(),
		lessonItem.GetLessonTime())

	// Step1: 输入参数检查
	if createrUid == 0 ||
		lessonItem.GetRoomId() == 0 ||
		len(lessonItem.GetPersonalLessonObid()) == 0 {
		infoLog.Printf("ProcCreateGroupLesson invalid param createrUid=%v roomId=%v personalLessonObid=%s lessonTime=%v",
			createrUid,
			lessonItem.GetRoomId(),
			lessonItem.GetPersonalLessonObid(),
			lessonItem.GetLessonTime())
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.CreateGroupLessonRspbody = &ht_group_lesson.CreateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 检查群聊课程中是否已经存在当前课程
	isApply, err := groupLessonManager.IsPersonalLessonApplyToGroup(lessonItem.GetPersonalLessonObid(), lessonItem.GetRoomId())
	if err != nil {
		infoLog.Printf("ProcCreateGroupLesson internal error createrUid=%v roomId=%v personalLessonObid=%s lessonTime=%s",
			createrUid,
			lessonItem.GetRoomId(),
			lessonItem.GetPersonalLessonObid(),
			lessonItem.GetLessonTime())
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.CreateGroupLessonRspbody = &ht_group_lesson.CreateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	if isApply {
		infoLog.Printf("ProcCreateGroupLesson createrUid=%v roomId=%v personalLessonObid=%s lessonTime=%s duplicate",
			createrUid,
			lessonItem.GetRoomId(),
			lessonItem.GetPersonalLessonObid(),
			lessonItem.GetLessonTime())
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_DUPLICATE_GROUP_LESSON
		rspBody.CreateGroupLessonRspbody = &ht_group_lesson.CreateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("duplicate lesson"),
			},
		}
		return false
	}
	// Step3: 如果个人课程没有在当前群聊中使用 创建个人课程
	obid, err := groupLessonManager.CreateGroupLesson(
		createrUid,
		lessonItem.GetRoomId(),
		lessonItem.GetPersonalLessonObid(),
		string(lessonItem.GetLessonTime()))
	if err != nil {
		infoLog.Printf("ProcCreateGroupLesson groupLessonManager.CreateGroupLesson createrUid=%v roomId=%v personalLessonObid=%s lessonTimeStamp=%v failed err=%s",
			createrUid,
			lessonItem.GetRoomId(),
			lessonItem.GetPersonalLessonObid(),
			lessonItem.GetLessonTime(),
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.CreateGroupLessonRspbody = &ht_group_lesson.CreateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcCreateGroupLesson groupLessonManager.CreateGroupLesson createrUid=%v roomId=%v personalLessonObid=%s lessonTimeStamp=%v",
		createrUid,
		lessonItem.GetRoomId(),
		lessonItem.GetPersonalLessonObid(),
		lessonItem.GetLessonTime())

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.CreateGroupLessonRspbody = &ht_group_lesson.CreateGroupLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		GroupLessonObid: []byte(obid),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/create_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 6.Proc update group lesson
func ProcUpdateGroupLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcUpdateGroupLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdateGroupLessonRspbody = &ht_group_lesson.UpdateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateGroupLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/update_group_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateGroupLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.UpdateGroupLessonRspbody = &ht_group_lesson.UpdateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateGroupLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateGroupLesson GetUpdateGroupLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.UpdateGroupLessonRspbody = &ht_group_lesson.UpdateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	groupLessonItem := subReqBody.GetGroupLesson()
	createrUid := groupLessonItem.GetCreateUid()
	groupLessonObid := groupLessonItem.GetGroupLessonObid()
	personalLessonItem := subReqBody.GetPersonalLesson()
	personalLessonObid := personalLessonItem.GetPersonalObid()
	infoLog.Printf("ProcUpdateGroupLesson createrUid=%v groupLessonObid=%s roomId=%v lessonTime=%s personalLessonObid=%s title=%s abstract=%s teacherAbstract=%s coursewareUrl=%s coverUrl=%s",
		createrUid,
		groupLessonObid,
		groupLessonItem.GetRoomId(),
		groupLessonItem.GetLessonTime(),
		personalLessonObid,
		personalLessonItem.GetLessonTitle(),
		personalLessonItem.GetLessonAbstract(),
		personalLessonItem.GetTeacherAbstract(),
		personalLessonItem.GetLessonCoursewareUrl(),
		personalLessonItem.GetCoverUrl())

	// Step1: 输入参数检查
	if createrUid == 0 ||
		len(groupLessonObid) == 0 ||
		len(personalLessonObid) == 0 {
		infoLog.Printf("ProcUpdateGroupLesson invalid param createrUid=%v groupLessonObid=%s lessonTime=%s personalLessonObid=%s title=%s abstract=%s teacherAbstract=%s coursewareUrl=%s coverUrl=%s",
			createrUid,
			groupLessonObid,
			groupLessonItem.GetLessonTime(),
			personalLessonObid,
			personalLessonItem.GetLessonTitle(),
			personalLessonItem.GetLessonAbstract(),
			personalLessonItem.GetTeacherAbstract(),
			personalLessonItem.GetLessonCoursewareUrl(),
			personalLessonItem.GetCoverUrl())
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdateGroupLessonRspbody = &ht_group_lesson.UpdateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 首先更新个人课程
	err = groupLessonManager.UpdatePersonalLesson(
		createrUid,
		personalLessonObid,
		personalLessonItem.GetLessonTitle(),
		personalLessonItem.GetLessonAbstract(),
		personalLessonItem.GetTeacherAbstract(),
		personalLessonItem.GetLessonCoursewareUrl(),
		personalLessonItem.GetCoverUrl())
	if err != nil {
		infoLog.Printf("ProcUpdateGroupLesson groupLessonManager.UpdatePersonalLesson createUid=%v obid=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s failed err=%s",
			createrUid,
			personalLessonObid,
			personalLessonItem.GetLessonTitle(),
			personalLessonItem.GetLessonAbstract(),
			personalLessonItem.GetTeacherAbstract(),
			personalLessonItem.GetLessonCoursewareUrl(),
			personalLessonItem.GetCoverUrl(),
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdateGroupLessonRspbody = &ht_group_lesson.UpdateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// Step3: 更新群组课程
	err = groupLessonManager.UpdateGroupLesson(groupLessonObid, createrUid, groupLessonItem.GetRoomId(), string(groupLessonItem.GetLessonTime()))
	if err != nil {
		infoLog.Printf("ProcUpdateGroupLesson groupLessonManager.UpdateGroupLesson createUid=%v obid=%s roomId=%v lessonTime=%s failed err=%s",
			createrUid,
			groupLessonObid,
			groupLessonItem.GetRoomId(),
			groupLessonItem.GetLessonTime(),
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdateGroupLessonRspbody = &ht_group_lesson.UpdateGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcUpdateGroupLesson UpdateGroupLesson createrUid=%v groupLessonObid=%s roomId=%v lessonTime=%s personalLessonObid=%s title=%s abstract=%s teacherAbstract=%s coursewareUrl=%s",
		createrUid,
		groupLessonObid,
		groupLessonItem.GetRoomId(),
		groupLessonItem.GetLessonTime(),
		personalLessonObid,
		personalLessonItem.GetLessonTitle(),
		personalLessonItem.GetLessonAbstract(),
		personalLessonItem.GetTeacherAbstract(),
		personalLessonItem.GetLessonCoursewareUrl())

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.UpdateGroupLessonRspbody = &ht_group_lesson.UpdateGroupLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/update_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 7.Proc del group lesson
func ProcDelGroupLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcDelGroupLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.DelGroupLessonRspbody = &ht_group_lesson.DelGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelGroupLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/del_group_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelGroupLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.DelGroupLessonRspbody = &ht_group_lesson.DelGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelGroupLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelGroupLesson GetDelGroupLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.DelGroupLessonRspbody = &ht_group_lesson.DelGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	createrUid := subReqBody.GetCreateUid()
	roomId := subReqBody.GetRoomId()
	obid := subReqBody.GetGroupLessonObid()
	infoLog.Printf("ProcDelGroupLesson createrUid=%v rooId=%v obid=%s",
		createrUid,
		roomId,
		obid)

	// Step1: 输入参数检查
	if createrUid == 0 ||
		roomId == 0 ||
		len(obid) == 0 {
		infoLog.Printf("ProcDelGroupLesson invalid param createrUid=%v roomId=%v obid=%s",
			createrUid,
			roomId,
			obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.DelGroupLessonRspbody = &ht_group_lesson.DelGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// 2018-02-05 检查此课程当前是否正在收费中如果是则提示用户当前群正在收费请停止收款再删除
	exist, err := walletApi.QueryLessonIsCharging(roomId, createrUid, string(obid))
	if err != nil {
		infoLog.Printf("ProcDelGroupLesson walletApi.QueryLessonIsCharging failed roomId=%v createUid=%v lessonObid=%s", roomId, createrUid, obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.DelGroupLessonRspbody = &ht_group_lesson.DelGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcDelGroupLesson roomId=%v createUid=%v lessonObid=%s is charging=%v", roomId, createrUid, obid, exist)
	if exist {
		infoLog.Printf("ProcDelGroupLesson lesson is charging roomId=%v createUid=%v lessonObid=%s", roomId, createrUid, obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_LESSON_IS_CHARGING
		rspBody.DelGroupLessonRspbody = &ht_group_lesson.DelGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("lesson is charging"),
			},
		}
		return false
	}
	// Step2: 删除群组课程
	err = groupLessonManager.DelGroupLesson(obid, createrUid, roomId)
	if err != nil {
		infoLog.Printf("ProcDelGroupLesson groupLessonManager.DelGroupLesson createUid=%v roomId=%v obid=%s failed err=%s",
			createrUid,
			roomId,
			obid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.DelGroupLessonRspbody = &ht_group_lesson.DelGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcDelGroupLesson groupLessonManager.DelGroupLesson createUid=%v roomId=%v obid=%s", createrUid, roomId, obid)

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.DelGroupLessonRspbody = &ht_group_lesson.DelGroupLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/del_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 8.Proc batch get group lesson
func ProcGetGroupLessonByObid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetGroupLessonByObid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.BatchGetGroupLessonRspbody = &ht_group_lesson.BatchGetGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetGroupLessonByObid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/get_group_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetGroupLessonByObid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.BatchGetGroupLessonRspbody = &ht_group_lesson.BatchGetGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetGroupLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetGroupLessonByObid GetBatchGetGroupLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.BatchGetGroupLessonRspbody = &ht_group_lesson.BatchGetGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	roomId := subReqBody.GetRoomId()
	obid := subReqBody.GetCliObid()
	infoLog.Printf("ProcGetGroupLessonByObid reqUid=%v roomId=%v cliObid=%s",
		reqUid,
		roomId,
		obid)

	if string(obid) == "0" {
		obid = []byte(GetBeginHexString())
		infoLog.Printf("ProcGetGroupLessonByObid reqUid=%v cliObid=%s", reqUid, obid)
	}

	// Step1: 输入参数检查
	if reqUid == 0 ||
		roomId == 0 ||
		len(obid) == 0 {
		infoLog.Printf("ProcGetGroupLessonByObid invalid param reqUid=%v roomId=%v obid=%s",
			reqUid,
			roomId,
			obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.BatchGetGroupLessonRspbody = &ht_group_lesson.BatchGetGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 删除个人课程
	lessonList, minObid, err := groupLessonManager.GetGroupLessonByObid(reqUid, roomId, obid)
	if err != nil {
		infoLog.Printf("ProcGetGroupLessonByObid groupLessonManager.GetPersonalLessonByObid reqUid=%v roomId=%v obid=%s failed err=%s",
			reqUid,
			roomId,
			obid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.BatchGetGroupLessonRspbody = &ht_group_lesson.BatchGetGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcGetGroupLessonByObid groupLessonManager.GetGroupLessonByObid reqUid=%v roomId=%v obid=%s minObid=%s lessonList=%#v", reqUid, roomId, obid, minObid, lessonList)
	groupLessonList := []*ht_group_lesson.GroupLessonItem{}
	for _, v := range lessonList {
		recordList := []*ht_group_lesson.RecordUrl{}
		// 根据群课程obid取课程录像列表
		recordListStore, err := groupLessonManager.GetRecordListByGroupLessonObid(v.RoomId, []byte(v.Id.Hex()))
		if err != nil {
			infoLog.Printf("ProcGetGroupLessonByObid groupLessonManager.GetRecordListByGroupLessonObid reqUid=%v cliObid=%s err=%s",
				reqUid,
				v.Id.Hex(),
				err)
			attr := "grouplesson/get_record_list_failed"
			libcomm.AttrAdd(attr, 1)
		}

		for _, j := range recordListStore {
			urlItem := &ht_group_lesson.RecordUrl{
				RecordObid:     []byte(j.Id.Hex()),
				VoiceUrl:       []byte(j.VoiceUrl),
				PptUrl:         []byte(j.PPTUrl),
				BeginTimeStamp: proto.Uint64(j.StartTimeStamp),
			}
			recordList = append(recordList, urlItem)
		}
		item := &ht_group_lesson.GroupLessonItem{
			GroupLessonObid:    []byte(v.Id.Hex()),
			CreateUid:          proto.Uint32(v.CreaterUid),
			RoomId:             proto.Uint32(v.RoomId),
			PersonalLessonObid: []byte(v.PersonalLessonObid),
			LessonTime:         []byte(v.LessonTime),
			RecordUrlList:      recordList,
		}
		groupLessonList = append(groupLessonList, item)
	}
	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.BatchGetGroupLessonRspbody = &ht_group_lesson.BatchGetGroupLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MinObid:    minObid,
		LessonList: groupLessonList,
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/get_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 9.Proc begin teaching room
func ProcBeginGroupLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcBeginGroupLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBeginGroupLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/begin_group_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBeginGroupLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBeginGroupLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBeginGroupLesson GetBeginGroupLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	teacherUid := subReqBody.GetTeacherUid()
	teacherName := subReqBody.GetTeacherName()
	roomId := subReqBody.GetRoomId()
	channelId := subReqBody.GetChannelId()
	groupLessonObid := subReqBody.GetGroupLessonObid()
	firstUrl := subReqBody.GetFirstUrl()
	lessonTitle := subReqBody.GetLessonTitle()
	lessonAbstract := subReqBody.GetLessonAbstract()
	teacherAbstract := subReqBody.GetTeacherAbstract()
	lessonCoursewareurl := subReqBody.GetLessonCoursewareUrl()
	lessonTime := subReqBody.GetLessonTime()
	infoLog.Printf("ProcBeginGroupLesson teacherUid=%v teacherName=%s rooId=%v channelId=%s groupLessonObid=%s firstUrl=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareurl=%s lessonTime=%s",
		teacherUid,
		teacherName,
		roomId,
		channelId,
		groupLessonObid,
		firstUrl,
		lessonTitle,
		lessonAbstract,
		teacherAbstract,
		lessonCoursewareurl,
		lessonTime)

	// Step1: 输入参数检查
	if teacherUid == 0 || len(teacherName) == 0 || roomId == 0 || len(channelId) == 0 || len(groupLessonObid) == 0 || len(lessonTitle) == 0 {
		infoLog.Printf("ProcBeginGroupLesson invalid param teacherUid=%v teacherName=%s rooId=%v channelId=%s groupLessonObid=%s firstUrl=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareurl=%s",
			teacherUid,
			teacherName,
			roomId,
			channelId,
			groupLessonObid,
			firstUrl,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareurl)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}

	// Step2: 检查当前是否已经正在进行教学
	ok, err := groupLessonManager.IsRoomLessoning(roomId)
	if err != nil {
		infoLog.Printf("ProcBeginGroupLesson groupLessonManager.IsRoomLessoning roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// 教学房间正在进行教学 返回课程已经开始了
	if ok {
		infoLog.Printf("ProcBeginGroupLesson teacherUid=%v teacherName=%s rooId=%v obid=%s channelId=%v is lessoning",
			teacherUid,
			teacherName,
			roomId,
			groupLessonObid,
			channelId)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_LESSION_ALREADY_BEGIN
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("already begin"),
			},
		}
		return false
	}
	// Step3: 查询muc申请用户是否管理员
	isAdmin, err := mucApi.IsUserAdmin(roomId, teacherUid)
	if err != nil || isAdmin == false {
		infoLog.Printf("ProcBeginGroupLesson roomId=%v createUid=%v mucApi.IsUserAdmin isAdmin=%v err=%s",
			roomId,
			teacherUid,
			isAdmin,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_NO_PERMISSION
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("no permission"),
			},
		}
		return false
	}
	// Step4: 查询voip 当前房间是否正在进行群语音
	xtHead := &common.XTHead{
		Flag:     head.Flag,
		Version:  head.Version,
		CryKey:   head.CryKey,
		TermType: head.TermType,
		Cmd:      head.Cmd,
		Seq:      head.Seq,
		From:     head.From,
		To:       head.To,
		Len:      0,
	}
	isVoipping, err := voipApi.GetRoomIsVoipping(xtHead, roomId)
	if err != nil || isVoipping == true {
		infoLog.Printf("ProcBeginGroupLesson roomId=%v createUid=%v voipApi.GetRoomIsVoipping isVoipping=%v err=%s",
			roomId,
			teacherUid,
			isVoipping,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_IS_VOIPPING
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("is voipping"),
			},
		}
		return false
	}

	// Step5:记录当前群聊课程的状态
	memberInfo := &util.MemberDetailInfo{
		Uid:          teacherUid,
		NickName:     teacherName,
		HeadPhotoUrl: nil,
		Country:      nil,
	}
	recordObid, beginTime, err := groupLessonManager.AddGroupLessonInfo(roomId,
		memberInfo,
		channelId,
		groupLessonObid,
		firstUrl,
		lessonTitle,
		lessonAbstract,
		teacherAbstract,
		lessonCoursewareurl)
	if err != nil {
		infoLog.Printf("ProcBeginGroupLesson groupLessonManager.AddGroupLessonInfo teacherUid=%v teacherName=%s rooId=%v channelId=%s groupLessonObid=%s firstUrl=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareurl=%s err=%s",
			teacherUid,
			teacherName,
			roomId,
			channelId,
			groupLessonObid,
			firstUrl,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareurl,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcBeginGroupLesson groupLessonManager.AddGroupLessonInfo teacherUid=%v teacherName=%s rooId=%v channelId=%s groupLessonObid=%s firstUrl=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareurl=%s beginTime=%v",
		teacherUid,
		teacherName,
		roomId,
		channelId,
		groupLessonObid,
		firstUrl,
		lessonTitle,
		lessonAbstract,
		teacherAbstract,
		lessonCoursewareurl,
		beginTime)
	// Step6: 广播群组课程开始
	retCode, err := mucApi.BroadCastBeginGroupLesson(roomId,
		teacherUid,
		teacherName,
		channelId,
		groupLessonObid,
		firstUrl,
		lessonTitle,
		lessonAbstract,
		teacherAbstract,
		lessonCoursewareurl,
		beginTime,
		lessonTime)

	if err != nil || retCode != 0 {
		attr := "grouplesson/req_bc_begin_lesson_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcBeginGroupLesson mucApi.BroadCastBeginGroupLesson teacherUid=%v teacherName=%s rooId=%v channelId=%s groupLessonObid=%s firstUrl=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareurl=%s err=%s",
			teacherUid,
			teacherName,
			roomId,
			channelId,
			groupLessonObid,
			firstUrl,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareurl,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("broad cast failed"),
			},
		}
		return false
	}

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.BeginGroupLessonRspbody = &ht_group_lesson.BeginGroupLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		LessonBeginTime: proto.Uint64(beginTime),
		RecordObid:      []byte(recordObid),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step8: 通知Record 进程进行录像
	err = agoraRecordApi.StartNewRecord(teacherUid, roomId, channelId)
	if err != nil {
		infoLog.Printf("ProcBeginGroupLesson teacherUid=%v roomId=%v channelId=%s failed err=%s",
			teacherUid,
			roomId,
			channelId,
			err)
		attr := "grouplesson/start_record_failed"
		libcomm.AttrAdd(attr, 1)
	}
	// Step9: 开始记录PPT录像和课程录像
	roomInfo, err := groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcBeginGroupLesson groupLessonManager.GetRoomInfoByRoomId roomId=%v err=%s", roomId, err)
		return false
	}

	fileName, err := groupLessonManager.SavePPTRecordToFile(filePath, string(recordObid), roomInfo.PPTRecored)
	infoLog.Printf("ProcBeginGroupLesson roomId=%v fileName=%s isSuccess=%v err=%s",
		roomId,
		fileName,
		err)
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/begin_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 10.Proc end group lesson
func ProcEndGroupLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcEndGroupLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcEndGroupLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/end_group_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcEndGroupLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetEndGroupLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcEndGroupLesson GetEndGroupLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	teacherUid := subReqBody.GetTeacherUid()
	teacherName := subReqBody.GetTeacherName()
	roomId := subReqBody.GetRoomId()
	obid := subReqBody.GetGroupLessonObid()
	channelId := subReqBody.GetChannelId()
	infoLog.Printf("ProcEndGroupLesson teacherUid=%v teacherName=%s roomId=%v obid=%s channelId=%s",
		teacherUid,
		teacherName,
		roomId,
		obid,
		channelId)

	// Step1: 输入参数检查
	if teacherUid == 0 ||
		roomId == 0 ||
		len(obid) == 0 ||
		len(channelId) == 0 {
		infoLog.Printf("ProcEndGroupLesson invalid param teacherUid=%v teacherName=%s roomId=%v obid=%s channelId=%s",
			teacherUid,
			teacherName,
			roomId,
			obid,
			channelId)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 检查该群是否正在上课
	ret, err := groupLessonManager.IsRoomLessoning(roomId)
	if err != nil {
		infoLog.Printf("ProcEndGroupLesson teachingRoomManager.IsRoomLessoning  roomId=%v reqUid=%v err=%s",
			roomId,
			teacherUid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	if !ret {
		infoLog.Printf("ProcEndGroupLesson roomId=%v reqUid=%v is not teaching",
			roomId,
			teacherUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("not teaching"),
			},
		}
		return true
	}
	// Step3: check whether req uid is room create
	roomInfo, err := groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcEndGroupLesson groupLessonManager.GetRoomInfoByRoomId roomId=%v err=%s", roomId, err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	if teacherUid != roomInfo.CreateUid.Uid {
		infoLog.Printf("ProcEndGroupLesson roomId=%v reqUid=%v createUid=%v not equal",
			roomId,
			teacherUid,
			roomInfo.CreateUid.Uid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step2: 删除群聊
	endTime, err := groupLessonManager.DelRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcEndGroupLesson groupLessonManager.DelRoomInfo teacherUid=%v teacherName=%s roomId=%v obid=%s channelId=%s err=%s",
			teacherUid,
			teacherName,
			roomId,
			obid,
			channelId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcEndGroupLesson groupLessonManager.DelRoomInfo teacherUid=%v teacherName=%s roomId=%v obid=%s channelId=%s err=%s",
		teacherUid,
		teacherName,
		roomId,
		obid,
		channelId)
	// Step3: 广播群课程结束
	retCode, err := mucApi.BroadCastEndGroupLesson(
		roomId,
		teacherUid,
		teacherName,
		channelId,
		endTime)
	if err != nil || retCode != 0 {
		attr := "grouplesson/req_bc_end_lesson_failed"
		libcomm.AttrAdd(attr, 1)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("broad cast failed"),
			},
		}
		return false
	}

	// Step4: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.EndGroupLessonRspbody = &ht_group_lesson.EndGroupLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	// Step5:结束agora record
	err = agoraRecordApi.EndRecord(teacherUid, roomId, channelId, roomInfo.RecordObid)
	if err != nil {
		infoLog.Printf("ProcEndGroupLesson teacherUid=%v roomId=%v channelId=%s EndRecord failed err=%s",
			teacherUid,
			roomId,
			channelId,
			err)
		attr := "grouplesson/end_record_failed"
		libcomm.AttrAdd(attr, 1)
	}
	// 记录课程结束时间
	endTimeStamp := uint64(time.Now().UnixNano() / 1000000)
	// Step6:记录并上传ppt文件到阿里云
	fileName, err := groupLessonManager.SavePPTRecordToFile(filePath, string(roomInfo.RecordObid), roomInfo.PPTRecored)
	infoLog.Printf("ProcEndGroupLesson roomId=%v fileName=%s isSuccess=%v err=%s",
		roomId,
		fileName,
		err)
	if err == nil {
		fullFileName, err := groupLessonManager.UploadToOSS(fileName, roomId)
		infoLog.Printf("ProcEndGroupLesson roomId=%v fileName=%s UploadToOSS err=%s",
			roomId,
			fileName,
			err)
		if err == nil {
			err = groupLessonManager.UpdateGroupLessonRecordPPTUrl(roomId, teacherUid, string(roomInfo.RecordObid), fullFileName, endTimeStamp)
			if err != nil {
				attr := "grouplesson/update_group_lesson_ppt_url_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcEndGroupLesson teacherUid=%v teacherName=%s roomId=%v obid=%s channelId=%s groupLessonManager.UpdateGroupLessonRecordPPTUrl err=%s",
					teacherUid,
					teacherName,
					roomId,
					roomInfo.RecordObid,
					channelId,
					err)
			}
		} else {
			attr := "grouplesson/upload_group_lesson_ppt_failed"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		attr := "grouplesson/save_group_lesson_ppt_failed"
		libcomm.AttrAdd(attr, 1)
	}

	// 老是结束课程同样需要上报
	createrUid := roomInfo.OriginLessonCreaterUid
	isTeacher := uint32(1)
	inTime := uint64(roomInfo.AlreadyInList[teacherUid].JoinTs / 1000000) // 将纳秒转换成毫秒
	outTime := uint64(time.Now().UnixNano() / 1000000)
	infoLog.Printf("ProcEndGroupLesson PublishGroupLessonOpLog reqUid=%v createrUid=%v isTeacher=%v inTime=%v outTime=%v, groupLessonObid=%s recordObid=%s",
		teacherUid,
		createrUid,
		isTeacher,
		inTime,
		outTime,
		roomInfo.GroupLessonObid,
		roomInfo.RecordObid)
	err = groupLessonManager.PublishGroupLessonOpLog(teacherUid, createrUid, isTeacher, inTime, outTime, string(roomInfo.GroupLessonObid), string(roomInfo.RecordObid))
	if err != nil {
		infoLog.Printf("ProcEndGroupLesson groupLessonManager.PublishGroupLessonOpLog roomId=%v reqUid=%v channelId=%s failed err=%s",
			roomId,
			teacherUid,
			channelId,
			err)
		attr := "grouplesson/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
	}

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/end_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 11.Proc req join group lesson
func ProcJoinGroupLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcJoinGroupLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcJoinGroupLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/join_group_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcJoinGroupLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetJoinGroupLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcJoinGroupLesson GetJoinGroupLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	channelId := subReqBody.GetChannelId()
	infoLog.Printf("ProcJoinGroupLesson roomId=%v reqUid=%v channelId=%s",
		roomId,
		reqUid,
		channelId)

	// Step1: 输入参数检查
	if reqUid == 0 ||
		roomId == 0 ||
		len(channelId) == 0 {
		infoLog.Printf("ProcJoinGroupLesson invalid param roomId=%v reqUid=%v channelId=%s",
			roomId,
			reqUid,
			channelId)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 检查房间是否正在上群组课
	ret, err := groupLessonManager.IsRoomLessoning(roomId)
	if err != nil {
		infoLog.Printf("ProcJoinGroupLesson teachingRoomManager.IsRoomLessoning roomId=%v reqUid=%v err=%s",
			roomId,
			reqUid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	if !ret {
		infoLog.Printf("ProcJoinGroupLesson roomId=%v reqUid=%v is not teaching",
			roomId,
			reqUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_LESSON_IS_OVER
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("not teaching"),
			},
		}
		return true
	}
	// Step3: 检查用户是否已经在群语音中
	roomInfo, err := groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcJoinGroupLesson teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	alreadyInList := roomInfo.AlreadyInList
	ret = isUserAlreadyIn(reqUid, alreadyInList)
	if ret { // 用户已经存在，直接返回成功
		infoLog.Printf("ProcJoinGroupLesson roomId=%v reqUid=%v already in",
			roomId,
			reqUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_USER_ALREADT_IN
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("already in"),
			},
		}
		return true
	}

	// Stpe4: add req user to room
	err = groupLessonManager.AddReqUserToAlreadyInList(roomId, reqUid)
	if err != nil {
		infoLog.Printf("ProcJoinGroupLesson groupLessonManager.AddReqUserToAlreadyInList roomId=%v reqUid=%v channelId=%s failed err=%s",
			roomId,
			reqUid,
			channelId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcJoinGroupLesson groupLessonManager.AddReqUserToAlreadyInList roomId=%v reqUid=%v channelId=%s", roomId, reqUid, channelId)

	// Step5: 广播群组课程状态变更
	roomInfo, err = groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcJoinGroupLesson teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	memberList := groupLessonManager.GetSortUidListFromAlreadyIn(roomInfo.AlreadyInList)
	infoLog.Printf("ProcJoinGroupLesson roomId=%v reqUid=%v channelId=%s memberList=%v",
		roomId,
		reqUid,
		channelId,
		memberList)
	retCode, err := mucApi.BroadCastGroupLessonStatChange(
		roomId,
		reqUid,
		memberList,
		roomInfo.LessonCurrentUrl,
		uint64(time.Now().UnixNano()/1000000))
	if err != nil || retCode != 0 {
		attr := "grouplesson/req_bc_group_lesson_stat_failed"
		libcomm.AttrAdd(attr, 1)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("broad cast failed"),
			},
		}
		infoLog.Printf("ProcJoinGroupLesson roomId=%v reqUid=%v retCode=%v err=%s", roomId, reqUid, retCode, err)
		return false
	}

	// Step6: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.JoinGroupLessonRspbody = &ht_group_lesson.JoinGroupLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		AlreadyInList:     memberList,
		CurrentPptUrl:     []byte(roomInfo.LessonCurrentUrl),
		BeginTimeStamp:    proto.Uint64(roomInfo.BeginTime),
		CurrentSystemTime: proto.Uint64(uint64(time.Now().UnixNano() / 1000000)),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/join_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}
func isUserAlreadyIn(reqUid uint32, alreadyInMap map[uint32]*util.MemberSimpleInfo) (ret bool) {
	ret = false
	if reqUid == 0 || len(alreadyInMap) == 0 {
		return ret
	}
	if _, ok := alreadyInMap[reqUid]; ok {
		ret = true
	}
	return ret
}

func isUserAlreadyInAdminList(reqUid uint32, alreadyInSlic []uint32) (ret bool) {
	ret = false
	if reqUid == 0 || len(alreadyInSlic) == 0 {
		return ret
	}
	for _, v := range alreadyInSlic {
		if v == reqUid {
			ret = true
			return ret
		}
	}
	return ret
}

// 12.proc heart beat
func ProcGroupLessonHeartBeat(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGroupLessonHeartBeat no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GroupLessonHeartBeatRspbody = &ht_group_lesson.GroupLessonHeartBeatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGroupLessonHeartBeat invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/heart_beat_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGroupLessonHeartBeat proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GroupLessonHeartBeatRspbody = &ht_group_lesson.GroupLessonHeartBeatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGroupLessonHeartBeatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGroupLessonHeartBeat GetGroupLessonHeartBeatReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GroupLessonHeartBeatRspbody = &ht_group_lesson.GroupLessonHeartBeatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	// Step1: param check
	if roomId == 0 || reqUid == 0 {
		infoLog.Printf("ProcGroupLessonHeartBeat invalid param roomId=%v reqUid=%v",
			roomId,
			reqUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.GroupLessonHeartBeatRspbody = &ht_group_lesson.GroupLessonHeartBeatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	// Step4: 返回响应
	rspBody.GroupLessonHeartBeatRspbody = &ht_group_lesson.GroupLessonHeartBeatRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	ret := groupLessonManager.HandleHeartBeat(roomId, reqUid)
	infoLog.Printf("ProcGroupLessonHeartBeat roomId=%v reqUid=%v teachingRoomManager.HandleHeartBeat ret=%v",
		roomId,
		reqUid,
		ret)
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/heart_beat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 13.Proc req leave group lesson
func ProcLeaveGroupLesson(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcLeaveGroupLesson no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcLeaveGroupLesson invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/leave_group_lesson_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcLeaveGroupLesson proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetLeaveGroupLessonReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcLeaveGroupLesson GetLeaveGroupLessonReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	channelId := subReqBody.GetChannelId()
	infoLog.Printf("ProcLeaveGroupLesson roomId=%v reqUid=%v channelId=%s",
		roomId,
		reqUid,
		channelId)

	// Step1: 输入参数检查
	if reqUid == 0 ||
		roomId == 0 ||
		len(channelId) == 0 {
		infoLog.Printf("ProcLeaveGroupLesson invalid param roomId=%v reqUid=%v channelId=%s",
			roomId,
			reqUid,
			channelId)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 检查用户是否已经在群语音中
	roomInfo, err := groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcLeaveGroupLesson teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		if err == util.ErrNotExist {
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_LESSON_IS_OVER
			rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
				Status: &ht_group_lesson.GroupLessonHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("lesson is over"),
				},
			}
		} else {
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
			rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
				Status: &ht_group_lesson.GroupLessonHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
		}

		return false
	}

	alreadyInList := roomInfo.AlreadyInList
	ret := isUserAlreadyIn(reqUid, alreadyInList)
	if !ret { // 用户已经存在，直接返回成功
		infoLog.Printf("ProcLeaveGroupLesson roomId=%v reqUid=%v already leave",
			roomId,
			reqUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_USER_ALREADT_IN
		rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("already leave"),
			},
		}
		return true
	}

	// Stpe3: remove user from already in list
	inTime, err := groupLessonManager.RemoveReqUserFromAlreadyInList(roomId, reqUid)
	if err != nil {
		infoLog.Printf("ProcLeaveGroupLesson groupLessonManager.RemoveReqUserFromAlreadyInList roomId=%v reqUid=%v channelId=%s failed err=%s",
			roomId,
			reqUid,
			channelId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	inTime = inTime / 1000000 // 将纳秒转换成毫秒
	outTime := uint64(time.Now().UnixNano() / 1000000)

	infoLog.Printf("OK ProcLeaveGroupLesson groupLessonManager.RemoveReqUserFromAlreadyInList roomId=%v reqUid=%v channelId=%s", roomId, reqUid, channelId)

	// Step5: 广播群组课程状态变更
	roomInfo, err = groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcLeaveGroupLesson teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	memberList := groupLessonManager.GetSortUidListFromAlreadyIn(roomInfo.AlreadyInList)
	infoLog.Printf("ProcLeaveGroupLesson roomId=%v reqUid=%v channelId=%s memberList=%v",
		roomId,
		reqUid,
		channelId,
		memberList)
	retCode, err := mucApi.BroadCastGroupLessonStatChange(
		roomId,
		reqUid,
		memberList,
		roomInfo.LessonCurrentUrl,
		uint64(time.Now().UnixNano()/1000000))
	if err != nil || retCode != 0 {
		attr := "grouplesson/req_bc_group_lesson_stat_failed"
		libcomm.AttrAdd(attr, 1)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("broad cast failed"),
			},
		}
		return false
	}

	// Step6: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.LeaveGroupLessonRspbody = &ht_group_lesson.LeaveGroupLessonRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	createrUid := roomInfo.OriginLessonCreaterUid
	isTeacher := uint32(0)
	if reqUid == roomInfo.CreateUid.Uid {
		isTeacher = 1
	}
	err = groupLessonManager.PublishGroupLessonOpLog(reqUid, createrUid, isTeacher, inTime, outTime, string(roomInfo.GroupLessonObid), string(roomInfo.RecordObid))
	if err != nil {
		infoLog.Printf("ProcLeaveGroupLesson groupLessonManager.PublishGroupLessonOpLog roomId=%v reqUid=%v channelId=%s failed err=%s",
			roomId,
			reqUid,
			channelId,
			err)
		attr := "grouplesson/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/leave_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 14.Proc group lesson message
func ProcGroupLessonMessage(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGroupLessonMessage no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GroupLessonMessageRspbody = &ht_group_lesson.GroupLessonMessageRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGroupLessonMessage invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/group_lesson_msg_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGroupLessonMessage proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GroupLessonMessageRspbody = &ht_group_lesson.GroupLessonMessageRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGroupLessonMessageReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGroupLessonMessage GetGroupLessonMessageReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GroupLessonMessageRspbody = &ht_group_lesson.GroupLessonMessageRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	senderUid := subReqBody.GetSenderUid()
	msg := subReqBody.GetMessgae()
	infoLog.Printf("ProcGroupLessonMessage roomId=%v senderUid=%v msgLen=%v",
		roomId,
		senderUid,
		len(msg))

	// Step1: 输入参数检查
	if senderUid == 0 ||
		roomId == 0 ||
		len(msg) == 0 {
		infoLog.Printf("ProcGroupLessonMessage invalid param roomId=%v senderUid=%v msgLen=%v",
			roomId,
			senderUid,
			len(msg))
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GroupLessonMessageRspbody = &ht_group_lesson.GroupLessonMessageRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 检查用户是否已经在群语音中
	roomInfo, err := groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcGroupLessonMessage teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.GroupLessonMessageRspbody = &ht_group_lesson.GroupLessonMessageRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	alreadyInList := roomInfo.AlreadyInList
	ret := isUserAlreadyIn(senderUid, alreadyInList)
	if !ret { // 用户不存在 直接返回错误
		infoLog.Printf("ProcGroupLessonMessage roomId=%v senderUid=%v already leave",
			roomId,
			senderUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_USER_ALREADT_IN
		rspBody.GroupLessonMessageRspbody = &ht_group_lesson.GroupLessonMessageRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("already leave"),
			},
		}
		return true
	}

	// Step3: 广播群组课程消息
	memberList := []uint32{}
	for _, v := range roomInfo.AlreadyInList {
		memberList = append(memberList, v.Uid)
	}
	infoLog.Printf("ProcGroupLessonMessage roomId=%v senderUid=%v memberList=%v",
		roomId,
		senderUid,
		memberList)
	retCode, err := mucApi.BroadCastGroupLessonMessage(roomId,
		senderUid,
		roomInfo.RecordObid,
		msg,
		memberList,
		uint64(time.Now().UnixNano()/1000000))
	if err != nil || retCode != 0 {
		attr := "grouplesson/req_bc_group_lesson_msg_failed"
		libcomm.AttrAdd(attr, 1)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.GroupLessonMessageRspbody = &ht_group_lesson.GroupLessonMessageRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("broad cast failed"),
			},
		}
		return false
	}

	// Step6: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.GroupLessonMessageRspbody = &ht_group_lesson.GroupLessonMessageRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/group_lesson_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 15.Proc change lesson message courseware url
func ProcGroupLessonChangeCourseware(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGroupLessonChangeCourseware no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGroupLessonChangeCourseware invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/group_lesson_change_url_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGroupLessonChangeCourseware proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetChangeCoursewareReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGroupLessonChangeCourseware GetChangeCoursewareReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	url := subReqBody.GetCoursewareUrl()
	infoLog.Printf("ProcGroupLessonChangeCourseware roomId=%v opUid=%v url=%s",
		roomId,
		opUid,
		url)

	// Step1: 输入参数检查
	if opUid == 0 ||
		roomId == 0 ||
		len(url) == 0 {
		infoLog.Printf("ProcGroupLessonChangeCourseware invalid param roomId=%v opUid=%v url=%v",
			roomId,
			opUid,
			url)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 检查用户是否是群课程老师
	roomInfo, err := groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcGroupLessonChangeCourseware teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	teacherUid := roomInfo.CreateUid.Uid
	if opUid != teacherUid { // 用户不存在 直接返回错误
		infoLog.Printf("ProcGroupLessonChangeCourseware roomId=%v opUid=%v teacherUid=%v not equal",
			roomId,
			opUid,
			teacherUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_NO_PERMISSION
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("is not teacher"),
			},
		}
		return true
	}
	// 更新群课程当前课间的url
	err = groupLessonManager.UpdateCurrentUrlByRoomId(roomId, url)
	if err != nil {
		infoLog.Printf("ProcGroupLessonChangeCourseware roomId=%v opUid=%v url=%s UpdateCurrentUrlByRoomId failed err=%s",
			roomId,
			opUid,
			url)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return true
	}

	roomInfo, err = groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcGroupLessonChangeCourseware teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	memberList := groupLessonManager.GetSortUidListFromAlreadyIn(roomInfo.AlreadyInList)
	infoLog.Printf("ProcGroupLessonChangeCourseware roomId=%v opUid=%v url=%s memberList=%v",
		roomId,
		opUid,
		url,
		memberList)
	retCode, err := mucApi.BroadCastGroupLessonStatChange(
		roomId,
		opUid,
		memberList,
		roomInfo.LessonCurrentUrl,
		uint64(time.Now().UnixNano()/1000000))
	if err != nil || retCode != 0 {
		attr := "grouplesson/req_bc_group_lesson_stat_failed"
		libcomm.AttrAdd(attr, 1)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("broad cast failed"),
			},
		}
		return false
	}

	// Step6: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.ChangeCoursewareRspbody = &ht_group_lesson.ChangeCoursewareRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))

	// Step7: 将ppt序列保存到文件中
	_, err = groupLessonManager.SavePPTRecordToFile(filePath, string(roomInfo.RecordObid), roomInfo.PPTRecored)
	if err != nil {
		infoLog.Printf("ProcGroupLessonChangeCourseware roomId=%v recordObid=%s url=%s",
			roomInfo.RoomId,
			roomInfo.RecordObid,
			url)
		attr := "grouplesson/save_group_lesson_ppt_failed"
		libcomm.AttrAdd(attr, 1)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/group_lesson_change_url_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 16.Proc update lesson message record url
func ProcUpdateGroupLessonRecordUrl(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcUpdateGroupLessonRecordUrl no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdateGroupLessonRecordRspbody = &ht_group_lesson.UpdateGroupLessonRecordUrlRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateGroupLessonRecordUrl invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/update_group_lesson_record_url_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateGroupLessonRecordUrl proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.UpdateGroupLessonRecordRspbody = &ht_group_lesson.UpdateGroupLessonRecordUrlRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateGroupLessonRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcUpdateGroupLessonRecordUrl GetUpdateGroupLessonRecordReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.UpdateGroupLessonRecordRspbody = &ht_group_lesson.UpdateGroupLessonRecordUrlRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	groupLessonObid := subReqBody.GetGroupLessonObid()
	url := subReqBody.GetRecordUrl()
	infoLog.Printf("ProcUpdateGroupLessonRecordUrl roomId=%v groupLessonObid=%s url=%s",
		roomId,
		groupLessonObid,
		url)

	// Step1: 输入参数检查
	if len(groupLessonObid) == 0 ||
		roomId == 0 ||
		len(url) == 0 {
		infoLog.Printf("ProcUpdateGroupLessonRecordUrl invalid param roomId=%v groupLessonObid=%s url=%s",
			roomId,
			groupLessonObid,
			url)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.UpdateGroupLessonRecordRspbody = &ht_group_lesson.UpdateGroupLessonRecordUrlRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 更新群组课程录像
	err = groupLessonManager.UpdateGroupLessonRecordVoiceUrl(roomId, 1000, string(groupLessonObid), string(url))
	if err != nil {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.UpdateGroupLessonRecordRspbody = &ht_group_lesson.UpdateGroupLessonRecordUrlRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateGroupLessonRecordUrl invalid param roomId=%v groupLessonObid=%s url=%s", roomId, groupLessonObid, url)
		return false
	}
	// Step3: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.UpdateGroupLessonRecordRspbody = &ht_group_lesson.UpdateGroupLessonRecordUrlRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/update_group_lesson_record_url_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 17.Proc get lesson by one obid
func ProcGetLessonByObid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetLessonByObid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetLessonByObid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/get_lesson_by_obid_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetLessonByObid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetLessonByObidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetLessonByObid GetGetLessonByOneObidReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	roomId := subReqBody.GetRoomId()
	groupLessonObidList := subReqBody.GetGroupLessonObidList()
	infoLog.Printf("ProcGetLessonByObid reqUid=%v groupLessonObidList=%#v",
		reqUid,
		groupLessonObidList)

	// Step1: param check
	if reqUid == 0 || len(groupLessonObidList) == 0 {
		infoLog.Printf("ProcGetLessonByObid invalid param reqUid=%v groupLessonObidList=%s",
			reqUid,
			groupLessonObidList)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	var lessonList []*ht_group_lesson.LessonDetailInfo
	for _, v := range groupLessonObidList {
		// Step2: 根据obid获取课程
		groupLesson, err := groupLessonManager.GetGroupLessonByOneObid(roomId, v)
		if err != nil {
			infoLog.Printf("ProcGetLessonByObid groupLessonManager.GetGroupLessonByOneObid roomId=%v groupLessonObid=%s err=%s",
				roomId,
				v,
				err)
			attr := "grouplesson/get_lesson_by_obid_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		personalLesson, err := groupLessonManager.GetPersonalLessonByOneObid([]byte(groupLesson.PersonalLessonObid))
		if err != nil {
			infoLog.Printf("ProcGetLessonByObid groupLessonManager.GetPersonalLessonByOneObid reqUid=%v cliObid=%s err=%s",
				reqUid,
				v,
				err)
			attr := "grouplesson/get_lesson_by_obid_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		infoLog.Printf("ProcGetLessonByObid reqUid=%v groupLessonObid=%s groupLesson=%#v personalLesson=%#v",
			reqUid,
			v,
			groupLesson,
			personalLesson)

		// 根据群课程obid取课程录像列表
		recordList, err := groupLessonManager.GetRecordListByGroupLessonObid(groupLesson.RoomId, []byte(groupLesson.Id.Hex()))
		if err != nil {
			infoLog.Printf("ProcGetLessonByObid groupLessonManager.GetRecordListByGroupLessonObid reqUid=%v cliObid=%s err=%s",
				reqUid,
				groupLesson.Id.Hex(),
				err)
			attr := "grouplesson/get_record_list_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		// Step3: send response
		var recordUrl []*ht_group_lesson.RecordUrl
		for _, v := range recordList {
			item := &ht_group_lesson.RecordUrl{
				RecordObid:     []byte(v.Id.Hex()),
				VoiceUrl:       []byte(v.VoiceUrl),
				PptUrl:         []byte(v.PPTUrl),
				BeginTimeStamp: proto.Uint64(v.StartTimeStamp),
			}
			recordUrl = append(recordUrl, item)
		}

		// Step3: 查询muc用户是否在群里面
		isInRoom, err := mucApi.IsUserAlreadyInRoom(groupLesson.RoomId, reqUid)
		if err != nil {
			infoLog.Printf("ProcGetLessonByObid mucApi.IsUserAlreadyInRoom roomId=%v reqUid=%v err=%s",
				groupLesson.RoomId,
				reqUid,
				err)
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
			rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
				Status: &ht_group_lesson.GroupLessonHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("query user is in room failed"),
				},
			}
			return false
		}
		var retValue uint32
		if isInRoom {
			retValue = 1
		} else {
			retValue = 0
		}

		lessonDetailInfoItem := &ht_group_lesson.LessonDetailInfo{
			IsUserInGroup: proto.Uint32(retValue),
			GroupLesson: &ht_group_lesson.GroupLessonItem{
				GroupLessonObid:    []byte(groupLesson.Id.Hex()),
				CreateUid:          proto.Uint32(groupLesson.CreaterUid),
				RoomId:             proto.Uint32(groupLesson.RoomId),
				PersonalLessonObid: []byte(groupLesson.PersonalLessonObid),
				LessonTime:         []byte(groupLesson.LessonTime),
				RecordUrlList:      recordUrl,
			},
			PersonalLesson: &ht_group_lesson.PersonalLessonItem{
				PersonalObid:        []byte(personalLesson.Id.Hex()),
				CreaterUid:          proto.Uint32(personalLesson.CreaterUid),
				LessonTitle:         []byte(personalLesson.LessonTitle),
				LessonAbstract:      []byte(personalLesson.LessonAbstract),
				TeacherAbstract:     []byte(personalLesson.TeacherAbstract),
				LessonCoursewareUrl: []byte(personalLesson.LessonCoursewareUrl),
				CoverUrl:            []byte(personalLesson.CoverUrl),
				RoomIdList:          personalLesson.ApplyRoomId,
			},
		}
		lessonList = append(lessonList, lessonDetailInfoItem)
	}

	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		LessonInfoList: lessonList,
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/get_lesson_by_one_obid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 18.Proc get latest group lesson msg
func ProcGetLatestGroupLessonMsg(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetLatestGroupLessonMsg no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GetLatestGroupLessonMsgRspbody = &ht_group_lesson.GetLatestGroupLessonMsgRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetLatestGroupLessonMsg invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/get_latest_group_lesson_msg"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetLatestGroupLessonMsg proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GetLatestGroupLessonMsgRspbody = &ht_group_lesson.GetLatestGroupLessonMsgRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetLatestGroupLessonMsgReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetLatestGroupLessonMsg GetGetLatestGroupLessonMsgReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GetLatestGroupLessonMsgRspbody = &ht_group_lesson.GetLatestGroupLessonMsgRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	recordObid := subReqBody.GetRecordObid()
	cliSeq := subReqBody.GetCliSeq()
	infoLog.Printf("ProcGetLatestGroupLessonMsg roomId=%v recordObid=%s cliSeq=%v",
		roomId,
		recordObid,
		cliSeq)

	// Step1: 输入参数检查
	if roomId == 0 ||
		len(recordObid) == 0 {
		infoLog.Printf("ProcGetLatestGroupLessonMsg invalid param roomId=%v recordObid=%s cliSeq=%v",
			roomId,
			recordObid,
			cliSeq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GetLatestGroupLessonMsgRspbody = &ht_group_lesson.GetLatestGroupLessonMsgRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 调用群课程消息存储api取消息
	maxMsgId, _, outMsgList, err := lessonMsgStoreApi.BatchGetLatestLessonMsg(roomId, reqUid, recordObid, cliSeq)
	if err != nil {
		infoLog.Printf("ProcGetLatestGroupLessonMsg lessonMsgStoreApi.BatchGetLatestLessonMsg roomId=%v recordObid=%s cliSeq=%v err=%s",
			roomId,
			recordObid,
			cliSeq,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.GetLatestGroupLessonMsgRspbody = &ht_group_lesson.GetLatestGroupLessonMsgRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}

	// Step3: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.GetLatestGroupLessonMsgRspbody = &ht_group_lesson.GetLatestGroupLessonMsgRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		CurMaxSeq: proto.Uint64(maxMsgId),
		MsgList:   outMsgList,
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/get_latest_group_lesson_msg_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 19.Proc Get group lesson Detail info by Obid
func ProcBatchGetGroupLessonDetailInfo(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcBatchGetGroupLessonDetailInfo no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.BatchGetGroupLessonDetailRspbody = &ht_group_lesson.BatchGetGroupLessonDetailRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchGetGroupLessonDetailInfo invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/batch_get_detail_info"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchGetGroupLessonDetailInfo proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.BatchGetGroupLessonDetailRspbody = &ht_group_lesson.BatchGetGroupLessonDetailRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchGetGroupLessonDetailReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchGetGroupLessonDetailInfo GetBatchGetGroupLessonDetailReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.BatchGetGroupLessonDetailRspbody = &ht_group_lesson.BatchGetGroupLessonDetailRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	roomId := subReqBody.GetRoomId()
	cliObid := subReqBody.GetCliObid()
	infoLog.Printf("ProcBatchGetGroupLessonDetailInfo reqUid=%v roomId=%v cliObid=%s",
		reqUid,
		roomId,
		cliObid)

	if string(cliObid) == "0" {
		cliObid = []byte(GetBeginHexString())
		infoLog.Printf("ProcBatchGetGroupLessonDetailInfo reqUid=%v cliObid=%s", reqUid, cliObid)
	}

	// Step1: param check
	if reqUid == 0 || roomId == 0 || len(cliObid) == 0 {
		infoLog.Printf("ProcBatchGetGroupLessonDetailInfo invalid param reqUid=%v roomId=%v cliObid=%s",
			reqUid,
			roomId,
			cliObid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.BatchGetGroupLessonDetailRspbody = &ht_group_lesson.BatchGetGroupLessonDetailRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: 获取群组课程列表
	groupLessonList, minObid, err := groupLessonManager.GetGroupLessonByObid(reqUid, roomId, cliObid)
	if err != nil {
		infoLog.Printf("ProcBatchGetGroupLessonDetailInfo groupLessonManager.GetPersonalLessonByObid reqUid=%v roomId=%v obid=%s failed err=%s",
			reqUid,
			roomId,
			cliObid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.BatchGetGroupLessonDetailRspbody = &ht_group_lesson.BatchGetGroupLessonDetailRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	var lessonList []*ht_group_lesson.LessonDetailInfo
	// Step3: 根据个人课程obid获取课程
	for _, v := range groupLessonList {
		personalLesson, err := groupLessonManager.GetPersonalLessonByOneObid([]byte(v.PersonalLessonObid))
		if err != nil {
			infoLog.Printf("ProcBatchGetGroupLessonDetailInfo groupLessonManager.GetPersonalLessonByOneObid reqUid=%v cliObid=%s err=%s",
				reqUid,
				v.Id.Hex(),
				err)
			attr := "grouplesson/get_lesson_by_obid_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		infoLog.Printf("ProcBatchGetGroupLessonDetailInfo reqUid=%v groupLessonObid=%s groupLesson=%#v personalLesson=%#v",
			reqUid,
			v.Id.Hex(),
			v,
			personalLesson)
		// 根据群课程obid取课程录像列表
		recordList, err := groupLessonManager.GetRecordListByGroupLessonObid(v.RoomId, []byte(v.Id.Hex()))
		if err != nil {
			infoLog.Printf("ProcBatchGetGroupLessonDetailInfo groupLessonManager.GetRecordListByGroupLessonObid reqUid=%v cliObid=%s err=%s",
				reqUid,
				v.Id.Hex(),
				err)
			attr := "grouplesson/get_record_list_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		// Step3: send response
		var recordUrl []*ht_group_lesson.RecordUrl
		for _, record := range recordList {
			item := &ht_group_lesson.RecordUrl{
				RecordObid:     []byte(record.Id.Hex()),
				VoiceUrl:       []byte(record.VoiceUrl),
				PptUrl:         []byte(record.PPTUrl),
				BeginTimeStamp: proto.Uint64(record.StartTimeStamp),
			}
			recordUrl = append(recordUrl, item)
		}

		// Step3: 查询muc用户是否在群里面
		isInRoom, err := mucApi.IsUserAlreadyInRoom(v.RoomId, reqUid)
		if err != nil {
			infoLog.Printf("ProcBatchGetGroupLessonDetailInfo mucApi.IsUserAlreadyInRoom roomId=%v reqUid=%v err=%s",
				v.RoomId,
				reqUid,
				err)
			attr := "grouplesson/is_user_in_room_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		var retValue uint32
		if isInRoom {
			retValue = 1
		} else {
			retValue = 0
		}

		lessonDetailInfoItem := &ht_group_lesson.LessonDetailInfo{
			IsUserInGroup: proto.Uint32(retValue),
			GroupLesson: &ht_group_lesson.GroupLessonItem{
				GroupLessonObid:    []byte(v.Id.Hex()),
				CreateUid:          proto.Uint32(v.CreaterUid),
				RoomId:             proto.Uint32(v.RoomId),
				PersonalLessonObid: []byte(v.PersonalLessonObid),
				LessonTime:         []byte(v.LessonTime),
				RecordUrlList:      recordUrl,
			},
			PersonalLesson: &ht_group_lesson.PersonalLessonItem{
				PersonalObid:        []byte(personalLesson.Id.Hex()),
				CreaterUid:          proto.Uint32(personalLesson.CreaterUid),
				LessonTitle:         []byte(personalLesson.LessonTitle),
				LessonAbstract:      []byte(personalLesson.LessonAbstract),
				TeacherAbstract:     []byte(personalLesson.TeacherAbstract),
				LessonCoursewareUrl: []byte(personalLesson.LessonCoursewareUrl),
				CoverUrl:            []byte(personalLesson.CoverUrl),
				RoomIdList:          personalLesson.ApplyRoomId,
			},
		}
		lessonList = append(lessonList, lessonDetailInfoItem)
	}

	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.BatchGetGroupLessonDetailRspbody = &ht_group_lesson.BatchGetGroupLessonDetailRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MinObid:    minObid,
		LessonList: lessonList,
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/batch_get_detail_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 20.Proc del group lesson record
func ProcDelRecord(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcDelRecord no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.DelRecordRspbody = &ht_group_lesson.DelRecordRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelRecord invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/del_record_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelRecord proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.DelRecordRspbody = &ht_group_lesson.DelRecordRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelRecordReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelRecord GetDelRecordReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.DelRecordRspbody = &ht_group_lesson.DelRecordRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	obid := subReqBody.GetRecordObid()
	infoLog.Printf("ProcDelRecord reqUid=%v obid=%s",
		reqUid,
		obid)

	// Step1: 输入参数检查
	if reqUid == 0 ||
		len(obid) == 0 {
		infoLog.Printf("ProcDelRecord invalid param reqUid=%v obid=%s",
			reqUid,
			obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.DelRecordRspbody = &ht_group_lesson.DelRecordRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 删除个人课程需要判断当前是否有群正在上这个课如果有群正在上课 返回特定的返回码通知操作者
	err = groupLessonManager.DelGroupLessonRecord(reqUid, obid)
	if err != nil {
		infoLog.Printf("ProcDelRecord groupLessonManager.DelGroupLessonRecord reqUid=%v obid=%s failed err=%s",
			reqUid,
			obid,
			err)
		if err == util.ErrPermissionDenied {
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_NO_PERMISSION
		} else {
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		}
		rspBody.DelRecordRspbody = &ht_group_lesson.DelRecordRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcDelRecord groupLessonManager.DelGroupLessonRecord reqUid=%v obid=%s", reqUid, obid)

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.DelRecordRspbody = &ht_group_lesson.DelRecordRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/del_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 21.Proc Query reconrd stat
func ProcQueryRecordStat(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcQueryRecordStat no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.QueryRecordStatRspbody = &ht_group_lesson.QueryRecordStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryRecordStat invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/query_record_stat_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryRecordStat proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.QueryRecordStatRspbody = &ht_group_lesson.QueryRecordStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryRecordStatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryRecordStat GetQueryRecordStatReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.QueryRecordStatRspbody = &ht_group_lesson.QueryRecordStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	obid := subReqBody.GetRecordObid()
	infoLog.Printf("ProcQueryRecordStat reqUid=%v obid=%s",
		reqUid,
		obid)

	// Step1: 输入参数检查
	if reqUid == 0 ||
		len(obid) == 0 {
		infoLog.Printf("ProcQueryRecordStat invalid param reqUid=%v obid=%s",
			reqUid,
			obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.QueryRecordStatRspbody = &ht_group_lesson.QueryRecordStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 删除个人课程需要判断当前是否有群正在上这个课如果有群正在上课 返回特定的返回码通知操作者
	isDeleted, err := groupLessonManager.QueryGroupLessonRecordStat(reqUid, obid)
	if err != nil {
		infoLog.Printf("ProcQueryRecordStat groupLessonManager.QueryGroupLessonRecordStat reqUid=%v obid=%s failed err=%s",
			reqUid,
			obid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.QueryRecordStatRspbody = &ht_group_lesson.QueryRecordStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcQueryRecordStat groupLessonManager.QueryGroupLessonRecordStat reqUid=%v obid=%s isDeleted=%v", reqUid, obid, isDeleted)

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.QueryRecordStatRspbody = &ht_group_lesson.QueryRecordStatRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		IsDeleted: proto.Bool(isDeleted),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/query_record_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 22.Proc get group lesson record
func ProcGetRecordByObid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetRecordByObid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GetRecordByObidRspbody = &ht_group_lesson.GetRecordByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetRecordByObid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/get_record_by_obid_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetRecordByObid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GetRecordByObidRspbody = &ht_group_lesson.GetRecordByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetRecordByObidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetRecordByObid GetGetRecordByObidReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GetRecordByObidRspbody = &ht_group_lesson.GetRecordByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	obid := subReqBody.GetRecordObid()
	infoLog.Printf("ProcGetRecordByObid reqUid=%v obid=%s",
		reqUid,
		obid)

	// Step1: 输入参数检查
	if reqUid == 0 ||
		len(obid) == 0 {
		infoLog.Printf("ProcGetRecordByObid invalid param reqUid=%v obid=%s",
			reqUid,
			obid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GetRecordByObidRspbody = &ht_group_lesson.GetRecordByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2:通过recordObid 查询记录的详情 如果记录别删除 查询返回error
	recordInfo, err := groupLessonManager.GetRecordByObid(reqUid, obid)
	if err != nil {
		infoLog.Printf("ProcGetRecordByObid groupLessonManager.GetRecordByObid reqUid=%v obid=%s failed err=%s",
			reqUid,
			obid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.GetRecordByObidRspbody = &ht_group_lesson.GetRecordByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcGetRecordByObid groupLessonManager.GetRecordByObid reqUid=%v obid=%s roomId=%v voiceurl=%s ppturl=%s recordTime=%v",
		reqUid,
		obid,
		recordInfo.RoomId,
		recordInfo.VoiceUrl,
		recordInfo.PPTUrl,
		recordInfo.StartTimeStamp)

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.GetRecordByObidRspbody = &ht_group_lesson.GetRecordByObidRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		RecordInfo: &ht_group_lesson.RecordUrl{
			RecordObid:     obid,
			VoiceUrl:       []byte(recordInfo.VoiceUrl),
			PptUrl:         []byte(recordInfo.PPTUrl),
			BeginTimeStamp: proto.Uint64(recordInfo.StartTimeStamp),
		},
		RoomId: proto.Uint32(recordInfo.RoomId),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/get_record_by_obid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 24.Proc get group lesson json by group lesson obid
func ProcGetGroupLessonJsonByObid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendJsonRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcGetGroupLessonJsonByObid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetGroupLessonJsonByObid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/get_lesson_by_obid_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := json.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetGroupLessonJsonByObid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v err=%s", head.From, head.To, head.Cmd, head.Seq, err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetLessonByObidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetGroupLessonJsonByObid GetGetLessonByOneObidReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	reqUid := subReqBody.GetReqUid()
	roomId := subReqBody.GetRoomId()
	groupLessonObidList := subReqBody.GetGroupLessonObidList()
	infoLog.Printf("ProcGetGroupLessonJsonByObid reqUid=%v groupLessonObidListLen=%v",
		reqUid,
		len(groupLessonObidList))

	// Step1: param check
	if reqUid == 0 || len(groupLessonObidList) == 0 {
		infoLog.Printf("ProcGetGroupLessonJsonByObid invalid param reqUid=%v groupLessonObidList=%s",
			reqUid,
			groupLessonObidList)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INPUT_PARAM_ERR
		rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	var lessonList []*ht_group_lesson.LessonDetailInfo
	for _, v := range groupLessonObidList {
		// Step2: 根据obid获取课程
		groupLesson, err := groupLessonManager.GetGroupLessonByOneObid(roomId, v)
		if err != nil {
			infoLog.Printf("ProcGetGroupLessonJsonByObid groupLessonManager.GetGroupLessonByOneObid roomId=%v groupLessonObid=%s err=%s",
				roomId,
				v,
				err)
			attr := "grouplesson/get_lesson_by_obid_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		personalLesson, err := groupLessonManager.GetPersonalLessonByOneObid([]byte(groupLesson.PersonalLessonObid))
		if err != nil {
			infoLog.Printf("ProcGetGroupLessonJsonByObid groupLessonManager.GetPersonalLessonByOneObid reqUid=%v cliObid=%s err=%s",
				reqUid,
				v,
				err)
			attr := "grouplesson/get_lesson_by_obid_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		infoLog.Printf("ProcGetGroupLessonJsonByObid reqUid=%v groupLessonObid=%s groupLesson=%#v personalLesson=%#v",
			reqUid,
			v,
			groupLesson,
			personalLesson)

		// 根据群课程obid取课程录像列表
		recordList, err := groupLessonManager.GetRecordListByGroupLessonObid(groupLesson.RoomId, []byte(groupLesson.Id.Hex()))
		if err != nil {
			infoLog.Printf("ProcGetGroupLessonJsonByObid groupLessonManager.GetRecordListByGroupLessonObid reqUid=%v cliObid=%s err=%s",
				reqUid,
				groupLesson.Id.Hex(),
				err)
			attr := "grouplesson/get_record_list_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}

		// Step3: send response
		var recordUrl []*ht_group_lesson.RecordUrl
		for _, v := range recordList {
			item := &ht_group_lesson.RecordUrl{
				RecordObid:     []byte(v.Id.Hex()),
				VoiceUrl:       []byte(v.VoiceUrl),
				PptUrl:         []byte(v.PPTUrl),
				BeginTimeStamp: proto.Uint64(v.StartTimeStamp),
			}
			recordUrl = append(recordUrl, item)
		}

		// Step3: 查询muc用户是否在群里面
		isInRoom, err := mucApi.IsUserAlreadyInRoom(groupLesson.RoomId, reqUid)
		if err != nil {
			infoLog.Printf("ProcGetGroupLessonJsonByObid mucApi.IsUserAlreadyInRoom roomId=%v reqUid=%v err=%s",
				groupLesson.RoomId,
				reqUid,
				err)
			result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
			rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
				Status: &ht_group_lesson.GroupLessonHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("query user is in room failed"),
				},
			}
			return false
		}
		var retValue uint32
		if isInRoom {
			retValue = 1
		} else {
			retValue = 0
		}

		lessonDetailInfoItem := &ht_group_lesson.LessonDetailInfo{
			IsUserInGroup: proto.Uint32(retValue),
			GroupLesson: &ht_group_lesson.GroupLessonItem{
				GroupLessonObid:    []byte(groupLesson.Id.Hex()),
				CreateUid:          proto.Uint32(groupLesson.CreaterUid),
				RoomId:             proto.Uint32(groupLesson.RoomId),
				PersonalLessonObid: []byte(groupLesson.PersonalLessonObid),
				LessonTime:         []byte(groupLesson.LessonTime),
				RecordUrlList:      recordUrl,
			},
			PersonalLesson: &ht_group_lesson.PersonalLessonItem{
				PersonalObid:        []byte(personalLesson.Id.Hex()),
				CreaterUid:          proto.Uint32(personalLesson.CreaterUid),
				LessonTitle:         []byte(personalLesson.LessonTitle),
				LessonAbstract:      []byte(personalLesson.LessonAbstract),
				TeacherAbstract:     []byte(personalLesson.TeacherAbstract),
				LessonCoursewareUrl: []byte(personalLesson.LessonCoursewareUrl),
				CoverUrl:            []byte(personalLesson.CoverUrl),
				RoomIdList:          personalLesson.ApplyRoomId,
			},
		}
		lessonList = append(lessonList, lessonDetailInfoItem)
	}

	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.GetLessonByObidRspbody = &ht_group_lesson.GetLessonByObidRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		LessonInfoList: lessonList,
	}
	infoLog.Printf("ProcGetGroupLessonJsonByObid reqUid=%v lessonInfoList=%v",
		reqUid,
		len(lessonList))

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/get_lesson_json_by_one_obid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 25.Proc del group lesson by uid
func ProcDelGroupLessonByUid(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcDelGroupLessonByUid no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.DelGroupLessonByUidRspbody = &ht_group_lesson.DelGroupLessonByUidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelGroupLessonByUid invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/del_group_lesson_by_uid_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelGroupLessonByUid proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.DelGroupLessonByUidRspbody = &ht_group_lesson.DelGroupLessonByUidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelGroupLessonByUidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelGroupLessonByUid GetDelGroupLessonByUidReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.DelGroupLessonByUidRspbody = &ht_group_lesson.DelGroupLessonByUidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	createrUid := subReqBody.GetCreateUid()
	roomId := subReqBody.GetRoomId()
	infoLog.Printf("ProcDelGroupLessonByUid createrUid=%v rooId=%v",
		createrUid,
		roomId)

	// Step1: 输入参数检查
	if createrUid == 0 ||
		roomId == 0 {
		infoLog.Printf("ProcDelGroupLessonByUid invalid param createrUid=%v roomId=%v ",
			createrUid,
			roomId)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.DelGroupLessonByUidRspbody = &ht_group_lesson.DelGroupLessonByUidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 删除群组课程
	err = groupLessonManager.DelGroupLessonByUid(createrUid, roomId)
	if err != nil {
		infoLog.Printf("ProcDelGroupLessonByUid groupLessonManager.DelGroupLessonByUid createUid=%v roomId=%v failed err=%s",
			createrUid,
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.DelGroupLessonByUidRspbody = &ht_group_lesson.DelGroupLessonByUidRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	infoLog.Printf("OK ProcDelGroupLessonByUid groupLessonManager.DelGroupLessonByUid createUid=%v roomId=%v", createrUid, roomId)

	// Step7: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.DelGroupLessonByUidRspbody = &ht_group_lesson.DelGroupLessonByUidRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/del_group_lesson_by_uid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 26.Proc get group lesson stat
func ProcQueryGroupLessonStat(c *gotcp.Conn, head *common.HeadV3, packet *common.HeadV3Packet) bool {
	// parse packet
	result := ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody := new(ht_group_lesson.GroupLessonRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, uint16(result))
		} else {
			infoLog.Printf("ProcQueryGroupLessonStat no need call from=%v to=%v seq=%v", head.From, head.To, head.Seq)
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryGroupLessonStat invalid param from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "grouplesson/query_group_lesson_stat_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryGroupLessonStat proto Unmarshal failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryGroupLessonStatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryGroupLessonStat GetQueryGroupLessonStatReqbody() failed from=%v to=%v cmd=0x%4x seq=%v", head.From, head.To, head.Cmd, head.Seq)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_PB_ERR
		rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	infoLog.Printf("ProcQueryGroupLessonStat roomId=%v reqUid=%v",
		roomId,
		reqUid)

	// Step1: 输入参数检查
	if reqUid == 0 ||
		roomId == 0 {
		infoLog.Printf("ProcQueryGroupLessonStat invalid param roomId=%v reqUid=%v",
			roomId,
			reqUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INVALID_PARAM
		rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}
	// Step2: 检查房间是否正在上群组课
	ret, err := groupLessonManager.IsRoomLessoning(roomId)
	if err != nil {
		infoLog.Printf("ProcQueryGroupLessonStat teachingRoomManager.IsRoomLessoning roomId=%v reqUid=%v err=%s",
			roomId,
			reqUid,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	if !ret {
		infoLog.Printf("ProcQueryGroupLessonStat roomId=%v reqUid=%v is not teaching",
			roomId,
			reqUid)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_LESSON_IS_OVER
		rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("not teaching"),
			},
		}
		return true
	}
	// Step3: 检查用户是否已经在群语音中
	roomInfo, err := groupLessonManager.GetRoomInfoByRoomId(roomId)
	if err != nil {
		infoLog.Printf("ProcQueryGroupLessonStat teachingRoomManager.GetRoomInfoByRoomId failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// Step4: 查询群的所有管理员
	adminList, err := mucApi.GetRoomAdminList(roomId, reqUid)
	if err != nil {
		infoLog.Printf("ProcQueryGroupLessonStat mucApi.GetRoomAdminList failed roomId=%v err=%s",
			roomId,
			err)
		result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_INTERNAL_ERR
		rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
			Status: &ht_group_lesson.GroupLessonHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	var teacherList []uint32
	var studentList []uint32
	memberList := groupLessonManager.GetSortUidListFromAlreadyIn(roomInfo.AlreadyInList)
	infoLog.Printf("ProcQueryGroupLessonStat roomId=%v reqUid=%v channelId=%s memberList=%v",
		roomId,
		reqUid,
		roomInfo.ChannelId,
		memberList)
	for _, v := range memberList {
		if isUserAlreadyInAdminList(v, adminList) {
			teacherList = append(teacherList, v)
		} else {
			studentList = append(studentList, v)
		}
	}
	// Step6: 返回响应
	result = ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS
	rspBody.QueryGroupLessonStatRspbody = &ht_group_lesson.QueryGroupLessonStatRspBody{
		Status: &ht_group_lesson.GroupLessonHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		TeacherList:       teacherList,
		StudentList:       studentList,
		CurrentPptUrl:     []byte(roomInfo.LessonCurrentUrl),
		BeginTimeStamp:    proto.Uint64(roomInfo.BeginTime),
		CurrentSystemTime: proto.Uint64(uint64(time.Now().UnixNano() / 1000000)),
		ChannelId:         roomInfo.ChannelId,
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, uint16(result))
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "grouplesson/query_group_lesson_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func SendJsonRsp(c *gotcp.Conn, reqHead *common.HeadV3, rspBody *ht_group_lesson.GroupLessonRspBody, ret uint16) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}

	s, err := json.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendJsonRsp json.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	infoLog.Printf("SendJsonRsp slic=%s", s)
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendJsonRsp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mongo
	// 创建mongodb对象
	mongo_url := cfg.Section("MONGO").Key("url").MustString("localhost")
	infoLog.Printf("Mongo url=%s", mongo_url)
	mongoSess, err = mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
		return
	}
	defer mongoSess.Close()
	//Optional. Switch the session to a monotonic behavior.
	mongoSess.SetMode(mgo.Monotonic, true)

	// 读取mucApi 配置
	mucIp := cfg.Section("MUC").Key("ip").MustString("127.0.0.1")
	mucPort := cfg.Section("MUC").Key("port").MustString("0")
	mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	infoLog.Printf("muc server ip=%v port=%v connLimit=%v", mucIp, mucPort, mucConnLimit)
	mucApi = common.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, mucConnLimit)

	// 读取mucApi 配置
	voipIp := cfg.Section("VOIP").Key("ip").MustString("127.0.0.1")
	voipPort := cfg.Section("VOIP").Key("port").MustString("0")
	voipConnLimit := cfg.Section("VOIP").Key("pool_limit").MustInt(1000)
	infoLog.Printf("voip server ip=%v port=%v connLimit=%v", voipIp, voipPort, voipConnLimit)
	voipApi = common.NewVoipApi(voipIp, voipPort, 3*time.Second, 3*time.Second, &common.XTHeadProtocol{}, voipConnLimit)

	// 读取mucStoreApi 配置
	lessonMsgStoreIp := cfg.Section("LESSONMSG").Key("ip").MustString("127.0.0.1")
	lessonMsgStorePort := cfg.Section("LESSONMSG").Key("port").MustString("0")
	lessonMsgStoreConnLimit := cfg.Section("LESSONMSG").Key("pool_limit").MustInt(1000)
	infoLog.Printf("LESSONMSG server ip=%v port=%v connLimit=%v", lessonMsgStoreIp, lessonMsgStorePort, lessonMsgStoreConnLimit)
	lessonMsgStoreApi = common.NewLessonMsgStoreApi(lessonMsgStoreIp, lessonMsgStorePort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, lessonMsgStoreConnLimit)

	// 读取agoraRecordApi 配置
	recordIp := cfg.Section("RECORD").Key("ip").MustString("127.0.0.1")
	recordPort := cfg.Section("RECORD").Key("port").MustString("0")
	recordConnLimit := cfg.Section("RECORD").Key("pool_limit").MustInt(1000)
	infoLog.Printf("RECORD server ip=%v port=%v connLimit=%v", recordIp, recordPort, recordConnLimit)
	agoraRecordApi = common.NewAgoraRecordApi(recordIp, recordPort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, recordConnLimit)

	// user info cache api
	userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	infoLog.Printf("user info cache ip=%v port=%v", userInfoIp, userInfoPort)
	userInfoCacheApi = common.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// 读取walletApi 配置
	walletIp := cfg.Section("WALLET").Key("ip").MustString("127.0.0.1")
	walletPort := cfg.Section("WALLET").Key("port").MustString("0")
	walletConnLimit := cfg.Section("WALLET").Key("pool_limit").MustInt(1000)
	infoLog.Printf("wallet server ip=%v port=%v connLimit=%v", walletIp, walletPort, walletConnLimit)
	walletApi = common.NewWalletApi(walletIp, walletPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, walletConnLimit)

	// ppt文件保存路径
	filePath = cfg.Section("PPTFILEPATH").Key("path").MustString("/mydata/ppt/")

	OSSBukect := cfg.Section("SERVICE_CONFIG").Key("oss_bucket").MustString("ht-blog")
	OSSRegion := cfg.Section("SERVICE_CONFIG").Key("oss_region").MustString("oss-cn-hongkong.aliyuncs.com")
	OSSAppID := cfg.Section("SERVICE_CONFIG").Key("oss_appid").MustString("ULuE3LK66Y6ZlEWS")
	OSSAppKey := cfg.Section("SERVICE_CONFIG").Key("oss_appkey").MustString("JTFyxX7TW9K1TSVMwglj272CxfvwUC")
	OSSPrefix := cfg.Section("SERVICE_CONFIG").Key("oss_prefix").MustString("groupclass/")

	// nsq producer config
	nsqUrl := cfg.Section("NSQ").Key("url").MustString("127.0.0.1:4150")
	nsqConfig := nsq.NewConfig()
	nsqProducer, err := nsq.NewProducer(nsqUrl, nsqConfig)
	if err != nil {
		log.Fatalf("main nsq.NewProcucer failed nsqUrl=%s", nsqUrl)
	}
	nsqOpLogTopic := cfg.Section("OPLOG").Key("topic").MustString("op_log")
	infoLog.Printf("nsq url=%s topic=%s", nsqUrl, nsqOpLogTopic)
	//创建RoomManager 和 DbUtil 对象
	DbUtil = util.NewDbUtil(mongoSess, infoLog)
	groupLessonManager = util.NewGroupLessonManager(DbUtil,
		agoraRecordApi,
		mucApi,
		infoLog,
		filePath,
		OSSBukect,
		OSSRegion,
		OSSAppID,
		OSSAppKey,
		OSSPrefix,
		nsqProducer,
		nsqOpLogTopic)
	groupLessonManager.AsyncDo() // 启动自动检测
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
