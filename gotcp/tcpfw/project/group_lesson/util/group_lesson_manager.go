// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"sync"
	"time"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_oplog"
	"github.com/HT_GOGO/gotcp/tcpfw/project/mnt_family/mnt_server/mnt_cache_logic/util"
	webcommon "github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/gogo/protobuf/proto"
	nsq "github.com/nsqio/go-nsq"
	"gopkg.in/mgo.v2/bson"
)

var (
	ErrNilParam                  = errors.New("err nil param obj")
	ErrInputParam                = errors.New("err input param err")
	ErrAlreadyExist              = errors.New("err already exist")
	ErrNotExist                  = errors.New("err not exist")
	ErrOnlineIpErr               = errors.New("err online state ip not exist")
	ErrSendToMucFaild            = errors.New("err send packet to muc failed")
	ErrPersonalLessonIsLessoning = errors.New("personal lesson is lessoning")
)

const (
	USE_TIMEOUT_THRESHOLD = 120 // 群组课程用户超时时间 30秒
)

type MemberDetailInfo struct {
	Uid          uint32
	NickName     []byte
	HeadPhotoUrl []byte
	Country      []byte
}

// 每个群成员基本信息
type MemberSimpleInfo struct {
	Uid      uint32 // 用户uid
	JoinTs   int64  // 加入课程的时刻 用户排序
	ActiveTS int64  // 上次收到心跳的时刻 单位是秒
}

type Int64Slic []int64

func (p Int64Slic) Len() int           { return len(p) }
func (p Int64Slic) Less(i, j int) bool { return p[i] < p[j] }
func (p Int64Slic) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

type PPTRecordItem struct {
	TimeStamp uint64 // 时间戳 单位是毫秒
	Url       string // 此时使用的课间链接
}

type RoomInfo struct {
	RoomId                 uint32                       // 群聊Id
	CreateUid              MemberDetailInfo             // 创建者uid
	AlreadyInList          map[uint32]*MemberSimpleInfo // 已经加入课程的用户列表 将老师包含在内
	ChannelId              []byte                       // 本次课程频道id
	GroupLessonObid        []byte                       // 群组课程的唯一标识符
	RecordObid             []byte                       // 本次上课记录的唯一标识符 用户发送群消息或者取本次课程录像
	LessonTitle            []byte                       // 课程名称
	LessonAbstract         []byte                       // 课程简介
	TeacherAbstrace        []byte                       // 老是简介
	LessonCoursewareUrl    []byte                       // 整个课间url
	LessonCurrentUrl       []byte                       // 课程当前使用ppt的url
	BeginTime              uint64                       // 群课程开始时间计时
	PPTRecored             []PPTRecordItem              // PPT录像 需要在切换市使用新的协成存储文件 当课程结束时上传PPT切换录像
	OriginLessonCreaterUid uint32
}

type GroupLessonManager struct {
	agoraRecordApi           *common.AgoraRecordApi
	mucApi                   *common.MucApi
	dbManager                *DbUtil
	infoLog                  *log.Logger
	sendSeq                  uint16
	roomInfoLock             sync.RWMutex // sync mutex goroutines use this.roomIdToRoomInfo
	roomIdToRoomInfo         map[uint32]*RoomInfo
	personLessonObidToRoomId map[string]uint32
	filePath                 string
	OSSBukect                string
	OSSRegion                string
	OSSAppID                 string
	OSSAppKey                string
	OSSPrefix                string
	nsqProducer              *nsq.Producer
	nsqTopic                 string
}

func NewGroupLessonManager(dbUtil *DbUtil,
	recordApi *common.AgoraRecordApi,
	muc *common.MucApi,
	logger *log.Logger,
	setPath string,
	ossBucket, ossRegion, ossAppId, ossAppKey, ossPrefix string,
	globleProducer *nsq.Producer,
	nsqOpLogTopic string) *GroupLessonManager {
	return &GroupLessonManager{
		agoraRecordApi:           recordApi,
		mucApi:                   muc,
		dbManager:                dbUtil,
		infoLog:                  logger,
		roomIdToRoomInfo:         map[uint32]*RoomInfo{},
		personLessonObidToRoomId: map[string]uint32{},
		sendSeq:                  0,
		filePath:                 setPath,
		OSSBukect:                ossBucket,
		OSSRegion:                ossRegion,
		OSSAppID:                 ossAppId,
		OSSAppKey:                ossAppKey,
		OSSPrefix:                ossPrefix,
		nsqProducer:              globleProducer,
		nsqTopic:                 nsqOpLogTopic,
	}
}

func (this *GroupLessonManager) GetPacketSeq() (packetSeq uint16) {
	this.sendSeq++
	packetSeq = this.sendSeq
	return packetSeq
}

func (this *GroupLessonManager) SavePPTRecordToFile(filePath string, recordObid string, recordPPT []PPTRecordItem) (recordFileName string, err error) {
	rootObj := simplejson.New()
	rootObj.Set("recordobid", recordObid)
	rootObj.Set("recordppt", recordPPT)
	saveData, err := rootObj.MarshalJSON()
	if err != nil {
		return recordFileName, err
	}
	recordFileName = filePath + recordObid + ".txt"
	err = ioutil.WriteFile(recordFileName, saveData, 0644)
	if err != nil {
		return recordFileName, err
	}
	return recordFileName, nil
}

func (this *GroupLessonManager) UploadToOSS(fileName string, roomId uint32) (fullFileName string, err error) {
	// Step1: param check
	if len(fileName) == 0 {
		this.infoLog.Printf("UploadToOSS fileName empty")
		err = util.ErrInvalidParam
		return fullFileName, err
	}
	this.infoLog.Printf("UploadToOSS fileName=%s", fileName)
	endPoint := this.OSSRegion
	accessKeyId := this.OSSAppID
	accessKeySecret := this.OSSAppKey
	bucket := this.OSSBukect

	client, err := oss.New(endPoint, accessKeyId, accessKeySecret)
	if err != nil {
		this.infoLog.Printf("UploadToOSS fileName=%s oss.New err=%s",
			fileName,
			err)
		return fullFileName, err
	}

	newBucket, err := client.Bucket(bucket)
	if err != nil {
		this.infoLog.Printf("UploadToOSS fileName=%s client.Bucket err=%s",
			fileName,
			err)
		return fullFileName, err
	}
	//file 1 2 3, //
	//命名规则 bai/{fileType}/{imagecount}/9.jpg
	fileExt := webcommon.GetExt(fileName)
	md5, err := webcommon.Md5File(fileName)
	if err != nil {
		this.infoLog.Printf("UploadToOSS fileName=%s webcommon.Md5File err=%s",
			fileName,
			err)
		return fullFileName, err
	}
	this.infoLog.Printf("UploadToOSS fileName=%s md5=%s fileExt=%s",
		fileName,
		md5,
		fileExt)

	prefix := this.OSSPrefix
	srcFileObj := prefix + fmt.Sprintf("%v", roomId) + "/m_" + md5 + fileExt
	this.infoLog.Printf("UploadToOSS localFile=%s srcFileObj=%s", fileName, srcFileObj)
	err = newBucket.PutObjectFromFile(srcFileObj, fileName)
	if err != nil {
		this.infoLog.Printf("UploadToOSS fileName=%s srcFileObj=%s err=%s",
			fileName,
			srcFileObj,
			err)
		return fullFileName, err
	}
	fullFileName = this.OSSBukect + "." + this.OSSRegion + "/" + srcFileObj
	this.infoLog.Printf("UploadToOSS roomId=%v fullFileName=%s", roomId, fullFileName)
	return fullFileName, nil
}

// 创建个人课程
func (this *GroupLessonManager) CreatePersonalLesson(createUid uint32, md5 string, lessonTitle []byte, lessonAbstract []byte, teacherAbstract []byte, lessonCoursewareUrl []byte, coverUrl []byte) (obid string, err error) {
	if createUid == 0 ||
		len(lessonTitle) == 0 ||
		len(lessonAbstract) == 0 {
		this.infoLog.Printf("CreatePersonalLesson input param error createUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s",
			createUid,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareUrl,
			coverUrl)
		err = ErrInputParam
		return obid, err
	}
	obid, err = this.dbManager.StorePersonalLessonInfo(createUid, md5, lessonTitle, lessonAbstract, teacherAbstract, lessonCoursewareUrl, coverUrl)
	return obid, err
}

// 更新个人课程
func (this *GroupLessonManager) UpdatePersonalLesson(createrUid uint32, obid []byte, lessonTitle []byte, lessonAbstract []byte, teacherAbstract []byte, lessonCoursewareUrl []byte, coverUrl []byte) (err error) {
	if createrUid == 0 ||
		len(obid) == 0 ||
		len(lessonTitle) == 0 ||
		len(lessonAbstract) == 0 {
		this.infoLog.Printf("UpdatePersonalLesson input param error createrUid=%v obid=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s",
			createrUid,
			obid,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareUrl,
			coverUrl)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.UpdatePersonalLessonInfo(obid, createrUid, lessonTitle, lessonAbstract, teacherAbstract, lessonCoursewareUrl, coverUrl)
	return err
}

// 删除个人课程
func (this *GroupLessonManager) DelPersonlLesson(createrUid uint32, obid []byte) (err error) {
	if createrUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("DelPersonlLesson input param error createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return err
	}
	// 当前课程正在某个群聊上课中 返回错误码
	if roomId, ok := this.personLessonObidToRoomId[string(obid)]; ok {
		this.infoLog.Printf("DelPersonlLesson createrUid=%v obid=%s roomId=%v is lessoning", createrUid, obid, roomId)
		err = ErrPersonalLessonIsLessoning
		return err
	}
	// 没有群组正在上当前课程直接删除
	err = this.dbManager.DelPersonalLessonByObid(createrUid, obid)
	if err != nil {
		return err
	}
	err = this.dbManager.DelGroupLessonByPersonalLessonObid(createrUid, obid)
	return err
}

// 分批取个人课程列表
func (this *GroupLessonManager) GetPersonalLessonByObid(createrUid uint32, cliObid []byte) (lessonList []PersonalLessonStore, maxObid []byte, err error) {
	if createrUid == 0 || len(cliObid) == 0 {
		this.infoLog.Printf("GetPersonalLessonByObid createrUid=%v cliObid=%s", createrUid, cliObid)
		err = ErrInputParam
		return nil, nil, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetPersonalLessonByObid invalid obid createrUid=%v obid=%s", createrUid, cliObid)
		err = ErrInputParam
		return nil, nil, err
	}
	lessonList, maxObid, err = this.dbManager.GetPersonalLessonByObid(createrUid, cliObid)
	return lessonList, maxObid, err
}

// 判断个人课程是否已经运用到某个群聊当中
func (this *GroupLessonManager) IsPersonalLessonApplyToGroup(obid []byte, roomId uint32) (isApply bool, err error) {
	if len(obid) == 0 || roomId == 0 {
		this.infoLog.Printf("IsPersonalLessonApplyToGroup invalid param obid=%s roomId=%v",
			obid,
			roomId)
		err = ErrInputParam
		return isApply, err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("IsPersonalLessonApplyToGroup invalid obid=%s roomId=%v",
			obid,
			roomId)
		err = ErrInputParam
		return isApply, err
	}
	isApply, err = this.dbManager.IsPersonalLessonApplyToGroup(obid, roomId)
	return isApply, err
}

// 创建群组课程 lessonBeginTime 为客户端输入的字符串服务端只需存储无需调整格式
func (this *GroupLessonManager) CreateGroupLesson(createUid, roomId uint32, personalLessonObid []byte, lessonBeginTime string) (obid string, err error) {
	if createUid == 0 ||
		roomId == 0 ||
		len(personalLessonObid) == 0 {
		this.infoLog.Printf("CreateGroupLesson input param error createUid=%v roomId=%v personalLessonObid=%s lessonBeginTime=%s",
			createUid,
			roomId,
			personalLessonObid,
			lessonBeginTime)
		err = ErrInputParam
		return obid, err
	}
	obid, err = this.dbManager.StoreGroupLessonInfo(createUid, roomId, personalLessonObid, lessonBeginTime)
	if err == nil { // 将群id添加到个人课程已经运营的群中
		err = this.dbManager.UpdatePersonalLessonApplyToGroup(personalLessonObid, roomId, true)
		if err != nil {
			this.infoLog.Printf("CreateGroupLesson dbManager.UpdatePersonalLessonApplyToGroup createUid=%v roomId=%v personalLessonObid=%s err=%s",
				createUid,
				roomId,
				personalLessonObid,
				err)
		}
	}
	return obid, err
}

// 更新群组课程
func (this *GroupLessonManager) UpdateGroupLesson(obid []byte, createrUid, roomId uint32, lessonBeginTime string) (err error) {
	if createrUid == 0 ||
		roomId == 0 ||
		len(obid) == 0 {
		this.infoLog.Printf("UpdateGroupLesson input param error createrUid=%v roomId=%v obid=%s lessonBeginTime=%s",
			createrUid,
			roomId,
			obid,
			lessonBeginTime)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.UpdateGroupLessonInfo(obid, createrUid, roomId, lessonBeginTime)
	return err
}

// 删除群组课程
func (this *GroupLessonManager) DelGroupLesson(obid []byte, createrUid, roomId uint32) (err error) {
	if createrUid == 0 || len(obid) == 0 || roomId == 0 {
		this.infoLog.Printf("DelGroupLesson input param error createrUid=%v obid=%s roomId=%v", createrUid, obid, roomId)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.DelGroupLessonByObid(obid, createrUid, roomId)
	if err == nil {
		err_inter := this.dbManager.UpdatePersonalLessonApplyToGroup(obid, roomId, false)
		if err_inter != nil {
			this.infoLog.Printf("DelGroupLesson dbManager.UpdatePersonalLessonApplyToGroup createrUid=%v obid=%s roomId=%v err=%s",
				createrUid,
				obid,
				roomId,
				err_inter)

		}
	}
	return err
}

// 分批取群组课程列表
func (this *GroupLessonManager) GetGroupLessonByObid(reqUid uint32, roomId uint32, cliObid []byte) (lessonList []GroupLessonStore, maxObid []byte, err error) {
	if reqUid == 0 || roomId == 0 || len(cliObid) == 0 {
		this.infoLog.Printf("GetGroupLessonByObid reqUid=%v roomId=%v cliObid=%s", reqUid, roomId, cliObid)
		err = ErrInputParam
		return nil, nil, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetGroupLessonByObid invalid obid reqUid=%v roomId=%v cliObid=%s", reqUid, roomId, cliObid)
		err = ErrInputParam
		return nil, nil, err
	}
	lessonList, maxObid, err = this.dbManager.GetGroupLessonByObid(reqUid, roomId, cliObid)
	return lessonList, maxObid, err
}

// 判断当前房间是否正在进行教学
func (this *GroupLessonManager) IsRoomLessoning(roomId uint32) (ok bool, err error) {
	if roomId == 0 {
		err = ErrInputParam
		this.infoLog.Printf("IsRoomLessoning invalid param roomId=%v", roomId)
		ok = false
		return ok, err
	}
	this.roomInfoLock.RLock()
	defer this.roomInfoLock.RUnlock()
	_, ok = this.roomIdToRoomInfo[roomId]
	this.infoLog.Printf("IsRoomLessoning roomId=%v isTeaching=%v", roomId, ok)
	return ok, nil
}

func (this *GroupLessonManager) IsLessonCreater(roomId, reqUid uint32) (isCreater bool, err error) {
	if roomId == 0 || reqUid == 0 {
		this.infoLog.Printf("IsRoomCreater roomId=%v reqUid=%v param error", roomId, reqUid)
		err = ErrInputParam
		return isCreater, err
	}
	this.roomInfoLock.RLock()
	defer this.roomInfoLock.RUnlock()
	if v, ok := this.roomIdToRoomInfo[roomId]; ok {
		if reqUid == v.CreateUid.Uid {
			isCreater = true
		} else {
			isCreater = false
		}
	} else {
		isCreater = false
	}
	return isCreater, nil
}

func (this *GroupLessonManager) AddGroupLessonInfo(roomId uint32,
	teacherUid *MemberDetailInfo,
	channelId []byte,
	groupLessonObid []byte,
	firstUrl []byte,
	lessonTitle []byte,
	lessonAbstract []byte,
	teacherAbstract []byte,
	lessonCoursewareurl []byte) (recordObid []byte, beginTime uint64, err error) {
	if roomId == 0 || teacherUid == nil || teacherUid.Uid == 0 || len(channelId) == 0 || len(groupLessonObid) == 0 || len(lessonTitle) == 0 {
		this.infoLog.Printf("AddGroupLessonInfo invalid param roomId=%v teacherUid.Uid=%v teacherUid.Name=%s channelId=%s groupLessonObid=%s firstUrl=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCourseUrl=%s",
			roomId,
			teacherUid.Uid,
			teacherUid.NickName,
			channelId,
			groupLessonObid,
			firstUrl,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareurl)
		err = ErrInputParam
		return nil, beginTime, err
	}

	// Step1: 首先查看内存中是否已经存在该教学房间相关信息
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	if _, ok := this.roomIdToRoomInfo[roomId]; ok {
		this.infoLog.Printf("AddGroupLessonInfo room already exist createUid=%v roomId=%v channelId=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCourseUrl=%s",
			teacherUid.Uid,
			roomId,
			channelId,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareurl)
		err = ErrAlreadyExist
		return nil, beginTime, err
	}
	// Step2 : 存储成功之后更新内存中的map
	beginTime = uint64(time.Now().UnixNano() / 1000000)
	tempRecordObid, err := this.dbManager.UpdateGroupLessonRecordStartTime(roomId, teacherUid.Uid, groupLessonObid)
	if err != nil {
		this.infoLog.Printf("AddGroupLessonInfo createUid=%v roomId=%v channelId=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCourseUrl=%s err=%s",
			teacherUid.Uid,
			roomId,
			channelId,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareurl,
			err)
		return nil, beginTime, err
	}
	recordObid = []byte(tempRecordObid)
	groupLessonStore, err := this.GetGroupLessonByOneObid(roomId, groupLessonObid)
	if err != nil {
		this.infoLog.Printf("AddGroupLessonInfo createUid=%v roomId=%v channelId=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCourseUrl=%s GetGroupLessonByOneObid failed err=%s",
			teacherUid.Uid,
			roomId,
			channelId,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareurl,
			err)
		// not need to return
	}
	roomInfo := &RoomInfo{
		RoomId:    roomId,
		CreateUid: *teacherUid,
		AlreadyInList: map[uint32]*MemberSimpleInfo{
			teacherUid.Uid: &MemberSimpleInfo{
				Uid:      teacherUid.Uid,
				JoinTs:   time.Now().UnixNano(),
				ActiveTS: time.Now().Unix(),
			}},
		ChannelId:           channelId,
		GroupLessonObid:     groupLessonObid,
		RecordObid:          recordObid,
		LessonTitle:         lessonTitle,
		LessonAbstract:      lessonAbstract,
		TeacherAbstrace:     teacherAbstract,
		LessonCoursewareUrl: lessonCoursewareurl,
		LessonCurrentUrl:    firstUrl,
		BeginTime:           beginTime,
		PPTRecored: []PPTRecordItem{
			PPTRecordItem{
				TimeStamp: uint64(time.Now().UnixNano() / 1000000),
				Url:       string(firstUrl),
			},
		},
		OriginLessonCreaterUid: groupLessonStore.CreaterUid,
	}

	this.roomIdToRoomInfo[roomId] = roomInfo
	this.infoLog.Printf("AddGroupLessonInfo new roomInfo createUid=%v roomId=%v channelId=%s groupLessonObid=%s lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCourseUrl=%s",
		teacherUid.Uid,
		roomId,
		channelId,
		groupLessonObid,
		lessonTitle,
		lessonAbstract,
		teacherAbstract,
		lessonCoursewareurl)

	return recordObid, beginTime, err
}

func (this *GroupLessonManager) GetRoomInfoByRoomId(roomId uint32) (roomInfo RoomInfo, err error) {
	if roomId == 0 {
		this.infoLog.Printf("GetRoomInfoByRoomId roomId=%v input err", roomId)
		err = ErrInputParam
		return roomInfo, err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("GetRoomInfoByRoomId GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return roomInfo, err
	}
	roomInfo = *roomInfoPoint
	return roomInfo, nil
}

func (this *GroupLessonManager) UpdateCurrentUrlByRoomId(roomId uint32, url []byte) (err error) {
	if roomId == 0 || len(url) == 0 {
		this.infoLog.Printf("GetRoomInfoByRoomId roomId=%v url=%s input err", roomId, url)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("GetRoomInfoByRoomId GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	roomInfoPoint.LessonCurrentUrl = url
	roomInfoPoint.PPTRecored = append(roomInfoPoint.PPTRecored, PPTRecordItem{
		TimeStamp: uint64(time.Now().UnixNano() / 1000000),
		Url:       string(url),
	})
	return nil
}

func (this *GroupLessonManager) GetRoomInfoWithOutLock(roomId uint32) (roomInfo *RoomInfo, err error) {
	roomInfo, ok := this.roomIdToRoomInfo[roomId]
	if ok {
		this.infoLog.Printf("GetRoomInfoWithOutLock roomId=%v exist in memory", roomId)
		return roomInfo, nil
	} else {
		this.infoLog.Printf("GetRoomInfoWithOutLock roomId=%v not exist in memory", roomId)
		err = ErrNotExist
		return nil, err
	}
}

func (this *GroupLessonManager) AddReqUserToAlreadyInList(roomId, reqUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 {
		this.infoLog.Printf("AddReqUserToAlreadyInList param error roomId=%v reqUid=%v",
			roomId,
			reqUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("AddReqUserToAlreadyInList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	roomInfoPoint.AlreadyInList[reqUid] = &MemberSimpleInfo{
		Uid:      reqUid,
		JoinTs:   time.Now().UnixNano(),
		ActiveTS: time.Now().Unix(),
	}
	return nil
}

func (this *GroupLessonManager) GetSortUidListFromAlreadyIn(alreadyInMap map[uint32]*MemberSimpleInfo) (uidList []uint32) {
	if alreadyInMap == nil {
		this.infoLog.Printf("GetSortUidListFromAlreadyIn alreadyInMap nil")
		return nil
	}
	joinTsToUid := make(map[int64]uint32, len(alreadyInMap))
	var joinTsSlic Int64Slic
	for i, v := range alreadyInMap {
		joinTsToUid[v.JoinTs] = i
		joinTsSlic = append(joinTsSlic, v.JoinTs)
	}
	sort.Stable(joinTsSlic)
	this.infoLog.Printf("GetSortUidListFromAlreadyIn sort ts=%v", joinTsSlic)
	for _, v := range joinTsSlic {
		uidList = append(uidList, joinTsToUid[v])
	}
	this.infoLog.Printf("GetSortUidListFromAlreadyIn sort uidList=%v", uidList)
	return uidList
}

func (this *GroupLessonManager) RemoveReqUserFromAlreadyInList(roomId, reqUid uint32) (inTime uint64, err error) {
	if roomId == 0 || reqUid == 0 {
		this.infoLog.Printf("RemoveReqUserFromAlreadyInList param error roomId=%v reqUid=%v ",
			roomId,
			reqUid)
		err = ErrInputParam
		return inTime, err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("RemoveReqUserFromAlreadyInList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return inTime, err
	}
	if v, ok := roomInfoPoint.AlreadyInList[reqUid]; ok {
		inTime = uint64(v.JoinTs)
	}
	delete(roomInfoPoint.AlreadyInList, reqUid)
	return inTime, nil
}

func (this *GroupLessonManager) RemoveReqUserFromAlreadyInListThreatUnSafe(roomId, reqUid uint32) (err error) {
	if roomId == 0 || reqUid == 0 {
		this.infoLog.Printf("RemoveReqUserFromAlreadyInListThreatUnSafe param error roomId=%v reqUid=%v",
			roomId,
			reqUid)
		err = ErrInputParam
		return err
	}
	// 在GetRoomInfoWithOutLock时可能会添加元素所以需要Lock 而不是RWLock
	roomInfoPoint, err := this.GetRoomInfoWithOutLock(roomId)
	if err != nil {
		this.infoLog.Printf("RemoveReqUserFromHandsUpList GetRoomInfoWithOutLock roomId=%v failed err=%s", roomId, err)
		return err
	}
	delete(roomInfoPoint.AlreadyInList, reqUid)
	return nil
}

func (this *GroupLessonManager) DelRoomInfo(roomId uint32) (endTime uint64, err error) {
	if roomId == 0 {
		this.infoLog.Printf("DelRoomInfo invalid param roomId=%v", roomId)
		err = ErrInputParam
		return endTime, err
	}
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	delete(this.roomIdToRoomInfo, roomId)
	endTime = uint64(time.Now().UnixNano() / 1000000)
	return endTime, nil
}

func (this *GroupLessonManager) HandleHeartBeat(roomId, uid uint32) (ret bool) {
	this.roomInfoLock.Lock()
	defer this.roomInfoLock.Unlock()
	if v, ok := this.roomIdToRoomInfo[roomId]; ok {
		if simpleInfo, searchRes := v.AlreadyInList[uid]; searchRes {
			if (time.Now().Unix() - simpleInfo.ActiveTS) > USE_TIMEOUT_THRESHOLD {
				// 老师掉线 直接广播课程结束 删除整个课程记录
				if uid == v.CreateUid.Uid {
					retCode, err := this.mucApi.BroadCastEndGroupLesson(
						roomId,
						uid,
						v.CreateUid.NickName,
						v.ChannelId,
						uint64(time.Now().UnixNano()/1000000))
					if err != nil || retCode != 0 {
						attr := "grouplesson/req_bc_end_lesson_failed"
						libcomm.AttrAdd(attr, 1)
						//将整个群的缓存信息删除
						delete(this.roomIdToRoomInfo, roomId)
						return false
					}
				} else {
					// 非老师掉线 删除成员并广播
					this.RemoveReqUserFromAlreadyInListThreatUnSafe(roomId, uid)
					// get user slic
					alreadyInList := v.AlreadyInList
					alreadyInSlic := this.GetSortUidListFromAlreadyIn(alreadyInList)
					code, err := this.BroadCastGroupLessonStat(roomId,
						uid,
						alreadyInSlic,
						v.LessonCurrentUrl,
						uint64(time.Now().UnixNano()/1000000))
					if err != nil {
						this.infoLog.Printf("roomId=%v uid=%v BroadCastRoomStat failed code=%v err=%s",
							roomId,
							uid,
							code,
							err)
					}
				}
				ret = false
			} else {
				v.AlreadyInList[uid].ActiveTS = time.Now().Unix()
				ret = true
			}
		} else {
			this.infoLog.Printf("HandleHeartBeat uid not exist roomId=%v uid=%v ", roomId, uid)
			ret = true
		}
	} else {
		this.infoLog.Printf("HandleHeartBeat roomId not exist roomId=%v uid=%v", roomId, uid)
		ret = true
	}
	return ret
}

func (this *GroupLessonManager) BroadCastGroupLessonStat(roomId, reqUid uint32,
	alreadyInSlic []uint32,
	lessonCurrentUrl []byte,
	sessionTime uint64) (code uint32, err error) {
	if roomId == 0 {
		err = ErrInputParam
		this.infoLog.Printf("BroadCastGroupLessonStat roomId=%v alreadyInSlic=%v lessonCurrentUrl=%s sessionTime=%v",
			roomId,
			alreadyInSlic,
			lessonCurrentUrl,
			sessionTime)
		return code, err
	}
	// Step5: begin broad cast
	code, err = this.mucApi.BroadCastGroupLessonStatChange(roomId,
		reqUid,
		alreadyInSlic,
		lessonCurrentUrl,
		sessionTime)
	this.infoLog.Printf("BroadCastGroupLessonStat roomId=%v alreadyInSlic=%v lessonCurrentUrl=%s sessionTime=%v code=%v err=%s",
		roomId,
		alreadyInSlic,
		lessonCurrentUrl,
		sessionTime,
		code,
		err)
	return code, err
}

func (this *GroupLessonManager) AsyncDo() {
	asyncDo(this.CheckAlreadyInUserAlive)
}

func asyncDo(fn func()) {
	go func() {
		fn()
	}()
}

func (this *GroupLessonManager) CheckAlreadyInUserAlive() {
	defer func() {
		recover()
	}()

	for {
		time.Sleep(10 * time.Second) // every 10 second
		this.roomInfoLock.Lock()
		for roomId, session := range this.roomIdToRoomInfo {
			this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v createUid=%v",
				roomId,
				session.CreateUid.Uid)
			isTeacherTimeOut := false
			for uid, simpleInfo := range session.AlreadyInList {
				ret := this.IsUserAlive(uid, simpleInfo.ActiveTS)
				if !ret { // user timeout
					// 如果是老师超时直接广播课程结束同时删除群课程的缓存信息
					this.infoLog.Printf("CheckAlreadyInUserAlive uid=%v lastActiveTime=%v timeout", uid, simpleInfo.ActiveTS)
					if uid == session.CreateUid.Uid {
						isTeacherTimeOut = true
					}
					// 删除timeout 用户
					delete(session.AlreadyInList, uid)
					alreadyInSlic := []uint32{}
					for i, _ := range session.AlreadyInList {
						alreadyInSlic = append(alreadyInSlic, i)
					}
					code, err := this.BroadCastGroupLessonStat(roomId, uid, alreadyInSlic, session.LessonCurrentUrl, session.BeginTime)
					if err != nil || code != 0 {
						attr := "grouplesson/bc_lesson_info_failed"
						libcomm.AttrAdd(attr, 1)
						this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v uid=%v BroadCastGroupLessonStat err=%s", roomId, uid, err)
					}
					createrUid := session.OriginLessonCreaterUid
					isTeacher := uint32(0)
					if uid == session.CreateUid.Uid {
						isTeacher = 1
					}
					inTime := uint64(simpleInfo.JoinTs / 1000000) // 将纳秒转换成毫秒
					outTime := uint64(time.Now().UnixNano() / 1000000)
					err = this.PublishGroupLessonOpLog(uid, createrUid, isTeacher, inTime, outTime, string(session.GroupLessonObid), string(session.RecordObid))
					if err != nil {
						this.infoLog.Printf("CheckAlreadyInUserAlive PublishGroupLessonOpLog roomId=%v reqUid=%v failed err=%s",
							roomId,
							uid,
							err)
						attr := "grouplesson/publish_to_nsq_failed"
						libcomm.AttrAdd(attr, 1)
					}
				}
			}

			if isTeacherTimeOut || len(session.AlreadyInList) == 0 { // 整个用户全部超时了直接删除room
				this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v isTeacherTimeOut=%v all timeout", roomId, isTeacherTimeOut)
				retCode, err := this.mucApi.BroadCastEndGroupLesson(
					roomId,
					session.CreateUid.Uid,
					session.CreateUid.NickName,
					session.ChannelId,
					uint64(time.Now().UnixNano()/1000000))
				if err != nil || retCode != 0 {
					this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v createUid=%v BroadCastEndGroupLesson failed err=%s",
						session.RoomId,
						session.CreateUid.Uid,
						err)
				}

				// 先广播结束 再将其删除 不能颠倒循序
				delete(this.roomIdToRoomInfo, roomId)

				// 结束Agora音频录制
				err = this.agoraRecordApi.EndRecord(session.CreateUid.Uid, session.RoomId, session.ChannelId, session.RecordObid)
				if err != nil {
					this.infoLog.Printf("CheckAlreadyInUserAlive teacherUid=%v roomId=%v channelId=%s endrecod failed err=%s",
						session.CreateUid.Uid,
						session.RoomId,
						session.ChannelId,
						err)
					attr := "grouplesson/end_record_failed"
					libcomm.AttrAdd(attr, 1)
				}
				// 记录课程结束时间
				endTimeStamp := uint64(time.Now().UnixNano() / 1000000)
				// 所有人都超时了也需要保存PPT录像 更新PPTurl
				fileName, err := this.SavePPTRecordToFile(this.filePath, string(session.RecordObid), session.PPTRecored)
				this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v all timeout SavePPTRecordToFile failed filePath=%s obid=%s err=%s",
					roomId,
					this.filePath,
					session.RecordObid,
					err)
				if err == nil {
					this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v filePath=%s obid=%s fileName=%s",
						roomId,
						this.filePath,
						session.RecordObid,
						fileName)
					fullFileName, err := this.UploadToOSS(fileName, roomId)
					if err != nil {
						this.infoLog.Printf("CheckAlreadyInUserAlive roomId=%v obid=%s fileName=%s UploadToOSS failed err=%s",
							roomId,
							session.RecordObid,
							fileName,
							err)
						attr := "grouplesson/upload_group_lesson_ppt_failed"
						libcomm.AttrAdd(attr, 1)
					} else {
						err = this.UpdateGroupLessonRecordPPTUrl(session.RoomId, session.CreateUid.Uid, string(session.RecordObid), fullFileName, endTimeStamp)
						if err != nil {
							attr := "grouplesson/update_group_lesson_ppt_url_failed"
							libcomm.AttrAdd(attr, 1)
							this.infoLog.Printf("CheckAlreadyInUserAlive teacherUid=%v teacherName=%s roomId=%v obid=%s channelId=%s UpdateGroupLessonRecordPPTUrl err=%s",
								session.CreateUid.Uid,
								session.CreateUid.NickName,
								session.RoomId,
								session.RecordObid,
								session.ChannelId,
								err)
						}
					}
				}
			}
		}
		this.roomInfoLock.Unlock()
	}
}

func (this *GroupLessonManager) IsUserAlive(uid uint32, lastActiveTime int64) (ret bool) {
	duration := time.Now().Unix() - lastActiveTime
	if duration >= int64(USE_TIMEOUT_THRESHOLD) {
		ret = false
	} else {
		ret = true
	}
	return ret
}

func (this *GroupLessonManager) UpdateGroupLessonRecordPPTUrl(roomId, reqUid uint32, obid string, url string, endTime uint64) (err error) {
	if len(obid) == 0 || roomId == 0 {
		this.infoLog.Printf("UpdateGroupLessonRecordPPTUrl invalid param roomId=%s reqUid=%v obid=%s", roomId, reqUid, obid)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateGroupLessonRecordPPTUrl invalid obid roomId=%s reqUid=%v obid=%v", roomId, reqUid, obid)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.UpdateGroupLessonRecordPPTUrl(roomId, reqUid, []byte(obid), url, endTime)
	return err
}

func (this *GroupLessonManager) UpdateGroupLessonRecordVoiceUrl(roomId, reqUid uint32, obid, url string) (err error) {
	if roomId == 0 || reqUid == 0 || len(obid) == 0 || len(url) == 0 {
		this.infoLog.Printf("UpdateGroupLessonRecordVoiceUrl invalid param roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(obid)
	if !ret {
		this.infoLog.Printf("UpdateGroupLessonRecordVoiceUrl invalid obid roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.UpdateGroupLessonRecordVoiceUrl(roomId, reqUid, []byte(obid), url)
	return err
}

func (this *GroupLessonManager) GetGroupLessonByOneObid(roomId uint32, cliObid []byte) (lesson GroupLessonStore, err error) {
	if len(cliObid) == 0 {
		this.infoLog.Printf("GetGroupLessonByOneObid invalid param roomId=%v cliObid=%v", roomId, cliObid)
		err = ErrInputParam
		return lesson, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetGroupLessonByOneObid invalid obid roomId=%v cliObid=%s", roomId, cliObid)
		err = ErrInputParam
		return lesson, err
	}
	lesson, err = this.dbManager.GetGroupLessonByOneObid(roomId, cliObid)
	return lesson, err
}

func (this *GroupLessonManager) GetPersonalLessonByOneObid(cliObid []byte) (lesson PersonalLessonStore, err error) {
	if len(cliObid) == 0 {
		this.infoLog.Printf("GetPersonalLessonByOneObid invalid param cliObid=%v", cliObid)
		err = ErrInputParam
		return lesson, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetPersonalLessonByOneObid invalid obid cliObid=%s", cliObid)
		err = ErrInputParam
		return lesson, err
	}
	lesson, err = this.dbManager.GetPersonalLessonByOneObid(cliObid)
	return lesson, err
}

func (this *GroupLessonManager) GetRecordListByGroupLessonObid(roomId uint32, cliObid []byte) (recordList []RecordStore, err error) {
	if roomId == 0 || len(cliObid) == 0 {
		this.infoLog.Printf("GetRecordListByGroupLessonObid invalid param roomId=%v cliObid=%v", roomId, cliObid)
		err = ErrInputParam
		return nil, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetRecordListByGroupLessonObid invalid obid roomId=%v cliObid=%s", roomId, cliObid)
		err = ErrInputParam
		return nil, err
	}
	recordList, err = this.dbManager.GetRecordListByGroupLessonObid(roomId, cliObid)
	return recordList, err
}

// 删除群课程录像
func (this *GroupLessonManager) DelGroupLessonRecord(createrUid uint32, obid []byte) (err error) {
	if createrUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("DelGroupLessonRecord input param error createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return err
	}
	err = this.dbManager.DelRecordByObid(createrUid, obid)
	return err
}

// 查询记录的状态
func (this *GroupLessonManager) QueryGroupLessonRecordStat(createrUid uint32, obid []byte) (isDelete bool, err error) {
	if createrUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("QueryGroupLessonRecordStat input param error createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return isDelete, err
	}
	isDelete, err = this.dbManager.QueryRecordStatByObid(createrUid, obid)
	return isDelete, err
}

// 查询记录的详情
func (this *GroupLessonManager) GetRecordByObid(createrUid uint32, obid []byte) (recordInfo *RecordStore, err error) {
	if createrUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("GetRecordByObid input param error createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return nil, err
	}
	recordInfo, err = this.dbManager.GetRecordByObidFromDb(createrUid, obid)
	return recordInfo, err
}

// 通过uid 删除群组课程
func (this *GroupLessonManager) DelGroupLessonByUid(createrUid, roomId uint32) (err error) {
	if createrUid == 0 || roomId == 0 {
		this.infoLog.Printf("DelGroupLesson input param error createrUid=%v roomId=%v", createrUid, roomId)
		err = ErrInputParam
		return err
	}
	obidList, err := this.dbManager.DelGroupLessonByUid(createrUid, roomId)
	if err == nil {
		for _, v := range obidList {
			err = this.dbManager.UpdatePersonalLessonApplyToGroup([]byte(v), roomId, false)
			if err != nil {
				this.infoLog.Printf("DelGroupLesson dbManager.UpdatePersonalLessonApplyToGroup createrUid=%v obid=%s roomId=%v err=%s",
					createrUid,
					v,
					roomId,
					err)
			}
		}
	}
	return err
}

// 通过个人的lessonobid 查询所运用的群聊
func (this *GroupLessonManager) GetGroupLessonSlicByPersonalLessonObid(createrUid uint32, obid []byte) (groupLessonSlic []GroupLessonItem, err error) {
	if createrUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("GetGroupLessonSlicByPersonalLessonObid input param error createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return nil, err
	}
	groupLessonSlic, err = this.dbManager.GetGroupLessonSlicByPersonalLessonObid(createrUid, obid)
	return groupLessonSlic, err
}

func (this *GroupLessonManager) PublishGroupLessonOpLog(reqUid, createrUid, isTeacher uint32,
	inTime, outTime uint64,
	groupLessonObid, recordObid string) (err error) {
	// 构造HTHeadV2 报文
	headV2 := &common.HeadV2{
		Cmd:     uint32(ht_oplog.CMD_TYPE_CMD_COMMIT_GROUP_LESSON_OP_REQ),
		Uid:     reqUid,
		SysType: uint16(ht_oplog.OPLOG_SYS_TYPE_CACHE_SYS_IM),
	}

	reqBody := &ht_oplog.OpLogReqBody{
		CommitGroupLessonOpReqbody: &ht_oplog.CommitGroupLessonOpReqBody{
			ReqUid:          proto.Uint32(reqUid),
			InTime:          proto.Uint64(inTime),
			OutTime:         proto.Uint64(outTime),
			GroupLessonObid: proto.String(groupLessonObid),
			CreaterUid:      proto.Uint32(createrUid),
			IsTeacher:       proto.Uint32(isTeacher),
			RecordObid:      proto.String(recordObid),
		},
	}

	s, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("PublishGroupLessonOpLog reqUid=%v createrUid=%v isTeacher=%v inTime=%v outTime=%v groupLessonObid=%s recordObid=%s err=%s",
			reqUid,
			createrUid,
			isTeacher,
			inTime,
			outTime,
			groupLessonObid,
			recordObid,
			err)
		return err
	}

	headV2.Len = uint32(common.PacketV2HeadLen) + uint32(len(s)) + 1
	headV2Buf := make([]byte, headV2.Len)
	headV2Buf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(headV2, headV2Buf[1:])
	if err != nil {
		this.infoLog.Printf("PublishGroupLessonOpLog reqUid=%v createrUid=%v isTeacher=%v inTime=%v outTime=%v groupLessonObid=%s recordObid=%s err=%s",
			reqUid,
			createrUid,
			isTeacher,
			inTime,
			outTime,
			groupLessonObid,
			recordObid,
			err)
		return err
	}
	copy(headV2Buf[common.PacketV2HeadLen:], s)
	headV2Buf[headV2.Len-1] = common.HTV2MagicEnd

	err = this.nsqProducer.Publish(this.nsqTopic, headV2Buf)
	if err != nil {
		// 统计apns失败总量
		attr := "grouplesson/nsq_publish_failed_req_count"
		libcomm.AttrAdd(attr, 1)
		this.infoLog.Printf("PublishGroupLessonOpLog reqUid=%v createrUid=%v isTeacher=%v inTime=%v outTime=%v groupLessonObid=%s recordObid=%s failed err=%s",
			reqUid,
			createrUid,
			isTeacher,
			inTime,
			outTime,
			groupLessonObid,
			recordObid,
			err)
	} else {
		// 统计apns成功的总量
		attr := "grouplesson/nsq_publish_succ_req_count"
		libcomm.AttrAdd(attr, 1)
		this.infoLog.Printf("PublishGroupLessonOpLog reqUid=%v createrUid=%v isTeacher=%v inTime=%v outTime=%v groupLessonObid=%s recordObid=%s success",
			reqUid,
			createrUid,
			isTeacher,
			inTime,
			outTime,
			groupLessonObid,
			recordObid)
	}
	return nil
}
