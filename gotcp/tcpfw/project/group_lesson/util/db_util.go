// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"log"
	"time"

	"github.com/HT_GOGO/gotcp/libcomm"
	_ "github.com/go-sql-driver/mysql"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Error type
var (
	ErrNilDbObject      = errors.New("not set  object current is nil")
	ErrDbParam          = errors.New("err param error")
	ErrNilNoSqlObject   = errors.New("not set nosql object current is nil")
	ErrCloseExplicitly  = errors.New("Closed explicitly")
	ErrPermissionDenied = errors.New("Permission denied")
)

var (
	LessonDB            = "lesson"
	PersonalLessonTable = "personal_lesson_table"
	GroupLessonTable    = "group_lesson_table"
	RecordTable         = "record_table"
	pageSize            = 20
)

type PersonalLessonStore struct {
	Id                  bson.ObjectId `bson:"_id,omitempty" json:"-"` // 在Mongo中唯一标识一条记录
	Md5                 string        //根据课程信息计算出来的md5 用户去重
	CreaterUid          uint32        //创建者uid
	LessonTitle         string        //课程名称
	LessonAbstract      string        //课程简介
	TeacherAbstract     string        //老是简介
	LessonCoursewareUrl string        //课件url
	CoverUrl            string        //课件第一页的ppt url
	ApplyRoomId         []uint32      //个人课程应用到哪些群聊中
	LessonStat          uint32        //课程状态，删除课程时并不真正的删除，只是标记一下 0:正常 1：删除
	UpdateTS            uint32        //更新时间戳
}

// 开始上课时 需要在录像列表总插入一条新的记录
// 课程结束回调时需要更新Slic中最后一条记录的url
// 当课程录像没有回调时 不影响 url为空下次开始上课时会重新插入记录
type RecordStore struct {
	Id              bson.ObjectId `bson:"_id,omitempty" json:"-"` // 在Mongo中唯一标识一条记录
	TeacherUid      uint32        //老师uid
	RoomId          uint32        //房间roomid
	GroupLessonObid string        //群组课程唯一标识符
	VoiceUrl        string        //语音录音url
	PPTUrl          string        //ppt切换url
	StartTimeStamp  uint64        //课程开始时间
	EndTimeStamp    uint64        //课程结束时间
	LessonDuration  uint64        //课程时长
	RecordStat      uint32        //课程状态，删除课程时并不真正的删除，只是标记一下 0:正常 1：删除
	UpdateTS        uint32        //更新时间
}

type GroupLessonStore struct {
	Id                 bson.ObjectId `bson:"_id,omitempty" json:"-"` // 在Mongo中唯一标识一条记录
	CreaterUid         uint32        //群组课创建者uid
	PersonalLessonObid string        //个人课程唯一标示符
	RoomId             uint32        //房间roomid
	LessonTime         string        //客户端输入的预订的上课时间 不限制格式
	LessonStat         uint32        //课程状态，删除课程时并不真正的删除，只是标记一下 0:正常 1：删除
	UpdateTS           uint32        //更新时间戳
}

type GroupLessonItem struct {
	GroupLessonObid string
	RoomId          uint32
}

type DbUtil struct {
	infoLog *log.Logger
	noSqlDb *mgo.Session
}

func NewDbUtil(mongoSess *mgo.Session, logger *log.Logger) *DbUtil {
	return &DbUtil{
		noSqlDb: mongoSess,
		infoLog: logger,
	}
}

func (this *DbUtil) RefreshSession(err error) {
	// 统计mongodb err count
	attr := "groupLesson/mongodb_error_count"
	libcomm.AttrAdd(attr, 1)
	this.noSqlDb.Refresh()
	this.infoLog.Printf("RefreshSession input err=%s", err)
}

func (this *DbUtil) StorePersonalLessonInfo(createUid uint32, md5 string, lessonTitle []byte, lessonAbstract []byte, teacherAbstract []byte, lessonCoursewareUrl []byte, coverUrl []byte) (obid string, err error) {
	if this.noSqlDb == nil {
		err = ErrNilDbObject
		return obid, err
	}

	if createUid == 0 || len(lessonTitle) == 0 {
		this.infoLog.Printf("StorePersonalLessonInfo invalid param createUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s",
			createUid,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareUrl,
			coverUrl)
		err = ErrDbParam
		return obid, err
	}

	// 将教学房间信息写入Mongo中 首先获取集合对象
	bsObid := bson.NewObjectId()
	obid = bsObid.Hex()

	this.infoLog.Printf("StorePersonalLessonInfo obid=%v obid srting=%v",
		obid,
		bsObid.String(),
	)

	mongoConn := this.noSqlDb.DB(LessonDB).C(PersonalLessonTable)
	lessonInfo := &PersonalLessonStore{
		Id:                  bsObid,
		Md5:                 md5,
		CreaterUid:          createUid,
		LessonTitle:         string(lessonTitle),
		LessonAbstract:      string(lessonAbstract),
		TeacherAbstract:     string(teacherAbstract),
		LessonCoursewareUrl: string(lessonCoursewareUrl),
		CoverUrl:            string(coverUrl),
		ApplyRoomId:         []uint32{},
		LessonStat:          0,
		UpdateTS:            uint32(time.Now().Unix()),
	}

	err = mongoConn.Insert(lessonInfo)
	// 如果RoomId+Uid 作为唯一所以 如果RoomId+Uid已经存在则插入失败
	if err != nil {
		this.RefreshSession(err)
		this.infoLog.Printf("AddLessonInfo Mongo Insert failed createUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s",
			createUid,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareUrl,
			err)
		return obid, err
	}

	// 更新完毕无需再次更新maxOrder了
	return obid, nil
}

func (this *DbUtil) UpdatePersonalLessonInfo(obid []byte, createrUid uint32, lessonTitle []byte, lessonAbstract []byte, teacherAbstract []byte, lessonCoursewareUrl []byte, coverUrl []byte) (err error) {
	if len(obid) == 0 {
		this.infoLog.Printf("UpdatePersonalLessonInfo invalid param obid=%s createrUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s",
			obid,
			createrUid,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareUrl,
			coverUrl)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("UpdatePersonalLessonInfo invalid obid=%s createrUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s lessonCoursewareUrl=%s coverUrl=%s",
			obid,
			createrUid,
			lessonTitle,
			lessonAbstract,
			teacherAbstract,
			lessonCoursewareUrl,
			coverUrl)
		err = ErrInputParam
		return err
	}

	updateTime := uint32(time.Now().Unix())
	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(PersonalLessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid, "createruid": createrUid}, bson.M{"$set": bson.M{"lessontitle": string(lessonTitle), "lessonabstract": string(lessonAbstract), "teacherabstract": string(teacherAbstract), "lessoncoursewareurl": string(lessonCoursewareUrl), "coverurl": string(coverUrl), "updatets": updateTime}})
	return err
}

func (this *DbUtil) IsPersonalLessonApplyToGroup(obid []byte, roomId uint32) (isApply bool, err error) {
	if len(obid) == 0 || roomId == 0 {
		this.infoLog.Printf("IsPersonalLessonApplyToGroup invalid param obid=%s roomId=%v",
			obid,
			roomId)
		err = ErrInputParam
		return isApply, err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("IsPersonalLessonApplyToGroup invalid obid=%s roomId=%v",
			obid,
			roomId)
		err = ErrInputParam
		return isApply, err
	}
	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(PersonalLessonTable)
	var storeIterm PersonalLessonStore
	err = mongoConn.Find(bson.M{"_id": bsObid}).One(&storeIterm)
	if err != nil {
		this.infoLog.Printf("IsPersonalLessonApplyToGroup obid=%s roomId=%v err=%s", obid, roomId, err)
		this.RefreshSession(err)
		err = mongoConn.Find(bson.M{"_id": bsObid}).One(&storeIterm)
		if err != nil {
			this.infoLog.Printf("IsPersonalLessonApplyToGroup retry obid=%s roomId=%v err=%s", obid, roomId, err)
			this.RefreshSession(err)
			return isApply, err
		}
	}

	roomIdSlic := storeIterm.ApplyRoomId
	if len(roomIdSlic) == 0 {
		isApply = false
		return isApply, nil
	}
	for _, v := range roomIdSlic {
		if v == roomId {
			isApply = true
			break
		}
	}
	return isApply, nil
}

// 更新个人课程运用到的群组id
// isAdd: true-->add false-->remove
func (this *DbUtil) UpdatePersonalLessonApplyToGroup(obid []byte, roomId uint32, isAdd bool) (err error) {
	if len(obid) == 0 || roomId == 0 {
		this.infoLog.Printf("UpdatePersonalLessonApplyToGroup invalid param obid=%s roomId=%v isAdd=%v",
			obid,
			roomId,
			isAdd)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("UpdatePersonalLessonApplyToGroup invalid obid=%s roomId=%v isAdd=%v",
			obid,
			roomId,
			isAdd)
		err = ErrInputParam
		return err
	}
	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(PersonalLessonTable)
	var storeIterm PersonalLessonStore
	err = mongoConn.Find(bson.M{"_id": bsObid}).One(&storeIterm)
	if err != nil {
		this.infoLog.Printf("UpdatePersonalLessonApplyToGroup obid=%s roomId=%v isAdd=%v err=%s", obid, roomId, isAdd, err)
		this.RefreshSession(err)
		err = mongoConn.Find(bson.M{"_id": bsObid}).One(&storeIterm)
		if err != nil {
			this.infoLog.Printf("UpdatePersonalLessonApplyToGroup retry obid=%s roomId=%v isAdd=%v err=%s", obid, roomId, isAdd, err)
			this.RefreshSession(err)
			return err
		}
	}
	if isAdd {
		for _, v := range storeIterm.ApplyRoomId {
			if v == roomId {
				this.infoLog.Printf("UpdatePersonalLessonApplyToGroup obid=%s roomId=%v isAdd=%v already apply", obid, roomId, isAdd)
				return nil
			}
		}
		storeIterm.ApplyRoomId = append(storeIterm.ApplyRoomId, roomId)
	} else {
		var newApplyRoomId []uint32
		for _, v := range storeIterm.ApplyRoomId {
			if v != roomId {
				newApplyRoomId = append(newApplyRoomId, v)
			}
		}
		storeIterm.ApplyRoomId = newApplyRoomId
	}

	err = mongoConn.Update(bson.M{"_id": bsObid}, bson.M{"$set": bson.M{"applyroomid": storeIterm.ApplyRoomId}})
	return err
}

func (this *DbUtil) DelPersonalLessonByObid(createrUid uint32, obid []byte) (err error) {
	if createrUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("DelPersonalLessonByObid invalid param createrUid=%v obid=%v", createrUid, obid)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("DelPersonalLessonByObid invalid obid createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return err
	}
	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(PersonalLessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid, "createruid": createrUid}, bson.M{"$set": bson.M{"lessonstat": uint32(1), "updatets": uint32(time.Now().Unix())}})
	//需要判断一下是否存在 todo
	if err != nil {
		this.infoLog.Printf("DelPersonalLessonByObid createrUid=%v obid=%s failed err=%s", createrUid, obid, err)
		this.RefreshSession(err)
		if err == mgo.ErrNotFound {
			this.infoLog.Printf("DelPersonalLessonByObid createrUid=%v obid=%s not found continue", createrUid, obid)
			return nil
		} else {
			return err
		}
	}
	return nil
}

func (this *DbUtil) DelGroupLessonByPersonalLessonObid(createrUid uint32, obid []byte) (err error) {
	if createrUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("DelGroupLessonByPersonalLessonObid invalid param createrUid=%v obid=%v", createrUid, obid)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("DelGroupLessonByPersonalLessonObid invalid obid createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return err
	}
	mongoConn := this.noSqlDb.DB(LessonDB).C(GroupLessonTable)
	err = mongoConn.Update(bson.M{"personallessonobid": string(obid), "createruid": createrUid}, bson.M{"$set": bson.M{"lessonstat": uint32(1), "updatets": uint32(time.Now().Unix())}})
	//需要判断一下是否存在 todo
	if err != nil {
		this.infoLog.Printf("DelGroupLessonByPersonalLessonObid createrUid=%v obid=%s failed err=%s", createrUid, obid, err)
		this.RefreshSession(err)
		if err == mgo.ErrNotFound {
			this.infoLog.Printf("DelGroupLessonByPersonalLessonObid createrUid=%v obid=%s not found continue", createrUid, obid)
			return nil
		} else {
			return err
		}
	}
	return nil
}

func (this *DbUtil) GetPersonalLessonByObid(createrUid uint32, cliObid []byte) (lessonList []PersonalLessonStore, maxObid []byte, err error) {
	if createrUid == 0 || len(cliObid) == 0 {
		this.infoLog.Printf("GetPersonalLessonByObid invalid param createrUid=%v cliObid=%v", createrUid, cliObid)
		err = ErrInputParam
		return lessonList, maxObid, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetPersonalLessonByObid invalid obid createrUid=%v obid=%s", createrUid, cliObid)
		err = ErrInputParam
		return lessonList, maxObid, err
	}
	bsObid := bson.ObjectIdHex(string(cliObid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(PersonalLessonTable)
	err = mongoConn.Find(bson.M{"createruid": createrUid, "lessonstat": 0, "_id": bson.M{"$lt": bsObid}}).Sort("-_id").Limit(pageSize).All(&lessonList)
	if err != nil {
		this.infoLog.Printf("GetPersonalLessonByObid createrUid=%v cliObid=%s err=%s", createrUid, cliObid, err)
		this.RefreshSession(err)
		return lessonList, maxObid, err
	}
	this.infoLog.Printf("GetPersonalLessonByObid createrUid=%v cliObid=%s lessonListLen=%v", createrUid, cliObid, len(lessonList))
	if len(lessonList) > 0 {
		maxObid = []byte(lessonList[len(lessonList)-1].Id.Hex())
	}
	return lessonList, maxObid, nil
}

func (this *DbUtil) StoreGroupLessonInfo(createUid, roomId uint32, personalLessonObid []byte, lessonBeginTime string) (obid string, err error) {
	if this.noSqlDb == nil {
		err = ErrNilDbObject
		return obid, err
	}

	if createUid == 0 ||
		roomId == 0 ||
		len(personalLessonObid) == 0 {
		this.infoLog.Printf("StoreGroupLessonInfo invalid param createUid=%v roomId=%v personalLessonObid=%s lessonBeginTime=%s",
			createUid,
			roomId,
			personalLessonObid,
			lessonBeginTime)
		err = ErrDbParam
		return obid, err
	}

	// 将教学房间信息写入Mongo中 首先获取集合对象
	bsObid := bson.NewObjectId()
	obid = bsObid.Hex()
	this.infoLog.Printf("StoreGroupLessonInfo obid=%v obid srting=%v",
		obid,
		bsObid.String(),
	)

	mongoConn := this.noSqlDb.DB(LessonDB).C(GroupLessonTable)
	lessonInfo := &GroupLessonStore{
		Id:                 bsObid,
		CreaterUid:         createUid,
		PersonalLessonObid: string(personalLessonObid),
		RoomId:             roomId,
		LessonTime:         lessonBeginTime,
		LessonStat:         0,
		UpdateTS:           uint32(time.Now().Unix()),
	}

	err = mongoConn.Insert(lessonInfo)
	if err != nil {
		this.RefreshSession(err)
		this.infoLog.Printf("StoreGroupLessonInfo Mongo Insert failed createUid=%v roomId=%v personalLessonObid=%s lessonBeginTime=%v err=%s",
			createUid,
			roomId,
			personalLessonObid,
			lessonBeginTime,
			err)
		return obid, err
	}

	// 更新完毕无需再次更新maxOrder了
	return obid, nil
}

func (this *DbUtil) UpdateGroupLessonInfo(obid []byte, createrUid, roomId uint32, lessonBeginTime string) (err error) {
	if len(obid) == 0 ||
		createrUid == 0 ||
		roomId == 0 {
		this.infoLog.Printf("UpdateGroupLessonInfo invalid param obid=%s createrUid=%v roomId=%v lessonBeginTime=%s",
			obid,
			createrUid,
			roomId,
			lessonBeginTime)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("UpdateGroupLessonInfo invalid obid=%s createrUid=%v roomId=%v lessonBeginTime=%s",
			obid,
			createrUid,
			roomId,
			lessonBeginTime)
		err = ErrInputParam
		return err
	}

	updateTime := uint32(time.Now().Unix())
	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(GroupLessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid, "roomid": roomId}, bson.M{"$set": bson.M{"lessontime": lessonBeginTime, "updatets": updateTime}})
	return err
}

func (this *DbUtil) DelGroupLessonByObid(obid []byte, createrUid, roomId uint32) (err error) {
	if createrUid == 0 ||
		roomId == 0 ||
		len(obid) == 0 {
		this.infoLog.Printf("DelGroupLessonByObid invalid param createrUid=%v roomId=%v obid=%s", createrUid, roomId, obid)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("DelGroupLessonByObid invalid obid createrUid=%v roomId=%v obid=%s", createrUid, roomId, obid)
		err = ErrInputParam
		return err
	}
	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(GroupLessonTable)
	err = mongoConn.Update(bson.M{"_id": bsObid, "createruid": createrUid, "roomid": roomId}, bson.M{"$set": bson.M{"lessonstat": uint32(1), "updatets": uint32(time.Now().Unix())}})
	//需要判断一下是否存在todo
	if err != nil {
		this.infoLog.Printf("DelGroupLessonByObid createrUid=%v obid=%s failed err=%s", createrUid, obid, err)
		this.RefreshSession(err)
		if err == mgo.ErrNotFound {
			this.infoLog.Printf("DelGroupLessonByObid createrUid=%v obid=%s not found continue", createrUid, obid)
			return nil
		} else {
			return err
		}
	}
	return nil
}

func (this *DbUtil) GetGroupLessonByObid(reqUid uint32, roomId uint32, cliObid []byte) (lessonList []GroupLessonStore, maxObid []byte, err error) {
	if reqUid == 0 || roomId == 0 || len(cliObid) == 0 {
		this.infoLog.Printf("GetGroupLessonByObid invalid param reqUid=%v roomId=%v cliObid=%v", reqUid, roomId, cliObid)
		err = ErrInputParam
		return lessonList, maxObid, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetGroupLessonByObid invalid obid reqUid=%v roomId=%v obid=%s", reqUid, roomId, cliObid)
		err = ErrInputParam
		return lessonList, maxObid, err
	}
	bsObid := bson.ObjectIdHex(string(cliObid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(GroupLessonTable)
	err = mongoConn.Find(bson.M{"lessonstat": 0, "roomid": roomId, "_id": bson.M{"$lt": bsObid}}).Sort("-_id").Limit(pageSize).All(&lessonList)
	if err != nil {
		this.infoLog.Printf("GetGroupLessonByObid reqUid=%v roomId=%v cliObid=%s err=%s", reqUid, roomId, cliObid, err)
		this.RefreshSession(err)
		return lessonList, maxObid, err
	}
	this.infoLog.Printf("GetGroupLessonByObid reqUid=%v roomId=%v cliObid=%s lessonListLen=%v", reqUid, roomId, cliObid, len(lessonList))
	if len(lessonList) > 0 {
		maxObid = []byte(lessonList[len(lessonList)-1].Id.Hex())
	}
	return lessonList, maxObid, nil
}

func (this *DbUtil) UpdateGroupLessonRecordStartTime(roomId, teacherUid uint32, obid []byte) (recordObid string, err error) {
	if roomId == 0 || teacherUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("UpdateGroupLessonRecordStartTime invalid param roomId=%v teacherUid=%v obid=%s",
			roomId,
			teacherUid,
			obid)
		err = ErrInputParam
		return recordObid, err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("UpdateGroupLessonRecordStartTime invalid obid roomId=%v teacherUid=%v obid=%s",
			roomId,
			teacherUid,
			obid)
		err = ErrInputParam
		return recordObid, err
	}

	// 将教学房间信息写入Mongo中 首先获取集合对象
	bsObid := bson.NewObjectId()
	recordObid = bsObid.Hex()
	this.infoLog.Printf("UpdateGroupLessonRecordStartTime roomId=%v teacherUid=%v groupLessonObid=%s recordObid=%s",
		roomId,
		teacherUid,
		obid,
		recordObid,
	)

	mongoConn := this.noSqlDb.DB(LessonDB).C(RecordTable)
	recordInfo := &RecordStore{
		Id:              bsObid,
		TeacherUid:      teacherUid,
		RoomId:          roomId,
		GroupLessonObid: string(obid),
		VoiceUrl:        "",
		PPTUrl:          "",
		StartTimeStamp:  uint64(time.Now().UnixNano() / 1000000),
		EndTimeStamp:    0,
		LessonDuration:  0,
		RecordStat:      0,
		UpdateTS:        uint32(time.Now().Unix()),
	}

	err = mongoConn.Insert(recordInfo)
	if err != nil {
		this.RefreshSession(err)
		this.infoLog.Printf("UpdateGroupLessonRecordStartTime Mongo Insert failed roomId=%v teacherUid=%v groupLessonObid=%s recordObid=%v err=%s",
			roomId,
			teacherUid,
			obid,
			recordObid,
			err)
		return recordObid, err
	}

	// 更新完毕无需再次更新maxOrder了
	return recordObid, err
}

func (this *DbUtil) UpdateGroupLessonRecordVoiceUrl(roomId, reqUid uint32, obid []byte, url string) (err error) {
	if roomId == 0 || reqUid == 0 || len(obid) == 0 || len(url) == 0 {
		this.infoLog.Printf("UpdateGroupLessonRecordVoiceUrl invalid param roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("UpdateGroupLessonRecordVoiceUrl invalid obid  roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}

	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(RecordTable)
	err = mongoConn.Update(bson.M{"_id": bsObid}, bson.M{"$set": bson.M{"voiceurl": url}})
	if err != nil {
		this.infoLog.Printf("UpdateGroupLessonRecordVoiceUrl roomId=%v reqUid=%v obid=%s url=%s failed refresh session",
			roomId,
			reqUid,
			obid,
			url)
		this.RefreshSession(err)
	}
	return err
}

func (this *DbUtil) UpdateGroupLessonRecordPPTUrl(roomId, reqUid uint32, obid []byte, url string, endTime uint64) (err error) {
	if roomId == 0 || reqUid == 0 || len(obid) == 0 || len(url) == 0 {
		this.infoLog.Printf("UpdateGroupLessonRecordPPTUrl invalid param roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("UpdateGroupLessonRecordPPTUrl invalid obid roomId=%v reqUid=%v obid=%s url=%s",
			roomId,
			reqUid,
			obid,
			url)
		err = ErrInputParam
		return err
	}

	bsObid := bson.ObjectIdHex(string(obid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(RecordTable)
	// Step1: 首先获取课程的开始时间
	var recordItem RecordStore
	err = mongoConn.Find(bson.M{"_id": bsObid}).One(&recordItem)
	if err != nil {
		this.infoLog.Printf("UpdateGroupLessonRecordPPTUrl reqUid=%v obid=%s err=%s", reqUid, obid, err)
		this.RefreshSession(err)
		return err
	}
	// Step2: 计算课程的结束时间
	duration := endTime - recordItem.StartTimeStamp
	// Step3: 更新db
	err = mongoConn.Update(bson.M{"_id": bsObid}, bson.M{"$set": bson.M{"ppturl": url, "endtimestamp": endTime, "lessonduration": duration}})
	if err != nil {
		this.infoLog.Printf("UpdateGroupLessonRecordPPTUrl roomId=%v reqUid=%v obid=%s url=%s failed refresh session",
			roomId,
			reqUid,
			obid,
			url)
		this.RefreshSession(err)
	}
	return err
}

func (this *DbUtil) GetGroupLessonByOneObid(roomId uint32, cliObid []byte) (lesson GroupLessonStore, err error) {
	if len(cliObid) == 0 {
		this.infoLog.Printf("GetGroupLessonByOneObid invalid param roomId=%v cliObid=%v", roomId, cliObid)
		err = ErrInputParam
		return lesson, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetGroupLessonByOneObid invalid obid roomId=%v obid=%s", roomId, cliObid)
		err = ErrInputParam
		return lesson, err
	}
	bsObid := bson.ObjectIdHex(string(cliObid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(GroupLessonTable)
	err = mongoConn.Find(bson.M{"lessonstat": 0, "_id": bsObid}).One(&lesson)
	if err != nil {
		this.infoLog.Printf("GetGroupLessonByOneObid roomId=%v cliObid=%s err=%s", roomId, cliObid, err)
		this.RefreshSession(err)
		return lesson, err
	}
	this.infoLog.Printf("GetGroupLessonByOneObid roomId=%v cliObid=%s lesson=%#v", roomId, cliObid, lesson)
	return lesson, nil
}

func (this *DbUtil) GetPersonalLessonByOneObid(cliObid []byte) (lesson PersonalLessonStore, err error) {
	if len(cliObid) == 0 {
		this.infoLog.Printf("GetPersonalLessonByOneObid invalid param cliObid=%v", cliObid)
		err = ErrInputParam
		return lesson, err
	}
	ret := bson.IsObjectIdHex(string(cliObid))
	if !ret {
		this.infoLog.Printf("GetPersonalLessonByOneObid invalid obid obid=%s", cliObid)
		err = ErrInputParam
		return lesson, err
	}
	bsObid := bson.ObjectIdHex(string(cliObid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(PersonalLessonTable)
	err = mongoConn.Find(bson.M{"lessonstat": 0, "_id": bsObid}).One(&lesson)
	if err != nil {
		this.infoLog.Printf("GetPersonalLessonByOneObid cliObid=%s err=%s", cliObid, err)
		this.RefreshSession(err)
		return lesson, err
	}
	this.infoLog.Printf("GetPersonalLessonByOneObid cliObid=%s lesson=%#v", cliObid, lesson)
	return lesson, nil
}

func (this *DbUtil) GetRecordListByGroupLessonObid(reqUid uint32, groupLessonObid []byte) (recordList []RecordStore, err error) {
	if reqUid == 0 || len(groupLessonObid) == 0 {
		this.infoLog.Printf("GetRecordListByGroupLessonObid invalid param reqUid=%v groupLessonObid=%v", reqUid, groupLessonObid)
		err = ErrInputParam
		return nil, err
	}
	ret := bson.IsObjectIdHex(string(groupLessonObid))
	if !ret {
		this.infoLog.Printf("GetRecordListByGroupLessonObid invalid obid reqUid=%v groupLessonObid=%s", reqUid, groupLessonObid)
		err = ErrInputParam
		return nil, err
	}

	mongoConn := this.noSqlDb.DB(LessonDB).C(RecordTable)
	err = mongoConn.Find(bson.M{"recordstat": 0, "grouplessonobid": string(groupLessonObid)}).Sort("-_id").All(&recordList)
	if err != nil {
		this.infoLog.Printf("GetRecordListByGroupLessonObid reqUid=%v groupLessonObid=%s err=%s", reqUid, groupLessonObid, err)
		this.RefreshSession(err)
		return nil, err
	}
	this.infoLog.Printf("GetRecordListByGroupLessonObid reqUid=%v groupLessonObid=%s recordLen=%v", reqUid, groupLessonObid, len(recordList))
	return recordList, nil
}

func (this *DbUtil) DelRecordByObid(createrUid uint32, recordObid []byte) (err error) {
	if createrUid == 0 ||
		len(recordObid) == 0 {
		this.infoLog.Printf("DelRecordByObid invalid param createrUid=%v recordObid=%s", createrUid, recordObid)
		err = ErrInputParam
		return err
	}
	ret := bson.IsObjectIdHex(string(recordObid))
	if !ret {
		this.infoLog.Printf("DelRecordByObid invalid obid createrUid=%v recordObid=%s", createrUid, recordObid)
		err = ErrInputParam
		return err
	}
	bsObid := bson.ObjectIdHex(string(recordObid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(RecordTable)
	var recordItem RecordStore
	err = mongoConn.Find(bson.M{"_id": bsObid}).One(&recordItem)
	if err != nil {
		this.infoLog.Printf("DelRecordByObid createrUid=%v recordObid=%s err=%s", createrUid, recordObid, err)
		this.RefreshSession(err)
		return err
	}

	groupLessonItem, err := this.GetGroupLessonByOneObid(recordItem.RoomId, []byte(recordItem.GroupLessonObid))
	if err != nil {
		return err
	}
	this.infoLog.Printf("GetRecordByObidFromDb groupLesson createrUid=%v reqUid=%v", groupLessonItem.CreaterUid, createrUid)
	if groupLessonItem.CreaterUid != createrUid {
		this.infoLog.Printf("GetRecordByObidFromDb groupLesson createrUid=%v reqUid=%v not equal", groupLessonItem.CreaterUid, createrUid)
		err = ErrPermissionDenied
		return err
	}

	err = mongoConn.Update(bson.M{"_id": bsObid}, bson.M{"$set": bson.M{"recordstat": uint32(1), "updatets": uint32(time.Now().Unix())}})
	//需要判断一下是否存在todo
	if err != nil {
		this.infoLog.Printf("DelRecordByObid createrUid=%v obid=%s failed err=%s", createrUid, recordObid, err)
		this.RefreshSession(err)
		if err == mgo.ErrNotFound {
			this.infoLog.Printf("DelRecordByObid createrUid=%v obid=%s not found continue", createrUid, recordObid)
			return nil
		} else {
			return err
		}
	}
	return nil
}

func (this *DbUtil) QueryRecordStatByObid(createrUid uint32, recordObid []byte) (isDeleted bool, err error) {
	if createrUid == 0 || len(recordObid) == 0 {
		this.infoLog.Printf("QueryRecordStatByObid invalid param createrUid=%v recordObid=%s", createrUid, recordObid)
		err = ErrInputParam
		return isDeleted, err
	}
	ret := bson.IsObjectIdHex(string(recordObid))
	if !ret {
		this.infoLog.Printf("QueryRecordStatByObid invalid obid createrUid=%v recordObid=%s", createrUid, recordObid)
		err = ErrInputParam
		return isDeleted, err
	}
	bsObid := bson.ObjectIdHex(string(recordObid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(RecordTable)
	var recordItem RecordStore
	err = mongoConn.Find(bson.M{"_id": bsObid}).One(&recordItem)
	if err != nil {
		this.infoLog.Printf("QueryRecordStatByObid createrUid=%v recordObid=%s err=%s", createrUid, recordObid, err)
		this.RefreshSession(err)
		return isDeleted, err
	}
	this.infoLog.Printf("QueryRecordStatByObid createrUid=%v recordObid=%s recordItem=%#v", createrUid, recordObid, recordItem)
	if recordItem.RecordStat > 0 {
		isDeleted = true
	} else {
		isDeleted = false
	}
	return isDeleted, nil
}

func (this *DbUtil) GetRecordByObidFromDb(createrUid uint32, recordObid []byte) (recordInfo *RecordStore, err error) {
	if createrUid == 0 || len(recordObid) == 0 {
		this.infoLog.Printf("GetRecordByObidFromDb invalid param createrUid=%v recordObid=%s", createrUid, recordObid)
		err = ErrInputParam
		return nil, err
	}
	ret := bson.IsObjectIdHex(string(recordObid))
	if !ret {
		this.infoLog.Printf("GetRecordByObidFromDb invalid obid createrUid=%v recordObid=%s", createrUid, recordObid)
		err = ErrInputParam
		return nil, err
	}
	bsObid := bson.ObjectIdHex(string(recordObid))
	mongoConn := this.noSqlDb.DB(LessonDB).C(RecordTable)
	var recordItem RecordStore
	err = mongoConn.Find(bson.M{"_id": bsObid, "recordstat": 0}).One(&recordItem)
	if err != nil {
		this.infoLog.Printf("GetRecordByObidFromDb createrUid=%v recordObid=%s err=%s", createrUid, recordObid, err)
		this.RefreshSession(err)
		return nil, err
	}
	this.infoLog.Printf("GetRecordByObidFromDb createrUid=%v recordObid=%s recordItem=%#v", createrUid, recordObid, recordItem)
	recordInfo = &recordItem
	return recordInfo, nil
}

func (this *DbUtil) DelGroupLessonByUid(createrUid, roomId uint32) (obidList []string, err error) {
	if createrUid == 0 ||
		roomId == 0 {
		this.infoLog.Printf("DelGroupLessonByUid invalid param createrUid=%v roomId=%v", createrUid, roomId)
		err = ErrInputParam
		return nil, err
	}
	mongoConn := this.noSqlDb.DB(LessonDB).C(GroupLessonTable)
	var lessonList []GroupLessonStore
	err = mongoConn.Find(bson.M{"lessonstat": 0, "roomid": roomId, "createruid": createrUid}).Sort("-_id").All(&lessonList)
	if err != nil {
		this.infoLog.Printf("DelGroupLessonByUid createrUid=%v roomId=%v get obid list err=%s", createrUid, roomId, err)
		this.RefreshSession(err)
		return nil, err
	}
	for _, v := range lessonList {
		obid := v.PersonalLessonObid
		obidList = append(obidList, obid)
		err = mongoConn.Update(bson.M{"_id": v.Id, "roomid": roomId}, bson.M{"$set": bson.M{"lessonstat": uint32(1), "updatets": uint32(time.Now().Unix())}})
		//需要判断一下是否存在todo
		if err != nil {
			this.infoLog.Printf("DelGroupLessonByUid createrUid=%v roomId=%v obid=%s update failed err=%s", createrUid, roomId, obid, err)
			this.RefreshSession(err)
		}
	}

	return obidList, nil
}

// 通过PersonLessonObid 查询个人课程运用到的没有删除的群聊
func (this *DbUtil) GetGroupLessonSlicByPersonalLessonObid(createrUid uint32, obid []byte) (groupLessonSlic []GroupLessonItem, err error) {
	if createrUid == 0 || len(obid) == 0 {
		this.infoLog.Printf("GetGroupLessonSlicByPersonalLessonObid invalid param createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return groupLessonSlic, err
	}
	ret := bson.IsObjectIdHex(string(obid))
	if !ret {
		this.infoLog.Printf("GetGroupLessonSlicByPersonalLessonObid invalid obid createrUid=%v obid=%s", createrUid, obid)
		err = ErrInputParam
		return groupLessonSlic, err
	}
	var lessonList []GroupLessonStore
	mongoConn := this.noSqlDb.DB(LessonDB).C(GroupLessonTable)
	err = mongoConn.Find(bson.M{"createruid": createrUid, "lessonstat": 0, "personallessonobid": string(obid)}).All(&lessonList)
	if err != nil {
		this.infoLog.Printf("GetGroupLessonSlicByPersonalLessonObid createrUid=%v obid=%s err=%s", createrUid, obid, err)
		this.RefreshSession(err)
		return groupLessonSlic, err
	}
	this.infoLog.Printf("GetGroupLessonSlicByPersonalLessonObid createrUid=%v obid=%s lessonListLen=%v", createrUid, obid, len(lessonList))
	for _, v := range lessonList {
		item := GroupLessonItem{
			GroupLessonObid: v.Id.Hex(),
			RoomId:          v.RoomId,
		}
		groupLessonSlic = append(groupLessonSlic, item)
	}
	return groupLessonSlic, nil
}
