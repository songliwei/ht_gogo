package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_group_lesson"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	Cmd int `short:"t" long:"cmd" description:"Command type" optional:"no"`
}

var options Options
var infoLog *log.Logger

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)
	//inviteUidCfg := cfg.Section("TEST_UID").Key("uid_slice").MustString("2325928")
	//uidSlic := strings.Split(inviteUidCfg, ",")
	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)
	defer conn.Close()

	v3Protocol := &common.HeadV3Protocol{}
	var head *common.HeadV3
	head = &common.HeadV3{Flag: 0xF0,
		Version:  4,
		CryKey:   0,
		TermType: 0,
		Cmd:      0x00,
		Seq:      0x1000,
		From:     2325928,
		To:       0,
		Len:      0,
	}

	var payLoad []byte
	reqBody := new(ht_group_lesson.GroupLessonReqBody)
	switch options.Cmd {
	// 1.创建个人课程
	case 0x7361:
		head.Cmd = 0x7361
		subReqBody := &ht_group_lesson.CreatePersonalLessonReqBody{
			LessonItem: &ht_group_lesson.PersonalLessonItem{
				PersonalObid:        nil,
				CreaterUid:          proto.Uint32(2325928),
				LessonTitle:         []byte("HelloTalk Lesson Test"),
				LessonAbstract:      []byte("lesson abstract"),
				TeacherAbstract:     []byte("teacher abstract"),
				LessonCoursewareUrl: []byte("www.hellotalk.com"),
				RoomIdList:          nil,
			},
		}
		reqBody.CreatePersonalLessonReqbody = subReqBody

	// 2.更新个人课程
	case 0x7363:
		head.Cmd = 0x7363
		subReqBody := &ht_group_lesson.UpdatePersonalLessonReqBody{
			ReqUid: proto.Uint32(2325928),
			LessonItem: &ht_group_lesson.PersonalLessonItem{
				PersonalObid:        []byte("5a39be4b372cbf61cba7cb2e"),
				CreaterUid:          proto.Uint32(2325928),
				LessonTitle:         []byte("lesson title"),
				LessonAbstract:      []byte("lesson abstract"),
				TeacherAbstract:     []byte("teacher abstract"),
				LessonCoursewareUrl: []byte("lesson courseware url"),
				RoomIdList:          nil,
			},
		}
		reqBody.UpdatePersonalLessonReqbody = subReqBody
	// 3.删除个人课程请求
	case 0x7365:
		head.Cmd = 0x7365
		subReqBody := &ht_group_lesson.DelPersonalLessonReqBody{
			ReqUid: proto.Uint32(2325928),
			Obid:   []byte("5a39be4b372cbf61cba7cb2e"),
		}
		reqBody.DelPersonalLessonReqbody = subReqBody
	// 4.分批获取课程列表请求
	case 0x7367:
		head.Cmd = 0x7367
		subReqBody := &ht_group_lesson.BatchGetPersonalLessonReqBody{
			ReqUid:  proto.Uint32(2325928),
			CliObid: []byte("0"),
		}
		reqBody.BatchGetPersonalLessonReqbody = subReqBody
	// 5.创建群组课程
	case 0x7369:
		head.Cmd = 0x7369
		subReqBody := &ht_group_lesson.CreateGroupLessonReqBody{
			GroupLesson: &ht_group_lesson.GroupLessonItem{
				GroupLessonObid:    nil,
				CreateUid:          proto.Uint32(2325928),
				RoomId:             proto.Uint32(17999),
				PersonalLessonObid: []byte("5a39b6cc372cbf61cba7cb2a"),
				LessonTime:         []byte("2017-12-17"),
				RecordUrlList:      nil,
			},
		}
		reqBody.CreateGroupLessonReqbody = subReqBody
	case 0x736B:
		head.Cmd = 0x736B
		subReqBody := &ht_group_lesson.UpdateGroupLessonReqBody{
			GroupLesson: &ht_group_lesson.GroupLessonItem{
				GroupLessonObid:    []byte("5a3a0d46372cbf56c69bc1c7"),
				CreateUid:          proto.Uint32(2325928),
				RoomId:             proto.Uint32(17999),
				PersonalLessonObid: []byte("5a39bd64372cbf61cba7cb2c"),
				LessonTime:         []byte("2017-12-18"),
				RecordUrlList:      nil},
			PersonalLesson: &ht_group_lesson.PersonalLessonItem{
				PersonalObid:        []byte("5a39bd64372cbf61cba7cb2c"),
				CreaterUid:          proto.Uint32(2325928),
				LessonTitle:         []byte("new lesson title"),
				LessonAbstract:      []byte("new lesson abstract"),
				TeacherAbstract:     []byte("new teacher abstract"),
				LessonCoursewareUrl: []byte("new lesson courseware url"),
				RoomIdList:          nil,
			},
		}
		reqBody.UpdateGroupLessonReqbody = subReqBody
	case 0x736D:
		head.Cmd = 0x736D
		subReqBody := &ht_group_lesson.DelGroupLessonReqBody{
			CreateUid:       proto.Uint32(2325928),
			RoomId:          proto.Uint32(17999),
			GroupLessonObid: []byte("5a3a0d46372cbf56c69bc1c7"),
		}
		reqBody.DelGroupLessonReqbody = subReqBody
	case 0x736F:
		head.Cmd = 0x736F
		subReqBody := &ht_group_lesson.BatchGetGroupLessonReqBody{
			ReqUid:  proto.Uint32(2325928),
			RoomId:  proto.Uint32(17999),
			CliObid: []byte("0"),
		}
		reqBody.BatchGetGroupLessonReqbody = subReqBody
	case 0x7371:
		head.Cmd = 0x7371
		subReqBody := &ht_group_lesson.BeginGroupLessonReqBody{
			TeacherUid:          proto.Uint32(2325928),
			TeacherName:         []byte("songliwei"),
			RoomId:              proto.Uint32(17999),
			ChannelId:           []byte("channel_id"),
			GroupLessonObid:     []byte(""),
			FirstUrl:            []byte(""),
			LessonTitle:         []byte(""),
			LessonAbstract:      []byte(""),
			TeacherAbstract:     []byte(""),
			LessonCoursewareUrl: []byte(""),
		}
		reqBody.BeginGroupLessonReqbody = subReqBody
	case 0x7373:
		head.Cmd = 0x7373
		subReqBody := &ht_group_lesson.EndGroupLessonReqBody{
			TeacherUid:      proto.Uint32(2325928),
			TeacherName:     []byte("songliwei"),
			RoomId:          proto.Uint32(17999),
			GroupLessonObid: []byte(""),
			ChannelId:       []byte("channel_id"),
		}
		reqBody.EndGroupLessonReqbody = subReqBody
	case 0x7375:
		head.Cmd = 0x7375
		subReqBody := &ht_group_lesson.JoinGroupLessonReqBody{
			RoomId:    proto.Uint32(17999),
			ReqUid:    proto.Uint32(1946612),
			ChannelId: []byte("channel_id"),
		}
		reqBody.JoinGroupLessonReqbody = subReqBody
	case 0x7377:
		head.Cmd = 0x7377
		subReqBody := &ht_group_lesson.GroupLessonHeartBeatReqBody{
			RoomId: proto.Uint32(17999),
			ReqUid: proto.Uint32(1946612),
		}
		reqBody.GroupLessonHeartBeatReqbody = subReqBody
	case 0x7379:
		head.Cmd = 0x7379
		subReqBody := &ht_group_lesson.LeaveGroupLessonReqBody{
			RoomId:    proto.Uint32(17999),
			ReqUid:    proto.Uint32(1946612),
			ChannelId: []byte("channel_id"),
		}
		reqBody.LeaveGroupLessonReqbody = subReqBody
	case 0x737B:
		head.Cmd = 0x737B
		subReqBody := &ht_group_lesson.GroupLessonMessageReqBody{
			RoomId:    proto.Uint32(17999),
			SenderUid: proto.Uint32(2325928),
			Messgae:   []byte("helloTalk"),
		}
		reqBody.GroupLessonMessageReqbody = subReqBody
	case 0x737D:
		head.Cmd = 0x737D
		subReqBody := &ht_group_lesson.ChangeCoursewareReqBody{
			RoomId:          proto.Uint32(17999),
			OpUid:           proto.Uint32(2325928),
			GroupLessonObid: []byte(""),
			CoursewareUrl:   []byte(""),
		}
		reqBody.ChangeCoursewareReqbody = subReqBody

	case 0x737F:
		head.Cmd = 0x737F
		subReqBody := &ht_group_lesson.UpdateGroupLessonRecordUrlReqBody{
			RoomId:          proto.Uint32(17999),
			GroupLessonObid: []byte(""),
			RecordUrl:       []byte(""),
		}
		reqBody.UpdateGroupLessonRecordReqbody = subReqBody

	case 0x7381:
		head.Cmd = 0x7381
		subReqBody := &ht_group_lesson.GetLessonByObidReqBody{
			ReqUid:              proto.Uint32(2325928),
			RoomId:              proto.Uint32(17999),
			GroupLessonObidList: [][]byte{[]byte("xxxx")},
		}
		reqBody.GetLessonByObidReqbody = subReqBody

	case 0x7383:
		head.Cmd = 0x7383
		subReqBody := &ht_group_lesson.GetLatestGroupLessonMsgReqBody{
			RoomId:     proto.Uint32(17999),
			ReqUid:     proto.Uint32(2325928),
			RecordObid: []byte(""),
			CliSeq:     proto.Uint64(0),
		}
		reqBody.GetLatestGroupLessonMsgReqbody = subReqBody
	default:
		infoLog.Println("UnKnow input cmd =", options.Cmd)
	}

	payLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return
	}

	head.Len = uint32(common.PacketV3HeadLen + len(payLoad) + 1) //整个报文长度
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV3ToSlice failed")
		return
	}
	copy(buf[common.PacketV3HeadLen:], payLoad) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	infoLog.Printf("len=%v payLaod=%v\n", len(payLoad), payLoad)
	// write
	conn.Write(buf)
	// read
	p, err := v3Protocol.ReadPacket(conn)
	if err == nil {
		rspPacket := p.(*common.HeadV3Packet)
		rspHead, _ := rspPacket.GetHead()
		rspPayLoad := rspPacket.GetBody()
		infoLog.Printf("resp len=%v cmd=%v\n", rspHead.Len, rspHead.Cmd)
		rspBody := &ht_group_lesson.GroupLessonRspBody{}
		err = proto.Unmarshal(rspPayLoad, rspBody)
		if err != nil {
			infoLog.Println("proto Unmarshal failed")
			return
		}
		switch rspHead.Cmd {
		case 0x7361:
			subRspBody := rspBody.GetCreatePersonalLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetCreatePersonalLessonRspbody rsp code=%v msg=%s obid=%s",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetObid())
		case 0x7363:
			subRspBody := rspBody.GetUpdatePersonalLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetUpdatePersonalLessonRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())
		case 0x7365:
			subRspBody := rspBody.GetDelPersonalLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetDelPersonalLessonRspbody code=%v reason=%s",
				status.GetCode(),
				status.GetReason())
		case 0x7367:
			subRspBody := rspBody.GetBatchGetPersonalLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetBatchGetPersonalLessonRspbody rsp code=%v msg=%s lessonList=%v",
				status.GetCode(),
				status.GetReason(),
				len(subRspBody.GetLessonList()))
			for i, v := range subRspBody.GetLessonList() {
				infoLog.Printf("index=%v obid=%s createrUid=%v title=%s abstract=%s teacherAbstract=%s url=%s roomid=%v",
					i,
					v.GetPersonalObid(),
					v.GetCreaterUid(),
					v.GetLessonTitle(),
					v.GetLessonAbstract(),
					v.GetTeacherAbstract(),
					v.GetLessonCoursewareUrl(),
					v.GetRoomIdList())
			}
		case 0x7369:
			subRspBody := rspBody.GetCreateGroupLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetCreateGroupLessonRspbody rsp code=%v msg=%s obid=%s",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetGroupLessonObid())

		case 0x736B:
			subRspBody := rspBody.GetUpdateGroupLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateGroupLessonRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 0x736D:
			subRspBody := rspBody.GetDelGroupLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetDelGroupLessonRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 0x736F:
			subRspBody := rspBody.GetBatchGetGroupLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("BatchGetGroupLessonReqBody rsp code=%v msg=%s maxObid=%s groupLessonLen=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetMaxObid(),
				len(subRspBody.GetLessonList()))
			for i, v := range subRspBody.GetLessonList() {
				infoLog.Printf("BatchGetGroupLessonReqBody index=%v gobid=%s createrUid=%v roomId=%v personalObid=%s lessontime=%s recordLen=%v",
					i,
					v.GetGroupLessonObid(),
					v.GetCreateUid(),
					v.GetRoomId(),
					v.GetPersonalLessonObid(),
					v.GetLessonTime(),
					len(v.GetRecordUrlList()))
				for _, j := range v.GetRecordUrlList() {
					infoLog.Printf("BatchGetGroupLessonReqBody recordObid=%s voiceUrl=%s recordUrl=%s timestamp=%v",
						j.GetRecordObid(),
						j.GetVoiceUrl(),
						j.GetPptUrl(),
						j.GetBeginTimeStamp())
				}
			}
		case 0x7371:
			subRspBody := rspBody.GetBeginGroupLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetBeginGroupLessonRspbody rsp code=%v msg=%s lessonBeginTime=%v recordObid=%s",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetLessonBeginTime(),
				subRspBody.GetRecordObid())

		case 0x7373:
			subRspBody := rspBody.GetEndGroupLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetEndGroupLessonRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())

		case 0x7375:
			subRspBody := rspBody.GetJoinGroupLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetJoinGroupLessonRspbody rsp code=%v msg=%s alreadyInList=%v ppturl=%s beginTime=%v currentSystime=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetAlreadyInList(),
				subRspBody.GetCurrentPptUrl(),
				subRspBody.GetBeginTimeStamp(),
				subRspBody.GetCurrentSystemTime())

		case 0x7377:
			subRspBody := rspBody.GetGroupLessonHeartBeatRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetGroupLessonHeartBeatRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 0x7379:
			subRspBody := rspBody.GetLeaveGroupLessonRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetLeaveGroupLessonRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 0x737B:
			subRspBody := rspBody.GetGroupLessonMessageRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetGroupLessonMessageRspbody rsp code=%v msg=%s msgId=%s",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetMsgId())
		case 0x737D:
			subRspBody := rspBody.GetChangeCoursewareRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("ChangeCoursewareRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 0x737F:
			subRspBody := rspBody.GetUpdateGroupLessonRecordRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateGroupLessonRecordRspbody rsp code=%v msg=%s",
				status.GetCode(),
				status.GetReason())
		case 0x7381:
			subRspBody := rspBody.GetGetLessonByObidRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("GetGetLessonByObidRspbody rsp code=%v msg=%s lessonListLen=%v",
				status.GetCode(),
				status.GetReason(),
				len(subRspBody.GetLessonInfoList()))
			for i, v := range subRspBody.GetLessonInfoList() {
				groupLessonItem := v.GetGroupLesson()
				personalLessonItme := v.GetPersonalLesson()
				infoLog.Printf("GetGetLessonByObidRspbody index=%v isInGroup=%v GroupLessonObid=%s createUid=%v roomId=%v personalObid=%s lessonTims=%s recordListLen=%v",
					i,
					groupLessonItem.GetGroupLessonObid(),
					groupLessonItem.GetCreateUid(),
					groupLessonItem.GetRoomId(),
					groupLessonItem.GetPersonalLessonObid(),
					groupLessonItem.GetLessonTime(),
					len(groupLessonItem.GetRecordUrlList()))
				infoLog.Printf("GetGetLessonByObidRspbody index=%v personalObid=%s createrUid=%v lessonTitle=%s lessonAbstract=%s teacherAbstract=%s url=%s roomIdList=%v",
					i,
					personalLessonItme.GetPersonalObid(),
					personalLessonItme.GetCreaterUid(),
					personalLessonItme.GetLessonTitle(),
					personalLessonItme.GetLessonAbstract(),
					personalLessonItme.GetTeacherAbstract(),
					personalLessonItme.GetLessonCoursewareUrl(),
					personalLessonItme.GetRoomIdList())
			}
		case 0x7383:
			subRspBody := rspBody.GetGetLatestGroupLessonMsgRspbody()
			status := subRspBody.GetStatus()
			infoLog.Printf("UpdateGroupLessonRecordRspbody rsp code=%v msg=%s curMaxSeq=%v msgListLen=%v",
				status.GetCode(),
				status.GetReason(),
				subRspBody.GetCurMaxSeq(),
				subRspBody.GetMsgList())
		default:
			infoLog.Println("UnKnow resp cmd =", rspHead.Cmd)
		}

	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
