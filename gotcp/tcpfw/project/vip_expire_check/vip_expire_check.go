package main

import (
	"database/sql"
	"fmt"

	simplejson "github.com/bitly/go-simplejson"
	"github.com/golang/protobuf/proto"
	"github.com/streadway/amqp"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_user"
	"github.com/HT_GOGO/gotcp/tcpfw/project/vip_expire_check/util"

	"log"
	"os"
	"runtime"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog          *log.Logger
	DbUtil           *util.DbUtil
	mcApi            *common.MemcacheApi
	userCacheApi     *common.UserCacheApi
	userInfoCacheApi *common.UserInfoCacheApi
	rabbitMQUrl      string
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)

const (
	CheckBeginTimestamp = 1473696000 // 2016-09-13 00:00:00 → 1473696000 以09-13为起始时间戳
	Client_Version_220  = 131584
)

const (
	NOTIFY_LANG_MODIFY_TYPE = "lang_modify"
	RABBIT_PROFILE          = "profile_update"
	EXCHANGENAME            = "push-msg"
	ROUTINGKEY              = "push-r-1"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func NotifyLanguageModify(uid uint32, termianlType uint8, obj *simplejson.Json) (err error) {
	// build notification
	infoObj := simplejson.New()
	infoObj.Set("type", NOTIFY_LANG_MODIFY_TYPE)
	infoObj.Set("data", obj)

	rootObject := BuildCommonObject(RABBIT_PROFILE, uid, termianlType)
	rootObject.Set("info", infoObj)
	strSlice, err := rootObject.MarshalJSON()
	if err != nil {
		infoLog.Printf("NotifyCorrectSentence simpleJson MarshalJSON failed uid=%v terminaltype=%v err=%s",
			uid,
			termianlType,
			err)
		return err
	}
	infoLog.Printf("NotifyCorrectSentence publish slice=%s", strSlice)
	err = RabbitMQPublish(rabbitMQUrl, EXCHANGENAME, "direct", ROUTINGKEY, string(strSlice), true)
	if err != nil {
		infoLog.Printf("NotifyCorrectSentence publish strSlice=%s failed", strSlice)
	} else {
		infoLog.Printf("NotifyCorrectSentence publish strSlice=%s success", strSlice)
	}
	return err
}

func BuildCommonObject(cmd string, uid uint32, terminalType uint8) (rootObject *simplejson.Json) {
	rootObject = simplejson.New()
	rootObject.Set("cmd", cmd)
	rootObject.Set("user_id", uid)
	rootObject.Set("os_type", terminalType)
	rootObject.Set("server_ts", time.Now().Unix())
	return rootObject
}

func RabbitMQPublish(amqpURI, exchange, exchangeType, routingKey, body string, reliable bool) error {

	// This function dials, connects, declares, publishes, and tears down,
	// all in one go. In a real service, you probably want to maintain a
	// long-lived connection as state, and publish against that.

	// infoLog.Printf("dialing %q", amqpURI)
	connection, err := amqp.Dial(amqpURI)
	if err != nil {
		return fmt.Errorf("Dial: %s", err)
	}
	defer connection.Close()

	channel, err := connection.Channel()
	if err != nil {
		return fmt.Errorf("Channel: %s", err)
	}

	//Printf("got Channel, declaring %q Exchange (%q)", exchangeType, exchange)
	if err := channel.ExchangeDeclare(
		exchange,     // name
		exchangeType, // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return fmt.Errorf("Exchange Declare: %s", err)
	}

	// Reliable publisher confirms require confirm.select support from the
	// connection.
	if reliable {
		infoLog.Printf("enabling publishing confirms.")
		if err := channel.Confirm(false); err != nil {
			return fmt.Errorf("Channel could not be put into confirm mode: %s", err)
		}

		confirms := channel.NotifyPublish(make(chan amqp.Confirmation, 1))

		defer confirmOne(confirms)
	}

	infoLog.Printf("declared Exchange, publishing %dB body (%q)", len(body), body)
	if err = channel.Publish(
		exchange,   // publish to an exchange
		routingKey, // routing to 0 or more queues
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "text/plain",
			ContentEncoding: "",
			Body:            []byte(body),
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
			// a bunch of application/implementation-specific fields
		},
	); err != nil {
		return fmt.Errorf("Exchange Publish: %s", err)
	}

	return nil
}

// One would typically keep a channel of publishings, a sequence number, and a
// set of unacknowledged sequence numbers and loop until the publishing channel
// is closed.
func confirmOne(confirms <-chan amqp.Confirmation) {
	infoLog.Printf("waiting for confirmation of one publishing")

	if confirmed := <-confirms; confirmed.Ack {
		infoLog.Printf("confirmed delivery with delivery tag: %d", confirmed.DeliveryTag)
	} else {
		infoLog.Printf("failed delivery of delivery tag: %d", confirmed.DeliveryTag)
	}
}

// step7: 通知usercache清除用户缓存
func SendPacketToUserCache(userId uint32) (err error) {
	head := &common.HeadV2{
		Version:  common.CVerMmedia,
		Cmd:      uint32(ht_user.ProtoCmd_CMD_MOD_USERINFO),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      userId,
	}

	userCacheReqBody := new(ht_user.ReqBody)
	userCacheReqBody.User = []*ht_user.UserInfoBody{&ht_user.UserInfoBody{
		UserID: proto.Uint32(userId),
	},
	}

	payLoad, err := proto.Marshal(userCacheReqBody)
	if err != nil {
		infoLog.Printf("SendPacketToUserCache proto marshal user=%v err=%s",
			userId,
			err)
		return err
	}
	ret, err := userCacheApi.SendPacket(head, payLoad)
	if err != nil {
		attr := "govipcheck/update_user_cache_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("SendPacketToUserCache userCacheApi.SendPacket failed userId=%v err=%s", userId, err)
		return err
	}
	infoLog.Printf("SendPacketToUserCache userCacheApi.SendPacket ret=%v", ret)
	return nil
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	// init dbUtil
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	DbUtil = util.NewDbUtil(db, infoLog)

	// 读取 Memcache Ip and port
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustInt(11211)
	infoLog.Printf("memcache ip=%v port=%v", mcIp, mcPort)
	mcApi = new(common.MemcacheApi)
	mcApi.Init(mcIp + ":" + strconv.Itoa(mcPort))

	// 读取user cache 配置
	userIp := cfg.Section("USERCACHE").Key("user_cache_ip").MustString("127.0.0.1")
	userPort := cfg.Section("USERCACHE").Key("user_cache_port").MustString("26000")
	userCacheConnLimit := cfg.Section("USERCACHE").Key("pool_limit").MustInt(1000)
	infoLog.Printf("user cache server ip=%v port=%v connLimit=%v", userIp, userPort, userCacheConnLimit)
	userCacheApi = common.NewUserCacheApi(userIp, userPort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, userCacheConnLimit)

	// 读取user info cache 配置
	userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	userInfoCacheConnLimit := cfg.Section("USER_INFO_CACHE").Key("pool_limit").MustInt(1000)
	infoLog.Printf("user info cache server ip=%v port=%v connLimit=%v", userInfoIp, userInfoPort, userInfoCacheConnLimit)
	userInfoCacheApi = common.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, userInfoCacheConnLimit)

	// 读取RabbitMQ的配置
	rabbitMQUrl = cfg.Section("RABBITMQ").Key("mq_url").MustString("amqp://pushServer:123456@10.243.134.240:5672/pushHost")
	infoLog.Printf("RabbitMQ URL=%s", rabbitMQUrl)
	// loop forever
	beginTimeStamp := int64(CheckBeginTimestamp)
	for {
		// sleep for 300 second
		time.Sleep(300 * time.Second)

		timeStampNow := time.Now().Unix()
		infoLog.Printf("beginTimeStamp=%v timeStampNow=%v", beginTimeStamp, timeStampNow)
		exipreList, err := DbUtil.GetExpiredVipUserList(beginTimeStamp, timeStampNow)
		if err != nil {
			// 统计总的请求量
			attr := "govipcheck/db_return_err"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("DbUtil.GetExpiredVipUserList beginTimeStamp=%v timeStampNow=%v err=%s",
				beginTimeStamp,
				timeStampNow,
				err)
			continue
		}
		for i, v := range exipreList {
			infoLog.Printf("DbUtil.GetExpiredVipUserList OK index=%v uid=%v", i, v)
			onlineStat, err := mcApi.GetUserOnlineStat(v)
			if err != nil {
				attr := "govipcheck/mc_return_err"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("mcApi.GetUserOnlineStat uid=%v failed err=%s", v, err)
				continue
			}

			if onlineStat.Version < Client_Version_220 {
				infoLog.Printf("onlineStat.Version=%v < 2.2.0 continue", onlineStat.Version)
				continue
			}

			langInfo, err := userInfoCacheApi.GetUserLanguageSetting(v)
			if err != nil {
				attr := "govipcheck/db_return_err"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("userInfoCacheApi.GetUserLanguageSetting uid=%v failed err=%s",
					v,
					err)
				continue
			}

			if langInfo.TeachLang2 > 0 || langInfo.TeachLang3 > 0 || langInfo.LearnLang2 > 0 ||
				langInfo.LearnLang3 > 0 || langInfo.LearnLang4 > 0 {
				infoLog.Printf("trace vip_exp_lang_change uid=%v langInfo=%#v", v, langInfo)
				// 重置VIP过期用户的语言设置为基本的一门学和教
				langInfo.TeachLang2 = 0
				langInfo.TeachLevel2 = 0
				langInfo.TeachLang3 = 0
				langInfo.TeachLevel3 = 0

				langInfo.LearnLang2 = 0
				langInfo.LearnLevel2 = 0
				langInfo.LearnLang3 = 0
				langInfo.LearnLevel3 = 0
				langInfo.LearnLang4 = 0
				langInfo.LearnLevel4 = 0
				err = userInfoCacheApi.UpdateUserLanguageSetting(v, langInfo)
				if err != nil {
					attr := "govipcheck/update_user_lang_err"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("userInfoCacheApi.UpdateUserLanguageSetting uid=%v langInfo=%#v failed err=%s",
						v,
						langInfo,
						err)
					continue
				}
				infoLog.Printf("userInfoCacheApi.UpdateUserLanguageSetting uid=%v ok", v)
				err = SendPacketToUserCache(v)
				if err != nil {
					attr := "govipcheck/update_user_cache_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("SendPacketToUserCache uid=%v langInfo=%#v failed err=%s",
						v,
						err)
					continue
				}

				// notify rabbitmq
				objLangModify := simplejson.New()
				objData := simplejson.New()
				objData.Set("language", 0)
				objData.Set("level", 0)
				objLangModify.Set("lang_learn_2", objData)
				objLangModify.Set("lang_learn_3", objData)
				objLangModify.Set("lang_teach_2", objData)
				objLangModify.Set("lang_teach_3", objData)

				err = NotifyLanguageModify(v, onlineStat.ClientType, objLangModify)
				if err != nil {
					infoLog.Printf("NotifyLanguageModify failed uid=%v err=%s", v, err)
					continue
				}
			} else {
				infoLog.Printf("do nothing:  uid=%v native=%v lang1=%v lang2=%v lang3=%v lang4=%v tech2=%v tech3=%v",
					v,
					langInfo.NativeLang,
					langInfo.LearnLang1,
					langInfo.LearnLang2,
					langInfo.LearnLang3,
					langInfo.LearnLang4,
					langInfo.TeachLang2,
					langInfo.TeachLang3)
			}
		}
		beginTimeStamp = timeStampNow // 重置需要检查的VIP过期时间戳
	}
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
