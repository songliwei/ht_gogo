// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

// Error type
var (
	ErrNilDbObject = errors.New("not set  object current is nil")
	ErrDbParam     = errors.New("err param error")
)

type TagLanguageInfo struct {
	NativeLang      uint8
	LearnLang1      uint8
	LearnLevel1     uint8
	LearnLang2      uint8
	LearnLevel2     uint8
	LearnLang3      uint8
	LearnLevel3     uint8
	LearnLang4      uint8
	LearnLevel4     uint8
	LearnAllowCount uint8 //购买的语言数
	TeachLang2      uint8
	TeachLevel2     uint8
	TeachLang3      uint8
	TeachLevel3     uint8
}

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

func (this *DbUtil) GetExpiredVipUserList(timeStart, timeEnd int64) (expireList []uint32, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return nil, err
	}

	if timeEnd == 0 {
		err = ErrDbParam
		return nil, err
	}

	rows, err := this.db.Query("SELECT USERID FROM HT_PURCHASE_TRANSLATE WHERE EXPIRETIME>? and EXPIRETIME<?;",
		timeStart,
		timeEnd)

	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var userId uint32
		if err := rows.Scan(&userId); err != nil {
			this.infoLog.Printf("GetExpiredVipUserList rows.Scan failed")
			continue
		}
		expireList = append(expireList, userId)
	}
	return expireList, nil
}

func (this *DbUtil) GetUserLanguageSetting(uid uint32) (langInfo *TagLanguageInfo, err error) {
	if this.db == nil {
		err = ErrNilDbObject
		return nil, err
	}

	if uid == 0 {
		err = ErrDbParam
		return nil, err
	}

	var storeNativeLang, storeLearnLang1, storeLearnLevel1, storeLearnLang2, storeLearnLevel2, storeLearnLang3, storeLearnLevel3 sql.NullInt64
	var storeLearnLang4, storeLearnLevel4, storeLearnAllowCount, storeTeachLang2, storeTeachLangLevel2, storeTeachLang3, storeTeachLangLevel3 sql.NullInt64
	err = this.db.QueryRow("SELECT NATIVELANG,LEARNLANG1,SKILLLEVEL1,LEARNLANG2,SKILLLEVEL2,LEARNLANG3,SKILLLEVEL3,LEARNLANG4,SKILLLEVEL4,ALLOWCOUNT,TEACHLANG2,TEACHSKILLLEVEL2,TEACHLANG3,TEACHSKILLLEVEL3 FROM HT_USER_LANGUAGE WHERE USERID=?;",
		uid).Scan(&storeNativeLang, &storeLearnLang1, &storeLearnLevel1, &storeLearnLang2, &storeLearnLevel2,
		&storeLearnLang3, &storeLearnLevel3, &storeLearnLang4, &storeLearnLevel4, &storeLearnAllowCount,
		&storeTeachLang2, &storeTeachLangLevel2, &storeTeachLang3, &storeTeachLangLevel3)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetUserLanguageSetting not found uid=%v in HT_USER_LANGUAGE err=%s", uid, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetUserLanguageSetting exec HT_USER_LANGUAGE failed uid=%v, err=%s", uid, err)
		return nil, err
	default:
	}

	var nativeLang, learnLang1, learnLevel1, learnLang2, learnLevel2, learnLang3, learnLevel3 uint8
	var learnLang4, learnLevel4, learnAllowCount, teachLang2, teachLangLevel2, teachLang3, teachLangLevel3 uint8
	if storeNativeLang.Valid {
		nativeLang = uint8(storeNativeLang.Int64)
	}

	if storeLearnLang1.Valid {
		learnLang1 = uint8(storeLearnLang1.Int64)
	}

	if storeLearnLevel1.Valid {
		learnLevel1 = uint8(storeLearnLevel1.Int64)
	}

	if storeLearnLang2.Valid {
		learnLang2 = uint8(storeLearnLang2.Int64)
	}

	if storeLearnLevel2.Valid {
		learnLevel2 = uint8(storeLearnLevel2.Int64)
	}

	if storeLearnLang3.Valid {
		learnLang3 = uint8(storeLearnLang3.Int64)
	}

	if storeLearnLevel3.Valid {
		learnLevel3 = uint8(storeLearnLevel3.Int64)
	}

	if storeLearnLang4.Valid {
		learnLang4 = uint8(storeLearnLang4.Int64)
	}

	if storeLearnLevel4.Valid {
		learnLevel4 = uint8(storeLearnLevel4.Int64)
	}

	if storeLearnAllowCount.Valid {
		learnAllowCount = uint8(storeLearnAllowCount.Int64)
	}

	if storeTeachLang2.Valid {
		teachLang2 = uint8(storeTeachLang2.Int64)
	}

	if storeTeachLangLevel2.Valid {
		teachLangLevel2 = uint8(storeTeachLangLevel2.Int64)
	}

	if storeTeachLang3.Valid {
		teachLang3 = uint8(storeTeachLang3.Int64)
	}

	if storeTeachLangLevel3.Valid {
		teachLangLevel3 = uint8(storeTeachLangLevel3.Int64)
	}

	langInfo = &TagLanguageInfo{
		NativeLang:      nativeLang,
		LearnLang1:      learnLang1,
		LearnLevel1:     learnLevel1,
		LearnLang2:      learnLang2,
		LearnLevel2:     learnLevel2,
		LearnLang3:      learnLang3,
		LearnLevel3:     learnLevel3,
		LearnLang4:      learnLang4,
		LearnLevel4:     learnLevel4,
		LearnAllowCount: learnAllowCount,
		TeachLang2:      teachLang2,
		TeachLevel2:     teachLangLevel2,
		TeachLang3:      teachLang3,
		TeachLevel3:     teachLangLevel3,
	}

	return langInfo, nil
}

func (this *DbUtil) UpdateUserLanguageSetting(uid uint32, langInfo *TagLanguageInfo) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	if uid == 0 {
		return ErrDbParam
	}

	_, err = this.db.Exec("UPDATE HT_USER_LANGUAGE SET NATIVELANG=?, LEARNLANG1=?, SKILLLEVEL1=?, LEARNLANG2=?, SKILLLEVEL2=?, LEARNLANG3=?, SKILLLEVEL3=?, LEARNLANG4=?, SKILLLEVEL4=?, TEACHLANG2=?, TEACHSKILLLEVEL2=?, TEACHLANG3=?, TEACHSKILLLEVEL3=?, UPDATETIME=UTC_TIMESTAMP() WHERE USERID =?;",
		langInfo.NativeLang,
		langInfo.LearnLang1,
		langInfo.LearnLevel1,
		langInfo.LearnLang2,
		langInfo.LearnLevel2,
		langInfo.LearnLang3,
		langInfo.LearnLevel3,
		langInfo.LearnLang4,
		langInfo.LearnLevel4,
		langInfo.TeachLang2,
		langInfo.TeachLevel2,
		langInfo.TeachLang3,
		langInfo.TeachLevel3,
		uid)
	if err != nil {
		this.infoLog.Printf("UpdateUserLanguageSetting insert faield uid=%v  err=%s", uid, err)
		return err
	} else {
		return nil
	}
}
