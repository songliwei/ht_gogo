package main

import (
	"fmt"

	"github.com/HT_GOGO/gotcp"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/seefan/gossdb"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/project/moment/util"
	mgo "gopkg.in/mgo.v2"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog   *log.Logger
	ssdbPool  *gossdb.Connectors
	mongoSess *mgo.Session
	DbUtil    *util.DbUtil
)

const (
	GO_MOMENT_STORE_MOMNET_REQ        = 101
	GO_MOMENT_MODIFY_MOMNET_REQ       = 102
	GO_MOMENT_GET_MOMNET_ID_LIST_REQ  = 103
	GO_MOMENT_GET_MID_SIMPLE_INFO_REQ = 104
)

const (
	GO_MOMENT_RET_SUCCESS          = 0 //成功
	GO_MOMENT_RET_PB_ERR           = 1 //pb序列化反序列化失败
	GO_MOMENT_RET_INTERNAL_ERR     = 2 //内部错误
	GO_MOMENT_RET_SESS_TIMEOUT_ERR = 3 //内部超时
	GO_MOMENT_RET_INPUT_PARAM_ERR  = 4 //输入参数不合法
	GO_MOMENT_RET_SSDB_ERR         = 5 //SSDB错误
	GO_MOMNET_RET_NO_AUTH          = 6 //没有权限

)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)
const (
	REPORT_THRESOLD  = 100
	TIME_ZONE_OFFSET = 8 * 3600
)

type MomentInfoStr struct {
	Mid       string
	LikeTs    uint32
	CommentTs uint32
	Level     uint32
	ShowTimes uint32
	ExpireTS  int64
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gomoment/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch int(head.Cmd) {
	case GO_MOMENT_STORE_MOMNET_REQ:
		go ProcStoreMoment(c, head, packet.GetBody())
	case GO_MOMENT_MODIFY_MOMNET_REQ:
		go ProcModifyMomentStatus(c, head, packet.GetBody())
	case GO_MOMENT_GET_MOMNET_ID_LIST_REQ:
		go ProcGetMomentIdList(c, head, packet.GetBody())
	case GO_MOMENT_GET_MID_SIMPLE_INFO_REQ:
		go ProcGetMidSimpleInfo(c, head, packet.GetBody())
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

func UidIsInSlice(uidList []uint32, uid uint32) bool {
	if uid == 0 || len(uidList) == 0 {
		return false
	}
	for _, v := range uidList {
		if uid == v {
			return true
		}
	}

	return false
}

// 1.proc store moment info
func ProcStoreMoment(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	result := uint16(GO_MOMENT_RET_SUCCESS)
	errMsg := "operation success"
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		bNeedCall = false
		infoLog.Printf("ProcStoreMoment invalid param")
		return false
	}

	// add static
	attr := "gomoment/store_moment_req_count"
	libcomm.AttrAdd(attr, 1)
	reqJson, err := simplejson.NewJson(payLoad)
	if err != nil {
		infoLog.Printf("ProcStoreMoment simplejson new packet error=%s", err)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "input param err"
		return false
	}

	infoLog.Printf("ProcStoreMoment req=%s", payLoad)
	nationLen := len(reqJson.Get("condition").Get("nationality").Get("national_array").MustArray())
	nationalSlice := make([]string, nationLen)
	for i := 0; i < nationLen; i++ {
		iterm := reqJson.Get("condition").Get("nationality").Get("national_array").GetIndex(i).MustString()
		nationalSlice[i] = iterm
	}

	learnLen := len(reqJson.Get("condition").Get("learn_language").Get("learn_array").MustArray())
	learnSlice := make([]uint32, learnLen)
	for i := 0; i < learnLen; i++ {
		iterm := uint32(reqJson.Get("condition").Get("learn_language").Get("learn_array").GetIndex(i).MustInt(0))
		learnSlice[i] = iterm
	}

	teachLen := len(reqJson.Get("condition").Get("teach_language").Get("teach_array").MustArray())
	teachSlice := make([]uint32, teachLen)
	for i := 0; i < teachLen; i++ {
		iterm := uint32(reqJson.Get("condition").Get("teach_language").Get("teach_array").GetIndex(i).MustInt(0))
		teachSlice[i] = iterm
	}

	placeLen := len(reqJson.Get("condition").Get("location").Get("place_array").MustArray())
	placeSlice := make([]string, placeLen)
	for i := 0; i < placeLen; i++ {
		countryName := reqJson.Get("condition").Get("location").Get("place_array").GetIndex(i).Get("country_name").MustString()
		admin := reqJson.Get("condition").Get("location").Get("place_array").GetIndex(i).Get("admin").MustString()
		locality := reqJson.Get("condition").Get("location").Get("place_array").GetIndex(i).Get("locality").MustString()
		iterm := fmt.Sprintf("%s-%s-%s", countryName, admin, locality)
		placeSlice[i] = iterm
	}

	// localityObj := simplejson.New()
	// localityObj.Set("type", "point")
	// // longAndLati := []float64{0, 0}
	// longAndLati := []float64{reqJson.Get("condition").Get("location").Get("long").MustFloat64(),
	// 	reqJson.Get("condition").Get("location").Get("lati").MustFloat64()}
	// localityObj.Set("coordinates", longAndLati)

	// localitySlice, err := localityObj.MarshalJSON()
	// if err != nil {
	// 	infoLog.Printf("ProcStoreMoment localityObj.MarshalJSON failed err=%s", err)
	// 	result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
	// 	errMsg = []byte("internal param err")
	// 	return false
	// }
	//
	localityObj := util.LocStore{
		Type: "Point",
		Coordinates: []float64{
			reqJson.Get("condition").Get("location").Get("long").MustFloat64(),
			reqJson.Get("condition").Get("location").Get("lati").MustFloat64(),
		},
	}

	classifyLen := len(reqJson.Get("show_classify").MustArray())
	showSlice := make([]uint32, classifyLen)
	for i := 0; i < classifyLen; i++ {
		iterm := uint32(reqJson.Get("show_classify").GetIndex(i).MustInt(0))
		showSlice[i] = iterm
	}

	momentStore := &util.MomentInfoStore{
		Mid:           reqJson.Get("mid").MustString(),
		NationalType:  uint32(reqJson.Get("condition").Get("nationality").Get("type").MustInt(0)), //默认展示给所有国家人看
		NationalArray: nationalSlice,
		LearnType:     uint32(reqJson.Get("condition").Get("learn_language").Get("type").MustInt(0)),
		LearnArray:    learnSlice,
		TeachType:     uint32(reqJson.Get("condition").Get("teach_language").Get("type").MustInt(0)),
		TeachArray:    teachSlice,
		RegisterType:  uint32(reqJson.Get("condition").Get("regiter_time").Get("type").MustInt(0)),
		RegBegin:      reqJson.Get("condition").Get("regiter_time").Get("begin").MustInt64(0),
		RegEnd:        reqJson.Get("condition").Get("regiter_time").Get("end").MustInt64(0),
		OnlineType:    uint32(reqJson.Get("condition").Get("online_time").Get("type").MustInt(0)),
		OnlineBegin:   reqJson.Get("condition").Get("online_time").Get("begin").MustInt64(0),
		OnlineEnd:     reqJson.Get("condition").Get("online_time").Get("end").MustInt64(0),
		LocalityType:  uint32(reqJson.Get("condition").Get("location").Get("type").MustInt(0)),
		PlaceArray:    placeSlice,
		Loc:           localityObj,
		AgeType:       uint32(reqJson.Get("condition").Get("age").Get("type").MustInt(0)),
		AgeBegin:      reqJson.Get("condition").Get("age").Get("begin").MustInt64(0),
		AgeEnd:        reqJson.Get("condition").Get("age").Get("end").MustInt64(0),
		Level:         uint32(reqJson.Get("level").MustInt(0)),
		ShowClassify:  showSlice,
		ShowTimes:     uint32(reqJson.Get("show_times").MustInt(1)),
		ActiveTime:    reqJson.Get("valid_time").Get("begin").MustInt64(0),
		ExpireTime:    reqJson.Get("valid_time").Get("end").MustInt64(0),
	}
	err = DbUtil.StoreMomentInfo(momentStore)
	if err != nil {
		infoLog.Printf("ProcStoreMoment DbUtil.StoreMomentInfo failed err=%s", err)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "internal err"
		return false
	} else {
		result = uint16(GO_MOMENT_RET_SUCCESS)
	}
	return true
}

func SendRetCode(c *gotcp.Conn, reqHead *common.HeadV2, ret uint16, errMsg string) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	rspObj := simplejson.New()
	status := simplejson.New()
	status.Set("code", ret)
	status.Set("reason", errMsg)
	rspObj.Set("status", status)
	s, err := rspObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("SendRetCode simpleJson.MarshalJSON failed uid=%v cmd=%v seq=%v ret=%v err=%s",
			head.Uid,
			head.Cmd,
			head.Seq,
			ret,
			errMsg)
		return false
	}

	infoLog.Printf("SendRetCode slice=%s", s)
	head.Len = uint32(common.PacketV2HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendRetCode SerialHeadV2ToSlice failed")
		return false
	}
	copy(buf[common.PacketV2HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV2MagicEnd

	rspPacket := common.NewHeadV2Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 2.proc modify moment status
func ProcModifyMomentStatus(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	result := uint16(GO_MOMENT_RET_SUCCESS)
	errMsg := "operation success"
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		bNeedCall = false
		infoLog.Printf("ProcModifyMomentStatus invalid param")
		return false
	}

	// add static
	attr := "gomoment/modify_moment_status_req_count"
	libcomm.AttrAdd(attr, 1)
	reqJson, err := simplejson.NewJson(payLoad)
	if err != nil {
		infoLog.Printf("ProcModifyMomentStatus simplejson new packet error=%s", err)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "input param err"
		return false
	}
	mid := reqJson.Get("mid").MustString()
	level := uint32(reqJson.Get("level").MustInt(0))
	infoLog.Printf("ProcModifyMomentStatus mid=%s level=%v", mid, level)
	err = DbUtil.ModifyMomentStatus(mid, level)
	if err != nil {
		infoLog.Printf("ProcModifyMomentStatus DbUtil.StoreMomentInfo failed err=%s", err)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "internal err"
		return false
	} else {
		result = uint16(GO_MOMENT_RET_SUCCESS)
	}
	return true
}

func ProcGetMomentIdList(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	timeBegin := time.Now().UnixNano()
	result := uint16(GO_MOMENT_RET_SUCCESS)
	errMsg := "operation success"
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		bNeedCall = false
		infoLog.Printf("ProcGetMomentIdList invalid param")
		return false
	}

	// add static
	attr := "gomoment/get_moment_id_list_req_count"
	libcomm.AttrAdd(attr, 1)
	reqJson, err := simplejson.NewJson(payLoad)
	if err != nil {
		infoLog.Printf("ProcGetMomentIdList simplejson new packet error=%s", err)
		result = uint16(GO_MOMENT_RET_INTERNAL_ERR)
		errMsg = "input param err"
		return false
	}
	//infoLog.Printf("DEBUG ProcGetMomentIdList reqJson=%s", payLoad)

	// lever_index
	levelLen := len(reqJson.Get("level_index").MustArray())
	levelIndexMap := map[uint32]uint32{}
	for i := 0; i < levelLen; i++ {
		level := uint32(reqJson.Get("level_index").GetIndex(i).Get("level").MustInt(0))
		levelIndexMap[level] = uint32(reqJson.Get("level_index").GetIndex(i).Get("index").MustInt64(0))
	}

	learnLen := len(reqJson.Get("learn_lang").MustArray())
	learnLang := make([]uint32, learnLen)
	for i := 0; i < learnLen; i++ {
		lang := uint32(reqJson.Get("learn_lang").GetIndex(i).MustInt(0))
		learnLang[i] = lang
	}

	teachLen := len(reqJson.Get("teach_lang").MustArray())
	teachLang := make([]uint32, teachLen)
	for i := 0; i < teachLen; i++ {
		lang := uint32(reqJson.Get("teach_lang").GetIndex(i).MustInt(0))
		teachLang[i] = lang
	}

	condition := &util.FeaturedCondition{
		LevelIndex:   levelIndexMap,
		Nationality:  reqJson.Get("nationality").MustString(),
		LearnLang:    learnLang,
		TeachLang:    teachLang,
		RegTimestamp: uint32(reqJson.Get("reg_time").MustInt64(0) + TIME_ZONE_OFFSET),
		Location: util.LocationInfo{
			Long:        reqJson.Get("location").Get("longitude").MustString(),
			Lati:        reqJson.Get("location").Get("latitude").MustString(),
			Country:     reqJson.Get("location").Get("country").MustString(),
			Admin1:      reqJson.Get("location").Get("admin1").MustString(),
			Admin2:      reqJson.Get("location").Get("admin2").MustString(),
			Admin3:      reqJson.Get("location").Get("admin3").MustString(),
			Locality:    reqJson.Get("location").Get("locality").MustString(),
			SubLocality: reqJson.Get("location").Get("sub_locality").MustString(),
		},
		Birthday:     reqJson.Get("birthday").MustString(),
		GetCount:     reqJson.Get("get_count").MustInt(),
		ShowClassify: uint32(reqJson.Get("show_classify").MustInt(0)),
	}
	// infoLog.Printf("ProcGetMomentIdList condition=%#v", condition)
	momentList, levelIndex, err := DbUtil.GetMomentList(condition)
	if err != nil {
		infoLog.Printf("ProcGetMomentIdList DbUtil.GetMomentList failed err=%s", err)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "internal err"
		return false
	}

	var indexSlice []*simplejson.Json
	for k, v := range levelIndex {
		itermObj := simplejson.New()
		itermObj.Set("level", k)
		itermObj.Set("index", v)
		indexSlice = append(indexSlice, itermObj)
	}

	var momentInfo []*simplejson.Json
	for i, v := range momentList {
		infoLog.Printf("ProcGetMomentIdList index=%v value=%v", i, v)
		likedTs, commentTs, deleted, _, err := DbUtil.GetMidInfo(v.Mid)
		if err != nil {
			infoLog.Printf("ProcGetMomentIdList DbUtil.GetMidInfo err=%s", err)
			continue
		}
		if deleted != uint32(util.OP_RESTORE) { //非正常同文不展示给用户看
			infoLog.Printf("ProcGetMomentIdList mid=%s deleted=%v likedTs=%v commentTs=%v",
				v.Mid,
				deleted,
				likedTs,
				commentTs)
			continue
		}

		itermObj := simplejson.New()
		itermObj.Set("mid", v.Mid)
		itermObj.Set("liked_ts", likedTs)
		itermObj.Set("comment_ts", commentTs)
		itermObj.Set("level", v.Level)
		itermObj.Set("show_times", v.ShowTimes)
		itermObj.Set("expire_ts", v.ExpireTS)
		momentInfo = append(momentInfo, itermObj)
	}

	statusObj := simplejson.New()
	statusObj.Set("code", GO_MOMENT_RET_SUCCESS)
	statusObj.Set("reason", "")

	rspObj := simplejson.New()
	rspObj.Set("status", statusObj)
	rspObj.Set("level_index", indexSlice)
	rspObj.Set("id_list", momentInfo)
	rspSlice, err := rspObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("ProcGetMomentIdList rspObj.MarshalJSON failed err=%s", err)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "internal err"
		return false
	}

	// infoLog.Printf("ProcGetMomentIdList from=%v id_list=%v rspSlice=%s", head.Uid, len(momentInfo), rspSlice)
	bRet := SendResp(c, head, rspSlice)
	bNeedCall = false // 已经回来响应无需再回响应
	if !bRet {
		infoLog.Printf("ProcGetMomentIdList send rsp failed")
	}
	timeEnd := time.Now().UnixNano()
	timeCost := (timeEnd - timeBegin) / 1000
	infoLog.Printf("ProcGetMomentIdList uid=%v cost=%v", head.Uid, timeCost)
	if timeCost > REPORT_THRESOLD*1000 {
		// add static
		attr := "gomoment/get_moment_id_list_proc_slow"
		libcomm.AttrAdd(attr, 1)

	}
	return true
}

func SendResp(c *gotcp.Conn, reqHead *common.HeadV2, rspSlice []byte) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}

	head.Len = uint32(common.PacketV2HeadLen + len(rspSlice) + 1) //整个报文长度
	head.Ret = uint16(GO_MOMENT_RET_SUCCESS)
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err := common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendResp SerialHeadV2ToSlice failed")
		return false
	}
	copy(buf[common.PacketV2HeadLen:], rspSlice) // return code
	buf[head.Len-1] = common.HTV2MagicEnd

	rspPacket := common.NewHeadV2Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func ProcGetMidSimpleInfo(c *gotcp.Conn, head *common.HeadV2, payLoad []byte) bool {
	result := uint16(GO_MOMENT_RET_SUCCESS)
	errMsg := "operation success"
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		bNeedCall = false
		infoLog.Printf("ProcGetMidSimpleInfo invalid param")
		return false
	}

	// add static
	attr := "gomoment/get_mid_simple_info_req_count"
	libcomm.AttrAdd(attr, 1)
	reqJson, err := simplejson.NewJson(payLoad)
	if err != nil {
		infoLog.Printf("ProcGetMidSimpleInfo simplejson new packet error=%s", err)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "input param err"
		return false
	}

	uid := reqJson.Get("userid").MustInt64(0)
	midCount := len(reqJson.Get("mid_array").MustArray())
	if uid == 0 || midCount == 0 {
		infoLog.Printf("ProcGetMidSimpleInfo input param error uid=%v midCount=%v", uid, midCount)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "input param err"
		return false
	}

	var midSimpleInfo []*simplejson.Json
	for i := 0; i < midCount; i++ {
		mid := reqJson.Get("mid_array").GetIndex(i).MustString()
		if len(mid) <= 0 {
			infoLog.Printf("ProcGetMidSimpleInfo mid=%s err", mid)
			continue
		}
		infoLog.Printf("ProcGetMidSimpleInfo index=%v mid=%s", i, mid)
		likedTs, commentTs, deleted, level, err := DbUtil.GetMidInfo(mid)
		if err != nil {
			infoLog.Printf("ProcGetMidSimpleInfo DbUtil.GetMidInfo err=%s", err)
			continue
		}

		itermObj := simplejson.New()
		itermObj.Set("mid", mid)
		itermObj.Set("liked_ts", likedTs)
		itermObj.Set("comment_ts", commentTs)
		itermObj.Set("deleted", deleted)
		itermObj.Set("featured_level", level)
		midSimpleInfo = append(midSimpleInfo, itermObj)
	}

	statusObj := simplejson.New()
	statusObj.Set("code", GO_MOMENT_RET_SUCCESS)
	statusObj.Set("reason", "")

	rspObj := simplejson.New()
	rspObj.Set("status", statusObj)
	rspObj.Set("mid_info_array", midSimpleInfo)

	rspSlice, err := rspObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("ProcGetMidSimpleInfo rspObj.MarshalJSON failed err=%s", err)
		result = uint16(GO_MOMENT_RET_INPUT_PARAM_ERR)
		errMsg = "internal err"
		return false
	}

	infoLog.Printf("ProcGetMidSimpleInfo rspSlice=%s", rspSlice)
	bRet := SendResp(c, head, rspSlice)
	bNeedCall = false
	if !bRet {
		infoLog.Printf("ProcGetMidSimpleInfo send rsp failed")
	}
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbIp := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	ssdbPool, err := gossdb.NewPool(&gossdb.Config{
		Host:             ssdbIp,
		Port:             ssdbPort,
		MinPoolSize:      5,
		MaxPoolSize:      50,
		AcquireIncrement: 5,
		MaxIdleTime:      120,
	})
	if err != nil {
		log.Fatalln("new ssdb pool failed err=", err)
		return
	}
	defer ssdbPool.Close()
	// init mongo
	// 创建mongodb对象
	mongo_url := cfg.Section("MONGO").Key("url").MustString("localhost")
	infoLog.Printf("Mongo url=%s", mongo_url)
	mongoSess, err = mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
		return
	}
	defer mongoSess.Close()
	// Optional. Switch the session to a monotonic behavior.
	mongoSess.SetMode(mgo.Monotonic, true)

	//创建RoomManager 和 DbUtil 对象
	DbUtil = util.NewDbUtil(ssdbPool, mongoSess, infoLog)
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
