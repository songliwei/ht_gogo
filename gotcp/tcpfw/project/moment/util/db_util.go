// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/seefan/gossdb"
	// "github.com/seefan/gossdb"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Error type
var (
	ErrNilNoSqlObject = errors.New("not set nosql object current is nil")
	ErrInputParam     = errors.New("input param error")
	ErrInternalErr    = errors.New("internal error")
)

var (
	MomentDB            = "moment"
	MomentInfoTable     = "moment_info"
	MomentMaxOrderTable = "moment_max_order"
	pageSize            = 20
)

const (
	MomentNormal = 0
	MomentDel    = 1
)

const (
	ToAll         = 0
	ToSpecificate = 1
)

const (
	MaxLevel      = 10
	FeaturedLevel = "featured_level"
)

type LocStore struct {
	Type        string
	Coordinates []float64
}

const (
	OP_RESTORE = 0
)

type MomentInfoStore struct {
	Id            string   `bson:"_id"` // 在Mongo中唯一标识一条记录
	Mid           string   // mid
	NationalType  uint32   // 0:给所有国籍人展示 1:展示给特定国籍国人看
	NationalArray []string // 两个字母国籍简写
	LearnType     uint32   // 0:给所有学习语言人展示 1:展示给特定学习语言的人看
	LearnArray    []uint32 // 学习语言简称slice
	TeachType     uint32   // 0:给所有教学语言人展示 1:展示给特定教学语言的人看
	TeachArray    []uint32 // 教学语言简称slice
	RegisterType  uint32   // 0:给所有注册的人展示 1:展示给特定注册时间的人看
	RegBegin      int64    // 注册起始时间
	RegEnd        int64    // 注册结束时间
	OnlineType    uint32   // 0:给所有在线的人展示 1:展示给特定在线时间的人看
	OnlineBegin   int64    // 在线起始时间
	OnlineEnd     int64    // 在线结束时间
	LocalityType  uint32   // 0:给所有地理位置的人展示 1:展示给特定国籍、第一级行政区、行政区的人看
	PlaceArray    []string // 地理位置 country-admin-locality
	//Locality      string   // {type:"point", coordinates:[-73.97, 40.77]} 序列化后的string
	Loc          LocStore // {type:"point", coordinates:[-73.97, 40.77]} 序列化后的string
	AgeType      uint32   // 0:给所有年龄的人展示 1:展示给特定年龄的人看
	AgeBegin     int64    // 年龄起始岁数
	AgeEnd       int64    // 年龄结束岁数
	Level        uint32   // 等级
	Order        uint64   // 序号
	ShowClassify []uint32 // 展示等级
	ShowTimes    uint32   // 展示次数
	Status       uint32   // 帖文状态 0：正常 1：删除
	ActiveTime   int64    // 生效时间
	ExpireTime   int64    // 过期时间
	UpdateTime   int64    // 更新时间
}

type MomentIdList struct {
	Mid       string
	Level     uint32 //
	ShowTimes uint32 //展示次数
	ExpireTS  int64  //过期时间
}

type LocationInfo struct {
	Long        string //精读
	Lati        string //纬度
	Country     string //国籍
	Admin1      string //一级行政区
	Admin2      string //二级行政区
	Admin3      string //三级行政区
	Locality    string //行政区
	SubLocality string //下一级行政区
}

type FeaturedCondition struct {
	LevelIndex   map[uint32]uint32 // 多个等级的获取情况
	Nationality  string            // 国籍
	LearnLang    []uint32          // 学习语言
	TeachLang    []uint32          // 教学语言
	RegTimestamp uint32            // 注册时间
	Location     LocationInfo      // 位置信息
	Birthday     string            // 生日
	GetCount     int               // 需要获取的个数
	ShowClassify uint32            // 请求展示分类
}

type MaxOrderInfo struct {
	Level    uint32 // 精选等级Id
	MaxOrder uint64 // 当前最大加入序号
}
type DbUtil struct {
	ssdbConnPool *gossdb.Connectors
	infoLog      *log.Logger
	noSqlDb      *mgo.Session
}

func NewDbUtil(ssdbPool *gossdb.Connectors, mongoSess *mgo.Session, logger *log.Logger) *DbUtil {
	return &DbUtil{
		ssdbConnPool: ssdbPool,
		noSqlDb:      mongoSess,
		infoLog:      logger,
	}
}

func (this *DbUtil) RefreshSession(err error) {
	attrName := "gomoment/mongodb_error_count"
	libcomm.AttrAdd(attrName, 1)
	this.noSqlDb.Refresh()
	this.infoLog.Printf("RefreshSession input err=%s", err)
}

func GetMomentInfoTable(level uint32) (table string) {
	table = fmt.Sprintf("%s_%v", MomentInfoTable, level)
	return table
}

func GetLearnBson(learnLang []uint32) (orCon []bson.M) {
	orCon = make([]bson.M, len(learnLang))
	for _, v := range learnLang {
		orCon = append(orCon, bson.M{"learnarray": v})
	}
	return orCon
}

func GetTeachBson(teachLang []uint32) (orCon []bson.M) {
	orCon = make([]bson.M, len(teachLang))
	for _, v := range teachLang {
		orCon = append(orCon, bson.M{"teacharray": v})
	}
	return orCon
}

func GetLocality(loc *LocationInfo) (strLocation string) {
	var admin string
	if len(loc.Admin1) != 0 {
		admin = loc.Admin1
	} else if len(loc.Admin2) != 0 {
		admin = loc.Admin2
	} else {
		admin = loc.Admin3 // admin 3 也可能为空
	}
	var locality string
	if len(loc.Locality) != 0 {
		locality = loc.Locality
	} else {
		locality = loc.SubLocality
	}

	strLocation = fmt.Sprintf("%s-%s-%s", loc.Country, admin, locality)
	return strLocation
}

func GetAge(birthday string) (age uint32) {
	strSlice := strings.Split(birthday, "-")
	if len(strSlice) < 3 {
		age = 0
		return age
	}

	year, _ := strconv.Atoi(strSlice[0])
	month, _ := strconv.Atoi(strSlice[1])
	day, _ := strconv.Atoi(strSlice[2])
	timeNow := time.Now()
	age = uint32(timeNow.Year() - year)
	if timeNow.Month() > time.Month(month) || timeNow.Month() == time.Month(month) && timeNow.Day() > day {
		age++
	}
	return age
}

func (this *DbUtil) StoreMomentInfo(moment *MomentInfoStore) (err error) {
	if this.noSqlDb == nil {
		err = ErrNilNoSqlObject
		return
	}

	if moment == nil {
		this.infoLog.Printf("StoreMomentInfo moment nil")
		err = ErrInputParam
		return err
	}

	this.infoLog.Printf("StoreMomentInfo moment=%#v", moment)

	// 获取maxOrder 集合
	maxOrderConn := this.noSqlDb.DB(MomentDB).C(MomentMaxOrderTable)

	// 每次将max order 递增1
	change := mgo.Change{
		Update:    bson.M{"$inc": bson.M{"maxorder": 1}},
		ReturnNew: true,
	}
	var maxOrderResult MaxOrderInfo
	_, err = maxOrderConn.Find(bson.M{"level": moment.Level}).Apply(change, &maxOrderResult)
	if err != nil {
		this.RefreshSession(err)
		this.infoLog.Printf("StoreMomentInfo moment=%#v inc maxorder failed err=%v",
			moment,
			err)
		return err
	}

	// 填充附加信息
	moment.Id = bson.NewObjectId().String()
	moment.Order = maxOrderResult.MaxOrder
	moment.UpdateTime = time.Now().Unix()
	moment.Status = 0

	tableName := GetMomentInfoTable(moment.Level)
	mongoConn := this.noSqlDb.DB(MomentDB).C(tableName)
	selector := bson.M{"mid": moment.Mid}

	data := bson.M{"$set": bson.M{"_id": moment.Id,
		"nationaltype":  moment.NationalType,
		"nationalarray": moment.NationalArray,
		"learntype":     moment.LearnType,
		"learnarray":    moment.LearnArray,
		"teachtype":     moment.TeachType,
		"teacharray":    moment.TeachArray,
		"registertype":  moment.RegisterType,
		"regbegin":      moment.RegBegin,
		"regend":        moment.RegEnd,
		"onlinetype":    moment.OnlineType,
		"onlinebegin":   moment.OnlineBegin,
		"onlineend":     moment.OnlineEnd,
		"localitytype":  moment.LocalityType,
		"placearray":    moment.PlaceArray,
		"loc":           moment.Loc,
		"agetype":       moment.AgeType,
		"agebegin":      moment.AgeBegin,
		"ageend":        moment.AgeEnd,
		"level":         moment.Level,
		"order":         moment.Order,
		"showclassify":  moment.ShowClassify,
		"showtimes":     moment.ShowTimes,
		"status":        moment.Status,
		"activetime":    moment.ActiveTime,
		"expiretime":    moment.ExpireTime,
		"updatetime":    moment.UpdateTime},
	}

	_, err = mongoConn.Upsert(selector, data)
	if err != nil {
		this.RefreshSession(err)
		this.infoLog.Printf("StoreMomentInfo moment=%#v Upsert failed err=%v",
			moment,
			err)
		return err
	}

	// 更新ssdb 设置当前帖子的level index
	if this.ssdbConnPool == nil {
		this.infoLog.Printf("StoreMomentInfo ssdbConnPool object is nil")
		err = ErrNilNoSqlObject
		return err
	}

	c, err := this.ssdbConnPool.NewClient()
	if err != nil {
		this.infoLog.Printf("StoreMomentInfo ssdbConnPool.NewClient failed")
		return err
	}
	defer c.Close()
	err = c.Hset(moment.Mid, FeaturedLevel, moment.Level)
	if err != nil {
		this.infoLog.Printf("StoreMomentInfo HSet failed mid=%s err=%s", moment.Mid, err)
		return err
	}

	// 更新完毕无需再次更新maxOrder了
	return nil
}

func (this *DbUtil) ModifyMomentStatus(mid string, level uint32) (err error) {
	if this.noSqlDb == nil {
		err = ErrNilNoSqlObject
		return
	}

	if len(mid) == 0 {
		this.infoLog.Printf("ModifyMomentStatus mid=%s in nil input error", mid)
		err = ErrInputParam
		return
	}

	tableName := GetMomentInfoTable(level)
	mongoConn := this.noSqlDb.DB(MomentDB).C(tableName)
	err = mongoConn.Remove(bson.M{"mid": mid})
	this.infoLog.Printf("ModifyMomentStatus mid=%s level=%v remove result=%s", mid, level, err)
	if err != nil {
		err = ErrInternalErr
		return err
	}
	// 更新ssdb 设置当前帖子的level index
	if this.ssdbConnPool == nil {
		this.infoLog.Printf("ModifyMomentStatus ssdbConnPool object is nil")
		err = ErrNilNoSqlObject
		return err
	}

	c, err := this.ssdbConnPool.NewClient()
	if err != nil {
		this.infoLog.Printf("ModifyMomentStatus ssdbConnPool.NewClient failed")
		return err
	}
	defer c.Close()
	err = c.Hset(mid, FeaturedLevel, 0) //featured_level 描述mid是否是精华，0 表示非精华，1-10 描述的是精华的等级
	if err != nil {
		this.infoLog.Printf("ModifyMomentStatus HSet failed mid=%s err=%s", mid, err)
		return err
	}
	return nil
}

func (this *DbUtil) GetMomentList(condition *FeaturedCondition) (momentList []*MomentIdList, levelIndex map[uint32]uint32, err error) {
	if this.noSqlDb == nil {
		this.infoLog.Printf("GetMomentList noSqlDb object is nil")
		err = ErrNilNoSqlObject
		return
	}

	levelIndex = map[uint32]uint32{}
	var count int = 0
	for i := MaxLevel; i > 0; i-- {
		var order uint32 = 0
		if v, ok := condition.LevelIndex[uint32(i)]; ok {
			order = v // 获取当前已经拉取到的最大orderid
		}
		var maxOrder uint32 = order
		tableName := GetMomentInfoTable(uint32(i))
		// this.infoLog.Printf("GetMomentList table=%s", tableName)
		mongoConn := this.noSqlDb.DB(MomentDB).C(tableName)
		onlineTs := time.Now().Unix()
		query := mongoConn.Find(bson.M{"status": uint32(MomentNormal),
			"order": bson.M{"$gt": order},
			"$or": []bson.M{bson.M{"nationaltype": uint32(ToAll)},
				bson.M{"nationalarray": condition.Nationality},
			},
			"activetime": bson.M{"$lte": onlineTs},
			"expiretime": bson.M{"$gte": onlineTs},
		})
		query.Sort("order") // 按照order的升序排

		var result []MomentInfoStore
		query.All(&result)
		for _, v := range result {
			// this.infoLog.Printf("GetMomentList index=%v moment=%#v", k, v)
			// 匹配学习语言
			learnMatch := false
			if v.LearnType == uint32(ToAll) {
				learnMatch = true
			} else {
				for _, lang := range condition.LearnLang {
					if U32IsInSlice(lang, v.LearnArray) {
						this.infoLog.Printf("GetMomentList learn lang=%v is in v.LearnType=%v v.LearnLang=%v", lang, v.LearnType, v.LearnArray)
						learnMatch = true
						break
					}
				}
			}

			if !learnMatch {
				this.infoLog.Printf("GetMomentList learn learnLang=%v is not match v.LearnType=%v v.LearnLang=%v", condition.LearnLang, v.LearnType, v.LearnArray)
				continue
			}

			// 匹配教学语言
			teachMatch := false
			if v.TeachType == ToAll {
				teachMatch = true
			} else {
				for _, lang := range condition.TeachLang {
					if U32IsInSlice(lang, v.TeachArray) {
						this.infoLog.Printf("GetMomentList teach lang=%v is in v.TeachType=%v v.TeachLang=%v", lang, v.TeachType, v.TeachArray)
						teachMatch = true
						break
					}
				}
			}

			if !teachMatch {
				this.infoLog.Printf("GetMomentList teach teachLang=%v is not match v.TeachLang=%v v.TeachLang=%v", condition.TeachLang, v.TeachType, v.TeachArray)
				continue
			}

			// 匹配注册时间
			if v.RegisterType != ToAll && (uint32(v.RegBegin) > condition.RegTimestamp || uint32(v.RegEnd) < condition.RegTimestamp) {
				this.infoLog.Printf("GetMomentList regtimestamp=%v is not match v.RegisterType=%v v.RegBegin=%v v.RegEnd=%v", condition.RegTimestamp, v.RegisterType, v.RegBegin, v.RegEnd)
				continue
			}

			// 匹配在线时间
			if v.OnlineType != ToAll && (v.OnlineBegin > onlineTs || v.OnlineEnd < onlineTs) {
				this.infoLog.Printf("GetMomentList onlinets=%v is not match v.OnlineType=%v v.TeachLang=%v", onlineTs, v.OnlineType, v.OnlineBegin, v.OnlineEnd)
				continue
			}

			// 匹配年龄
			if v.AgeType != ToAll && (uint32(v.AgeBegin) > GetAge(condition.Birthday) || uint32(v.AgeEnd) < GetAge(condition.Birthday)) {
				this.infoLog.Printf("GetMomentList birthday=%v calcAge=%v is not match v.AgeType=%v v.AgeBegin=%v v.AgeEnd=%v", condition.Birthday, GetAge(condition.Birthday), v.AgeType, v.AgeBegin, v.AgeEnd)
				continue
			}

			// 位置匹配
			clientLocation := GetLocality(&condition.Location)
			tempLocation := condition.Location
			tempLocation.Locality = ""
			tempLocation.SubLocality = ""
			clientLocationModify := GetLocality(&tempLocation)
			if v.LocalityType != ToAll && !(StringIsInSlice(clientLocation, v.PlaceArray) || StringIsInSlice(clientLocationModify, v.PlaceArray)) {
				this.infoLog.Printf("GetMomentList place=%s is not match v.LocalityType=%v v.PlaceArray=%v", GetLocality(&condition.Location), v.LocalityType, v.PlaceArray)
				continue
			}

			// 展示分类是否匹配
			classMatch := false
			if U32IsInSlice(condition.ShowClassify, v.ShowClassify) {
				this.infoLog.Printf("GetMomentList show class=%v is in v.ShowClassify=%v ", condition.ShowClassify, v.ShowClassify)
				classMatch = true
			}
			if !classMatch {
				this.infoLog.Printf("GetMomentList classify is not match condition.ShowClassify=%v v.ShowClassify=%v", condition.ShowClassify, v.ShowClassify)
				continue
			}

			// 被删除的帖文不再返回给客户端
			_, _, deleted, _, err := this.GetMidInfo(v.Mid)
			if err != nil {
				this.infoLog.Printf("GetMomentList DbUtil.GetMidInfo err=%s", err)
				continue
			}
			if deleted != uint32(OP_RESTORE) { //非正常同文不展示给用户看
				this.infoLog.Printf("GetMomentList mid=%s deleted=%v continue",
					v.Mid,
					deleted)
				continue
			}

			iterm := &MomentIdList{
				Mid:       v.Mid,
				Level:     v.Level,
				ShowTimes: v.ShowTimes,
				ExpireTS:  v.ExpireTime,
			}

			momentList = append(momentList, iterm)
			maxOrder = uint32(v.Order)
			count++
			if count > condition.GetCount {
				break
			}
		}

		// 添加每个队列获取到的最大序号
		levelIndex[uint32(i)] = maxOrder
		if count > condition.GetCount {
			this.infoLog.Printf("GetMomentList reach max count level=%v maxOrder=%v", i, maxOrder)
			break
		}
	}
	return momentList, levelIndex, nil
}

func U32IsInSlice(searchU32 uint32, targetSlice []uint32) bool {
	if len(targetSlice) == 0 {
		return false
	}
	for _, v := range targetSlice {
		if searchU32 == v {
			return true
		}
	}
	return false
}

func StringIsInSlice(searchStr string, targetSlice []string) bool {
	if len(targetSlice) == 0 {
		return false
	}
	for _, v := range targetSlice {
		if searchStr == v || strings.ToLower(searchStr) == strings.ToLower(v) {
			return true
		}
	}
	return false
}

func (this *DbUtil) GetMidInfo(mid string) (likedTs uint64, commentTs uint64, deleted uint32, level uint32, err error) {
	if this.ssdbConnPool == nil {
		this.infoLog.Printf("GetMidInfo ssdbConnPool object is nil")
		err = ErrNilNoSqlObject
		return 0, 0, 0, 0, err
	}

	if len(mid) == 0 {
		this.infoLog.Printf("GetMidInfo input param err")
		err = ErrInputParam
		return 0, 0, 0, 0, err
	}

	c, err := this.ssdbConnPool.NewClient()
	if err != nil {
		this.infoLog.Printf("GetMidInfo ssdbConnPool.NewClient failed")
		return 0, 0, 0, 0, err
	}
	defer c.Close()
	keyValue, err := c.MultiHget(mid, "liked_ts", "comment_ts", "deleted", FeaturedLevel)
	if err != nil {
		this.infoLog.Printf("GetMidInfo MultiHget failed err=%s", err)
		return 0, 0, 0, 0, err
	}
	likedTs = 0
	if v, ok := keyValue["liked_ts"]; ok {
		likedTs = v.UInt64()
	}

	commentTs = 0
	if v, ok := keyValue["comment_ts"]; ok {
		commentTs = v.UInt64()
	}

	deleted = 0
	if v, ok := keyValue["deleted"]; ok {
		deleted = v.UInt32()
	}

	level = 0
	if v, ok := keyValue[FeaturedLevel]; ok {
		level = v.UInt32()
	}

	this.infoLog.Printf("likedTs=%v commentTs=%v deleted=%v level=%v", likedTs, commentTs, deleted, level)
	return likedTs, commentTs, deleted, level, nil
}
