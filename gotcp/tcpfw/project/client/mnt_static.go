package main

import (
	// "github.com/bitly/go-simplejson"
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"github.com/seefan/gossdb"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()

	// init ssdb
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	pool, err := gossdb.NewPool(&gossdb.Config{
		Host:             ssdbHost,
		Port:             ssdbPort,
		MinPoolSize:      5,
		MaxPoolSize:      50,
		AcquireIncrement: 5,
	})
	if err != nil {
		infoLog.Printf("ssdb new pool failed err=%s", err)
		return
	}

	c, err := pool.NewClient()
	if err != nil {
		infoLog.Printf("new ssdb clinet failed err=%s", err)
		return
	}
	defer c.Close()
	// 遍历 tmp_user_momet_info 表读取每个uid 查询 ssdb 得到moment数和likecoutn
	beginUid := 10000
	//endUid := cfg.Section("ENDUID").Key("uid").MustInt()
	endUid := 6200000
	for beginUid < endUid {
		changUid := beginUid
		rows, err := db.Query("SELECT userid FROM tmp_user_momet_info WHERE userid > ? limit 1000", beginUid)
		if err != nil {
			infoLog.Printf("mysql get uid failed err=%s", err)
			return
		}
		defer rows.Close()
		for rows.Next() {
			var userId int
			if err := rows.Scan(&userId); err != nil {
				infoLog.Printf("mysql get uid rows.Scan failed err=%s", err)
				continue
			}
			// 修改uid
			changUid = userId
			hashName := fmt.Sprintf("%v_user_info", userId)
			infoLog.Printf("hashname=%s", hashName)
			val, err := c.MultiHget(hashName, "like_count", "moment_count")
			if err != nil {
				infoLog.Print("uid=%v ssdb get like_count moment_count failed", userId)
				continue
			}

			var likeCount uint64 = 0
			if v, ok := val["like_count"]; ok {
				likeCount = v.UInt64()
			}

			var mntCount uint64 = 0
			if v, ok := val["moment_count"]; ok {
				mntCount = v.UInt64()
			}
			infoLog.Printf("uid=%v like_count=%v moment_count=%v", userId, likeCount, mntCount)
			_, err = db.Exec("UPDATE tmp_user_momet_info SET mnt_count = ?, like_count=? WHERE userid=?",
				mntCount,
				likeCount,
				userId)

			if err != nil {
				infoLog.Printf("insert tmp_user_momet_info faield uid=%v mntCount=%v linkCount=%v err=%v",
					userId,
					mntCount,
					likeCount,
					err)
				continue
			}
		}
		// 更新uid
		if changUid >= beginUid+1000 {
			beginUid = changUid
		} else {
			beginUid += 1000
		}
	}

	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
