package main

import (
	// "github.com/bitly/go-simplejson"

	"errors"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/gansidui/gotcp/tcpfw/common"
	"github.com/gansidui/gotcp/tcpfw/include/ht_muc_store"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

const (
	HashKeyMsgBase     = 100000000000 //每个人的群消息队列是1000亿条
	HScanOperaterCount = 1000
)

var (
	ErrNilDbObject = errors.New("not set object current is nil")
	ErrLegthErr    = errors.New("length error")
)

var (
	ssdbMasterApi *common.SsdbApi
)

func HscanArray(setName string, keyStart, keyEnd string, limit int64, reverse bool) (keys []string, values []string, err error) {
	if ssdbMasterApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}

	keys, storeValues, err := ssdbMasterApi.HscanArray(setName, keyStart, keyEnd, limit, reverse)
	if err != nil {
		infoLog.Printf("HscanArray ssdbApi.HscanArray return err=%s setName=%s limit=%v",
			err,
			setName,
			limit)
		return nil, nil, err
	}

	values = make([]string, len(storeValues))
	for i, v := range storeValues {
		values[i] = v.String()
	}
	return keys, values, nil
}

func MultiHgetAll(setName string) (kvs []string, err error) {
	if ssdbMasterApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	keyStart := ""
	keyEnd := ""
	for {
		keys, values, err := HscanArray(setName, keyStart, keyEnd, HScanOperaterCount, false)
		if err != nil {
			infoLog.Printf("MultiHgetAll hashName=%s err=%s", setName, err)
			return nil, err
		}
		if len(keys) != len(values) {
			infoLog.Printf("MultiHgetAll outKeysLen=%v outValuesLen=%v not equal", len(keys), len(values))
			err = ErrLegthErr
			return nil, err
		}
		for i := 0; i < len(keys); i += 1 {
			kvs = append(kvs, keys[i])
			kvs = append(kvs, values[i])
			keyStart = keys[i]
		}
		// 如果帖子数为空 或者keys的个数小余1000 说明加载完成直接break
		if len(keys) < HScanOperaterCount {
			infoLog.Printf("MultiHgetAll hansName=%s reload over", setName)
			break
		}
	}
	return kvs, nil
}

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbMasterHost := cfg.Section("MASTERSSDB").Key("ip").MustString("127.0.0.1")
	ssdbMasterPort := cfg.Section("MASTERSSDB").Key("port").MustInt(8888)
	infoLog.Printf("ssdb master host=%s port=%v", ssdbMasterHost, ssdbMasterPort)
	ssdbMasterApi, err = common.NewSsdbApi(ssdbMasterHost, ssdbMasterPort, 5, 50)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	hashStr := cfg.Section("HASHLIST").Key("value").MustString("")
	hashSlic := strings.Split(hashStr, ",")
	for i, v := range hashSlic {
		infoLog.Printf("index=%v uid=%s", i, v)
		hashName := fmt.Sprintf("%s#gmsg", v)
		kvs, err := MultiHgetAll(hashName)
		if err != nil {
			infoLog.Printf("MultiHgetAll hashName=%s err=%s", hashName, err)
			return
		}
		for index := 0; index < len(kvs); index += 2 {
			key := kvs[index+1]
			value, err := ssdbMasterApi.Get(key)
			if err != nil || len(value) == 0 {
				infoLog.Printf("ssdbMasterApi.get key=%s failed err=%s valuelen=%v",
					key,
					err,
					len(value.String()))
				continue
			}
			valueSlic := value.Bytes()
			mucMsg := new(ht_muc_store.MucMsg)
			err = proto.Unmarshal(valueSlic, mucMsg)
			if err != nil {
				infoLog.Printf("proto Unmarshal failed key=%v value=%v", kvs[index], kvs[index+1])
				continue
			} else {
				infoLog.Printf("key=%v value=%s fromId=%v roomId=%v cmd=0x%4x format=%v content=%s msgTime=%v",
					kvs[index],
					kvs[index+1],
					mucMsg.GetFromId(),
					mucMsg.GetRoomId(),
					mucMsg.GetCmd(),
					mucMsg.GetFormat(),
					mucMsg.GetContent(),
					mucMsg.GetMsgTime())
			}
		}
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
