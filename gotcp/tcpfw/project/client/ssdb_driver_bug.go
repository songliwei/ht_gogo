package main

import (
	// "github.com/bitly/go-simplejson"

	"log"
	"os"
	"time"

	"github.com/gansidui/gotcp/tcpfw/common"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, 5, 50)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}
	loopCount := cfg.Section("TESTLOOP").Key("count").MustInt(8888)
	for i := 0; i < loopCount; i++ {
		value, err := ssdbApi.Hget("2325928#gmsg", "0")
		if err != nil {
			infoLog.Printf("ssdbApi.Hget 2325928#gmsg 0 failed err=%s", err)
		} else {
			infoLog.Printf("ssdbApi.Hget 2325928#gmsg 0 value=%s", value)
		}
		err = ssdbApi.Set("75149_1507864480375_2325928", "Hello World")
		if err != nil {
			infoLog.Printf("Set failed err=%s")
		}
		count, err := ssdbApi.Hincr("2325928#gmsg", "0", 1)
		if err != nil {
			infoLog.Printf("ssdbApi.Hinc failed err=%s", err)
		} else {
			infoLog.Printf("after Hinc count=%v", count)
		}
		if i%20 == 0 {
			time.Sleep(1 * time.Second)
		}
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
