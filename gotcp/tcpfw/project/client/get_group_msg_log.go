package main

import (
	// "github.com/bitly/go-simplejson"
	"database/sql"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mysql
	mysqlHost := cfg.Section("LOGMYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("LOGMYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("LOGMYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("LOGMYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("LOGMYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql log host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()
	roomId := cfg.Section("ROOMID").Key("roomid").MustInt(0)
	msgCount := cfg.Section("ROOMID").Key("msgcount").MustInt(10)
	// 遍历 tmp_user_momet_info 表读取每个uid 查询 ssdb 得到moment数和likecoutn
	rows, err := db.Query("SELECT FROMID, ROOMID, TYPE, CONTENT, TIME FROM LOG_MUC_MESSAGE WHERE ROOMID=? ORDER BY TIME DESC limit ?", roomId, msgCount)
	if err != nil {
		infoLog.Printf("mysql get muc message failed roomId=%v err=%s", roomId, err)
		return
	}
	defer rows.Close()
	index := 0
	for rows.Next() {
		var fromId, targetRoomId, msgType uint32
		var msgCnt, sendTime string
		if err := rows.Scan(&fromId, &targetRoomId, &msgType, &msgCnt, &sendTime); err != nil {
			infoLog.Printf("mysql get uid rows.Scan failed index=%v err=%s", index, err)
			continue
		}
		infoLog.Printf("index=%v fromId=%v roomId=%v msgType=%v msgCnt=%x sendTime=%s",
			index,
			fromId,
			targetRoomId,
			msgType,
			msgCnt,
			sendTime)
		index = index + 1
	}

	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
