package main

import (
	"errors"
	"fmt"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"log"
	"os"
	"runtime"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

var (
	ErrNilDbObject     = errors.New("not set  object current is nil")
	ErrDbParam         = errors.New("err param error")
	ErrNilNoSqlObject  = errors.New("not set nosql object current is nil")
	ErrMemberQuit      = errors.New("member quit state is not 0")
	ErrCloseExplicitly = errors.New("Closed explicitly")
)

var (
	infoLog   *log.Logger
	mongoSess *mgo.Session
)

const (
	favorDB          = "favorite"
	favorRecordTable = "favor_record"
)

type WordBody struct {
	Content     string
	Translate   string
	ContentTran string
}

type TextInfo struct {
	Content       string
	Translate     string
	ContentTran   string
	TranslateTran string
}

type VoiceInfo struct {
	Url         string
	Duration    uint32
	Size        uint32
	VoiceToText TextInfo
}

type ImageInfo struct {
	ThumbUrl string
	BigUrl   string
	Width    uint32
	Height   uint32
}

type SentenceCorrect struct {
	Source string
	Target string
}

type CorrectInfo struct {
	Comment string
	Body    []SentenceCorrect
}

type UrlInfo struct {
	Url         string
	ThumbUrl    string
	Description string
	Title       string
}

type MntInfo struct {
	Mid          string
	PictureCount uint32
	Url          UrlInfo
}

type FavorRecord struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"-"`
	ClientId   int64
	Uid        uint32
	Md5        string
	SrcUid     uint32
	Type       uint32
	Text       TextInfo
	Word       WordBody
	Voice      VoiceInfo
	Correction CorrectInfo
	Image      ImageInfo
	Mid        MntInfo
	CommentId  string
	Tags       []string
	FavTs      uint64
	FavStat    uint32
	FavVer     uint32
}

func UidIsInSlice(uidList []uint32, uid uint32) bool {
	if uid == 0 || len(uidList) == 0 {
		return false
	}
	for _, v := range uidList {
		if uid == v {
			return true
		}
	}

	return false
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func GetIndexTags(index int) (err error) {
	if mongoSess == nil {
		infoLog.Printf("GetIndexTags mongoSess object is nil")
		err = ErrNilNoSqlObject
		return err
	}
	//从Mongodb 中获取所有的群成员
	favTable := fmt.Sprintf("%s_%v", favorRecordTable, index)
	mongoConn := mongoSess.DB(favorDB).C(favTable)
	query := mongoConn.Find(bson.M{"tags": bson.M{"$size": 1}})
	var result []FavorRecord
	query.All(&result)
	for k, v := range result {
		infoLog.Printf("GetIndexTags index=%v tags=%v", k, v.Tags[0])
	}
	return nil
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// get config roomid
	endIndex := cfg.Section("INDEX").Key("end").MustInt(0)
	// init mongo
	// 创建mongodb对象
	mongo_url := cfg.Section("MONGO").Key("url").MustString("localhost")
	infoLog.Printf("Mongo url=%s", mongo_url)
	mongoSess, err = mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
		return
	}
	defer mongoSess.Close()
	// Optional. Switch the session to a monotonic behavior.
	mongoSess.SetMode(mgo.Monotonic, true)

	for index := 0; index < endIndex; index++ {
		infoLog.Printf("index=%v", index)
		err := GetIndexTags(index)
		if err != nil {
			infoLog.Printf("GetIndexTags failed err=%s", err)
			continue
		}
	}
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
