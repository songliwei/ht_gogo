package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gansidui/gotcp/tcpfw/common"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var redisApi *common.RedisApi
var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis pool
	redisIp := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis ip=%v port=%v", redisIp, redisPort)

	redisApi = common.NewRedisApi(fmt.Sprintf("%s:%v", redisIp, redisPort))
	uid := cfg.Section("TESTUID").Key("uid").MustInt(0)
	// Step1: clear uid#LIST mid
	hashName := fmt.Sprintf("%v#LIST", uid)
	keys, err := redisApi.Hkeys(hashName)
	if err != nil {
		infoLog.Printf("redisApi.Hkeys hashName=%s err=%s return", hashName, err)
		return
	}
	for _, v := range keys {
		infoLog.Printf("hashName=%s key=%s Hdel", hashName, v)
		_, err := redisApi.Hdel(hashName, v)
		if err != nil {
			infoLog.Printf("redisApi.Hdel hashName=%s key=%s err=%s", hashName, v, err)
		}
	}
	err = redisApi.Del(hashName)
	if err != nil {
		infoLog.Printf("redisApi.Del hashName=%s err=%s", hashName, err)
	} else {
		infoLog.Printf("redisApi.Del hashName=%s success", hashName)
	}

	// Step2: clear language hashmap
	hashName = "LANG#2"
	err = redisApi.Del(hashName)
	if err != nil {
		infoLog.Printf("redisApi.Del hashName=%s err=%s", hashName, err)
	} else {
		infoLog.Printf("redisApi.Del hashName=%s success", hashName)
	}

	hashName = "LANG#-5"
	err = redisApi.Del(hashName)
	if err != nil {
		infoLog.Printf("redisApi.Del hashName=%s err=%s", hashName, err)
	} else {
		infoLog.Printf("redisApi.Del hashName=%s success", hashName)
	}

	hashName = "LANG#2-5"
	err = redisApi.Del(hashName)
	if err != nil {
		infoLog.Printf("redisApi.Del hashName=%s err=%s", hashName, err)
	} else {
		infoLog.Printf("redisApi.Del hashName=%s success", hashName)
	}

	hashName = "MIDINDEX"
	keys, err = redisApi.Hkeys(hashName)
	if err != nil {
		infoLog.Printf("redisApi.Hkeys hashName=%s err=%s return", hashName, err)
		return
	}
	for _, v := range keys {
		infoLog.Printf("hashName=%s key=%s Hdel", hashName, v)
		value, err := redisApi.Hget(hashName, v)
		if err != nil {
			infoLog.Printf("hashName=%s key=%v redisApi.Hget err=%s",
				hashName,
				v,
				err)
			continue
		}
		err = redisApi.Del(value)
		if err != nil {
			infoLog.Printf("Del mid=%s err=%s", value, err)
		} else {
			infoLog.Printf("Del mid=%s success", value)
		}
		_, err = redisApi.Hdel(hashName, v)
		if err != nil {
			infoLog.Printf("redisApi.Hdel hashName=%s key=%s err=%s", hashName, v, err)
		}
	}

	err = redisApi.Del(hashName)
	if err != nil {
		infoLog.Printf("redisApi.Del hashName=%s err=%s", hashName, err)
	} else {
		infoLog.Printf("redisApi.Del hashName=%s success", hashName)
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
