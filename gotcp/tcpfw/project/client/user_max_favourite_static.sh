#!/bin/bash
PWD=`pwd`
FILE="$PWD/user_max_favourite.log"
TOTAL="$PWD/fav_total_static.csv"
TEXT="$PWD/fav_text_static.csv"
VOICE="$PWD/fav_voice_static.csv"
CORRECT="$PWD/fav_correct_static.csv"

echo "-----------------------------------------TOTAL------------------------------------------------------------------"
echo "---------------------------------------------------------------------------------------------------------------"
echo "UID,TEXT_TRANS,TEXT_NOTRANS,VOICE_TRANS,VOICE_NOTRANS,CORRECT_SEND,CORRECT_RECV,TOTAL_FAV,TOTAL_TRANS,TOTAL_NOT_TRANS,TOTAL_CORRECT" >> $TOTAL
#calc total
cat $FILE | awk '{print  $10" "$3}' | awk -F "[ =]" '{print $2 " " $4}' | sort -nr | head -10 | awk '{print $2}' > "$PWD/total.log"

while read line
do
#	cat $FILE | grep "\bUID=$line\b" | awk '{print $3" "$4" "$5" "$6" "$7" "$8" "$9 " "$10" "$11" "$12" "$13}'
	cat $FILE | grep "\bUID=$line\b" | awk '{print $3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13}' | awk -F "[=\t]" '{print $2","$4","$6","$8","$10","$12","$14","$16","$18","$20","$22}' >> $TOTAL
done < "$PWD/total.log"

echo "-----------------------------------------TEXT------------------------------------------------------------------"
echo "---------------------------------------------------------------------------------------------------------------"
#TEXT total
echo "UID,TEXT_TRANS,TEXT_NOTRANS,VOICE_TRANS,VOICE_NOTRANS,CORRECT_SEND,CORRECT_RECV,TOTAL_FAV,TOTAL_TRANS,TOTAL_NOT_TRANS,TOTAL_CORRECT" >> $TEXT
cat $FILE | awk '{print $4" "$5" "$3}' | awk -F "[ =]" '{print $2+$4 " " $6}'| sort -nr | head -10 | awk '{print $2}' > "$PWD/text_total.log"

while read line
do
#	cat $FILE | grep "\bUID=$line\b" | awk '{print $3" "$4" "$5" "$6" "$7" "$8" "$9 " "$10" "$11" "$12" "$13}'
	cat $FILE | grep "\bUID=$line\b" | awk '{print $3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13}' | awk -F "[=\t]" '{print $2","$4","$6","$8","$10","$12","$14","$16","$18","$20","$22}' >> $TEXT

done < "$PWD/text_total.log"

echo "------------------------------------------VOICE----------------------------------------------------------------"
echo "---------------------------------------------------------------------------------------------------------------"
#voice total
cat $FILE | awk '{print $6" "$7" "$3}' | awk -F "[ =]" '{print $2+$4 " " $6}'| sort -nr | head -10 | awk '{print $2}' > "$PWD/voice_total.log"

echo "UID,TEXT_TRANS,TEXT_NOTRANS,VOICE_TRANS,VOICE_NOTRANS,CORRECT_SEND,CORRECT_RECV,TOTAL_FAV,TOTAL_TRANS,TOTAL_NOT_TRANS,TOTAL_CORRECT" >> $VOICE
while read line
do
#	cat $FILE | grep "\bUID=$line\b" | awk '{print $3" "$4" "$5" "$6" "$7" "$8" "$9 " "$10" "$11" "$12" "$13}'
	cat $FILE | grep "\bUID=$line\b" | awk '{print $3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13}' | awk -F "[=\t]" '{print $2","$4","$6","$8","$10","$12","$14","$16","$18","$20","$22}' >> $VOICE

done < "$PWD/voice_total.log"

echo "------------------------------------------CORRECT--------------------------------------------------------------"
echo "---------------------------------------------------------------------------------------------------------------"
#correct total
cat $FILE | awk '{print $8" "$9" "$3}' | awk -F "[ =]" '{print $2+$4 " " $6}'| sort -nr | head -10 | awk '{print $2}' > "$PWD/correct_total.log"

echo "UID,TEXT_TRANS,TEXT_NOTRANS,VOICE_TRANS,VOICE_NOTRANS,CORRECT_SEND,CORRECT_RECV,TOTAL_FAV,TOTAL_TRANS,TOTAL_NOT_TRANS,TOTAL_CORRECT" >> $CORRECT
while read line
do
#	cat $FILE | grep "\bUID=$line\b" | awk '{print $3" "$4" "$5" "$6" "$7" "$8" "$9 " "$10" "$11" "$12" "$13}'
	cat $FILE | grep "\bUID=$line\b" | awk '{print $3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13}' | awk -F "[=\t]" '{print $2","$4","$6","$8","$10","$12","$14","$16","$18","$20","$22}' >> $CORRECT

done < "$PWD/correct_total.log"
