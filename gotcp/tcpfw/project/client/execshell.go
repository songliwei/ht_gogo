package main

import (
	"fmt"
	"os/exec"
	"time"
)

var pid int

func run() {
	cmd := exec.Command("/mydata/local_src/Agora_Recording_SDK_for_Linux_FULL/samples/release/bin/recorder", "--appId", "9101377F20E84760B827B18B84CE58CF", "--uid", "10000001", "--channel", "ham", "--appliteDir", "/mydata/local_src/Agora_Recording_SDK_for_Linux_FULL/bin", "--recordFileRootDir", "/mydata/record_file", "--isAudioOnly", "1", "--isMixingEnabled", "1", "--idle", "300")

	if err := cmd.Start(); err != nil {
		panic(err.Error())
	}

	fmt.Printf("shell pid=%v", cmd.Process.Pid)
	pid = cmd.Process.Pid
}

func main() {
	go run()
	time.Sleep(10 * time.Second)

	cmd := exec.Command("kill", "-9", fmt.Sprintf("%v", pid))
	if err := cmd.Start(); err != nil {
		fmt.Println("Start: ", err.Error())
		return
	}
	fmt.Printf("kill -9 pid=%v", pid)

	if err := cmd.Wait(); err != nil {
		fmt.Println("Wait: ", err.Error())
		return

	}

}
