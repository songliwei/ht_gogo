package main

import (
	"errors"
	"strings"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"log"
	"os"
	"runtime"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

var (
	ErrNilDbObject     = errors.New("not set  object current is nil")
	ErrDbParam         = errors.New("err param error")
	ErrNilNoSqlObject  = errors.New("not set nosql object current is nil")
	ErrMemberQuit      = errors.New("member quit state is not 0")
	ErrCloseExplicitly = errors.New("Closed explicitly")
)

var (
	infoLog   *log.Logger
	mongoSess *mgo.Session
)

var (
	RoomInfoDB        = "roominfo"
	RoomMemberTable   = "room_member"
	RoomMaxOrderTable = "room_max_order"
	pageSize          = 20
)

const (
	ENUM_QUIT_STATE_NORMAL       = 0
	ENUM_QUIT_STATE_SELF_QUIT    = 1
	ENUM_QUIT_STATE_ADMIN_REMOVE = 2
)

type MemberInfoStore struct {
	Id          string `bson:"_id"` // 在Mongo中唯一标识一条记录
	RoomId      uint32
	Uid         uint32
	InviteUid   uint32
	NickName    string
	OrderId     uint32
	JoinTS      uint32
	PushSetting uint32
	ContactList uint32
	QuitStat    uint32
	UpdateTS    uint32
}

func UidIsInSlice(uidList []uint32, uid uint32) bool {
	if uid == 0 || len(uidList) == 0 {
		return false
	}
	for _, v := range uidList {
		if uid == v {
			return true
		}
	}

	return false
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func GetRoomMemberList(roomId uint32) (memberList []uint32, err error) {
	if mongoSess == nil {
		infoLog.Printf("GetRoomMemberList mongoSess object is nil")
		err = ErrNilNoSqlObject
		return nil, err
	}
	//从Mongodb 中获取所有的群成员
	mongoConn := mongoSess.DB(RoomInfoDB).C(RoomMemberTable)
	query := mongoConn.Find(bson.M{"roomid": roomId, "quitstat": uint32(ENUM_QUIT_STATE_NORMAL)})
	query.Sort("orderid") // 按照order的升序排
	var result []MemberInfoStore
	query.All(&result)
	for k, v := range result {
		infoLog.Printf("GetRoomMemberList index=%v uid=%v", k, v.Uid)
		memberList = append(memberList, v.Uid)
	}

	return memberList, nil
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// get config roomid
	roomIdStr := cfg.Section("ROOMIDS").Key("roomid").MustString("")
	roomIdSlice := strings.Split(roomIdStr, ",")
	infoLog.Printf("roomIdSize=%v", len(roomIdSlice))
	// init mongo
	// 创建mongodb对象
	mongo_url := cfg.Section("MONGO").Key("url").MustString("localhost")
	infoLog.Printf("Mongo url=%s", mongo_url)
	mongoSess, err = mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
		return
	}
	defer mongoSess.Close()
	// Optional. Switch the session to a monotonic behavior.
	mongoSess.SetMode(mgo.Monotonic, true)
	//memberListStr := "["
	for index, v := range roomIdSlice {
		infoLog.Printf("index=%v v=%s", index, v)
		roomId, err := strconv.ParseUint(v, 10, 32)
		if err != nil {
			infoLog.Printf("ParseUint failed roomId=%s err=%s", v, err)
			continue
		}
		memberList, err := GetRoomMemberList(uint32(roomId))
		if err != nil {
			infoLog.Printf("GetRoomMemberList failed err=%s", err)
			continue
		}
		if len(memberList) > 170 && len(memberList) < 200 {
			infoLog.Printf("almost fulll roomId=%v memberSize=%v", roomId, len(memberList))
		}
		//	if index != 0 {
		//		memberListStr = memberListStr + ","
		//	}

		//	for i, v := range memberList {
		//		infoLog.Printf("roomId=%v index=%v member=%v", roomId, i, v)
		//		if i != 0 {
		//			memberListStr = memberListStr + ","
		//		}
		//		memberListStr = memberListStr + fmt.Sprintf("%v", v)
		//	}
	}
	//memberListStr += "]"
	//infoLog.Printf("memberListStr=%s", memberListStr)
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
