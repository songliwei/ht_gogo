package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/gansidui/gotcp/tcpfw/common"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var redisApi *common.RedisApi
var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis pool
	redisIp := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis ip=%v port=%v", redisIp, redisPort)

	redisApi = common.NewRedisApi(fmt.Sprintf("%s:%v", redisIp, redisPort))

	hashStr := cfg.Section("HASHLIST").Key("value").MustString("")
	hashSlic := strings.Split(hashStr, ",")
	for i, v := range hashSlic {
		infoLog.Printf("index=%v uid=%s", i, v)
		hashName := fmt.Sprintf("%s#gmsg", v)
		val, err := redisApi.HgetInt64(hashName, "0")
		if err != nil {
			infoLog.Printf("redisApi.HgetInt64 hashName=%s failed err=%s", hashName, err)
		} else {
			infoLog.Printf("redisApi.HgetInt64 hashName=%s value=%v success", hashName, val)
		}
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
