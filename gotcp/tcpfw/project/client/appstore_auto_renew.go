package main

import (
	"errors"
	"log"
	"net"
	"strings"

	simplejson "github.com/bitly/go-simplejson"
	"github.com/dogenzaka/go-iap/appstore"

	"github.com/gansidui/gotcp/libcomm"
	"github.com/gansidui/gotcp/tcpfw/project/vip_auto_renew/util"

	"os"
	"runtime"
	"strconv"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	ErrInputParam  = errors.New("invalid intput param")
	ErrFakeOrder   = errors.New("fake order")
	ErrOnlineIpErr = errors.New("err online state ip not exist")
)

var (
	infoLog        *log.Logger
	appstoreClient appstore.Client
)

const (
	PURCHASE_PWD                 = "f2e40870103240f7b485b6f186fa141a"
	PACKAGE_NAME                 = "com.hellotalk"
	WECHAT_PAY_APPID             = "wxd061f34c48968028"
	WECHAT_PAY_COMMERCIAL_TENANT = "1233820102"
	WECHAT_PAY_SIGN_KEY          = "B3A32EF18D804B1BA5B8CA07BEBCBE3E"
)

const (
	TypePurchase = 0 //0-购买
	TypeRenew    = 1 //1-续订
	TRYTIMES     = 3
)

const (
	PaymentPending  = 0
	PaymentReceived = 1
	FreeTrial       = 2
)

const (
	DEFAULT_USER      = 200002
	ExpireReceiptCode = 21006
	VerifySuccessCode = 0
	AppStoreCheckDate = 5 * 24 * 3600
)

const (
	YEAR_NORMAL = 0 //'0-非年会员 1-年会员'
	YEAR_VIP    = 1
)

const (
	E_NOT_VIP       = 0   // 非会员
	E_DAYS_VIP      = 1   // 按天计数的会员(旧版本购买的月、年会员)
	E_MONTHAUTO_VIP = 2   // 月自动续费的会员
	E_YEARAUTO_VIP  = 3   // 年自动续费的会员
	E_LIFETIME_VIP  = 100 // 终生会员 (50年后过期)
)

const (
	PRODUCT_ONE_MONTH = 1
	// "com.hellotalk.onemonth1"
	PRODUCT_ONE_MONTHAUTO = 2
	// "com.hellotalk.onemonthauto"
	// or GooglePlay="com.hellotalk.onemonth1auto"
	// 2016-07-16 android use "com.hellotalk.1monthautorenew"
	// 2016-08-08  iOS use "com.hellotalk.monthauto"
	PRODUCT_ONE_YEAR = 3
	// "com.hellotalk.oneyear"
	PRODUCT_THREE_MONTH = 4
	// "com.hellotalk.3months"
	PRODUCT_YEAR_AUTO = 5
	// "com.hellotalk.yearauto"
	PRODUCT_LIFT_TIME = 6
	// "com.hellotalk.lifetime"
	// product id for upgrade vip price
	PRODUCT_ONE_MONTH_SUB_PLAN = 7
	// iOS:"com.hellotalk.onemonthsubscriptionplan"
	// And:"com.hellotalk.onemonthsubscriptionplan2"
	PRODUCT_ONE_YEAR_SUB_PLAN = 8
	// iOS:"com.hellotalk.oneyearsubscriptionplan"
	// iOS:"com.hellotalk.yearauto2"
	// And:"com.hellotalk.oneyearsubscriptionplan2"
	PRODUCT_LIFE_TIME_SUB_PLAN = 9
	// "com.hellotalk.lifetimeplan"

	PRODUCT_GIFT_ONEMONTH = 11
	// "com.hellotalk.Gift1M" or GooglePlay="com.hellotalk.g1m"
	PRODUCT_GIFT_THREEMONTH = 12
	// "com.hellotalk.Gift3M" or GooglePlay="com.hellotalk.g3m"
	PRODUCT_GIFT_ONEYEAR = 13
	// "com.hellotalk.Gift1Y" or GooglePlay="com.hellotalk.g1y"
	// product id for trial membership
	PRODUCT_ONE_MONTH_SUB = 14
	// iOS:"com.hellotalk.onemonthsubscription"
	// iOS:"com.hellotalk.monthauto_freetrial"
	// And:"com.hellotalk.onemonthsubscription2"
	PRODUCT_ONE_YEAR_SUB = 15
	// iOS:"com.hellotalk.oneyearsubscription"
	// iOS:"com.hellotalk.yearauto2_freetrial"
	// And:"com.hellotalk.oneyearsubscription2"
	PRODUCT_ONE_MULITLANG = 21
	// "com.hellotalk.1moreLang"
	PRODUCT_ONE_MONTH_AUTO_B = 22
	// "com.hellotalk.onemonthauto.b"
	// iOS:"com.hellotalk.super1monthauto"
	PRODUCT_ONE_MONTH_AUTO_C = 23
	// "com.hellotalk.onemonthauto.c"
	PRODUCT_YEAR_AUTO_B = 24
	// "com.hellotalk.yearauto.b"
	// iOS:"com.hellotalk.super1yearauto"
	PRODUCT_YEAR_AUTO_C = 25
	// "com.hellotalk.yearauto.c"
	PRODUCT_LIFE_TIME_B = 26
	// "com.hellotalk.lifetime.b"
	// iOS:"com.hellotalk.superlifetime"
	PRODUCT_LIFE_TIME_C = 27
	// "com.hellotalk.lifetime.c"
	PRODUCT_ITEM_END = 28
	// product item end
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)

const (
	CheckBeginTimestamp = 1473696000 // 2016-09-13 00:00:00 → 1473696000 以09-13为起始时间戳
	Client_Version_220  = 131584
)

// 与数据库中STATE的定义一致
const (
	E_STATE_INIT    = 0 // 未验证
	E_STATE_SUCC    = 1 // 验证成功
	E_STATE_INVALID = 2 // 验证失败
	E_STATE_EXPIRED = 3 // 验证过期
)

const (
	RB_AUTORENEW_EXPIRED = "autorenew_expired"
	RB_PURCHASE_NOTIFY   = "purchase_item"
	RABBIT_SUBMIT        = "user_submit"
	EXCHANGENAME         = "push-msg"
	ROUTINGKEY           = "push-r-1"
)

type AppleRenewItem struct {
	UserId          uint32
	TransactionId   string
	ProductId       string
	LatestReceipt   string
	ExpireTimeStamp uint64
	Status          uint32
	Currency        string
	PayMoney        string
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

// Convert uint to net.IP http://www.outofmemory.cn
func inet_ntoa(ipnr int64) net.IP {
	var bytes [4]byte
	bytes[0] = byte((ipnr >> 24) & 0xFF)
	bytes[1] = byte((ipnr >> 16) & 0xFF)
	bytes[2] = byte((ipnr >> 8) & 0xFF)
	bytes[3] = byte(ipnr & 0xFF)

	return net.IPv4(bytes[3], bytes[2], bytes[1], bytes[0])
}

// Convert net.IP to int64 ,  http://www.outofmemory.cn
func inet_aton(ipnr net.IP) int64 {
	bits := strings.Split(ipnr.String(), ".")

	b0, _ := strconv.Atoi(bits[0])
	b1, _ := strconv.Atoi(bits[1])
	b2, _ := strconv.Atoi(bits[2])
	b3, _ := strconv.Atoi(bits[3])

	var sum int64

	sum += int64(b0) << 24
	sum += int64(b1) << 16
	sum += int64(b2) << 8
	sum += int64(b3)
	return sum
}

func TimeStampToDate(timeStamp int64) (dataString string) {
	// 设置时区为8时区
	var realyZone int = 8
	// zone 里面记录的是小时需要将小时转换成秒
	localZone := time.FixedZone("UTC", int(realyZone)*3600)
	timeNow := time.Unix(timeStamp, 0)
	timeLocal := timeNow.In(localZone)
	dataString = timeLocal.Format("2006-01-02 15:04:05")
	return dataString
}

func AppleStoreVerifyAutoRenew(appleItem *AppleRenewItem) (err error) {
	if appleItem == nil {
		err = ErrInputParam
		infoLog.Printf("AppleStoreVerifyAutoRenew invalid input param")
		return err
	}

	req := appstore.IAPRequest{
		ReceiptData: string(appleItem.LatestReceipt),
		Password:    PURCHASE_PWD,
	}
	resp := &appstore.IAPResponse{}
	err = appstoreClient.Verify(req, resp)
	if err != nil {
		infoLog.Printf("AppleStoreVerifyAutoRenew userId=%v receipts=%v currency=%s payMoney=%s err=%s", appleItem.UserId, appleItem.LatestReceipt, appleItem.Currency, appleItem.PayMoney, err)
		return err
	}
	// infoLog.Printf("AppleStoreVerifyAutoRenew uid=%v appleItem=%#v resp=%#v",
	// 	appleItem.UserId,
	// 	*appleItem,
	// 	resp)
	// copy the transaction
	receipt := resp.Receipt
	latestReceiptInfo := resp.LatestReceiptInfo
	if resp.Status == ExpireReceiptCode {
		latestReceiptInfo = resp.LatestExpireReceiptInfo
	}

	verifyRspStr, err := GenAppStoreAutoRenewVerifyRspStr(resp)
	if err != nil {
		attr := "goautorenew/appstore_json_rsp_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("AppleStoreVerifyAutoRenew GenAppStoreAutoRenewVerifyRspStr failed rsp=%#v err=%s", resp, err)
	}
	if latestReceiptInfo.Bid != util.HelloTalkBid ||
		latestReceiptInfo.ProductID != appleItem.ProductId {
		infoLog.Printf("AppleStoreVerifyAutoRenew Faker Order userId=%v bid=%s productId=%s", appleItem.UserId, latestReceiptInfo.Bid, latestReceiptInfo.ProductID)
		err = ErrFakeOrder
		return err
	}
	itemCode := EnumProductCode(appleItem.ProductId)
	infoLog.Printf("AppleStoreVerifyAutoRenew userId=%v itemCode=%v verifyRspStr=%s", appleItem.UserId, itemCode, verifyRspStr)
	// 判断验证结果
	if resp.Status == ExpireReceiptCode { // 续订过期
		infoLog.Printf("AppleStoreVerifyAutoRenew check Renew Receipt expired status=21006 uid=%v transactionId=%s expireDate=%s",
			appleItem.UserId,
			appleItem.TransactionId,
			latestReceiptInfo.ExpiresDate)
	} else if resp.Status == VerifySuccessCode { // 校验成功
		receiptExpireTs, err := strconv.ParseUint(receipt.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("AppleStoreVerifyAutoRenew strconv.ParseUint receipt.ExpiresDate=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				receipt.ExpiresDate,
				appleItem.UserId,
				verifyRspStr,
				appleItem.Currency,
				appleItem.PayMoney,
				err)
			return err
		}

		latestExpireTs, err := strconv.ParseUint(latestReceiptInfo.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("AppleStoreVerifyAutoRenew strconv.ParseUint latestReceiptInfo.ExpiresDate=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				latestReceiptInfo.ExpiresDate,
				appleItem.UserId,
				verifyRspStr,
				appleItem.Currency,
				appleItem.PayMoney,
				err)
			return err
		}

		latestPurchTs, err := strconv.ParseUint(latestReceiptInfo.PurchaseDateMS, 10, 64)
		if err != nil {
			infoLog.Printf("AppleStoreVerifyAutoRenew strconv.ParseUint latestReceiptInfo.PurchaseDateMS=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				latestReceiptInfo.PurchaseDateMS,
				appleItem.UserId,
				verifyRspStr,
				appleItem.Currency,
				appleItem.PayMoney,
				err)
			return err
		}
		utilTimeStamp := latestExpireTs / 1000 // chage ms to second
		purchasePeriod := (latestExpireTs - latestPurchTs) / 1000

		// auto renew not change
		// 秒的时间戳一样 可能毫秒的时间戳不一样 （iOS回执数据里可能出现的） pippo 2016-09-14
		infoLog.Printf("AppleStoreVerifyAutoRenew check Renew Receipt is not change. uid=%v utilTimeStamp=%v purchasePeriod=%v expireDate=%s receiptExpireTs=%v",
			appleItem.UserId,
			utilTimeStamp,
			purchasePeriod,
			latestReceiptInfo.ExpiresDateFormatted,
			receiptExpireTs)
	}
	return err
}

func GenAppStoreAutoRenewVerifyRspStr(rsp *appstore.IAPResponse) (verifyRspStr string, err error) {
	if rsp == nil {
		err = ErrInputParam
		return verifyRspStr, err
	}
	rspReceipt := rsp.Receipt

	receipt := simplejson.New()
	receipt.Set("expires_date_formatted", rspReceipt.ExpiresDateFormatted)
	receipt.Set("original_purchase_date_pst", rspReceipt.OriginalPurchaseDatePST)
	receipt.Set("unique_identifier", rspReceipt.UniqueIdentifier)
	receipt.Set("original_transaction_id", rspReceipt.OriginalTransactionID)
	receipt.Set("expires_date", rspReceipt.ExpiresDate)
	receipt.Set("app_item_id", rspReceipt.AppItemID)
	receipt.Set("transaction_id", rspReceipt.TransactionID)
	receipt.Set("quantity", rspReceipt.Quantity)
	receipt.Set("expires_date_formatted_pst", rspReceipt.ExpiresDateFormattedPST)
	receipt.Set("product_id", rspReceipt.ProductID)
	receipt.Set("bvrs", rspReceipt.Bvrs)
	receipt.Set("unique_vendor_identifier", rspReceipt.UniqueVendorIdentifier)
	receipt.Set("web_order_line_item_id", rspReceipt.WebOrderLineItemID)
	receipt.Set("original_purchase_date_ms", rspReceipt.OriginalPurchaseDateMS)
	receipt.Set("version_external_identifier", rspReceipt.VersionExternalIdentifier)
	receipt.Set("bid", rspReceipt.Bid)
	receipt.Set("purchase_date_ms", rspReceipt.PurchaseDateMS)
	receipt.Set("purchase_date", rspReceipt.PurchaseDate)
	receipt.Set("purchase_date_pst", rspReceipt.PurchaseDatePST)
	receipt.Set("original_purchase_date", rspReceipt.OriginalPurchaseDate)
	receipt.Set("item_id", rspReceipt.ItemId)

	rspJsonObj := simplejson.New()
	rspJsonObj.Set("receipt", receipt)
	rspJsonObj.Set("status", rsp.Status)
	rspJsonObj.Set("latest_receipt", rsp.LatestReceipt)

	latestReceiptInfo := simplejson.New()
	if rsp.Status == ExpireReceiptCode {
		rspLatestReceiptInfo := rsp.LatestExpireReceiptInfo
		latestReceiptInfo.Set("original_purchase_date_pst", rspLatestReceiptInfo.OriginalPurchaseDatePST)
		latestReceiptInfo.Set("unique_identifier", rspLatestReceiptInfo.UniqueIdentifier)
		latestReceiptInfo.Set("original_transaction_id", rspLatestReceiptInfo.OriginalTransactionID)
		latestReceiptInfo.Set("expires_date", rspLatestReceiptInfo.ExpiresDate)
		latestReceiptInfo.Set("app_item_id", rspLatestReceiptInfo.AppItemID)
		latestReceiptInfo.Set("transaction_id", rspLatestReceiptInfo.TransactionID)
		latestReceiptInfo.Set("quantity", rspLatestReceiptInfo.Quantity)
		latestReceiptInfo.Set("product_id", rspLatestReceiptInfo.ProductID)
		latestReceiptInfo.Set("bvrs", rspLatestReceiptInfo.Bvrs)
		latestReceiptInfo.Set("bid", rspLatestReceiptInfo.Bid)
		latestReceiptInfo.Set("unique_vendor_identifier", rspLatestReceiptInfo.UniqueVendorIdentifier)
		latestReceiptInfo.Set("web_order_line_item_id", rspLatestReceiptInfo.WebOrderLineItemID)
		latestReceiptInfo.Set("original_purchase_date_ms", rspLatestReceiptInfo.OriginalPurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted", rspLatestReceiptInfo.ExpiresDateFormatted)
		latestReceiptInfo.Set("purchase_date", rspLatestReceiptInfo.PurchaseDate)
		latestReceiptInfo.Set("purchase_date_ms", rspLatestReceiptInfo.PurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted_pst", rspLatestReceiptInfo.ExpiresDateFormattedPST)
		latestReceiptInfo.Set("purchase_date_pst", rspLatestReceiptInfo.PurchaseDatePST)
		latestReceiptInfo.Set("original_purchase_date", rspLatestReceiptInfo.OriginalPurchaseDate)
		latestReceiptInfo.Set("item_id", rspLatestReceiptInfo.ItemId)
		rspJsonObj.Set("latest_expired_receipt_info", latestReceiptInfo)
	} else {
		rspLatestReceiptInfo := rsp.LatestReceiptInfo
		latestReceiptInfo.Set("original_purchase_date_pst", rspLatestReceiptInfo.OriginalPurchaseDatePST)
		latestReceiptInfo.Set("unique_identifier", rspLatestReceiptInfo.UniqueIdentifier)
		latestReceiptInfo.Set("original_transaction_id", rspLatestReceiptInfo.OriginalTransactionID)
		latestReceiptInfo.Set("expires_date", rspLatestReceiptInfo.ExpiresDate)
		latestReceiptInfo.Set("app_item_id", rspLatestReceiptInfo.AppItemID)
		latestReceiptInfo.Set("transaction_id", rspLatestReceiptInfo.TransactionID)
		latestReceiptInfo.Set("quantity", rspLatestReceiptInfo.Quantity)
		latestReceiptInfo.Set("product_id", rspLatestReceiptInfo.ProductID)
		latestReceiptInfo.Set("bvrs", rspLatestReceiptInfo.Bvrs)
		latestReceiptInfo.Set("bid", rspLatestReceiptInfo.Bid)
		latestReceiptInfo.Set("unique_vendor_identifier", rspLatestReceiptInfo.UniqueVendorIdentifier)
		latestReceiptInfo.Set("web_order_line_item_id", rspLatestReceiptInfo.WebOrderLineItemID)
		latestReceiptInfo.Set("original_purchase_date_ms", rspLatestReceiptInfo.OriginalPurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted", rspLatestReceiptInfo.ExpiresDateFormatted)
		latestReceiptInfo.Set("purchase_date", rspLatestReceiptInfo.PurchaseDate)
		latestReceiptInfo.Set("purchase_date_ms", rspLatestReceiptInfo.PurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted_pst", rspLatestReceiptInfo.ExpiresDateFormattedPST)
		latestReceiptInfo.Set("purchase_date_pst", rspLatestReceiptInfo.PurchaseDatePST)
		latestReceiptInfo.Set("original_purchase_date", rspLatestReceiptInfo.OriginalPurchaseDate)
		latestReceiptInfo.Set("item_id", rspLatestReceiptInfo.ItemId)
		rspJsonObj.Set("latest_receipt_info", latestReceiptInfo)
	}

	verifyRspSlic, err := rspJsonObj.Encode()
	verifyRspStr = string(verifyRspSlic)
	return verifyRspStr, err
}

func EnumProductCode(productId string) (productCode uint32) {
	if productId == "com.hellotalk.onemonth1" {
		productCode = PRODUCT_ONE_MONTH
	} else if productId == "com.hellotalk.onemonthauto" {
		productCode = PRODUCT_ONE_MONTHAUTO
	} else if productId == "com.hellotalk.onemonthsubscriptionplan" ||
		productId == "com.hellotalk.onemonthsubscriptionplan2" {
		productCode = PRODUCT_ONE_MONTH_SUB_PLAN
	} else if productId == "com.hellotalk.onemonth1auto" ||
		productId == "com.hellotalk.1monthautorenew" ||
		productId == "com.hellotalk.monthauto" {
		productCode = PRODUCT_ONE_MONTHAUTO
	} else if productId == "com.hellotalk.oneyear" ||
		productId == "com.hellotalk.oneyeardiscount" {
		productCode = PRODUCT_ONE_YEAR
	} else if productId == "com.hellotalk.3months" {
		productCode = PRODUCT_THREE_MONTH
	} else if productId == "com.hellotalk.Gift1M" {
		productCode = PRODUCT_GIFT_ONEMONTH
	} else if productId == "com.hellotalk.g1m" {
		productCode = PRODUCT_GIFT_ONEMONTH
	} else if productId == "com.hellotalk.Gift3M" {
		productCode = PRODUCT_GIFT_THREEMONTH
	} else if productId == "com.hellotalk.g3m" {
		productCode = PRODUCT_GIFT_THREEMONTH
	} else if productId == "com.hellotalk.Gift1Y" {
		productCode = PRODUCT_GIFT_ONEYEAR
	} else if productId == "com.hellotalk.g1y" {
		productCode = PRODUCT_GIFT_ONEYEAR
	} else if productId == "com.hellotalk.1morelang" ||
		productId == "com.hellotalk.1moreLang2" {
		productCode = PRODUCT_ONE_MULITLANG
	} else if productId == "com.hellotalk.yearauto" {
		productCode = PRODUCT_YEAR_AUTO
	} else if productId == "com.hellotalk.oneyearsubscriptionplan" ||
		productId == "com.hellotalk.oneyearsubscriptionplan2" ||
		productId == "com.hellotalk.yearauto2" {
		productCode = PRODUCT_ONE_YEAR_SUB_PLAN
	} else if productId == "com.hellotalk.lifetime" {
		productCode = PRODUCT_LIFT_TIME
	} else if productId == "com.hellotalk.lifetimeplan" {
		productCode = PRODUCT_LIFE_TIME_SUB_PLAN
	} else if productId == "com.hellotalk.onemonthsubscription" ||
		productId == "com.hellotalk.onemonthsubscription2" ||
		productId == "com.hellotalk.monthauto_freetrial" {
		productCode = PRODUCT_ONE_MONTH_SUB
	} else if productId == "com.hellotalk.oneyearsubscription" ||
		productId == "com.hellotalk.oneyearsubscription2" ||
		productId == "com.hellotalk.yearauto2_freetrial" {
		productCode = PRODUCT_ONE_YEAR_SUB
	} else if productId == "com.hellotalk.onemonthauto.b" ||
		productId == "com.hellotalk.super1monthauto" {
		productCode = PRODUCT_ONE_MONTH_AUTO_B
	} else if productId == "com.hellotalk.onemonthauto.c" {
		productCode = PRODUCT_ONE_MONTH_AUTO_C
	} else if productId == "com.hellotalk.yearauto.b" ||
		productId == "com.hellotalk.super1yearauto" {
		productCode = PRODUCT_YEAR_AUTO_B
	} else if productId == "com.hellotalk.yearauto.c" {
		productCode = PRODUCT_YEAR_AUTO_C
	} else if productId == "com.hellotalk.lifetime.b" ||
		productId == "com.hellotalk.superlifetime" {
		productCode = PRODUCT_LIFE_TIME_B
	} else if productId == "com.hellotalk.lifetime.c" {
		productCode = PRODUCT_LIFE_TIME_C
	}
	return productCode
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		infoLog.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		infoLog.Fatalln("Must input config file name")
	}

	// infoLog.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		infoLog.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.infoLog")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		infoLog.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// read appstore config
	productEnv := cfg.Section("APPSTORE").Key("product").MustBool(true)
	appTimeOut := cfg.Section("APPSTORE").Key("time_out").MustInt(2)
	infoLog.Printf("APPSTORE product=%v time_out=%v", productEnv, appTimeOut)
	appstoreConfig := appstore.Config{
		IsProduction: productEnv,
		TimeOut:      time.Second * time.Duration(appTimeOut),
	}
	appstoreClient = appstore.NewWithConfig(appstoreConfig)
	appleItem := &AppleRenewItem{
		UserId:          4282629,
		TransactionId:   "700000148535777",
		ProductId:       "com.hellotalk.yearauto",
		LatestReceipt:   "ewoJInNpZ25hdHVyZSIgPSAiQXlzbzYyNEorR3VhaDlNd0NoMmNXamRxOWozajJuNi9XdnFHU0JTSUxFWWJ3Q2tBdnluVFBlVzFOK3U2b21PS0RIK0REYi9jZStkYW50N1VnVnl4RkVTeDZPc1V2R3RDZ0hNRlhXMlA5Vm80Y2k0akw4dDd4NVdjb2xmZklUNlgxOFpDUlRncnUvdFRrdGpJeGFqVmQvc045SjU5REhtbkhkdk9Ic0dtOXNSR0hkQkFkY0tucmZ0Uk1CTzFuUnNiZGwvSlNPVHZNY1V5WURsei9Ud0s2NUEwU1d1WERiNCtqRkgxTnFWNUwycWRRQVJuaXZNai9CQURuMXZySDEwNU5QcFV5QnZ2bk5kZTF1OUxGT2crVGZtTGVnSkJRTHNkZDhwV3NjVUFOb3ZWaC9zWGd6UjRPOGlWYWdKWFpPQ2QwVGNKOExoVm9zTDhHNDBvZU9ldHBod0FBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ERTJMVEV4TFRBNUlEQTVPalV3T2pJeUlFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5CMWNtTm9ZWE5sTFdSaGRHVXRiWE1pSUQwZ0lqRTBOemczTVRNNE1qSXdNREFpT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdWeUlpQTlJQ0kxTnpFMk56WmlOelEwWXpSaU9HVXlZemhrT1Rka01HVTVNRGhrTXpObE5qTmxaVFkxT0RobElqc0tDU0p2Y21sbmFXNWhiQzEwY21GdWMyRmpkR2x2YmkxcFpDSWdQU0FpTnpBd01EQXdNVFE0TlRNMU56YzNJanNLQ1NKbGVIQnBjbVZ6TFdSaGRHVWlJRDBnSWpFMU1UQXlORGs0TWpJd01EQWlPd29KSW1Gd2NDMXBkR1Z0TFdsa0lpQTlJQ0kxTlRjeE16QTFOVGdpT3dvSkluUnlZVzV6WVdOMGFXOXVMV2xrSWlBOUlDSTNNREF3TURBeE5EZzFNelUzTnpjaU93b0pJbkYxWVc1MGFYUjVJaUE5SUNJeElqc0tDU0ozWldJdGIzSmtaWEl0YkdsdVpTMXBkR1Z0TFdsa0lpQTlJQ0kzTURBd01EQXdNak01TURZMk9UWWlPd29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdSaGRHVXRiWE1pSUQwZ0lqRTBOemczTVRNNE1qSXdNREFpT3dvSkluVnVhWEYxWlMxMlpXNWtiM0l0YVdSbGJuUnBabWxsY2lJZ1BTQWlPVEUxTlVORlF6WXRNREpDTWkwMFJqRkJMVUV6UmpVdFFqZzBRa1JDT1RoQk56bENJanNLQ1NKbGVIQnBjbVZ6TFdSaGRHVXRabTl5YldGMGRHVmtMWEJ6ZENJZ1BTQWlNakF4TnkweE1TMHdPU0F3T1RvMU1Eb3lNaUJCYldWeWFXTmhMMHh2YzE5QmJtZGxiR1Z6SWpzS0NTSnBkR1Z0TFdsa0lpQTlJQ0l4TVRRMk56STNOakl5SWpzS0NTSmxlSEJwY21WekxXUmhkR1V0Wm05eWJXRjBkR1ZrSWlBOUlDSXlNREUzTFRFeExUQTVJREUzT2pVd09qSXlJRVYwWXk5SFRWUWlPd29KSW5CeWIyUjFZM1F0YVdRaUlEMGdJbU52YlM1b1pXeHNiM1JoYkdzdWVXVmhjbUYxZEc4aU93b0pJbkIxY21Ob1lYTmxMV1JoZEdVaUlEMGdJakl3TVRZdE1URXRNRGtnTVRjNk5UQTZNaklnUlhSakwwZE5WQ0k3Q2draWIzSnBaMmx1WVd3dGNIVnlZMmhoYzJVdFpHRjBaU0lnUFNBaU1qQXhOaTB4TVMwd09TQXhOem8xTURveU1pQkZkR012UjAxVUlqc0tDU0p3ZFhKamFHRnpaUzFrWVhSbExYQnpkQ0lnUFNBaU1qQXhOaTB4TVMwd09TQXdPVG8xTURveU1pQkJiV1Z5YVdOaEwweHZjMTlCYm1kbGJHVnpJanNLQ1NKaWFXUWlJRDBnSW1OdmJTNW9aV3hzYjFSaGJHc3VhR1ZzYkc5VVlXeHJJanNLQ1NKaWRuSnpJaUE5SUNJeE5TSTdDbjA9IjsKCSJwb2QiID0gIjcwIjsKCSJzaWduaW5nLXN0YXR1cyIgPSAiMCI7Cn0=",
		ExpireTimeStamp: uint64(1510249822000),
		Status:          1,
		Currency:        "TRY",
		PayMoney:        "39.99",
	}
	err = AppleStoreVerifyAutoRenew(appleItem)
	if err != nil {
		infoLog.Printf("AppleStoreVerifyAutoRenew err=%s", err)
	}
	return
}

func checkError(err error) {
	if err != nil {
		infoLog.Fatal(err)
	}
}
