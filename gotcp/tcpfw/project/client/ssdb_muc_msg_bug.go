package main

import (
	// "github.com/bitly/go-simplejson"

	"fmt"
	"log"
	"os"
	"strings"

	"github.com/gansidui/gotcp/tcpfw/common"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbSlaverHost := cfg.Section("SLAVESSDB").Key("ip").MustString("127.0.0.1")
	ssdbSlavePort := cfg.Section("SLAVESSDB").Key("port").MustInt(8888)
	infoLog.Printf("ssdb slave host=%s port=%v", ssdbSlaverHost, ssdbSlavePort)
	ssdbSlaveApi, err := common.NewSsdbApi(ssdbSlaverHost, ssdbSlavePort, 5, 50)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	uidStr := cfg.Section("uidstr").Key("value").MustString("")
	uidSlic := strings.Split(uidStr, ",")
	hashKey := "99999999999"
	for i, v := range uidSlic {
		infoLog.Printf("index=%v uid=%s", i, v)
		hashName := fmt.Sprintf("%v#gmsg", v)
		value, err := ssdbSlaveApi.Hget(hashName, hashKey)
		if err != nil {
			infoLog.Printf("Hget hashName=%s hashKey=%s err=%s", hashName, hashKey, err)
			continue
		}
		if len(value) > 0 {
			infoLog.Printf("Hget hashName=%s hashKey=%s hashValue=%s", hashName, hashKey, value)
		}
	}

	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
