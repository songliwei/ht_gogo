package main

import (
	// "github.com/bitly/go-simplejson"

	"fmt"
	"log"
	"os"

	"github.com/gansidui/gotcp/tcpfw/common"
	"github.com/gansidui/gotcp/tcpfw/include/ht_chatrecord"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, 5, 50)
	if err != nil {
		infoLog.Printf("ssdb new pool failed err=%s", err)
		return
	}

	// 获取测试的uid
	targetUid := uint32(cfg.Section("TARGETUID").Key("uid").MustUint(1946612))
	// Step1: 获取ispeakto 的聊天计数
	hashName := GetISpeakToHashMapName(targetUid)
	kvIspeakTo, err := ssdbApi.MultiHgetAll(hashName)
	if err != nil {
		infoLog.Printf("ssdbApi.MultiHgetAll hasName=%s failed err=%s", hashName, err)
	}
	for k, v := range kvIspeakTo {
		valSlic := v.Bytes()
		item := &ht_chat_record.ChatRecordItem{}
		err = proto.Unmarshal(valSlic, item)
		if err != nil {
			infoLog.Printf("proto.Unmarshal proto Unmarshal failed key=%s", k)
			continue
		}
		infoLog.Printf("targetUid=%v Ispeakto k=%s from=%v to=%v totalCount=%v startTime=%v",
			targetUid,
			k,
			item.GetFromId(),
			item.GetToId(),
			item.GetTotalCount(),
			item.GetStartTime())
	}

	//Stpe2: 获取speaktome的聊天计数
	hashName = GetSpeakToMeHashMapName(targetUid)
	kvSpeakToMe, err := ssdbApi.MultiHgetAll(hashName)
	if err != nil {
		infoLog.Printf("ssdbApi.MultiHgetAll hasName=%s failed err=%s", hashName, err)
	}
	for k, v := range kvSpeakToMe {
		valSlic := v.Bytes()
		item := &ht_chat_record.ChatRecordItem{}
		err = proto.Unmarshal(valSlic, item)
		if err != nil {
			infoLog.Printf("proto.Unmarshal proto Unmarshal failed key=%s", k)
			continue
		}
		infoLog.Printf("targetUid=%v SpeakToMe k=%s from=%v to=%v totalCount=%v startTime=%v",
			targetUid,
			k,
			item.GetFromId(),
			item.GetToId(),
			item.GetTotalCount(),
			item.GetStartTime())
	}
	return
}

func GetISpeakToHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#ispeakto", uid)
	return hashName
}

func GetSpeakToMeHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#speaktome", uid)
	return hashName
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
