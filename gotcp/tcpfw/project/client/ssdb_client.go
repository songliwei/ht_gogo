package main

import (
	// "github.com/bitly/go-simplejson"

	"log"
	"os"

	"github.com/gansidui/gotcp/tcpfw/common"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, 5, 50)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	/*pool, err := gossdb.NewPool(&gossdb.Config{
		Host:             ssdbHost,
		Port:             ssdbPort,
		MinPoolSize:      5,
		MaxPoolSize:      50,
		AcquireIncrement: 5,
	})
	if err != nil {
		log.Fatal(err)
		return
	}

	c, err := pool.NewClient()
	if err != nil {
		log.Println(err.Error())
		return
	}
	defer c.Close() */
	//	keys := []string{"1946612"}
	hashName := "2325928#speaktome"
	keyValues, err := ssdbApi.MultiHgetAll(hashName)
	//	outKeys, outValues, err := ssdbApi.MultiHgetSliceArray(hashName, keys)
	//	infoLog.Printf("hashName=%s keys=%v outKeys=%v outValues=%v", hashName, keys, outKeys, outValues)
	if err != nil {
		infoLog.Printf("ssdbApi.MultiHgetAll exec failed err=%s", err)
		return
	}
	//	for i, v := range outKeys {
	//		infoLog.Printf("index=%v key=%v", i, v)
	//	}
	for i, v := range keyValues {
		infoLog.Printf("index=%v value=%v", i, v)
	}

	//	val, err := ssdbApi.Hscan("1946612#LIST", "", "", 1000, true)
	//	if err != nil {
	//		infoLog.Printf("ssdbApi.Hscan exec failed err=%s", err)
	//		return
	//	}

	//	for i, v := range val {
	//		infoLog.Printf("key=%s value=%v", i, v)
	//	}

	//	keys, values, err = ssdbApi.HscanArray("1946612#LIST", "", "", 1000, true)
	//	if err != nil {
	//		infoLog.Printf("ssdbApi.HscanArray exec failed err=%s", err)
	//		return
	//	}
	//	for i, v := range keys {
	//		infoLog.Printf("index=%v key=%v", i, v)
	//	}
	//	for i, v := range values {
	//		infoLog.Printf("index=%v value=%v", i, v)
	//	}

	//score, err := ssdbApi.Zget("12796031747157820353_like_uid_set", "2325928")
	//	score, err := ssdbApi.Zget("12796031747157820353_like_uid_set", "1946612")
	//	if err != nil {
	//		infoLog.Printf("ssdbApi.Zget failed err=%s", err)
	//	} else {
	//		infoLog.Printf("key=2325928 score=%v", score)
	//	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
