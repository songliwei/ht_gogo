package main

import (
	"log"
	"net"
	"os"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

var (
	infoLog *log.Logger
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	Config  string `short:"c" long:"config" description:"config file" optional:"no"`
	Cmd     uint32 `short:"t" long:"command" description:"test commad" optional:"no"`
	Op      uint32 `short:"o" long:"operator" description:"operator add or del"`
	List    uint32 `short:"l" logn:"list" description:"list type"`
	Type    uint32 `short:"y" long:"Latest" description:"get latest or history moment"`
	MntType uint32 `short:"n" long:"moment" description:"moment type"`
	Seq     uint32 `short:"s" long:"seq" description:"specific seq"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
		os.Exit(1)
	}
	if options.Config == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.Config)
	if err != nil {
		log.Printf("load config file=%s failed", options.Config)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)

	uid := cfg.Section("TEST_UID").Key("UID").MustInt(1946612)
	headV2Protocol := &common.HeadV2Protocol{}
	reqBody := new(ht_moment.ReqBody)
	head := &common.HeadV2{Cmd: uint32(ht_moment.CMD_TYPE_CMD_OPERATOR_UID),
		Len: uint32(common.EmptyPacktV2Len),
		Uid: uint32(uid)}
	cmd := options.Cmd
	infoLog.Println("cmd =", cmd)
	switch cmd {
	case uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_DOWN):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_DOWN)
			reqBody = &ht_moment.ReqBody{
				ViewLatestMntIdReqbody: &ht_moment.ViewLatestMomentIDReqBody{
					Userid:      proto.Uint32(2325928),
					Qtype:       ht_moment.QUERY_TYPE_DEFAULT.Enum(),
					LangType:    proto.Uint32(0),
					Nationality: []byte("CN"),
					LocalMaxMid: []byte("0"),
					ReqUserId:   proto.Uint32(2325928),
					Ostype:      proto.Uint32(1),
					Version:     proto.Uint32(1),
					FeaturedCon: nil,
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_UP):
		{

		}
	case uint32(ht_moment.CMD_TYPE_CMD_DELETE_USER):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_DELETE_USER)
			// 更新redis
			reqBody = &ht_moment.ReqBody{
				DeleteUserReqbody: &ht_moment.DeleteUserReqBody{
					Userid: []uint32{2325928},
					Token:  []byte("songliwei"),
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_RESTORE_USER):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_RESTORE_USER)
			// 更新redis
			reqBody = &ht_moment.ReqBody{
				RestoreUserReqbody: &ht_moment.RestoreUserReqBody{
					Userid: []uint32{2325928},
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_STATUS):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_STATUS)
			// 更新redis
			reqBody = &ht_moment.ReqBody{
				ModMntStatusReqbody: &ht_moment.ModMntStatusReqBody{
					Mid: []byte("12649839614609097695"),
					//OpType:   ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY.Enum(),
					OpType:   ht_moment.MntStatusOpType_OP_RESTORE.Enum(),
					OpReason: []byte("just test"),
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_POST_AD_MOMENT):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_POST_AD_MOMENT)
			// 更新redis
			reqBody = &ht_moment.ReqBody{
				PostMntReqbody: &ht_moment.PostMomentReqBody{
					Moment: &ht_moment.MomentBody{
						Userid:      proto.Uint32(10086),
						Mid:         nil,
						Content:     []byte("test post ad moment"),
						PostTime:    proto.Uint64(1509441479),
						Latitude:    nil,
						Longitude:   nil,
						LangType:    proto.Uint32(1),
						StrLangType: []byte("EN"),
						UrlInfo:     nil,
					},
					Ostype:  proto.Uint32(1),
					Version: proto.Uint32(1),
					Token:   []byte("songliwei"),
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_OPERATOR_UID):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_OPERATOR_UID)
			reqBody = &ht_moment.ReqBody{
				OpUidReqbody: &ht_moment.OperatorUidReqBody{
					ListType: ht_moment.OPERATORLIST_OP_NOT_SHARE_LIST.Enum(),
					OpType:   ht_moment.OPERATORTYPE_OP_ADD.Enum(),
					OpUserId: proto.Uint32(2325928),
					Userid:   proto.Uint32(900866),
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_GET_OPERATOR_UID_LIST):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_GET_OPERATOR_UID_LIST)
			reqBody = &ht_moment.ReqBody{
				GetOpUidListReqbody: &ht_moment.GetUidListReqBody{
					ListType:    ht_moment.OPERATORLIST_OP_NOT_SHARE_LIST.Enum(),
					HideListVer: proto.Uint32(0),
					NotShareVer: proto.Uint32(0),
					Userid:      proto.Uint32(900866),
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_BACKEND_OP_UID):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_BACKEND_OP_UID)
			reqBody = &ht_moment.ReqBody{
				BackendOpUidReqbody: &ht_moment.BackEndOpUidReqBody{
					ListType: ht_moment.OPERATORLIST_OP_MOMENT_FOR_SELF_LIST.Enum(),
					OpType:   ht_moment.OPERATORTYPE_OP_DEL.Enum(),
					OpUserId: proto.Uint32(900866),
					Userid:   proto.Uint32(2325928),
					OpReason: []byte("just dont tell you"),
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_QUERY_REVEAL_ENTRY):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_QUERY_REVEAL_ENTRY)
			reqBody = &ht_moment.ReqBody{
				QueryRevealentryReqbody: &ht_moment.QueryRevealEntryReqBody{
					ReqUserId: proto.Uint32(2325928),
					Qtype:     []ht_moment.QUERY_TYPE{ht_moment.QUERY_TYPE(options.MntType)},
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_OP_MID_BUCKET):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_OP_MID_BUCKET)
			// 更新redis
			reqBody = &ht_moment.ReqBody{
				OpMidBucketStatusReqbody: &ht_moment.OpMidBucketStatusReqBody{
					Userid: proto.Uint32(5547632),
					Mid:    []byte("12967250724387062751"),
					State:  []ht_moment.MID_BUCKET_STATUS{ht_moment.MID_BUCKET_STATUS_BS_LEARN},
					OpType: ht_moment.OPERATORTYPE_OP_ADD.Enum(),
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INDEX_MOMENT):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INDEX_MOMENT)
			reqBody = &ht_moment.ReqBody{
				GetUserIndexMomentReqbody: &ht_moment.GetUserIndexMomentReqBody{
					Userid: proto.Uint32(2325928),
					UserIndex: []*ht_moment.MomentUserIndexInfo{
						&ht_moment.MomentUserIndexInfo{
							Userid: proto.Uint32(2325928),
							Seq:    proto.Uint32(9),
						},
					},
				},
			}
		}
	case uint32(ht_moment.CMD_TYPE_CMD_SET_COMMENT_STAT):
		{
			head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_SET_COMMENT_STAT)
			subReqBody := &ht_moment.SetCommentStatReqBody{
				Userid: proto.Uint32(10000),
				Mid:    []byte("12754277481925805003"),
				Cid:    []byte("1"),
				Token:  []byte("null token"),
				Ctype:  ht_moment.COMMENT_TYPE_TEXT.Enum(),
				Status: ht_moment.COMMENT_STAT_STAT_DEL.Enum(),
			}
			reqBody.SetCommentStatReqbody = subReqBody
		}
	default:
		{
			infoLog.Println("unknow cmd =", cmd)
			return
		}
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Println("marshaling error: ", err)
	}

	head.Len = uint32(common.PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV3ToSlice failed")
		os.Exit(1)
	}
	copy(buf[common.PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = common.HTV2MagicEnd

	infoLog.Println("payLoad=", common.NewHeadV2Packet(buf).Serialize())
	// write
	conn.Write(common.NewHeadV2Packet(buf).Serialize())

	// read
	p, err := headV2Protocol.ReadPacket(conn)
	if err == nil {
		packet := p.(*common.HeadV2Packet)
		// infoLog.Printf("Server reply:[%v] [%v]\n", headV2Packet.GetLength(), string(headV2Packet.GetBody()))
		rspHead, err := packet.GetHead()
		if err == nil {
			infoLog.Printf("head=%+v", rspHead)
			rspBody := &ht_moment.RspBody{}
			err = proto.Unmarshal(packet.GetBody(), rspBody)
			if err != nil {
				infoLog.Println("proto Unmarshal failed")
				return
			}
			switch rspHead.Cmd {
			case uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_DOWN):
				{
					subRspBody := rspBody.GetViewLatestMntIdRspbody()
					status := subRspBody.GetStatus()
					infoLog.Printf("status=%+v pagesize=%d", status, subRspBody.GetPageSize())
					bucketList := subRspBody.GetBucket().GetBucketList()
					for i, v := range bucketList {
						infoLog.Printf("index=%v bucket name=%s index=%v", i, v.GetBucketName(), v.GetIndex())
					}
					idList := subRspBody.GetIdList()
					for i, v := range idList {
						infoLog.Printf("index=%v mid=%v LikedTs=%v CommentTs=%v Deleted=%v OpReason=%v",
							i,
							v.GetMid(),
							v.GetLikedTs(),
							v.GetCommentTs(),
							v.GetDeleted(),
							v.GetOpReason())
					}
				}
			case uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_UP):
				{
					infoLog.Printf("rspHead.Cmd=%v", rspHead.Cmd)
				}
			case uint32(ht_moment.CMD_TYPE_CMD_DELETE_USER):
				{
					subRspBody := rspBody.GetDeleteUserRspbody()
					infoLog.Printf("stat=%v reason=%s",
						subRspBody.GetStatus().GetCode(),
						subRspBody.GetStatus().GetReason())
				}
			case uint32(ht_moment.CMD_TYPE_CMD_RESTORE_USER):
				{
					subRspBody := rspBody.GetRestoreUserRspbody()
					infoLog.Printf("stat=%v reason=%s",
						subRspBody.GetStatus().GetCode(),
						subRspBody.GetStatus().GetReason())
				}
			case uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_STATUS):
				{
					subRspBody := rspBody.GetModMntStatusRspbody()
					infoLog.Printf("stat=%v reason=%s",
						subRspBody.GetStatus().GetCode(),
						subRspBody.GetStatus().GetReason())
				}
			case uint32(ht_moment.CMD_TYPE_CMD_POST_AD_MOMENT):
				{
					subRspBody := rspBody.GetPostMntRspbody()
					infoLog.Printf("stat=%v reason=%s Mid=%s postTime=%v",
						subRspBody.GetStatus().GetCode(),
						subRspBody.GetStatus().GetReason(),
						subRspBody.GetMid(),
						subRspBody.GetPostTime())
				}
			case uint32(ht_moment.CMD_TYPE_CMD_OPERATOR_UID):
				{
					subRspBody := rspBody.GetOpUidRspbody()
					infoLog.Printf("stat=%v list_type=%v", subRspBody.GetStatus().GetCode(), subRspBody.GetListType())
				}
			case uint32(ht_moment.CMD_TYPE_CMD_GET_OPERATOR_UID_LIST):
				{
					subRspBody := rspBody.GetGetOpUidListRspbody()
					status := subRspBody.GetStatus()
					infoLog.Println("stat =", status.GetCode())
					listType := subRspBody.GetListType()
					switch listType {

					case ht_moment.OPERATORLIST_OP_HIDE_LIST:
						{
							uidArray := subRspBody.GetHideUidArray().GetUidArray()
							infoLog.Printf("list version=%v", subRspBody.GetHideUidArray().GetListVer())
							for i, v := range uidArray {
								infoLog.Printf("index=%v uid=%v timeStamp=%v", i, v.GetUid(), v.GetOpTimeStamp())
							}
						}
					case ht_moment.OPERATORLIST_OP_NOT_SHARE_LIST:
						{
							uidArray := subRspBody.GetNotShareArray().GetUidArray()
							infoLog.Printf("list version=%v", subRspBody.GetNotShareArray().GetListVer())
							for i, v := range uidArray {
								infoLog.Printf("index=%v uid=%v timeStamp=%v", i, v.GetUid(), v.GetOpTimeStamp())
							}
						}
					}

				}
			case uint32(ht_moment.CMD_TYPE_CMD_BACKEND_OP_UID):
				{
					subRspBody := rspBody.GetBackendOpUidRspbody()
					status := subRspBody.GetStatus()
					infoLog.Printf("status=%v list_type=%v", status.GetCode(), subRspBody.GetListType())
				}
			case uint32(ht_moment.CMD_TYPE_CMD_QUERY_REVEAL_ENTRY):
				{
					subRspBody := rspBody.GetQueryRevealentryRspbody()
					status := subRspBody.GetStatus()
					len := len(subRspBody.ListReveal)
					infoLog.Printf("QueryRevealEntry ret status=%v ListRevral size=%d\n", status, len)
					if len > 0 {
						revealBody := subRspBody.ListReveal[0]
						infoLog.Printf("tyep=%v be_reveal=%v\n", revealBody.GetQtype(), revealBody.GetBeReveal())
					} else {
						infoLog.Println("error len = 0")
					}
				}
			case uint32(ht_moment.CMD_TYPE_CMD_OP_MID_BUCKET):
				{
					subRspBody := rspBody.GetOpMidBucketStatusRspbody()
					infoLog.Printf("stat=%v reason=%s",
						subRspBody.GetStatus().GetCode(),
						subRspBody.GetStatus().GetReason())
				}
			case uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INDEX_MOMENT):
				{
					subRspBody := rspBody.GetGetUserIndexMomentRspbody()
					status := subRspBody.GetStatus()
					infoLog.Printf("QueryMid ret status=%v\n", status)
					if status.GetCode() == 0 {
						moments := subRspBody.GetMoment()
						for i := 0; i < len(moments); i++ {
							iterm := moments[i]
							infoLog.Printf("uid=%v mid=%s posttime=%v lat=%v long=%v lang=%v\n", iterm.GetUserid(), iterm.GetMid(), iterm.GetPostTime(), iterm.GetLatitude(), iterm.GetLongitude(), iterm.GetLangType())
						}
					}

				}
			case uint32(ht_moment.CMD_TYPE_CMD_SET_COMMENT_STAT):
				{
					subRspBody := rspBody.GetSetCommentStatRspbody()
					status := subRspBody.GetStatus()
					infoLog.Printf("SetCommentStat ret status=%v\n", status)
					if status.GetCode() == 0 {
						infoLog.Printf("SetCommentStat Mid=%s Cid=%s", subRspBody.GetMid(), subRspBody.GetCid())
					}

				}
			}
		} else {
			infoLog.Println("get HTV2Head failed err =", err)
		}
	}
	conn.Close()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
