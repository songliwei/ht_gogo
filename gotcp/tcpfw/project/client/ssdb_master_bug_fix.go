package main

import (
	// "github.com/bitly/go-simplejson"

	"log"
	"os"
	"strings"
	"time"

	"github.com/gansidui/gotcp/tcpfw/common"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbMasterHost := cfg.Section("MASTERSSDB").Key("ip").MustString("127.0.0.1")
	ssdbMasterPort := cfg.Section("MASTERSSDB").Key("port").MustInt(8888)
	infoLog.Printf("ssdb master host=%s port=%v", ssdbMasterHost, ssdbMasterPort)
	ssdbMasterApi, err := common.NewSsdbApi(ssdbMasterHost, ssdbMasterPort, 5, 50)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	ssdbSlaverHost := cfg.Section("SLAVESSDB").Key("ip").MustString("127.0.0.1")
	ssdbSlavePort := cfg.Section("SLAVESSDB").Key("port").MustInt(8888)
	infoLog.Printf("ssdb slave host=%s port=%v", ssdbSlaverHost, ssdbSlavePort)
	ssdbSlaveApi, err := common.NewSsdbApi(ssdbSlaverHost, ssdbSlavePort, 5, 50)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	midStr := cfg.Section("midstr").Key("value").MustString("")
	midSlic := strings.Split(midStr, ",")
	for i, v := range midSlic {
		infoLog.Printf("index=%v mid=%s", i, v)
		keyValues, err := ssdbSlaveApi.MultiHgetAll(v)
		if err != nil {
			infoLog.Printf("relad hashName=%s err=%s", v, err)
			continue
		}
		keyValueStrings := make(map[string]interface{})
		for k, v := range keyValues {
			keyValueStrings[k] = v.String()
		}
		err = ssdbMasterApi.MultiHset(v, keyValueStrings)
		if err != nil {
			infoLog.Printf("MultiHset hashName=%s err=%s", v, err)
			continue
		}
		if i%10 == 0 {
			time.Sleep(1 * time.Second)
		}

	}

	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
