package main

import (
	// "github.com/bitly/go-simplejson"
	"database/sql"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()

	for uid := 10000; uid < 5300000; uid++ {
		//	for uid := 10000; uid < 12000; uid++ {
		var textTrans, textNoTrans, voiceTrans, voiceNoTrans, crectSend, crectRecv sql.NullInt64
		err = db.QueryRow("SELECT T_TRANS, T_NOTRANS, V_TRANS, V_NOTRANS, CORRECT_SEND, CORRECT_RECV FROM LOG_FAVORITE_SUBMIT WHERE USERID = ? ORDER BY UPDATETIME DESC limit 1", uid).Scan(&textTrans, &textNoTrans, &voiceTrans, &voiceNoTrans, &crectSend, &crectRecv)
		total := textTrans.Int64 + textNoTrans.Int64 + voiceTrans.Int64 + voiceNoTrans.Int64 + crectSend.Int64 + crectRecv.Int64
		totalTrans := textTrans.Int64 + voiceTrans.Int64
		totalNoTrans := textNoTrans.Int64 + voiceNoTrans.Int64
		totalCrect := crectSend.Int64 + crectRecv.Int64
		if total > 0 {
			infoLog.Printf("UID=%-10d T_TRANS=%-10d  T_NOTRANS=%-10d  V_TRANS=%-10d  V_NOTRANS=%-10d CORRECT_SEND=%-10d CORRECT_RECV=%-10d  TOTAL=%-10d  ToTaL_TRANS=%-10d  TOTAL_NOT_TRANS=%-10d TOTAL_CORRECT=%-10d",
				uid,
				textTrans.Int64,
				textNoTrans.Int64,
				voiceTrans.Int64,
				voiceNoTrans.Int64,
				crectSend.Int64,
				crectRecv.Int64,
				total,
				totalTrans,
				totalNoTrans,
				totalCrect)
		}
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
