package main

import (
	// "github.com/bitly/go-simplejson"

	"bytes"
	"compress/zlib"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"github.com/seefan/gossdb"
	"github.com/seefan/gossdb/conf"
	"github.com/ssdb/gossdb/ht_offline"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
	Uid        uint32 `short:"u" long:"uid" description:"target user id"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	pool, err := gossdb.NewPool(&conf.Config{
		Host:             ssdbHost,
		Port:             ssdbPort,
		MinPoolSize:      5,
		MaxPoolSize:      50,
		AcquireIncrement: 5,
	})
	if err != nil {
		infoLog.Printf("ssdb new pool failed err=%s", err)
		return
	}

	c, err := pool.NewClient()
	if err != nil {
		infoLog.Printf("new ssdb clinet failed err=%s", err)
		return
	}
	defer c.Close()

	beginUid := options.Uid
	hashTimeTable := fmt.Sprintf("%v_msg", beginUid)
	ret, err := c.Hsize(hashTimeTable) // get uid
	if err != nil {
		infoLog.Printf("ssdb failed exists hashTimeTable=%s err=%s", hashTimeTable, err)
	}
	if ret == 0 {
		infoLog.Printf("ssdb hashTimeTable=%s size=%v empty", hashTimeTable, ret)
		return
	} else {
		infoLog.Printf("hashTimeTable=%s size=%v", hashTimeTable, ret)
	}
	keyValue, err := c.HgetAll(hashTimeTable)
	if err == nil {
		for k, v := range keyValue {
			infoLog.Printf("index=%s", k)
			if k == "0" {
				continue
			}
			itermMsg := new(ht_offline.Msg)
			err := proto.Unmarshal(v.Bytes(), itermMsg)
			if err != nil {
				infoLog.Printf("index=%s proto Unmarshal failed", k)
				continue
			}
			infoLog.Printf("index=%s from=%v to=%v cmd=0x%4X format=%v msg_time=%v",
				k,
				itermMsg.GetFromUid(),
				itermMsg.GetToUid(),
				itermMsg.GetCmd(),
				itermMsg.GetFormat(),
				itermMsg.GetMsgTime())
			//len(itermMsg.GetContent()))
			if itermMsg.GetCmd() == 0x4001 || itermMsg.GetCmd() == 0x7049 {
				messagePayLoad := itermMsg.GetContent()
				if itermMsg.GetCmd() == 0x4001 {
					messagePayLoad = messagePayLoad[20:]
				} else {
					messagePayLoad = messagePayLoad[55 : len(messagePayLoad)-1]
				}
				compressBuff := bytes.NewBuffer(messagePayLoad)
				r, err := zlib.NewReader(compressBuff)
				if err != nil {
					infoLog.Printf("index=%s zlib.NewReader failed err=%s", k, err)
					continue
				}
				// 解压缩
				unCompressSlice, err := ioutil.ReadAll(r)
				if err != nil {
					infoLog.Printf("index=%s zlib. unCompress failed err=%s", k, err)
					continue
				}
				r.Close()
				// 解压之后的内容构造json
				infoLog.Printf("index=%s message=%s", k, unCompressSlice)
				// rootObj, err := simplejson.NewJson(unCompressSlice)
				// if err != nil {
				// 	infoLog.Printf("index=%s simplejson new packet error err=%s", k, err)
				// 	continue
				// } else {
				// 	infoLog.Printf("index=%s message=%#v", )
				// }
			}
		}
	} else {
		infoLog.Printf("ssdb hgetall hashTimeTable=%s err=%s failed", hashTimeTable, err)
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
