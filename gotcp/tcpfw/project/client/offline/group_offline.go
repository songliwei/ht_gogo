package main

import (
	// "github.com/bitly/go-simplejson"

	"fmt"
	"log"
	"os"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc_store"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		return
	}

	beginUid := cfg.Section("TESTUID").Key("begin_uid").MustInt(10000)
	hashName := fmt.Sprintf("%v#gms", beginUid)
	keys, values, err := ssdbApi.MultiHgetAllSlice(hashName)
	if err != nil {
		infoLog.Printf("ssdbApi.MultiHgetAllSlice hashName=%s err=%s", hashName, err)
		return
	}
	for i, v := range values {
		infoLog.Printf("hashName=%s index=%s", hashName, keys[i])
		tmepValue := v.Bytes()
		msgBody := new(ht_muc_store.MucMsg)
		err := proto.Unmarshal(tmepValue, msgBody)
		if err != nil {
			infoLog.Printf("hashName=%s index=%s err=%s", hashName, keys[i], err)
			continue
		}
		infoLog.Printf("hashName=%s index=%s from=%v roomId=%v cmd=0x%4x msg_time=%s",
			hashName,
			keys[i],
			msgBody.GetFromId(),
			msgBody.GetRoomId(),
			msgBody.GetCmd(),
			msgBody.GetMsgTime())
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
