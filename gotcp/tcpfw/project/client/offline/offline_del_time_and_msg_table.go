package main

import (
	// "github.com/bitly/go-simplejson"

	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init ssdb
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		return
	}

	// init dbUtil
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	beginUid := cfg.Section("TESTUID").Key("begin_uid").MustInt(10000)
	endUid := cfg.Section("TESTUID").Key("end_uid").MustInt(10000)
	// uidSlicCfg := cfg.Section("TESTUID").Key("uidSlice").MustString("1946612")
	// uidSlic := strings.Split(uidSlicCfg, ",")
	// 查询整个群所有群成员
	rows, err := db.Query("SELECT USERID FROM HT_USER_STATE  WHERE  USERID  > ? AND USERID < ?  AND LASTACTIVE < DATE_SUB(UTC_TIMESTAMP(), INTERVAL 6 MONTH) AND LASTACTIVE > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 16 MONTH)", beginUid, endUid)
	if err != nil {
		infoLog.Printf("db.Query err=%s", err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var uid uint32
		if err := rows.Scan(&uid); err != nil {
			infoLog.Printf("rows.Scan failed err=%s", err)
			continue
		}
		hashTimeTable := fmt.Sprintf("%v_time", uid)
		ret, err := ssdbApi.Hsize(hashTimeTable) // get uid
		if err != nil {
			infoLog.Printf("ssdb failed exists hashTimeTable=%s err=%s", hashTimeTable, err)
			continue
		}
		if ret == 0 {
			infoLog.Printf("ssdb hashTimeTable=%s size=%v empty", hashTimeTable, ret)
		} else {
			infoLog.Printf("hashTimeTable=%s size=%v", hashTimeTable, ret)
			err = ssdbApi.Hclear(hashTimeTable)
			if err != nil {
				infoLog.Printf("ssdb hclear hashTimeTable=%s err=%s failed", hashTimeTable, err)
				continue
			} else {
				infoLog.Printf("ssdb hclear hashTimeTable=%s success", hashTimeTable)
			}
		}

		hashMsgTable := fmt.Sprintf("%v_msg", uid)
		ret, err = ssdbApi.Hsize(hashMsgTable) // get uid
		if err != nil {
			infoLog.Printf("ssdb failed exists hashMsgTable=%s err=%s", hashMsgTable, err)
			continue
		}
		if ret == 0 {
			infoLog.Printf("ssdb hashMsgTable=%s size=%v empty", hashMsgTable, ret)
			continue
		} else {
			infoLog.Printf("hashMsgTable=%s size=%v", hashMsgTable, ret)
			if uid%4 == 0 {
				time.Sleep(1 * time.Second)
			}
			err = ssdbApi.Hclear(hashMsgTable)
			if err != nil {
				infoLog.Printf("ssdb hclear hashMsgTable=%s err=%s failed", hashMsgTable, err)
				continue
			} else {
				infoLog.Printf("ssdb hclear hashMsgTable=%s success", hashMsgTable)
			}
		}
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
