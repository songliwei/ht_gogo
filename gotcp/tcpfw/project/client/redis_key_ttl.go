package main

import (
	"fmt"
	"log"
	"os"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	"strings"
	"time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var redisApi *common.RedisApi
var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis pool
	redisIp := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis ip=%v port=%v", redisIp, redisPort)

	redisApi = common.NewRedisApi(fmt.Sprintf("%s:%v", redisIp, redisPort))

	keysCfg := cfg.Section("KEYS").Key("key_slic").MustString("13053539313432797187")
	keySlic := strings.Split(keysCfg, ",")
	// ttl := cfg.Section("KEYS").Key("ttl").MustUint64(12 * 3600)
	for i, v := range keySlic {
		infoLog.Printf("index=%v key=%s", i, v)
		// err := redisApi.Expire(v, ttl)
		result, err := redisApi.TTL(v)
		if err != nil {
			infoLog.Printf("redisApi.TTL key=%s failed err=%s", v, err)
		} else {
			infoLog.Printf("redisApi.TTL key=%s ttl=%v", v, result)
		}
		if i%100 == 0 {
			infoLog.Printf("index=%v sleep 1", i)
			time.Sleep(1 * time.Second)
		}
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
