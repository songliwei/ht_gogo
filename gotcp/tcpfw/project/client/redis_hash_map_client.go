package main

import (
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var redisApi *common.RedisApi
var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

const (
	HGetAllCount = 130
)

var (
	ErrDbParam     = errors.New("err param error")
	ErrNilDbObject = errors.New("not set object current is nil")
)

func MultiHgetAllSlice(setName string) (outKeys []string, outValues []string, err error) {
	if redisApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	keyLen, err := redisApi.Hlen(setName)
	if err != nil {
		infoLog.Printf("MultiHgetAllSlice redisApi.Hlen hashName=%s err=%s", setName, err)
		return nil, nil, err
	}
	if keyLen < HGetAllCount {
		kvs, err := redisApi.Hgetall(setName)
		if err != nil {
			infoLog.Printf("MultiHgetAllSlice redisApi.Hgetall setName=%s err=%s", setName, err)
			return nil, nil, err
		} else {
			for k, v := range kvs {
				outKeys = append(outKeys, k)
				outValues = append(outValues, v)
			}
		}
	} else {
		keys, err := redisApi.Hkeys(setName)
		if err != nil {
			infoLog.Printf("MultiHgetAllSlice hashName=%s err=%s", setName, err)
			return nil, nil, err
		}
		keyCount := len(keys)
		if keyCount < HGetAllCount {
			kvs, err := redisApi.Hgetall(setName)
			if err != nil {
				infoLog.Printf("MultiHgetAllSlice redisApi.Hgetall setName=%s err=%s", setName, err)
				return nil, nil, err
			} else {
				for k, v := range kvs {
					outKeys = append(outKeys, k)
					outValues = append(outValues, v)
				}
			}
		}
		// keys 的长度确实大于100 分批获取
		beginIndex := 0
		endIndex := HGetAllCount
		for {
			if endIndex >= keyCount {
				infoLog.Printf("MultiHgetAllSlice hashName=%s endIndex=%v keyCount=%v",
					setName,
					endIndex,
					keyCount)
				endIndex = keyCount
			}
			itemKeys := keys[beginIndex:endIndex]
			itemValues, err := redisApi.Hmget(setName, itemKeys)
			if err != nil {
				infoLog.Printf("MultiHgetAllSlice hashName=%s beginIndex=%v endIndex=%v err=%s itemKeys=%v ",
					setName,
					beginIndex,
					endIndex,
					err,
					itemKeys)
				return nil, nil, err
			}
			outKeys = append(outKeys, itemKeys...)
			outValues = append(outValues, itemValues...)
			// beginIndex 往前移动HGetAllCount
			beginIndex += HGetAllCount
			endIndex += HGetAllCount
			if beginIndex >= keyCount {
				infoLog.Printf("MultiHgetAllSlice hashName=%s keyCount=%v beginIndex=%v complete",
					setName,
					keyCount,
					beginIndex)
				break
			}
		}
	}
	return outKeys, outValues, nil
}

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis pool
	redisIp := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis ip=%v port=%v", redisIp, redisPort)

	redisApi = common.NewRedisApi(fmt.Sprintf("%s:%v", redisIp, redisPort))

	outKeys, outValues, err := MultiHgetAllSlice("5547472#gmsg")
	infoLog.Printf("outKeysLen=%v outValues=%v err=%s", len(outKeys), len(outValues), err)
	for i, v := range outKeys {
		infoLog.Printf("index=%v keys=%v", i, v)
	}

	for i, v := range outValues {
		infoLog.Printf("index=%v values=%v", i, v)
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
