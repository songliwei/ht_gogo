package main

import (
	// "github.com/bitly/go-simplejson"

	"log"
	"net"
	"os"
	"strconv"
	"strings"

	"github.com/gansidui/gotcp/tcpfw/common"
	"github.com/gansidui/gotcp/tcpfw/include/ht_moment"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var infoLog *log.Logger

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	checkError(err)

	uidStr := cfg.Section("uidstr").Key("value").MustString("")
	uidSlic := strings.Split(uidStr, ",")
	headV2Protocol := &common.HeadV2Protocol{}
	reqBody := new(ht_moment.ReqBody)
	head := &common.HeadV2{Cmd: uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INFO),
		Len: uint32(common.EmptyPacktV2Len),
		Uid: uint32(2325928),
	}

	for i, v := range uidSlic {
		infoLog.Printf("index=%v uid=%s", i, v)
		if i%30 == 0 {
			infoLog.Printf("index=%v sleep 1 second", i)
			time.Sleep(1 * time.Second)
		}
		uidElem, err := strconv.ParseUint(v, 10, 32)
		if err != nil {
			infoLog.Printf("index=%v uid=%v strconv.ParseUit err=%s", i, v, err)
			continue
		}
		subReqBody := &ht_moment.GetUserInfoReqBody{
			Userid: proto.Uint32(uint32(uidElem)),
		}
		reqBody.GetUserInfoReqbody = subReqBody
		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			infoLog.Printf("marshaling uid=%v error=%s", v, err)
		}

		head.Len = uint32(common.PacketV2HeadLen) + uint32(len(payLoad)) + 1
		buf := make([]byte, head.Len)
		buf[0] = common.HTV2MagicBegin
		err = common.SerialHeadV2ToSlice(head, buf[1:])
		if err != nil {
			infoLog.Println("SerialHeadV3ToSlice failed uid=%v", v)
			continue
		}
		copy(buf[common.PacketV2HeadLen:], payLoad)
		buf[head.Len-1] = common.HTV2MagicEnd
		// write
		conn.Write(common.NewHeadV2Packet(buf).Serialize())

		// read
		p, err := headV2Protocol.ReadPacket(conn)
		if err == nil {
			packet := p.(*common.HeadV2Packet)
			// infoLog.Printf("Server reply:[%v] [%v]\n", headV2Packet.GetLength(), string(headV2Packet.GetBody()))
			rspHead, err := packet.GetHead()
			if err == nil {
				rspBody := &ht_moment.RspBody{}
				err = proto.Unmarshal(packet.GetBody(), rspBody)
				if err != nil {
					infoLog.Println("proto Unmarshal failed")
					return
				}
				switch rspHead.Cmd {
				case uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INFO):
					{
						subRspBody := rspBody.GetGetUserInfoRspbody()
						infoLog.Printf("stat=%v msg=%v\n", subRspBody.GetStatus().GetCode(), subRspBody.GetStatus().GetReason())
					}
				default:
					{
						infoLog.Println("Unknow cmd=", rspHead.Cmd)
					}
				}
			} else {
				infoLog.Println("get HTV2Head failed err =", err)
			}
		} else {
			infoLog.Printf("index=%v uid=%v ReadPacket failed err=%s", i, v, err)
		}
	}
	return
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
