package main

import (
	// "github.com/bitly/go-simplejson"
	// "database/sql"

	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	"gopkg.in/olivere/elastic.v3"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ClientConf string `short:"c" long:"conf" description:"Clinet Config" optional:"no"`
}

var options Options
var parser = flags.NewParser(&options, flags.Default)

var (
	ErrNilDbObject = errors.New("not set  object current is nil")
	ErrDbParam     = errors.New("err param error")
)

var (
	infoLog *log.Logger
	db      *sql.DB
)

const (
	Terminalios     = 0
	TerminalAndroid = 1
)

const (
	//HelloTalk使用时长
	AppUseDuration                      = "app_use_duration"
	AppUseCount                         = "app_use_count"
	AppUseUserNumber                    = "app_use_user_number"
	AppUseDurationAvgUserNumber         = "app_use_duration_avg_user_number"
	AppUseDurationAvgCount              = "app_use_duration_avg_count"
	AppUseCountDurationLess1Min         = "app_use_count_duration_less_1_min"
	AppUseUserNumberDurationLess1Min    = "app_use_user_number_duration_less_1_min"
	AppUseCountDurationBt1To5Min        = "app_use_count_duration_bt_1_5_min"
	AppUseUserNumberDurationBt1To5Min   = "app_use_user_number_duration_bt_1_5_min"
	AppUseCountDurationBt5To10Min       = "app_use_count_duration_bt_5_10_min"
	AppUseUserNumberDurationBt5To10Min  = "app_use_user_number_duration_bt_5_10_min"
	AppUseCountDurationBt10To15Min      = "app_use_count_duration_bt_10_15_min"
	AppUseUserNumberDurationBt10To15Min = "app_use_user_number_duration_bt_10_15_min"
	AppUseCountDurationBt15To30Min      = "app_use_count_duration_bt_15_30_min"
	AppUseUserNumberDurationBt15To30Min = "app_use_user_number_duration_bt_15_30_min"
	AppUseCountDurationBt30To60Min      = "app_use_count_duration_bt_30_60_min"
	AppUseUserNumberDurationBt30To60Min = "app_use_user_number_duration_bt_30_60_min"
	AppUseCountDurationMore60Min        = "app_use_count_duration_more_60_min"
	AppUseUserNumberDurationMore60Min   = "app_use_user_number_duration_more_60_min"

	//HelloTalk Tab使用时长
	ChatTabDuration                      = "chat_tab_duration"
	ChatTabCount                         = "chat_tab_count"
	ChatTabUserNumber                    = "chat_tab_user_number"
	ChatTabDurationAvgUserNumber         = "chat_tab_duration_avg_user_number"
	ChatTabDurationAvgCount              = "chat_tab_duration_avg_count"
	ChatTabCountDurationLess1Min         = "chat_tab_count_duration_less_1_min"
	ChatTabUserNumberDurationLess1Min    = "chat_tab_user_number_duration_less_1_min"
	ChatTabCountDurationBt1To5Min        = "chat_tab_count_duration_bt_1_5_min"
	ChatTabUserNumberDurationBt1To5Min   = "chat_tab_user_number_duration_bt_1_5_min"
	ChatTabCountDurationBt5To10Min       = "chat_tab_count_duration_bt_5_10_min"
	ChatTabUserNumberDurationBt5To10Min  = "chat_tab_user_number_duration_bt_5_10_min"
	ChatTabCountDurationBt10To15Min      = "chat_tab_count_duration_bt_10_15_min"
	ChatTabUserNumberDurationBt10To15Min = "chat_tab_user_number_duration_bt_10_15_min"
	ChatTabCountDurationBt15To30Min      = "chat_tab_count_duration_bt_15_30_min"
	ChatTabUserNumberDurationBt15To30Min = "chat_tab_user_number_duration_bt_15_30_min"
	ChatTabCountDurationBt30To60Min      = "chat_tab_count_duration_bt_30_60_min"
	ChatTabUserNumberDurationBt30To60Min = "chat_tab_user_number_duration_bt_30_60_min"
	ChatTabCountDurationMore60Min        = "chat_tab_count_duration_more_60_min"
	ChatTabUserNumberDurationMore60Min   = "chat_tab_user_number_duration_more_60_min"

	//Search Tab 使用时长
	SearchTabDuration                      = "search_tab_duration"
	SearchTabCount                         = "search_tab_count"
	SearchTabUserNumber                    = "search_tab_user_number"
	SearchTabAvgUserNumber                 = "search_tab_duration_avg_user_number"
	SearchTabDurationAvgCount              = "search_tab_duration_avg_count"
	SearchTabCountDurationLess1Min         = "search_tab_count_duration_less_1_min"
	SearchTabUserNumberDurationLess1Min    = "search_tab_user_number_duration_less_1_min"
	SearchTabCountDurationBt1To5Min        = "search_tab_count_duration_bt_1_5_min"
	SearchTabUserNumberDurationBt1To5Min   = "search_tab_user_number_duration_bt_1_5_min"
	SearchTabCountDurationBt5To10Min       = "search_tab_count_duration_bt_5_10_min"
	SearchTabUserNumberDurationBt5To10Min  = "search_tab_user_number_duration_bt_5_10_min"
	SearchTabCountDurationBt10To15Min      = "search_tab_count_duration_bt_10_15_min"
	SearchTabUserNumberDurationBt10To15Min = "search_tab_user_number_duration_bt_10_15_min"
	SearchTabCountDurationBt15To30Min      = "search_tab_count_duration_bt_15_30_min"
	SearchTabUserNumberDurationBt15To30Min = "search_tab_user_number_duration_bt_15_30_min"
	SearchTabCountDurationBt30To60Min      = "search_tab_count_duration_bt_30_60_min"
	SearchTabUserNumberDurationBt30To60Min = "search_tab_user_number_duration_bt_30_60_min"
	SearchTabCountDurationMore60Min        = "search_tab_count_duration_more_60_min"
	SearchTabUserNumberDurationMore60Min   = "search_tab_user_number_duration_more_60_min"

	//Moments使用时长
	MomentsTabDuration                      = "moments_tab_duration"
	MomentsTabCount                         = "moments_tab_count"
	MomentsTabUserNumber                    = "moments_tab_user_number"
	MomentsTabDurationAvgUserNumber         = "moments_tab_duration_avg_user_number"
	MomentsTabDurationAvgCount              = "moments_tab_duration_avg_count"
	MomentsTabCountDurationLess1Min         = "moments_tab_count_duration_less_1_min"
	MomentsTabUserNumberDurationLess1Min    = "moments_tab_user_number_duration_less_1_min"
	MomentsTabCountDurationBt1To5Min        = "moments_tab_count_duration_bt_1_5_min"
	MomentsTabUserNumberDurationBt1To5Min   = "moments_tab_user_number_duration_bt_1_5_min"
	MomentsTabCountDurationBt5To10Min       = "moments_tab_count_duration_bt_5_10_min"
	MomentsTabUserNumberDurationBt5To10Min  = "moments_tab_user_number_duration_bt_5_10_min"
	MomentsTabCountDurationBt10To15Min      = "moments_tab_count_duration_bt_10_15_min"
	MomentsTabUserNumberDurationBt10To15Min = "moments_tab_user_number_duration_bt_10_15_min"
	MomentsTabCountDurationBt15To30Min      = "moments_tab_count_duration_bt_15_30_min"
	MomentsTabUserNumberDurationBt15To30Min = "moments_tab_user_number_duration_bt_15_30_min"
	MomentsTabCountDurationBt30To60Min      = "moments_tab_count_duration_bt_30_60_min"
	MomentsTabUserNumberDurationBt30To60Min = "moments_tab_user_number_duration_bt_30_60_min"
	MomentsTabCountDurationMore60Min        = "moments_tab_count_duration_more_60_min"
	MomentsTabUserNumberDurationMore60Min   = "moments_tab_user_number_duration_more_60_min"

	//使用Default 页面的时长
	MomentsDefaultDuration                      = "moments_default_duration"
	MomentsDefaultCount                         = "moments_default_count"
	MomentsDefaultUserNumber                    = "moments_default_user_number"
	MomentsDefaultDurationAvgUserNumber         = "moments_default_duration_avg_user_number"
	MomentsDefaultDurationAvgCount              = "moments_default_duration_avg_count"
	MomentsDefaultCountDurationLess1Min         = "moments_default_count_duration_less_1_min"
	MomentsDefaultUserNumberDurationLess1Min    = "moments_default_user_number_duration_less_1_min"
	MomentsDefaultCountDurationBt1To5Min        = "moments_default_count_duration_bt_1_5_min"
	MomentsDefaultUserNumberDurationBt1To5Min   = "moments_default_user_number_duration_bt_1_5_min"
	MomentsDefaultCountDurationBt5To10Min       = "moments_default_count_duration_bt_5_10_min"
	MomentsDefaultUserNumberDurationBt5To10Min  = "moments_default_user_number_duration_bt_5_10_min"
	MomentsDefaultCountDurationBt10To15Min      = "moments_default_count_duration_bt_10_15_min"
	MomentsDefaultUserNumberDurationBt10To15Min = "moments_default_user_number_duration_bt_10_15_min"
	MomentsDefaultCountDurationBt15To30Min      = "moments_default_count_duration_bt_15_30_min"
	MomentsDefaultUserNumberDurationBt15To30Min = "moments_default_user_number_duration_bt_15_30_min"
	MomentsDefaultCountDurationBt30To60Min      = "moments_default_count_duration_bt_30_60_min"
	MomentsDefaultUserNumberDurationBt30To60Min = "moments_default_user_number_duration_bt_30_60_min"
	MomentsDefaultCountDurationMore60Min        = "moments_default_count_duration_more_60_min"
	MomentsDefaultUserNumberDurationMore60Min   = "moments_default_user_number_duration_more_60_min"

	//使用Learn 页面的时长
	MomentsLearnDuration                      = "moments_learn_duration"
	MomentsLearnCount                         = "moments_learn_count"
	MomentsLearnUserNumber                    = "moments_learn_user_number"
	MomentsLearnDurationAvgUserNumber         = "moments_learn_duration_avg_user_number"
	MomentsLearnDurationAvgCount              = "moments_learn_duration_avg_count"
	MomentsLearnCountDurationLess1Min         = "moments_learn_count_duration_less_1_min"
	MomentsLearnUserNumberDurationLess1Min    = "moments_learn_user_number_duration_less_1_min"
	MomentsLearnCountDurationBt1To5Min        = "moments_learn_count_duration_bt_1_5_min"
	MomentsLearnUserNumberDurationBt1To5Min   = "moments_learn_user_number_duration_bt_1_5_min"
	MomentsLearnCountDurationBt5To10Min       = "moments_learn_count_duration_bt_5_10_min"
	MomentsLearnUserNumberDurationBt5To10Min  = "moments_learn_user_number_duration_bt_5_10_min"
	MomentsLearnCountDurationBt10To15Min      = "moments_learn_count_duration_bt_10_15_min"
	MomentsLearnUserNumberDurationBt10To15Min = "moments_learn_user_number_duration_bt_10_15_min"
	MomentsLearnCountDurationBt15To30Min      = "moments_learn_count_duration_bt_15_30_min"
	MomentsLearnUserNumberDurationBt15To30Min = "moments_learn_user_number_duration_bt_15_30_min"
	MomentsLearnCountDurationBt30To60Min      = "moments_learn_count_duration_bt_30_60_min"
	MomentsLearnUserNumberDurationBt30To60Min = "moments_learn_user_number_duration_bt_30_60_min"
	MomentsLearnCountDurationMore60Min        = "moments_learn_count_duration_more_60_min"
	MomentsLearnUserNumberDurationMore60Min   = "moments_learn_user_number_duration_more_60_min"
	//使用Follow页面的时长
	MomentsFollowingDuration                      = "moments_following_duration"
	MomentsFollowingCount                         = "moments_following_count"
	MomentsFollowingUserNumber                    = "moments_following_user_number"
	MomentsFollowingDurationAvgUserNumber         = "moments_following_duration_avg_user_number"
	MomentsFollowingDurationAvgCount              = "moments_following_duration_avg_count"
	MomentsFollowingCountDurationLess1Min         = "moments_following_count_duration_less_1_min"
	MomentsFollowingUserNumberDurationLess1Min    = "moments_following_user_number_duration_less_1_min"
	MomentsFollowingCountDurationBt1To5Min        = "moments_following_count_duration_bt_1_5_min"
	MomentsFollowingUserNumberDurationBt1To5Min   = "moments_following_user_number_duration_bt_1_5_min"
	MomentsFollowingCountDurationBt5To10Min       = "moments_following_count_duration_bt_5_10_min"
	MomentsFollowingUserNumberDurationBt5To10Min  = "moments_following_user_number_duration_bt_5_10_min"
	MomentsFollowingCountDurationBt10To15Min      = "moments_following_count_duration_bt_10_15_min"
	MomentsFollowingUserNumberDurationBt10To15Min = "moments_following_user_number_duration_bt_10_15_min"
	MomentsFollowingCountDurationBt15To30Min      = "moments_following_count_duration_bt_15_30_min"
	MomentsFollowingUserNumberDurationBt15To30Min = "moments_following_user_number_duration_bt_15_30_min"
	MomentsFollowingCountDurationBt30To60Min      = "moments_following_count_duration_bt_30_60_min"
	MomentsFollowingUserNumberDurationBt30To60Min = "moments_following_user_number_duration_bt_30_60_min"
	MomentsFollowingCountDurationMore60Min        = "moments_following_count_duration_more_60_min"
	MomentsFollowingUserNumberDurationMore60Min   = "moments_following_user_number_duration_more_60_min"
	//使用ClassMate页面时长
	MomentsClassMatesDuration                      = "moments_classmates_duration"
	MomentsClassMatesCount                         = "moments_classmates_count"
	MomentsClassMatesUserNumber                    = "moments_classmates_user_number"
	MomentsClassMatesDurationAvgUserNumber         = "moments_classmates_duration_avg_user_number"
	MomentsClassMatesDurationAvgCount              = "moments_classmates_duration_avg_count"
	MomentsClassMatesCountDurationLess1Min         = "moments_classmates_count_duration_less_1_min"
	MomentsClassMatesUserNumberDurationLess1Min    = "moments_classmates_user_number_duration_less_1_min"
	MomentsClassMatesCountDurationBt1To5Min        = "moments_classmates_count_duration_bt_1_5_min"
	MomentsClassMatesUserNumberDurationBt1To5Min   = "moments_classmates_user_number_duration_bt_1_5_min"
	MomentsClassMatesCountDurationBt5To10Min       = "moments_classmates_count_duration_bt_5_10_min"
	MomentsClassMatesUserNumberDurationBt5To10Min  = "moments_classmates_user_number_duration_bt_5_10_min"
	MomentsClassMatesCountDurationBt10To15Min      = "moments_classmates_count_duration_bt_10_15_min"
	MomentsClassMatesUserNumberDurationBt10To15Min = "moments_classmates_user_number_duration_bt_10_15_min"
	MomentsClassMatesCountDurationBt15To30Min      = "moments_classmates_count_duration_bt_15_30_min"
	MomentsClassMatesUserNumberDurationBt15To30Min = "moments_classmates_user_number_duration_bt_15_30_min"
	MomentsClassMatesCountDurationBt30To60Min      = "moments_classmates_count_duration_bt_30_60_min"
	MomentsClassMatesUserNumberDurationBt30To60Min = "moments_classmates_user_number_duration_bt_30_60_min"
	MomentsClassMatesCountDurationMore60Min        = "moments_classmates_count_duration_more_60_min"
	MomentsClassMatesUserNumberDurationMore60Min   = "moments_classmates_user_number_duration_more_60_min"
	//进入MomentDetail 页面时长
	MomentsDetailDuration                      = "moments_detail_duration"
	MomentsDetailCount                         = "moments_detail_count"
	MomentsDetailUserNumber                    = "moments_detail_user_number"
	MomentsDetailDurationAvgUserNumber         = "moments_detail_duration_avg_user_number"
	MomentsDetailDurationAvgCount              = "moments_detail_duration_avg_count"
	MomentsDetailCountDurationLess1Min         = "moments_detail_count_duration_less_1_min"
	MomentsDetailUserNumberDurationLess1Min    = "moments_detail_user_number_duration_less_1_min"
	MomentsDetailCountDurationBt1To5Min        = "moments_detail_count_duration_bt_1_5_min"
	MomentsDetailUserNumberDurationBt1To5Min   = "moments_detail_user_number_duration_bt_1_5_min"
	MomentsDetailCountDurationBt5To10Min       = "moments_detail_count_duration_bt_5_10_min"
	MomentsDetailUserNumberDurationBt5To10Min  = "moments_detail_user_number_duration_bt_5_10_min"
	MomentsDetailCountDurationBt10To15Min      = "moments_detail_count_duration_bt_10_15_min"
	MomentsDetailUserNumberDurationBt10To15Min = "moments_detail_user_number_duration_bt_10_15_min"
	MomentsDetailCountDurationBt15To30Min      = "moments_detail_count_duration_bt_15_30_min"
	MomentsDetailUserNumberDurationBt15To30Min = "moments_detail_user_number_duration_bt_15_30_min"
	MomentsDetailCountDurationBt30To60Min      = "moments_detail_count_duration_bt_30_60_min"
	MomentsDetailUserNumberDurationBt30To60Min = "moments_detail_user_number_duration_bt_30_60_min"
	MomentsDetailCountDurationMore60Min        = "moments_detail_count_duration_more_60_min"
	MomentsDetailUserNumberDurationMore60Min   = "moments_detail_user_number_duration_more_60_min"

	//使用Profile页面的时长
	ProfileTabDuration                      = "profile_tab_duration"
	ProfileTabCount                         = "profile_tab_count"
	ProfileTabUserNumber                    = "profile_tab_user_number"
	ProfileTabDurationAvgUserNumber         = "profile_tab_duration_avg_user_number"
	ProfileTabDurationAvgCount              = "profile_tab_duration_avg_count"
	ProfileTabCountDurationLess1Min         = "profile_tab_count_duration_less_1_min"
	ProfileTabUserNumberDurationLess1Min    = "profile_tab_user_number_duration_less_1_min"
	ProfileTabCountDurationBt1To5Min        = "profile_tab_count_duration_bt_1_5_min"
	ProfileTabUserNumberDurationBt1To5Min   = "profile_tab_user_number_duration_bt_1_5_min"
	ProfileTabCountDurationBt5To10Min       = "profile_tab_count_duration_bt_5_10_min"
	ProfileTabUserNumberDurationBt5To10Min  = "profile_tab_user_number_duration_bt_5_10_min"
	ProfileTabCountDurationBt10To15Min      = "profile_tab_count_duration_bt_10_15_min"
	ProfileTabUserNumberDurationBt10To15Min = "profile_tab_user_number_duration_bt_10_15_min"
	ProfileTabCountDurationBt15To30Min      = "profile_tab_count_duration_bt_15_30_min"
	ProfileTabUserNumberDurationBt15To30Min = "profile_tab_user_number_duration_bt_15_30_min"
	ProfileTabCountDurationBt30To60Min      = "profile_tab_count_duration_bt_30_60_min"
	ProfileTabUserNumberDurationBt30To60Min = "profile_tab_user_number_duration_bt_30_60_min"
	ProfileTabCountDurationMore60Min        = "profile_tab_count_duration_more_60_min"
	ProfileTabUserNumberDurationMore60Min   = "profile_tab_user_number_duration_more_60_min"
)

func GetDayOfMonth(year, month int) int {
	switch month {
	case 1:
		return 31
	case 2:
		if (year%4 == 0 && year%100 != 0) || (year%400 == 0) {
			return 29
		} else {
			return 28
		}
	case 3:
		return 31
	case 4:
		return 30
	case 5:
		return 31
	case 6:
		return 30
	case 7:
		return 31
	case 8:
		return 31
	case 9:
		return 30
	case 10:
		return 31
	case 11:
		return 30
	case 12:
		return 31
	default:
		return 30
	}
}

// 1.GetUserHellotalk
func GetUseHellotalkTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000) // 将毫秒转成秒
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}

func GetiOSUseHellotalkTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}

func UpdateAppUsageStaticInDb(osType uint32, iterm string, count float64) (err error) {
	if db == nil {
		return ErrNilDbObject
	}

	if len(iterm) == 0 {
		return ErrDbParam
	}
	timeNow := time.Now()
	timeThen := timeNow.Add(-24 * time.Hour)
	targetYear, targetMonth, targetDay := timeThen.Date()
	yesterday := fmt.Sprintf("%4d%02d%02d", targetYear, targetMonth, targetDay)
	infoLog.Printf("UpdateAppUsageStaticInDb yesterday=%s", yesterday)
	_, err = db.Exec("INSERT INTO mg_app_usage_statist(date, ostype, name, count) VALUES (?, ?, ?, ?);",
		yesterday,
		osType,
		iterm,
		count)
	if err != nil {
		infoLog.Printf("insert into mg_app_usage_statist faield err=%v", err)
		return err
	} else {
		return nil
	}
}
func GetAndroidUseHellotalkTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}

func GetiOSUseHellotalkDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}
	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), AppUseUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkDistribute update AppUseUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

func GetAndroidUseHellotalkDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute client.Search get totalOneMinuteCountQuery failed err=%s", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("GetAndroidUseHellotalkDistribute client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("GetAndroidUseHellotalkDistribute totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}
	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_total AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), AppUseUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkDistribute update AppUseUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 2.GetUserHelloTalkTab
func GetUseHellotalkTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseHellotalkTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)
	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseHellotalkTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)
	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseHellotalkTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ChatTabUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseHellotalkTabDistribute update ChatTabUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseHellotalkTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND NOT erminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_hellotalk_tab AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_tab Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ChatTabUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseHellotalkTabDistribute update ChatTabUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 3.GetUseSearchTab
func GetUseSearchTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_search_tab")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_search_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseSearchTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_search_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseSearchTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_search_tab Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_search_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseSearchTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}
	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), SearchTabUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseSearchTabDistribute update SearchTabUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseSearchTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}
	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_search_tab AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_search_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_search_tab Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), SearchTabUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseSearchTabDistribute update SearchTabUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 4.GetUseMomentTab
func GetUseMomentTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseMomentTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseMomentTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseMomentTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabCountDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsTabUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentTabDistribute update MomentsTabUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseMomentTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabCountDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_tab AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_tab Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsTabUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentTabDistribute update MomentsTabUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 5.GetUseMomentDefaultTab
func GetUseMomentDefaultTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseMomentDefaultTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseMomentDefaultTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseMomentDefaultTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}

	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}

	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDefaultUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseMomentDefaultTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}

	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}

	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_default_tab AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_default_tab Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDefaultUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDefaultTabDistribute update MomentsDefaultUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 6.GetUseMomentLearnTab
func GetUseMomentLearnTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseMomentLearnTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseMomentLearnTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseMomentLearnTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}
	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}

	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}

	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsLearnUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseMomentLearnTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}
	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}

	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}

	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_learn_tab AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_learn_tab Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsLearnUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentLearnTabDistribute update MomentsLearnUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 7.GetUseMomentFollowingTab
func GetUseMomentFollowingTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseMomentFollowingTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseMomentFollowingTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)

	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseMomentFollowingTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}

	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsFollowingUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseMomentFollowingTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}

	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_following_tab AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_following_tab Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsFollowingUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentFollowingTabDistribute update MomentsFollowingUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 8.GetUseMomentClassmateTab
func GetUseMomentClassmateTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseMomentClassmateTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)
	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseMomentClassmateTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)
	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseMomentClassmateTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}

	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsClassMatesUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseMomentClassmateTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}

	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}

	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}

	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}

	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}

	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_classmates_tab AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_classmates_tab Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsClassMatesUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentClassmateTabDistribute update MomentsClassMatesUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 9.GetUseMomentDetailTab
func GetUseMomentDetailTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_detail get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseMomentDetailTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_detail get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)
	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseMomentDetailTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_moment_detail get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))

	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)
	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseMomentDetailTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}
	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}
	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), MomentsDetailUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseMomentDetailTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}
	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}
	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_moment_detail AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_moment_detail iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_moment_detail Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), MomentsDetailUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseMomentDetailTabDistribute update MomentsDetailUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

// 10.GetUseOtherProfileTab
func GetUseOtherProfileTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_hellotalk_total totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
}
func GetiOSUseOtherProfileTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)
	// update db
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetAndroidUseOtherProfileTabTotal(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	sumAgg := elastic.NewSumAggregation()
	sumAgg = sumAgg.Field("cost_u64")

	strTotalQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND NOT terminaltype:0")
	strTotalQuery = strTotalQuery.AnalyzeWildcard(true)
	totalTimeQuery := elastic.NewBoolQuery()
	totalTimeQuery = totalTimeQuery.Must(strTotalQuery)
	totalTimeQuery = totalTimeQuery.Filter(boolQuery)
	searchTimeResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTimeQuery).    // return all results, but ...
		Aggregation("1", sumAgg). // add our aggregation to the query
		Pretty(true).             // pretty print request and response JSON
		Do()                      // execute
	if err != nil {
		infoLog.Println("client.Search failed err=", err)
		return
	}
	var totalTime float64
	if sumAggResult, ok := searchTimeResult.Aggregations.Sum("1"); ok {
		totalTime = *sumAggResult.Value / float64(1000)
		// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Andirod totalTime=%v", strTargetDay, totalTime)
	} else {
		infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab get sumAggResult failed", strTargetDay)
	}
	// infoLog.Printf("================================================================================================")
	totalCountQuery := elastic.NewBoolQuery()
	totalCountQuery = totalCountQuery.Must(strTotalQuery)
	totalCountQuery = totalCountQuery.Filter(boolQuery)
	totalCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalCountQuery). // return all results, but ...
		Pretty(true).           // pretty print request and response JSON
		Do()                    // execute
	if err != nil {
		infoLog.Println("client.Search get totalCount failed err=", err)
		return
	}
	totalCount := totalCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Andirod totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalUserAgg := elastic.NewCardinalityAggregation()
	totalUserAgg = totalUserAgg.Field("userid.raw")
	totalUserQuery := elastic.NewBoolQuery()
	totalUserQuery = totalUserQuery.Must(strTotalQuery)
	totalUserQuery = totalUserQuery.Filter(boolQuery)
	totalUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalUserQuery).          // return all results, but ...
		Aggregation("1", totalUserAgg). // add our aggregation to the query
		Pretty(true).                   // pretty print request and response JSON
		Do()                            // execute
	if err != nil {
		infoLog.Println("client.Search get totalUser failed err=", err)
		return
	}
	aggValueMetric, ok := totalUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalUser := *(aggValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Andirod totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Andirod 总时长=%v\t总次数=%v\t总人数=%v\t人均时长=%v\t次均时长=%v",
		strTargetDay,
		totalTime,
		totalCount,
		totalUser,
		totalTime/float64(totalUser),
		totalTime/float64(totalCount))
	userAvgCount := totalTime / float64(totalUser)
	durationAvgCount := totalTime / float64(totalCount)
	// update db
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabDuration, totalTime)
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabTotal update totalTime=%v failed", totalTime)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabCount, float64(totalCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabTotal update totalCount=%v failed", totalCount)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabUserNumber, float64(totalUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabTotal update totalUser=%v failed", totalUser)
	}

	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabDurationAvgUserNumber, userAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabTotal update userAvgCount=%v failed", userAvgCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabDurationAvgCount, durationAvgCount)
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabTotal update durationAvgCount=%v failed", durationAvgCount)
	}
}
func GetiOSUseOtherProfileTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}
	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}
	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(Terminalios), ProfileTabUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetiOSUseOtherProfileTabDistribute update ProfileTabUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}
func GetAndroidUseOtherProfileTabDistribute(client *elastic.Client, year, month, day int) {
	strTargetDay := fmt.Sprintf("%4d.%02d.%02d", year, month, day)
	tsBegin := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local).Unix()
	tsBegin = tsBegin * 1000 // change to ms
	tsEnd := time.Date(year, time.Month(month), day, 23, 59, 59, 999999999, time.Local).Unix()
	tsEnd = tsEnd * 1000 // change to ms
	// infoLog.Printf("tsBegin=%v tsEnd=%v", tsBegin, tsEnd)

	indexBegin := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day-1)

	indexEnd := fmt.Sprintf("logstash-client-log-%4d.%02d.%02d", year, month, day)
	// infoLog.Printf("indexBegegin=%s indexEnd=%s", indexBegin, indexEnd)
	rangeQuery := elastic.NewRangeQuery("@timestamp")
	rangeQuery = rangeQuery.Gte(tsBegin)
	rangeQuery = rangeQuery.Lte(tsEnd)
	rangeQuery = rangeQuery.Format("epoch_millis")
	boolQuery := elastic.NewBoolQuery()
	boolQuery = boolQuery.Must(rangeQuery)

	strOneMinuteQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND NOT terminaltype:0 AND cost_u64:<=60000")
	strOneMinuteQuery = strOneMinuteQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteCountQuery := elastic.NewBoolQuery()
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Must(strOneMinuteQuery)
	totalOneMinuteCountQuery = totalOneMinuteCountQuery.Filter(boolQuery)
	totalOneMinuteCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteCountQuery failed err=", err)
		return
	}
	totalOneMinuteCount := totalOneMinuteCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneMinuteUserAgg := elastic.NewCardinalityAggregation()
	totalOneMinuteUserAgg = totalOneMinuteUserAgg.Field("userid.raw")
	totalOneMinuteUserQuery := elastic.NewBoolQuery()
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Must(strOneMinuteQuery)
	totalOneMinuteUserQuery = totalOneMinuteUserQuery.Filter(boolQuery)
	totalOneMinuteUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneMinuteUserQuery).          // return all results, but ...
		Aggregation("1", totalOneMinuteUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneMinuteUser failed err=", err)
		return
	}
	aggOneMinuteValueMetric, ok := totalOneMinuteUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneMinuteUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneMinuteUser := *(aggOneMinuteValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Android [0min, 1min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneMinuteCount,
		totalOneMinuteUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabCountDurationLess1Min, float64(totalOneMinuteCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabCountDurationLess1Min=%v failed", totalOneMinuteCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabUserNumberDurationLess1Min, float64(totalOneMinuteUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabUserNumberDurationLess1Min=%v failed", totalOneMinuteUser)
	}
	// one to five minute static
	// 1到5分钟
	strOneToFiveQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND NOT terminaltype:0 AND cost_u64:[60000 TO 300000}")
	strOneToFiveQuery = strOneToFiveQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveCountQuery := elastic.NewBoolQuery()
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Must(strOneToFiveQuery)
	totalOneToFiveCountQuery = totalOneToFiveCountQuery.Filter(boolQuery)
	totalOneToFiveCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveCountQuery failed err=", err)
		return
	}
	totalOneToFiveCount := totalOneToFiveCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalOneToFiveUserAgg := elastic.NewCardinalityAggregation()
	totalOneToFiveUserAgg = totalOneToFiveUserAgg.Field("userid.raw")
	totalOneToFiveUserQuery := elastic.NewBoolQuery()
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Must(strOneToFiveQuery)
	totalOneToFiveUserQuery = totalOneToFiveUserQuery.Filter(boolQuery)
	totalOneToFiveUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalOneToFiveUserQuery).          // return all results, but ...
		Aggregation("1", totalOneToFiveUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalOneToFiveUser failed err=", err)
		return
	}
	aggOneToFiveValueMetric, ok := totalOneToFiveUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalOneToFiveUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalOneToFiveUser := *(aggOneToFiveValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Android [1min, 5min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalOneToFiveCount,
		totalOneToFiveUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabCountDurationBt1To5Min, float64(totalOneToFiveCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabCountDurationBt1To5Min=%v failed", totalOneToFiveCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabUserNumberDurationBt1To5Min, float64(totalOneToFiveUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt1To5Min=%v failed", totalOneToFiveUser)
	}
	// five to ten minute static
	// 5到10分钟
	strFiveTOTenQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND NOT terminaltype:0 AND cost_u64:[300000 TO 600000}")
	strFiveTOTenQuery = strFiveTOTenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenCountQuery := elastic.NewBoolQuery()
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenCountQuery = totalFiveTOTenCountQuery.Filter(boolQuery)
	totalFiveTOTenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenCountQuery). // return all results, but ...
		Pretty(true).                    // pretty print request and response JSON
		Do()                             // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenCountQuery failed err=", err)
		return
	}
	totalFiveTOTenCount := totalFiveTOTenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiveTOTenUserAgg := elastic.NewCardinalityAggregation()
	totalFiveTOTenUserAgg = totalFiveTOTenUserAgg.Field("userid.raw")
	totalFiveTOTenUserQuery := elastic.NewBoolQuery()
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Must(strFiveTOTenQuery)
	totalFiveTOTenUserQuery = totalFiveTOTenUserQuery.Filter(boolQuery)
	totalFiveTOTenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiveTOTenUserQuery).          // return all results, but ...
		Aggregation("1", totalFiveTOTenUserAgg). // add our aggregation to the query
		Pretty(true).                            // pretty print request and response JSON
		Do()                                     // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiveTOTenUser failed err=", err)
		return
	}
	aggFiveTOTenValueMetric, ok := totalFiveTOTenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiveTOTenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiveTOTenUser := *(aggFiveTOTenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Android [5min, 10min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiveTOTenCount,
		totalFiveTOTenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabCountDurationBt5To10Min, float64(totalFiveTOTenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabCountDurationBt5To10Min=%v failed", totalFiveTOTenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabUserNumberDurationBt5To10Min, float64(totalFiveTOTenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt5To10Min=%v failed", totalFiveTOTenUser)
	}
	// ten to fiften minute static
	// 10到15分钟
	strTenToFiftenQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND NOT terminaltype:0 AND cost_u64:[600000 TO 900000}")
	strTenToFiftenQuery = strTenToFiftenQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenCountQuery := elastic.NewBoolQuery()
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenCountQuery = totalTenToFiftenCountQuery.Filter(boolQuery)
	totalTenToFiftenCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenCountQuery). // return all results, but ...
		Pretty(true).                      // pretty print request and response JSON
		Do()                               // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenCountQuery failed err=", err)
		return
	}
	totalTenToFiftenCount := totalTenToFiftenCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalTenToFiftenUserAgg := elastic.NewCardinalityAggregation()
	totalTenToFiftenUserAgg = totalTenToFiftenUserAgg.Field("userid.raw")
	totalTenToFiftenUserQuery := elastic.NewBoolQuery()
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Must(strTenToFiftenQuery)
	totalTenToFiftenUserQuery = totalTenToFiftenUserQuery.Filter(boolQuery)
	totalTenToFiftenUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalTenToFiftenUserQuery).          // return all results, but ...
		Aggregation("1", totalTenToFiftenUserAgg). // add our aggregation to the query
		Pretty(true).                              // pretty print request and response JSON
		Do()                                       // execute
	if err != nil {
		infoLog.Println("client.Search get totalTenToFiftenUser failed err=", err)
		return
	}
	aggTenToFiftenValueMetric, ok := totalTenToFiftenUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalTenToFiftenUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalTenToFiftenUser := *(aggTenToFiftenValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Android [10min, 15min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalTenToFiftenCount,
		totalTenToFiftenUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabCountDurationBt10To15Min, float64(totalTenToFiftenCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabCountDurationBt10To15Min=%v failed", totalTenToFiftenCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabUserNumberDurationBt10To15Min, float64(totalTenToFiftenUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt10To15Min=%v failed", totalTenToFiftenUser)
	}
	// fiften to thirty minute static
	// 15到30分钟
	strFiftenToThirtyQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND NOT terminaltype:0 AND cost_u64:[900000 TO 1800000}")
	strFiftenToThirtyQuery = strFiftenToThirtyQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyCountQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyCountQuery = totalFiftenToThirtyCountQuery.Filter(boolQuery)
	totalFiftenToThirtyCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyCountQuery failed err=", err)
		return
	}
	totalFiftenToThirtyCount := totalFiftenToThirtyCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalFiftenToThirtyUserAgg := elastic.NewCardinalityAggregation()
	totalFiftenToThirtyUserAgg = totalFiftenToThirtyUserAgg.Field("userid.raw")
	totalFiftenToThirtyUserQuery := elastic.NewBoolQuery()
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Must(strFiftenToThirtyQuery)
	totalFiftenToThirtyUserQuery = totalFiftenToThirtyUserQuery.Filter(boolQuery)
	totalFiftenToThirtyUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalFiftenToThirtyUserQuery).          // return all results, but ...
		Aggregation("1", totalFiftenToThirtyUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalFiftenToThirtyUser failed err=", err)
		return
	}
	aggFiftenToThirtyValueMetric, ok := totalFiftenToThirtyUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalFiftenToThirtyUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalFiftenToThirtyUser := *(aggFiftenToThirtyValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Android [15min, 30min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalFiftenToThirtyCount,
		totalFiftenToThirtyUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabCountDurationBt15To30Min, float64(totalFiftenToThirtyCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabCountDurationBt15To30Min=%v failed", totalFiftenToThirtyCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabUserNumberDurationBt15To30Min, float64(totalFiftenToThirtyUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt15To30Min=%v failed", totalFiftenToThirtyUser)
	}
	// thirty to one hour minute static
	// 30到60分钟
	strThirtyToOneHourQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND NOT terminaltype:0 AND cost_u64:[1800000 TO 3600000}")
	strThirtyToOneHourQuery = strThirtyToOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourCountQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourCountQuery = totalThirtyToOneHourCountQuery.Filter(boolQuery)
	totalThirtyToOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourCountQuery). // return all results, but ...
		Pretty(true).                          // pretty print request and response JSON
		Do()                                   // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourCountQuery failed err=", err)
		return
	}
	totalThirtyToOneHourCount := totalThirtyToOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalThirtyToOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalThirtyToOneHourUserAgg = totalThirtyToOneHourUserAgg.Field("userid.raw")
	totalThirtyToOneHourUserQuery := elastic.NewBoolQuery()
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Must(strThirtyToOneHourQuery)
	totalThirtyToOneHourUserQuery = totalThirtyToOneHourUserQuery.Filter(boolQuery)
	totalThirtyToOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalThirtyToOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalThirtyToOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                  // pretty print request and response JSON
		Do()                                           // execute
	if err != nil {
		infoLog.Println("client.Search get totalThirtyToOneHourUser failed err=", err)
		return
	}
	aggThirtyToOneHourValueMetric, ok := totalThirtyToOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalThirtyToOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalThirtyToOneHourUser := *(aggThirtyToOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Android [30min, 60min] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalThirtyToOneHourCount,
		totalThirtyToOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabCountDurationBt30To60Min, float64(totalThirtyToOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabCountDurationBt30To60Min=%v failed", totalThirtyToOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabUserNumberDurationBt30To60Min, float64(totalThirtyToOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabUserNumberDurationBt30To60Min=%v failed", totalThirtyToOneHourUser)
	}
	// 60分钟以上的
	strBigThenOneHourQuery := elastic.NewQueryStringQuery("cmd:use_other_profile_tab AND NOT terminaltype:0 AND cost_u64:[3600000 TO *]")
	strBigThenOneHourQuery = strBigThenOneHourQuery.AnalyzeWildcard(true)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourCountQuery := elastic.NewBoolQuery()
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourCountQuery = totalBigThenOneHourCountQuery.Filter(boolQuery)
	totalBigThenOneHourCountResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourCountQuery). // return all results, but ...
		Pretty(true).                         // pretty print request and response JSON
		Do()                                  // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourCountQuery failed err=", err)
		return
	}
	totalBigThenOneHourCount := totalBigThenOneHourCountResult.TotalHits()
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalCount=%v", strTargetDay, totalCount)
	// infoLog.Printf("================================================================================================")
	totalBigThenOneHourUserAgg := elastic.NewCardinalityAggregation()
	totalBigThenOneHourUserAgg = totalBigThenOneHourUserAgg.Field("userid.raw")
	totalBigThenOneHourUserQuery := elastic.NewBoolQuery()
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Must(strBigThenOneHourQuery)
	totalBigThenOneHourUserQuery = totalBigThenOneHourUserQuery.Filter(boolQuery)
	totalBigThenOneHourUserResult, err := client.Search().
		Index(indexBegin, indexEnd). // search in indexBegin and indexEnd
		SearchType("count").
		IgnoreUnavailable(true).
		Query(totalBigThenOneHourUserQuery).          // return all results, but ...
		Aggregation("1", totalBigThenOneHourUserAgg). // add our aggregation to the query
		Pretty(true).                                 // pretty print request and response JSON
		Do()                                          // execute
	if err != nil {
		infoLog.Println("client.Search get totalBigThenOneHourUser failed err=", err)
		return
	}
	aggBigThenOneHourValueMetric, ok := totalBigThenOneHourUserResult.Aggregations.ValueCount("1")
	if !ok {
		infoLog.Println("totalBigThenOneHourUserResult totalResult.Aggregations.ValueCount(\"1\") failed")
		return
	}
	totalBigThenOneHourUser := *(aggBigThenOneHourValueMetric.Value)
	// infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab iOS totalUser=%v", strTargetDay, totalUser)
	infoLog.Printf("================================================================================================")
	infoLog.Printf("strTargetDay=%s cmd:use_other_profile_tab Android [60min, *] 总次数=%v\t总人数=%v\t",
		strTargetDay,
		totalBigThenOneHourCount,
		totalBigThenOneHourUser)
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabCountDurationMore60Min, float64(totalBigThenOneHourCount))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabCountDurationMore60Min=%v failed", totalBigThenOneHourCount)
	}
	err = UpdateAppUsageStaticInDb(uint32(TerminalAndroid), ProfileTabUserNumberDurationMore60Min, float64(totalBigThenOneHourUser))
	if err != nil {
		infoLog.Printf("GetAndroidUseOtherProfileTabDistribute update ProfileTabUserNumberDurationMore60Min=%v failed", totalBigThenOneHourUser)
	}
}

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ClientConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ClientConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ClientConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ClientConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/clinet.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s")
	if err != nil {
		infoLog.Println("open mysql failed err=%s", err)
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.3")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustString("9200")

	infoLog.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	// Obtain a client. You can also provide your own HTTP client here.
	client, err := elastic.NewClient(elastic.SetURL("http://"+serverIp+":"+serverPort),
		elastic.SetMaxRetries(10),
		elastic.SetBasicAuth("hellotalk_admin", "hellotalk_admin123456"),
		//elastic.SetTraceLog(infoLog),
		//elastic.SetInfoLog(infoLog),
		elastic.SetErrorLog(infoLog))
	if err != nil {
		// Handle error
		panic(err)
	}

	// Getting the ES version number is quite common, so there's a shortcut
	esversion, err := client.ElasticsearchVersion("http://127.0.0.1:9200")
	if err != nil {
		// Handle error
		panic(err)
	}
	infoLog.Printf("Elasticsearch version %s", esversion)

	// configDate := cfg.Section("CONFIG_DATE").Key("date").MustInt(1)
	// tsNow := time.Now()
	// year, month, day := tsNow.Date()
	// infoLog.Printf("year=%4d month=%2d day=%02d", year, month, day)

	// var targetYear, targetMonth, targetDay int
	// if day-configDate <= 0 {
	// 	targetMonth = int(month) - 1
	// 	if targetMonth <= 0 {
	// 		targetYear = year - 1
	// 		targetMonth = 12
	// 	}
	// 	dayOfLastMonth := GetDayOfMonth(targetYear, targetMonth)
	// 	targetDay = dayOfLastMonth + day - configDate
	// } else {
	// 	targetYear = year
	// 	targetMonth = int(month)
	// 	targetDay = day - configDate
	// }
	//
	ticker := time.NewTicker(time.Hour * 24)
	for range ticker.C {
		timeNow := time.Now()
		timeThen := timeNow.Add(-24 * time.Hour)
		targetYear, month, targetDay := timeThen.Date()
		targetMonth := int(month)
		infoLog.Printf("targetYear=%v targetMonth=%v targetDay=%v", targetYear, targetMonth, targetDay)
		// 1.GetUserHelloTotal
		//GetUseHellotalkTotal(client, targetYear, targetMonth, targetDay)

		GetiOSUseHellotalkTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseHellotalkTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseHellotalkDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseHellotalkDistribute(client, targetYear, targetMonth, targetDay)

		// 2.use_hello_tab
		// GetUseHellotalkTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseHellotalkTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseHellotalkTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseHellotalkTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseHellotalkTabDistribute(client, targetYear, targetMonth, targetDay)

		// 3.GetUseSearchTab
		//GetUseSearchTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseSearchTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseSearchTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseSearchTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseSearchTabDistribute(client, targetYear, targetMonth, targetDay)

		// 4.GetUseMomentTab
		//GetUseMomentTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentTabDistribute(client, targetYear, targetMonth, targetDay)

		// 5.GetUseMomentDefaultTab
		//GetUseMomentDefaultTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentDefaultTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentDefaultTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentDefaultTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentDefaultTabDistribute(client, targetYear, targetMonth, targetDay)

		// 6.GetUseMomentLearnTab
		//GetUseMomentLearnTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentLearnTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentLearnTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentLearnTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentLearnTabDistribute(client, targetYear, targetMonth, targetDay)

		// 7.GetUseMomentFollowingTab
		//GetUseMomentFollowingTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentFollowingTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentFollowingTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentFollowingTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentFollowingTabDistribute(client, targetYear, targetMonth, targetDay)

		// 8.GetUseMomentClassmateTab
		//GetUseMomentClassmateTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentClassmateTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentClassmateTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentClassmateTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentClassmateTabDistribute(client, targetYear, targetMonth, targetDay)

		// 9.GetUseMomentDetailTab
		//GetUseMomentDetailTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentDetailTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentDetailTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseMomentDetailTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseMomentDetailTabDistribute(client, targetYear, targetMonth, targetDay)

		// 10.GetUseOtherProfileTab
		//GetUseOtherProfileTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseOtherProfileTabTotal(client, targetYear, targetMonth, targetDay)
		GetAndroidUseOtherProfileTabTotal(client, targetYear, targetMonth, targetDay)
		GetiOSUseOtherProfileTabDistribute(client, targetYear, targetMonth, targetDay)
		GetAndroidUseOtherProfileTabDistribute(client, targetYear, targetMonth, targetDay)
	}
	// 关闭定时器
	ticker.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
