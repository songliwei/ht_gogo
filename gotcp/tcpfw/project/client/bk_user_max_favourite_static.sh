#!/bin/bash
PWD=`pwd`
FILE="$PWD/user_max_favourite.log"

echo "-----------------------------------------TOTAL-----------------------------------------------------------------"
echo "---------------------------------------------------------------------------------------------------------------"
#calc total
cat $FILE | awk '{print  $10" "$3}' | awk -F "[ =]" '{print $2 " " $4}' | sort -nr | head -10 | awk '{print $2}' > "$PWD/total.log"

while read line
do
#	cat $FILE | grep "\bUID=$line\b" | awk '{print $3" "$4" "$5" "$6" "$7" "$8" "$9 " "$10" "$11" "$12" "$13}'
	cat $FILE | grep "\bUID=$line\b" | awk '{print $3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13}'
done < "$PWD/total.log"

echo "-----------------------------------------TEXT------------------------------------------------------------------"
echo "---------------------------------------------------------------------------------------------------------------"
#TEXT total
cat $FILE | awk '{print $4" "$5" "$3}' | awk -F "[ =]" '{print $2+$4 " " $6}'| sort -nr | head -10 | awk '{print $2}' > "$PWD/text_total.log"

while read line
do
#	cat $FILE | grep "\bUID=$line\b" | awk '{print $3" "$4" "$5" "$6" "$7" "$8" "$9 " "$10" "$11" "$12" "$13}'
	cat $FILE | grep "\bUID=$line\b" | awk '{print $3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13}'
done < "$PWD/text_total.log"

echo "------------------------------------------VOICE----------------------------------------------------------------"
echo "---------------------------------------------------------------------------------------------------------------"
#voice total
cat $FILE | awk '{print $6" "$7" "$3}' | awk -F "[ =]" '{print $2+$4 " " $6}'| sort -nr | head -10 | awk '{print $2}' > "$PWD/voice_total.log"

while read line
do
#	cat $FILE | grep "\bUID=$line\b" | awk '{print $3" "$4" "$5" "$6" "$7" "$8" "$9 " "$10" "$11" "$12" "$13}'
	cat $FILE | grep "\bUID=$line\b" | awk '{print $3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13}'
done < "$PWD/voice_total.log"

echo "------------------------------------------CORRECT--------------------------------------------------------------"
echo "---------------------------------------------------------------------------------------------------------------"
#correct total
cat $FILE | awk '{print $8" "$9" "$3}' | awk -F "[ =]" '{print $2+$4 " " $6}'| sort -nr | head -10 | awk '{print $2}' > "$PWD/correct_total.log"

while read line
do
#	cat $FILE | grep "\bUID=$line\b" | awk '{print $3" "$4" "$5" "$6" "$7" "$8" "$9 " "$10" "$11" "$12" "$13}'
	cat $FILE | grep "\bUID=$line\b" | awk '{print $3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13}'
done < "$PWD/correct_total.log"
