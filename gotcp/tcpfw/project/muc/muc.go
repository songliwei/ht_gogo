package main

import (
	"bytes"
	"compress/zlib"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"
	"sync"
	"unicode/utf8"

	"github.com/HT_GOGO/gotcp"
	simplejson "github.com/bitly/go-simplejson"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc_store"
	"github.com/HT_GOGO/gotcp/tcpfw/project/muc/util"
	nsq "github.com/nsqio/go-nsq"
	mgo "gopkg.in/mgo.v2"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"github.com/streadway/amqp"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog *log.Logger
	db      *sql.DB
	logDb   *sql.DB
	//ssdb        *gossdb.Connectors
	mongoSess         *mgo.Session
	mcApi             *common.MemcacheApi
	offlineApi        *common.OfflineApiV2
	mucStoreApi       *common.MucMsgStoreApi
	lessonMsgStoreApi *common.LessonMsgStoreApi
	DbUtil            *util.DbUtil
	LogDbUtil         *util.LogDbUtil
	roomManager       *util.RoomManager
	ticker            *time.Ticker
	reqRecord         map[string]int64
	reqLock           sync.Mutex // sync mutex goroutines use reqRecord
	rabbitMQUrl       string
	relationApi       *common.RelationApi
	chatRecordApi     *common.ChatRecordApi
	userInfoCacheApi  *common.UserInfoCacheApi
	requestJoinLimit  int
	roomActionTopic   string
	globalProducer    *nsq.Producer
	groupLessonApi    *common.GroupLessonApi
	walletApi         *common.WalletApi
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)

const (
	MUC_MESSAGE_LNE_LIMIT   = 30
	ROOM_ID_FROM_OR_CODE    = 1
	ROOM_ID_FROM_INVITE     = 0
	REQ_THRESHOLD           = 1
	ROOM_NAME_MEMEBER_LIMIE = 3
)

const (
	PERSON_TO_PRESON_MQ_CORRECT_TYPE = "correct_sentence"
	GROUP_CHAT_MQ_CORRECT_TYPE       = "correct_group_sentence"
	RABBIT_SUBMIT                    = "user_submit"
	EXCHANGENAME                     = "push-msg"
	ROUTINGKEY                       = "push-r-1"
)

const (
	AT_INIT_JOIN_ROOM    = 1 //主动加入群聊
	AT_INVITED_JOIN_ROOM = 2 //被邀请加入群聊
	AT_INIT_QUIT_ROOM    = 3 //主动退出群聊
	AT_KICKED_OUT_ROOM   = 4 //被T出群聊
	AT_CREATE_ROOM       = 5 //create群聊
)

// 消息类型
const (
	MT_TEXT         = 0  // 文字
	MT_TRANSLATE    = 1  // 带翻译的文字
	MT_IMAGE        = 2  // 图片
	MT_VOICE        = 3  // 音频
	MT_LOCATE       = 4  // 定位
	MT_PROFILE      = 5  // 用户介绍
	MT_VOICETEXT    = 6  // 语音转文字
	MT_CORRECTION   = 7  // 修改句子
	MT_STICKERS     = 8  // 自定义表情
	MT_DOODLE       = 9  // 涂鸦
	MT_VOIP         = 10 // VOIP？
	MT_NOTIFY       = 11 // 公众消息？
	MT_VIDEO        = 12 // 视频 video
	MT_GVOIP        = 13 // group voip
	MT_LINK         = 14
	MT_CARD         = 15
	MT_START_CHARGE = 16
	MT_STOP_CHARGE  = 17
	MT_UNKNOWN      = 100
)

const (
	REQTHRESHOLD   = 300
	SENDPHOTOLIMIT = 5
)

const (
	ALL_JOIN_ROOM     = 0 // 全部加入群聊
	PORTION_JOIN_ROOM = 1 // 部分加入群聊 需要等待对方加入群聊确认
)

func GetMessageType(strMsgType string) uint8 {
	if 0 == strings.Compare(strMsgType, "text") {
		return MT_TEXT
	} else if 0 == strings.Compare(strMsgType, "translate") {
		return MT_TRANSLATE
	} else if 0 == strings.Compare(strMsgType, "voice") {
		return MT_VOICE
	} else if 0 == strings.Compare(strMsgType, "image") {
		return MT_IMAGE
	} else if 0 == strings.Compare(strMsgType, "introduction") {
		return MT_PROFILE
	} else if 0 == strings.Compare(strMsgType, "location") {
		return MT_LOCATE
	} else if 0 == strings.Compare(strMsgType, "voice_text") {
		return MT_VOICETEXT
	} else if 0 == strings.Compare(strMsgType, "correction") {
		return MT_CORRECTION
	} else if (0 == strings.Compare(strMsgType, "sticker")) ||
		(0 == strings.Compare(strMsgType, "new_sticker")) {
		return MT_STICKERS
	} else if 0 == strings.Compare(strMsgType, "doodle") {
		return MT_DOODLE
	} else if 0 == strings.Compare(strMsgType, "video") {
		return MT_VIDEO
	} else if 0 == strings.Compare(strMsgType, "link") {
		return MT_LINK
	} else if 0 == strings.Compare(strMsgType, "card") {
		return MT_CARD
	} else if strMsgType == "start_charge" {
		return MT_START_CHARGE
	} else if strMsgType == "stop_charge" {
		return MT_STOP_CHARGE
	}

	return MT_UNKNOWN
}

func SubString(str string, begin, length int) (substr string) {
	// 将字符串的转换成[]rune
	rs := []rune(str)
	lth := len(rs)

	// 简单的越界判断
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length
	if end > lth {
		end = lth
	}

	// 返回子串
	return string(rs[begin:end])
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV3packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}
	// reqBody 也为一个指针
	reqBody := &ht_muc.MucReqBody{}
	err = proto.Unmarshal(packet.GetBody(), reqBody)
	if err != nil {
		infoLog.Printf("OnMessage proto Unmarshal failed")
		return false
	}
	// 统计总的请求量
	attr := "gomuc/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch ht_muc.MUC_CMD_TYPE(head.Cmd) {
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_CREATE_ROOM:
		go ProcCreateRoom(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_REMOVE_MEMBER:
		go ProcRemoveMember(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_USER_QUIT:
		go ProcQuitRoom(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_MODIFY_ROOMNAME:
		go ProcModifyRoomName(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MODIFY_MEMBER_NAME:
		go ProcModifyMemberName(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MODIFY_MUC_PUSH_SETTING:
		go ProcModifyPushSetting(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GET_MUC_ROOM_INFO:
		go ProcGetRoomInfo(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_MESSAGE:
		go ProcMucMessage(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_ADD_MUC_TO_CONTACT_LIST:
		go ProcAddRoomToContactList(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GET_MUC_CONTACT_LIST:
		go ProcGetRoomFromContactList(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_VOIP_BLOCK_SETTING:
		go ProcS2SVoipBlockSetting(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_INVITE_BROADCAST:
		go ProcS2SGvoipInviteBroadCast(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_END_BROADCAST:
		go ProcS2SGvoipEndBroadCast(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_MEMBER_JOIN_BROADCAST, ht_muc.MUC_CMD_TYPE_GO_CMD_GVOIP_MEMBER_LEAVE_BROADCAST:
		go ProcS2SGvoipMemberChangeBroadCast(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_REQ_JOIN_ROOM:
		go ProcRequestJoinRoom(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_OPEN_REQ_VERIFY:
		go ProcMucOpenVerify(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_REQ_JOIN_ROOM_HANDLE:
		go ProcMucJoinRoomHandle(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_SET_ADMIN_REQ:
		go ProcMucSetAdmin(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_CREATEUSER_AUTHORIZATION_TRANS:
		go ProcCreateUserAuthTrans(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_GET_ROOM_BASE_INFO:
		go ProcMucGetRoomBaseInfo(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_SET_ROOM_ANNOUNCEMENT:
		go ProcMucSetRoomAnnouncement(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_QUREY_USR_IS_IN_ROOM:
		go ProcQueryUserIsAlreadyInRoom(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GET_MUC_GET_QRCODE_INFO:
		go ProcQueryQRcodeInfo(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_UPDATE_ROOM_MEMBER_LIMIT:
		go ProcUpdateRoomMemberLimit(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_BATCH_GET_MUC_MSG:
		go ProcBatchGetMucMsg(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_INVITE_MEMBER_REQ:
		go ProcInviteMemberReq(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_IS_USER_ADMIN:
		go ProcQueryUserIsAdmin(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_NOTIFY_CREATE_TEACHING_ROOM:
		go ProcNotifyCreateTeachingRoom(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_TEACHING_ROOM_STAT_BC_REQ:
		go ProcTeachingRoomStatBroadCast(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_NOTIFY_TEACHING_ROOM_END:
		go ProcNotifyTeachingRoomEnd(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_BEGIN_GROUP_LESSON_BROADCASE_REQ:
		go ProcNotifyBeginGroupLesson(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_END_GROUP_LESSON_BROADCASE_REQ:
		go ProcNotifyEndGroupLesson(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GROUP_STAT_CHANGE_BROADCASE_REQ:
		go ProcNotifyGroupLessonStatChange(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GROUP_LESSON_MESSAGE_BROADCASE_REQ:
		go ProcNotifyGroupLessonMessage(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_BATCH_GET_ROOM_SIMPLE_INFO:
		go ProcBatchGetRoomSimpleInfo(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_SET_ROOM_AVATAR:
		go ProcModifyRoomAvatar(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_GET_ADMIN_LIST_REQ:
		go ProcGetRoomAdminList(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_START_LESSON_CHARGE_REQ:
		go ProcStartLessonCharge(c, head, reqBody)
	case ht_muc.MUC_CMD_TYPE_GO_CMD_STOP_LESSON_CHARGE_REQ:
		go ProcStopLessonCharge(c, head, reqBody)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

func IsDuplicateSec(roomId, uid uint32, seq uint16) (result bool) {
	seqKey := fmt.Sprintf("%v-%v-%v", roomId, uid, seq)
	reqLock.Lock()
	defer reqLock.Unlock()
	if _, ok := reqRecord[seqKey]; ok {
		result = true
	} else {
		result = false
		// 如果不存在 这添加元素
		reqRecord[seqKey] = time.Now().Unix()
	}
	return result
}

func PublishActionEvent(uid, roomId, handlerId uint32, actionType int) (err error) {
	if uid == 0 || roomId == 0 || actionType == 0 {
		infoLog.Printf("PublishActionEvent invalid param uid=%v roomId=%v actionType=%v", uid, roomId, actionType)
		err = util.ErrInputParam
		return err
	}
	notifyObj := simplejson.New()
	notifyObj.Set("userid", uid)
	notifyObj.Set("room_id", roomId)
	notifyObj.Set("action_type", actionType)
	notifyObj.Set("handler_id", handlerId)
	notifyObj.Set("ts", time.Now().Unix())
	notifySlice, err := notifyObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("PublishActionEvent notifyObj.MarshalJson failed uid=%v roomId=%v actionType=%v",
			uid,
			roomId,
			actionType)
		return err
	}

	err = globalProducer.Publish(roomActionTopic, notifySlice)
	if err != nil {
		// 统计apns失败总量
		attr := "gomuc/post_action_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("PublishActionEvent Could not connect uid=%v roomId=%v actionType=%v",
			uid,
			roomId,
			actionType)
		return err
	} else {
		// 统计apns成功的总量
		attr := "gomuc/post_action_to_nsq_succ"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("PublishActionEvent Publish success uid=%v roomId=%v actionType=%v",
			uid,
			roomId,
			actionType)
	}
	return nil
}

// 1.proc create room
func ProcCreateRoom(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			infoLog.Printf("ProcCreateRoom defer SendCreatRoomRetCode")
			SendCreatRoomRetCode(c, head, result, errMsg)
		} else {
			infoLog.Printf("ProcCreateRoom defer not SendCreatRoomRetCode")
		}
	}()
	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// test is duplicate req
	bRet := IsDuplicateSec(0, head.From, head.Seq)
	if bRet {
		infoLog.Printf("ProcCreateRoom duplicate req uid=%v seq=%v", head.From, head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}
	// add static
	attr := "gomuc/create_room_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetCreateRoomReqbody()
	creatUid := subReqBody.GetCreateUid()
	nickName := subReqBody.GetNickName()
	proMemberList := subReqBody.GetMembers()
	var memberList []*ht_muc.RoomMemberInfo
	for _, v := range proMemberList {
		if v.GetUid() != 0 {
			memberList = append(memberList, v)
		} else {
			infoLog.Printf("ProcCreateRoom member=#v error", *v)
		}
	}
	infoLog.Printf("ProcCreateRoom recv from=%v to=%v cmd=%v seq=%v userid=%v count=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		creatUid,
		len(memberList))
	// first check input param
	if creatUid == 0 || len(memberList) < 2 {
		infoLog.Printf("ProcCreateRoom invalid param uid=%v count=%v", creatUid, len(memberList))
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}
	// all client support muc chat no need to check version
	// second check black me list
	blackMeList, err := DbUtil.GetBlackMeList(creatUid)
	if err != nil {
		infoLog.Printf("ProcCreateRoom GetBlackMeList failed uid=%v", creatUid)
	}
	var totalBalck []uint32
	if len(blackMeList) != 0 {
		for i, v := range memberList {
			infoLog.Printf("ProcCreateRoom invite member index=%v uid=%v", i, v)
			if UidIsInSlice(blackMeList, v.GetUid()) {
				infoLog.Printf("ProcCreateRoom uidInfo=%#v is in blackMeList", v)
				totalBalck = append(totalBalck, v.GetUid())
			}
		}
	}
	if len(totalBalck) != 0 {
		subMucRspBody := new(ht_muc.CreateRoomRspBody)
		errMsg = []byte("some one black me")
		subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SOME_ONE_BLACK_ME)), Reason: errMsg}
		subMucRspBody.ListBlackMe = totalBalck
		bNeedCall = false // 无需调用defer 中的函数
		SendCreatRoomResp(c, head, subMucRspBody)
		return false
	}

	// third get vip expire ts from db
	vipExpireTs, err := DbUtil.GetUserVIPExpireTS(creatUid)
	if err != nil {
		infoLog.Printf("ProcCreateRoom GetUserVIPExpireTS failed uid=%v", creatUid)
	}
	tsNow := time.Now().Unix()
	maxMember := util.MUC_MEMBER_LIMIT
	if vipExpireTs > uint64(tsNow) {
		maxMember = util.MUC_MEMBER_LIMIT_VIP
	}
	if len(memberList) > maxMember {
		infoLog.Printf("ProcCreateRoom memberList count=%v excess maxMember=%v", len(memberList), maxMember)
		result = uint16(ht_muc.MUC_RET_CODE_RET_MEMBER_EXEC_LIMIT)
		errMsg = []byte("member over limit")
		return false
	}

	// fourth crate muc room
	var uidList []uint32
	for _, v := range memberList {
		uidList = append(uidList, v.GetUid())
	}
	roomId, roomTS, err := roomManager.CreateMuc(creatUid, uidList, uint32(maxMember))
	if err != nil {
		infoLog.Printf("ProcCreateRoom room manager create room failed create=%v", creatUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	// sixth send ack
	subMucRspBody := new(ht_muc.CreateRoomRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomId = proto.Uint32(roomId)
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	SendCreatRoomResp(c, head, subMucRspBody)
	bNeedCall = false

	// fiveth broadcast message
	opInfo := &ht_muc.RoomMemberInfo{
		Uid:      proto.Uint32(creatUid),
		NickName: nickName,
	}
	err = roomManager.NotifyInviteMember(opInfo, opInfo, memberList, roomId, uint64(roomTS), uint32(ht_muc.ROOMID_FROM_TYPE_ENUM_FROM_INVITE), true)
	if err != nil {
		infoLog.Printf("ProcCreateRoom NotifyInviteMember failed uid=%v roomId=%v roomTS=%v", creatUid, roomId, roomTS)
	}

	// 2017-12-28 发布事件到NSQ
	err = PublishActionEvent(creatUid, roomId, 0, AT_CREATE_ROOM)
	if err != nil {
		// add static
		attr := "gomuc/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcCreateRoom PublishPostEvent uid=%v roomId=%v actionType=%v failed",
			creatUid,
			roomId,
			AT_CREATE_ROOM)
	}
	// infoLog.Printf("DEBUG ProcCreateRoom roomId=%v roomTS=%v", roomId, roomTS)
	return true
}
func SendCreatRoomRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.CreateRoomRspbody = new(ht_muc.CreateRoomRspBody)
	subMucRspBody := rspBody.GetCreateRoomRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendCreatRoomResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendCreatRoomResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendCreatRoomResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.CreateRoomRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.CreateRoomRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendCreatRoomResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendCreatRoomResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func UidIsInSlice(uidList []uint32, uid uint32) bool {
	if uid == 0 || len(uidList) == 0 {
		return false
	}
	for _, v := range uidList {
		if uid == v {
			return true
		}
	}

	return false
}

// 2.proc creater invite member
// func ProcInviteMember(c *gotcp.Conn, p gotcp.Packet) bool {
// 	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
// 	var head *common.HeadV3
// 	packet, ok := p.(*common.HeadV3Packet)
// 	if !ok {
// 		infoLog.Printf("ProcInviteMember Convert to HeadV3Packet failed")
// 		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
// 		errMsg := []byte("Internal error")
// 		SendInviteMemberRetCode(c, head, result, errMsg)
// 		return false
// 	}
// 	head, err := packet.GetHead()
// 	if err != nil {
// 		infoLog.Printf("ProcInviteMember Get head faild")
// 		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
// 		errMsg := []byte("Internal error")
// 		SendInviteMemberRetCode(c, head, result, errMsg)
// 		return false
// 	}

// 	reqBody := &ht_muc.MucReqBody{}
// 	err = proto.Unmarshal(packet.GetBody(), reqBody)
// 	if err != nil {
// 		infoLog.Printf("ProcInviteMember proto Unmarshal failed")
// 		result = uint16(ht_muc.MUC_RET_CODE_RET_PB_ERR)
// 		errMsg := []byte("pb unmarshal error")
// 		SendInviteMemberRetCode(c, head, result, errMsg)
// 		return false
// 	}

// 	// add static
// 	attr := "gomuc/invite_member_req_count"
// 	libcomm.AttrAdd(attr, 1)

// 	subReqBody := reqBody.GetInviteMemberReqbody()
// 	inviteUid := subReqBody.GetInviteUid()
// 	roomId := subReqBody.GetRoomId()
// 	memberList := subReqBody.GetMembers()
// 	infoLog.Printf("ProcInviteMembe recv from=%v to=%v cmd=%v seq=%v inviteUid=%v  roomId=%v count=%v",
// 		head.From,
// 		head.To,
// 		head.Cmd,
// 		head.Seq,
// 		inviteUid,
// 		roomId,
// 		len(memberList))

// 	// first check input param
// 	if inviteUid == 0 || roomId == 0 || len(memberList) == 0 {
// 		infoLog.Printf("ProcInviteMember invalid param")
// 		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
// 		errMsg := []byte("input param error")
// 		SendInviteMemberRetCode(c, head, result, errMsg)
// 		return false
// 	}
// 	// second check black me list
// 	blackMeList, err := DbUtil.GetBlackMeList(inviteUid)
// 	if err != nil {
// 		infoLog.Printf("ProcCreateRoom GetBlackMeList failed uid=%v", inviteUid)
// 	}
// 	var totalBalck []uint32
// 	if len(blackMeList) != 0 {
// 		for i, v := range memberList {
// 			infoLog.Printf("ProcCreateRoom black me index=%v uid=%v", i, v)
// 			if UidIsInSlice(blackMeList, v.GetUid()) {
// 				infoLog.Printf("ProcCreateRoom uid=%v is in blackMeList", v)
// 				totalBalck = append(totalBalck, v.GetUid())
// 			}
// 		}
// 	}
// 	if len(totalBalck) != 0 {
// 		subMucRspBody := new(ht_muc.InviteMemberRspBody)
// 		errMsg := []byte("some one black me")
// 		subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SOME_ONE_BLACK_ME)), Reason: errMsg}
// 		subMucRspBody.ListBlackMe = totalBalck
// 		SendInviteMemberResp(c, head, subMucRspBody)
// 		return false
// 	}
// 	// third invite member into room
// 	roomTS, _, err := roomManager.InviteMember(roomId, inviteUid, memberList)
// 	if err != nil {
// 		infoLog.Printf("ProcInviteMember roomId=%v inviteId=%v InviteMember failed err=%v", roomId, inviteUid, err)
// 		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
// 		errMsg := []byte("internal error")
// 		SendInviteMemberRetCode(c, head, result, errMsg)
// 		return false
// 	}

// 	// fourth broadcast message
// 	opInfo := &ht_muc.RoomMemberInfo{
// 		Uid:      proto.Uint32(inviteUid),
// 		NickName: subReqBody.GetNickName(),
// 	}
// 	err = roomManager.NotifyInviteMember(opInfo, opInfo, memberList, roomId, uint64(roomTS), uint32(ht_muc.ROOMID_FROM_TYPE_ENUM_FROM_INVITE))
// 	if err != nil {
// 		infoLog.Printf("ProcInviteMember roomId=%v inviteId=%v NotifyInviteMember failed", roomId, inviteUid)
// 	}

// 	infoLog.Printf("DEBUG ProcInviteMember roomId=%v roomTS=%v", roomId, roomTS)
// 	// fiveth send ack
// 	subMucRspBody := new(ht_muc.InviteMemberRspBody)
// 	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
// 	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
// 	SendInviteMemberResp(c, head, subMucRspBody)
// 	return true
// }
// func SendInviteMemberRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
// 	head := new(common.HeadV3)
// 	if reqHead != nil {
// 		*head = *reqHead
// 	}
// 	rspBody := new(ht_muc.MucRspBody)
// 	rspBody.InviteMemberRspbody = new(ht_muc.InviteMemberRspBody)
// 	subMucRspBody := rspBody.GetInviteMemberRspbody()
// 	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
// 	s, err := proto.Marshal(rspBody)
// 	if err != nil {
// 		infoLog.Printf("SendInviteMemberRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
// 			head.From,
// 			head.To,
// 			head.Cmd,
// 			head.Seq)
// 		return false
// 	}
// 	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
// 	head.Ret = ret
// 	buf := make([]byte, head.Len)
// 	buf[0] = common.HTV3MagicBegin
// 	err = common.SerialHeadV3ToSlice(head, buf[1:])
// 	if err != nil {
// 		infoLog.Printf("SendInviteMemberRetCode SerialHeadV3ToSlice failed")
// 		return false
// 	}
// 	copy(buf[common.PacketV3HeadLen:], s) // return code
// 	buf[head.Len-1] = common.HTV3MagicEnd

// 	rspPacket := common.NewHeadV3Packet(buf)
// 	c.AsyncWritePacket(rspPacket, time.Second)
// 	return true
// }
// func SendInviteMemberResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.InviteMemberRspBody) bool {
// 	head := new(common.HeadV3)
// 	if reqHead != nil {
// 		*head = *reqHead
// 	}
// 	rspBody := new(ht_muc.MucRspBody)
// 	rspBody.InviteMemberRspbody = subMucRspBody
// 	s, err := proto.Marshal(rspBody)
// 	if err != nil {
// 		infoLog.Printf("SendInviteMemberResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
// 			head.From,
// 			head.To,
// 			head.Cmd,
// 			head.Seq)
// 		return false
// 	}

// 	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
// 	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
// 	buf := make([]byte, head.Len)
// 	buf[0] = common.HTV3MagicBegin
// 	err = common.SerialHeadV3ToSlice(head, buf[1:])
// 	if err != nil {
// 		infoLog.Printf("SendInviteMemberResp SerialHeadV3ToSlice failed")
// 		return false
// 	}
// 	copy(buf[common.PacketV3HeadLen:], s) // return code
// 	buf[head.Len-1] = common.HTV3MagicEnd

// 	rspPacket := common.NewHeadV3Packet(buf)
// 	c.AsyncWritePacket(rspPacket, time.Second)
// 	return true
// }

// 3.Proc remove member
func ProcRemoveMember(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRemoveMemberRetCode(c, head, result, errMsg)
		}
	}()
	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/remove_member_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetRemoveMemberReqbody()
	adminUid := subReqBody.GetAdminUid()
	adminName := string(subReqBody.GetAdminName())
	roomId := subReqBody.GetRoomId()
	removeUid := subReqBody.GetRemoveUid()
	removeName := string(subReqBody.GetRemoveName())
	infoLog.Printf("ProcRemoveMembe recv from=%v to=%v cmd=%v seq=%v roomId=%v adminUid=%v removeUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		adminUid,
		removeUid)
	// first check input param
	if roomId == 0 || removeUid == 0 || adminUid == 0 || removeUid == adminUid {
		infoLog.Printf("ProcRemoveMember invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false

	}

	// second check whether user is admin
	bCreater, err := roomManager.IsCreateUid(roomId, adminUid)
	if err != nil {
		infoLog.Printf("ProcRemoveMember check uid=%v roomManager.IsCreateUid failed err=%v", adminUid, err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}

	if !bCreater { //如果不是创建者 继续检查是否是管理员
		bAdmin, err := roomManager.IsUserAdmin(roomId, adminUid)
		if err != nil {
			infoLog.Printf("ProcRemoveMember check uid=%v roomManager.IsUserAdmin failed err=%v", adminUid, err)
			result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
			errMsg = []byte("Internal error")
			return false
		}
		// 判断用户是否是管理员 如果不是管理员则出错
		if !bAdmin {
			infoLog.Printf("ProcRemoveMember uid=%v is not admin permission denied", adminUid)
			result = uint16(ht_muc.MUC_RET_CODE_RET_PERMISSION_DENIED)
			errMsg = []byte("permission denied")
			return false
		} else {
			// 继续检查被删除的用户是否是管理员
			bRemoveUserAdmind, err := roomManager.IsUserAdmin(roomId, removeUid)
			if err != nil {
				infoLog.Printf("ProcRemoveMember check uid=%v roomManager.IsUserAdmin failed err=%v", removeUid, err)
				result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
				errMsg = []byte("Internal error")
				return false
			}
			if bRemoveUserAdmind {
				infoLog.Printf("ProcRemoveMember removeUid=%v is admin permission denied", removeUid)
				result = uint16(ht_muc.MUC_RET_CODE_RET_PERMISSION_DENIED)
				errMsg = []byte("permission denied")
				return false
			}
		}
	} else {
		err = groupLessonApi.DelGroupLessonByUid(roomId, removeUid)
		if err != nil {
			infoLog.Printf("ProcRemoveMember groupLessonApi.DelGroupLessonByUid roomId=%v createrUid=%v err=%s",
				roomId,
				removeUid,
				err)
		}
		infoLog.Printf("ProcRemoveMember adminUid=%v is creater", adminUid)
	}

	// third remove member in db
	roomTS, err := roomManager.RemoveMember(roomId, adminUid, removeUid)
	if err != nil {
		infoLog.Printf("ProcRemoveMember roomManager RemoveMember failed roomId=%v adminUid=%v removeUid=%v",
			roomId,
			adminUid,
			removeUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	// fourth notify remove member

	err = roomManager.NotifyRemoveMember(roomId, adminUid, adminName, removeUid, removeName, uint64(roomTS))
	if err != nil {
		infoLog.Printf("ProcRemoveMember NotifyRemoveMember faield roomId=%v adminUid=%v removeUid%v err=%v",
			roomId,
			adminUid,
			removeUid,
			err)
	}
	// infoLog.Printf("DEBUG ProcRemoveMember roomId=%v roomTS=%v", roomId, roomTS)
	// fifth send ack
	subMucRspBody := new(ht_muc.RemoveMemberRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	bNeedCall = false
	SendRemoveMemberResp(c, head, subMucRspBody)

	// 2017-12-28 发布事件到NSQ
	err = PublishActionEvent(removeUid, roomId, adminUid, AT_KICKED_OUT_ROOM)
	if err != nil {
		// add static
		attr := "gomuc/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcRemoveMember PublishPostEvent uid=%v roomId=%v actionType=%v failed",
			adminUid,
			roomId,
			AT_KICKED_OUT_ROOM)
	}
	return true
}
func SendRemoveMemberRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.RemoveMemberRspbody = new(ht_muc.RemoveMemberRspBody)
	subMucRspBody := rspBody.GetRemoveMemberRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRemoveMemberRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendRemoveMemberRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendRemoveMemberResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.RemoveMemberRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.RemoveMemberRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRemoveMemberResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendRemoveMemberResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 4.proc member quit
func ProcQuitRoom(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendQuitRoomRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/quit_room_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetQuitRoomReqbody()
	quitUid := subReqBody.GetQuitUid()
	quitName := subReqBody.GetQuitName()
	roomId := subReqBody.GetRoomId()
	infoLog.Printf("ProcQuitRoom recv from=%v to=%v cmd=%v seq=%v roomId=%v quitUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		quitUid)
	// first check input param
	if roomId == 0 || quitUid == 0 {
		infoLog.Printf("ProcQuitRoom invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// Step1: 首先检查退群者是否是群主，如果是则检查群是否开启了群课程收费 如果开启直接返回客户端先关掉收费
	ret, err := roomManager.IsCreateUid(roomId, quitUid)
	if err != nil {
		infoLog.Printf("ProcQuitRoom roomId=%v quitUid=%v IsCreateUid failed err=%s",
			roomId,
			quitUid,
			err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error error")
		return false
	}
	// 确认是群住退群 检查当前群是否有课程正在收费
	if ret {
		exist, err := walletApi.GetRoomExistLessonCharging(roomId, quitUid)
		if err != nil {
			infoLog.Printf("ProcQuitRoom roomId=%v quitUid=%v GetRoomExistLessonCharging failed err=%s",
				roomId,
				quitUid,
				err)
			result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
			errMsg = []byte("internal error error")
			return false
		}
		infoLog.Printf("ProcQuitRoom roomId=%v quitUid=%v exist=%v", roomId, quitUid, exist)
		if exist {
			infoLog.Printf("ProcQuitRoom roomId=%v quitUid=%v exist lesson charging", roomId, quitUid)
			result = uint16(ht_muc.MUC_RET_CODE_RET_LESSON_IS_CHARGING)
			errMsg = []byte("lesson is charging")
			return false
		}
	}

	// second process member quit
	newCreater, roomTS, bIsCreater, bIsAdmin, err := roomManager.QuitMucRoom(roomId, quitUid)
	if err != nil {
		infoLog.Printf("ProcQuitRoom internal err")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	// third broadcast message
	err = roomManager.NotifyMemberQuit(roomId, quitUid, string(quitName), newCreater, uint64(roomTS))
	if err != nil {
		infoLog.Printf("ProcQuitRoom NotifyMemberQuit failed roomId=%v quitUid=%v newCreater=%v",
			roomId,
			quitUid,
			newCreater)
	}

	// infoLog.Printf("DEBUG ProcQuitRoom roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := new(ht_muc.QuitRoomRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	bNeedCall = false
	SendQuitRoomRsp(c, head, subMucRspBody)

	// 如果管理员退群 通知grouplesson 更新群课程
	if bIsCreater || bIsAdmin {
		infoLog.Printf("ProcQuitRoom bIsCreater=%v bIsAdmin=%v quit roomId=%v quitUid=%v newCreater=%v",
			bIsCreater,
			bIsAdmin,
			roomId,
			quitUid,
			newCreater)
		err = groupLessonApi.DelGroupLessonByUid(roomId, quitUid)
		if err != nil {
			infoLog.Printf("ProcQuitRoom groupLessonApi.DelGroupLessonByUid roomId=%v createrUid=%v err=%s",
				roomId,
				quitUid,
				err)
		}
	}

	// 2017-12-28 发布事件到NSQ
	err = PublishActionEvent(quitUid, roomId, quitUid, AT_INIT_QUIT_ROOM)
	if err != nil {
		// add static
		attr := "gomuc/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcQuitRoom PublishPostEvent uid=%v roomId=%v actionType=%v failed",
			quitUid,
			roomId,
			AT_INIT_QUIT_ROOM)
	}
	return true
}
func SendQuitRoomRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.QuitRoomRspbody = new(ht_muc.QuitRoomRspBody)
	subMucRspBody := rspBody.GetQuitRoomRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendQuitRoomRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendCreatRoomResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendQuitRoomRsp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.QuitRoomRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.QuitRoomRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendQuitRoomRsp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendQuitRoomRsp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 5.proc modify room name
func ProcModifyRoomName(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendModifyRoomNameRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/modify_room_name_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetModifyRoomNameReqbody()
	opUid := subReqBody.GetOpUid()
	opName := subReqBody.GetOpName()
	roomId := subReqBody.GetRoomId()
	roomName := string(subReqBody.GetRoomName())
	infoLog.Printf("ProcModifyRoomName recv from=%v to=%v cmd=%v seq=%v roomId=%v roomName=%s opUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		roomName,
		opUid)
	// first check input param
	if roomId == 0 || opUid == 0 || len(roomName) == 0 {
		infoLog.Printf("ProcModifyRoomName invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second modify room in db and memory
	roomTS, err := roomManager.ModifyRoomName(roomId, opUid, roomName)
	if err != nil {
		infoLog.Printf("ProcModifyRoomName roomManager.ModifyRoomName failed roomId=%v opUid=%v roomName=%s",
			roomId,
			opUid,
			roomName)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}
	// third broadcast notifycation
	err = roomManager.NotifyModifyRoomName(roomId, opUid, string(opName), string(roomName), uint64(roomTS))
	if err != nil {
		infoLog.Printf("ProcModifyRoomName roomManager.NotifyModifyRoomName failed roomId=%v opUid=%v roomName=%s",
			roomId,
			opUid,
			roomName)
	}

	// infoLog.Printf("DEBUG ProcModifyRoomName roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := new(ht_muc.ModifyRoomNameRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	bNeedCall = false
	SendModifyRoomNameResp(c, head, subMucRspBody)
	return true
}
func SendModifyRoomNameRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.ModifyRoomNameRspbody = new(ht_muc.ModifyRoomNameRspBody)
	subMucRspBody := rspBody.GetModifyRoomNameRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendModifyRoomNameRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendModifyRoomNameRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendModifyRoomNameResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.ModifyRoomNameRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.ModifyRoomNameRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendModifyRoomNameResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendModifyRoomNameResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 6.proc modify member name
func ProcModifyMemberName(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendModifyMemberNameRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/modify_member_name_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetModifyMemberNameReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	opName := string(subReqBody.GetOpName())
	infoLog.Printf("ProcModifyMemberName recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v opName=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid,
		opName)
	// first check input param
	if roomId == 0 || opUid == 0 || len(opName) == 0 {
		infoLog.Printf("ProcModifyMemberName invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second modify member name
	roomTS, err := roomManager.ModifyMemberName(roomId, opUid, opName)
	if err != nil {
		infoLog.Printf("ProcModifyMemberName failed roomId=%v opUid=%v opName=%s", roomId, opUid, opName)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	// third broadcast member name chage
	err = roomManager.NotifyModifyMemberName(roomId, opUid, opName, uint64(roomTS))
	if err != nil {
		infoLog.Printf("ProcModifyMemberName NotifyModifyMemberName faild roomId=%v opUid=%v opName=%v",
			roomId,
			opUid,
			opName)
	}

	// infoLog.Printf("DEBUG ProcModifyMemberName roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := new(ht_muc.ModifyMemberNameRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	bNeedCall = false
	SendModifyMemberNameResp(c, head, subMucRspBody)
	return true
}
func SendModifyMemberNameRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.ModifyMemberNameRspbody = new(ht_muc.ModifyMemberNameRspBody)
	subMucRspBody := rspBody.GetModifyMemberNameRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendModifyMemberNameRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendModifyMemberNameRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendModifyMemberNameResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.ModifyMemberNameRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.ModifyMemberNameRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendModifyMemberNameResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendModifyMemberNameResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 7.Proc modify pushsetting
func ProcModifyPushSetting(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendModifyPushSettingRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/modify_push_setting_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetModifyPushSettingReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	pushSetting := subReqBody.GetPushSetting()
	infoLog.Printf("ProcModifyPushSetting recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v pushSetting=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid,
		pushSetting)
	// first check input param
	if roomId == 0 || opUid == 0 || pushSetting > 1 {
		infoLog.Printf("ProcModifyPushSetting invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second modify push setting in db
	err := roomManager.ModifyPushSetting(roomId, opUid, pushSetting)
	if err != nil {
		infoLog.Printf("ProcModifyPushSetting modify faield roomId=%v opUid=%v pushSettig=%v", roomId, opUid, pushSetting)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	// third send ack
	subMucRspBody := new(ht_muc.ModifyPushSettingRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	bNeedCall = false // 无需调用defer函数
	SendModifyPushSettingResp(c, head, subMucRspBody)
	return true
}
func SendModifyPushSettingRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.ModifyPushSettingRspbody = new(ht_muc.ModifyPushSettingRspBody)
	subMucRspBody := rspBody.GetModifyPushSettingRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendModifyPushSettingRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendModifyPushSettingRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendModifyPushSettingResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.ModifyPushSettingRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.ModifyPushSettingRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendModifyPushSettingResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendModifyPushSettingResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 8.Proc Get Room Info
func ProcGetRoomInfo(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendGetRoomInfoRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/get_room_info_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetGetRoomInfoReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	roomTS := subReqBody.GetRoomTimestamp()
	infoLog.Printf("ProcGetRoomInfo recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v roomTS=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid,
		roomTS)
	// first check input param
	if roomId == 0 || opUid == 0 {
		infoLog.Printf("ProcGetRoomInfo invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second get room info
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcGetRoomInfo faield roomId=%v opUid=%v roomTS=%v", roomId, opUid, roomTS)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	// third send ack
	subMucRspBody := new(ht_muc.GetRoomInfoRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	var memberInfo []*ht_muc.RoomMemberInfo
	memberList := roomInfo.MemberList
	var pushSetting uint32 = 0
	for _, v := range memberList {
		if v.Uid == opUid {
			pushSetting = v.PushSetting
		}
		iterm := &ht_muc.RoomMemberInfo{
			Uid:      proto.Uint32(v.Uid),
			NickName: []byte(v.NickName),
		}
		memberInfo = append(memberInfo, iterm)
	}

	var adminList []uint32
	for _, v := range roomInfo.AdminList {
		if v != 0 {
			adminList = append(adminList, v)
		}
	}
	verify := ht_muc.VERIFY_STAT(roomInfo.VerifyStat)
	subMucRspBody.RoomInfo = &ht_muc.RoomInfoBody{
		RoomId:       proto.Uint32(roomId),
		CreateUid:    proto.Uint32(roomInfo.CreateUid),
		ListAdminUid: adminList,
		AdminLimit:   proto.Uint32(roomInfo.AdminLimit),
		RoomLimit:    proto.Uint32(roomInfo.MemberLimit),
		RoomName:     []byte(roomInfo.RoomName),
		RoomDesc:     []byte(roomInfo.RoomDesc),
		RoomAvatar:   []byte(roomInfo.RoomAvatar),
		VerifyStat:   &verify,
		Announcement: &ht_muc.AnnoType{
			PublishUid:  proto.Uint32(roomInfo.Announcement.PublishUid),
			PublishTs:   proto.Uint32(roomInfo.Announcement.PublishTS),
			AnnoContent: []byte(roomInfo.Announcement.AnnoContect),
		},
		RoomTimestamp: proto.Uint64(uint64(roomInfo.RoomTS)),
		PushSetting:   proto.Uint32(pushSetting),
		Members:       memberInfo,
	}
	// infoLog.Printf("DEBUG ProcGetRoomInfo roomId=%v creatUid=%v admminList=%v adminLimit=%v roomLimit=%v roomName=%s roomDesc=%s verifyStat=%v",
	// roomId,
	// roomInfo.CreateUid,
	// adminList,
	// roomInfo.AdminLimit,
	// roomInfo.MemberLimit,
	// roomInfo.RoomName,
	// roomInfo.RoomDesc,
	// verify)
	// infoLog.Printf("DEBUG ProcGetRoomInfo publishUid=%v publishTS=%v AnnoCotent=%s roomTS=%v pushSetting=%v members=%v",
	// roomInfo.Announcement.PublishUid,
	// roomInfo.Announcement.PublishTS,
	// roomInfo.Announcement.AnnoContect,
	// roomInfo.RoomTS,
	// pushSetting,
	// roomInfo.MemberList)
	bNeedCall = false
	SendGetRoomInfoResp(c, head, subMucRspBody)
	return true
}
func SendGetRoomInfoRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.GetRoomInfoRspbody = new(ht_muc.GetRoomInfoRspBody)
	subMucRspBody := rspBody.GetGetRoomInfoRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendGetRoomInfoRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendGetRoomInfoRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendGetRoomInfoResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.GetRoomInfoRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.GetRoomInfoRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendGetRoomInfoResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendGetRoomInfoResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 9.ProcMucMessage
func ProcMucMessage(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendMucMessageRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/muc_msg_req_count"
	libcomm.AttrAdd(attr, 1)

	// 获取muc msg构造解压对象
	subReqBody := reqBody.GetMucMessageReqbody()
	msg := subReqBody.GetMsg()
	compressBuff := bytes.NewBuffer(msg)
	r, err := zlib.NewReader(compressBuff)
	defer r.Close()
	if err != nil {
		infoLog.Printf("ProcMucMessage zlib.NewReader failed err=%v", err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	// 解压缩
	unCompressSlice, err := ioutil.ReadAll(r)
	if err != nil {
		infoLog.Printf("ProcMucMessage zlib. unCompress failed")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	// 解压之后的内容构造json
	rootObj, err := simplejson.NewJson(unCompressSlice)
	if err != nil {
		infoLog.Printf("ProcMucMessage simplejson new packet error", err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("NewJson failed")
		return false
	}
	// 群固有属性
	roomId := uint32(rootObj.Get("room_id").MustInt64(0))
	strMsgId := rootObj.Get("msg_id").MustString()
	strSendTime := rootObj.Get("send_time").MustString()
	strMsgType := rootObj.Get("msg_type").MustString()
	msgType := GetMessageType(strMsgType)
	// strMsgModel := rootObj.Get("msg_model").MustString()
	fromId := uint32(rootObj.Get("sender_id").MustInt64(0))
	// userProfile := rootObj.Get("sender_ts").MustUint64(0)
	strNickName := rootObj.Get("sender_name").MustString()

	remindType := rootObj.Get("at").Get("remind_type").MustInt(0)
	var remindList []uint32
	// remindType == 2 查看数组
	if remindType == 2 {
		tempList := rootObj.Get("at").Get("remind_list").MustArray()
		infoLog.Printf("ProcMucMessage remind_list=%v", tempList)
		for _, v := range tempList {
			tempUid, err := v.(json.Number).Int64()
			if err != nil {
				infoLog.Printf("ProcMucMessage remind_list get Uid faild")
				continue
			}
			remindList = append(remindList, uint32(tempUid))
		}
	}

	var bReply bool
	replyUid := uint32(rootObj.Get("reply_info").Get("from_id").MustInt64(0))
	if replyUid != 0 {
		bReply = true
	}

	infoLog.Printf("ProcMucMessage recv from=%v to=%v cmd=%v seq=%v roomId=%v strMsgId=%s strMsgType=%s msgType=%v fromId=%v strNickName=%s sendTime=%s remindType=%v remidList=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		strMsgId,
		strMsgType,
		msgType,
		fromId,
		strNickName,
		strSendTime,
		remindType,
		remindList)
	// first check input param
	if head.From == 0 || head.To == 0 || roomId == 0 || fromId == 0 || head.From != fromId || head.To != roomId || msgType == MT_UNKNOWN {
		infoLog.Printf("ProcModifyPushSetting invalid param head.From=%v head.To=%v roomId=%v fromId=%v msgType=%v",
			head.From,
			head.To,
			roomId,
			fromId,
			msgType)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// Second Check whether user is in room

	pushInfo := util.MucPushInfo{NickName: strNickName, MsgId: strMsgId}
	switch msgType {
	case MT_TEXT:
		strMessageText := rootObj.Get("text").Get("text").MustString()
		if utf8.RuneCountInString(strMessageText) > MUC_MESSAGE_LNE_LIMIT { // 字符串超过长度截取部分子串
			pushInfo.PushParam = SubString(strMessageText, 0, MUC_MESSAGE_LNE_LIMIT) + "..."

		} else {
			pushInfo.PushParam = strMessageText
		}
		pushInfo.PushType = util.PUSH_TEXT
	case MT_TRANSLATE:
		strMessageText := rootObj.Get("translate").Get("src_text").MustString()
		if utf8.RuneCountInString(strMessageText) > MUC_MESSAGE_LNE_LIMIT { // 字符串超过长度截取部分子串
			pushInfo.PushParam = SubString(strMessageText, 0, MUC_MESSAGE_LNE_LIMIT) + "..."
		} else {
			pushInfo.PushParam = strMessageText
		}
		pushInfo.PushType = util.PUSH_TEXT
	case MT_PROFILE:
		strIntroduceName := rootObj.Get("introduction").Get("user_profile").Get("nick_name").MustString()
		if utf8.RuneCountInString(strIntroduceName) > MUC_MESSAGE_LNE_LIMIT { // 字符串超过长度截取部分子串
			pushInfo.PushParam = SubString(strIntroduceName, 0, MUC_MESSAGE_LNE_LIMIT) + "..."
		} else {
			pushInfo.PushParam = strIntroduceName
		}
		pushInfo.PushType = util.PUSH_INTRODUCE
	case MT_VOICE, MT_VOICETEXT:
		pushInfo.PushType = util.PUSH_VOICE

	case MT_IMAGE:
		pushInfo.PushType = util.PUSH_IMAGE

	case MT_LOCATE:
		pushInfo.PushType = util.PUSH_LOCATION

	case MT_CORRECTION:
		pushInfo.PushType = util.PUSH_CORRECT_SENTENCE

	case MT_DOODLE:
		pushInfo.PushType = util.PUSH_DOODLE

	case MT_STICKERS:
		pushInfo.PushType = util.PUSH_STICKERS

	case MT_VIDEO:
		pushInfo.PushType = util.PUSH_VIDEO

	case MT_LINK:
		pushInfo.PushType = util.PUSH_LINK

	case MT_CARD:
		pushInfo.PushType = util.PUSH_CARD
	default:
		infoLog.Printf("ProcMucMessage Unhandle type=%v", msgType)
	}

	// infoLog.Printf("DEBUG ProcMucMessage msgId=%s", strMsgId)
	// 添加消息经过服务器的附加属性
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("get room info failed")
		infoLog.Printf("ProcMucMessage faield roomId=%v fromId=%v", roomId, fromId)
		return false
	}
	memberList := roomInfo.MemberList
	// 如果用户不在群聊中 不允许发送群消息
	var bInRoom bool = false
	for _, v := range memberList {
		if v.Uid == fromId {
			bInRoom = true
		}
	}

	if !bInRoom {
		infoLog.Printf("BroadcastMucMessageCompatibleV2 user is not in room roomId=%v uid=%v", roomId, head.From)
		result = uint16(ht_muc.MUC_RET_CODE_RET_PERMISSION_DENIED)
		errMsg = []byte("permission denied")
		return false
	}

	// Third send ack
	subMucRspBody := new(ht_muc.MucMessageRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RspDetial = &ht_muc.MucRspDetial{
		MsgId: []byte(strMsgId),
	}
	bNeedCall = false
	SendMucMessageResp(c, head, subMucRspBody)

	// 提交改错记录写rabbitMQ
	if msgType == MT_CORRECTION {
		NotifyCorrectSentence(head.From, head.TermType, rootObj)
	}
	// 写入记录到数据库
	mucMessageLog := util.MUCMessageLog{
		FromId:       head.From,
		RoomId:       roomId,
		MsgType:      msgType,
		Msg:          msg,
		MsgTimeStamp: time.Now().Unix(),
	}
	LogDbUtil.AsyncAddMucMessageLog(&mucMessageLog, 1) // 写入数据库超时时间1秒
	// 后续失败都不用返回给客户端 直接返回false

	// 图片URL腾讯云服务的兼容策略(图片服务器新老版本的URL替换) 2015-06-29
	if msgType == MT_IMAGE {
		FixQcloudImageCompatibleWithToId(rootObj.Get("image"), head.To)
		imageObd := simplejson.New()
		imageObd.Set("url", rootObj.Get("image").Get("url").MustString(""))
		imageObd.Set("thumb_url", rootObj.Get("image").Get("thumb_url").MustString(""))
		imageSlic, err := imageObd.MarshalJSON()
		if err != nil {
			infoLog.Printf("ProcMucMessage msgTyep=MT_IMAGE simpleJson MarshalJSON failed roomId=%v fromId=%v seq=%v err=%s",
				roomId,
				fromId,
				head.Seq,
				err)
		} else {
			pushInfo.PushParam = string(imageSlic)
		}
	} else if msgType == MT_DOODLE {
		FixQcloudImageCompatibleWithToId(rootObj.Get("doodle"), head.To)
	} else if msgType == MT_VOICE {
		FixQcloudVoiceCompatibleWithToId(rootObj.Get("voice"), head.To)
		voiceObd := simplejson.New()
		voiceObd.Set("url", rootObj.Get("voice").Get("url").MustString(""))
		voiceObd.Set("duration", rootObj.Get("voice").Get("duration").MustInt(0))
		voiceSlic, err := voiceObd.MarshalJSON()
		if err != nil {
			infoLog.Printf("ProcMucMessage msgTyep=MT_VOICE simpleJson MarshalJSON failed roomId=%v fromId=%v seq=%v err=%s",
				roomId,
				fromId,
				head.Seq,
				err)
		} else {
			pushInfo.PushParam = string(voiceSlic)
		}
	} else if msgType == MT_VOICETEXT {
		FixQcloudVoiceCompatibleWithToId(rootObj.Get("voice_text"), head.To)
	}

	formatTime, ts := GetCurrentFormatTime()
	rootObj.Set("server_time", formatTime)
	rootObj.Set("server_ts", ts)
	rootObj.Set("room_name", roomInfo.RoomName)
	rootObj.Set("room_ts", roomInfo.RoomTS)
	strMsgBody, err := rootObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("ProcMucMessage simpleJson MarshalJSON failed roomId=%v fromId=%v seq=%v",
			roomId,
			fromId,
			head.Seq)
		return false
	}
	infoLog.Printf("ProcMucMessage outJson=%s", strMsgBody)
	// 压缩JSON消息数据
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(strMsgBody)
	w.Close()
	compressSliceByte := b.Bytes()

	// 新的群消息处理 新起一个协程
	go NewGroupMsgStoreProcess(roomId, fromId, uint32(head.Cmd), head.Reserved[0], compressSliceByte, &pushInfo, uint32(remindType), remindList, bReply, replyUid)

	// 拼装成新的pb格式
	bcReqBody := new(ht_muc.MucReqBody)
	bcReqBody.MucMessageReqbody = &ht_muc.MucMessageReqBody{Msg: compressSliceByte}
	bcSlice, err := proto.Marshal(bcReqBody)
	if err != nil {
		infoLog.Printf("ProcMucMessage proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	err = roomManager.BroadcastMucMessageCompatibleV2(roomId, head, bcSlice, &pushInfo, uint32(remindType), remindList, compressSliceByte, true, bReply, replyUid)
	if err != nil {
		infoLog.Printf("ProcMucMessage BroadcastMucMessage failed roomId=%v fromId=%v to=%v err=%v",
			roomId,
			head.From,
			head.To,
			err)
	}
	return true
}
func FixQcloudImageCompatible(imageObject *simplejson.Json) {
	if imageObject == nil {
		infoLog.Printf("FixQcloudImageCompatible nil imageObject")
		return
	}

	strUrl := imageObject.Get("url").MustString()
	infoLog.Printf("FixQcloudImageCompatible url=%s", strUrl)

	findHelloTalk := strings.Index(strUrl, "hellotalk.")
	var strThumbUrl string
	if findHelloTalk != -1 { // find hellotalk.
		findJpg := strings.LastIndex(strUrl, ".")
		if findJpg != -1 {
			strSuffix := strUrl[findJpg:]
			if strings.Compare(strSuffix, ".jpg") != 0 && strings.Compare(strSuffix, ".png") != 0 &&
				strings.Compare(strSuffix, ".JPG") != 0 && strings.Compare(strSuffix, ".PNG") != 0 {
				infoLog.Printf("FixQcloudImageCompatible Image URL unexpected suffix! url=%s", strUrl)
			}
			strThumbUrl = strUrl[0:findJpg] + "_thum" + strSuffix
			imageObject.Set("thumb_url", strThumbUrl)

		} else {
			infoLog.Printf("FixQcloudImageCompatible Image URL dont find .jpg. URL! url=%s", strUrl)
		}
	} else { // 不包含hellotalk.的URL要以http开头才处理
		if strings.Index(strUrl, "http") == 0 || strings.Index(strUrl, "HTTP") == 0 {
			strNewUrl := "p" + strUrl + "?m.jpg"
			imageObject.Set("url", strNewUrl) // 更新url

			if strings.Index(strUrl, "myqcloud.") != -1 {
				strThumbUrl = strUrl[0 : strings.LastIndex(strUrl, "/")+1]
				strThumbUrl += "scale"
			} else {
				strThumbUrl = strUrl + "/scale"
			}
			imageObject.Set("thumb_url", strThumbUrl) // 设置ThumbUrl
			infoLog.Printf("FixQcloudImageCompatible thumb_url=%s", strThumbUrl)
		} else {
			infoLog.Printf("FixQcloudImageCompatible Unexpecet image url! url=%s", strUrl)
		}
	}
	imageObject.Set("name", strUrl)
	return
}
func FixQcloudImageCompatibleWithToId(imageObject *simplejson.Json, toId uint32) {
	if imageObject == nil {
		infoLog.Printf("FixQcloudImageCompatibleWithToId nil imageObject")
		return
	}

	strUrl := imageObject.Get("url").MustString()
	infoLog.Printf("FixQcloudImageCompatibleWithToId url=%s", strUrl)
	//检查是否以https开头
	bHttps := (strings.Index(strUrl, "https") == 0 || strings.Index(strUrl, "HTTPS") == 0)
	// 群聊消息简化处理 凡是以https开头的都讲https替换成http
	if bHttps { //URL 以HTTP 或者HTTPS开头
		// 2016-06-21 liling
		// 改变url
		// A >= 2.2.8 and B < 2.2.8
		// 不需要添加p，但仍要为 thumb 在后面加上 scale
		// A -> B    : 将https:// 替换为 http://
		// B -> A    : http://us-ht.img-us-west-1.aliyuncs.com/     => https://us-aws-cdn.nihaotalk.com/
		//            http://sz-ht.img-cn-shenzhen.aliyuncs.com/    => https://sz-ali-cdn-img.nihaotalk.com/
		//            http://sg-ht.img-ap-southeast-1.aliyuncs.com/ => https://sg-aws-cdn.nihaotalk.com/
		// A -> B 如果以https开头且收方版本号<2.2.8 需要将https替换成http
		strNewUrl := strings.Replace(strUrl, "https", "http", 1)
		imageObject.Set("url", strNewUrl) // 更新url
	}

	strThumbUrl := imageObject.Get("thumb_url").MustString()
	if len(strThumbUrl) == 0 {
		strThumbUrl = imageObject.Get("url").MustString() + "/scale"
		imageObject.Set("thumb_url", strThumbUrl)
	}

	imageObject.Set("name", imageObject.Get("url").MustString())
	infoLog.Printf("FixQcloudImageCompatibleWithToId to=%v url=%s thumb_url=%s",
		toId,
		imageObject.Get("url").MustString(),
		imageObject.Get("thumb_url").MustString())
}

func FixQcloudVoiceCompatibleWithToId(voiceObject *simplejson.Json, toId uint32) {
	if voiceObject == nil {
		infoLog.Printf("FixQcloudVoiceCompatibleWithToId nil voiceObject")
		return
	}

	strUrl := voiceObject.Get("url").MustString()
	infoLog.Printf("FixQcloudVoiceCompatibleWithToId url=%s", strUrl)

	//检查是否以https开头
	bHttps := (strings.Index(strUrl, "https") == 0 || strings.Index(strUrl, "HTTPS") == 0)
	if bHttps { //URL 以HTTP 或者HTTPS开头
		// 2016-06-21 liling
		// 改变url
		// A >= 2.2.8 and B < 2.2.8
		// 不需要添加p，但仍要为 thumb 在后面加上 scale
		// A -> B    : 将https:// 替换为 http://
		// B -> A    : http://us-ht.img-us-west-1.aliyuncs.com/     => https://us-aws-cdn.nihaotalk.com/
		//            http://sz-ht.img-cn-shenzhen.aliyuncs.com/    => https://sz-ali-cdn-img.nihaotalk.com/
		//            http://sg-ht.img-ap-southeast-1.aliyuncs.com/ => https://sg-aws-cdn.nihaotalk.com/
		// A -> B 如果以https开头且收方版本号<2.2.8 需要将https替换成http
		strNewUrl := strings.Replace(strUrl, "https", "http", 1)
		voiceObject.Set("url", strNewUrl) // 更新url
	}

	infoLog.Printf("FixQcloudVoiceCompatibleWithToId to=%v url=%s",
		toId,
		voiceObject.Get("url").MustString())
}

func NewGroupMsgStoreProcess(roomId, fromId, cmd uint32,
	fromType uint8,
	msg []byte,
	pushInfo *util.MucPushInfo,
	remindType uint32,
	remindList []uint32,
	bReply bool,
	replyUid uint32) (err error) {
	if roomId == 0 || fromId == 0 || cmd == 0 || len(msg) == 0 {
		infoLog.Printf("NewGroupMsgStoreProcess invalid param roomId=%v fromId=%v cmd=%v msg_len=%v",
			roomId,
			fromId,
			cmd,
			len(msg))
		err = errors.New("error input param")
		return err
	}
	infoLog.Printf("NewGroupMsgStoreProcess roomId=%v fromId=%v cmd=0x%4X remidType=%v remidList=%v bReply=%v replyUid=%v",
		roomId,
		fromId,
		cmd,
		remindType,
		remindList,
		bReply,
		replyUid)
	// step1 : check whether user is in room
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("NewGroupMsgStoreProcess roomManager.GetRoomInfo faild roomId=%v fromId=%v cmd=%v err=%s",
			roomId,
			fromId,
			err)
		return err
	}
	var sendToList []uint32
	var bInRoom bool = false
	memberList := roomInfo.MemberList
	for _, v := range memberList {
		sendToList = append(sendToList, v.Uid)
		if v.Uid == fromId {
			bInRoom = true
		}
	}
	if !bInRoom {
		infoLog.Printf("NewGroupMsgStoreProcess user is not in room roomId=%v uid=%v", roomId, fromId)
		err = errors.New("error input param")
		return err
	}
	// step2 : store muc message
	timeStamp := uint64(time.Now().UnixNano() / 1000000)
	msgIndex, err := mucStoreApi.StoreMucMsg(roomId, fromId, cmd, timeStamp, msg)
	if err != nil {
		infoLog.Printf("NewGroupMsgStoreProcess mucStoreApi.StoreMucMsg failed roomId=%v fromId=%v cmd=%v err=%s",
			roomId,
			fromId,
			cmd,
			err)
		// add static
		attr := "gomuc/muc_store_msg_faield"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	infoLog.Printf("NewGroupMsgStoreProcess mucStoreApi.StoreMucMsgIndex roomId=%v fromId=%v sendToListLen=%v",
		roomId,
		fromId,
		len(sendToList))
	// step3 : store muc message index
	ret, err := mucStoreApi.StoreMucMsgIndex(fromId, msgIndex, sendToList)
	if err != nil || ret != uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("NewGroupMsgStoreProcess mucStoreApi.StoreMucMsgIndex failed roomId=%v fromId=%v cmd=%v err=%s",
			roomId,
			fromId,
			cmd,
			err)
		// add static
		attr := "gomuc/muc_store_msg_index_faield"
		libcomm.AttrAdd(attr, 1)
		if err == nil {
			err = errors.New("error store muc msg failed")
		}
		return err
	}
	// step4 : send notify or push message
	err = roomManager.BroadCastNewMucMessageNotify(
		roomId,
		fromId,
		fromType,
		memberList,
		msgIndex,
		remindType,
		remindList,
		bReply,
		replyUid,
		pushInfo)
	if err != nil {
		attr := "gomuc/muc_send_notify_faield"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("NewGroupMsgStoreProcess roomId=%v fromId=%v msgIndex=%s send notify failed",
			roomId,
			fromId,
			msgIndex)
	}
	return err
}

// iOS 端使用的时间戳是毫秒 所以需要将timeStamp 转成毫秒
func GetCurrentFormatTime() (formatTime string, timeStamp int64) {
	currTime := time.Now().UTC()
	formatTime = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d",
		currTime.Year(),
		currTime.Month(),
		currTime.Day(),
		currTime.Hour(),
		currTime.Minute(),
		currTime.Second())
	timeStamp = currTime.UnixNano() / (1000 * 1000)
	return
}
func SendMucMessageRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucMessageRspbody = new(ht_muc.MucMessageRspBody)
	subMucRspBody := rspBody.GetMucMessageRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucMessageRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucMessageRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendMucMessageResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.MucMessageRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucMessageRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucMessageResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucMessageResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 10.Proc add muc to contact list
func ProcAddRoomToContactList(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendAddRoomToContactListRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/add_room_to_contactlist_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetAddRoomToContactListReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	opType := subReqBody.GetOpType()
	infoLog.Printf("ProcAddRoomToContactList recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v opType=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid,
		opType)
	// first check input param
	if roomId == 0 || opUid == 0 || opType > 1 {
		infoLog.Printf("ProcAddRoomToContactList invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// operator room in contact list
	err := roomManager.AddRoomToContactList(roomId, opUid, opType)
	if err != nil {
		infoLog.Printf("ProcAddRoomToContactList modify faield roomId=%v opUid=%v opType=%v err=%v",
			roomId,
			opUid,
			opType,
			err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	// third send ack
	subMucRspBody := new(ht_muc.AddRoomToContactListRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	SendAddRoomToContactListResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}
func SendAddRoomToContactListRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.AddRoomToContactListRspbody = new(ht_muc.AddRoomToContactListRspBody)
	subMucRspBody := rspBody.GetAddRoomToContactListRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendAddRoomToContactListRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendAddRoomToContactListRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendAddRoomToContactListResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.AddRoomToContactListRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.AddRoomToContactListRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendAddRoomToContactListResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendAddRoomToContactListResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 11.Get muc contact list
func ProcGetRoomFromContactList(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendAddRoomToContactListRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/get_room_from_contactlist_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetGetRoomFromContactListReqbody()
	opUid := subReqBody.GetOpUid()
	infoLog.Printf("ProcGetRoomFromContactList recv from=%v to=%v cmd=%v seq=%v opUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		opUid)
	// first check input param
	if opUid == 0 {
		infoLog.Printf("ProcGetRoomFromContactList invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// operator room in contact list
	roomList, err := roomManager.GetRoomFromContactList(opUid)
	if err != nil {
		infoLog.Printf("ProcGetRoomFromContactList modify faield opUid=%v err=%v",
			opUid,
			err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	// infoLog.Printf("DEBUG ProcGetRoomFromContactList roomList=%#v", roomList)
	// third send ack
	subMucRspBody := new(ht_muc.GetRoomFromContactListRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.ListRoomInfo = roomList
	SendGetRoomFromContactListResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}
func SendGetRoomFromContactListRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.GetRoomFromContactListRspbody = new(ht_muc.GetRoomFromContactListRspBody)
	subMucRspBody := rspBody.GetGetRoomFromContactListRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendGetRoomFromContactListRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendGetRoomFromContactListRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendGetRoomFromContactListResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.GetRoomFromContactListRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.GetRoomFromContactListRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendGetRoomFromContactListResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendGetRoomFromContactListResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 13.s2svoip block setting
func ProcS2SVoipBlockSetting(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendS2SVoipBlockSettingRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}
	// add static
	attr := "gomuc/s2s_voip_block_setting_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetS2SVoipBlockSettingReqbody()
	blockId := subReqBody.GetBlockId()
	blockType := subReqBody.GetBlockType()
	action := subReqBody.GetAction()
	opUid := head.From
	infoLog.Printf("ProcS2SVoipBlockSetting recv from=%v to=%v cmd=%v seq=%v blockId=%v blockType=%v action=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		blockId,
		blockType,
		action)
	// first check input param
	if opUid == 0 || blockId == 0 || action > ht_muc.VOIP_BLOCK_SETTING_ENUM_VOIP_UN_KNOW || blockType > ht_muc.VOIP_BLOCK_TYPE_ENUM_UN_KNOW_TYPE {
		infoLog.Printf("ProcS2SVoipBlockSetting invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// operator room in contact list
	err := roomManager.UpdateVoipBlockSetting(opUid, blockId, uint32(blockType), uint32(action))
	if err != nil {
		infoLog.Printf("ProcS2SVoipBlockSetting modify faield opUid=%v blockId=%v blockType=%v action=%v err=%v",
			opUid,
			blockId,
			blockType,
			action,
			err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	infoLog.Printf("ProcS2SVoipBlockSetting succ ret=%v", uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))
	// third send ack
	subMucRspBody := new(ht_muc.S2SVoipBlockSettingRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	SendS2SVoipBlockSettingResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}
func SendS2SVoipBlockSettingRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.S2SVoipBlockSettingRspbody = new(ht_muc.S2SVoipBlockSettingRspBody)
	subMucRspBody := rspBody.GetS2SVoipBlockSettingRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendS2SVoipBlockSettingRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendS2SVoipBlockSettingRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendS2SVoipBlockSettingResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.S2SVoipBlockSettingRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.S2SVoipBlockSettingRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendS2SVoipBlockSettingResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendS2SVoipBlockSettingResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 14.S2S GVOIP invite broadcast
func ProcS2SGvoipInviteBroadCast(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		return false
	}
	// add static
	attr := "gomuc/s2s_gvoip_begin_bc_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetGvoipInviteBroadcastReqbody()
	createUid := subReqBody.GetCreateUid()
	createName := subReqBody.GetCreateName()
	roomId := subReqBody.GetRoomId()
	channelId := subReqBody.GetChannelId()
	timeStamp := subReqBody.GetTimestamp()

	infoLog.Printf("ProcS2SGvoipInviteBroadCast recv from=%v to=%v cmd=%v seq=%v createUid=%v createName=%s roomId=%v channelId=%s ts=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		createUid,
		createName,
		roomId,
		channelId,
		timeStamp)
	// first check input param
	if createUid == 0 || roomId == 0 {
		infoLog.Printf("ProcS2SGvoipInviteBroadCast invalid param")
		return false
	}
	strMsgId := fmt.Sprintf("MSG_%v", timeStamp)
	pushInfo := util.MucPushInfo{PushType: uint8(util.PUSH_GVOIP), NickName: string(createName), MsgId: strMsgId}

	// second Broad cast
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcS2SGvoipInviteBroadCast proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	// build xtpacket for old version
	var packetPayLoad []byte
	common.MarshalUint32(createUid, &packetPayLoad)
	common.MarshalSlice(createName, &packetPayLoad)
	common.MarshalUint32(roomId, &packetPayLoad)
	common.MarshalSlice(channelId, &packetPayLoad)
	common.MarshalUint64(timeStamp, &packetPayLoad)

	err = roomManager.BroadcastMucMessageCompatible(roomId, head, bcSlice, &pushInfo, 0, nil, packetPayLoad, true, false, 0)
	if err != nil {
		infoLog.Printf("ProcS2SGvoipInviteBroadCast modify faield creatUid=%v createName=%s roomId=%v channelId=%s timestamp=%v err=%v",
			createUid,
			createName,
			roomId,
			channelId,
			timeStamp,
			err)
		return false
	}
	return true
}

// 15.S2S GVOIP End broadcast
func ProcS2SGvoipEndBroadCast(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		return false
	}
	// add static
	attr := "gomuc/s2s_gvoip_end_bc_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetGvoipEndBreadcastReqbody()
	roomId := subReqBody.GetRoomId()
	channelId := subReqBody.GetChannelId()
	timeStamp := subReqBody.GetTimestamp()

	infoLog.Printf("ProcS2SGvoipEndBroadCast recv from=%v to=%v cmd=%v seq=%v roomId=%v channelId=%s ts=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		channelId,
		timeStamp)
	// first check input param
	if head.To == 0 || roomId == 0 || head.To != roomId {
		infoLog.Printf("ProcS2SGvoipEndBroadCast invalid param")
		return false
	}

	// second Broad cast
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcS2SGvoipEndBroadCast proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	var packetPayLoad []byte
	common.MarshalUint32(roomId, &packetPayLoad)
	common.MarshalSlice(channelId, &packetPayLoad)
	common.MarshalUint64(timeStamp, &packetPayLoad)

	// second Broad cast
	err = roomManager.BroadcastMucMessageCompatible(roomId, head, bcSlice, nil, 0, nil, packetPayLoad, false, false, 0)
	if err != nil {
		infoLog.Printf("ProcS2SGvoipEndBroadCast modify faield roomId=%v channelId=%s timestamp=%v err=%v",
			roomId,
			channelId,
			timeStamp,
			err)
		return false
	}
	return true
}

// 16.S2S GVOIP member change broadcast
func ProcS2SGvoipMemberChangeBroadCast(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		return false
	}
	// add static
	attr := "gomuc/s2s_gvoip_member_change_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetGvoipMemberChangeBraodcastReqbody()
	roomId := subReqBody.GetRoomId()
	channelId := subReqBody.GetChannelId()
	changeUid := subReqBody.GetChangeUid()
	memberCount := subReqBody.GetMemberCount()
	watcherCount := subReqBody.GetTotalWatcherCount()
	totalList := subReqBody.GetTotalWatcherList()

	infoLog.Printf("ProcS2SGvoipMemberChangeBroadCast recv from=%v to=%v cmd=%v seq=%v roomId=%v channelId=%s changeUid=%v memberCount=%v watcheCount=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		channelId,
		changeUid,
		memberCount,
		watcherCount)
	// first check input param
	if head.From == 0 || head.To == 0 || roomId == 0 || head.From != changeUid || head.To != roomId {
		infoLog.Printf("ProcS2SGvoipMemberChangeBroadCast invalid param")
		return false
	}

	bcReqBody := &ht_muc.MucReqBody{}
	subReqBody.TotalWatcherList = nil
	bcReqBody.GvoipMemberChangeBraodcastReqbody = subReqBody
	sliceBody, err := proto.Marshal(bcReqBody)
	if err != nil {
		infoLog.Printf("ProcS2SGvoipMemberChangeBroadCast proto marshal err=%v", err)
		return false
	}

	var packetPayLoad []byte
	common.MarshalUint32(roomId, &packetPayLoad)
	common.MarshalSlice(channelId, &packetPayLoad)
	common.MarshalUint32(changeUid, &packetPayLoad)
	common.MarshalUint32(memberCount, &packetPayLoad)
	common.MarshalUint32(watcherCount, &packetPayLoad)

	// second Broad cast
	err = roomManager.MultiCastNotificationCompatible(totalList, head, sliceBody, true, packetPayLoad)
	if err != nil {
		infoLog.Printf("ProcS2SGvoipMemberChangeBroadCast modify faield roomId=%v channelId=%s err=%v",
			roomId,
			channelId,
			err)
		return false
	}
	return true
}

// 17.proc member join request
func ProcRequestJoinRoom(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRequestJoinRoomRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}
	// add static
	attr := "gomuc/join_room_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetRequestJoinRoomReqbody()
	roomId := subReqBody.GetRoomId()
	inviter := subReqBody.GetInviterInfo()
	proInviteeList := subReqBody.GetInviteeInfo()
	reqCount := subReqBody.GetReqCount()
	var inviteeList []*ht_muc.RoomMemberInfo
	for _, v := range proInviteeList {
		if v.GetUid() != 0 {
			inviteeList = append(inviteeList, v)
		} else {
			infoLog.Printf("ProcRequestJoinRoom member=#v error", *v)
		}
	}
	roomIdFrom := subReqBody.GetRoomIdFrom()
	msgId := subReqBody.GetMsgId()
	infoLog.Printf("ProcRequestJoinRoom recv from=%v to=%v cmd=%v seq=%v roomId=%v inviter=%v count=%v roomIdFrom=%v msgId=%s reqCount=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		inviter.GetUid(),
		len(inviteeList),
		roomIdFrom,
		msgId,
		reqCount)

	infoLog.Printf("ProcRequestJoinRoom roomId=%v invter=%v inviteeList=%v",
		roomId,
		inviter.GetUid(),
		inviteeList)

	// first check input param
	if roomId == 0 || inviter.GetUid() == 0 || len(inviteeList) == 0 || len(msgId) == 0 {
		infoLog.Printf("ProcRequestJoinRoom invalid param roomId=%v inviterUid=%v membercount=%v msgId=%s",
			roomId,
			inviter.GetUid(),
			len(inviteeList),
			msgId)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}
	// 如果head.From 不等于邀请者的uid 则为新版本用户发出的申请
	// 首先检验用户是否已经在群聊内
	if reqCount > 1 && head.From != inviter.GetUid() {
		infoLog.Printf("ProcRequestJoinRoom from=%v roomId=%v inviter=%v roomIdFrom=%v reqCount=%v",
			head.From,
			roomId,
			inviter.GetUid(),
			roomIdFrom,
			reqCount)
		bResult, err := roomManager.IsUserAlreadyIn(roomId, inviteeList[0].GetUid())
		if err != nil {
			infoLog.Printf("ProcRequestJoinRoom from=%v roomId=%v inviter=%v roomIdFrom=%v reqCount=%v err=%s",
				head.From,
				roomId,
				inviter.GetUid(),
				roomIdFrom,
				reqCount,
				err)
			result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
			errMsg = []byte("check is admin failed")
			return false
		}
		if bResult {
			// 返回响应加群成功
			subMucRspBody := new(ht_muc.RequestJoinRoomRspBody)
			subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_MUC_ALREADT_IN))}
			subMucRspBody.RoomTimestamp = proto.Uint64(0)
			SendRequestJoinRoomRsp(c, head, subMucRspBody)
			bNeedCall = false
			return true
		}

		if reqCount > REQ_THRESHOLD {
			result = uint16(ht_muc.MUC_RET_CODE_RET_INVALID_INVITE_LINK)
			errMsg = []byte("invalid invite link")
			return false
		}
	}
	// 检查邀请者是否已经被 被邀请者拉黑
	blackMeList, err := DbUtil.GetBlackMeList(inviter.GetUid())
	if err != nil {
		infoLog.Printf("ProcRequestJoinRoom GetBlackMeList failed uid=%v", inviter.GetUid())
	}
	var totalBalck []uint32
	if len(blackMeList) != 0 {
		for i, v := range inviteeList {
			infoLog.Printf("ProcRequestJoinRoom black me index=%v uid=%v", i, v.GetUid())
			if UidIsInSlice(blackMeList, v.GetUid()) {
				infoLog.Printf("ProcRequestJoinRoom uid=%v is in blackMeList", v.GetUid())
				totalBalck = append(totalBalck, v.GetUid())
			}
		}
	}
	if len(totalBalck) != 0 {
		subMucRspBody := new(ht_muc.RequestJoinRoomRspBody)
		errMsg := []byte("some one black me")
		subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SOME_ONE_BLACK_ME)), Reason: errMsg}
		subMucRspBody.ListBlackMe = totalBalck
		SendRequestJoinRoomRsp(c, head, subMucRspBody)
		bNeedCall = false
		return false
	}
	// 首先检查是否超过群成员总数限制
	bResult, err := roomManager.IsExceedRoomMemberLimit(roomId, uint32(len(inviteeList)))
	if err != nil {
		infoLog.Printf("ProcRequestJoinRoom internal error roomId=%v err=%v", roomId, err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}
	// 超过群成员总数限制
	if bResult {
		infoLog.Printf("ProcRequestJoinRoom exceed member limit roomId=%v", roomId)
		result = uint16(ht_muc.MUC_RET_CODE_RET_MEMBER_EXEC_LIMIT)
		errMsg = []byte("member exceed limit")
		return false
	}

	// 检查邀请者是否是管理员如果是管理员直接加群并广播
	// 获取群聊确认
	bIsOpenVerify, err := roomManager.IsOpenVerify(roomId)
	if err != nil {
		infoLog.Printf("ProcRequestJoinRoom roomManager.IsOpenVerify exec failed roomId=%v", roomId)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("get verify stat failed")
		return false
	}
	bAdmin, err := roomManager.IsUserAdmin(roomId, inviter.GetUid())
	if err != nil {
		infoLog.Printf("ProcRequestJoinRoom roomManager.IsUserAdmin exec failed roomId=%v", roomId)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("check is admin failed")
		return false
	}

	if !bIsOpenVerify || (bAdmin && roomIdFrom == ROOM_ID_FROM_INVITE) {
		roomTS, err := roomManager.AddMember(roomId, inviter, inviteeList)
		if err != nil {
			if err == util.ErrAlreadyIn {
				// 返回响应加群成功
				subMucRspBody := new(ht_muc.RequestJoinRoomRspBody)
				subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_MUC_ALREADT_IN))}
				subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
				SendRequestJoinRoomRsp(c, head, subMucRspBody)
				bNeedCall = false
				return true
			} else {
				infoLog.Printf("ProcRequestJoinRoom internal error roomId=%v err=%v", roomId, err)
				result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
				errMsg = []byte("internal error")
				return false
			}
		}
		excludeInviter := true
		if head.From != inviter.GetUid() {
			excludeInviter = false
		}
		err = roomManager.NotifyInviteMember(inviter, inviter, inviteeList, roomId, uint64(roomTS), roomIdFrom, excludeInviter)
		if err != nil {
			infoLog.Printf("ProcRequestJoinRoom NotifyInviteMember roomId=%v inviterId=%v inviteeListCount=%v failed err=%v",
				roomId,
				inviter.GetUid(),
				len(inviteeList),
				err)
		}

		// infoLog.Printf("DEBUG ProcRequestJoinRoom roomId=%v roomTS=%v", roomId, roomTS)
		// 返回响应加群成功
		subMucRspBody := new(ht_muc.RequestJoinRoomRspBody)
		subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
		subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
		SendRequestJoinRoomRsp(c, head, subMucRspBody)
		bNeedCall = false
	} else { // 检查失败或者不是管理员
		// 首先将请求转发到管理员
		// second Broad cast
		reqSlice, err := proto.Marshal(reqBody)
		if err != nil {
			infoLog.Printf("ProcRequestJoinRoom proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
				head.From,
				head.To,
				head.Cmd,
				head.Seq)
			result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
			errMsg = []byte("internal error")
			return false
		}
		roomTS, err := roomManager.NotifyAdminRequestJoin(roomId, head, reqSlice)
		if err != nil {
			infoLog.Printf("ProcRequestJoinRoom internal error roomId=%v err=%v", roomId, err)
			result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
			errMsg = []byte("internal error")
			return false
		}

		// infoLog.Printf("DEBUG ProcRequestJoinRoom roomId=%v roomTS=%v", roomId, roomTS)
		// 返回响应请求成功
		subMucRspBody := new(ht_muc.RequestJoinRoomRspBody)
		subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_WAIT_ADMIN_VERIFY))}
		subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
		SendRequestJoinRoomRsp(c, head, subMucRspBody)
		bNeedCall = false
	}

	// 2017-12-28 发布事件到NSQ
	if len(inviteeList) > 0 {
		err = PublishActionEvent(inviteeList[0].GetUid(), roomId, inviter.GetUid(), AT_INIT_JOIN_ROOM)
		if err != nil {
			// add static
			attr := "gomuc/publish_to_nsq_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcQuitRoom PublishPostEvent uid=%v roomId=%v actionType=%v failed",
				inviter.GetUid(),
				roomId,
				AT_INIT_JOIN_ROOM)
		}
	}

	return true
}
func SendRequestJoinRoomRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.RequestJoinRoomRspbody = new(ht_muc.RequestJoinRoomRspBody)
	subMucRspBody := rspBody.GetRequestJoinRoomRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRequestJoinRoomRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendRequestJoinRoomRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendRequestJoinRoomRsp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.RequestJoinRoomRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.RequestJoinRoomRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendRequestJoinRoomRsp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendRequestJoinRoomRsp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 18.proc muc open verify
func ProcMucOpenVerify(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendMucOpenVerifyRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/open_verify_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetMucOpenVerifyReqbody()
	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	opStat := subReqBody.GetOpType()
	infoLog.Printf("ProcMucOpenVerify recv from=%v to=%v cmd=%v seq=%v roomId=%v reqUid=%v opStat=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		reqUid,
		opStat)

	// first check input param
	if roomId == 0 || reqUid == 0 {
		infoLog.Printf("ProcMucOpenVerify invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}
	// 首先检查群求者是否是群创建者
	bResult, err := roomManager.IsCreateUid(roomId, reqUid)
	if err != nil {
		infoLog.Printf("ProcMucOpenVerify roomManager.IsCreateUid() failed roomId=%v reqUid=%v",
			roomId,
			reqUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}

	if !bResult {
		// 操作用户不是创建者 返回错误
		infoLog.Printf("ProcMucOpenVerify roomId=%v reqUid=%v is not creater",
			roomId,
			reqUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_PERMISSION_DENIED)
		errMsg = []byte("Internal error")
		return false
	}
	// 否则设置群验证状态
	roomTS, err := roomManager.UpdateVerifyStat(roomId, uint32(opStat))
	if err != nil {
		infoLog.Printf("ProcMucOpenVerify UpdateVerifyStat failed roomId=%v reqUid=%v opStat=%v",
			roomId,
			reqUid,
			opStat)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	err = roomManager.NotifyOpenVerify(roomId, reqUid, uint32(opStat), roomTS)
	if err != nil {
		infoLog.Printf("ProcMucOpenVerify NotifyOpenVerify failed roomId=%v reqUid=%v opStat=%v",
			roomId,
			reqUid,
			opStat)
	}

	// infoLog.Printf("DEBUG ProcMucOpenVerify roomId=%v roomTS=%v", roomId, roomTS)
	// 返回响应请求成功
	subMucRspBody := new(ht_muc.MucOpenVerifyRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	SendMucOpenVerifyRsp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}

func SendMucOpenVerifyRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucOpenVerifyRspbody = new(ht_muc.MucOpenVerifyRspBody)
	subMucRspBody := rspBody.GetMucOpenVerifyRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucOpenVerifyRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucOpenVerifyRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendMucOpenVerifyRsp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.MucOpenVerifyRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucOpenVerifyRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucOpenVerifyRsp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucOpenVerifyRsp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 19.proc admin process join room request
func ProcMucJoinRoomHandle(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendMucJoinRoomHandleRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/join_room_handle_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetMcuJoinRoomHandleReqbody()
	roomId := subReqBody.GetRoomId()
	opUidInfo := subReqBody.GetOpUidInfo()
	inviterInfo := subReqBody.GetInviterInfo()
	inviteeInfo := subReqBody.GetInviteeInfo()
	handleType := subReqBody.GetHandleType()
	roomIdFrom := subReqBody.GetRoomIdFrom()
	msgId := subReqBody.GetMsgId()
	infoLog.Printf("ProcMucJoinRoomHandle recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v inviter=%v invitee=%v type=%v roomIdFrom=%v msgId=%s",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUidInfo.GetUid(),
		inviterInfo.GetUid(),
		inviteeInfo.GetUid(),
		handleType,
		roomIdFrom,
		msgId)

	// first check input param
	if roomId == 0 || opUidInfo.GetUid() == 0 || inviterInfo.GetUid() == 0 || inviteeInfo.GetUid() == 0 || handleType > ht_muc.HANDLE_OP_TYPE_ENUM_HANDLE_ACCEPT {
		infoLog.Printf("ProcMucJoinRoomHandle invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// 然后检查操作者是否是管理员
	bRet, err := roomManager.IsUserAdmin(roomId, opUidInfo.GetUid())
	if err != nil {
		infoLog.Printf("ProcMucJoinRoomHandle exec IsUserAdmin failed roomId=%v opUid=%v", roomId, opUidInfo.GetUid())
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	if !bRet {
		infoLog.Printf("ProcMucJoinRoomHandle roomId=%v opUid=%v is not admin", roomId, opUidInfo.GetUid())
		result = uint16(ht_muc.MUC_RET_CODE_RET_PERMISSION_DENIED)
		errMsg = []byte("permission denied")
		return false
	}

	// 是管理员 继续处理结果
	var roomTS int64 = 0
	switch handleType {
	case ht_muc.HANDLE_OP_TYPE_ENUM_HANDLE_REJECT:
		{ // 被拒绝 需要通知邀请者和其他管理员
			var targetList []uint32
			// 获取群资料时间戳
			roomInfo, err := roomManager.GetRoomInfo(roomId)
			if err != nil {
				infoLog.Printf("ProcMucJoinRoomHandle GetRoomInfo roomId=%v uid=%v failed",
					roomId,
					inviterInfo.GetUid())
				result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
				errMsg = []byte("Internal error")
				return false
			}

			if opUidInfo.GetUid() != roomInfo.CreateUid {
				targetList = append(targetList, roomInfo.CreateUid)
			}
			adminList := roomInfo.AdminList
			for _, v := range adminList {
				if opUidInfo.GetUid() != v {
					targetList = append(targetList, v)
				}
			}

			if roomIdFrom == uint32(ht_muc.ROOMID_FROM_TYPE_ENUM_FROM_INVITE) {
				targetList = append(targetList, inviterInfo.GetUid())
			} else if roomIdFrom == uint32(ht_muc.ROOMID_FROM_TYPE_ENUM_FROM_QRCODE) {
				targetList = append(targetList, inviteeInfo.GetUid())
			}

			reqSlice, err := proto.Marshal(reqBody)
			if err != nil {
				infoLog.Printf("ProcMucJoinRoomHandle proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
					head.From,
					head.To,
					head.Cmd,
					head.Seq)
				result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
				errMsg = []byte("internal error")
				return false
			}
			err = roomManager.MultiCastNotification(targetList, head, reqSlice, false)
			if err != nil {
				infoLog.Printf("ProcMucJoinRoomHandle MultiCastNotification roomId=%v count=%v failed",
					roomId,
					len(targetList))
				result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
				errMsg = []byte("Internal error")
				return false
			}
			roomTS = roomInfo.RoomTS
		}
	case ht_muc.HANDLE_OP_TYPE_ENUM_HANDLE_ACCEPT:
		{ // 接受用户 首先将用户加入群聊 然后进行广播
			addList := []*ht_muc.RoomMemberInfo{inviteeInfo}
			roomTS, err = roomManager.AddMember(roomId, opUidInfo, addList)
			if err != nil {
				if err == util.ErrExecLimit {
					infoLog.Printf("ProcMucJoinRoomHandle error roomId=%v err=%v", roomId, err)
					result = uint16(ht_muc.MUC_RET_CODE_RET_MEMBER_EXEC_LIMIT)
					errMsg = []byte("exceed member limit")
				} else {
					infoLog.Printf("ProcMucJoinRoomHandle internal error roomId=%v err=%v", roomId, err)
					result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
					errMsg = []byte("internal error")
				}
				return false
			}
			err = roomManager.NotifyAdminPromotJoin(opUidInfo, inviterInfo, addList, roomId, uint64(roomTS), roomIdFrom, msgId)
			if err != nil {
				infoLog.Printf("ProcMucJoinRoomHandle NotifyInviteMember roomId=%v inviterId=%v invitee=%v failed err=%v",
					roomId,
					opUidInfo.GetUid(),
					inviteeInfo.GetUid(),
					err)

			}
		}
	default:
		infoLog.Printf("ProcMucJoinRoomHandle roomId=%v opUid=%v reqUid=%v unKnow tyep=%v",
			roomId,
			opUidInfo.GetUid(),
			inviteeInfo.GetUid(),
			handleType)
	}

	// infoLog.Printf("DEBUG ProcMucJoinRoomHandle roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := new(ht_muc.MucJoinRoomHandleRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	SendMucJoinRoomHandleResp(c, head, subMucRspBody)
	bNeedCall = false
	err = PublishActionEvent(inviteeInfo.GetUid(), roomId, opUidInfo.GetUid(), AT_INIT_JOIN_ROOM)
	if err != nil {
		// add static
		attr := "gomuc/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcMucJoinRoomHandle PublishPostEvent uid=%v roomId=%v actionType=%v failed",
			opUidInfo.GetUid(),
			roomId,
			AT_INIT_JOIN_ROOM)
	}

	return true
}
func SendMucJoinRoomHandleRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.McuJoinRoomHandleRspbody = new(ht_muc.MucJoinRoomHandleRspBody)
	subMucRspBody := rspBody.GetMcuJoinRoomHandleRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucJoinRoomHandleRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucJoinRoomHandleRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendMucJoinRoomHandleResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.MucJoinRoomHandleRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.McuJoinRoomHandleRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucJoinRoomHandleResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucJoinRoomHandleResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 20.proc set admind req
func ProcMucSetAdmin(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendMucSetAdminRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/muc_set_admin_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetMucSetAdminReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	adminList := subReqBody.GetMembers()
	infoLog.Printf("ProcMucSetAdmin recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v adminSize=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid,
		len(adminList))
	// first check input param
	if roomId == 0 || opUid == 0 {
		infoLog.Printf("ProcMucSetAdmin invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second check whether op user is admin
	bRet, err := roomManager.IsCreateUid(roomId, opUid)
	if err != nil {
		infoLog.Printf("ProcMucSetAdmin exec IsCreateUid failed roomId=%v opUid=%v err=%v", roomId, opUid, err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	// third the op uid is not admin send err rsp
	if !bRet {
		infoLog.Printf("ProcMucSetAdmin user is not creater roomId=%v opUid=%v err=%v", roomId, opUid, err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_PERMISSION_DENIED)
		errMsg = []byte("Internal error")
		return false
	}

	// second get room Info
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcMucSetAdmin roomId=%v opUid=%v get room info failed", roomId, opUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	// get prev admin list
	prevAdminList := roomInfo.AdminList

	// forth set new admin list
	roomTS, err := roomManager.SetAdminList(roomId, opUid, adminList)
	if err != nil {
		infoLog.Printf("ProcMucSetAdmin roomManager.SetAdminList failed roomId=%v opUid=%v adminCount=%v",
			roomId,
			opUid,
			len(adminList))
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}
	// var notifyList []*ht_muc.RoomMemberInfo
	// for _, v := range adminList {
	// 	if UidIsInSlice(prevAdminList, v.GetUid()) {
	// 		infoLog.Printf("ProcMucSetAdmin uid=%v is already admin not notify", v.GetUid())
	// 	} else {
	// 		notifyList = append(notifyList, v)
	// 	}
	// }
	// fifth broadcast notifycation
	err = roomManager.NotifySetAdmin(roomId, opUid, adminList, roomTS)
	if err != nil {
		infoLog.Printf("ProcMucSetAdmin roomManager.NotifySetAdmin failed roomId=%v opUid=%v",
			roomId,
			opUid)
	}
	var newAdminList []uint32
	for _, v := range adminList {
		newAdminList = append(newAdminList, v.GetUid())
	}
	// infoLog.Printf("DEBUG ProcMucSetAdmin roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := new(ht_muc.MucSetAdminRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	SendMucSetAdminResp(c, head, subMucRspBody)
	bNeedCall = false

	// 取消某个管理员的身份通知grouplesson 删除群组的课程
	for _, v := range prevAdminList {
		if !UidIsInSlice(newAdminList, v) {
			infoLog.Printf("ProcMucSetAdmin roomId=%v uid=%v abrogate admin",
				roomId,
				v)
			err = groupLessonApi.DelGroupLessonByUid(roomId, v)
			if err != nil {
				infoLog.Printf("ProcMucSetAdmin groupLessonApi.DelGroupLessonByUid roomId=%v createrUid=%v err=%s",
					roomId,
					v,
					err)
			}
		}
	}

	return true
}
func SendMucSetAdminRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucSetAdminRspbody = new(ht_muc.MucSetAdminRspBody)
	subMucRspBody := rspBody.GetMucSetAdminRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucSetAdminRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucSetAdminRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendMucSetAdminResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.MucSetAdminRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucSetAdminRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucSetAdminResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucSetAdminResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 21.proc create user right transfer
func ProcCreateUserAuthTrans(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendCreateUserAuthTransRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}
	// add static
	attr := "gomuc/create_auth_trans_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetCreateUserTransReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	memberInfo := subReqBody.GetMember()
	infoLog.Printf("ProcCreateUserAuthTrans recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v memeberUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid,
		memberInfo.GetUid())
	// first check input param
	if roomId == 0 || opUid == 0 || memberInfo.GetUid() == 0 {
		infoLog.Printf("ProcCreateUserAuthTrans invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second check whether op user is admin
	bRet, err := roomManager.IsCreateUid(roomId, opUid)
	if err != nil {
		infoLog.Printf("ProcCreateUserAuthTrans exec IsCreateUid failed roomId=%v opUid=%v err=%v", roomId, opUid, err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	// third the op uid is not admin send err rsp
	if !bRet {
		infoLog.Printf("ProcCreateUserAuthTrans user is not creater roomId=%v opUid=%v err=%v", roomId, opUid, err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_PERMISSION_DENIED)
		errMsg = []byte("Internal error")
		return false
	}

	// 如果是群主转让则检查该群是否发起了课程收费
	if bRet {
		exist, err := walletApi.GetRoomExistLessonCharging(roomId, opUid)
		if err != nil {
			infoLog.Printf("ProcCreateUserAuthTrans roomId=%v opUid=%v GetRoomExistLessonCharging failed err=%s",
				roomId,
				opUid,
				err)
			result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
			errMsg = []byte("internal error error")
			return false
		}
		infoLog.Printf("ProcCreateUserAuthTrans roomId=%v opUid=%v exist=%v", roomId, opUid, exist)
		if exist {
			infoLog.Printf("ProcQuitRoom roomId=%v opUid=%v exist lesson charging", roomId, opUid)
			result = uint16(ht_muc.MUC_RET_CODE_RET_LESSON_IS_CHARGING)
			errMsg = []byte("lesson is charging")
			return false
		}
	}
	// forth set new admin list
	roomTS, err := roomManager.SetCreateUid(roomId, opUid, memberInfo.GetUid())
	if err != nil {
		infoLog.Printf("ProcCreateUserAuthTrans roomManager.SetAdminList failed roomId=%v opUid=%v memberUid=%v",
			roomId,
			opUid,
			memberInfo.GetUid())
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}
	// fifth broadcast notifycation
	err = roomManager.NotifyCreateUserAuthTrans(roomId, opUid, memberInfo, roomTS)
	if err != nil {
		infoLog.Printf("ProcCreateUserAuthTrans roomManager.NotifyCreateUserAuthTrans failed roomId=%v opUid=%v",
			roomId,
			opUid)
	}

	// infoLog.Printf("DEBUG ProcCreateUserAuthTrans roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := new(ht_muc.CreateUserAuthTransRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))
	SendCreateUserAuthTransResp(c, head, subMucRspBody)
	bNeedCall = false

	// 群组转让通知grouplesson 删除群组的课程
	err = groupLessonApi.DelGroupLessonByUid(roomId, opUid)
	if err != nil {
		infoLog.Printf("ProcCreateUserAuthTrans groupLessonApi.DelGroupLessonByUid roomId=%v createrUid=%v err=%s",
			roomId,
			opUid,
			err)
	}
	return true
}
func SendCreateUserAuthTransRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.CreateUserTransRspbody = new(ht_muc.CreateUserAuthTransRspBody)
	subMucRspBody := rspBody.GetCreateUserTransRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendCreateUserAuthTransRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendCreateUserAuthTransRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendCreateUserAuthTransResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.CreateUserAuthTransRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.CreateUserTransRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendCreateUserAuthTransResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendCreateUserAuthTransResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 22.proc Get room base info
func ProcMucGetRoomBaseInfo(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendMucGetRoomBaseInfoRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/get_room_base_info_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetMucGetRoomBaseInfoReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetReqUid()
	infoLog.Printf("ProcMucGetRoomBaseInfo recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid)
	// first check input param
	if roomId == 0 || opUid == 0 {
		infoLog.Printf("ProcMucGetRoomBaseInfo invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second get room Info
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcMucGetRoomBaseInfo roomId=%v opUid=%v get room info failed", roomId, opUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}

	// sixth send ack
	subMucRspBody := new(ht_muc.MucGetRoomBaseInfoRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomInfo.RoomTS))
	verify := ht_muc.VERIFY_STAT(roomInfo.VerifyStat)
	subMucRspBody.BaseInfo = &ht_muc.RoomBaseInfo{
		CreateUid:    proto.Uint32(roomInfo.CreateUid),
		ListAdminUid: roomInfo.AdminList[:],
		RoomLimit:    proto.Uint32(roomInfo.MemberLimit),
		VerifyStat:   &verify,
		Announcement: &ht_muc.AnnoType{
			PublishUid:  proto.Uint32(roomInfo.Announcement.PublishUid),
			PublishTs:   proto.Uint32(roomInfo.Announcement.PublishTS),
			AnnoContent: []byte(roomInfo.Announcement.AnnoContect),
		},
	}
	// infoLog.Printf("DEBUG ProcMucGetRoomBaseInfo baseInfo=%#v", subMucRspBody)

	SendMucGetRoomBaseInfoResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}
func SendMucGetRoomBaseInfoRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucGetRoomBaseInfoRspbody = new(ht_muc.MucGetRoomBaseInfoRspBody)
	subMucRspBody := rspBody.GetMucGetRoomBaseInfoRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucGetRoomBaseInfoRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucGetRoomBaseInfoRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendMucGetRoomBaseInfoResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.MucGetRoomBaseInfoRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucGetRoomBaseInfoRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucGetRoomBaseInfoResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucGetRoomBaseInfoResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 23.proc create user right transfer
func ProcMucSetRoomAnnouncement(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendMucSetRoomAnnoRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/set_room_anno_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetMucSetRoomAnnouncementReqbody()
	roomId := subReqBody.GetRoomId()
	anno := subReqBody.GetAnnouncement()
	infoLog.Printf("ProcMucSetRoomAnnouncement recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v ts=%v content=%s",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		anno.GetPublishUid(),
		anno.GetPublishTs(),
		anno.GetAnnoContent())
	// first check input param
	if roomId == 0 || anno.GetPublishUid() == 0 {
		infoLog.Printf("ProcMucSetRoomAnnouncement invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second check whether op user is admin
	bRet, err := roomManager.IsUserAdmin(roomId, anno.GetPublishUid())
	if err != nil {
		infoLog.Printf("ProcMucSetRoomAnnouncement exec IsUserAdmin failed roomId=%v opUid=%v err=%v", roomId, anno.GetPublishUid(), err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	// third the op uid is not admin send err rsp
	if !bRet {
		infoLog.Printf("ProcMucSetRoomAnnouncement user is not creater roomId=%v opUid=%v err=%v", roomId, anno.GetPublishUid(), err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_PERMISSION_DENIED)
		errMsg = []byte("Internal error")
		return false
	}
	// 群公告发布时间一服务端时间戳为准
	anno.PublishTs = proto.Uint32(uint32(time.Now().Unix()))

	// forth set new announcement
	roomTS, err := roomManager.SetAnnouncement(roomId, anno)
	if err != nil {
		infoLog.Printf("ProcMucSetRoomAnnouncement roomManager.SetAdminList failed roomId=%v publishUid=%v publishTs=%v",
			roomId,
			anno.GetPublishUid(),
			anno.GetPublishTs())
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}
	// fifth broadcast notifycation
	err = roomManager.NotifySetAnnouncement(roomId, anno, roomTS)
	if err != nil {
		infoLog.Printf("ProcMucSetRoomAnnouncement roomManager.NotifySetAnnouncement failed roomId=%v publishUid=%v",
			roomId,
			anno.GetPublishUid())
	}

	// sixth send ack
	subMucRspBody := new(ht_muc.MucSetRoomAnnouncementRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTS))

	// infoLog.Printf("DEBUG ProcMucSetRoomAnnouncement roomId=%v roomTS=%v", roomId, roomTS)
	SendMucSetRoomAnnoResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}
func SendMucSetRoomAnnoRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucSetRoomAnnouncementRspbody = new(ht_muc.MucSetRoomAnnouncementRspBody)
	subMucRspBody := rspBody.GetMucSetRoomAnnouncementRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucSetRoomAnnoRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucSetRoomAnnoRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func SendMucSetRoomAnnoResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.MucSetRoomAnnouncementRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.MucSetRoomAnnouncementRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendMucSetRoomAnnoResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendMucSetRoomAnnoResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 24.Proc QueryUserIsAlreadyInRoom
func ProcQueryUserIsAlreadyInRoom(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendQueryUserIsAlreadyInRoomRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/query_user_is_in_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetQueryUserIsAlreadyInReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetOpUid()
	infoLog.Printf("ProcQueryUserIsAlreadyInRoom recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid)
	// first check input param
	if roomId == 0 || opUid == 0 {
		infoLog.Printf("ProcQueryUserIsAlreadyInRoom invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second get room Info
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcQueryUserIsAlreadyInRoom roomId=%v opUid=%v get room info failed", roomId, opUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}

	var bInRoom bool = false
	memberList := roomInfo.MemberList
	for _, v := range memberList {
		if v.Uid == opUid {
			bInRoom = true
		}
	}

	subMucRspBody := new(ht_muc.QueryUserIsAlreadyInRoomRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.IsInRoom = proto.Bool(bInRoom)

	// infoLog.Printf("DEBUG ProcQueryUserIsAlreadyInRoom subMucRspBody=%#v", subMucRspBody)
	SendQueryUserIsAlreadyInRoomResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}
func SendQueryUserIsAlreadyInRoomRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.QueryUserIsAlreadyInRspbody = new(ht_muc.QueryUserIsAlreadyInRoomRspBody)
	subMucRspBody := rspBody.GetQueryUserIsAlreadyInRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendQueryUserIsAlreadyInRoomRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendQueryUserIsAlreadyInRoomRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendQueryUserIsAlreadyInRoomResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.QueryUserIsAlreadyInRoomRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.QueryUserIsAlreadyInRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendQueryUserIsAlreadyInRoomResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendQueryUserIsAlreadyInRoomResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 25.Proc QueryQRcode info
func ProcQueryQRcodeInfo(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendQueryQRcodeInfoRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/query_orcode_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetQueryQrcodeInfoReqbody()
	roomId := subReqBody.GetRoomId()
	scanUid := subReqBody.GetScanUid()
	shareUid := subReqBody.GetShareId()
	maxShowCnt := subReqBody.GetMaxShowCnt()
	infoLog.Printf("ProcQueryQRcodeInfo recv from=%v to=%v cmd=%v seq=%v roomId=%v scanUid=%v shareUid=%v maxshowCnt=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		scanUid,
		shareUid,
		maxShowCnt)
	// first check input param
	if roomId == 0 || scanUid == 0 || shareUid == 0 {
		infoLog.Printf("ProcQueryQRcodeInfo invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second get room Info
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcQueryQRcodeInfo roomId=%v scanUid=%v get room info failed", roomId, scanUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}

	memberList := roomInfo.MemberList
	if maxShowCnt > uint32(len(memberList)) {
		maxShowCnt = uint32(len(memberList))
	}
	var bScanInRoom bool
	var bShareInRoom bool
	var subMemberList []uint32

	for i, v := range memberList {
		if v.Uid == scanUid {
			bScanInRoom = true
		}
		if v.Uid == shareUid {
			bShareInRoom = true
		}

		if uint32(i) < maxShowCnt {
			subMemberList = append(subMemberList, v.Uid)
		}
	}

	subMucRspBody := new(ht_muc.QueryQRcodeInfoRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	subMucRspBody.IsScanerInRoom = proto.Bool(bScanInRoom)
	subMucRspBody.IsSharerInRoom = proto.Bool(bShareInRoom)
	subMucRspBody.RoomName = []byte(roomInfo.RoomName)
	subMucRspBody.MemberCount = proto.Uint32(uint32(len(memberList)))
	subMucRspBody.Userids = subMemberList
	subMucRspBody.RoomAvatar = []byte(roomInfo.RoomAvatar)

	infoLog.Printf("DEBUG ProcQueryQRcodeInfo RoomName=%s roomId=%v IsScanerInRoom=%v IsSharerInRoom=%v MemberCount=%v Userids=%v",
		subMucRspBody.GetRoomName(),
		roomId,
		subMucRspBody.GetIsScanerInRoom(),
		subMucRspBody.GetIsSharerInRoom(),
		subMucRspBody.GetMemberCount(),
		subMucRspBody.GetUserids())

	SendQueryQRcodeInfoResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}

func SendQueryQRcodeInfoRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.QueryQrcodeInfoRspbody = new(ht_muc.QueryQRcodeInfoRspBody)
	subMucRspBody := rspBody.GetQueryQrcodeInfoRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendQueryQRcodeInfoRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendQueryQRcodeInfoRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendQueryQRcodeInfoResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.QueryQRcodeInfoRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.QueryQrcodeInfoRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendQueryQRcodeInfoResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendQueryQRcodeInfoResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 26.Proc update room member limit
func ProcUpdateRoomMemberLimit(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendUpdateRoomMemberLimitRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/update_room_member_limit_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetUpdateRoomMemberLimitReqbody()
	opUid := subReqBody.GetOpUid()

	infoLog.Printf("ProcUpdateRoomMemberLimit recv from=%v to=%v cmd=%v seq=%v opUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		opUid)
	// first check input param
	if opUid == 0 {
		infoLog.Printf("ProcUpdateRoomMemberLimit invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second update room member limit
	err := roomManager.UpdateRoomMemberLimit(opUid)
	if err != nil {
		infoLog.Printf("ProcUpdateRoomMemberLimit internal error")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}
	// third return rsp
	subMucRspBody := new(ht_muc.UpdateRoomMemberLimitRspBody)
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}

	SendUpdateRoomMemberLimitResp(c, head, subMucRspBody)
	infoLog.Printf("ProcUpdateRoomMemberLimit send resp")

	bNeedCall = false
	return true
}

func SendUpdateRoomMemberLimitRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.UpdateRoomMemberLimitRspbody = new(ht_muc.UpdateRoomMemberLimitRspBody)
	subMucRspBody := rspBody.GetUpdateRoomMemberLimitRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendUpdateRoomMemberLimitRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendUpdateRoomMemberLimitRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendUpdateRoomMemberLimitResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.UpdateRoomMemberLimitRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.UpdateRoomMemberLimitRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendUpdateRoomMemberLimitResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendUpdateRoomMemberLimitResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 31.proc get muc message
func ProcBatchGetMucMsg(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendBatchGetMucMsgRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/batch_get_muc_msg_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetBatchGetMucMessageReqbody()
	opUid := subReqBody.GetOpUid()
	cliSeq := subReqBody.GetCliSeqId()
	infoLog.Printf("ProcBatchGetMucMsg recv from=%v to=%v cmd=%v seq=%v opUid=%v cliSeq=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		opUid,
		cliSeq)
	// first check input param
	if opUid == 0 {
		infoLog.Printf("ProcBatchGetMucMsg invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}
	maxMsgId, hasMore, outMsgList, err := mucStoreApi.BatchGetMucMsg(opUid, cliSeq)
	if err != nil {
		infoLog.Printf("ProcBatchGetMucMsg  mucStoreApi.BatchGetMucMsg faild opUid=%v cliSeq=%v err=%s", opUid, cliSeq, err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	subMucRspBody := &ht_muc.BatchGetMucMessageRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
		MsgList:  outMsgList,
		MaxSeqId: proto.Uint64(maxMsgId),
		HasMore:  proto.Uint32(hasMore),
	}
	infoLog.Printf("ProcBatchGetMucMsg opUid=%v msgLen=%v maxSeqId=%v hasMore=%v", opUid, len(outMsgList), maxMsgId, hasMore)
	SendBatchGetMucMsgResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}
func SendBatchGetMucMsgRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.BatchGetMucMessageRspbody = new(ht_muc.BatchGetMucMessageRspBody)
	subMucRspBody := rspBody.GetBatchGetMucMessageRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendBatchGetMucMsgRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendBatchGetMucMsgRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendBatchGetMucMsgResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.BatchGetMucMessageRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.BatchGetMucMessageRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendBatchGetMucMsgResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendBatchGetMucMsgResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 31.proc invite member
func ProcInviteMemberReq(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendInviteMemberRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		infoLog.Printf("ProcInviteMemberReq invalid param")
		bNeedCall = false
		return false
	}
	// add static
	attr := "gomuc/invite_member_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetInviteMemberToGroupReqbody()
	roomId := subReqBody.GetRoomId()
	inviter := subReqBody.GetInviterInfo()
	inviteeList := subReqBody.GetInviteeInfo()
	roomIdFrom := subReqBody.GetRoomIdFrom()
	msgId := subReqBody.GetMsgId()
	roomProfile := subReqBody.GetRoomProfile()
	// 将roomprofile 置为空
	roomProfile.RoomPhotoUrl = nil
	infoLog.Printf("ProcInviteMemberReq recv from=%v to=%v cmd=%v seq=%v roomId=%v inviter=%v count=%v roomIdFrom=%v msgId=%s roomUrl=%s roomAvatar=%s",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		inviter.GetUid(),
		len(inviteeList),
		roomIdFrom,
		msgId,
		roomProfile.GetRoomPhotoUrl(),
		roomProfile.GetRoomAvatar())

	// first check input param
	if roomId == 0 || inviter.GetUid() == 0 || len(inviteeList) == 0 || len(msgId) == 0 {
		infoLog.Printf("ProcInviteMemberReq invalid param roomId=%v inviterUid=%v membercount=%v msgId=%s",
			roomId,
			inviter.GetUid(),
			len(inviteeList),
			msgId)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}
	// 检查邀请者是否已经被 被邀请者拉黑
	blackMeList, err := DbUtil.GetBlackMeList(inviter.GetUid())
	if err != nil {
		infoLog.Printf("ProcInviteMemberReq GetBlackMeList failed uid=%v", inviter.GetUid())
	}
	var totalBalck []uint32
	if len(blackMeList) != 0 {
		for i, v := range inviteeList {
			infoLog.Printf("ProcInviteMemberReq black me index=%v uid=%v", i, v.GetUid())
			if UidIsInSlice(blackMeList, v.GetUid()) {
				infoLog.Printf("ProcInviteMemberReq uid=%v is in blackMeList", v.GetUid())
				totalBalck = append(totalBalck, v.GetUid())
			}
		}
	}
	if len(totalBalck) != 0 {
		subMucRspBody := new(ht_muc.InviteMemberToGroupRspBody)
		errMsg := []byte("some one black me")
		subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SOME_ONE_BLACK_ME)), Reason: errMsg}
		subMucRspBody.ListBlackMe = totalBalck
		SendInviteMemberRsp(c, head, subMucRspBody)
		bNeedCall = false
		return false
	}
	// 首先检查是否超过群成员总数限制
	bResult, err := roomManager.IsExceedRoomMemberLimit(roomId, uint32(len(inviteeList)))
	if err != nil {
		infoLog.Printf("ProcInviteMemberReq IsExceedRoomMemberLimit internal error roomId=%v inviterUid=%v err=%v", roomId, inviter.GetUid(), err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}
	// 超过群成员总数限制
	if bResult {
		infoLog.Printf("ProcInviteMemberReq exceed member limit roomId=%v inviterUid=%v bResult=%v", roomId, inviter.GetUid(), bResult)
		result = uint16(ht_muc.MUC_RET_CODE_RET_MEMBER_EXEC_LIMIT)
		errMsg = []byte("member exceed limit")
		return false
	}

	roomTs, newInviteList, waitForAdmin, err := procInviterMember(head, roomId, inviter, inviteeList, roomIdFrom, msgId, roomProfile)
	if err != nil {
		infoLog.Printf("ProcInviteMemberReq procInviterMember exec failed roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v msgId=%s err=%s",
			roomId,
			inviter.GetUid(),
			len(inviteeList),
			roomIdFrom,
			msgId,
			err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("old proc failed")
		return false
	}

	infoLog.Printf("ProcInviteMemberReq roomId=%v roomTs=%v newInviteList=%v waitForAdmin=%v",
		roomId,
		roomTs,
		newInviteList,
		waitForAdmin)

	bNeedCall = false
	// 返回响应加群成功
	subMucRspBody := new(ht_muc.InviteMemberToGroupRspBody)
	if waitForAdmin {
		subMucRspBody.Status = &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_WAIT_ADMIN_VERIFY)),
			Reason: []byte("wait for admin verify"),
		}
	} else {
		subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))}
	}

	subMucRspBody.RoomTimestamp = proto.Uint64(uint64(roomTs))
	subMucRspBody.NotifyList = newInviteList
	SendInviteMemberRsp(c, head, subMucRspBody)

	err = PublishActionEvent(inviteeList[0].GetUid(), roomId, inviter.GetUid(), AT_INVITED_JOIN_ROOM)
	if err != nil {
		// add static
		attr := "gomuc/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcInviteMemberReq PublishPostEvent uid=%v roomId=%v actionType=%v failed",
			inviter.GetUid(),
			roomId,
			AT_INVITED_JOIN_ROOM)
	}
	return true
}
func procInviterMember(head *common.HeadV3,
	roomId uint32,
	inviter *ht_muc.RoomMemberInfo,
	inviteeList []*ht_muc.RoomMemberInfo,
	roomIdFrom uint32,
	msgId []byte,
	roomProfile *ht_muc.RoomProfile) (roomTs uint64, sendReqList []uint32, waitForAdmin bool, err error) {
	if roomId == 0 || inviter == nil || len(inviteeList) == 0 || roomProfile == nil {
		infoLog.Printf("procInviterMember nil param roomId=%v inviter=%v inviteeLen=%v roomIdFrom=%v msgId=%s roomUrl=%s",
			roomId,
			*inviter,
			len(inviteeList),
			roomIdFrom,
			msgId,
			roomProfile.GetRoomPhotoUrl())
		err = util.ErrInputParam
		return roomTs, nil, waitForAdmin, err
	}

	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("procInviterMember roomManager.GetRoomInfo failed roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v msgId=%s err=%s",
			roomId,
			inviter.GetUid(),
			len(inviteeList),
			roomIdFrom,
			msgId,
			err)
		return roomTs, nil, waitForAdmin, err
	}

	roomTs = uint64(roomInfo.RoomTS) // set room ts
	memberCnt := len(roomInfo.MemberList)
	sendInviteToAll := false
	if memberCnt+len(inviteeList) > requestJoinLimit {
		sendInviteToAll = true
	}
	infoLog.Printf("procInviterMember roomId=%v inviterUid=%v memberCnt=%v inviteeCnt=%v requestJoinLimit=%v sendInviteToAll=%v",
		roomId,
		inviter.GetUid(),
		memberCnt,
		len(inviteeList),
		requestJoinLimit,
		sendInviteToAll)

	// 首先将接收方分成 新、老两拨用户 老的只能按照老的逻辑处理 新的需要判断人数和关注数、消息数
	var oldInviteeList []*ht_muc.RoomMemberInfo
	var newInviteeList []*ht_muc.RoomMemberInfo
	for _, v := range inviteeList {
		onlineStat, err := mcApi.GetUserOnlineStat(v.GetUid())
		if err == nil {
			if (onlineStat.ClientType == common.CClientTyepIOS && onlineStat.Version > common.CVerSion237) ||
				(onlineStat.ClientType == common.CClientTypeAndroid && onlineStat.Version > common.CVerSion239) {
				newInviteeList = append(newInviteeList, v)
			} else {
				oldInviteeList = append(oldInviteeList, v)
			}
		} else {
			infoLog.Printf("procInviterMember userOnline failed roomId=%v uid=%v err=%v", roomId, v.GetUid(), err)
			oldInviteeList = append(oldInviteeList, v)
		}
	}
	for i, v := range oldInviteeList {
		infoLog.Printf("procInviterMember roomId=%v inviterUid=%v old index=%v inviteeUid=%v",
			roomId,
			inviter.GetUid(),
			i,
			v.GetUid())
	}
	for i, v := range newInviteeList {
		infoLog.Printf("procInviterMember roomId=%v inviterUid=%v new index=%v inviteeUid=%v",
			roomId,
			inviter.GetUid(),
			i,
			v.GetUid())
	}

	newWaitAdmin := false
	oldWaitAdmin := false
	// new client version proc
	if len(newInviteeList) > 0 {
		sendReqList, newWaitAdmin, err = procNewInviterMember(head, roomId, inviter, newInviteeList, roomIdFrom, msgId, roomProfile, sendInviteToAll)
		if err != nil {
			infoLog.Printf("procInviterMember call procNewInviterMember  roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v msgId=%s err=%s",
				roomId,
				inviter.GetUid(),
				len(newInviteeList),
				roomIdFrom,
				msgId,
				err)
			return roomTs, sendReqList, waitForAdmin, err
		}
	}

	// old client version proc
	if len(oldInviteeList) > 0 {
		oldWaitAdmin, err = procOldInviterMember(head, roomId, inviter, oldInviteeList, roomIdFrom, msgId)
		if err != nil {
			infoLog.Printf("procInviterMember call procOldInviterMember roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v msgId=%s err=%s",
				roomId,
				inviter.GetUid(),
				len(oldInviteeList),
				roomIdFrom,
				msgId,
				err)
			return roomTs, sendReqList, waitForAdmin, err
		}
		infoLog.Print("procInviterMember procOldInviterMember roomId=%v roomTs=%v seqReqLis=%v waitForAdmin=%v err=%s",
			roomId,
			roomTs,
			sendReqList,
			waitForAdmin,
			err)
	}
	if newWaitAdmin || oldWaitAdmin {
		waitForAdmin = true
	}
	return roomTs, sendReqList, waitForAdmin, nil
}

func procOldInviterMember(head *common.HeadV3,
	roomId uint32,
	inviter *ht_muc.RoomMemberInfo,
	inviteeList []*ht_muc.RoomMemberInfo,
	roomIdFrom uint32,
	msgId []byte) (waitForAdmin bool, err error) {
	if head == nil || roomId == 0 || inviter == nil || len(inviteeList) == 0 {
		infoLog.Printf("procOldInviterMember nil param roomId=%v inviter=%v inviteeLen=%v roomIdFrom=%v msgId=%s",
			roomId,
			*inviter,
			len(inviteeList),
			roomIdFrom,
			msgId)
		err = util.ErrInputParam
		return waitForAdmin, err
	}
	// 检查邀请者是否是管理员如果是管理员直接加群并广播
	// 获取群聊确认
	bIsOpenVerify, err := roomManager.IsOpenVerify(roomId)
	if err != nil {
		infoLog.Printf("procOldInviterMember roomManager.IsOpenVerify exec failed roomId=%v err=%s", roomId, err)
		return waitForAdmin, err
	}
	bAdmin, err := roomManager.IsUserAdmin(roomId, inviter.GetUid())
	if err != nil {
		infoLog.Printf("procOldInviterMember roomManager.IsUserAdmin exec failed roomId=%v err=%s", roomId, err)
		return waitForAdmin, err
	}

	if !bIsOpenVerify || (bAdmin && roomIdFrom == ROOM_ID_FROM_INVITE) {
		tempTs, err := roomManager.AddMember(roomId, inviter, inviteeList)
		if err != nil {
			if err == util.ErrAlreadyIn {
				return waitForAdmin, nil
			} else {
				return waitForAdmin, err
			}
		}
		err = roomManager.NotifyInviteMember(inviter, inviter, inviteeList, roomId, uint64(tempTs), roomIdFrom, true)
		if err != nil {
			infoLog.Printf("procOldInviterMember NotifyInviteMember roomId=%v inviterId=%v inviteeListCount=%v failed err=%v",
				roomId,
				inviter.GetUid(),
				len(inviteeList),
				err)
		}
		return waitForAdmin, err
	} else { // 检查失败或者不是管理员
		// 首先将请求转发到管理员
		// second Broad cast
		joinHead := &common.HeadV3{
			Flag:     head.Flag,
			Version:  head.Version,
			CryKey:   head.CryKey,
			TermType: head.TermType,
			Cmd:      uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_MUC_REQ_JOIN_ROOM),
			Seq:      head.Seq,
			From:     head.From,
			To:       head.To,
			Len:      0,
			Ret:      0,
		}

		reqBody := &ht_muc.MucReqBody{
			RequestJoinRoomReqbody: &ht_muc.RequestJoinRoomReqBody{
				RoomId:      proto.Uint32(roomId),
				InviterInfo: inviter,
				InviteeInfo: inviteeList,
				RoomIdFrom:  proto.Uint32(roomIdFrom),
				MsgId:       msgId,
			},
		}
		reqSlice, err := proto.Marshal(reqBody)
		if err != nil {
			infoLog.Printf("procOldInviterMember proto.Marshal failed roomId=%v inviterUid=%v inviteeLen=%v err=%s",
				roomId,
				inviter.GetUid(),
				len(inviteeList),
				err)
			return waitForAdmin, err
		}
		_, err = roomManager.NotifyAdminRequestJoin(roomId, joinHead, reqSlice)
		if err != nil {
			infoLog.Printf("procOldInviterMember internal error roomId=%v err=%v", roomId, err)
			return waitForAdmin, err
		}
		waitForAdmin = true
	}
	return waitForAdmin, nil
}
func procNewInviterMember(head *common.HeadV3,
	roomId uint32,
	inviter *ht_muc.RoomMemberInfo,
	inviteeList []*ht_muc.RoomMemberInfo,
	roomIdFrom uint32,
	msgId []byte,
	roomProfile *ht_muc.RoomProfile,
	sendInviteToAll bool) (sendReqList []uint32, waitForAdmin bool, err error) {
	if head == nil || roomId == 0 || inviter == nil || len(inviteeList) == 0 || roomProfile == nil {
		infoLog.Printf("procNewInviterMember nil param roomId=%v inviter=%v inviteeLen=%v roomIdFrom=%v msgId=%s",
			roomId,
			*inviter,
			len(inviteeList),
			roomIdFrom,
			msgId)
		err = util.ErrInputParam
		return nil, waitForAdmin, err
	}
	if sendInviteToAll { // 给所有的客户端发送群邀请等待对方确认请求
		err = roomManager.MultiCastInviteReq(head, roomId, inviter, inviteeList, roomIdFrom, msgId, roomProfile)
		if err != nil {
			infoLog.Printf("procNewInviterMember roomId=%v inviter=%v inviteeLen=%v roomIdFrom=%v msgId=%s MultiCastInviteReq failed err=%s",
				roomId,
				inviter.GetUid(),
				len(inviteeList),
				roomIdFrom,
				msgId,
				err)
			return nil, waitForAdmin, err
		}
		for _, v := range inviteeList {
			sendReqList = append(sendReqList, v.GetUid())
		}
	} else { // 根据聊天条数和follower 确认是发送邀请确认还是直接邀请进群
		var sendInviteToList []*ht_muc.RoomMemberInfo
		var inviteDirectList []*ht_muc.RoomMemberInfo
		for _, v := range inviteeList {
			inviteeUid := v.GetUid()
			chatCnt, err := chatRecordApi.GetChatCount(inviteeUid, inviter.GetUid())
			if err != nil {
				infoLog.Printf("procNewInviterMember chatRecordApi.GetChatCount roomId=%v inviterUid=%v inviteeUid=%v err=%s",
					roomId,
					inviter.GetUid(),
					inviteeUid,
					err)
				// add static
				attr := "gomuc/get_chat_count_failed"
				libcomm.AttrAdd(attr, 1)
				chatCnt = SENDPHOTOLIMIT + 1 //获取失败直接默认已经可以回复5句以上
			}
			if chatCnt > SENDPHOTOLIMIT { // 邀请者可以发送图片给被邀请者，也即被邀请者给邀请者恢复5句以上 直接加入群聊
				inviteDirectList = append(inviteDirectList, v)
			} else {
				followingList, err := relationApi.GetFollowingLists(inviteeUid) // 获取正在关注列表
				if err != nil {
					infoLog.Printf("procNewInviterMember relationApi.GetFollowingLists roomId=%v inviterUid=%v inviteeUid=%v err=%s",
						roomId,
						inviter.GetUid(),
						inviteeUid,
						err)
					// add static
					attr := "gomuc/get_fllowing_list_failed"
					libcomm.AttrAdd(attr, 1)
					// 获取关注列表失败 直接加入群聊
					inviteDirectList = append(inviteDirectList, v)
				}
				isFollowing := UidIsInSlice(followingList, inviter.GetUid())
				if isFollowing { // 被邀请者关注邀请者者直接加入
					inviteDirectList = append(inviteDirectList, v)
				} else {
					sendInviteToList = append(sendInviteToList, v)
					sendReqList = append(sendReqList, inviteeUid)
				}
			}
		}

		infoLog.Printf("procNewInviterMember roomId=%v inviterUid=%v inviteDirectListLen=%v sendInviteToListLen=%v",
			roomId,
			inviter.GetUid(),
			len(inviteDirectList),
			len(sendInviteToList))
		if len(inviteDirectList) > 0 {
			waitForAdmin, err = procOldInviterMember(head, roomId, inviter, inviteDirectList, roomIdFrom, msgId)
			if err != nil {
				infoLog.Printf("procNewInviterMember old version call procOldInviterMember roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v err=%s",
					roomId,
					inviter.GetUid(),
					len(inviteDirectList),
					roomIdFrom,
					err)
				return nil, waitForAdmin, err
			}
		} else {
			infoLog.Printf("procNewInviterMember old version roomId=%v inviterUid=%v inviteeLen=%v roomIdFrom=%v empty",
				roomId,
				inviter.GetUid(),
				len(inviteDirectList),
				roomIdFrom)
		}

		if len(sendInviteToList) > 0 {
			err = roomManager.MultiCastInviteReq(head, roomId, inviter, sendInviteToList, roomIdFrom, msgId, roomProfile)
			if err != nil {
				infoLog.Printf("procNewInviterMember roomId=%v inviter=%v inviteeLen=%v roomIdFrom=%v msgId=%s MultiCastInviteReq failed err=%s",
					roomId,
					inviter.GetUid(),
					len(sendInviteToList),
					roomIdFrom,
					msgId,
					err)
				return nil, waitForAdmin, err
			}
		} else {
			infoLog.Printf("procNewInviterMember new version roomId=%v inviter=%v inviteeLen=%v roomIdFrom=%v msgId=%s",
				roomId,
				inviter.GetUid(),
				len(sendInviteToList),
				roomIdFrom,
				msgId)
		}
	}
	return sendReqList, waitForAdmin, nil
}
func SendInviteMemberRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.InviteMemberToGroupRspbody = new(ht_muc.InviteMemberToGroupRspBody)
	subMucRspBody := rspBody.GetInviteMemberToGroupRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendInviteMemberRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendInviteMemberRetCode SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendInviteMemberRsp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.InviteMemberToGroupRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.InviteMemberToGroupRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendInviteMemberRsp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendInviteMemberRsp SerialHeadV3ToSlice failed from=%v to=%v cmd=%v seq=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			err)
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 32.proc query whether user is admin
func ProcQueryUserIsAdmin(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendQueryUserIsAdminRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/query_user_is_admin_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetQueryUserIsAdminReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetUid()
	infoLog.Printf("ProcQueryUserIsAdmin recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid)
	// first check input param
	if roomId == 0 || opUid == 0 {
		infoLog.Printf("ProcQueryUserIsAdmin invalid param roomId=%v opUid=%v", roomId, opUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second check whether op user is admin
	bRet, err := roomManager.IsUserAdmin(roomId, opUid)
	if err != nil {
		infoLog.Printf("ProcQueryUserIsAdmin exec IsUserAdmin failed roomId=%v opUid=%v err=%v", roomId, opUid, err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	var isAdmin uint32 = 0
	if bRet {
		isAdmin = 1
	}
	// infoLog.Printf("DEBUG ProcMucSetAdmin roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := &ht_muc.QueryUserIsAdminRspBody{
		Status: &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))},
		Result: proto.Uint32(isAdmin),
	}

	SendQueryUserIsAdminResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}
func SendQueryUserIsAdminRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.QueryUserIsAdminRspbody = new(ht_muc.QueryUserIsAdminRspBody)
	subMucRspBody := rspBody.GetQueryUserIsAdminRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendQueryUserIsAdminRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendQueryUserIsAdminRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendQueryUserIsAdminResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.QueryUserIsAdminRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.QueryUserIsAdminRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendQueryUserIsAdminResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendQueryUserIsAdminResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 33.proc notif create teaching room
func ProcNotifyCreateTeachingRoom(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendNotifyCreateTeachingRoomRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/not_cre_tr_cnt"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetNotifyCreateTeachingRoomReqbody()
	roomId := subReqBody.GetChatRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	createUid := subReqBody.GetCreateUid().GetUid()
	notifyList := subReqBody.GetNotifyUidList()
	lessName := subReqBody.GetLessonName()
	lessAbstract := subReqBody.GetLessonAbstract()
	lessCover := subReqBody.GetLessonCoverUrl()
	createTime := subReqBody.GetCreateTimeStamp()
	obid := subReqBody.GetObid()
	infoLog.Printf("ProcNotifyCreateTeachingRoom recv from=%v to=%v cmd=%v seq=%v roomId=%v wBid=%v createUid=%v notifyLen=%v lessonName=%s lessonAbstract=%s lessCover=%s timestamp=%v obid=%s",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		wBid,
		createUid,
		len(notifyList),
		lessName,
		lessAbstract,
		lessCover,
		createTime,
		obid)
	// first check input param
	if roomId == 0 || wBid == 0 || createUid == 0 || len(obid) == 0 {
		infoLog.Printf("ProcNotifyCreateTeachingRoom invalid param roomId=%v wBid=%v creatUid=%v", roomId, wBid, createUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	strMsgId := fmt.Sprintf("MSG_%v", createTime)
	pushInfo := &util.MucPushInfo{
		PushType: uint8(util.PUSH_WHITE_BOARD),
		NickName: string(subReqBody.GetCreateUid().GetNickName()),
		MsgId:    strMsgId,
	}

	// second Broad cast
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcNotifyCreateTeachingRoom proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second check whether op user is admin

	// infoLog.Printf("DEBUG ProcMucSetAdmin roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := &ht_muc.NotifyCreateTeachingRoomRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendNotifyCreateTeachingRoomResp(c, head, subMucRspBody)
	bNeedCall = false

	// broad cast notification
	err = roomManager.MultiCastNotificationWithPush(roomId, notifyList, createUid, head, bcSlice, pushInfo, true)
	if err != nil {
		infoLog.Printf("ProcNotifyCreateTeachingRoom MultiCastNotificationWithPush failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("bc failed")
		return false
	}
	return true
}
func SendNotifyCreateTeachingRoomRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyCreateTeachingRoomRspbody = new(ht_muc.NotifyCreateTeachingRoomRspBody)
	subMucRspBody := rspBody.GetNotifyCreateTeachingRoomRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyCreateTeachingRoomRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyCreateTeachingRoomRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendNotifyCreateTeachingRoomResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.NotifyCreateTeachingRoomRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyCreateTeachingRoomRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyCreateTeachingRoomResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyCreateTeachingRoomResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 34.proc notify room stat
func ProcTeachingRoomStatBroadCast(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendTeachingRoomStatBCRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/tr_stat_bc_cnt"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetTeachingRoomStatBroadCastReqbody()
	roomId := subReqBody.GetRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	handsUpStat := subReqBody.GetHandsUpStat()
	wBStat := subReqBody.GetWhiteBoardStat()
	alreadyInList := subReqBody.GetAlreadyInList()
	handsUpList := subReqBody.GetHandsUpList()
	connList := subReqBody.GetMicrophoneConnList()
	sessionTime := subReqBody.GetSessionTime()
	infoLog.Printf("ProcTeachingRoomStatBroadCast recv from=%v to=%v cmd=%v seq=%v roomId=%v wBid=%v handsUpStat=%v wBStat=%v alreadyInLen=%v handsUpLen=%v connLen=%v sessionTime=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		wBid,
		handsUpStat,
		wBStat,
		len(alreadyInList),
		len(handsUpList),
		len(connList),
		sessionTime)
	// first check input param
	if roomId == 0 || wBid == 0 || len(alreadyInList) == 0 {
		infoLog.Printf("ProcTeachingRoomStatBroadCast invalid param roomId=%v wBid=%v from=%v", roomId, wBid, head.From)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second Broad cast
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcTeachingRoomStatBroadCast proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("input param error")
		return false
	}
	// second check whether op user is admin

	// infoLog.Printf("DEBUG ProcMucSetAdmin roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := &ht_muc.TeachingRoomStatBroadCastRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendTeachingRoomStatBCResp(c, head, subMucRspBody)
	bNeedCall = false

	err = roomManager.MultiCastNotificationWithPush(roomId, alreadyInList, head.From, head, bcSlice, nil, false)
	if err != nil {
		infoLog.Printf("ProcTeachingRoomStatBroadCast MultiCastNotificationWithPush failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("bc failed")
		return false
	}
	return true
}
func SendTeachingRoomStatBCRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.TeachingRoomStatBroadCastRspbody = new(ht_muc.TeachingRoomStatBroadCastRspBody)
	subMucRspBody := rspBody.GetTeachingRoomStatBroadCastRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendTeachingRoomStatBCRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendTeachingRoomStatBCRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendTeachingRoomStatBCResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.TeachingRoomStatBroadCastRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.TeachingRoomStatBroadCastRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendTeachingRoomStatBCResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendTeachingRoomStatBCResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 35.proc notif create teaching room end
func ProcNotifyTeachingRoomEnd(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendNotifyTeachingRoomEndRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/not_tr_end_cnt"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetNotifyTeachingRoomEndReqbody()
	roomId := subReqBody.GetChatRoomId()
	wBid := subReqBody.GetWhiteBoardId()
	notifyList := subReqBody.GetNotifyUidList()
	ts := subReqBody.GetTimeStamp()
	infoLog.Printf("ProcNotifyTeachingRoomEnd recv from=%v to=%v cmd=%v seq=%v roomId=%v wBid=%v notifyLen=%v ts=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		wBid,
		len(notifyList),
		ts)
	// first check input param
	if roomId == 0 || wBid == 0 || len(notifyList) == 0 {
		infoLog.Printf("ProcNotifyTeachingRoomEnd invalid param roomId=%v wBid=%v notifyLen=%v", roomId, wBid, len(notifyList))
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second Broad cast
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcNotifyTeachingRoomEnd proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// sixth send ack
	subMucRspBody := &ht_muc.NotifyTeachingRoomEndRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendNotifyTeachingRoomEndResp(c, head, subMucRspBody)
	bNeedCall = false

	err = roomManager.MultiCastNotificationWithPush(roomId, notifyList, head.From, head, bcSlice, nil, true)
	if err != nil {
		infoLog.Printf("ProcNotifyTeachingRoomEnd MultiCastNotificationWithPush failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("bc failed")
		return false
	}
	// infoLog.Printf("DEBUG ProcMucSetAdmin roomId=%v roomTS=%v", roomId, roomTS)
	return true
}
func SendNotifyTeachingRoomEndRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyTeachingRoomEndRspbody = new(ht_muc.NotifyTeachingRoomEndRspBody)
	subMucRspBody := rspBody.GetNotifyTeachingRoomEndRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyTeachingRoomEndRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyTeachingRoomEndRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendNotifyTeachingRoomEndResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.NotifyTeachingRoomEndRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyTeachingRoomEndRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyTeachingRoomEndResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyTeachingRoomEndResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 36.proc notif begin group lesson
func ProcNotifyBeginGroupLesson(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendNotifyBeginGroupLessonRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/begin_gl_cnt"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetNotifyBeginGroupLessonReqbody()
	teacherUid := subReqBody.GetTeacherUid()
	teacherName := subReqBody.GetTeacherName()
	roomId := subReqBody.GetRoomId()
	channelId := subReqBody.GetChannelId()
	groupLessonObid := subReqBody.GetGroupLessonObid()
	firstUrl := subReqBody.GetFirstUrl()
	lessTitle := subReqBody.GetLessonTitle()
	lessAbstract := subReqBody.GetLessonAbstract()
	teacherAbstract := subReqBody.GetTeacherAbstract()
	coursewareUrl := subReqBody.GetLessonCoursewareUrl()
	beginTs := subReqBody.GetBeginTimeStamp()
	lessonTime := subReqBody.GetLessonTime()

	infoLog.Printf("ProcNotifyBeginGroupLesson recv from=%v to=%v cmd=%v seq=%v teacherUid=%v teacherName=%s roomId=%v channelId=%s groupLessonObid=%s firstUrl=%s lessTitle=%s lessAbstract=%s teacherAbstract=%s coursewareUrl=%s beginTs=%v lessonTime=%s",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		teacherUid,
		teacherName,
		roomId,
		channelId,
		groupLessonObid,
		firstUrl,
		lessTitle,
		lessAbstract,
		teacherAbstract,
		coursewareUrl,
		beginTs,
		lessonTime)
	// Step1: first check input param
	if teacherUid == 0 || roomId == 0 || len(channelId) == 0 || len(groupLessonObid) == 0 || len(lessTitle) == 0 {
		infoLog.Printf("ProcNotifyBeginGroupLesson invalid param teacherUid=%v teacherName=%s roomId=%v channelId=%s groupLessonObid=%s firstUrl=%s lessTitle=%s lessAbstract=%s teacherAbstract=%s coursewareUrl=%s beginTs=%v",
			teacherUid,
			teacherName,
			roomId,
			channelId,
			groupLessonObid,
			firstUrl,
			lessTitle,
			lessAbstract,
			teacherAbstract,
			coursewareUrl,
			beginTs)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	strMsgId := fmt.Sprintf("MSG_%v", beginTs)
	pushInfo := &util.MucPushInfo{
		PushType: uint8(util.PUSH_GROUP_LESSON),
		NickName: string(teacherName),
		MsgId:    strMsgId,
	}

	// Step2: send resp
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcNotifyBeginGroupLesson proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("input param error")
		return false
	}
	subMucRspBody := &ht_muc.NotifyBeginGroupLessonRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendNotifyBeginGroupLessonResp(c, head, subMucRspBody)
	bNeedCall = false

	// Step3:broad cast notification
	err = roomManager.BroadCastNotificationWithPush(roomId, teacherUid, head, bcSlice, pushInfo, true)
	if err != nil {
		infoLog.Printf("ProcNotifyBeginGroupLesson BroadCastNotificationWithPush failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("bc failed")
		return false
	}
	return true
}
func SendNotifyBeginGroupLessonRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyBeginGroupLessonRspbody = new(ht_muc.NotifyBeginGroupLessonRspBody)
	subMucRspBody := rspBody.GetNotifyBeginGroupLessonRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyBeginGroupLessonRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyBeginGroupLessonRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendNotifyBeginGroupLessonResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.NotifyBeginGroupLessonRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyBeginGroupLessonRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyBeginGroupLessonResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyBeginGroupLessonResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 37.proc notif end group lesson
func ProcNotifyEndGroupLesson(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendNotifyEndGroupLessonRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/end_gl_cnt"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetNotifyEndGroupLessonReqbody()
	teacherUid := subReqBody.GetTeacherUid()
	teacherName := subReqBody.GetTeacherName()
	roomId := subReqBody.GetRoomId()
	channelId := subReqBody.GetChannelId()
	timeStamp := subReqBody.GetTimeStamp()

	infoLog.Printf("ProcNotifyEndGroupLesson recv from=%v to=%v cmd=%v seq=%v teacherUid=%v teacherName=%s roomId=%v channelId=%s timeStamp=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		teacherUid,
		teacherName,
		roomId,
		channelId,
		timeStamp)
	// Step1: first check input param
	if teacherUid == 0 || roomId == 0 || len(channelId) == 0 {
		infoLog.Printf("ProcNotifyEndGroupLesson invalid param teacherUid=%v teacherName=%s roomId=%v channelId=%s timeStamp=%v",
			teacherUid,
			teacherName,
			roomId,
			channelId,
			timeStamp)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// Step2: send resp
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcNotifyEndGroupLesson proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("input param error")
		return false
	}
	subMucRspBody := &ht_muc.NotifyEndGroupLessonRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendNotifyEndGroupLessonResp(c, head, subMucRspBody)
	bNeedCall = false

	// Step3:broad cast notification
	err = roomManager.BroadCastNotificationWithPush(roomId, teacherUid, head, bcSlice, nil, true)
	if err != nil {
		infoLog.Printf("ProcNotifyEndGroupLesson BroadCastNotificationWithPush failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("bc failed")
		return false
	}
	return true
}
func SendNotifyEndGroupLessonRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyEndGroupLessonRspbody = new(ht_muc.NotifyEndGroupLessonRspBody)
	subMucRspBody := rspBody.GetNotifyEndGroupLessonRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyEndGroupLessonRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyEndGroupLessonRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendNotifyEndGroupLessonResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.NotifyEndGroupLessonRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyEndGroupLessonRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyEndGroupLessonResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyEndGroupLessonResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 38.proc notify group lesson stat change
func ProcNotifyGroupLessonStatChange(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendNotifyGroupLessonStatChangeRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/gl_stat_bc_cnt"
	libcomm.AttrAdd(attr, 1)
	// 一开始只有memberlist 需要在这边将memberlist 拆分成teacherlist 和 memberlist
	subReqBody := reqBody.GetNotifyGroupLessonStatChangeReqbody()
	roomId := subReqBody.GetRoomId()
	memberList := subReqBody.GetStudentList()
	currentCoursewareUrl := subReqBody.GetCurrentCoursewareUrl()
	timeStamp := subReqBody.GetTimeStamp()
	infoLog.Printf("ProcNotifyGroupLessonStatChange recv from=%v to=%v cmd=%v seq=%v roomId=%v memberLen=%v url=%v timeStamp=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		len(memberList),
		currentCoursewareUrl,
		timeStamp)
	// Step1: check input param
	if roomId == 0 || len(memberList) == 0 {
		infoLog.Printf("ProcNotifyGroupLessonStatChange invalid param roomId=%v memberLen=%v from=%v", roomId, len(memberList), head.From)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonStatChange faield roomId=%v memberLen=%v from=%v", roomId, len(memberList), head.From)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}
	roomCreaterId := roomInfo.CreateUid
	adminList := roomInfo.AdminList
	adminList = append(adminList, roomCreaterId)
	teacherList := []uint32{}
	studentList := []uint32{}
	for _, v := range memberList {
		if UidIsInSlice(adminList, v) {
			teacherList = append(teacherList, v)
		} else {
			studentList = append(studentList, v)
		}
	}
	subReqBody.TeacherList = teacherList
	subReqBody.StudentList = studentList
	reqBody.NotifyGroupLessonStatChangeReqbody = subReqBody
	// Step2: Broad cast
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonStatChange proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("input param error")
		return false
	}
	subMucRspBody := &ht_muc.NotifyGroupLessonStatChangeRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendNotifyGroupLessonStatChangeResp(c, head, subMucRspBody)
	bNeedCall = false

	// Step3: Multicaset notifycation
	err = roomManager.MultiCastNotificationWithPush(roomId, memberList, head.From, head, bcSlice, nil, false)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonStatChange MultiCastNotificationWithPush failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("bc failed")
		return false
	}
	return true
}
func SendNotifyGroupLessonStatChangeRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyGroupLessonStatChangeRspbody = new(ht_muc.NotifyGroupLessonStatChangeRspBody)
	subMucRspBody := rspBody.GetNotifyGroupLessonStatChangeRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyGroupLessonStatChangeRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyGroupLessonStatChangeRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendNotifyGroupLessonStatChangeResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.NotifyGroupLessonStatChangeRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyGroupLessonStatChangeRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyGroupLessonStatChangeResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyGroupLessonStatChangeResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 39.proc notify group lesson message
func ProcNotifyGroupLessonMessage(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendNotifyGroupLessonMessageRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/gl_msg_bc_cnt"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetNotifyGroupLessonMessageReqbody()
	roomId := subReqBody.GetRoomId()
	fromId := subReqBody.GetFromId()
	recordObid := subReqBody.GetRecordObid()
	msg := subReqBody.GetMessage()
	memberList := subReqBody.GetMemberList()
	timeStamp := subReqBody.GetTimeStamp()
	infoLog.Printf("ProcNotifyGroupLessonMessage recv from=%v to=%v cmd=%v seq=%v roomId=%v  fromId=%v recordObid=%s memberLen=%v timeStamp=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		fromId,
		recordObid,
		len(memberList),
		timeStamp)
	// Step1: check input param
	if roomId == 0 || len(memberList) == 0 || fromId == 0 || len(recordObid) == 0 {
		infoLog.Printf("ProcNotifyGroupLessonMessage invalid param roomId=%v  fromId=%v recordObid=%s memberLen=%v timeStamp=%v",
			roomId,
			fromId,
			recordObid,
			len(memberList),
			timeStamp)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	compressBuff := bytes.NewBuffer(msg)
	r, err := zlib.NewReader(compressBuff)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonMessage zlib.NewReader failed err=%v", err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	defer r.Close()
	// 解压缩
	unCompressSlice, err := ioutil.ReadAll(r)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonMessage zlib.unCompress failed")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("Internal error")
		return false
	}
	// 解压之后的内容构造json
	rootObj, err := simplejson.NewJson(unCompressSlice)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonMessage simplejson new packet error", err)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("NewJson failed")
		return false
	}
	// 群固有属性
	roomId = uint32(rootObj.Get("room_id").MustInt64(0))
	strMsgId := rootObj.Get("msg_id").MustString()
	strSendTime := rootObj.Get("send_time").MustString()
	strMsgType := rootObj.Get("msg_type").MustString()
	// strMsgModel := rootObj.Get("msg_model").MustString()
	fromId = uint32(rootObj.Get("sender_id").MustInt64(0))
	// userProfile := rootObj.Get("sender_ts").MustUint64(0)
	strNickName := rootObj.Get("sender_name").MustString()

	infoLog.Printf("ProcNotifyGroupLessonMessage recv from=%v to=%v cmd=%v seq=%v roomId=%v strMsgId=%s strMsgType=%s fromId=%v strNickName=%s sendTime=%s",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		strMsgId,
		strMsgType,
		fromId,
		strNickName,
		strSendTime)
	// Step2:
	subMucRspBody := &ht_muc.NotifyGroupLessonMessageRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendNotifyGroupLessonMessageResp(c, head, subMucRspBody)
	bNeedCall = false

	formatTime, ts := GetCurrentFormatTime()
	rootObj.Set("server_time", formatTime)
	rootObj.Set("server_ts", ts)
	strMsgBody, err := rootObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonMessage simpleJson MarshalJSON failed roomId=%v fromId=%v seq=%v",
			roomId,
			fromId,
			head.Seq)
		return false
	}
	infoLog.Printf("ProcNotifyGroupLessonMessage outJson=%s", strMsgBody)
	// 压缩JSON消息数据
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(strMsgBody)
	w.Close()
	compressSliceByte := b.Bytes()

	// 拼装成新的pb格式
	bcReqBody := new(ht_muc.MucReqBody)
	bcReqBody.NotifyGroupLessonMessageReqbody = &ht_muc.NotifyGroupLessonMessageReqBody{
		RoomId:     proto.Uint32(roomId),
		FromId:     proto.Uint32(fromId),
		RecordObid: recordObid,
		Message:    compressSliceByte,
		MemberList: nil,
		TimeStamp:  proto.Uint64(uint64(time.Now().UnixNano())),
	}
	bcSlice, err := proto.Marshal(bcReqBody)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonMessage proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	// Step3: Multicaset notifycation
	err = roomManager.MultiCastNotificationOnlyOnline(roomId, memberList, fromId, head, bcSlice, true)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonMessage MultiCastNotificationOnlyOnline failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	// Step4: 存储消息
	msgIndex, err := lessonMsgStoreApi.StoreGroupLessonMsg(roomId,
		fromId,
		uint32(ht_muc.MUC_CMD_TYPE_GO_CMD_GROUP_LESSON_MESSAGE_BROADCASE_REQ),
		recordObid,
		uint64(ts),
		compressSliceByte)
	if err != nil {
		infoLog.Printf("ProcNotifyGroupLessonMessage StoreGroupLessonMsg failed from=%v to=%v cmd=%v seq=%v roomId=%v err=%s",
			head.From,
			head.To,
			head.Cmd,
			head.Seq,
			roomId,
			err)
	} else {
		infoLog.Printf("ProcNotifyGroupLessonMessage StoreGroupLessonMsg roomId=%v strMsgId=%s strMsgType=%s fromId=%v strNickName=%s sendTime=%s msgIndex=%v",
			roomId,
			strMsgId,
			strMsgType,
			fromId,
			strNickName,
			strSendTime,
			msgIndex)
	}
	return true
}
func SendNotifyGroupLessonMessageRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyGroupLessonMessageRspbody = new(ht_muc.NotifyGroupLessonMessageRspBody)
	subMucRspBody := rspBody.GetNotifyGroupLessonMessageRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyGroupLessonMessageRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyGroupLessonMessageRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendNotifyGroupLessonMessageResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.NotifyGroupLessonMessageRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.NotifyGroupLessonMessageRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendNotifyGroupLessonMessageResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendNotifyGroupLessonMessageResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 41.Proc Batch get room simple info
func ProcBatchGetRoomSimpleInfo(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendBatchGetRoomSimpleInfoRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/batch_get_simple_info_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetBatchGetRoomSimpleInfoReqbody()
	reqUid := subReqBody.GetReqUid()
	roomIdList := subReqBody.GetRoomIdList()
	infoLog.Printf("ProcBatchGetRoomSimpleInfo recv from=%v to=%v cmd=%v seq=%v reqUid=%v roomIdList=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		reqUid,
		roomIdList)
	// first check input param
	if len(roomIdList) == 0 || reqUid == 0 {
		infoLog.Printf("ProcBatchGetRoomSimpleInfo invalid param reqUid=%v roomIdList=%v", reqUid, roomIdList)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second get room simple info
	roomInfoList := []*ht_muc.RoomSimpleInfoItem{}
	for _, roomId := range roomIdList {
		roomInfo, err := roomManager.GetRoomInfo(roomId)
		if err != nil {
			attr := "gomuc/batch_get_simple_info_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcBatchGetRoomSimpleInfo faield roomId=%v reqUid=%v", roomId, reqUid)
			continue
		}
		var roomName string
		roomName = roomInfo.RoomName
		// 检查群名称是否为空 如果为空则填充3个人的昵称
		if len(roomName) == 0 {
			count := 0
			for _, v := range roomInfo.MemberList {
				_, nickName, err := userInfoCacheApi.GetUserNickName(v.Uid)
				if err != nil {
					infoLog.Printf("ProcBatchGetRoomSimpleInfo GetUserNickName uid=%v err=%s", v.Uid, err)
					continue
				}
				roomName += nickName + ","
				count += 1
				if count > ROOM_NAME_MEMEBER_LIMIE {
					break
				}
			}
			roomName = strings.TrimSuffix(roomName, ",")
		}
		item := &ht_muc.RoomSimpleInfoItem{
			RoomId:      proto.Uint32(roomId),
			RoomName:    []byte(roomName),
			MemberCount: proto.Uint32(uint32(len(roomInfo.MemberList))),
		}
		infoLog.Printf("ProcBatchGetRoomSimpleInfo roomId=%v roomName=%s memberCount=%v", roomId, roomName, len(roomInfo.MemberList))
		roomInfoList = append(roomInfoList, item)
	}

	subMucRspBody := &ht_muc.BatchGetRoomSimpleInfoRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
		SimpleInfoList: roomInfoList,
	}

	bNeedCall = false
	SendBatchGetRoomSimpleInfoResp(c, head, subMucRspBody)
	return true
}
func SendBatchGetRoomSimpleInfoRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.BatchGetRoomSimpleInfoRspbody = new(ht_muc.BatchGetRoomSimpleInfoRspBody)
	subMucRspBody := rspBody.GetBatchGetRoomSimpleInfoRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendBatchGetRoomSimpleInfoRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendBatchGetRoomSimpleInfoRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendBatchGetRoomSimpleInfoResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.BatchGetRoomSimpleInfoRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.BatchGetRoomSimpleInfoRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendBatchGetRoomSimpleInfoResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendBatchGetRoomSimpleInfoResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 42.proc modify room name
func ProcModifyRoomAvatar(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendModifyRoomAvatarRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/modify_room_avatar_req_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetSetRoomAvatarReqbody()
	opUid := subReqBody.GetReqUid()
	roomId := subReqBody.GetRoomId()
	roomAvatar := string(subReqBody.GetRoomAvatar())
	infoLog.Printf("ProcModifyRoomAvatar recv from=%v to=%v cmd=%v seq=%v roomId=%v roomAvatar=%s opUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		roomAvatar,
		opUid)
	// first check input param
	if roomId == 0 || opUid == 0 || len(roomAvatar) == 0 {
		infoLog.Printf("ProcModifyRoomAvatar invalid param")
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second modify room in db and memory
	err := roomManager.ModifyRoomAvatar(roomId, opUid, roomAvatar)
	if err != nil {
		infoLog.Printf("ProcModifyRoomAvatar roomManager.ModifyRoomName failed roomId=%v opUid=%v roomAvatar=%s",
			roomId,
			opUid,
			roomAvatar)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}

	// infoLog.Printf("DEBUG ProcModifyRoomName roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := &ht_muc.SetRoomAvatarRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendModifyRoomAvatarResp(c, head, subMucRspBody)
	return true
}
func SendModifyRoomAvatarRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.SetRoomAvatarRspbody = new(ht_muc.SetRoomAvatarRspBody)
	subMucRspBody := rspBody.GetSetRoomAvatarRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendModifyRoomAvatarRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendModifyRoomAvatarRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendModifyRoomAvatarResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.SetRoomAvatarRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.SetRoomAvatarRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendModifyRoomAvatarResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendModifyRoomAvatarResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 43.proc query whether user is admin
func ProcGetRoomAdminList(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendGetRoomAdminRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/get_admin_list_count"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetGetAdminListReqbody()
	roomId := subReqBody.GetRoomId()
	opUid := subReqBody.GetUid()
	infoLog.Printf("ProcGetRoomAdminList recv from=%v to=%v cmd=%v seq=%v roomId=%v opUid=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		opUid)
	// first check input param
	if roomId == 0 || opUid == 0 {
		infoLog.Printf("ProcGetRoomAdminList invalid param roomId=%v opUid=%v", roomId, opUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// second check whether op user is admin
	roomInfo, err := roomManager.GetRoomInfo(roomId)
	if err != nil {
		infoLog.Printf("ProcGetRoomAdminList faield roomId=%v opUid=%v", roomId, opUid)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("internal error")
		return false
	}
	adminList := roomInfo.AdminList
	adminList = append(adminList, roomInfo.CreateUid)
	// infoLog.Printf("DEBUG ProcMucSetAdmin roomId=%v roomTS=%v", roomId, roomTS)
	// sixth send ack
	subMucRspBody := &ht_muc.GetAdminListRspBody{
		Status:    &ht_muc.MucHeader{Code: proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS))},
		AdminList: adminList,
	}

	SendGetRoomAdminResp(c, head, subMucRspBody)
	bNeedCall = false
	return true
}

func SendGetRoomAdminRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.GetAdminListRspbody = new(ht_muc.GetAdminListRspBody)
	subMucRspBody := rspBody.GetGetAdminListRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendGetRoomAdminRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendGetRoomAdminRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func SendGetRoomAdminResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.GetAdminListRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.GetAdminListRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendGetRoomAdminResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendGetRoomAdminResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 44.proc notif start lesson charge
func ProcStartLessonCharge(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendStartLessonChargeRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/start_lc_cnt"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetStartLessonChargeReqbody()
	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	nickName := subReqBody.GetNickName()
	chargeId := subReqBody.GetChargeId()
	currencyInfo := subReqBody.GetCurrencyInfo()
	currency := currencyInfo.GetCurrencyType()
	amount := currencyInfo.GetAmount()
	lessonName := subReqBody.GetLessonName()
	lessonObid := subReqBody.GetLessonObid()
	msgId := subReqBody.GetMsgId()
	infoLog.Printf("ProcStartLessonCharge recv from=%v to=%v cmd=%v seq=%v roomId=%v reqUid=%v nickName=%s chargeId=%v currency=%s amount=%v lessoName=%s lessonObid=%s msgId=%s",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		reqUid,
		nickName,
		chargeId,
		currency,
		amount,
		lessonName,
		lessonObid,
		msgId)
	// Step1: first check input param
	if roomId == 0 || reqUid == 0 || chargeId == 0 || len(currency) == 0 || amount == 0 || len(lessonName) == 0 || len(lessonObid) == 0 {
		infoLog.Printf("ProcStartLessonCharge invalid param roomId=%v reqUid=%v nickName=%s chargeId=%v currency=%s amount=%v lessoName=%s lessonObid=%s msgId=%s",
			roomId,
			reqUid,
			nickName,
			chargeId,
			currency,
			amount,
			lessonName,
			lessonObid,
			msgId)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	pushInfo := &util.MucPushInfo{
		PushType: uint8(util.PUSH_START_CHARGE),
		NickName: string(nickName),
		MsgId:    msgId,
	}

	// Step2: send resp
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcStartLessonCharge proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("input param error")
		return false
	}
	subMucRspBody := &ht_muc.StartLessonChargeRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendStartLessonChargeResp(c, head, subMucRspBody)
	bNeedCall = false

	// Step3:broad cast notification
	err = roomManager.BroadCastNotificationWithPush(roomId, reqUid, head, bcSlice, pushInfo, true)
	if err != nil {
		infoLog.Printf("ProcStartLessonCharge BroadCastNotificationWithPush failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("bc failed")
		return false
	}
	return true
}
func SendStartLessonChargeRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.StartLessonChargeRspbody = new(ht_muc.StartLessonChargeRspBody)
	subMucRspBody := rspBody.GetStartLessonChargeRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendStartLessonChargeRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendStartLessonChargeRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendStartLessonChargeResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.StartLessonChargeRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.StartLessonChargeRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendStartLessonChargeResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendStartLessonChargeResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

// 45.proc notif end lesson charge
func ProcStopLessonCharge(c *gotcp.Conn, head *common.HeadV3, reqBody *ht_muc.MucReqBody) bool {
	result := uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS)
	errMsg := []byte("operation success")
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendStopLessonChargeRetCode(c, head, result, errMsg)
		}
	}()

	// 检查输入参数是否为空
	if head == nil || reqBody == nil {
		bNeedCall = false
		return false
	}

	// add static
	attr := "gomuc/end_gl_cnt"
	libcomm.AttrAdd(attr, 1)

	subReqBody := reqBody.GetStopLessonChargeReqbody()
	roomId := subReqBody.GetRoomId()
	reqUid := subReqBody.GetReqUid()
	nickName := subReqBody.GetNickName()
	chargeId := subReqBody.GetChargeId()
	currencyInfo := subReqBody.GetCurrencyInfo()
	currency := currencyInfo.GetCurrencyType()
	amount := currencyInfo.GetAmount()
	lessonName := subReqBody.GetLessonName()
	lessonObid := subReqBody.GetLessonObid()
	msgId := subReqBody.GetMsgId()
	explictSelf := subReqBody.GetExplictSelf()
	infoLog.Printf("ProcStopLessonCharge recv from=%v to=%v cmd=%v seq=%v roomId=%v reqUid=%v nickName=%s chargeId=%v currency=%s amount=%v lessoName=%s lessonObid=%s msgId=%s explictSelf=%v",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		roomId,
		reqUid,
		nickName,
		chargeId,
		currency,
		amount,
		lessonName,
		lessonObid,
		msgId,
		explictSelf)
	// Step1: first check input param
	if roomId == 0 || reqUid == 0 || chargeId == 0 || len(currency) == 0 || amount == 0 || len(lessonName) == 0 || len(lessonObid) == 0 {
		infoLog.Printf("ProcStopLessonCharge invalid param roomId=%v reqUid=%v nickName=%s chargeId=%v currency=%s amount=%v lessoName=%s lessonObid=%s msgId=%s",
			roomId,
			reqUid,
			nickName,
			chargeId,
			currency,
			amount,
			lessonName,
			lessonObid,
			msgId)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INPUT_PARAM_ERR)
		errMsg = []byte("input param error")
		return false
	}

	// Step2: send resp
	bcSlice, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ProcStopLessonCharge proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("input param error")
		return false
	}
	subMucRspBody := &ht_muc.StopLessonChargeRspBody{
		Status: &ht_muc.MucHeader{
			Code:   proto.Uint32(uint32(ht_muc.MUC_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
	}
	SendStopLessonChargeResp(c, head, subMucRspBody)
	bNeedCall = false

	// Step3:broad cast notification
	err = roomManager.BroadCastNotificationWithPush(roomId, reqUid, head, bcSlice, nil, explictSelf)
	if err != nil {
		infoLog.Printf("ProcStopLessonCharge BroadCastNotificationWithPush failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		result = uint16(ht_muc.MUC_RET_CODE_RET_INTERNAL_ERR)
		errMsg = []byte("bc failed")
		return false
	}
	return true
}
func SendStopLessonChargeRetCode(c *gotcp.Conn, reqHead *common.HeadV3, ret uint16, errMsg []byte) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.StopLessonChargeRspbody = new(ht_muc.StopLessonChargeRspBody)
	subMucRspBody := rspBody.GetStopLessonChargeRspbody()
	subMucRspBody.Status = &ht_muc.MucHeader{Code: proto.Uint32(uint32(ret)), Reason: errMsg}
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendStopLessonChargeRetCode proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}
	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendStopLessonChargeRetCode SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}
func SendStopLessonChargeResp(c *gotcp.Conn, reqHead *common.HeadV3, subMucRspBody *ht_muc.StopLessonChargeRspBody) bool {
	head := new(common.HeadV3)
	if reqHead != nil {
		*head = *reqHead
	}
	rspBody := new(ht_muc.MucRspBody)
	rspBody.StopLessonChargeRspbody = subMucRspBody
	s, err := proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("SendStopLessonChargeResp proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return false
	}

	head.Len = uint32(common.PacketV3HeadLen + len(s) + 1) //整个报文长度
	head.Ret = uint16(subMucRspBody.GetStatus().GetCode())
	buf := make([]byte, head.Len)
	buf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Printf("SendStopLessonChargeResp SerialHeadV3ToSlice failed")
		return false
	}
	copy(buf[common.PacketV3HeadLen:], s) // return code
	buf[head.Len-1] = common.HTV3MagicEnd

	rspPacket := common.NewHeadV3Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true
}

func NotifyCorrectSentence(uid uint32, termianlType uint8, obj *simplejson.Json) (err error) {
	// build notification
	infoObj := simplejson.New()
	infoObj.Set("type", GROUP_CHAT_MQ_CORRECT_TYPE)
	infoObj.Set("data", obj)

	rootObject := BuildCommonObject(RABBIT_SUBMIT, uid, termianlType)
	rootObject.Set("info", infoObj)
	strSlice, err := rootObject.MarshalJSON()
	if err != nil {
		infoLog.Printf("NotifyCorrectSentence simpleJson MarshalJSON failed uid=%v terminaltype=%v err=%s",
			uid,
			termianlType,
			err)
		return err
	}
	infoLog.Printf("NotifyCorrectSentence publish slice=%s", strSlice)
	err = RabbitMQPublish(rabbitMQUrl, EXCHANGENAME, "direct", ROUTINGKEY, string(strSlice), true)
	if err != nil {
		infoLog.Printf("NotifyCorrectSentence publish strSlice=%s failed", strSlice)
	} else {
		infoLog.Printf("NotifyCorrectSentence publish strSlice=%s success", strSlice)
	}
	return err
}

func BuildCommonObject(cmd string, uid uint32, terminalType uint8) (rootObject *simplejson.Json) {
	rootObject = simplejson.New()
	rootObject.Set("cmd", cmd)
	rootObject.Set("user_id", uid)
	rootObject.Set("os_type", terminalType)
	rootObject.Set("server_ts", time.Now().Unix())
	return rootObject
}

func RabbitMQPublish(amqpURI, exchange, exchangeType, routingKey, body string, reliable bool) error {

	// This function dials, connects, declares, publishes, and tears down,
	// all in one go. In a real service, you probably want to maintain a
	// long-lived connection as state, and publish against that.

	infoLog.Printf("dialing %q", amqpURI)
	connection, err := amqp.Dial(amqpURI)
	if err != nil {
		return fmt.Errorf("Dial: %s", err)
	}
	defer connection.Close()

	infoLog.Printf("got Connection, getting Channel")
	channel, err := connection.Channel()
	if err != nil {
		return fmt.Errorf("Channel: %s", err)
	}

	//Printf("got Channel, declaring %q Exchange (%q)", exchangeType, exchange)
	if err := channel.ExchangeDeclare(
		exchange,     // name
		exchangeType, // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return fmt.Errorf("Exchange Declare: %s", err)
	}

	// Reliable publisher confirms require confirm.select support from the
	// connection.
	if reliable {
		infoLog.Printf("enabling publishing confirms.")
		if err := channel.Confirm(false); err != nil {
			return fmt.Errorf("Channel could not be put into confirm mode: %s", err)
		}

		confirms := channel.NotifyPublish(make(chan amqp.Confirmation, 1))

		defer confirmOne(confirms)
	}

	infoLog.Printf("declared Exchange, publishing %dB body (%q)", len(body), body)
	if err = channel.Publish(
		exchange,   // publish to an exchange
		routingKey, // routing to 0 or more queues
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			Headers:         amqp.Table{},
			ContentType:     "text/plain",
			ContentEncoding: "",
			Body:            []byte(body),
			DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
			Priority:        0,              // 0-9
			// a bunch of application/implementation-specific fields
		},
	); err != nil {
		return fmt.Errorf("Exchange Publish: %s", err)
	}

	return nil
}

// One would typically keep a channel of publishings, a sequence number, and a
// set of unacknowledged sequence numbers and loop until the publishing channel
// is closed.
func confirmOne(confirms <-chan amqp.Confirmation) {
	infoLog.Printf("waiting for confirmation of one publishing")

	if confirmed := <-confirms; confirmed.Ack {
		infoLog.Printf("confirmed delivery with delivery tag: %d", confirmed.DeliveryTag)
	} else {
		infoLog.Printf("failed delivery of delivery tag: %d", confirmed.DeliveryTag)
	}
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// 读取 Memcache Ip and port
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustInt(11211)
	infoLog.Printf("memcache ip=%v port=%v", mcIp, mcPort)
	mcApi = new(common.MemcacheApi)
	mcApi.Init(mcIp + ":" + strconv.Itoa(mcPort))

	// 读取RabbitMQ的配置
	rabbitMQUrl = cfg.Section("RABBITMQ").Key("mq_url").MustString("amqp://pushServer:123456@10.243.134.240:5672/pushHost")
	infoLog.Printf("RabbitMQ URL=%s", rabbitMQUrl)
	// init mongo
	// 创建mongodb对象
	mongo_url := cfg.Section("MONGO").Key("url").MustString("localhost")
	infoLog.Printf("Mongo url=%s", mongo_url)
	mongoSess, err = mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
		return
	}
	defer mongoSess.Close()
	// Optional. Switch the session to a monotonic behavior.
	// mongoSess.SetMode(mgo.Monotonic, true)
	requestJoinLimit = cfg.Section("REQUESTJOINLIMIT").Key("limit").MustInt(0)
	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	// init log mysql
	logMysqlHost := cfg.Section("LOGMYSQL").Key("mysql_host").MustString("127.0.0.1")
	logMysqlUser := cfg.Section("LOGMYSQL").Key("mysql_user").MustString("IMServerLog")
	logMysqlPasswd := cfg.Section("LOGMYSQL").Key("mysql_passwd").MustString("hello")
	logMysqlDbName := cfg.Section("LOGMYSQL").Key("mysql_db").MustString("HT_LOGDB")
	logMysqlPort := cfg.Section("LOGMYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("log host=%v user=%v passwd=%v dbname=%v port=%v",
		logMysqlHost,
		logMysqlUser,
		logMysqlPasswd,
		logMysqlDbName,
		logMysqlPort)

	logDb, err = sql.Open("mysql", logMysqlUser+":"+logMysqlPasswd+"@"+"tcp("+logMysqlHost+":"+logMysqlPort+")/"+logMysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open log mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	logChangLen := cfg.Section("LOGCHANLEN").Key("length").MustInt(2000)
	LogDbUtil = util.NewLogDbUtil(logDb, infoLog, uint32(logChangLen))
	LogDbUtil.Do()

	// 读取offline 配置
	offlineIp := cfg.Section("OFFLINE").Key("offline_ip").MustString("127.0.0.1")
	offlinePort := cfg.Section("OFFLINE").Key("offline_port").MustString("0")
	offlineConnLimit := cfg.Section("OFFLINE").Key("pool_limit").MustInt(1000)
	infoLog.Printf("offline server ip=%v port=%v connLimit=%v", offlineIp, offlinePort, offlineConnLimit)
	offlineApi = common.NewOfflineApiV2(offlineIp, offlinePort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, offlineConnLimit)

	// 读取mucStoreApi 配置
	mucStoreIp := cfg.Section("MUCSTORE").Key("ip").MustString("127.0.0.1")
	mucStorePort := cfg.Section("MUCSTORE").Key("port").MustString("0")
	mucStoreConnLimit := cfg.Section("MUCSTORE").Key("pool_limit").MustInt(1000)
	infoLog.Printf("MUCSTORE server ip=%v port=%v connLimit=%v", mucStoreIp, mucStorePort, mucStoreConnLimit)
	mucStoreApi = common.NewMucMsgStoreApi(mucStoreIp, mucStorePort, 3*time.Second, 3*time.Second, &common.HeadV2Protocol{}, mucStoreConnLimit)

	// 读取pushServerApi 配置
	pushIp := cfg.Section("PUSHSERVER").Key("ip").MustString("127.0.0.1")
	pushPort := cfg.Section("PUSHSERVER").Key("port").MustString("0")
	pushConnLimit := cfg.Section("PUSHSERVER").Key("pool_limit").MustInt(1000)
	infoLog.Printf("PUSHSERVER server ip=%v port=%v connLimit=%v", pushIp, pushPort, pushConnLimit)
	pushServerApi := common.NewPushServerApi(pushIp, pushPort, 1*time.Second, 1*time.Second, &common.XTHeadProtocol{}, pushConnLimit)

	// 读取relation 配置
	relationIp := cfg.Section("RELATION").Key("ip").MustString("127.0.0.1")
	relationPort := cfg.Section("RELATION").Key("port").MustString("0")
	relationConnLimit := cfg.Section("RELATION").Key("pool_limit").MustInt(1000)
	infoLog.Printf("RELATION server ip=%v port=%v connLimit=%v", relationIp, relationPort, relationConnLimit)
	relationApi = common.NewRelationApi(relationIp, relationPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, relationConnLimit)

	// 读取chat record 配置
	chatRecordIp := cfg.Section("CHATRECORD").Key("ip").MustString("127.0.0.1")
	chatRecordPort := cfg.Section("CHATRECORD").Key("port").MustString("0")
	chatRecordConnLimit := cfg.Section("CHATRECORD").Key("pool_limit").MustInt(1000)
	infoLog.Printf("CHATRECORD server ip=%v port=%v connLimit=%v", chatRecordIp, chatRecordPort, chatRecordConnLimit)
	chatRecordApi = common.NewChatRecordApi(chatRecordIp, chatRecordPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, chatRecordConnLimit)

	// 读取lesson store api 配置
	lessonStoreIp := cfg.Section("LESSONSTORE").Key("ip").MustString("127.0.0.1")
	lessonStorePort := cfg.Section("LESSONSTORE").Key("port").MustString("0")
	lessonStoreConnLimit := cfg.Section("LESSONSTORE").Key("pool_limit").MustInt(1000)
	infoLog.Printf("LESSONSTORE server ip=%v port=%v connLimit=%v", lessonStoreIp, lessonStorePort, lessonStoreConnLimit)
	lessonMsgStoreApi = common.NewLessonMsgStoreApi(lessonStoreIp, lessonStorePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, lessonStoreConnLimit)

	// 读取group lesson api 配置
	groupLessonIp := cfg.Section("GROUPLESSON").Key("ip").MustString("127.0.0.1")
	groupLessonPort := cfg.Section("GROUPLESSON").Key("port").MustString("0")
	groupLessonConnLimit := cfg.Section("GROUPLESSON").Key("pool_limit").MustInt(1000)
	infoLog.Printf("GROUPLESSON server ip=%v port=%v connLimit=%v", groupLessonIp, groupLessonPort, groupLessonConnLimit)
	groupLessonApi = common.NewGroupLessonApi(groupLessonIp, groupLessonPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, groupLessonConnLimit)

	// 读取walletApi 配置
	walletIp := cfg.Section("WALLET").Key("ip").MustString("127.0.0.1")
	walletPort := cfg.Section("WALLET").Key("port").MustString("0")
	walletConnLimit := cfg.Section("WALLET").Key("pool_limit").MustInt(1000)
	infoLog.Printf("wallet server ip=%v port=%v connLimit=%v", walletIp, walletPort, walletConnLimit)
	walletApi = common.NewWalletApi(walletIp, walletPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, walletConnLimit)

	// user info cache api
	userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	infoLog.Printf("user info cache ip=%v port=%v", userInfoIp, userInfoPort)
	userInfoCacheApi = common.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// 读取IMServer 配置
	imCount := cfg.Section("IMSERVER").Key("imserver_cnt").MustInt(2)
	imConnLimit := cfg.Section("IMSERVER").Key("pool_limit").MustInt(1000)
	imServer := make(map[string]*common.ImServerApiV2, imCount)
	imOldServer := make(map[string]*common.ImServerApiV2, imCount)
	infoLog.Printf("IMServer Count=%v", imCount)
	for i := 0; i < imCount; i++ {
		ipKey := "imserver_ip_" + strconv.Itoa(i)
		ipOnlineKye := "imserver_ip_online_" + strconv.Itoa(i)
		portKey := "imserver_port_" + strconv.Itoa(i)
		oldPortKey := "imserver_old_port_" + strconv.Itoa(i)
		imIp := cfg.Section("IMSERVER").Key(ipKey).MustString("127.0.0.1")
		imIpOnline := cfg.Section("IMSERVER").Key(ipOnlineKye).MustString("127.0.0.1")
		imPort := cfg.Section("IMSERVER").Key(portKey).MustString("18380")
		imOldPort := cfg.Section("IMSERVER").Key(oldPortKey).MustString("18280")

		infoLog.Printf("im server ip=%v ip_online=%v port=%v oldPort=%v", imIp, imIpOnline, imPort, imOldPort)
		imServer[imIpOnline] = common.NewImServerApiV2(imIp, imPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, imConnLimit)
		imOldServer[imIpOnline] = common.NewImServerApiV2(imIp, imOldPort, 3*time.Second, 3*time.Second, &common.XTHeadProtocol{}, imConnLimit)
	}

	// 读取PC 端IMServer 配置
	pcImCount := cfg.Section("PCIMSERVER").Key("pc_imserver_cnt").MustInt(1)
	pcImConnLimit := cfg.Section("PCIMSERVER").Key("pc_pool_limit").MustInt(1000)
	pcImServer := make(map[string]*common.ImServerApiV2, pcImCount)
	infoLog.Printf("PCIMServer Count=%v", pcImCount)
	for i := 0; i < pcImCount; i++ {
		pcIpKey := "pc_imserver_ip_" + strconv.Itoa(i)
		pcIpOnlineKye := "pc_imserver_ip_online_" + strconv.Itoa(i)
		pcPortKey := "pc_imserver_port_" + strconv.Itoa(i)
		pcImIp := cfg.Section("PCIMSERVER").Key(pcIpKey).MustString("127.0.0.1")
		pcImIpOnline := cfg.Section("PCIMSERVER").Key(pcIpOnlineKye).MustString("127.0.0.1")
		pcImPort := cfg.Section("PCIMSERVER").Key(pcPortKey).MustString("18380")
		infoLog.Printf("pc im server ip=%v ip_online=%v port=%v", pcImIp, pcImIpOnline, pcImPort)
		pcImServer[pcImIpOnline] = common.NewImServerApiV2(pcImIp, pcImPort, 3*time.Second, 3*time.Second, &common.HeadV3Protocol{}, pcImConnLimit)
	}
	// nsq producer config
	nsqUrl := cfg.Section("NSQ").Key("url").MustString("127.0.0.1:4150")
	nsqConfig := nsq.NewConfig()
	globalProducer, err = nsq.NewProducer(nsqUrl, nsqConfig)
	if err != nil {
		log.Fatalf("main nsq.NewProcucer failed nsqUrl=%s", nsqUrl)
	}
	roomActionTopic = cfg.Section("ROOMACTION").Key("topic").MustString("group_update")

	//创建RoomManager 和 DbUtil 对象
	DbUtil = util.NewDbUtil(db, mongoSess, infoLog)
	roomManager = util.NewRoomManager(DbUtil, mcApi, offlineApi, pushServerApi, imServer, imOldServer, pcImServer, infoLog)
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	// 创建请求滤器
	reqRecord = map[string]int64{}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// 启动tikcer
	ticker = time.NewTicker(time.Second * 60)
	go func() {
		index := 0
		for range ticker.C {
			reqLock.Lock()
			curSecond := time.Now().Unix()
			for k, v := range reqRecord {
				infoLog.Printf("main key=%v value=%v", k, v)
				if v+REQTHRESHOLD < curSecond {
					infoLog.Printf("main delete key=%v value=%v", k, v)
					delete(reqRecord, k)
				}
			}
			infoLog.Printf("main remain req size=%v", len(reqRecord))
			reqLock.Unlock()

			infoLog.Printf("main ticker tick index=%v", index)
			index++
		}
	}()

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	ticker.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
