// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// Error type
var (
	ErrWriteBlocking = errors.New("write packet was blocking")
)

const (
	MUCMESSAGELIMIT = 50
)

type MUCMessageLog struct {
	FromId       uint32
	RoomId       uint32
	MsgType      uint8
	Msg          []byte
	MsgTimeStamp int64
}

type LogDbUtil struct {
	db             *sql.DB
	infoLog        *log.Logger
	packetSendChan chan *MUCMessageLog // packeet receive chanel
}

func NewLogDbUtil(mysqlDb *sql.DB, logger *log.Logger, chanLen uint32) *LogDbUtil {
	return &LogDbUtil{
		db:             mysqlDb,
		infoLog:        logger,
		packetSendChan: make(chan *MUCMessageLog, chanLen),
	}
}

func (this *LogDbUtil) Do() {
	asyncDo(this.writeLoop)
}

func (this *LogDbUtil) writeLoop() {
	defer func() {
		recover()
	}()

	var mucMsgs []*MUCMessageLog
	for {
		select {
		case p := <-this.packetSendChan:
			mucMsgs = append(mucMsgs, p)
			if len(mucMsgs) >= MUCMESSAGELIMIT {
				err := this.BatchWriteMucMessageLog(mucMsgs)
				if err != nil {
					this.infoLog.Printf("writeLoop failed err=%s", err)
				} else {
					this.infoLog.Printf("writeLoop success")
				}
				// 清空mucMsgs
				mucMsgs = make([]*MUCMessageLog, 0)
			}
		}
	}
}

func asyncDo(fn func()) {
	go func() {
		fn()
	}()
}

func (this *LogDbUtil) AsyncAddMucMessageLog(p *MUCMessageLog, timeout time.Duration) (err error) {
	// this.infoLog.Printf("AsyncAddMucMessageLog from=%v roomId=%v type=%v timestamp=%v msg=%x", p.FromId, p.RoomId, p.MsgType, p.MsgTimeStamp, p.Msg)
	if timeout == 0 {
		select {
		case this.packetSendChan <- p:
			return nil

		default:
			return ErrWriteBlocking
		}

	} else {
		select {
		case this.packetSendChan <- p:
			return nil

		case <-time.After(timeout):
			return ErrWriteBlocking
		}
	}
}

func (this *LogDbUtil) WriteMucMessageLog(mucLog *MUCMessageLog) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}

	if mucLog.RoomId == 0 || mucLog.FromId == 0 {
		return ErrDbParam
	}

	_, err = this.db.Exec("INSERT INTO LOG_MUC_MESSAGE(FROMID,ROOMID,TYPE,CONTENT,LENGTH,TIME) VALUES (?, ?, ?, ?, ?, FROM_UNIXTIME(?))",
		mucLog.FromId,
		mucLog.RoomId,
		mucLog.MsgType,
		mucLog.Msg,
		len(mucLog.Msg),
		mucLog.MsgTimeStamp)
	if err != nil {
		this.infoLog.Printf("WriteMucMessageLog insert faield fromId=%v roomId=%v  err=%v", mucLog.FromId, mucLog.RoomId, err)
		return err
	} else {
		return nil
	}
}

//2017-01-12 09:56:56
func TimeStampToSqlTime(ts int64) (outTime string) {
	tm := time.Unix(ts, 0)
	tm = tm.UTC()
	outTime = fmt.Sprintf("%4d-%02d-%02d %02d:%02d:%02d", tm.Year(), tm.Month(), tm.Day(), tm.Hour(), tm.Minute(), tm.Second())
	return outTime
}

func (this *LogDbUtil) BatchWriteMucMessageLog(mucLogs []*MUCMessageLog) (err error) {
	if this.db == nil {
		return ErrNilDbObject
	}
	stmt, err := this.db.Prepare("INSERT INTO LOG_MUC_MESSAGE(FROMID,ROOMID,TYPE,CONTENT,LENGTH,TIME) VALUES(?, ?, ?, ?, ?, ?)")
	if err != nil {
		stmt, err = this.db.Prepare("INSERT INTO LOG_MUC_MESSAGE(FROMID,ROOMID,TYPE,CONTENT,LENGTH,TIME) VALUES(?, ?, ?, ?, ?, ?)")
		if err != nil {
			this.infoLog.Printf("BatchWriteMucMessageLog this.db.Prepare return err=%s", err)
			return err
		}
	}

	for i, v := range mucLogs {
		if v.RoomId == 0 || v.FromId == 0 {
			this.infoLog.Printf("BatchWriteMucMessageLog index=%v from=%v roomId=%v type=%v err param", i, v.FromId, v.RoomId, v.MsgType)
			continue
		}
		_, err = stmt.Exec(v.FromId, v.RoomId, v.MsgType, v.Msg, len(v.Msg), TimeStampToSqlTime(v.MsgTimeStamp))
		if err != nil {
			this.infoLog.Printf("BatchWriteMucMessageLog index=%v from=%v roomId=%v type=%v exec err=%s", i, v.FromId, v.RoomId, v.MsgType, err)
		}
	}
	stmt.Close()
	return nil
}
