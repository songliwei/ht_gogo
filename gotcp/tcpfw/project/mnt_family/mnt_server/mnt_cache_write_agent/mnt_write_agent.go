package main

import (
	"fmt"
	"hash/crc64"
	"runtime/pprof"
	"strings"
	"sync"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_cache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/HT_GOGO/gotcp/tcpfw/project/mnt_family/mnt_server/mnt_cache_write_agent/util"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

type TaskEleme struct {
	HashName  string
	KeyValues []string
}

type ReloadTaskEleme struct {
	HashName string
	Expire   uint64
}

var (
	infoLog        *log.Logger
	redisMasterApi *common.RedisApi
	ssdbOperator   *util.SsdbOperator
	zsetLock       sync.Mutex // sync mutex goroutines use reqRecord
	hashMapLock    sync.Mutex // sync mutex goroutines use reqRecord
	// cpuProfile     string
	memProfile           string
	memProfileRate       int
	chanCountLimit       int
	reloadChanCountLimit int
	checkChanCountLimit  int
	indexToChan          map[int]chan *TaskEleme
	reloadIndexToChan    map[int]chan *ReloadTaskEleme
)

const (
	ProcTimeThresold = 300000
)

const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)

// func startCPUProfile() {
// 	if cpuProfile != "" {
// 		f, err := os.Create(cpuProfile)
// 		if err != nil {
// 			infoLog.Printf("Can not create cpu profile output file: %s", err)
// 			return
// 		}
// 		if err := pprof.StartCPUProfile(f); err != nil {
// 			infoLog.Printf("Can not start cpu profile: %s", err)
// 			f.Close()
// 			return
// 		}
// 	}
// }

// func stopCPUProfile() {
// 	if cpuProfile != "" {
// 		pprof.StopCPUProfile() // 把记录的概要信息写到已指定的文件
// 	}
// }

func startMemProfile() {
	if memProfile != "" && memProfileRate > 0 {
		runtime.MemProfileRate = memProfileRate
	}
}

func stopMemProfile() {
	if memProfile != "" {
		f, err := os.Create(memProfile)
		if err != nil {
			infoLog.Printf("Can not create mem profile output file: %s", err)
			return
		}
		if err = pprof.WriteHeapProfile(f); err != nil {
			infoLog.Printf("Can not write %s: %s", memProfile, err)
		}
		f.Close()
	}
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gomntagent/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch uint32(head.Cmd) {
	case uint32(ht_moment_cache.CMD_TYPE_CMD_SET_ZSET_LIST):
		go ProcSetZsetList(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_SET_MID_HASH_MAP):
		go ProcSetMidHashMap(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_SET_KEY_VALUE):
		go ProcSetKeyValue(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_HINC_HASH_MAP):
		go ProcHincReqBody(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_BATCH_ADD_MID_TO_HASH):
		go ProcBatchAddMidToHashMap(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_SET_USER_ALL_MNT_STATUS_REQ):
		go ProcSetUserAllMntReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_MNT_STATUS_REQ):
		go ProcModMntStatReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_OP_UID_IN_REDIS_REQ):
		go ProcOpUidInRedisReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_BACK_END_OP_UID_IN_REDIS_REQ):
		go ProcBackEndOpUidInRedisReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_RELOAD_HAHS_MAP):
		go ProcReloadHashMap(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_SET_HASH_MAP):
		go ProcSetHashMap(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_CLEAR_USER_INFO_CACHE):
		go ProcClearUserInfoCache(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_DELETE_KEY_REQ):
		go ProcDeleteKey(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_RELOAD_ZSET_REQ):
		go ProcReloadZset(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

// 1.proc set not share to uid list or hide uid list
func ProcSetZsetList(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetZsetListRspbody = &ht_moment_cache.SetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetZsetList invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/set_zset_list_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetZsetList proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetZsetListRspbody = &ht_moment_cache.SetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetZsetListReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSetZsetList GetSetZsetListReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetZsetListRspbody = &ht_moment_cache.SetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	infoLog.Printf("ProcSetZsetList zsetname=%s keyValueLen=%v", subReqBody.GetZsetName(), len(subReqBody.GetKeyScore()))
	if len(subReqBody.GetZsetName()) == 0 || len(subReqBody.GetKeyScore()) == 0 {
		infoLog.Printf("ProcSetZsetList zsetname=%s keyValue=%v input param error", subReqBody.GetZsetName(), subReqBody.GetKeyScore())
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
		rspBody.SetZsetListRspbody = &ht_moment_cache.SetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("empty param"),
			},
		}
		return true
	}

	zsetLock.Lock()
	defer zsetLock.Unlock()
	count, err := redisMasterApi.ZaddSlice(subReqBody.GetZsetName(), subReqBody.GetKeyScore())
	if err != nil {
		infoLog.Printf("ProcSetZsetList redisMasterApi.ZaddSlice() failed zsetname=%s err=%s", subReqBody.GetZsetName(), err)
		attr := "gomntagent/set_zset_list_failed"
		libcomm.AttrAdd(attr, 1)

		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetZsetListRspbody = &ht_moment_cache.SetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcSetZsetList zsetName=%s add count=%v", subReqBody.GetZsetName(), count)
	rspBody.SetZsetListRspbody = &ht_moment_cache.SetZsetListRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcSetZsetList uid=%v zsetName=%s totalProcTime=%v", head.Uid, subReqBody.GetZsetName(), totalProcTime)
	if totalProcTime > ProcTimeThresold {
		attr := "gomntagent/set_zset_list_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.proc set mid --> content 如果之前reids中没有缓存任何的mid-->内容的映射需要执行加载
// func ProcSetMidHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
// 	// parse packet
// 	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
// 	rspBody := new(ht_moment_cache.MntCacheRspBody)
// 	defer func() {
// 		SendRsp(c, head, rspBody, result)
// 	}()

// 	// 检查输入参数是否为空
// 	payLoad := packet.GetBody()
// 	if head == nil || len(payLoad) == 0 {
// 		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
// 		rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
// 			Status: &ht_moment_cache.MntCacheHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("invalid param"),
// 			},
// 		}
// 		infoLog.Printf("ProcSetMidHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
// 		return false
// 	}

// 	// add static
// 	attr := "gomntagent/set_hash_map_count"
// 	libcomm.AttrAdd(attr, 1)
// 	reqBody := new(ht_moment_cache.MntCacheReqBody)
// 	err := proto.Unmarshal(payLoad, reqBody)
// 	if err != nil {
// 		infoLog.Printf("ProcSetMidHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
// 		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
// 		rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
// 			Status: &ht_moment_cache.MntCacheHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("proto unmarshal failed"),
// 			},
// 		}
// 		return false
// 	}
// 	subReqBody := reqBody.GetSetMidHashMapReqbody()
// 	if subReqBody == nil {
// 		infoLog.Printf("ProcSetMidHashMap GetSetMidHashMapReqbody() failed")
// 		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
// 		rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
// 			Status: &ht_moment_cache.MntCacheHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("get req body failed"),
// 			},
// 		}
// 		return false
// 	}

// 	infoLog.Printf("ProcSetMidHashMap hashName=%s keyValueLen=%v",
// 		subReqBody.GetHashName(),
// 		len(subReqBody.GetKeyValue()))
// 	hashMapLock.Lock()
// 	defer hashMapLock.Unlock()
// 	// check if exist midindex hash map
// 	ret, err := redisMasterApi.Exists(util.MidIndexHashMap)
// 	if err != nil {
// 		infoLog.Printf("ProcSetMidHashMap redisMasterApi.Exists failed err=%s hashName=%s keyValueLen=%v",
// 			err,
// 			subReqBody.GetHashName(),
// 			len(subReqBody.GetKeyValue()))
// 		// add static
// 		attr := "gomntagent/set_hash_map_inter_err_count"
// 		libcomm.AttrAdd(attr, 1)
// 		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
// 		rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
// 			Status: &ht_moment_cache.MntCacheHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("internal error"),
// 			},
// 		}
// 		return false
// 	}
// 	if !ret {
// 		// 不存在midindex hash map reload it from ALL#LIST hash map
// 		err = ReloadMomentContent()
// 		if err != nil {
// 			infoLog.Printf("ProcSetMidHashMap ReloadMomentContent failed err=%s", err)
// 			// add static
// 			attr := "gomntagent/reload_mnt_count_inter_err_count"
// 			libcomm.AttrAdd(attr, 1)
// 			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
// 			rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
// 				Status: &ht_moment_cache.MntCacheHeader{
// 					Code:   proto.Uint32(uint32(result)),
// 					Reason: []byte("relad mnt content failed"),
// 				},
// 			}
// 			return false
// 		}
// 	}

// 	err = redisMasterApi.Hmset(subReqBody.GetHashName(), subReqBody.GetKeyValue())
// 	if err != nil {
// 		infoLog.Printf("ProcSetMidHashMap redisMasterApi.Hmset() failed hashName=%s err=%s", subReqBody.GetHashName(), err)
// 		attr := "gomntagent/set_hash_map_failed"
// 		libcomm.AttrAdd(attr, 1)
// 		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
// 		rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
// 			Status: &ht_moment_cache.MntCacheHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("set hash map error"),
// 			},
// 		}
// 		return false
// 	}
// 	//  将 mid-->时间戳写入MidIndexHashMap 中
// 	maxIndex, err := redisMasterApi.Hincrby(util.MidIndexHashMap, util.MaxIndexKey, 1)
// 	if err != nil {
// 		attr := "gomntagent/hincrby_redis_failed_count"
// 		libcomm.AttrAdd(attr, 1)
// 		infoLog.Printf("ProcSetMidHashMap redisMasterApi.Hincrby mid=%s err=%s failed", subReqBody.GetHashName(), err)
// 		return false
// 	}
// 	err = redisMasterApi.Hset(util.MidIndexHashMap, fmt.Sprintf("%v", maxIndex), subReqBody.GetHashName())
// 	if err != nil {
// 		// add static
// 		attr := "gomntagent/set_redis_failed_count"
// 		libcomm.AttrAdd(attr, 1)
// 		infoLog.Printf("ProcSetMidHashMap redisMasterApi.Hmset mid=%s err=%s failed", subReqBody.GetHashName(), err)
// 	}
// 	// 检查moment的数据是否超过限制
// 	err = CheckMomentContentCount(util.MomentCountThreshold)
// 	if err != nil {
// 		// add static
// 		attr := "gomntagent/check_hashmap_failed_count"
// 		libcomm.AttrAdd(attr, 1)
// 		infoLog.Printf("ProcSetMidHashMap CheckHashMapCount mid=%s err=%s failed", subReqBody.GetHashName(), err)
// 	}

// 	// infoLog.Printf("ProcSetMidHashMap hashMap=%s add success", subReqBody.GetHashName())
// 	rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
// 		Status: &ht_moment_cache.MntCacheHeader{
// 			Code:   proto.Uint32(uint32(result)),
// 			Reason: []byte("success"),
// 		},
// 	}
// 	totalProcTime := packet.CalcProcessTime()
// 	infoLog.Printf("ProcSetMidHashMap uid=%v hashName=%s totalProcTime=%v", head.Uid, subReqBody.GetHashName(), totalProcTime)
// 	if totalProcTime > ProcTimeThresold {
// 		attr := "gomntagent/set_hash_map_proc_slow"
// 		libcomm.AttrAdd(attr, 1)
// 	}
// 	return true
// }
func Crc64(input string) uint64 {
	tab := crc64.MakeTable(crc64.ISO)
	return crc64.Checksum([]byte(input), tab)
}

func ProcSetMidHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcSetMidHashMap no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetMidHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/set_hash_map_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetMidHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetMidHashMapReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSetMidHashMap GetSetMidHashMapReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// infoLog.Printf("ProcSetMidHashMap hashMap=%s add success", subReqBody.GetHashName())
	rspBody.SetMidHashMapRspbody = &ht_moment_cache.SetMidHashMapRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	task := &TaskEleme{
		HashName:  subReqBody.GetHashName(),
		KeyValues: subReqBody.GetKeyValue(),
	}
	index := int(Crc64(subReqBody.GetHashName()) % uint64(chanCountLimit))
	infoLog.Printf("ProcSetMidHashMap hashName=%s keyValueLen=%v index=%v",
		subReqBody.GetHashName(),
		len(subReqBody.GetKeyValue()),
		index)

	select {
	case indexToChan[index] <- task:
		infoLog.Printf("ProcSetMidHashMap message hashName=%s index=%v put into chan succ", subReqBody.GetHashName(), index)
	}

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcSetMidHashMap uid=%v hashName=%s totalProcTime=%v", head.Uid, subReqBody.GetHashName(), totalProcTime)
	if totalProcTime > ProcTimeThresold {
		attr := "gomntagent/set_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}
func ReloadMomentContent() (err error) {
	if ssdbOperator == nil {
		err = util.ErrNilDbObject
		return err
	}
	var keys, values []string
	var keyStart int64 = util.BaseMntIndex
	var keyEnd int64
	keyEnd, err = ssdbOperator.GetMaxSeqId(util.AllListHashMap)
	if err != nil {
		infoLog.Printf("ReloadMomentContent ssdbOperator.GetMaxSeqId hashName=%s err=%s",
			util.AllListHashMap,
			err)
		return err
	}
	infoLog.Printf("ReloadMomentContent hashmap=%s keyEnd=%v", util.AllListHashMap, keyEnd)
	maxIndex := fmt.Sprintf("%v", keyEnd)
	for repeat := 0; repeat < util.MomentCountThreshold; repeat += util.HScanOperaterCount {
		itemKeys, itemValues, err := ssdbOperator.HscanArray(util.AllListHashMap, keyStart, keyEnd, util.HScanOperaterCount, true)
		if err != nil {
			infoLog.Printf("ReloadMomentContent ssdbOperator.HscanArray failed")
			return err
		}
		if len(itemKeys) != len(itemValues) {
			infoLog.Printf("ReloadMomentContent ssdbOperator.HscanArray hashName=%s keyLen=%v valuesLen=%v not equal", util.AllListHashMap, len(itemKeys), len(itemValues))
			err = util.ErrSsdbResult
			return err
		}

		keys = append(keys, itemKeys...)
		values = append(values, itemValues...)
		// 如果帖子数为空 或者keys的个数小余1000 说明加载完成直接break
		if len(itemKeys) < util.HScanOperaterCount || keyEnd < util.HScanOperaterCount {
			infoLog.Printf("ReloadMomentContent hansName=%s reload over maxIndex=%s", util.AllListHashMap, maxIndex)
			break
		}
		// 否则将keyEnd 的需要减少util.HScanOperaterCount 1000
		keyEnd = keyEnd - util.HScanOperaterCount
	}

	for index := len(values) - 1; index > 0; index -= 1 {
		key := keys[index]     // 序号
		value := values[index] // mid
		// step1 加载mid全部内容
		outKeys, outValues, err := ssdbOperator.MultiHgetAllSlice(value)
		// 检测加载mid的内存是否成功
		if err != nil || len(outKeys) == 0 || len(outValues) == 0 || len(outKeys) != len(outValues) {
			// add static
			attr := "gomntagent/reload_count_from_ssdb_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ReloadMomentContent key=%s mid=%s ssdbOperator.MultiHgetAll failed err=%s", key, value, err)
			continue
		}
		// outKeys 添加完成之后 判断是否存在moment_creater 如果不存在moment_creater 字段则从content中解析出来之后重新设置回ssdb中
		existKey := false
		for _, v := range outKeys {
			if v == util.MomentCreater {
				existKey = true
				break
			}
		}
		infoLog.Printf("ReloadMomentContent hashName=%s existKey=%v", value, existKey)
		if !existKey {
			infoLog.Printf("ReloadMomentContent hashName=%s not exist moment creater", value)
			// Step4 将mid-->uid 添加到redis中
			var contentValue string
			for i, v := range outKeys {
				if v == util.MntContentKey {
					contentValue = outValues[i]
					break
				}
			}
			if len(contentValue) > 0 {
				// infoLog.Printf("ProcGetHashMap hashname=%s contentValue=%s", value, contentValue)
				storeMomentBody := new(ht_moment_store.StoreMomentBody)
				err = proto.Unmarshal([]byte(contentValue), storeMomentBody)
				if err != nil {
					attr := "gomntagent/proto_unmarshal_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ReloadMomentContent proto Unmarshal failed hashname=%s contentValueLen=%v", value, len(contentValue))
				} else {
					setValue := fmt.Sprintf("%v", storeMomentBody.GetUserid())
					// 将moment的创建着写入ssdb中
					err = ssdbOperator.Hset(value, util.MomentCreater, setValue)
					if err != nil {
						attr := "gomntagent/ssdb_hset_failed"
						libcomm.AttrAdd(attr, 1)
						infoLog.Printf("ReloadMomentContent hset hashName=%s key=%s value=%s err=%s", value, util.MomentCreater, setValue, err)
					} else {
						infoLog.Printf("ReloadMomentContent hset hashName=%s key=%s value=%s", value, util.MomentCreater, setValue)
					}
					// 将moment的创建者添加到keyValues中
					outKeys = append(outKeys, util.MomentCreater)
					outValues = append(outValues, setValue)
					// infoLog.Printf("ReloadMomentContent hashName=%s add moment create=%s", value, setValue)
				}
			}
		}
		var kvs []string
		for i, v := range outKeys {
			kvs = append(kvs, v)
			kvs = append(kvs, outValues[i])
		}
		// step2 将mid的详细内容设置到reids中
		err = redisMasterApi.Hmset(value, kvs)
		if err != nil {
			// add static
			attr := "gomntagent/reload_cont_set_redis_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ReloadMomentContent redisMasterApi.Hmset mid=%s err=%s failed", value, err)
			continue
		}
		// Step3 将mid的过期时间 设置到redis中
		err = redisMasterApi.Expire(value, util.MidContentExpireTime) //如果是设置整个mid设置过期时间为3天
		if err != nil {
			// add static
			attr := "gomntagent/expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ReloadMomentContent redisMasterApi.Expire hashName=%s err=%s failed", value, err)
		}
	}
	return nil
}

func CheckMomentContentCount(limitCount uint32) (err error) {
	count, err := redisMasterApi.Hlen(util.MidIndexHashMap)
	if err != nil {
		infoLog.Printf("CheckMomentContentCount hashName=%s limitCount=%v err=%s", util.MidIndexHashMap, limitCount, err)
		return err
	}
	if count > int64(limitCount) {
		infoLog.Printf("CheckMomentContentCount hashName=%s limitCount=%v realCount=%v excced", util.MidIndexHashMap, limitCount, count)
		// 获取当前最大的序号
		maxIndex, err := redisMasterApi.Hincrby(util.MidIndexHashMap, util.MaxIndexKey, 0)
		if err != nil {
			infoLog.Printf("CheckMomentContentCount hashName=%s redisMasterApi.Hincrby failed err=%s", util.MidIndexHashMap, err)
			// add static
			attr := "gomntagent/check_mnt_count_failed_count"
			libcomm.AttrAdd(attr, 1)
			return err
		}
		// count 包含了0字段 需要加1
		minIndex := maxIndex + 1 - count + 2
		// 第一次检查时可能出现maxIndex < count 的情况此时需要修改minindex
		if minIndex < 1 {
			minIndex = 1
		}
		deleteCount := count - int64(limitCount)
		infoLog.Printf("CheckMomentContentCount maxIndex=%v minIndex=%v deleteCount=%v", maxIndex, minIndex, deleteCount)
		var keys []string
		for i := 0; i < int(deleteCount); i += 1 {
			keyItem := minIndex + int64(i)
			keys = append(keys, fmt.Sprintf("%v", keyItem))
		}
		// infoLog.Printf("CheckMomentContentCount keys=%v", keys)
		values, err := redisMasterApi.Hmget(util.MidIndexHashMap, keys)
		if err != nil {
			infoLog.Printf("CheckMomentContentCount redisMasterApi.Hmget failed hashName=%s err=%s", util.MidIndexHashMap, err)
			// add static
			attr := "gomntagent/check_mnt_count_failed_count"
			libcomm.AttrAdd(attr, 1)
			return err
		}

		for _, deleteMid := range values {
			// infoLog.Printf("CheckMomentContentCount delte mid=%s", deleteMid)
			if deleteMid == "" {
				// infoLog.Printf("CheckMomentContentCount delte mid=%s empty", deleteMid)
				continue
			}
			// 首先删除mid对应的详细内容
			err = redisMasterApi.Del(deleteMid)
			if err != nil {
				infoLog.Printf("CheckMomentContentCount delete mid=%s failed err=%s", deleteMid, err)
				continue
			}
		}
		deleteKeyCount, err := redisMasterApi.HBatchDel(util.MidIndexHashMap, keys)
		infoLog.Printf("CheckMomentContentCount HBatchDel deleteKeyCount=%v err=%s", deleteKeyCount, err)
		// for _, deleteKey := range keys {
		// 	// 再清除mid对应的时间戳
		// 	// infoLog.Printf("CheckMomentContentCount Hdel keys=%s ", deleteKey)
		// 	_, err = redisMasterApi.Hdel(util.MidIndexHashMap, deleteKey)
		// 	// if err != nil && err != common.ErrRedisExecFailed {
		// 	if err != nil {
		// 		infoLog.Printf("CheckMomentContentCount Hdel keys=%s failed err=%s", deleteKey, err)
		// 	}
		// }
	} else {
		infoLog.Printf("CheckMomentContentCount hashName=%s limitCount=%v realCount=%v not exceed", util.MidIndexHashMap, limitCount, count)
	}
	return nil
}

// 3.proc set key value
func ProcSetKeyValue(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetKeyValueRspbody = &ht_moment_cache.SetKeyValueRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetKeyValue invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/set_key_value_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetKeyValue proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetKeyValueRspbody = &ht_moment_cache.SetKeyValueRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetKeyValueReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSetKeyValue GetSetKeyValueReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetKeyValueRspbody = &ht_moment_cache.SetKeyValueRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	key := subReqBody.GetKey()
	value := subReqBody.GetValue()
	expire := subReqBody.GetExpireTs()
	infoLog.Printf("ProcSetKeyValue key=%s value=%v exipre=%v", key, value, expire)
	err = redisMasterApi.Set(key, value)
	if err != nil {
		infoLog.Printf("ProcSetKeyValue redisMasterApi.Set failed err=%s", err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetKeyValueRspbody = &ht_moment_cache.SetKeyValueRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("reids set err"),
			},
		}
		return false
	}

	// 更新整个hash map的过期时间
	err = redisMasterApi.Expire(key, uint64(expire))
	if err != nil {
		// add static
		attr := "gomntagent/set_key_expire_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcSetKeyValue redisMasterApi.Expire key=%s value=%s err=%s failed", key, value, err)
	}

	rspBody.SetKeyValueRspbody = &ht_moment_cache.SetKeyValueRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcSetKeyValue uid=%v key=%s value=%s totalProcTime=%v", head.Uid, key, value, totalProcTime)
	if totalProcTime > ProcTimeThresold {
		attr := "gomntagent/set_key_value_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 4.proc inc hash map specific key
func ProcHincReqBody(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcHincReqBody invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/hinc_hash_map_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcHincReqBody proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetHincReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcHincReqBody GetHincRspbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := subReqBody.GetHashName()
	incIterms := subReqBody.GetIncIterms()
	if strings.HasSuffix(hashName, util.UserInfoSuffix) {
		infoLog.Printf("ProcHincReqBody hashName=%s incIterms=%v continue", hashName, incIterms)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
		}
		return true
	}

	infoLog.Printf("ProcHincReqBody hashName=%s incIterms=%v", hashName, incIterms)
	hashMapLock.Lock()
	defer hashMapLock.Unlock()
	// check if exist midindex hash map
	ret, err := redisMasterApi.Exists(hashName)
	infoLog.Printf("ProcHincReqBody redisMasterApi.Exists hashName=%s ret=%v", hashName, ret)
	if err != nil {
		infoLog.Printf("ProcHincReqBody redisMasterApi.Exists failed err=%s hashName=%s",
			err,
			hashName)
		// add static
		attr := "gomntagent/hinc_hash_map_inter_err_count"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	if !ret {
		// 不存在 hashmap 直接加载，因为每次更新前都是先更新 db在更新redis
		err = ReloadHashMap(hashName)
		if err != nil {
			infoLog.Printf("ProcHincReqBody ReloadHashMap failed hashName=%s err=%s", hashName, err)
			// add static
			attr := "gomntagent/hinc_hash_map_inter_err_count"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("relad hash map failed"),
				},
			}
			return false
		}
	} else {
		for _, v := range incIterms {
			value, err := redisMasterApi.Hincrby(hashName, v.GetItermKey(), int(v.GetIncCount()))
			if err != nil {
				infoLog.Printf("ProcHincReqBody redisMasterApi.Hincrby hashName=%s v=%v incCount=%v faield",
					hashName,
					v.GetItermKey(),
					int(v.GetIncCount()))
				attr := "gomntagent/hinc_set_redis_failed"
				libcomm.AttrAdd(attr, 1)
			} else {
				infoLog.Printf("ProcHincReqBody redisMasterApi.Hincrby hashName=%s v=%v count=%v prevValue=%v success", hashName, v.GetItermKey(), int(v.GetIncCount()), value)
			}
		}
	}

	// 更新整个hash map的过期时间
	err = redisMasterApi.Expire(hashName, util.HashMapExpireTime)
	if err != nil {
		// add static
		attr := "gomntagent/set_key_expire_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcHincReqBody redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
	}
	// } else {
	// 	infoLog.Printf("ProcHincReqBody hashName=%s expire=%v", hashName, util.HashMapExpireTime)
	// }

	rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcHincReqBody uid=%v hashName=%s totalProcTime=%v", head.Uid, hashName, totalProcTime)
	if totalProcTime > ProcTimeThresold {
		attr := "gomntagent/hinc_req_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 加载整个hashmap 比如uid_user_info 或者整个mid hashmap
func ReloadHashMap(hashName string) (err error) {
	if ssdbOperator == nil || len(hashName) == 0 {
		err = util.ErrNilDbObject
		return err
	}
	// Step1 从ssdb 中加载 mid列表
	outKeys, outValues, err := ssdbOperator.MultiHgetAllSlice(hashName)
	if err != nil {
		infoLog.Printf("ReloadHashMap ssdbOperator.MultiHgetAll failed hashname=%s err=%s", hashName, err)
		return err
	}
	var kvs []string
	for i, v := range outKeys {
		kvs = append(kvs, v)
		kvs = append(kvs, outValues[i])
	}
	// infoLog.Printf("ReloadHashMap hashName=%s kvs=%v", hashName, kvs)
	// step2 将hashmap的详细内容设置到reids中
	err = redisMasterApi.Hmset(hashName, kvs)
	if err != nil {
		// add static
		attr := "gomntagent/reload_hashmap_set_redis_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ReloadHashMap redisMasterApi.Hmset hashName=%s err=%s failed", hashName, err)
		return err
	}
	// Step3 设置整个key的过期时间
	err = redisMasterApi.Expire(hashName, util.HashMapExpireTime)
	if err != nil {
		// add static
		attr := "gomntagent/set_key_expire_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ReloadHashMap redisMasterApi.Hmset hashName=%s err=%s failed", hashName, err)
		return err
	}
	return nil
}

// 5.proc batch add mid to hashmap
// 将mid添加到不同的桶中比如uid#LIST,LANG#2-5,LANG#2,LLANG#2,uid#FRIEND
// 其中uid#FRIEND，uid#LIST有过期时间
// LANG#2-5,LANG#2,LLANG#2 无过期时间
func ProcBatchAddMidToHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.BatchAddMidToHashmapRspbody = &ht_moment_cache.BatchAddMidToHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBatchAddMidToHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/batch_add_to_hm_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBatchAddMidToHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.BatchAddMidToHashmapRspbody = &ht_moment_cache.BatchAddMidToHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBatchAddMidToHashmapReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBatchAddMidToHashMap GetHincRspbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.BatchAddMidToHashmapRspbody = &ht_moment_cache.BatchAddMidToHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	items := subReqBody.GetHashIterms()
	hashMapLock.Lock()
	defer hashMapLock.Unlock()
	for _, v := range items {
		hashName := v.GetHashName()
		key := v.GetKey()
		value := v.GetValue()
		expire := v.GetExpire()
		// infoLog.Printf("ProcBatchAddMidToHashMap hashName=%s key=%v value=%v", hashName, key, value)
		// check if exist midindex hash map
		ret, err := redisMasterApi.Exists(hashName)
		if err != nil {
			infoLog.Printf("ProcBatchAddMidToHashMap redisMasterApi.Exists failed err=%s hashName=%s key=%v value=%v",
				err,
				hashName,
				key,
				value)
			// add static
			attr := "gomntagent/batch_add_hash_map_inter_err_count"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		// 对于不存在且没有过期时间的hashmap执行加载操作否则丢弃本次操作
		if !ret {
			// uid#FRIEND 不存在不加载
			suffixRet := strings.HasSuffix(hashName, util.FriendSuffix)
			if suffixRet {
				infoLog.Printf("ProcBatchAddMidToHashMap ReloadMids not exist hashName=%s suffix=%s suffixRet=%v",
					hashName,
					util.FriendSuffix,
					suffixRet)
				continue
			} else {
				// 不存在midindex hash map reload it from ALL#LIST hash map
				err = ReloadMids(hashName, expire)
				if err != nil {
					infoLog.Printf("ProcBatchAddMidToHashMap ReloadMids failed hashName=%s err=%s", hashName, err)
					// add static
					attr := "gomntagent/batch_add_hash_map_inter_err_count"
					libcomm.AttrAdd(attr, 1)
					// 继续后续的添加处理
					continue
				}
			}

		}
		// hashmap 存在 直接添加mid
		// 首先将最大index记录到hashmap 中
		err = redisMasterApi.Hset(hashName, util.MaxIndexKey, key)
		if err != nil {
			infoLog.Printf("ProcBatchAddMidToHashMap redisMasterApi.Hset hashName=%s key=%s value=%s failed", hashName, util.MaxIndexKey, key)
			attr := "gomntagent/batch_add_hash_map_inter_err_count"
			libcomm.AttrAdd(attr, 1)
		}
		// 再将新的index-->mid 记录到hashmap中
		err = redisMasterApi.Hset(hashName, key, value)
		if err != nil {
			infoLog.Printf("ProcBatchAddMidToHashMap redisMasterApi.Hset hashName=%s key=%s value=%s failed", hashName, key, value)
			attr := "gomntagent/batch_add_hash_map_inter_err_count"
			libcomm.AttrAdd(attr, 1)
		}

		// 更新整个hash map的过期时间
		if expire > 0 {
			err = redisMasterApi.Expire(hashName, expire)
			if err != nil {
				// add static
				attr := "gomntagent/set_key_expire_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcBatchAddMidToHashMap redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
			}
			suffixRet := strings.HasSuffix(hashName, util.FriendSuffix)
			if suffixRet {
				infoLog.Printf("ProcBatchAddMidToHashMap hashName=%s suffix=%s suffixRet=%v check moment count=%v",
					hashName,
					util.FriendSuffix,
					suffixRet,
					util.FriendCountThreshold)
				err = CheckMidCount(hashName, util.FriendCountThreshold)
				if err != nil {
					// add static
					attr := "gomntagent/check_mid_count_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ProcBatchAddMidToHashMap CheckMidCount hashName=%s err=%s failed", hashName, err)
				}
			}
		} else {
			// 如果永不过期需要检查一下元素个数是否超过阈值
			// 检查元素个数
			err = CheckMidCount(hashName, util.MidCountThreshold)
			if err != nil {
				// add static
				attr := "gomntagent/check_mid_count_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcBatchAddMidToHashMap CheckMidCount hashName=%s err=%s failed", hashName, err)
			}
		}

	}

	rspBody.BatchAddMidToHashmapRspbody = &ht_moment_cache.BatchAddMidToHashMapRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcBatchAddMidToHashMap uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > ProcTimeThresold {
		attr := "gomntagent/hinc_req_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}
func ReloadMids(hashName string, expire uint64) (err error) {
	if ssdbOperator == nil || len(hashName) == 0 {
		err = util.ErrNilDbObject
		return err
	}
	// 当过期时间为0 时代表永不过期 需要限制保存的数量
	// 仅仅针对uid#LIST/LANG#2-5/LANG#2/LLANG#2等桶
	if expire <= 0 {
		// Step1 从ssdb 中加载 mid列表
		var keyValues []string
		var keyStart int64 = util.BaseMntIndex
		keyEnd, err := ssdbOperator.GetMaxSeqId(hashName)
		if err != nil {
			infoLog.Printf("ReloadMids ssdbOperator.GetMaxSeqId hashName=%s err=%s",
				hashName,
				err)
			return err
		}
		maxIndex := fmt.Sprintf("%v", keyEnd)
		for repeat := 0; repeat < util.MidCountThreshold; repeat += util.HScanOperaterCount {
			keys, values, err := ssdbOperator.HscanArray(hashName, keyStart, keyEnd, util.HScanOperaterCount, true)
			if err != nil {
				infoLog.Printf("ReloadMids ssdbOperator.HscanArray failed")
				return err
			}
			if len(keys) != len(values) {
				infoLog.Printf("ReloadMids ssdbOperator.HscanArray hashName=%s keyLen=%v valuesLen=%v not equal", hashName, len(keys), len(values))
				err = util.ErrSsdbResult
				return err
			}
			for i := 0; i < len(keys); i += 1 {
				keyValues = append(keyValues, keys[i])
				keyValues = append(keyValues, values[i])
			}
			// 如果帖子数为空 或者keys的个数小余1000 说明加载完成直接break
			if len(keys) < util.HScanOperaterCount || keyEnd < util.HScanOperaterCount {
				infoLog.Printf("ReloadMids hansName=%s reload over maxIndex=%s keyEnd=%v", hashName, maxIndex, keyEnd)
				break
			}
			keyEnd -= util.HScanOperaterCount
		}

		// step1 将mid列表设置到reids中
		if len(keyValues) > 0 {
			err = redisMasterApi.Hmset(hashName, keyValues)
			if err != nil {
				// add static
				attr := "gomntagent/reload_mid_set_redis_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ReloadMids redisMasterApi.Hmset hashName=%s err=%s failed", hashName, err)
				return err
			}
		} else {
			infoLog.Printf("ReloadMids hansName=%s empty", hashName)
		}

		// Step3 将最大的序号保存到redis中
		err = redisMasterApi.Hset(hashName, util.MaxIndexKey, maxIndex)
		if err != nil {
			// add static
			attr := "gomntagent/reload_mid_set_redis_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ReloadMids redisMasterApi.Hmset hashName=%s err=%s failed", hashName, err)
			return err
		}
	} else { //当过期时间大于0 时 代表会过期需要加载整个hash 表到redis中 然后设置过期时间
		// Step1 从ssdb 中加载 mid列表
		var keyValues []string
		var keyStart int64 = util.BaseMntIndex
		keyEnd, err := ssdbOperator.GetMaxSeqId(hashName)
		if err != nil {
			infoLog.Printf("ReloadMids ssdbOperator.GetMaxSeqId hashName=%s err=%s",
				hashName,
				err)
			return err
		}
		maxIndex := fmt.Sprintf("%v", keyEnd)
		for repeat := 0; repeat < util.MidCountThreshold; repeat += util.HScanOperaterCount {
			keys, values, err := ssdbOperator.HscanArray(hashName, keyStart, keyEnd, util.HScanOperaterCount, true)
			if err != nil {
				infoLog.Printf("ReloadMids ssdbOperator.HscanArray failed")
				return err
			}
			if len(keys) != len(values) {
				infoLog.Printf("ReloadMids ssdbOperator.HscanArray hashName=%s keyLen=%v valuesLen=%v not equal", hashName, len(keys), len(values))
				err = util.ErrSsdbResult
				return err
			}
			for i, v := range keys {
				keyValues = append(keyValues, v)
				keyValues = append(keyValues, values[i])
			}
			// 如果帖子数为空 或者keys的个数小余1000 说明加载完成直接break
			if len(keys) < util.HScanOperaterCount || keyEnd < util.HScanOperaterCount {
				infoLog.Printf("ReloadMids hansName=%s reload over maxIndex=%s keyEnd=%v", hashName, maxIndex, keyEnd)
				break
			}
			keyEnd -= util.HScanOperaterCount
		}
		keyValues = append(keyValues, util.BucketMaxSeqKey)
		keyValues = append(keyValues, maxIndex)
		// step2 将hashmap的详细内容设置到reids中
		err = redisMasterApi.Hmset(hashName, keyValues)
		if err != nil {
			// add static
			attr := "gomntagent/reload_hashmap_set_redis_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ReloadMids redisMasterApi.Hmset hashName=%s err=%s failed", hashName, err)
			return err
		}
		// Step4 设置整个key的过期时间
		err = redisMasterApi.Expire(hashName, expire)
		if err != nil {
			// add static
			attr := "gomntagent/set_key_expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ReloadMids redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
			return err
		}
	}
	return nil
}
func CheckMidCount(hashName string, limitCount uint32) (err error) {
	count, err := redisMasterApi.Hlen(hashName)
	if err != nil {
		infoLog.Printf("CheckMidCount hashName=%s limitCount=%v err=%s", hashName, limitCount, err)
		return err
	}
	// 需要减去1才是正在的元素个数
	count -= 1
	if count > int64(limitCount) {
		infoLog.Printf("CheckMidCount hashName=%s limitCount=%v realCount=%v excced", hashName, limitCount, count)
		maxIndex, err := redisMasterApi.HgetInt64(hashName, util.MaxIndexKey)
		if err != nil {
			infoLog.Printf("CheckMidCount redisMasterApi.HgetInt64 failed hashName=%s err=%s", hashName, err)
			// add static
			attr := "gomntagent/check_mid_count_failed_count"
			libcomm.AttrAdd(attr, 1)
			return err
		}
		// 注意减去1之后才是真正的元素个数
		deleteCount := count - int64(limitCount)
		beginIndex := maxIndex - count + 1 // 注意需要+1 才是待删除的需要
		for i := 0; i < int(deleteCount); i += 1 {
			// infoLog.Printf("CheckMidCount delte index=%v", beginIndex)
			_, err := redisMasterApi.Hdel(hashName, fmt.Sprintf("%v", beginIndex))
			if err != nil {
				infoLog.Printf("CheckMidCount Hdel index=%v failed err=%s", beginIndex, err)
			}
			beginIndex += 1
		}
	} else {
		infoLog.Printf("CheckMidCount hashName=%s limitCount=%v realCount=%v not exceed", hashName, limitCount, count)
	}
	return nil
}

// 6.proc set user all mnt req
func ProcSetUserAllMntReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserAllMntReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/set_all_mnt_stat_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserAllMntReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserAllMntStatReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserAllMntReq GetSetUserAllMntStatReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uidSlic := subReqBody.GetUserid()
	stat := subReqBody.GetStat()
	infoLog.Printf("ProcSetUserAllMntReq uidSlic=%v stat=%v", uidSlic, stat)
	// param check
	if len(uidSlic) == 0 {
		infoLog.Printf("ProcSetUserAllMntReq input param error uidSlic=%v stat=%v uid=%v",
			uidSlic,
			stat,
			head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}

	// 循环遍历设置帖子的状态
	strStat := fmt.Sprintf("%v", stat)
	for _, v := range uidSlic {
		hashName := fmt.Sprintf("%v#LIST", v)
		result, err := redisMasterApi.Exists(hashName)
		if err != nil || result == false {
			infoLog.Printf("ProcSetUserAllMntReq redisMasterApi.Exists hashmap=%s err=%s", hashName, err)
			continue
		}

		keyValues, err := redisMasterApi.Hgetall(hashName)
		if err != nil {
			infoLog.Printf("ProcSetUserAllMntReq redisMasterApi.Hgetall hashmap=%s err=%s", hashName, err)
			continue
		}
		for index, mid := range keyValues {
			infoLog.Printf("ProcSetUserAllMntReq index=%s mid=%s", index, mid)
			err = redisMasterApi.Hset(mid, util.MntDeletedKey, strStat)
			if err != nil {
				infoLog.Printf("ProcSetUserAllMntReq set mid=%s err=%s", mid, err)
				continue
			}
			err = redisMasterApi.Expire(mid, util.MidContentExpireTime) //如果是设置整个mid设置过期时间为3天
			if err != nil {
				// add static
				attr := "gomntagent/expire_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcSetUserAllMntReq redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
			}
		}
	}

	// send resp
	rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcSetUserAllMntReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntagent/set_all_mnt_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 7.proc modify mnt status
func ProcModMntStatReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModMntStatReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/mod_mnt_stat_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModMntStatReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetModMntStatusReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcModMntStatReq GetSetUserAllMntStatReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	mid := subReqBody.GetMid()
	opType := subReqBody.GetOpType()
	opReason := subReqBody.GetOpReason()
	infoLog.Printf("ProcModMntStatReq mid=%s opType=%v opReason=%v", mid, opType, opReason)
	// param check
	if len(mid) == 0 {
		infoLog.Printf("ProcModMntStatReq input param error mid=%s opType=%v opReason=%v uid=%v", mid, opType, opReason, head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}
	// 设置mid对应uid的user_info
	postUid, err := GetUidByMid(string(mid))
	if err == nil {
		userInfo := fmt.Sprintf("%v_user_info", postUid)
		outKeys, outValues, err := ssdbOperator.MultiHgetAllSlice(userInfo)
		if err == nil && len(outKeys) == len(outValues) {
			var keyValues []string
			for i, v := range outKeys {
				keyValues = append(keyValues, v)
				keyValues = append(keyValues, outValues[i])
			}

			err = redisMasterApi.Hmset(userInfo, keyValues)
			if err == nil {
				// 设置这个hashmap 的过期时间
				err = redisMasterApi.Expire(userInfo, util.HashMapExpireTime)
				if err != nil {
					infoLog.Printf("ProcModMntStatReq redisMasterApi.Expire hashname=%s faield err=%s", userInfo, err)
				}
			} else {
				infoLog.Printf("ProcModMntStatReq redisMasterApi.Hmset hashName=%s err=%v", userInfo, err)
			}
		} else {
			infoLog.Printf("ProcModMntStatReq ssdbOperator.MultiHgetAll reqUid=%v mid=%s err=%s",
				head.Uid,
				mid,
				err)
			// add static
			attr := "gomntagent/hget_all_failed"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		infoLog.Printf("ProcModMntStatReq GetUidByMid reqUid=%v mid=%s failed", head.Uid, mid)
		// add static
		attr := "gomntagent/get_uid_failed"
		libcomm.AttrAdd(attr, 1)
	}
	// 查看mid对应的hashmap 是否存在 存在则设置
	exist, err := redisMasterApi.Exists(string(mid))
	if err != nil || exist == false {
		infoLog.Printf("ProcModMntStatReq redis not exist mid=%s opType=%v opReason=%v uid=%v", mid, opType, opReason, head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
		}
		return true
	}

	keyValues := []string{
		util.MntDeletedKey,
		fmt.Sprintf("%d", opType),
		util.MntOpReasonKey,
		string(opReason),
	}
	if opType == ht_moment_cache.MntStatOpType_OP_RESTORE ||
		opType == ht_moment_cache.MntStatOpType_OP_HIDE ||
		opType == ht_moment_cache.MntStatOpType_OP_DELETE ||
		opType == ht_moment_cache.MntStatOpType_OP_FOR_FOLLOWER_ONLY {
		err = redisMasterApi.Hmset(string(mid), keyValues)
		if err != nil {
			infoLog.Printf("ProcModMntStatReq redisMasterApi.MultiHset mid=%s opType=%v opReason=%v err=%s", mid, opType, opReason, err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		err = redisMasterApi.Expire(string(mid), util.MidContentExpireTime) //如果是设置整个mid设置过期时间为3天
		if err != nil {
			// add static
			attr := "gomntagent/expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcModMntStatReq redisMasterApi.Expire hashName=%s err=%s failed", string(mid), err)
		}
	} else {
		infoLog.Printf("ProcModMntStatReq unhandle mid=%s opType=%v opReason=%v", mid, opType, opReason)
	}

	// send resp
	rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcModMntStatReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntagent/mod_mnt_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 8.proc modify mnt status
func ProcOpUidInRedisReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.OpUidInRedisRspbody = &ht_moment_cache.OpUidInRedisRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcOpUidInRedisReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/op_uid_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcOpUidInRedisReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.OpUidInRedisRspbody = &ht_moment_cache.OpUidInRedisRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetOpUidInRedisReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcOpUidInRedisReq GetOpUidInRedisReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.OpUidInRedisRspbody = &ht_moment_cache.OpUidInRedisRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	listType := subReqBody.GetListType() //列表类型
	opType := subReqBody.GetOpType()     //操作类型
	opUid := subReqBody.GetOpUserId()    //被操作的uid
	selfUid := subReqBody.GetUserid()    //自己的uid
	addTime := subReqBody.GetAddTime()

	infoLog.Printf("ProcOpUidInRedisReq listType=%v opType=%v opUid=%v selfUid=%v addTime=%v",
		listType,
		opType,
		opUid,
		selfUid,
		addTime)
	// param check
	if opUid == 0 || selfUid == 0 {
		infoLog.Printf("ProcOpUidInRedisReq input param error listType=%v opType=%v opUid=%v selfUid=%v addTime=%v",
			listType,
			opType,
			opUid,
			selfUid,
			addTime)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.OpUidInRedisRspbody = &ht_moment_cache.OpUidInRedisRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}
	// 设置mid对应uid的user_info 将uid add 到opuid#not_share_set/opuid#hide_user_set 中
	switch listType {
	case ht_moment_cache.OpList_OP_HIDE_LIST:
		zsetName := fmt.Sprintf("%v%s", selfUid, util.HideOtherMntKey)
		zsetKey := fmt.Sprintf("%v", opUid)
		exist, err := redisMasterApi.Exists(zsetName)
		if err == nil && exist == true {
			err = UpdateZsetList(zsetName, zsetKey, addTime, opType)
			if err != nil {
				infoLog.Printf("ProcOpUidInRedisReq UpdateZsetList err zsetName=%s zsetKey=%s addTime=%v opType=%v err=%s",
					zsetName,
					zsetKey,
					addTime,
					opType,
					err)
			}
		}
	case ht_moment_cache.OpList_OP_NOT_SHARE_LIST:
		zsetName := fmt.Sprintf("%v%s", selfUid, util.NotShareMyMntKey)
		zsetKey := fmt.Sprintf("%v", opUid)
		exist, err := redisMasterApi.Exists(zsetName)
		if err == nil && exist == true {
			err = UpdateZsetList(zsetName, zsetKey, addTime, opType)
			if err != nil {
				infoLog.Printf("ProcOpUidInRedisReq UpdateZsetList err zsetName=%s zsetKey=%s addTime=%v opType=%v err=%s",
					zsetName,
					zsetKey,
					addTime,
					opType,
					err)
			}
		}
	default:
		infoLog.Printf("ProcOpUidInRedisReq unhandle listType=%v selfUid=%v op=%v opUid=%v", selfUid, opType, opUid)
	}
	// send resp
	rspBody.OpUidInRedisRspbody = &ht_moment_cache.OpUidInRedisRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcOpUidInRedisReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntagent/mod_mnt_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func UpdateZsetList(zsetName, zsetKey string, score uint32, opType ht_moment_cache.OpType) (err error) {
	if len(zsetName) == 0 || len(zsetKey) == 0 {
		err = util.ErrDbParam
		return err
	}
	switch opType {
	case ht_moment_cache.OpType_OP_ADD:
		// 添加元素
		_, err = redisMasterApi.Zadd(zsetName, int64(score), zsetKey)
		if err != nil {
			infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v err=%s", zsetName, zsetKey, opType, err)
			return err
		}
		// 更新当前版本
		_, err = redisMasterApi.Zadd(zsetName, int64(score), util.VersionField)
		if err != nil {
			infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v err=%s", zsetName, zsetKey, opType, err)
			return err
		}
	case ht_moment_cache.OpType_OP_DEL:
		// 删除元素
		_, err = redisMasterApi.Zrem(zsetName, zsetKey)
		if err != nil {
			infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v err=%s", zsetName, zsetKey, opType, err)
			return err
		}
		// 更新版本
		_, err = redisMasterApi.Zadd(zsetName, int64(score), util.VersionField)
		if err != nil {
			infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v err=%s", zsetName, zsetKey, opType, err)
			return err
		}
	default:
		infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v unhandle", zsetName, zsetKey, opType)
	}
	return nil
}

// 9.proc modify mnt status
func ProcBackEndOpUidInRedisReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.BackEndOpUidInRedisRspbody = &ht_moment_cache.BackEndOpUidInRedisRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBackEndOpUidInRedisReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/back_end_op_uid_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBackEndOpUidInRedisReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.BackEndOpUidInRedisRspbody = &ht_moment_cache.BackEndOpUidInRedisRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBackEndOpUidInRedisReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcBackEndOpUidInRedisReq GetOpUidInRedisReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.BackEndOpUidInRedisRspbody = &ht_moment_cache.BackEndOpUidInRedisRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	listType := subReqBody.GetListType()
	opType := subReqBody.GetOpType()
	opUid := subReqBody.GetOpUserId()
	uid := subReqBody.GetUserid()
	opReason := subReqBody.GetOpReason()
	addTime := subReqBody.GetAddTime()

	infoLog.Printf("ProcBackEndOpUidInRedisReq listType=%v opType=%v opUid=%v uid=%v opReason=%s addTime=%v",
		listType,
		opType,
		opUid,
		uid,
		opReason,
		addTime)
	// param check
	if opUid == 0 {
		infoLog.Printf("ProcBackEndOpUidInRedisReq input param error listType=%v opType=%v opUid=%v uid=%v opReason=%s addTime=%v",
			listType,
			opType,
			opUid,
			uid,
			opReason,
			addTime)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.BackEndOpUidInRedisRspbody = &ht_moment_cache.BackEndOpUidInRedisRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}
	// 设置mid对应uid的user_info
	from := head.Uid
	switch listType {
	case ht_moment_cache.OpList_OP_MOMENT_FOR_FOLLOWER_LIST:
		zsetName := util.MntForFollowerList
		zsetKey := fmt.Sprintf("%v", opUid)
		exist, err := redisMasterApi.Exists(zsetName)
		if err == nil && exist == true {
			err = UpdateZsetList(zsetName, zsetKey, addTime, opType)
			if err != nil {
				infoLog.Printf("ProcBackEndOpUidInRedisReq UpdateZsetList err zsetName=%s zsetKey=%s addTime=%v opType=%v err=%s",
					zsetName,
					zsetKey,
					addTime,
					opType,
					err)
			}
		}
	case ht_moment_cache.OpList_OP_MOMENT_FOR_SELF_LIST:
		zsetName := util.MntForSelfList
		zsetKey := fmt.Sprintf("%v", opUid)
		exist, err := redisMasterApi.Exists(zsetName)
		if err == nil && exist == true {
			err = UpdateZsetList(zsetName, zsetKey, addTime, opType)
			if err != nil {
				infoLog.Printf("ProcBackEndOpUidInRedisReq UpdateZsetList err zsetName=%s zsetKey=%s addTime=%v opType=%v err=%s",
					zsetName,
					zsetKey,
					addTime,
					opType,
					err)
			}
		}
	default:
		infoLog.Printf("ProcBackEndOpUidInRedisReq unhandle listType=%v from=%v op=%v opUid=%v", from, opType, opUid)
	}
	// send resp
	rspBody.BackEndOpUidInRedisRspbody = &ht_moment_cache.BackEndOpUidInRedisRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcBackEndOpUidInRedisReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntagent/back_end_po_uid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetUidByMid(mid string) (uid uint32, err error) {
	if len(mid) == 0 {
		infoLog.Printf("GetUidByMid input param error mid=%s", mid)
		err = util.ErrDbParam
		return uid, err
	}

	uidValue, err := redisMasterApi.Get(mid)
	if err == nil {
		tempUid, err := strconv.ParseUint(uidValue, 10, 32)
		if err == nil {
			uid = uint32(tempUid)
		} else {
			uid, err = ssdbOperator.GetUidByMid(mid)
		}
	} else {
		uid, err = ssdbOperator.GetUidByMid(mid)
	}
	return uid, err
}

// 10.proc reload mid list
// maybe uid#LISt/uid#FRIEND/LANG#2-5/LLANG#2
// func ProcReloadHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
// 	// parse packet
// 	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
// 	rspBody := new(ht_moment_cache.MntCacheRspBody)
// 	defer func() {
// 		SendRsp(c, head, rspBody, result)
// 	}()

// 	// 检查输入参数是否为空
// 	payLoad := packet.GetBody()
// 	if head == nil || len(payLoad) == 0 {
// 		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
// 		rspBody.ReloadHashMapRspbody = &ht_moment_cache.ReloadHashMapRspBody{
// 			Status: &ht_moment_cache.MntCacheHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("invalid param"),
// 			},
// 		}
// 		infoLog.Printf("ProcReloadHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
// 		return false
// 	}

// 	// add static
// 	attr := "gomntagent/reload_hm_count"
// 	libcomm.AttrAdd(attr, 1)
// 	reqBody := new(ht_moment_cache.MntCacheReqBody)
// 	err := proto.Unmarshal(payLoad, reqBody)
// 	if err != nil {
// 		infoLog.Printf("ProcReloadHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
// 		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
// 		rspBody.ReloadHashMapRspbody = &ht_moment_cache.ReloadHashMapRspBody{
// 			Status: &ht_moment_cache.MntCacheHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("proto unmarshal failed"),
// 			},
// 		}
// 		return false
// 	}
// 	subReqBody := reqBody.GetReloadHashMapReqbody()
// 	if subReqBody == nil {
// 		infoLog.Printf("ProcReloadHashMap GetReloadHashMapReqbody() failed")
// 		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
// 		rspBody.ReloadHashMapRspbody = &ht_moment_cache.ReloadHashMapRspBody{
// 			Status: &ht_moment_cache.MntCacheHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("get req body failed"),
// 			},
// 		}
// 		return false
// 	}
// 	items := subReqBody.GetHashList()
// 	hashMapLock.Lock()
// 	defer hashMapLock.Unlock()
// 	for _, v := range items {
// 		hashName := v.GetHashName()
// 		expire := v.GetExpire()
// 		// infoLog.Printf("ProcReloadHashMap hashName=%s expire=%v", hashName, expire)
// 		// check if exist midindex hash map
// 		ret, err := redisMasterApi.Exists(hashName)
// 		if err != nil {
// 			infoLog.Printf("ProcReloadHashMap redisMasterApi.Exists failed err=%s hashName=%s expire=%v",
// 				err,
// 				hashName,
// 				expire)
// 			// add static
// 			attr := "gomntagent/reload_hash_map_inter_err_count"
// 			libcomm.AttrAdd(attr, 1)
// 			continue
// 		}

// 		if ret {
// 			infoLog.Printf("ProcReloadHashMap hashName=%s already exist", hashName)
// 			continue
// 		}
// 		// 不存在midindex hash map reload it from ALL#LIST hash map
// 		err = ReloadMids(hashName, expire)
// 		if err != nil {
// 			infoLog.Printf("ProcReloadHashMap ReloadMids failed hashName=%s err=%s", hashName, err)
// 			// add static
// 			attr := "gomntagent/reload_hash_map_inter_err_count"
// 			libcomm.AttrAdd(attr, 1)
// 			// 继续后续的添加处理
// 			continue
// 		}
// 		// 更新整个hash map的过期时间
// 		if expire > 0 {
// 			err = redisMasterApi.Expire(hashName, expire)
// 			if err != nil {
// 				// add static
// 				attr := "gomntagent/expire_failed"
// 				libcomm.AttrAdd(attr, 1)
// 				infoLog.Printf("ProcReloadHashMap redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
// 			}
// 			suffixRet := strings.HasSuffix(hashName, util.FriendSuffix)
// 			if suffixRet {
// 				infoLog.Printf("ProcReloadHashMap hashName=%s suffix=%s suffixRet=%v check moment count=%v",
// 					hashName,
// 					util.FriendSuffix,
// 					suffixRet,
// 					util.FriendCountThreshold)
// 				err = CheckMidCount(hashName, util.FriendCountThreshold)
// 				if err != nil {
// 					// add static
// 					attr := "gomntagent/check_mid_count_failed"
// 					libcomm.AttrAdd(attr, 1)
// 					infoLog.Printf("ProcReloadHashMap CheckMidCount hashName=%s err=%s failed", hashName, err)
// 				}
// 			}
// 		} else {
// 			// 如果永不过期需要检查一下元素个数是否超过阈值
// 			// 检查元素个数
// 			err = CheckMidCount(hashName, util.MidCountThreshold)
// 			if err != nil {
// 				// add static
// 				attr := "gomntagent/check_mid_count_failed"
// 				libcomm.AttrAdd(attr, 1)
// 				infoLog.Printf("ProcReloadHashMap CheckMidCount hashName=%s err=%s failed", hashName, err)
// 			}
// 		}
// 	}
// 	rspBody.ReloadHashMapRspbody = &ht_moment_cache.ReloadHashMapRspBody{
// 		Status: &ht_moment_cache.MntCacheHeader{
// 			Code:   proto.Uint32(uint32(result)),
// 			Reason: []byte("success"),
// 		},
// 	}
// 	totalProcTime := packet.CalcProcessTime()
// 	infoLog.Printf("ProcReloadHashMap uid=%v totalProcTime=%v", head.Uid, totalProcTime)
// 	if totalProcTime > ProcTimeThresold {
// 		attr := "gomntagent/hinc_req_proc_slow"
// 		libcomm.AttrAdd(attr, 1)
// 	}
// 	return true
// }
func ProcReloadHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcReloadHashMap no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ReloadHashMapRspbody = &ht_moment_cache.ReloadHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcReloadHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/reload_hm_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcReloadHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ReloadHashMapRspbody = &ht_moment_cache.ReloadHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetReloadHashMapReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcReloadHashMap GetReloadHashMapReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ReloadHashMapRspbody = &ht_moment_cache.ReloadHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	rspBody.ReloadHashMapRspbody = &ht_moment_cache.ReloadHashMapRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 将任务放入队列中
	items := subReqBody.GetHashList()
	for _, v := range items {
		hashName := v.GetHashName()
		expire := v.GetExpire()
		task := &ReloadTaskEleme{
			HashName: hashName,
			Expire:   expire,
		}
		index := int(Crc64(hashName) % uint64(reloadChanCountLimit))
		infoLog.Printf("ProcReloadHashMap hashName=%s expire=%v index=%v",
			hashName,
			expire,
			index)
		select {
		case reloadIndexToChan[index] <- task:
			infoLog.Printf("ProcReloadHashMap message hashName=%s index=%v put into chan succ", hashName, index)
		}

	}

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcReloadHashMap uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > ProcTimeThresold {
		attr := "gomntagent/hinc_req_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 11.proc set hash map
func ProcSetHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetHashMapRspbody = &ht_moment_cache.SetHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/set_hash_map_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetHashMapRspbody = &ht_moment_cache.SetHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetHashMapReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSetHashMap GetSetHashMapReqbody() uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetHashMapRspbody = &ht_moment_cache.SetHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := subReqBody.GetHashName()
	keyValues := subReqBody.GetKeyValues()
	// infoLog.Printf("ProcSetHashMap hashName=%s keyValues=%v", hashName, keyValues)
	err = redisMasterApi.Hmset(hashName, keyValues)
	if err != nil {
		infoLog.Printf("ProcSetHashMap  GetSetHashMapReqbody() uid=%v cmd=0x%4x seq=%v err=%s", head.Uid, head.Cmd, head.Seq, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetHashMapRspbody = &ht_moment_cache.SetHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// 设置过期时间为3天
	err = redisMasterApi.Expire(hashName, util.ExpirePeriod)
	if err != nil {
		// add static
		attr := "gomntagent/set_key_expire_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcSetHashMap redisMasterApi.Expire key=%s err=%s failed", hashName, err)
	}

	result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody.SetHashMapRspbody = &ht_moment_cache.SetHashMapRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 12.proc clear uer info cache
func ProcClearUserInfoCache(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment_cache.ClearUserInfoCacheRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcClearUserInfoCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/clear_user_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcClearUserInfoCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment_cache.ClearUserInfoCacheRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetClearUserInfoCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcClearUserInfoCache GetClearUserInfoCacheReqbody() uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment_cache.ClearUserInfoCacheRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	userId := subReqBody.GetUserid()
	// infoLog.Printf("ProcClearUserInfoCache uid=%v", userId)
	hashName := GetUserInfoHashMapName(userId)
	err = redisMasterApi.Del(hashName)
	if err != nil {
		// add static
		attr := "gomntagent/delete_hashmap_failed_count"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcClearUserInfoCache redisMasterApi.Del hashName=%s err=%s", hashName, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment_cache.ClearUserInfoCacheRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody.ClearUserInfoCacheRspbody = &ht_moment_cache.ClearUserInfoCacheRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func GetUserInfoHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v_user_info", uid)
	return hashName
}

// 13.proc delete key req
func ProcDeleteKey(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.DeleteKeyRspbody = &ht_moment_cache.DeleteKeyRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDeleteKey invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/del_key_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDeleteKey proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.DeleteKeyRspbody = &ht_moment_cache.DeleteKeyRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDeleteKeyReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDeleteKey GetDeleteKeyReqbody() uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.DeleteKeyRspbody = &ht_moment_cache.DeleteKeyRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	userId := subReqBody.GetReqUid()
	key := subReqBody.GetKey()
	infoLog.Printf("ProcDeleteKey reqUid=%v key=%s", userId, key)
	err = redisMasterApi.Del(key)
	if err != nil {
		// add static
		attr := "gomntagent/delete_key_failed_count"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcDeleteKey redisMasterApi.Del reqUid=%v key=%s err=%s", userId, key, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.DeleteKeyRspbody = &ht_moment_cache.DeleteKeyRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody.DeleteKeyRspbody = &ht_moment_cache.DeleteKeyRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 14.proc reload zset list
func ProcReloadZset(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ReloadZsetRspbody = &ht_moment_cache.ReloadZsetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcReloadZset invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntagent/reload_zset_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcReloadZset proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ReloadZsetRspbody = &ht_moment_cache.ReloadZsetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetReloadZsetReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcReloadZset GetReloadZsetReqbody() uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ReloadZsetRspbody = &ht_moment_cache.ReloadZsetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	userId := subReqBody.GetReqUid()
	zsetName := subReqBody.GetZsetName()
	ttl := subReqBody.GetExpireTs()
	infoLog.Printf("ProcReloadZset reqUid=%v zsetName=%s ttl=%v ", userId, zsetName, ttl)
	ret, err := redisMasterApi.Exists(zsetName)
	if err == nil && ret {
		// already exist
		infoLog.Printf("ProcReloadZset reqUid=%v zsetName=%s ttl=%v already exist", userId, zsetName, ttl)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
		rspBody.ReloadZsetRspbody = &ht_moment_cache.ReloadZsetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
		}
		return true
	}

	zsetLock.Lock()
	defer zsetLock.Unlock()
	keyScore, err := ssdbOperator.ReloadZset(zsetName)
	if err != nil {
		// add static
		attr := "gomntagent/reload_zset_failed_count"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcReloadZset ssdbOperator.ReloadZset reqUid=%v zsetName=%s ttl=%v err=%s", userId, zsetName, ttl, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ReloadZsetRspbody = &ht_moment_cache.ReloadZsetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	val, err := redisMasterApi.ZaddStringSlice(zsetName, keyScore)
	if err != nil {
		// add static
		attr := "gomntagent/set_zset_failed_count"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcReloadZset redisMasterApi.ZaddSlice reqUid=%v zsetName=%s ttl=%v err=%s", userId, zsetName, ttl, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ReloadZsetRspbody = &ht_moment_cache.ReloadZsetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	if ttl > 0 {
		err = redisMasterApi.Expire(zsetName, uint64(ttl))
		if err != nil {
			attr := "gomntagent/expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcReloadZset redisMasterApi.ZaddSlice reqUid=%v zsetName=%s ttl=%v err=%s", userId, zsetName, ttl, err)
		}
	} else {
		infoLog.Printf("ProcReloadZset reqUid=%v zsetName=%s ttl=%v not expire", userId, zsetName, ttl)
	}
	infoLog.Printf("ProcReloadZset reqUid=%v zsetName=%s ttl=%v val=%v", userId, zsetName, ttl, val)
	result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody.ReloadZsetRspbody = &ht_moment_cache.ReloadZsetRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func TaskHandleLoop(index int) {
	defer func() {
		recover()
	}()
	infoLog.Printf("TaskHandleLoop index=%v", index)
	for {
		select {
		case task := <-indexToChan[index]:
			TaskSetMidHashMap(task.HashName, task.KeyValues)
		}
	}
}

func TaskSetMidHashMap(hashName string, keyValues []string) {
	infoLog.Printf("TaskSetMidHashMap hashName=%s keyValueLen=%v", hashName, len(keyValues))
	err := redisMasterApi.Hmset(hashName, keyValues)
	if err != nil {
		infoLog.Printf("TaskSetMidHashMap redisMasterApi.Hmset() failed hashName=%s err=%s", hashName, err)
		attr := "gomntagent/set_hash_map_failed"
		libcomm.AttrAdd(attr, 1)
		return
	}
	// 如果hashname 中包含了#LIST 则设置过期时间为1天
	suffixRet := strings.HasSuffix(hashName, util.UserSuffix)
	if suffixRet {
		infoLog.Printf("TaskSetMidHashMap hashName=%s suffix=%s suffixRet=%v ",
			hashName,
			util.UserSuffix,
			suffixRet)
		err = redisMasterApi.Expire(hashName, util.HashListExpireTime) //如果是加载整个uid#LIST 则设置过期时间为1天
		if err != nil {
			// add static
			attr := "gomntagent/expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("TaskSetMidHashMap redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
		}
	} else {
		infoLog.Printf("TaskSetMidHashMap hashName=%s set expire=%v", hashName, util.MidContentExpireTime)
		err = redisMasterApi.Expire(hashName, util.MidContentExpireTime) //如果是设置整个mid设置过期时间为3天
		if err != nil {
			// add static
			attr := "gomntagent/expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("TaskSetMidHashMap redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
		}
	}
	return
}

func ReloadTaskHandleLoop(index int) {
	defer func() {
		recover()
	}()
	infoLog.Printf("ReloadTaskHandleLoop index=%v", index)
	for {
		select {
		case task := <-reloadIndexToChan[index]:
			TaskReloadHashMap(task.HashName, task.Expire)
		}
	}
}

func TaskReloadHashMap(hashName string, expire uint64) {
	infoLog.Printf("TaskReloadHashMap hashName=%s expire=%v", hashName, expire)
	// check if exist midindex hash map
	ret, err := redisMasterApi.Exists(hashName)
	if err != nil {
		infoLog.Printf("TaskReloadHashMap redisMasterApi.Exists failed err=%s hashName=%s expire=%v",
			err,
			hashName,
			expire)
		// add static
		attr := "gomntagent/reload_hash_map_inter_err_count"
		libcomm.AttrAdd(attr, 1)
		return
	}

	if ret {
		infoLog.Printf("TaskReloadHashMap hashName=%s already exist", hashName)
		return
	}
	// 不存在midindex hash map reload it from ALL#LIST hash map
	err = ReloadMids(hashName, expire)
	if err != nil {
		infoLog.Printf("TaskReloadHashMap ReloadMids failed hashName=%s err=%s", hashName, err)
		// add static
		attr := "gomntagent/reload_hash_map_inter_err_count"
		libcomm.AttrAdd(attr, 1)
		// 继续后续的添加处理
		return
	}
	// 更新整个hash map的过期时间
	if expire > 0 {
		err = redisMasterApi.Expire(hashName, expire)
		if err != nil {
			// add static
			attr := "gomntagent/expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("TaskReloadHashMap redisMasterApi.Expire hashName=%s err=%s failed", hashName, err)
		}
		suffixRet := strings.HasSuffix(hashName, util.FriendSuffix)
		if suffixRet {
			infoLog.Printf("TaskReloadHashMap hashName=%s suffix=%s suffixRet=%v check moment count=%v",
				hashName,
				util.FriendSuffix,
				suffixRet,
				util.FriendCountThreshold)
			err = CheckMidCount(hashName, util.FriendCountThreshold)
			if err != nil {
				// add static
				attr := "gomntagent/check_mid_count_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("TaskReloadHashMap CheckMidCount hashName=%s err=%s failed", hashName, err)
			}
		}
	} else {
		// 如果永不过期需要检查一下元素个数是否超过阈值
		// 检查元素个数
		err = CheckMidCount(hashName, util.MidCountThreshold)
		if err != nil {
			// add static
			attr := "gomntagent/check_mid_count_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("TaskReloadHashMap CheckMidCount hashName=%s err=%s failed", hashName, err)
		}
	}
	return
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_moment_cache.MntCacheRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init ssdboperator
	ssdbIp := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	ssdbMinPoolSize := cfg.Section("SSDB").Key("ssdb_min_size").MustInt(5)
	ssdbMaxPoolSize := cfg.Section("SSDB").Key("ssdb_max_size").MustInt(50)
	infoLog.Printf("ssdb ip=%v port=%v minSize=%v maxSize=%v", ssdbIp, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	redisApi := common.NewRedisApi(ssdbIp + ":" + strconv.Itoa(ssdbPort))
	if err != nil {
		infoLog.Printf("common.NewRedisApi failed err=%s", err)
		return
	}
	ssdbOperator = util.NewSsdbOperator(redisApi, infoLog)

	// read profile config path
	// cpuProfile = cfg.Section("PROFILE").Key("cpu_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/cpu.prof")
	memProfile = cfg.Section("PROFILE").Key("mem_prof").MustString("/home/ht/goproj/mnt_family/mnt_server/write_agent/bin/mem.out")
	memProfileRate = cfg.Section("PROFILE").Key("mem_rate").MustInt(512 * 1024)
	// read ssdb config
	chanCountLimit = cfg.Section("CHAN").Key("count").MustInt(50)
	chanLenght := cfg.Section("CHAN").Key("length").MustInt(10000)
	indexToChan = make(map[int]chan *TaskEleme, chanCountLimit)
	for i := 0; i < chanCountLimit; i += 1 {
		indexToChan[i] = make(chan *TaskEleme, chanLenght)
	}
	infoLog.Printf("chan count=%v length=%v indexToChanLen=%v", chanCountLimit, chanLenght, len(indexToChan))
	for i := 0; i < chanCountLimit; i += 1 {
		go TaskHandleLoop(i)
	}

	// reload chan config
	reloadChanCountLimit = cfg.Section("RELOADCHAN").Key("count").MustInt(30)
	reloadChanLenght := cfg.Section("RELOADCHAN").Key("length").MustInt(10000)
	reloadIndexToChan = make(map[int]chan *ReloadTaskEleme, reloadChanCountLimit)
	for i := 0; i < reloadChanCountLimit; i += 1 {
		reloadIndexToChan[i] = make(chan *ReloadTaskEleme, reloadChanLenght)
	}
	infoLog.Printf("reload chan count=%v length=%v reloadChanLenght=%v", reloadChanCountLimit, reloadChanLenght, len(reloadIndexToChan))
	for i := 0; i < reloadChanCountLimit; i += 1 {
		go ReloadTaskHandleLoop(i)
	}
	// checkChanCountLimit = cfg.Section("CHECKCHAN").Key("count").MustInt(10000)
	// checkMomentCountChan = make(chan int, checkChanCountLimit)
	// go CheckTaskHandleLoop()
	//startCPUProfile()
	// startMemProfile()
	// 检查是否已经reload 过如果没有则重新加载最新的20万条信息流
	ret, err := redisMasterApi.Exists(util.ReloadKey)
	if err != nil {
		infoLog.Printf("redisMasterApi.Exists failed err=%s key=%s exist main process", err, util.ReloadKey)
		return
	}
	if !ret {
		infoLog.Printf("redisMasterApi.Exists key=%s not reload exec reload process", util.ReloadKey)
		// 不存在midindex hash map 时必须要加锁来加载
		hashMapLock.Lock()
		// 不存在midindex hash map reload it from ALL#LIST hash map
		err = ReloadMomentContent()
		// 加载完成之后必须要解锁
		hashMapLock.Unlock()
		if err != nil {
			infoLog.Printf("ReloadMomentContent failed err=%s exist main process", err)
			// add static
			attr := "gomntagent/reload_mnt_count_inter_err_count"
			libcomm.AttrAdd(attr, 1)
			return
		}
		// 设置reload key
		err = redisMasterApi.Set(util.ReloadKey, "succ")
		if err != nil {
			infoLog.Printf("ReloadMomentContent over set util.ReloadKey failed try again err=%s", err)
			err = redisMasterApi.Set(util.ReloadKey, "succ")
			if err != nil {
				infoLog.Printf("ReloadMomentContent over set util.ReloadKey again failed err=%s must manal fix", err)
				return
			}
		}
	}

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	//stopCPUProfile()
	// stopMemProfile()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
