// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"fmt"
	"log"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/golang/protobuf/proto"
)

type SsdbOperator struct {
	redisApi *common.RedisApi
	infoLog  *log.Logger
}

func NewSsdbOperator(targetRedisApi *common.RedisApi, logger *log.Logger) *SsdbOperator {
	return &SsdbOperator{
		redisApi: targetRedisApi,
		infoLog:  logger,
	}
}

// 仅仅用于遍历uid#LIST/uid#FRIENDS/LANG#num-num/LANG#num/LLANG#num
// 这几个桶 因为这些桶的序号都是从100000000001 100亿零1开始递增的
// 使用redis driver 取到ssdb driver 时 使用Hmget 取代 Hscan
// 不能用于Hscan 其它的桶
func (this *SsdbOperator) HscanArray(setName string, keyStart, keyEnd, limit int64, reverse bool) (outKeys []string, outValues []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	this.infoLog.Printf("hashName=%s keyStart=%v keyEnd=%v limit=%v reverse=%v",
		setName,
		keyStart,
		keyEnd,
		limit,
		reverse)
	// Step1 检查输入、输出参数(keyStart, keyEnd]
	// 所有的序号从100000000001 开始
	if keyStart == 0 {
		keyStart = BaseMntIndex
	}
	if keyEnd == 0 {
		keyEnd, err = this.GetMaxSeqId(setName)
		if err != nil {
			this.infoLog.Printf("hashName=%s GetMaxSeqId failed err=%s", setName, err)
			return nil, nil, err
		}
	}

	if reverse { // 逆序遍历
		for i := keyEnd; i > keyStart; i -= 1 {
			this.infoLog.Printf("HscanArray hashName=%s key=%v", setName, i)
			outKeys = append(outKeys, fmt.Sprintf("%v", i))
			if len(outKeys) >= int(limit) {
				this.infoLog.Printf("HscanArray hashName=%s limit=%v current=%v break", setName, limit, i)
				break
			}
		}
	} else { // 顺序遍历
		keyEnd += 1 //需要能够取到keyEnd
		for i := keyStart + 1; i < keyEnd; i += 1 {
			this.infoLog.Printf("HscanArray hashName=%s key=%v", setName, i)
			outKeys = append(outKeys, fmt.Sprintf("%v", i))
			if len(outKeys) >= int(limit) {
				this.infoLog.Printf("HscanArray hashName=%s limit=%v current=%v break",
					setName,
					limit,
					i)
				break
			}
		}
	}
	this.infoLog.Printf("HscanArray hashName=%s outKeys=%s", setName, outKeys)
	outValues, err = this.redisApi.Hmget(setName, outKeys)
	if err != nil {
		this.infoLog.Printf("HscanArray hashName=%s keyStart=%v end=%v limit=%v reverse=%v err=%s",
			setName,
			keyStart,
			keyEnd,
			limit,
			reverse,
			err)
		return nil, nil, err
	}

	return outKeys, outValues, nil
}

func (this *SsdbOperator) GetMaxSeqId(hashName string) (value int64, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return value, err
	}
	exist, err := this.redisApi.Hexists(hashName, BucketMaxSeqKey)
	if err != nil {
		this.infoLog.Printf("GetMaxSeqId hashName=%s redisApi.Hexists err=%s",
			hashName,
			err)
		return value, err
	}
	if exist == 0 { // ssdb 中不存在最大的seqId这个key 先Hsize 再Hset回去
		val, err := this.redisApi.Hlen(hashName)
		if err != nil {
			this.infoLog.Printf("GetMaxSeqId hashName=%s hsize failed err=%v",
				hashName,
				err)
			return value, err
		}
		value = int64(BaseMntIndex) + val
		indexStr := fmt.Sprintf("%v", value)
		err = this.redisApi.Hset(hashName, BucketMaxSeqKey, indexStr)
		if err != nil {
			this.infoLog.Printf("GetMaxSeqId hashName=%s Hset failed err=%v",
				hashName,
				err)
			return value, err
		}
	} else { // ssdb 中存在最大的seqId 直接获取
		value, err = this.redisApi.HgetInt64(hashName, BucketMaxSeqKey)
		if err != nil {
			this.infoLog.Printf("GetMaxSeqId hashName=%s HgetInt64 failed err=%v",
				hashName,
				err)
			return value, err
		}
	}
	return value, err
}

// 仅用于取mid的详细内容
// 当元素个数<100 直接取全部
// 当元素个数>100 用hkeys取全部的keys再遍历全部的key分批100大小取
func (this *SsdbOperator) MultiHgetAllSlice(setName string) (outKeys []string, outValues []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	keyLen, err := this.redisApi.Hlen(setName)
	if err != nil {
		this.infoLog.Printf("MultiHgetAllSlice redisApi.Hlen hashName=%s err=%s", setName, err)
		return nil, nil, err
	}
	if keyLen < HGetAllCount {
		kvs, err := this.redisApi.Hgetall(setName)
		if err != nil {
			this.infoLog.Printf("MultiHgetAllSlice redisApi.Hgetall setName=%s err=%s", setName, err)
			return nil, nil, err
		} else {
			for k, v := range kvs {
				outKeys = append(outKeys, k)
				outValues = append(outValues, v)
			}
			return outKeys, outValues, nil
		}
	} else {
		keys, err := this.redisApi.Hkeys(setName)
		if err != nil {
			this.infoLog.Printf("MultiHgetAllSlice hashName=%s err=%s", setName, err)
			return nil, nil, err
		}
		keyCount := len(keys)
		if keyCount < HGetAllCount {
			kvs, err := this.redisApi.Hgetall(setName)
			if err != nil {
				this.infoLog.Printf("MultiHgetAllSlice redisApi.Hgetall setName=%s err=%s", setName, err)
				return nil, nil, err
			} else {
				for k, v := range kvs {
					outKeys = append(outKeys, k)
					outValues = append(outValues, v)
				}
				return outKeys, outValues, nil
			}
		}
		// keys 的长度确实大于100 分批获取
		beginIndex := 0
		endIndex := HGetAllCount
		for {
			if endIndex >= keyCount {
				endIndex = keyCount
			}
			itemKeys := keys[beginIndex:endIndex]
			itemValues, err := this.redisApi.Hmget(setName, itemKeys)
			if err != nil {
				this.infoLog.Printf("MultiHgetAllSlice hashName=%s beginIndex=%v endIndex=%v err=%s itemKeys=%v ",
					setName,
					beginIndex,
					endIndex,
					err,
					itemKeys)
				return nil, nil, err
			}
			outKeys = append(outKeys, itemKeys...)
			outValues = append(outValues, itemValues...)
			// beginIndex 往前移动HGetAllCount
			beginIndex += HGetAllCount
			endIndex += HGetAllCount
			if beginIndex >= keyCount {
				this.infoLog.Printf("MultiHgetAllSlice hashName=%s keyCount=%v beginIndex=%v complete",
					setName,
					keyCount,
					beginIndex)
				break
			}
		}
	}
	return outKeys, outValues, nil
}

func (this *SsdbOperator) MultiHset(setName string, kvs []string) (err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return err
	}
	err = this.redisApi.Hmset(setName, kvs)
	if err != nil {
		this.infoLog.Printf("MultiHset redisApi.Hmset hashName=%s err=%s", setName, err)
	}
	return err
}

func (this *SsdbOperator) GetUidByMid(mid string) (uid uint32, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return uid, err
	}
	if len(mid) == 0 {
		err = ErrDbParam
		return uid, err
	}
	value, err := this.redisApi.Hget(mid, MntContentKey)
	if err != nil {
		return uid, err
	}
	reqBody := new(ht_moment_store.StoreMomentBody)
	err = proto.Unmarshal([]byte(value), reqBody)
	if err != nil {
		this.infoLog.Printf("GetUidByMid proto Unmarshal failed mid=%s", mid)
		return uid, err
	}
	uid = reqBody.GetUserid()
	return uid, nil
}

func (this *SsdbOperator) Hset(setName string, key, value string) (err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return err
	}

	err = this.redisApi.Hset(setName, key, value)
	if err != nil {
		this.infoLog.Printf("Hset redisApi.Hset return err=%s setName=%s key=%v value=%v",
			err,
			setName,
			key,
			value)
	}
	return err
}

func (this *SsdbOperator) ReloadZset(setName string) (keyScore []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	start := int64(0)
	stop := int64(-1)
	keyScore, err = this.redisApi.Zrange(setName, start, stop)
	return keyScore, err
}
