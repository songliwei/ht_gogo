// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import "errors"

// Error type
var (
	ErrNotExistInReids = errors.New("not exist in redis")
	ErrNilDbObject     = errors.New("not set object current is nil")
	ErrDbParam         = errors.New("err param error")
	ErrSsdbResult      = errors.New("err ssdb return result err")
	ErrLegthErr        = errors.New("err length is not enouth")
)

const (
	MntForFollowerList  = "mnt_for_follower_list"
	MntForSelfList      = "mnt_for_self_list"
	MntCountForSelfKey  = "moment_count"
	MntCountForOtherKey = "moment_count_for_others"
	LearnLangPrefix     = "LLNANG#"
	LangPrefix          = "LANG#"
	MidIndexHashMap     = "MIDINDEX"
	AllListHashMap      = "ALL#LIST"
	MaxIndexKey         = "0"
	FriendSuffix        = "#FRIEND"
	UserSuffix          = "#LIST"
	MntContentKey       = "0"
	BucketMaxSeqKey     = "0"
	MntDeletedKey       = "deleted"
	MntOpReasonKey      = "op_reason"

	HideOtherMntKey  = "#hide_user_set"
	NotShareMyMntKey = "#not_share_set"
	VersionField     = "1"
	MomentCreater    = "moment_creater"
	UserInfoSuffix   = "_user_info"
	ReloadKey        = "reload"
)

const (
	ProcTimeThresold     = 300000
	BaseMntIndex         = 100000000000 // 100亿
	ChineseSimple        = 2
	ChineseTraditional   = 3
	ChineseGuangDoHua    = 4
	Japanse              = 5
	Korean               = 6
	MomentCountThreshold = 200000
	MidCountThreshold    = 3000
	HScanOperaterCount   = 1000
	FriendCountThreshold = 600
	HashMapExpireTime    = 7 * 24 * 3600
	MidToUidExpireTs     = 7 * 24 * 3600
	HashListExpireTime   = 1 * 24 * 3600
	MidContentExpireTime = 6 * 3600
	HGetAllCount         = 100
	ExpirePeriod         = 259200 //3 * 86400 = 3 day   units:second
)
