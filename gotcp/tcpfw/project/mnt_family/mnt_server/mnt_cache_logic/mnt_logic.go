package main

import (
	"database/sql"
	"fmt"
	"sort"
	"strings"
	"unicode/utf8"

	"github.com/HT_GOGO/gotcp"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/golang/protobuf/proto"
	nsq "github.com/nsqio/go-nsq"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/libgenmid"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_cache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_user"
	"github.com/HT_GOGO/gotcp/tcpfw/project/mnt_family/mnt_server/mnt_cache_logic/util"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog     *log.Logger
	db          *sql.DB
	proSyncReq  *util.ProcSyncReq
	dbdAgentApi *common.SrvToSrvApiV2
	// writeAgentMastreApi *common.SrvToSrvApiV2
	// writeAgentSlaveApi  *common.SrvToSrvApiV2
	redisMasterApi *common.RedisApi
	userCacheApi   *common.UserCacheApi
	relationApi    *common.RelationApi
	// cmntCacheApi        *common.CmntCacheApi
	gLearnDay        uint32
	gLearnLimit      uint32
	gShowSwitch      bool
	globalProducer   *nsq.Producer
	userActionTopic  string
	p2pAgentApi      *common.P2PWorkerApiV2
	modMntBucketChan chan []byte
	closeChan        chan struct{} // close chanel
)

const (
	BackEndDeletedUser = 4 //4-后台删除
	SelfDeletedUser    = 5 //5-用户删除自己
)

const (
	NSQ_EVENT_LIKE               = 1
	NSQ_EVENT_CANCLE_LIKE        = 2
	NSQ_EVENT_COMMENT            = 3
	NSQ_EVENT_DELETE_COMMENT     = 4
	NSQ_EVENT_REPORT             = 5
	NSQ_EVENT_FAVORITE           = 6
	NSQ_EVENT_SHARE              = 7
	NSQ_EVENT_BUCKET_TO_ME       = 8
	NSQ_EVENT_BUCKET_TO_FOLLOW   = 9
	NSQ_EVENT_BUCKET_TO_ALL      = 10
	NSQ_EVENT_OP_RESTORE         = 11
	NSQ_EVENT_OP_HIDE            = 12
	NSQ_EVENT_OP_DELETE          = 13
	NSQ_EVENT_OP_FOLLOWER_ONLY   = 14
	NSQ_EVENT_DELETE_USER        = 15
	NSQ_EVENT_RESTORE_USER       = 16
	NSQ_EVENT_DELETE_MNT_BY_USER = 17
	NSQ_EVENT_CANCLE_FAVORITE    = 18
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed err=%s", err)
		return false
	}

	// infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		return false
	}
	// 统计总的请求量
	attr := "gomntlogic/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_moment.CMD_TYPE_CMD_POST_MOMENT):
		go ProcPostMntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_GET_MID_CONTENT):
		go ProcGetMntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_DEL_MOMENT):
		go ProcDelMntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_DOWN):
		go ProcLastMntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_UP):
		go ProcHisMntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_DELETE_USER):
		go ProcDelUserService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_RESTORE_USER):
		go ProcRestoreUserService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_STATUS):
		go ProcModMntStatusService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_POST_AD_MOMENT):
		go ProcPostAdMntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_OPERATOR_UID):
		go ProcOperatorUidService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_GET_OPERATOR_UID_LIST):
		go ProcGetUidListService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_BACKEND_OP_UID):
		go ProcBackEndOpUidService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_QUERY_BEBLOCKED):
		go ProcQueryBeBlockedService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_QUERY_REVEAL_ENTRY):
		go ProcQueryRevealEntryService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_OP_MID_BUCKET):
		go ProcModifyShowFieldService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INDEX_MOMENT):
		go ProcQueryIndexMntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_BUCKET):
		go ProcModMntBucketService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_POST_COMMENT):
		go ProcPostComntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_DEL_COMMENT):
		go ProcDelComntService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_SET_COMMENT_STAT):
		go ProcSetComntStatService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_POST_LIKE):
		go ProcPostLikeService(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INFO):
		go ProcGetUserInfo(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

// 1.proc store moment info
func ProcPostMntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcPostMntService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcPostMntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/post_mnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/post_mnt_count"
	libcomm.AttrAdd(attr, 1)

	// Step1 检查用户是否已被删除
	ret, err := IsDeletedUser(head.Uid)
	if err != nil || ret == true {
		infoLog.Printf("ProcPostMntService IsDeletedUser err=%s ret=%v", err, ret)
		// add static
		attr := "gomntlogic/post_mnt_no_auth_count"
		libcomm.AttrAdd(attr, 1)

		result = uint16(ht_moment.RET_CODE_RET_NO_AUTH)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("no auth user"),
			},
		}
		return false
	}

	reqBody := new(ht_moment.ReqBody)
	err = proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcPostMntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetPostMntReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcPostMntService GetPostMntReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// Step2: 产生mid
	mid := libgenmid.GenMid()
	infoLog.Printf("ProcPostMntService uid=%v mid=%v", head.Uid, mid)
	midStr := fmt.Sprintf("%v", mid)
	subReqBody.Moment.Mid = []byte(midStr)

	// Step3: 产生key-value 并更新dbd
	storeBody, err := util.MntReqBodyToStoreBody(subReqBody.GetMoment())
	if err != nil {
		infoLog.Printf("ProcPostMntService MntReqBodyToStoreBody err=%s uid=%v cmd=0x%4x seq=%v", err, head.Uid, head.Cmd, head.Seq)
		// add static
		attr := "gomntlogic/mnt_to_store_body_failed"
		libcomm.AttrAdd(attr, 1)

		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change to store body failed"),
			},
		}
		return false
	}
	pbsBuf, err := proto.Marshal(storeBody)
	if err != nil {
		infoLog.Printf("ProcPostMntService proto.Marshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	var keyValue []string
	// add content
	keyValue = append(keyValue, util.MntContentKey)
	keyValue = append(keyValue, string(pbsBuf))

	// add deleted and operator reason
	mapMntForFollwerList, err := proSyncReq.GetMntForFollowerList(head)
	if err != nil {
		infoLog.Printf("ProcPostMntService proSyncReq.GetMntForFollowerList failed err=%s uid=%v", err, head.Uid)
		mapMntForFollwerList = make(map[uint32]uint32)
		mapMntForFollwerList[0] = 0
	}
	mapMntForSelfList, err := proSyncReq.GetMntForSelfList(head)
	if err != nil {
		infoLog.Printf("ProcPostMntService proSyncReq.GetMntForSelfList failed err=%s uid=%v", err, head.Uid)
		mapMntForSelfList = make(map[uint32]uint32)
		mapMntForSelfList[0] = 0
	}

	if _, ok := mapMntForFollwerList[head.Uid]; ok { // 帖子只给粉丝看
		// add delete filed
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY))

		// add operator reason
		keyValue = append(keyValue, util.MntOpReasonKey)
		keyValue = append(keyValue, "back end op")
		// infoLog.Printf("ProcPostMntService uid=%v exist in ForFollowerList", head.Uid)
	} else if _, ok := mapMntForSelfList[head.Uid]; ok { // 帖子指给自己看
		// add delete filed
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_HIDE))

		// add operator reason
		keyValue = append(keyValue, util.MntOpReasonKey)
		keyValue = append(keyValue, "back end op")
		// infoLog.Printf("ProcPostMntService uid=%v exist in ForSelfList", head.Uid)
	} else {
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_RESTORE))

		// add operator reason
		keyValue = append(keyValue, util.MntOpReasonKey)
		keyValue = append(keyValue, "")
		// infoLog.Printf("ProcPostMntService XXXXX keyValue=%v", keyValue)
	}

	// add liked count filed
	keyValue = append(keyValue, util.LikedCountKey)
	keyValue = append(keyValue, "0")

	// add liked ts filed
	keyValue = append(keyValue, util.LikedTsKey)
	keyValue = append(keyValue, "0")

	// add comment count filed
	keyValue = append(keyValue, util.CommentCountKey)
	keyValue = append(keyValue, "0")

	// add comment ts filed
	keyValue = append(keyValue, util.CommentTsKey)
	keyValue = append(keyValue, "0")

	// add total comment count filed
	keyValue = append(keyValue, util.TotalCmntCountKey)
	keyValue = append(keyValue, "0")

	// add show filed
	keyValue = append(keyValue, util.ShowFieldKey)
	keyValue = append(keyValue, "0")

	// add fetured level 0 表示非精华，1-10 描述的是精华的等级
	keyValue = append(keyValue, util.FeaturedLevel)
	keyValue = append(keyValue, "0")

	// 将创建者插入到moment的详细内容中
	keyValue = append(keyValue, util.MomentCreater)
	keyValue = append(keyValue, fmt.Sprintf("%v", head.Uid))

	dbRet, err := UpdateMidHashMapInDb(head, midStr, keyValue)
	if err != nil || dbRet != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("ProcPostMntService uid=%v mid=%s err=%s ret=%v", head.Uid, midStr, err, dbRet)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// step4 :answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("post mnt success"),
		},
		Mid:      []byte(midStr),
		PostTime: proto.Uint64(uint64(time.Now().UnixNano() / 1000000)),
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// step5:发送请求到dbd 做其他业务逻辑更新操作
	dbdRet, err := PostMidToDbd(head, subReqBody, mid)
	if err != nil {
		infoLog.Printf("ProcPostMntService uid=%v PostMidToDbd ret err=%s", head.Uid, err)
	}
	infoLog.Printf("ProcPostMntService uid=%v PostMidToDbd ret=%v", head.Uid, dbdRet)
	if dbdRet != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		// add static
		attr := "gomntlogic/post_mid_to_dbd_failed"
		libcomm.AttrAdd(attr, 1)
	}

	// step6:更新完成之后删除用户的comment缓存贴子数、评论数
	// 异步发送不需要回包
	// if cmntCacheApi != nil && head.Uid != 0 {
	// 	err = cmntCacheApi.ClearUserInfoCache(head.Uid)
	// 	if err != nil {
	// 		infoLog.Printf("ProcPostMntService cmntCacheApi.ClearUserInfoCache uid=%v failed err=%s", head.Uid, err)
	// 		// add static
	// 		attr := "gomntlogic/clear_user_info_cache_failed"
	// 		libcomm.AttrAdd(attr, 1)
	// 	}
	// }
	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcPostMntService uid=%v mid=%v totalProcTime=%v", head.Uid, mid, totalProcTime)
	if totalProcTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_mnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func IsDeletedUser(uid uint32) (ret bool, err error) {
	ret = false
	userType, err := GetUserTypeInDb(uid)
	if err != nil {
		infoLog.Printf("IsDeletedUser db return err=%s", err)
		return ret, err
	}
	if userType == uint32(BackEndDeletedUser) || userType == uint32(SelfDeletedUser) {
		ret = true
	}
	return ret, nil
}

func GetUserTypeInDb(uid uint32) (userType uint32, err error) {
	if uid == 0 {
		infoLog.Printf("GetUserTypeInDb invalid param")
		err = util.ErrInvalidParam
		return userType, err
	}
	if db == nil {
		return userType, util.ErrNilDbObject
	}

	var storeUserType sql.NullInt64
	err = db.QueryRow("SELECT USERTYPE FROM HT_USER_ACCOUNT WHERE USERID = ?", uid).Scan(&storeUserType)
	switch {
	case err == sql.ErrNoRows:
		infoLog.Printf("GetUserTypeInDb not found uid=%v in HT_USER_ACCOUNT err=%s", uid, err)
		return userType, err
	case err != nil:
		infoLog.Printf("GetUserTypeInDb exec HT_USER_ACCOUNT failed uid=%v, err=%s", uid, err)
		return userType, err
	default:
	}

	if storeUserType.Valid {
		userType = uint32(storeUserType.Int64)
	}

	return userType, nil
}

func UpdateMidHashMapInDb(head *common.HeadV2, hashName string, setKeyValue []string) (ret uint16, err error) {
	if len(hashName) == 0 || len(setKeyValue) == 0 {
		err = util.ErrInvalidParam
		return ret, err
	}
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_UPDATE_MID_HASH_MAP)
	// infoLog.Printf("UpdateMidHashMapInDb uid=%v hashMap=%s setKeyValue=%v", reqHead.Uid, hashName, setKeyValue)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		UpdateMidHashMapReqbody: &ht_moment_cache.UpdateMidHashMapReqBody{
			HashName: proto.String(hashName),
			KeyValue: setKeyValue,
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("UpdateMidHashMapInDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return ret, err
	}
	dbdPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("UpdateMidHashMapInDb dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return ret, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("UpdateMidHashMapInDb change packet failed uid=%v", reqHead.Uid)
		err = util.ErrChagePacket
		return ret, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		infoLog.Printf("UpdateMidHashMapInDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func GenerateSetMidToUidReqBody(midStr string, uid uint32) (reqPayLoad []byte, err error) {
	reqBody := &ht_moment_cache.MntCacheReqBody{
		SetKeyValueReqbody: &ht_moment_cache.SetKeyValueReqBody{
			Key:      proto.String(midStr),
			Value:    proto.String(fmt.Sprintf("%v", uid)),
			ExpireTs: proto.Uint32(uint32(util.MidToUidExpireTs)),
		},
	}
	reqPayLoad, err = proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("proto.Marshal failed uid=%v err=%s", uid, err)
	}
	return reqPayLoad, err
}

func Utf8StrLen(targetStr string) (count int) {
	count = utf8.RuneCountInString(targetStr)
	return count
}

func PostMidToDbd(head *common.HeadV2, subReqBody *ht_moment.PostMomentReqBody, mid uint64) (ret uint16, err error) {
	if head == nil || subReqBody == nil {
		err = util.ErrInvalidParam
		return ret, err
	}
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_POST_MID_REQ)
	// infoLog.Printf("PostMidToDbd reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		PostMidReqbody: &ht_moment_cache.PostMidReqBody{
			Version:        proto.Uint32(subReqBody.GetVersion()),
			Mid:            proto.Uint64(mid),
			ContentByteLen: proto.Uint32(uint32(len(subReqBody.GetMoment().GetContent()))),
			ContentWords:   proto.Uint32(uint32(Utf8StrLen(string(subReqBody.GetMoment().GetContent())))),
			StrLangType:    subReqBody.GetMoment().GetStrLangType(),
			OnlyToMe:       proto.Uint32(subReqBody.GetOnlyToMe()),
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("PostMidToDbd proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return ret, err
	}
	dbdPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("PostMidToDbd dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return ret, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("PostMidToDbd change packet failed uid=%v", reqHead.Uid)
		err = util.ErrChagePacket
		return ret, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		infoLog.Printf("PostMidToDbd GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

// 2.proc Get Moment Service
func ProcGetMntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcPostMntService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ViewMntRspbody = &ht_moment.ViewMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetMntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/get_mnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/get_mnt_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetMntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ViewMntRspbody = &ht_moment.ViewMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetViewMntReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetMntService GetViewMntReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ViewMntRspbody = &ht_moment.ViewMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetUserid()
	midSlice := subReqBody.GetMid()
	areaCode := subReqBody.GetAreaCode()
	infoLog.Printf("ProcGetMntService reqUid=%v mid size=%v areaCode=%s", reqUid, len(midSlice), areaCode)
	keys := []string{
		util.MntContentKey,
		util.LikedCountKey,
		util.LikedTsKey,
		util.CommentCountKey,
		util.CommentTsKey,
		util.MntDeletedKey,
		util.TotalCmntCountKey,
	}
	var momentList []*ht_moment.MomentBlockInfo
	for i, v := range midSlice {
		midStr := string(v)
		err = redisMasterApi.Expire(midStr, util.MidContentExpireTime) //如果是设置整个mid设置过期时间为3天
		if err != nil {
			// add static
			attr := "gomntlogic/expire_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcGetMntService redisMasterApi.Expire hashName=%s err=%s failed", midStr, err)
		}
		// infoLog.Printf("ProcGetMntService index=%v mid=%s", i, midStr)
		// step1: get moment content mid->uid的映射保存在moment的
		values, err := proSyncReq.GetSpecificKeyInMidHashMap(head, midStr, keys)
		if err != nil {
			// add static
			attr := "gomntlogic/get_mnt_count_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcGetMntService proSyncReq.GetSpecificKeyInMidHashMap hashmap=%s keys=%v err=%s", midStr, keys, err)
			continue
		}

		// step2: get uid by mid
		var createrUid uint32
		strUid, err := redisMasterApi.Hget(midStr, util.MomentCreater)
		if err != nil {
			infoLog.Printf("ProcGetMntService get mid=%s post createrUid fialed err=%s", midStr, err)
			createrUid = 0 // get failed set createrUid 0
		} else {
			tempUid, err := strconv.ParseUint(strUid, 10, 32)
			if err != nil {
				infoLog.Printf("ProcGetMntService hashName=%s strconv.ParseUint faield strUid=%s err=%s", midStr, strUid, err)
				createrUid = 0
			} else {
				// infoLog.Printf("ProcGetMntService hashName=%s moment creater createrUid=%v", midStr, tempUid)
			}
			createrUid = uint32(tempUid)
		}
		var commentCount, likedCount, deleted, likeByMe, totalCommentCount uint32
		var likedTs, commentTs uint64
		var mntStore string
		if len(values) == len(keys) {
			mntStore = values[0]
			tempLikedCount, err := strconv.ParseUint(values[1], 10, 32)
			if err != nil {
				infoLog.Printf("ProcGetMntService strconv.ParseUint value=%s, err=%s", values[1], err)
				likedCount = 0
			} else {
				likedCount = uint32(tempLikedCount)
			}
			likedTs, err = strconv.ParseUint(values[2], 10, 64)
			if err != nil {
				infoLog.Printf("ProcGetMntService strconv.ParseUint value=%s, err=%s", values[2], err)
				likedTs = 0
			}
			tempCommentCount, err := strconv.ParseUint(values[3], 10, 32)
			if err != nil {
				infoLog.Printf("ProcGetMntService strconv.ParseUint value=%s, err=%s", values[3], err)
				commentCount = 0
			} else {
				commentCount = uint32(tempCommentCount)
			}
			commentTs, err = strconv.ParseUint(values[4], 10, 64)
			if err != nil {
				infoLog.Printf("ProcGetMntService strconv.ParseUint value=%s, err=%s", values[4], err)
				commentTs = 0
			}
			tempDeleted, err := strconv.ParseUint(values[5], 10, 32)
			if err != nil {
				infoLog.Printf("ProcGetMntService strconv.ParseUint value=%s, err=%s", values[5], err)
				deleted = 0
			} else {
				deleted = uint32(tempDeleted)
			}
			tempTotalCommentCount, err := strconv.ParseUint(values[6], 10, 32)
			if err != nil {
				infoLog.Printf("ProcGetMntService strconv.ParseUint value=%s, err=%s", values[6], err)
				totalCommentCount = 0
			} else {
				totalCommentCount = uint32(tempTotalCommentCount)
			}
		} else {
			mntStore = ""
			likedCount = 0
			likedTs = 0
			commentCount = 0
			commentTs = 0
			deleted = 0
			totalCommentCount = 0
		}

		// parse pb
		storeBody := new(ht_moment_store.StoreMomentBody)
		err = proto.Unmarshal([]byte(mntStore), storeBody)
		if err != nil {
			// add static
			attr := "gomntlogic/get_req_mnt_content_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcGetMntService storeMoment Unmarshal failed uid=%v cmd=0x%4x seq=%v err=%s", head.Uid, head.Cmd, head.Seq, err)
			continue
		}
		pbReqBody, err := util.MntStoreBodyToReqBody(storeBody)
		if err != nil {
			// add static
			attr := "gomntlogic/mnt_store_to_req_body_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcGetMntService util.MntStoreBodyToReqBody failed midStr=%s uid=%v err=%s", midStr, head.Uid, err)
			continue
		}

		//添加改错的CID
		var commentList []*ht_moment.BaseComment
		correctCidKey := midStr + util.CorrectKey
		kvs, err := proSyncReq.ZrangeList(head, correctCidKey, 0, util.LastCmntCount, 1) // 1:zrrange 0:zrange
		if err != nil {
			infoLog.Printf("ProcGetMntService proSyncReq.ZrangeList failed uid=%v zsetName=%v offset=%v limit=%v err=%s",
				head.Uid,
				correctCidKey,
				0,
				util.LastCmntCount,
				err)
			// not need return
		} else {
			var cmntKeys []string
			if len(kvs) > 0 {
				for index := len(kvs) - 2; index >= 0; index -= 2 {
					intKey, err := strconv.ParseUint(kvs[index], 10, 64)
					if err != nil {
						infoLog.Printf("ProcGetMntService strconv.ParseUint failed key=%s err=%s", kvs[index], err)
						continue
					}
					intKey += util.BaseCommentIndex
					strKey := fmt.Sprintf("%v", intKey)
					cmntKeys = append(cmntKeys, strKey)
				}
			} else {
				infoLog.Printf("ProcGetMntService correct zlist empty uid=%v mid=%s", head.Uid, midStr)
			}

			// infoLog.Printf("ProcGetMntService cmntKeyLen=%v cmntKeys=%v", len(cmntKeys), cmntKeys)
			for index := util.LastCmntCount; index >= 0; index -= 1 {
				if totalCommentCount > uint32(index) {
					itermKey := fmt.Sprintf("%v", (util.BaseCommentIndex + totalCommentCount - uint32(index)))
					// infoLog.Printf("ProcGetMntService uid=%v strKey=%s", head.Uid, itermKey)
					ret := IsStringExistInSlice(cmntKeys, itermKey)
					if ret == false {
						cmntKeys = append(cmntKeys, itermKey)
					}
				}
			}
			// infoLog.Printf("ProcGetMntService after append cmntKeyLen=%v cmntKeys=%v", len(cmntKeys), cmntKeys)
			if len(cmntKeys) > 0 {
				cmntValue, err := proSyncReq.GetSpecificKeyInMidHashMap(head, midStr, cmntKeys)
				if err != nil || (len(cmntKeys) != len(cmntValue)) {
					infoLog.Printf("ProcGetMntService get comment value failed uid=%v mid=%s err=%s", head.Uid, midStr, err)
					// add static
					attr := "gomntlogic/get_mnt_count_failed"
					libcomm.AttrAdd(attr, 1)
					continue
				}
				correctMap := map[string]*ht_moment_store.StoreCommentBody{}
				var correctSlice []string
				cmmMap := map[string]*ht_moment_store.StoreCommentBody{}
				var cmmSlice []string
				for i, v := range cmntValue {
					storeCmntBody := new(ht_moment_store.StoreCommentBody)
					err := proto.Unmarshal([]byte(v), storeCmntBody)
					if err != nil {
						// add static
						attr := "gomntlogic/cmnt_unmarshal_failed"
						libcomm.AttrAdd(attr, 1)
						infoLog.Printf("ProcGetMntService storeComment Unmarshal failed uid=%v cmd=0x%4x seq=%v err=%s", head.Uid, head.Cmd, head.Seq, err)
						continue
					}
					// 如果是帖文已经被删除 或(请求用户不是评论用户自身且帖文被隐藏了) continue
					if storeCmntBody.GetDeleted() == 1 ||
						(storeCmntBody.GetDeleted() == 2 && storeCmntBody.GetUserid() != reqUid) {
						infoLog.Printf("ProcGetMntService index=%v midStr=%s  deleted=%v storeUid=%v uid=%v cmnt delete",
							i,
							midStr,
							storeCmntBody.GetDeleted(),
							storeCmntBody.GetUserid(),
							reqUid)
						continue
					}
					// 如果请求用户是评论用户自己且评论状态为隐藏时修复评论的状态为0
					if storeCmntBody.GetDeleted() == 2 && storeCmntBody.GetUserid() == reqUid {
						storeCmntBody.Deleted = proto.Uint32(0) // 将帖子的状态恢复为0
						commentCount += 1
						infoLog.Printf("ProcGetMntService index=%v midStr=%s  deleted=%v storeUid=%v reqUid=%v fix cmntCount=%v",
							i,
							midStr,
							storeCmntBody.GetDeleted(),
							storeCmntBody.GetUserid(),
							reqUid,
							commentCount)
					}
					if storeCmntBody.GetCtype() == ht_moment_store.STORE_COMMENT_TYPE_CORRECT {
						correctMap[cmntKeys[i]] = storeCmntBody
						correctSlice = append(correctSlice, cmntKeys[i])
						if len(correctSlice) > util.CmntCountThreshold {
							// infoLog.Printf("ProcGetMntService delete correct key=%s", correctSlice[0])
							delete(correctMap, correctSlice[0])
							correctSlice = correctSlice[1:]
						}
					} else if storeCmntBody.GetCtype() == ht_moment_store.STORE_COMMENT_TYPE_TEXT ||
						storeCmntBody.GetCtype() == ht_moment_store.STORE_COMMENT_TYPE_WITH_VOICE {
						cmmMap[cmntKeys[i]] = storeCmntBody
						cmmSlice = append(cmmSlice, cmntKeys[i])
						if len(cmmSlice) > util.CmntCountThreshold {
							// infoLog.Printf("ProcGetMntService delete cmm key=%s", cmmSlice[0])
							delete(cmmMap, cmmSlice[0])
							cmmSlice = cmmSlice[1:]
						}
					}
				}
				var correctKey []string
				for i, _ := range correctMap {
					correctKey = append(correctKey, i)
				}
				sort.Strings(correctKey)
				for _, v := range correctKey {
					// infoLog.Printf("ProcGetMntService correctKey=%s", v)
					pbCmntBody, err := util.CmtStoreBodyToReqBody(correctMap[v])
					if err != nil {
						infoLog.Printf("ProcGetMntService util.CmtStoreBodyToReqBody failed uid=%v mid=%v index=%v err=%s",
							head.Uid,
							midStr,
							i,
							err)
						continue
					}
					baseCommentItem := &ht_moment.BaseComment{
						CommentBody: pbCmntBody,
					}

					commentList = append(commentList, baseCommentItem)
				}
				// infoLog.Printf("ProcGetMntService correct_map size=%v", len(commentList))
				switch len(commentList) {
				case 0:
					infoLog.Printf("ProcGetMntService correct_map empty mid=%s", midStr)
				case 1:
					if len(cmmMap) == util.CmntCountThreshold {
						delete(cmmMap, cmmSlice[0])
						cmmSlice = cmmSlice[1:]
					}
				case 2:
					cmmLen := len(cmmMap)
					if cmmLen > 1 {
						// delete two element
						delete(cmmMap, cmmSlice[0])
						delete(cmmMap, cmmSlice[1])
						if cmmLen > 2 {
							cmmSlice = cmmSlice[2:]
						} else {
							cmmSlice = nil
						}
					}
				case 3:
					for _, v := range cmmSlice {
						delete(cmmMap, v)
					}
					cmmSlice = nil
				default:
					infoLog.Printf("ProcGetMntService bug never come here uid=%v mid=%s", head.Uid, midStr)
				}
				var cmmKey []string
				for i, _ := range cmmMap {
					cmmKey = append(cmmKey, i)
				}
				sort.Strings(cmmKey)
				for _, v := range cmmKey {
					// infoLog.Printf("ProcGetMntService cmmkey value=%s", v)
					pbCmntBody, err := util.CmtStoreBodyToReqBody(cmmMap[v])
					if err != nil {
						infoLog.Printf("ProcGetMntService util.CmtStoreBodyToReqBody failed uid=%v mid=%v index=%v err=%s",
							head.Uid,
							midStr,
							i,
							err)
						continue
					}
					baseCommentItem := &ht_moment.BaseComment{
						CommentBody: pbCmntBody,
					}
					commentList = append(commentList, baseCommentItem)
				}
			}
		}

		// 请求人是自己则执行修复
		if createrUid == head.Uid { // 自己发布的帖子
			if (deleted == uint32(ht_moment.MntStatusOpType_OP_HIDE)) ||
				(deleted == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY)) {
				deleted = uint32(ht_moment.MntStatusOpType_OP_RESTORE)
			}
		} else { // 其他人
			if deleted == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
				deleted = uint32(ht_moment.MntStatusOpType_OP_RESTORE)
			}
		}
		// is like by me
		bLikeByMe := IsLikedByMe(head, midStr, head.Uid)
		if bLikeByMe {
			likeByMe = 1
		} else {
			likeByMe = 0
		}

		momentIterm := &ht_moment.MomentBlockInfo{
			Moment:       pbReqBody,
			CommentList:  commentList,
			Deleted:      proto.Uint32(deleted),
			LikedByMe:    proto.Uint32(likeByMe),
			LikedCount:   proto.Uint64(uint64(likedCount)),
			LikedTs:      proto.Uint64(likedTs),
			CommentCount: proto.Uint64(uint64(commentCount)),
			CommentTs:    proto.Uint64(commentTs),
		}
		momentList = append(momentList, momentIterm)
	}
	// step4 :answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.ViewMntRspbody = &ht_moment.ViewMomentRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("post mnt success"),
		},
		MomentList: momentList,
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_mnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}
func IsStringExistInSlice(strSlice []string, x string) (ret bool) {
	if strSlice == nil || len(strSlice) == 0 {
		ret = false
		return ret
	}
	ret = false
	for _, v := range strSlice {
		if v == x {
			ret = true
			break
		}
	}
	return ret
}
func IsLikedByMe(head *common.HeadV2, midStr string, uid uint32) (ret bool) {
	if midStr == "" || uid == 0 {
		ret = false
	}
	zsetLikeList := midStr + util.LikedKey
	strUid := fmt.Sprintf("%v", uid)
	score, err := proSyncReq.Zget(head, zsetLikeList, strUid)
	if err != nil || score == 0 {
		ret = false
	} else {
		ret = true
	}
	// infoLog.Printf("IsLikedByMe midStr=%s uid=%v ret=%v", midStr, uid, ret)
	return ret
}

// 3.proc delete mnt service
func ProcDelMntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcDelMntService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelMntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/del_mnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/del_mnt_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelMntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelMntReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelMntService GetDelMntReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uid := subReqBody.GetUserid()
	midStr := string(subReqBody.GetMid())
	// step1: check whether uid is equal post moment uid
	keys := []string{
		util.MntContentKey,
	}
	values, err := proSyncReq.GetSpecificKeyInMidHashMap(head, midStr, keys)
	if err != nil {
		// add static
		attr := "gomntlogic/del_mnt_get_content_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcDelMntService proSyncReq.GetSpecificKeyInMidHashMap hashmap=%s keys=%v err=%s", midStr, keys, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get mnt post user failed"),
			},
		}
		return false
	}
	if len(values) == 0 {
		infoLog.Printf("ProcDelMntService proSyncReq.GetSpecificKeyInMidHashMap hashmap=%s keys=%v err=%s", midStr, keys, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get mnt post user failed"),
			},
		}
		return false
	}

	storeMomentBody := new(ht_moment_store.StoreMomentBody)
	err = proto.Unmarshal([]byte(values[0]), storeMomentBody)
	if err != nil {
		infoLog.Printf("ProcDelMntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}

	if storeMomentBody.GetUserid() != uid {
		infoLog.Printf("ProcDelMntService req uid=%v not equal post moment uid=%v cmd=0x%4x seq=%v",
			uid,
			storeMomentBody.GetUserid(),
			head.Cmd,
			head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_NO_AUTH)
		rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("no auth userid"),
			},
		}
		return false
	}

	// step2: desc the count in db
	hashName, descItem, err := GetDescMomentCountItem(head, midStr, uid, -1)
	if err != nil {
		infoLog.Printf("ProcDelMntService DescMomentCount return err=%s uid=%v cmd=0x%4x seq=%v",
			err,
			uid,
			head.Cmd,
			head.Seq)
	} else {
		ret, err := PostDescMomentCountToDb(head, hashName, descItem)
		if err != nil {
			infoLog.Printf("ProcDelMntService PostDescMomentCountToDb failed uid=%v midStr=%s err=%v", uid, midStr, err)
		} else {
			infoLog.Printf("ProcDelMntService PostDescMomentCountToDb uid=%v midStr=%s ret=%v", uid, midStr, ret)
		}
	}

	// step3: change deleted field to 1
	keyValue := []string{
		util.MntDeletedKey,
		fmt.Sprintf("%d", int(ht_moment.MntStatusOpType_OP_DELETE_BY_USER)),
	}
	ret, err := UpdateMidHashMapInDb(head, midStr, keyValue)
	if err != nil || ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("ProcDelMntService UpdateMidHashMapInDb failed uid=%v midStr=%s err=%s ret=%v",
			uid,
			midStr,
			err,
			ret)
		result = uint16(ht_moment.RET_CODE_RET_SSDB_ERR)
		rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	}
	// step4 :answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.DelMntRspbody = &ht_moment.DeleteMomentRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("del mnt success"),
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/del_mnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}

	// clear base info cache
	// 异步发送不需要回包
	// if cmntCacheApi != nil && uid != 0 {
	// 	err = cmntCacheApi.ClearUserInfoCache(uid)
	// 	if err != nil {
	// 		infoLog.Printf("ProcDelMntService cmntCacheApi.ClearUserInfoCache uid=%v failed err=%s", uid, err)
	// 		// add static
	// 		attr := "gomntlogic/clear_user_info_cache_failed"
	// 		libcomm.AttrAdd(attr, 1)
	// 	}
	// }
	// 发布nsq事件到话题topic
	eventType := NSQ_EVENT_DELETE_MNT_BY_USER
	err = PublishPostEvent(uid, midStr, eventType)
	if err != nil {
		// add static
		attr := "gomntlogic/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcDelMntService PublishPostEvent uid=%v mid=%s eventType=%v failed",
			uid,
			midStr,
			eventType)
	}
	return true
}

func GetDescMomentCountItem(head *common.HeadV2, midStr string, uid uint32, desc int) (hashName string, descItem []*ht_moment_cache.HincIterm, err error) {
	if midStr == "" || midStr == "1" {
		return hashName, nil, util.ErrInvalidParam
	}
	keys := []string{
		util.LikedCountKey,
		util.MntDeletedKey,
	}
	values, err := proSyncReq.GetSpecificKeyInMidHashMap(head, midStr, keys)
	if err != nil {
		// add static
		attr := "gomntlogic/del_mnt_get_content_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("GetDescMomentCount proSyncReq.GetSpecificKeyInHashMap hashmap=%s keys=%v err=%s", midStr, keys, err)
		return hashName, nil, err
	}
	if len(values) != 2 {
		infoLog.Printf("GetDescMomentCount values len=%v not equal 2", len(values))
		return hashName, nil, err
	}
	tempLikedCount, err := strconv.ParseUint(values[0], 10, 32)
	if err != nil {
		infoLog.Printf("GetDescMomentCount strconv.ParseUint value=%s, err=%s", values[0], err)
		return hashName, nil, err
	}
	likedCount := uint32(tempLikedCount)

	tempDeleted, err := strconv.ParseUint(values[1], 10, 32)
	if err != nil {
		infoLog.Printf("GetDescMomentCount strconv.ParseUint value=%s, err=%s", values[1], err)
		return hashName, nil, err
	}
	deleted := uint32(tempDeleted)

	hashName = fmt.Sprintf("%v%s", uid, util.UserInfoSuffix)
	if deleted == uint32(ht_moment.MntStatusOpType_OP_HIDE) {
		item := &ht_moment_cache.HincIterm{
			ItermKey: proto.String(util.MomentCountKey),
			IncCount: proto.Int32(int32(desc)),
		}
		descItem = append(descItem, item)
		item = &ht_moment_cache.HincIterm{
			ItermKey: proto.String(util.MomentLikeCountKey),
			IncCount: proto.Int32(-int32(likedCount)),
		}
		descItem = append(descItem, item)
	} else if deleted == uint32(ht_moment.MntStatusOpType_OP_RESTORE) ||
		deleted == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
		item := &ht_moment_cache.HincIterm{
			ItermKey: proto.String(util.MomentCountKey),
			IncCount: proto.Int32(int32(desc)),
		}
		descItem = append(descItem, item)
		item = &ht_moment_cache.HincIterm{
			ItermKey: proto.String(util.MomentLikeCountKey),
			IncCount: proto.Int32(-int32(likedCount)),
		}
		descItem = append(descItem, item)
		item = &ht_moment_cache.HincIterm{
			ItermKey: proto.String(util.MomentCountForOther),
			IncCount: proto.Int32(int32(desc)),
		}
		descItem = append(descItem, item)
		item = &ht_moment_cache.HincIterm{
			ItermKey: proto.String(util.MomentLikeCountForOther),
			IncCount: proto.Int32(-int32(likedCount)),
		}
		descItem = append(descItem, item)
	}
	return hashName, descItem, nil
}

func PostDescMomentCountToDb(head *common.HeadV2, hashName string, descItem []*ht_moment_cache.HincIterm) (ret uint16, err error) {
	if len(hashName) == 0 || len(descItem) == 0 {
		err = util.ErrInvalidParam
		return ret, err
	}
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_HINC_HASH_MAP)
	// infoLog.Printf("PostDescMomentCountToDb reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		HincReqbody: &ht_moment_cache.HincReqBody{
			HashName:  proto.String(hashName),
			IncIterms: descItem,
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("PostDescMomentCountToDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return ret, err
	}
	dbdPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("PostDescMomentCountToDb dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return ret, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("PostDescMomentCountToDb change packet failed uid=%v", reqHead.Uid)
		err = util.ErrChagePacket
		return ret, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		infoLog.Printf("PostDescMomentCountToDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

// 4.proc last mnt service
func ProcLastMntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcLastMntService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcLastMntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/get_last_mnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/get_last_mnt_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcLastMntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetViewLatestMntIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcLastMntService GetViewLatestMntIdReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	uid := subReqBody.GetUserid()
	qtype := subReqBody.GetQtype()
	langType := subReqBody.GetLangType()
	infoLog.Printf("ProcLastMntService uid=%v qtype=%v langType=%v head.Uid=%v",
		uid,
		qtype,
		langType,
		head.Uid)
	var buckets []*util.BucketStr
	selfMidMap := make(map[string]bool)
	followerMidMap := make(map[string]bool)
	var idList []*ht_moment.MomentIdResult
	//needFilter := true
	var teachSli []uint32
	var learnSli []uint32
	// Step1: get all bucket like learn/teach-learn bucket
	// for user profile no need other process just send response
	switch qtype {
	case ht_moment.QUERY_TYPE_DEFAULT:
		// add static
		attr := "gomntlogic/get_last_default_mnt_count"
		libcomm.AttrAdd(attr, 1)
		// 如果langTyep 为0 代表取默认分组下面的所有语言的帖文包括
		// 自己的桶uid#LIST
		// 好友的桶FRIEND#LIST
		// 所有学习语言
		// 如果langType 不为0 代表取特定学习语言桶里面的帖文
		if langType == 0 {
			// add slef list
			selfHashName := fmt.Sprintf("%v#LIST", uid)
			selfBucket, err := proSyncReq.GetBucketList(head, selfHashName, util.DefaultMidPageSize)
			if err != nil {
				infoLog.Printf("ProcLastMntService proSyncReq.GetBucketList uid=%v hashName=%s failed err=%s", uid, selfHashName, err)
				attr := "gomntlogic/get_last_default_mnt_err"
				libcomm.AttrAdd(attr, 1)
			} else {
				// add bucket to list
				buckets = append(buckets, selfBucket)
				// add self mid list
				for i, _ := range selfBucket.MidMap {
					// infoLog.Printf("ProcLastMntService self bucket list index=%v midStr=%#v", i, *v)
					selfMidMap[i] = true
				}
			}
			// infoLog.Printf("ProcLastMntService hashName=%s seq=%v size=%v",
			// selfBucket.Name,
			// selfBucket.Seq,
			// selfBucket.Size)
			// add follower list
			followerHashName := fmt.Sprintf("%v#FRIEND", uid)
			followerBucket, err := proSyncReq.GetBucketList(head, followerHashName, util.DefaultMidPageSize*2)
			if err != nil {
				infoLog.Printf("ProcLastMntService proSyncReq.GetBucketList uid=%v hashName=%s failed err=%s", uid, followerHashName, err)
				attr := "gomntlogic/get_last_default_mnt_err"
				libcomm.AttrAdd(attr, 1)
			} else {
				// add bucket to list
				buckets = append(buckets, followerBucket)
				// add self mid list
				for i, _ := range followerBucket.MidMap {
					// infoLog.Printf("ProcLastMntService follower bucket list index=%v midStr=%#v", i, *v)
					followerMidMap[i] = true
				}
			}
			// infoLog.Printf("ProcLastMntService hashName=%s seq=%v size=%v",
			// followerBucket.Name,
			// followerBucket.Seq,
			// followerBucket.Size)
		}
		// 查询user cache 缓存
		_, nativeLang, teachLang, learnLang, err := userCacheApi.QueryLangInfo(uid)
		if err != nil {
			infoLog.Printf("ProcLastMntService userCacheApi.QueryTeachAndLearnLang failed uid=%v err=%s", uid, err)
			// add static
			attr := "gomntlogic/get_teach_and_learn_lang_faild"
			libcomm.AttrAdd(attr, 1)
		} else {
			// infoLog.Printf("nativeLang=%v teachLang=%v learnLang=%v", nativeLang, teachLang, learnLang)
			teachSet := common.NewSet()
			learnSet := common.NewSet()
			teachSet.Add(nativeLang)
			if langType == 0 {
				for _, v := range teachLang {
					if v != 0 {
						teachSet.Add(v)
					}
				}

				for _, v := range learnLang {
					if v != 0 {
						learnSet.Add(v)
					}
				}
			} else if Uint32IsInSlice(learnLang, langType) {
				for _, v := range teachLang {
					if v != 0 {
						teachSet.Add(v)
					}
				}
				learnSet.Add(langType)
			} else {
				attr := "gomntlogic/lang_is_not_in_learn"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcLastMntService uid=%v lang_type=%v learnLang=%v drop", uid, langType, learnLang)
			}
			// 取出teachSli 和 learnSli
			teachSli = teachSet.List()
			learnSli = learnSet.List()
			var learnAndTeachHashSli []string
			for _, t := range teachSli {
				for _, l := range learnSli {
					item := fmt.Sprintf("LANG#%v-%v", l, t)
					learnAndTeachHashSli = append(learnAndTeachHashSli, item)
				}
			}
			infoLog.Printf("ProcLastMntService uid=%v learnSli=%v teachSli=%v learnAndTeachSli=%v",
				uid,
				learnSli,
				teachSli,
				learnAndTeachHashSli)

			if len(learnSli) == 0 || len(teachSli) == 0 {
				attr := "gomntlogic/get_teach_and_learn_lang_faild"
				libcomm.AttrAdd(attr, 1)
			}
			for _, v := range learnAndTeachHashSli {
				langBucket, err := proSyncReq.GetBucketList(head, v, util.DefaultMidPageSize*2)
				if err != nil {
					infoLog.Printf("ProcLastMntService proSyncReq.GetBucketList uid=%v hashName=%s failed err=%s", uid, v, err)
					attr := "gomntlogic/get_last_default_mnt_err"
					libcomm.AttrAdd(attr, 1)
				} else {
					// add bucket to list
					buckets = append(buckets, langBucket)
					// for i, v := range langBucket.MidMap {
					// 	infoLog.Printf("ProcLastMntService leanr-teach bucket list index=%v midStr=%#v", i, *v)
					// }
				}
			}
		}
	case ht_moment.QUERY_TYPE_PARTNERS:
		// add static
		attr := "gomntlogic/get_last_part_mnt_count"
		libcomm.AttrAdd(attr, 1)
		// add follower list
		followerHashName := fmt.Sprintf("%v#FRIEND", uid)
		followerBucket, err := proSyncReq.GetBucketList(head, followerHashName, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcGetMntService proSyncReq.GetBucketList uid=%v hashName=%s failed err=%s", uid, followerHashName, err)
			attr := "gomntlogic/get_last_default_mnt_err"
			libcomm.AttrAdd(attr, 1)
		} else {
			// add bucket to list
			buckets = append(buckets, followerBucket)
			// add self mid list
			for i, _ := range followerBucket.MidMap {
				// infoLog.Printf("ProcGetMntService follower bucket list index=%v midStr=%#v", i, *v)
				followerMidMap[i] = true
			}
		}
	case ht_moment.QUERY_TYPE_USER:
		// add static
		attr := "gomntlogic/get_last_user_mnt_count"
		libcomm.AttrAdd(attr, 1)
		// add follower list
		userHashName := fmt.Sprintf("%v#LIST", uid)
		_, mapForbitList, err := proSyncReq.GetNotShareToUidList(head, uid, 0)
		if err == nil {
			if _, ok := mapForbitList[head.Uid]; ok {
				// infoLog.Printf("ProcLastMntService uid=%v forbit reqUid=%v", uid, head.Uid)
				result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
				rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
					Status: &ht_moment.Header{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("success"),
					},
					Bucket: &ht_moment.BucketInfo{
						BucketList: []*ht_moment.BucketBody{
							&ht_moment.BucketBody{
								BucketName: []byte(userHashName),
								Index:      proto.Uint64(0),
							},
						},
					},
				}
				return true
			}
		} else {
			infoLog.Printf("ProcLastMntService proSyncReq.GetNotShareToUidList uid=%v err=%s", uid, err)
		}

		userBucket, lastIndex, err := proSyncReq.GetBucketListAndIndex(head, userHashName, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcLastMntService proSyncReq.GetBucketListAndIndex uid=%v hashName=%s failed err=%s", uid, userHashName, err)
			attr := "gomntlogic/get_last_default_mnt_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		} else {
			// 遍历所有的mid列表 返回响应
			// 首先逆序排好序 再返回给客户端
			var mids []string
			for k, _ := range userBucket.MidMap {
				if k == "1" {
					continue
				}
				mids = append(mids, k)
			}
			sort.Sort(sort.Reverse(sort.StringSlice(mids)))
			for _, v := range mids {
				// infoLog.Printf("ProcLastMntService user bucket index=%v v=%s", i, v)
				if v == "1" {
					continue
				}
				midResult, err := proSyncReq.GetMidInfo(head, v)
				if err != nil {
					// add static
					attr := "gomntlogic/get_last_user_mid_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ProcLastMntService proSyncReq.GetMidInfo mid=%v err=%s", v, err)
					continue
				}
				if head.Uid == uid { // 拉取自己profile取自己发布所有帖子
					if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_HIDE) ||
						midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
						midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
					}
				} else { // 查看其他人的profile 取他人发布的所有帖子
					if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
						midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
					}
				}
				idList = append(idList, midResult)
			}
			// infoLog.Printf("ProcLastMntService hashName=%s IdList=%v", userHashName, idList)
			result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
			rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("success"),
				},
				Bucket: &ht_moment.BucketInfo{
					BucketList: []*ht_moment.BucketBody{
						&ht_moment.BucketBody{
							BucketName: []byte(userHashName),
							Index:      proto.Uint64(lastIndex),
						},
					},
				},
				IdList: idList,
			}
			return true
		}
	case ht_moment.QUERY_TYPE_CLASSMATE:
		// add static
		attr := "gomntlogic/get_last_classmate_mnt_count"
		libcomm.AttrAdd(attr, 1)
		// 查询user cache 缓存
		_, nativeLang, teachLang, learnLang, err := userCacheApi.QueryLangInfo(head.Uid)
		if err != nil {
			infoLog.Printf("ProcLastMntService userCacheApi.QueryTeachAndLearnLang failed uid=%v err=%s", head.Uid, err)
			// add static
			attr := "gomntlogic/get_teach_and_learn_lang_faild"
			libcomm.AttrAdd(attr, 1)
		} else {
			teachSet := common.NewSet()
			learnSet := common.NewSet()
			teachSet.Add(nativeLang)
			if langType == 0 {
				for _, v := range teachLang {
					if v != 0 {
						teachSet.Add(v)
					}
				}

				for _, v := range learnLang {
					if v != 0 {
						learnSet.Add(v)
					}
				}
			} else if Uint32IsInSlice(learnLang, langType) {
				for _, v := range teachLang {
					if v != 0 {
						teachSet.Add(v)
					}
				}
				learnSet.Add(langType)
			} else {
				infoLog.Printf("ProcLastMntService uid=%v classmate lang_type=%v learnLang=%v drop", uid, langType, learnLang)
				result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
				rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
					Status: &ht_moment.Header{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("input lang type error"),
					},
				}
				return false
			}
			// 取出teachSli 和 learnSli
			teachSli = teachSet.List()
			learnSli = learnSet.List()
			var learnAndTeachHashSli []string
			for _, t := range teachSli {
				for _, l := range learnSli {
					item := fmt.Sprintf("LANG#%v-%v", t, l)
					learnAndTeachHashSli = append(learnAndTeachHashSli, item)
					infoLog.Printf("ProcLastMntService learnAndTeach=%s", item)
				}
			}

			if len(learnSli) == 0 || len(teachSli) == 0 {
				attr := "gomntlogic/get_teach_and_learn_lang_faild"
				libcomm.AttrAdd(attr, 1)
			}

			for _, v := range learnAndTeachHashSli {
				langBucket, err := proSyncReq.GetBucketList(head, v, util.DefaultMidPageSize)
				if err != nil {
					infoLog.Printf("ProcLastMntService proSyncReq.GetBucketList uid=%v hashName=%s failed err=%s", uid, v, err)
					attr := "gomntlogic/get_last_classmate_mnt_err"
					libcomm.AttrAdd(attr, 1)
				} else {
					// add bucket to list
					buckets = append(buckets, langBucket)
				}
			}
		}
	case ht_moment.QUERY_TYPE_LEARN:
		// add static
		attr := "gomntlogic/get_last_learn_mnt_count"
		libcomm.AttrAdd(attr, 1)
		// 查询user cache 缓存
		_, _, _, learnLang, err := userCacheApi.QueryLangInfo(head.Uid)
		if err != nil {
			infoLog.Printf("ProcGetMntService userCacheApi.QueryTeachAndLearnLang failed uid=%v err=%s", head.Uid, err)
			// add static
			attr := "gomntlogic/get_teach_and_learn_lang_faild"
			libcomm.AttrAdd(attr, 1)
		} else {
			if len(learnLang) == 0 {
				attr := "gomntlogic/get_teach_and_learn_lang_faild"
				libcomm.AttrAdd(attr, 1)
			}
			var learnSli []string
			if langType == 0 {
				for _, v := range learnLang {
					if v != 0 {
						item := fmt.Sprintf("%s%v", util.LearnLangBucketPrefix, v)
						learnSli = append(learnSli, item)
					}
				}
			} else if Uint32IsInSlice(learnLang, langType) {
				item := fmt.Sprintf("%s%v", util.LearnLangBucketPrefix, langType)
				learnSli = append(learnSli, item)
			} else {
				infoLog.Printf("ProcGetMntService uid=%v learn lang_type=%v learnLang=%v drop", uid, langType, learnLang)
				result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
				rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
					Status: &ht_moment.Header{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("input lang type error"),
					},
				}
				return false
			}

			for _, v := range learnSli {
				learnBucket, err := proSyncReq.GetBucketList(head, v, util.DefaultMidPageSize)
				if err != nil {
					infoLog.Printf("ProcGetMntService proSyncReq.GetBucketList uid=%v hashName=%s failed err=%s", uid, v, err)
					attr := "gomntlogic/get_last_classmate_mnt_err"
					libcomm.AttrAdd(attr, 1)
				} else {
					// add bucket to list
					buckets = append(buckets, learnBucket)
				}
			}
		}
	default:
		infoLog.Printf("ProcLastMntService uid=%v unknow query type=%v", uid, qtype)
	}

	if len(buckets) == 0 {
		// add static
		attr := "gomntlogic/get_last_mnt_bucket_empty"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcLastMntService buckets empty uid=%v qtype=%v langType=%v", uid, qtype, langType)
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("mid list empty"),
			},
		}
		return true
	}
	// Step2: get all userid
	uidSet := common.NewSet()
	// first add self
	uidSet.Add(uid)
	// second add moment relevent uid
	for _, v := range buckets {
		// tempMidMap只用于保存能获取创建者的mid列表 不能取得创建者的mid列表直接丢弃
		tempMidMap := make(map[string]*util.MidInfoStr)
		for k, t := range v.MidMap {
			creater, err := proSyncReq.GetMidCreateUser(head, k)
			if err != nil {
				// add static
				attr := "gomntlogic/get_mnt_creater_err"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcLastMntService proSyncReq.GetMidCreateUser get mid=%s err=%s", k, err)
				continue
			}
			t.Uid = creater
			tempMidMap[k] = t
			uidSet.Add(creater)
			// infoLog.Printf("ProcLastMntService hashName=%s mid=%s creater=%v", v.Name, k, creater)
		}
		v.MidMap = tempMidMap
	}

	// Step3: 查询帖文作者的UserCache 缓存 为后续过滤做准备
	uidSlic := uidSet.List()
	userInfo, err := userCacheApi.BatchQueryUserInfo(head.Uid, uidSlic)
	if err != nil {
		// add static
		attr := "gomntlogic/get_last_mnt_query_userinfo_err"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcLastMntService userCacheApi.BatchQueryUserInfo from=%v uidLis=%v err=%s", head.Uid, uidSlic, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// for i, v := range userInfo {
	// 	infoLog.Printf("ProcLastMntService userinfo index=%v uid=%v", i, v.GetUserID())
	// }
	// Step4: 查询自己的关注列表 head.Uid 才是真正协议发送者的uid
	selfFollowerList, err := relationApi.GetFollowingLists(uid)
	if err != nil {
		// add static
		attr := "gomntlogic/get_last_mnt_get_following_list_err"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcLastMntService relationApi.GetFollowingLists from=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// infoLog.Printf("ProcLastMntService selfFollowerList=%v", selfFollowerList)

	// Step4: mid 过滤
	var pbBucktList []*ht_moment.BucketBody
	switch qtype {
	case ht_moment.QUERY_TYPE_DEFAULT:
		mids, lastMid, err := MigraBucketListV2(head, buckets, userInfo, selfFollowerList, uid, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcLastMntService MigraBucketList reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/get_last_mnt_migra_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		// for i, v := range mids {
		// 	infoLog.Printf("ProcLastMntService migra idex=%v mid=%s", i, v)
		// }

		for _, v := range buckets {
			var bucketSeq uint64
			if v.Seq == 0 {
				bucketSeq = uint64(v.Size) //如果这次下拉没有取到这个桶的内容，就将index设为桶的size
			} else {
				bucketSeq = v.Seq //否则设为该桶这次取到的id
			}
			bucketItem := &ht_moment.BucketBody{
				BucketName: []byte(v.Name),
				Index:      proto.Uint64(bucketSeq),
			}
			pbBucktList = append(pbBucktList, bucketItem)
			// infoLog.Printf("ProcLastMntService bucket name=%s seq=%v size=%v bucketSeq=%v", v.Name, v.Seq, v.Size, bucketSeq)
		}
		totalCnt := 0
		// mids is desc order
		//首先去掉重复的mid
		mids = RemoveDuplicateMid(mids)
		//将所有的mid按照从大到小逆序排序
		sort.Sort(sort.Reverse(sort.StringSlice(mids)))
		for _, v := range mids {
			if v == "1" {
				infoLog.Printf("ProcLastMntService err mid=%s", v)
				continue
			}
			midResult, err := proSyncReq.GetMidInfo(head, v)
			if err != nil {
				// add static
				attr := "gomntlogic/get_last_mid_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcLastMntService proSyncReq.GetMidInfo mid=%v err=%s", v, err)
				continue
			}
			// 如果帖子当前的状态为OP_FOR_FOLLOWER_ONLY or OP_HIDE 且在自己发布的桶内这修复
			if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_HIDE) ||
				midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
				if _, ok := selfMidMap[v]; ok {
					midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
				}
			}
			// 如果当前帖子的状态为OP_FOR_FOLLOWER_ONLY 且在following通内这修复帖子的状态
			if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
				if _, ok := followerMidMap[v]; ok {
					midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
				}
			}

			idList = append(idList, midResult)
			totalCnt += 1
			if totalCnt >= util.DefaultMidPageSize || v == lastMid {
				break
			}
		}
	case ht_moment.QUERY_TYPE_PARTNERS: // 对应客户端following 如果帖子当前的状态为OP_FOR_FOLLOWER_ONLY 需要进行修复
		mids, lastMid, err := MigraFollowerBucketList(head, buckets, userInfo, selfFollowerList, head.Uid, util.DefaultMidPageSize, langType)
		if err != nil {
			infoLog.Printf("ProcLastMntService MigraFollowerBucketList reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/get_last_mnt_migrafollower_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		for _, v := range buckets {
			var bucketSeq uint64
			if v.Seq == 0 {
				bucketSeq = uint64(v.Size) //如果这次下拉没有取到这个桶的内容，就将index设为桶的size
			} else {
				bucketSeq = v.Seq //否则设为该桶这次取到的id
			}
			bucketItem := &ht_moment.BucketBody{
				BucketName: []byte(v.Name),
				Index:      proto.Uint64(bucketSeq),
			}
			pbBucktList = append(pbBucktList, bucketItem)
			// infoLog.Printf("ProcLastMntService bucket name=%s seq=%v size=%v bucketSeq=%v", v.Name, v.Seq, v.Size, bucketSeq)
		}

		totalCnt := 0
		// mids is desc order
		//将所有的mid按照从大到小逆序排序
		sort.Sort(sort.Reverse(sort.StringSlice(mids)))
		for _, v := range mids {
			if v == "1" {
				infoLog.Printf("ProcLastMntService err mid=%s", v)
				continue
			}
			midResult, err := proSyncReq.GetMidInfo(head, v)
			if err != nil {
				// add static
				attr := "gomntlogic/get_last_mid_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcLastMntService proSyncReq.GetMidInfo mid=%v err=%s", v, err)
				continue
			}
			// 如果当前帖子的状态为OP_FOR_FOLLOWER_ONLY 且在following通内这修复帖子的状态
			if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
				if _, ok := followerMidMap[v]; ok {
					midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
				}
				attr := "gomntlogic/mnt_follower_only_cnt"
				libcomm.AttrAdd(attr, 1)
			}

			idList = append(idList, midResult)
			totalCnt += 1
			if totalCnt >= util.DefaultMidPageSize || v == lastMid {
				break
			}
		}
	case ht_moment.QUERY_TYPE_CLASSMATE:
		mids, lastMid, err := MigraBucketList(head, buckets, userInfo, selfFollowerList, head.Uid, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcLastMntService MigraBucketList reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/get_last_mnt_migra_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		for _, v := range buckets {
			var bucketSeq uint64
			if v.Seq == 0 {
				bucketSeq = uint64(v.Size) //如果这次下拉没有取到这个桶的内容，就将index设为桶的size
			} else {
				bucketSeq = v.Seq //否则设为该桶这次取到的id
			}
			bucketItem := &ht_moment.BucketBody{
				BucketName: []byte(v.Name),
				Index:      proto.Uint64(bucketSeq),
			}
			pbBucktList = append(pbBucktList, bucketItem)
			infoLog.Printf("ProcLastMntService bucket name=%s seq=%v size=%v bucketSeq=%v", v.Name, v.Seq, v.Size, bucketSeq)
		}
		totalCnt := 0
		//首先去掉重复的mid
		mids = RemoveDuplicateMid(mids)
		// mids is desc order
		//将所有的mid按照从大到小逆序排序
		sort.Sort(sort.Reverse(sort.StringSlice(mids)))
		for _, v := range mids {
			if v == "1" {
				infoLog.Printf("ProcLastMntService err mid=%s", v)
				continue
			}
			midResult, err := proSyncReq.GetMidInfo(head, v)
			if err != nil {
				// add static
				attr := "gomntlogic/get_last_mid_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcLastMntService proSyncReq.GetMidInfo mid=%v err=%s", v, err)
				continue
			}
			idList = append(idList, midResult)
			totalCnt += 1
			if totalCnt >= util.DefaultMidPageSize || v == lastMid {
				break
			}
		}
	case ht_moment.QUERY_TYPE_LEARN:
		mids, lastMid, err := MigraBucketListExcBlackAndAge(head, buckets, userInfo, head.Uid, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcLastMntService MigraBucketList reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/get_last_mnt_migra_excblack_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		for _, v := range buckets {
			var bucketSeq uint64
			if v.Seq == 0 {
				bucketSeq = uint64(v.Size) //如果这次下拉没有取到这个桶的内容，就将index设为桶的size
			} else {
				bucketSeq = v.Seq //否则设为该桶这次取到的id
			}
			bucketItem := &ht_moment.BucketBody{
				BucketName: []byte(v.Name),
				Index:      proto.Uint64(bucketSeq),
			}
			pbBucktList = append(pbBucktList, bucketItem)
			// infoLog.Printf("ProcLastMntService bucket name=%s seq=%v size=%v bucketSeq=%v", v.Name, v.Seq, v.Size, bucketSeq)
		}
		totalCnt := 0
		//首先去掉重复的mid
		mids = RemoveDuplicateMid(mids)
		// mids is desc order
		//将所有的mid按照从大到小逆序排序
		sort.Sort(sort.Reverse(sort.StringSlice(mids)))
		for _, v := range mids {
			if v == "1" {
				infoLog.Printf("ProcLastMntService err mid=%s", v)
				continue
			}
			midResult, show, err := proSyncReq.GetMidInfoWithShow(head, v)
			if err != nil {
				// add static
				attr := "gomntlogic/get_last_mid_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcLastMntService proSyncReq.GetMidInfoWithShow mid=%v err=%s", v, err)
				continue
			}
			if gShowSwitch && ((show & uint64(ht_moment.MID_BUCKET_STATUS_BS_LEARN)) == uint64(ht_moment.MID_BUCKET_STATUS_BS_LEARN)) {
				infoLog.Printf("ProcLastMntService show=%v drop mid=%s", show, v)
				continue
			}
			idList = append(idList, midResult)
			totalCnt += 1
			if totalCnt >= util.DefaultMidPageSize || v == lastMid {
				break
			}
		}
	default:
		infoLog.Printf("ProcLastMntService uid=%v unknow query type=%v", uid, qtype)
	}
	// step4 :answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.ViewLatestMntIdRspbody = &ht_moment.ViewLatestMomentIDRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Bucket: &ht_moment.BucketInfo{
			BucketList: pbBucktList,
		},
		IdList: idList,
	}

	for i, v := range pbBucktList {
		infoLog.Printf("ProcLastMntService index=%v uid=%v bucket_name=%s bucket_index=%v",
			i,
			uid,
			v.GetBucketName(),
			v.GetIndex())
	}

	for i, v := range idList {
		infoLog.Printf("ProcLastMntService  uid=%v index=%v mid=%s deleted=%v", uid, i, v.GetMid(), v.GetDeleted())
	}
	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/get_last_mid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func RemoveDuplicateMid(srcMids []string) (destMid []string) {
	if len(srcMids) == 0 {
		return nil
	}
	midMap := map[string]bool{}
	for _, v := range srcMids {
		midMap[v] = true
	}
	for k, _ := range midMap {
		destMid = append(destMid, k)
	}
	return destMid
}

func Uint32IsInSlice(targetList []uint32, iterm uint32) bool {
	if len(targetList) == 0 {
		return false
	}
	for _, v := range targetList {
		if iterm == v {
			return true
		}
	}
	return false
}

// following list 可能为空
func MigraBucketList(head *common.HeadV2, buckets []*util.BucketStr, userInfo []*ht_user.UserInfoBody, followingList []uint32, reqUid uint32, needCnt int) (mids []string, lastMid string, err error) {
	if head == nil || len(buckets) == 0 || len(userInfo) == 0 || reqUid == 0 || needCnt == 0 {
		infoLog.Printf("MigraBucketList param error bucketsLen=%v userInfoLen=%v followingListLen=%v reqUid=%v needCnt=%v",
			len(buckets),
			len(userInfo),
			len(followingList),
			reqUid,
			needCnt)
	}
	infoLog.Printf("bucketslen=%v userinfoLen=%v followingList=%v reqUid=%v needCnt=%v",
		len(buckets),
		len(userInfo),
		len(followingList),
		reqUid,
		needCnt)
	// change following slice to map for search
	mapFollowingList := make(map[uint32]bool)
	for _, v := range followingList {
		mapFollowingList[v] = true
	}
	// get self age
	selfAge := GetSelfAge(userInfo, reqUid)
	// get i hide other moment uid list
	_, iHideUidList, err := proSyncReq.GetHideOtherMntUidList(head, reqUid, 0)
	if err != nil {
		infoLog.Printf("MigraBucketList GetHideOtherMntUidList uid=%v failed err=%s", reqUid, err)
	}
	//所有桶里的mid先放到这个slice里，然后再过滤 当前slice中mid是无序的
	var totalMid []string
	midToUid := make(map[string]uint32)
	mapUidToForbitList := make(map[uint32]map[uint32]uint32)
	var cntBeforeFilter uint64
	for _, v := range buckets {
		cntBeforeFilter += uint64(len(v.MidMap))
		for k, t := range v.MidMap {
			totalMid = append(totalMid, k)
			midToUid[k] = t.Uid
			// infoLog.Printf("MigraBucketList mid=%s createrUid=%v", k, t.Uid)
		}
	}
	//将所有的mid按照从大到小逆序排序
	sort.Sort(sort.Reverse(sort.StringSlice(totalMid)))
	// infoLog.Printf("MigraBucketList sort totalMid=%v", totalMid)
	//过滤年龄,只过滤top_max个mid
	realCnt := 0
	//执行顺序遍历即可
	for _, v := range totalMid {
		var tempUid uint32
		if mntCreater, ok := midToUid[v]; !ok {
			infoLog.Printf("MigraBucketList mid=%s not found in midToUid", v)
			continue
		} else if mntCreater == 0 {
			infoLog.Printf("MigraBucketList mid=%s not found create uid=%v contine", v, mntCreater)
			continue
		} else {
			tempUid = mntCreater // 必须要设置值
		}
		// 找到了uid
		var forbitList map[uint32]uint32
		forbitList, ok := mapUidToForbitList[tempUid]
		if !ok {
			_, mapForbitLIst, err := proSyncReq.GetNotShareToUidList(head, tempUid, 0)
			if err != nil {
				infoLog.Printf("MigraBucketList proSyncReq.GetNotShareToUidList uid=%v failed", tempUid)
				mapForbitLIst = make(map[uint32]uint32)
				mapUidToForbitList[tempUid] = mapForbitLIst
			} else {
				mapUidToForbitList[tempUid] = mapForbitLIst
				// infoLog.Printf("MigraBucketList mapUidToForbitList uid=%v forbitList=%v", tempUid, mapForbitLIst)
			}
			forbitList = mapUidToForbitList[tempUid]
		} else {
			// infoLog.Printf("MigraBucketList mapUidToForbitList found uid=%v", tempUid)
		}
		// 查询uid 的年龄
		otherAge := GetSelfAge(userInfo, tempUid)
		// 我隐藏某人信息流和某人屏蔽我看他的信息流优先级更高
		if IsHideOtherMnt(iHideUidList, tempUid) || IsForbitMeToSee(forbitList, reqUid) {
			infoLog.Printf("MigraBucketList i=%v ignore other=%v mnt=%s", reqUid, tempUid, v)
			continue
		}
		// chech following relation
		if IsFriend(reqUid, tempUid, mapFollowingList) {
			// 已近关注的用户
			mids = append(mids, v)
			realCnt += 1
		} else if IsAgeValid(uint32(selfAge), uint32(otherAge)) && !IsBlack(reqUid, tempUid, userInfo) {
			mids = append(mids, v)
			realCnt += 1
		} else {
			// infoLog.Printf("MigraBucketList reqUid=%v realCnt=%v mid=%s drop", reqUid, realCnt, v)
		}

		// check whether reach need count
		if realCnt >= needCnt {
			infoLog.Printf("MigraBucketList reach needCnt=%v realCnt=%v", needCnt, realCnt)
			break
		}
	}
	//获取mids的mid的index
	if len(mids) == 0 {
		infoLog.Printf("MigraBucketList beforeFilte=%v afterFilter=%v mids=%v",
			cntBeforeFilter,
			len(mids),
			mids)
		return nil, lastMid, nil
	}
	lastMid = mids[len(mids)-1]
	for _, v := range mids {
		for _, t := range buckets {
			if i, ok := t.MidMap[v]; ok {
				seq, err := strconv.ParseUint(i.Seq, 10, 64)
				if err != nil {
					infoLog.Printf("MigraBucketList strconv.ParseUint mid=%s seq=%s", v, t.Seq)
				}
				if t.Seq == 0 {
					t.Seq = seq
					infoLog.Printf("MigraBucketList hashName=%s Seq=%v init", t.Name, t.Seq)
				} else if seq < t.Seq {
					// infoLog.Printf("MigraBucketList hashName=%s t.Seq=%v seq=%v update", t.Name, t.Seq, seq)
					t.Seq = seq
				}
				// mid is unique after find need break
				break
			}
		}
	}
	// infoLog.Printf("MigraBucketList beforeFilte=%v afterFilter=%v mids=%v lastMid=%s",
	// 	cntBeforeFilter,
	// 	len(mids),
	// 	mids,
	// 	lastMid)
	infoLog.Printf("MigraBucketList beforeFilte=%v afterFilter=%v lastMid=%s",
		cntBeforeFilter,
		len(mids),
		lastMid)
	return mids, lastMid, nil
}

func MigraBucketListV2(head *common.HeadV2, buckets []*util.BucketStr, userInfo []*ht_user.UserInfoBody, followingList []uint32, reqUid uint32, needCnt int) (mids []string, lastMid string, err error) {
	if head == nil || len(buckets) == 0 || len(userInfo) == 0 || reqUid == 0 || needCnt == 0 {
		infoLog.Printf("MigraBucketListV2 param error bucketsLen=%v userInfoLen=%v followingListLen=%v reqUid=%v needCnt=%v",
			len(buckets),
			len(userInfo),
			len(followingList),
			reqUid,
			needCnt)
	}
	// infoLog.Printf("bucketslen=%v userinfoLen=%v followingList=%v reqUid=%v needCnt=%v",
	// len(buckets),
	// len(userInfo),
	// len(followingList),
	// reqUid,
	// needCnt)
	// change following slice to map for search
	mapFollowingList := make(map[uint32]bool)
	for _, v := range followingList {
		mapFollowingList[v] = true
	}
	// get self age
	selfAge := GetSelfAge(userInfo, reqUid)
	// get i hide other moment uid list
	_, iHideUidList, err := proSyncReq.GetHideOtherMntUidList(head, reqUid, 0)
	if err != nil {
		infoLog.Printf("MigraBucketListV2 GetHideOtherMntUidList uid=%v failed err=%s", reqUid, err)
	}
	// 只有桶的个数大于1个时才有需要进行过滤操作
	if len(buckets) > 1 {
		// 首先找出整体最大的
		var totalMinMidSlic []string
		var minMaxMid string // 所有桶最新mid中较大的一个mid
		for _, v := range buckets {
			var tempMidMap []string
			// 将mid 放入slice 中
			for i, _ := range v.MidMap {
				tempMidMap = append(tempMidMap, i)
			}
			if len(tempMidMap) > 0 {
				//将所有的mid按照从大到小逆序排序
				sort.Sort(sort.Reverse(sort.StringSlice(tempMidMap)))
				v.MaxMid = tempMidMap[0]
				v.MinMid = tempMidMap[len(tempMidMap)-1]
				totalMinMidSlic = append(totalMinMidSlic, v.MinMid)
				infoLog.Printf("MigraBucketListV2 HashName=%s MaxMid=%s MidMid=%s totalMinMidLen=%v", v.Name, v.MaxMid, v.MinMid, len(totalMinMidSlic))
			}
		}
		if len(totalMinMidSlic) > 0 {
			// 然后在每个桶最新的mid中选出一个较大的mid
			sort.Sort(sort.Reverse(sort.StringSlice(totalMinMidSlic)))
			minMaxMid = totalMinMidSlic[0]
		}
		// infoLog.Printf("MigraBucketListV2 reqUid=%v minMaxMid=%s", reqUid, minMaxMid)
		for _, v := range buckets {
			// 如果当前桶中最大的mid比某个桶中最新的mid还要小,直接清空当前通所有的mid,但是需要设置当前桶最大的mid序号
			// infoLog.Printf("MigraBucketListV2 uid=%v HashName=%s MidMapLen=%v", reqUid, v.Name, len(v.MidMap))
			if strings.Compare(v.MaxMid, minMaxMid) < 0 && len(v.MidMap) > 0 {
				seq, err := strconv.ParseUint((v.MidMap[v.MaxMid]).Seq, 10, 64)
				if err != nil {
					infoLog.Printf("MigraBucketListV2 strconv.ParseUint mid=%s seq=%s err=%s", v.MaxMid, v.MidMap[v.MaxMid].Seq, err)
				}
				// 设置当前桶的最大序号
				v.Seq = seq
				// 清空当前桶的所有mid
				v.MidMap = nil
				infoLog.Printf("MigraBucketListV2 HashName=%s MaxMid=%s Seq=%v minMaxMid=%s drop whole midmap", v.Name, v.MaxMid, seq, minMaxMid)
			}
		}
	}

	//所有桶里的mid先放到这个slice里，然后再过滤 当前slice中mid是无序的
	var totalMid []string
	midToUid := make(map[string]uint32)
	mapUidToForbitList := make(map[uint32]map[uint32]uint32)
	var cntBeforeFilter uint64
	for _, v := range buckets {
		cntBeforeFilter += uint64(len(v.MidMap))
		for k, t := range v.MidMap {
			totalMid = append(totalMid, k)
			midToUid[k] = t.Uid
			// infoLog.Printf("MigraBucketList mid=%s createrUid=%v", k, t.Uid)
		}
	}
	//将所有的mid按照从大到小逆序排序
	sort.Sort(sort.Reverse(sort.StringSlice(totalMid)))
	// infoLog.Printf("MigraBucketList sort totalMid=%v", totalMid)
	//过滤年龄,只过滤top_max个mid
	realCnt := 0
	//执行顺序遍历即可
	for _, v := range totalMid {
		var tempUid uint32
		if mntCreater, ok := midToUid[v]; !ok {
			infoLog.Printf("MigraBucketListV2 mid=%s not found in midToUid", v)
			continue
		} else if mntCreater == 0 {
			infoLog.Printf("MigraBucketListV2 mid=%s not found create uid=%v contine", v, mntCreater)
			continue
		} else {
			tempUid = mntCreater // 必须要设置值
		}
		// 找到了uid
		var forbitList map[uint32]uint32
		forbitList, ok := mapUidToForbitList[tempUid]
		if !ok {
			_, mapForbitLIst, err := proSyncReq.GetNotShareToUidList(head, tempUid, 0)
			if err != nil {
				infoLog.Printf("MigraBucketListV2 proSyncReq.GetNotShareToUidList uid=%v failed", tempUid)
				mapForbitLIst = make(map[uint32]uint32)
				mapUidToForbitList[tempUid] = mapForbitLIst
			} else {
				mapUidToForbitList[tempUid] = mapForbitLIst
				// infoLog.Printf("MigraBucketListV2 mapUidToForbitList uid=%v forbitList=%v", tempUid, mapForbitLIst)
			}
			forbitList = mapUidToForbitList[tempUid]
		} else {
			// infoLog.Printf("MigraBucketListV2 mapUidToForbitList found uid=%v", tempUid)
		}
		// 查询uid 的年龄
		otherAge := GetSelfAge(userInfo, tempUid)
		// 我隐藏某人信息流和某人屏蔽我看他的信息流优先级更高
		if IsHideOtherMnt(iHideUidList, tempUid) || IsForbitMeToSee(forbitList, reqUid) {
			infoLog.Printf("MigraBucketListV2 i=%v ignore other=%v mnt=%s", reqUid, tempUid, v)
			continue
		}
		// chech following relation
		if IsFriend(reqUid, tempUid, mapFollowingList) {
			// 已近关注的用户
			mids = append(mids, v)
			realCnt += 1
		} else if IsAgeValid(uint32(selfAge), uint32(otherAge)) && !IsBlack(reqUid, tempUid, userInfo) {
			mids = append(mids, v)
			realCnt += 1
		} else {
			// infoLog.Printf("MigraBucketListV2 reqUid=%v realCnt=%v mid=%s drop", reqUid, realCnt, v)
		}

		// check whether reach need count
		if realCnt >= needCnt {
			infoLog.Printf("MigraBucketListV2 reach needCnt=%v realCnt=%v", needCnt, realCnt)
			break
		}
	}
	//获取mids的mid的index
	if len(mids) == 0 {
		infoLog.Printf("MigraBucketListV2 beforeFilte=%v afterFilter=%v mids=%v",
			cntBeforeFilter,
			len(mids),
			mids)
		return nil, lastMid, nil
	}
	lastMid = mids[len(mids)-1]
	for _, v := range mids {
		for _, t := range buckets {
			if i, ok := t.MidMap[v]; ok {
				seq, err := strconv.ParseUint(i.Seq, 10, 64)
				if err != nil {
					infoLog.Printf("MigraBucketListV2 strconv.ParseUint mid=%s seq=%s", v, t.Seq)
				}
				if t.Seq == 0 {
					t.Seq = seq
					infoLog.Printf("MigraBucketListV2 hashName=%s Seq=%v init", t.Name, t.Seq)
				} else if seq < t.Seq {
					// infoLog.Printf("MigraBucketListV2 hashName=%s t.Seq=%v seq=%v update", t.Name, t.Seq, seq)
					t.Seq = seq
				}
				// mid is unique after find need break
				break
			}
		}
	}
	// infoLog.Printf("MigraBucketListV2 beforeFilte=%v afterFilter=%v mids=%v lastMid=%s",
	// 	cntBeforeFilter,
	// 	len(mids),
	// 	mids,
	// 	lastMid)
	infoLog.Printf("MigraBucketListV2 reqUid=%v beforeFilte=%v afterFilter=%v lastMid=%s",
		reqUid,
		cntBeforeFilter,
		len(mids),
		lastMid)
	return mids, lastMid, nil
}

func MigraHistoryBucketList(head *common.HeadV2, buckets []*util.BucketStr, userInfo []*ht_user.UserInfoBody, followingList []uint32, reqUid uint32, needCnt int) (mids []string, lastMid string, err error) {
	if head == nil || len(buckets) == 0 || len(userInfo) == 0 || reqUid == 0 || needCnt == 0 {
		infoLog.Printf("MigraHistoryBucketList param error bucketsLen=%v userInfoLen=%v followingListLen=%v reqUid=%v needCnt=%v",
			len(buckets),
			len(userInfo),
			len(followingList),
			reqUid,
			needCnt)
	}
	infoLog.Printf("MigraHistoryBucketList bucketslen=%v userinfoLen=%v followingList=%v reqUid=%v needCnt=%v",
		len(buckets),
		len(userInfo),
		len(followingList),
		reqUid,
		needCnt)
	// change following slice to map for search
	mapFollowingList := make(map[uint32]bool)
	for _, v := range followingList {
		mapFollowingList[v] = true
	}
	// get self age
	selfAge := GetSelfAge(userInfo, reqUid)
	// get i hide other moment uid list
	_, iHideUidList, err := proSyncReq.GetHideOtherMntUidList(head, reqUid, 0)
	if err != nil {
		infoLog.Printf("MigraHistoryBucketList GetHideOtherMntUidList uid=%v failed err=%s", reqUid, err)
	}
	// 只有桶的个数大于1个时才有需要进行过滤操作
	if len(buckets) > 1 {
		// 首先找出整体最大的
		var totalMinMidSlic []string
		var minMaxMid string // 所有桶最新mid中较大的一个mid
		for _, v := range buckets {
			var tempMidMap []string
			// 将mid 放入slice 中
			for i, _ := range v.MidMap {
				tempMidMap = append(tempMidMap, i)
			}
			if len(tempMidMap) > 0 {
				//将所有的mid按照从大到小逆序排序
				sort.Sort(sort.Reverse(sort.StringSlice(tempMidMap)))
				v.MaxMid = tempMidMap[0]
				v.MinMid = tempMidMap[len(tempMidMap)-1]
				totalMinMidSlic = append(totalMinMidSlic, v.MinMid)
				infoLog.Printf("MigraHistoryBucketList HashName=%s MaxMid=%s MidMid=%s totalMinMidLen=%v", v.Name, v.MaxMid, v.MinMid, len(totalMinMidSlic))
			}
		}
		if len(totalMinMidSlic) > 0 {
			// 然后在每个桶最新的mid中选出一个较大的mid
			sort.Sort(sort.Reverse(sort.StringSlice(totalMinMidSlic)))
			minMaxMid = totalMinMidSlic[0]
		}
		infoLog.Printf("MigraHistoryBucketList reqUid=%v minMaxMid=%s", reqUid, minMaxMid)
		for _, v := range buckets {
			// 如果当前桶中最大的mid比某个桶中最新的mid还要小,直接清空当前通所有的mid,但是需要设置当前桶最大的mid序号
			infoLog.Printf("MigraHistoryBucketList uid=%v HashName=%s MidMapLen=%v", reqUid, v.Name, len(v.MidMap))
			if strings.Compare(v.MaxMid, minMaxMid) < 0 && len(v.MidMap) > 0 {
				seq, err := strconv.ParseUint((v.MidMap[v.MaxMid]).Seq, 10, 64)
				if err != nil {
					infoLog.Printf("MigraHistoryBucketList strconv.ParseUint mid=%s seq=%s err=%s", v.MaxMid, v.MidMap[v.MaxMid].Seq, err)
				}
				// 设置当前桶的最大序号
				v.Seq = seq
				// 清空当前桶的所有mid
				v.MidMap = nil
				infoLog.Printf("MigraHistoryBucketList HashName=%s MaxMid=%s Seq=%v minMaxMid=%s drop whole midmap", v.Name, v.MaxMid, seq, minMaxMid)
			}
		}
	}

	//所有桶里的mid先放到这个slice里，然后再过滤 当前slice中mid是无序的
	var totalMid []string
	midToUid := make(map[string]uint32)
	mapUidToForbitList := make(map[uint32]map[uint32]uint32)
	var cntBeforeFilter uint64
	for _, v := range buckets {
		cntBeforeFilter += uint64(len(v.MidMap))
		for k, t := range v.MidMap {
			totalMid = append(totalMid, k)
			midToUid[k] = t.Uid
			// infoLog.Printf("MigraBucketList mid=%s createrUid=%v", k, t.Uid)
		}
	}
	//将所有的mid按照从大到小逆序排序
	sort.Sort(sort.Reverse(sort.StringSlice(totalMid)))
	// infoLog.Printf("MigraBucketList sort totalMid=%v", totalMid)
	//过滤年龄,只过滤top_max个mid
	realCnt := 0
	//执行顺序遍历即可
	for _, v := range totalMid {
		var tempUid uint32
		if mntCreater, ok := midToUid[v]; !ok {
			infoLog.Printf("MigraHistoryBucketList mid=%s not found in midToUid", v)
			continue
		} else if mntCreater == 0 {
			infoLog.Printf("MigraHistoryBucketList mid=%s not found create uid=%v contine", v, mntCreater)
			continue
		} else {
			tempUid = mntCreater // 必须要设置值
		}
		// 找到了uid
		var forbitList map[uint32]uint32
		forbitList, ok := mapUidToForbitList[tempUid]
		if !ok {
			_, mapForbitLIst, err := proSyncReq.GetNotShareToUidList(head, tempUid, 0)
			if err != nil {
				infoLog.Printf("MigraHistoryBucketList proSyncReq.GetNotShareToUidList uid=%v failed", tempUid)
				mapForbitLIst = make(map[uint32]uint32)
				mapUidToForbitList[tempUid] = mapForbitLIst
			} else {
				mapUidToForbitList[tempUid] = mapForbitLIst
				// infoLog.Printf("MigraHistoryBucketList mapUidToForbitList uid=%v forbitList=%v", tempUid, mapForbitLIst)
			}
			forbitList = mapUidToForbitList[tempUid]
		} else {
			// infoLog.Printf("MigraHistoryBucketList mapUidToForbitList found uid=%v", tempUid)
		}
		// 查询uid 的年龄
		otherAge := GetSelfAge(userInfo, tempUid)
		// 我隐藏某人信息流和某人屏蔽我看他的信息流优先级更高
		if IsHideOtherMnt(iHideUidList, tempUid) || IsForbitMeToSee(forbitList, reqUid) {
			infoLog.Printf("MigraHistoryBucketList i=%v ignore other=%v mnt=%s", reqUid, tempUid, v)
			continue
		}
		// chech following relation
		if IsFriend(reqUid, tempUid, mapFollowingList) {
			// 已近关注的用户
			mids = append(mids, v)
			realCnt += 1
		} else if IsAgeValid(uint32(selfAge), uint32(otherAge)) && !IsBlack(reqUid, tempUid, userInfo) {
			mids = append(mids, v)
			realCnt += 1
		} else {
			// infoLog.Printf("MigraHistoryBucketList reqUid=%v realCnt=%v mid=%s drop", reqUid, realCnt, v)
		}

		// check whether reach need count
		if realCnt >= needCnt {
			infoLog.Printf("MigraHistoryBucketList reach needCnt=%v realCnt=%v", needCnt, realCnt)
			break
		}
	}
	//获取mids的mid的index
	if len(mids) == 0 {
		infoLog.Printf("MigraHistoryBucketList beforeFilte=%v afterFilter=%v mids=%v",
			cntBeforeFilter,
			len(mids),
			mids)
		return nil, lastMid, nil
	}
	lastMid = mids[len(mids)-1]
	for _, v := range mids {
		for _, t := range buckets {
			if i, ok := t.MidMap[v]; ok {
				seq, err := strconv.ParseUint(i.Seq, 10, 64)
				if err != nil {
					infoLog.Printf("MigraHistoryBucketList strconv.ParseUint mid=%s seq=%s", v, t.Seq)
				}
				if t.Seq == 0 {
					t.Seq = seq
					infoLog.Printf("MigraHistoryBucketList hashName=%s Seq=%v init", t.Name, t.Seq)
				} else if seq < t.Seq {
					// infoLog.Printf("MigraHistoryBucketList hashName=%s t.Seq=%v seq=%v update", t.Name, t.Seq, seq)
					t.Seq = seq
				}
				// mid is unique after find need break
				break
			}
		}
	}
	// infoLog.Printf("MigraHistoryBucketList beforeFilte=%v afterFilter=%v mids=%v lastMid=%s",
	// 	cntBeforeFilter,
	// 	len(mids),
	// 	mids,
	// 	lastMid)
	infoLog.Printf("MigraHistoryBucketList reqUid=%v beforeFilte=%v afterFilter=%v lastMid=%s",
		reqUid,
		cntBeforeFilter,
		len(mids),
		lastMid)
	return mids, lastMid, nil
}

// following list 可能为空
func MigraFollowerBucketList(head *common.HeadV2,
	buckets []*util.BucketStr,
	userInfo []*ht_user.UserInfoBody,
	followingList []uint32,
	reqUid uint32,
	needCnt int,
	langType uint32) (mids []string, lastMid string, err error) {
	if head == nil || len(buckets) == 0 || len(userInfo) == 0 || reqUid == 0 || needCnt == 0 {
		infoLog.Printf("MigraFollowerBucketList param error bucketsLen=%v userInfoLen=%v followingListLen=%v reqUid=%v needCnt=%v",
			len(buckets),
			len(userInfo),
			len(followingList),
			reqUid,
			needCnt)
	}
	// change following slice to map for search
	mapFollowingList := make(map[uint32]bool)
	for _, v := range followingList {
		mapFollowingList[v] = true
	}

	// get i hide other moment uid list
	_, iHideUidList, err := proSyncReq.GetHideOtherMntUidList(head, reqUid, 0)
	if err != nil {
		infoLog.Printf("MigraFollowerBucketList GetHideOtherMntUidList uid=%v failed err=%s", reqUid, err)
	}
	//所有桶里的mid先放到这个slice里，然后再过滤 当前slice中mid是无序的
	var totalMid []string
	midToUid := make(map[string]uint32)
	mapUidToForbitList := make(map[uint32]map[uint32]uint32)
	var cntBeforeFilter uint64
	for _, v := range buckets {
		cntBeforeFilter += uint64(len(v.MidMap))
		for k, t := range v.MidMap {
			totalMid = append(totalMid, k)
			midToUid[k] = t.Uid
		}
	}
	//将所有的mid按照从大到小逆序排序
	sort.Sort(sort.Reverse(sort.StringSlice(totalMid)))
	//过滤年龄,只过滤top_max个mid
	realCnt := 0
	//执行顺序遍历即可
	for _, v := range totalMid {
		var tempUid uint32
		if mntCreater, ok := midToUid[v]; !ok {
			infoLog.Printf("MigraFollowerBucketList mid=%s not found in midToUid", v)
			continue
		} else if mntCreater == 0 {
			infoLog.Printf("MigraFollowerBucketList mid=%s not found create uid=%v contine", v, mntCreater)
			continue
		} else {
			tempUid = mntCreater
		}
		// 找到了uid
		var forbitList map[uint32]uint32
		forbitList, ok := mapUidToForbitList[tempUid]
		if !ok {
			_, mapForbitLIst, err := proSyncReq.GetNotShareToUidList(head, tempUid, 0)
			if err != nil {
				infoLog.Printf("MigraFollowerBucketList proSyncReq.GetNotShareToUidList uid=%v failed", tempUid)
				mapForbitLIst = make(map[uint32]uint32)
				mapUidToForbitList[tempUid] = mapForbitLIst
			} else {
				mapUidToForbitList[tempUid] = mapForbitLIst
			}
			forbitList = mapUidToForbitList[tempUid]
		} else {
			infoLog.Printf("MigraFollowerBucketList mapUidToForbitList found uid=%v", tempUid)
		}

		// 我隐藏某人信息流和某人屏蔽我看他的信息流优先级更高
		if IsHideOtherMnt(iHideUidList, tempUid) || IsForbitMeToSee(forbitList, reqUid) {
			infoLog.Printf("MigraFollowerBucketList i=%v ignore other=%v mnt=%s", reqUid, tempUid, v)
			continue
		}

		if langType != 0 && !IsLangMatch(reqUid, tempUid, userInfo, langType) {
			infoLog.Printf("MigraFollowerBucketList langType=%v reqUid=%v tempUid=%v lang not mathc",
				langType,
				reqUid,
				tempUid)
			continue
		}
		// chech following relation
		if IsFriend(reqUid, tempUid, mapFollowingList) {
			// 已近关注的用户
			mids = append(mids, v)
			realCnt += 1
		} else {
			infoLog.Printf("MigraFollowerBucketList reqUid=%v realCnt=%v mid=%s drop", reqUid, realCnt, v)
		}

		// check whether reach need count
		if realCnt >= needCnt {
			infoLog.Printf("MigraFollowerBucketList reach needCnt=%v realCnt=%v", needCnt, realCnt)
			break
		}
	}
	//获取mids的mid的index
	if len(mids) == 0 {
		infoLog.Printf("MigraFollowerBucketList beforeFilte=%v afterFilter=%v mids=%v",
			cntBeforeFilter,
			len(mids),
			mids)
		return nil, lastMid, nil
	}
	lastMid = mids[len(mids)-1]
	for _, v := range mids {
		for _, t := range buckets {
			if i, ok := t.MidMap[v]; ok {
				seq, err := strconv.ParseUint(i.Seq, 10, 64)
				if err != nil {
					infoLog.Printf("MigraFollowerBucketList strconv.ParseUint mid=%s seq=%s", v, t.Seq)
				}
				if t.Seq == 0 {
					t.Seq = seq
				} else if seq < t.Seq {
					t.Seq = seq
				}
				// mid is unique after find need break
				break
			}
		}
	}
	infoLog.Printf("MigraFollowerBucketList beforeFilte=%v afterFilter=%v", cntBeforeFilter, len(mids))
	return mids, lastMid, nil
}

// following list 可能为空
func MigraBucketListExcBlackAndAge(head *common.HeadV2, buckets []*util.BucketStr, userInfo []*ht_user.UserInfoBody, reqUid uint32, needCnt int) (mids []string, lastMid string, err error) {
	if head == nil || len(buckets) == 0 || len(userInfo) == 0 || reqUid == 0 || needCnt == 0 {
		infoLog.Printf("MigraBucketListExcBlackAndAge param error bucketsLen=%v userInfoLen=%v  reqUid=%v needCnt=%v",
			len(buckets),
			len(userInfo),
			reqUid,
			needCnt)
	}
	// get self age
	selfAge := GetSelfAge(userInfo, reqUid)
	// get i hide other moment uid list
	_, iHideUidList, err := proSyncReq.GetHideOtherMntUidList(head, reqUid, 0)
	if err != nil {
		infoLog.Printf("MigraBucketListExcBlackAndAge GetHideOtherMntUidList uid=%v failed err=%s", reqUid, err)
	}
	//所有桶里的mid先放到这个slice里，然后再过滤 当前slice中mid是无序的
	var totalMid []string
	midToUid := make(map[string]uint32)
	mapUidToForbitList := make(map[uint32]map[uint32]uint32)
	var cntBeforeFilter uint64
	for _, v := range buckets {
		cntBeforeFilter += uint64(len(v.MidMap))
		for k, t := range v.MidMap {
			totalMid = append(totalMid, k)
			midToUid[k] = t.Uid
		}
	}
	//将所有的mid按照从大到小逆序排序
	sort.Sort(sort.Reverse(sort.StringSlice(totalMid)))
	//过滤年龄,只过滤top_max个mid
	realCnt := 0
	//执行顺序遍历即可
	for _, v := range totalMid {
		var tempUid uint32
		if mntCreater, ok := midToUid[v]; !ok {
			infoLog.Printf("MigraBucketListExcBlackAndAge mid=%s not found in midToUid", v)
			continue
		} else if mntCreater == 0 {
			infoLog.Printf("MigraBucketListExcBlackAndAge mid=%s not found create uid=%v contine", v, mntCreater)
			continue
		} else {
			tempUid = mntCreater
		}
		// 找到了uid
		var forbitList map[uint32]uint32
		forbitList, ok := mapUidToForbitList[tempUid]
		if !ok {
			_, mapForbitLIst, err := proSyncReq.GetNotShareToUidList(head, tempUid, 0)
			if err != nil {
				infoLog.Printf("MigraBucketListExcBlackAndAge proSyncReq.GetNotShareToUidList uid=%v failed", tempUid)
				mapForbitLIst = make(map[uint32]uint32)
				mapUidToForbitList[tempUid] = mapForbitLIst
			} else {
				mapUidToForbitList[tempUid] = mapForbitLIst
			}
			forbitList = mapUidToForbitList[tempUid]
		} else {
			infoLog.Printf("MigraBucketListExcBlackAndAge mapUidToForbitList found uid=%v", tempUid)
		}
		// 查询uid 的年龄
		otherAge := GetSelfAge(userInfo, tempUid)
		// 我隐藏某人信息流和某人屏蔽我看他的信息流优先级更高
		if IsHideOtherMnt(iHideUidList, tempUid) || IsForbitMeToSee(forbitList, reqUid) {
			infoLog.Printf("MigraBucketListExcBlackAndAge i=%v ignore other=%v mnt=%s", reqUid, tempUid, v)
			continue
		}
		// 年龄匹配并且没有拉黑这个用户
		if IsAgeValid(uint32(selfAge), uint32(otherAge)) && !IsBlack(reqUid, tempUid, userInfo) {
			mids = append(mids, v)
			realCnt += 1
		} else {
			// infoLog.Printf("MigraBucketListExcBlackAndAge reqUid=%v realCnt=%v mid=%s drop", reqUid, realCnt, v)
		}

		// check whether reach need count
		if realCnt >= needCnt {
			infoLog.Printf("MigraBucketListExcBlackAndAge reach needCnt=%v realCnt=%v", needCnt, realCnt)
			break
		}
	}
	//获取mids的mid的index
	if len(mids) == 0 {
		infoLog.Printf("MigraBucketListExcBlackAndAge beforeFilte=%v afterFilter=%v mids=%v",
			cntBeforeFilter,
			len(mids),
			mids)
		return nil, lastMid, nil
	}
	lastMid = mids[len(mids)-1]
	for _, v := range mids {
		for _, t := range buckets {
			if i, ok := t.MidMap[v]; ok {
				seq, err := strconv.ParseUint(i.Seq, 10, 64)
				if err != nil {
					infoLog.Printf("MigraBucketListExcBlackAndAge strconv.ParseUint mid=%s seq=%s", v, t.Seq)
				}
				if t.Seq == 0 {
					t.Seq = seq
				} else if seq < t.Seq {
					t.Seq = seq
				}
				// mid is unique after find need break
				break
			}
		}
	}
	infoLog.Printf("MigraBucketListExcBlackAndAge beforeFilte=%v afterFilter=%v", cntBeforeFilter, len(mids))
	return mids, lastMid, nil
}

func GetSelfAge(userInfo []*ht_user.UserInfoBody, reqUid uint32) uint64 {
	if len(userInfo) == 0 || reqUid == 0 {
		infoLog.Printf("GetSelfAge input param error userInfo len=%v reqUid=%v", len(userInfo), reqUid)
		return 0
	}
	for _, v := range userInfo {
		if reqUid == v.GetUserID() {
			return v.GetBirthday()
		}
	}
	return 0
}

/**
 * [IsHideOtherMnt		检查自己是否隐藏某人信息流]
 * @param  mapUidToTime [自己隐藏用户列表]
 * @param  uid       	[对方uid]
 * @return              [隐藏:true 没有隐藏:false]
 */
func IsHideOtherMnt(mapUidToTime map[uint32]uint32, uid uint32) bool {
	if mapUidToTime == nil || uid == 0 {
		infoLog.Printf("IsHideOtherMnt nil mapUidToTime or 0 uid return false uid=%v", uid)
		return false
	}
	if _, ok := mapUidToTime[uid]; !ok {
		return false
	} else {
		return true
	}
}

/**
 * [IsForbitMeToSee		 检查某人是否屏蔽我看他的信息流]
 * @param  mapForbitList [某人的屏蔽用户列表]
 * @param  uid		     [我的Uid]
 * @return               [屏蔽:true 没有屏蔽:false]
 */
func IsForbitMeToSee(mapUidToTime map[uint32]uint32, uid uint32) bool {
	if mapUidToTime == nil || uid == 0 {
		infoLog.Printf("IsForbitMeToSee nil mapUidToTime or 0 uid return false uid=%v", uid)
		return false
	}
	if _, ok := mapUidToTime[uid]; !ok {
		return false
	} else {
		return true
	}
}

func IsFriend(self, other uint32, followingList map[uint32]bool) bool {
	if self == 0 || other == 0 || len(followingList) == 0 {
		infoLog.Printf("IsFriend param error self=%v other=%v followingListLen=%v", self, other, followingList)
		return false
	}
	if _, ok := followingList[other]; ok {
		return true
	} else {
		return false
	}
}

func IsAgeValid(selfAge, otherAge uint32) bool {
	//处理年龄为0的情况
	if selfAge == 0 {
		selfAge = 1
	}
	if otherAge == 0 {
		otherAge = 1
	}
	//处理用户年龄超过上限的情况
	if selfAge > 90 {
		selfAge = 90
	}
	if otherAge > 90 {
		otherAge = 90
	}
	if selfAge < 18 {
		if otherAge < 18 {
			return true
		} else {
			return false
		}
	} else if selfAge < 91 {
		if otherAge < 18 {
			return false
		} else {
			return true
		}
	} else {
		return false
	}

}

func IsBlack(self, other uint32, userInfo []*ht_user.UserInfoBody) (valid bool) {
	if self == 0 || other == 0 || len(userInfo) == 0 {
		infoLog.Printf("IsBlack input param err self=%v other=%v userInfoLen=%v", self, other, len(userInfo))
		valid = false
		return valid
	}
	for _, v := range userInfo {
		if self == v.GetUserID() {
			for _, t := range v.GetBlackidList() {
				if other == t {
					valid = true
					infoLog.Printf("IsBlack self=%v black other=%v", self, other)
					break
				}
			}
			break
		}
	}

	if !valid { //  如果自己没有拉黑对方，需要继续检查是否已经被对方拉黑
		for _, v := range userInfo {
			if other == v.GetUserID() {
				for _, t := range v.GetBlackidList() {
					if self == t {
						valid = true
						infoLog.Printf("IsBlack other=%v black self=%v", other, self)
						break
					}
				}
				break
			}
		}

	}
	return valid
}

func IsLangMatch(self, other uint32, userInfo []*ht_user.UserInfoBody, langType uint32) (valid bool) {
	// 如果输入的uid为0 这返回不匹配
	if self == 0 || other == 0 || len(userInfo) == 0 {

		valid = false
		return valid
	}
	// 如果学习语言是 0 这返回匹配
	if langType == 0 {
		valid = true
		return valid
	}

	var selfInfo *ht_user.UserInfoBody
	var otherInfo *ht_user.UserInfoBody
	for _, v := range userInfo {
		if v.GetUserID() == self {
			selfInfo = v
		}
		if v.GetUserID() == other {
			otherInfo = v
		}
	}

	if selfInfo != nil && otherInfo != nil {
		if langType == otherInfo.GetNativeLang() ||
			langType == otherInfo.GetTeachLang2() ||
			langType == otherInfo.GetTeachLang3() {
			setLLang := common.NewSet()
			setTLang := common.NewSet()
			if otherInfo.GetLearnLang1() != 0 {
				setLLang.Add(otherInfo.GetLearnLang1())
			}
			if otherInfo.GetLearnLang2() != 0 {
				setLLang.Add(otherInfo.GetLearnLang2())
			}
			if otherInfo.GetLearnLang3() != 0 {
				setLLang.Add(otherInfo.GetLearnLang3())
			}

			if selfInfo.GetNativeLang() != 0 {
				setTLang.Add(selfInfo.GetNativeLang())
			}

			if selfInfo.GetTeachLang2() != 0 {
				setTLang.Add(selfInfo.GetTeachLang2())
			}

			if selfInfo.GetTeachLang3() != 0 {
				setTLang.Add(selfInfo.GetTeachLang3())
			}
			lLangSlic := setLLang.List()
			for _, v := range lLangSlic {
				if setTLang.Has(v) {
					valid = true // 只要Target的学习语言跟self的教学语言有一项匹配即可
					break
				}
			}
		} else {
			infoLog.Printf("IsLangMatch langType=%v native=%v t2=%v t3=%v do not match",
				langType,
				otherInfo.GetNativeLang(),
				otherInfo.GetTeachLang2(),
				otherInfo.GetTeachLang3())
			valid = false
		}
	} else {
		valid = false
	}
	return valid
}

// 5.proc history mnt service
func ProcHisMntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcHisMntService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcHisMntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/get_his_mnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/get_his_mnt_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcHisMntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetViewHistoryMntIdReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcHisMntService GetViewHistoryMntIdReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uid := subReqBody.GetUserid()
	qtype := subReqBody.GetQtype()
	langType := subReqBody.GetLangType()
	infoLog.Printf("ProcHisMntService reqUid=%v uid=%v qtype=%v", head.Uid, uid, qtype)
	var buckets []*util.BucketStr
	selfMidMap := make(map[string]bool)
	followerMidmap := make(map[string]bool)
	var idList []*ht_moment.MomentIdResult
	// first get all bucket mids
	bucketSlic := subReqBody.GetBucket().GetBucketList()
	if len(bucketSlic) == 0 {
		infoLog.Printf("ProcHisMntService bucketList empty failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param err"),
			},
		}
		return false
	}
	switch qtype {
	case ht_moment.QUERY_TYPE_DEFAULT:
		for _, v := range bucketSlic {
			lastIndex := v.GetIndex()
			bucketName := v.GetBucketName()
			// 如果lastIndex 为 0 说明这个桶已经取完了直接continue
			if lastIndex == 0 {
				infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v continue", bucketName, lastIndex)
				continue
			}
			// lastIndex = lastIndex - 1 // 这个不是已经取回去的最大SeqId，当客户端拉取最新的时候，如果没有取到这个桶会自动将其初始化为最大的SeqId 但是这个贴文其实没有取回去过
			if lastIndex == util.BaseMntIndex {
				infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v bucket empty", bucketName, lastIndex)
				continue
			}
			infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v", bucketName, lastIndex)
			bucketItem, err := proSyncReq.GetBucketHisList(head, string(bucketName), lastIndex, util.DefaultMidPageSize*2)
			if err != nil {
				infoLog.Printf("ProcGetMntService proSyncReq.GetBucketHisList uid=%v hashName=%s failed err=%s", uid, bucketName, err)
				attr := "gomntlogic/get_his_default_mnt_err"
				libcomm.AttrAdd(attr, 1)
			} else {
				// add bucket to list
				buckets = append(buckets, bucketItem)
				if strings.HasSuffix(string(bucketName), "#LIST") {
					// add self mid list
					for i, _ := range bucketItem.MidMap {
						// infoLog.Printf("ProcGetMntService self bucket list index=%v midStr=%#v", i, *v)
						selfMidMap[i] = true
					}
				} else if strings.HasSuffix(string(bucketName), "#FRIEND") {
					for i, _ := range bucketItem.MidMap {
						// infoLog.Printf("ProcGetMntService follower bucket list index=%v midStr=%#v", i, *v)
						followerMidmap[i] = true
					}
				}
			}
		}
	case ht_moment.QUERY_TYPE_PARTNERS:
		for _, v := range bucketSlic {
			lastIndex := v.GetIndex()
			bucketName := v.GetBucketName()
			// infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v", bucketName, lastIndex)
			// 如果lastIndex 为 0 说明这个桶已经取完了直接continue
			if lastIndex == 0 {
				infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v continue", bucketName, lastIndex)
				continue
			}
			// lastIndex = lastIndex - 1 // 这个不是已经取回去的最大SeqId，当客户端拉取最新的时候，如果没有取到这个桶会自动将其初始化为最大的SeqId 但是这个贴文其实没有取回去过
			if lastIndex == util.BaseMntIndex {
				infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v bucket empty", bucketName, lastIndex)
				continue
			}
			bucketItem, err := proSyncReq.GetBucketHisList(head, string(bucketName), lastIndex, util.DefaultMidPageSize)
			if err != nil {
				infoLog.Printf("ProcGetMntService proSyncReq.GetBucketHisList uid=%v hashName=%s failed err=%s", uid, bucketName, err)
				attr := "gomntlogic/get_his_part_mnt_err"
				libcomm.AttrAdd(attr, 1)
			} else {
				// add bucket to list
				buckets = append(buckets, bucketItem)
				infoLog.Printf("ProcGetMntService partners hashName=%s seq=%v size=%v",
					bucketItem.Name,
					bucketItem.Seq,
					bucketItem.Size)
			}
		}
	case ht_moment.QUERY_TYPE_USER:
		lastIndex := bucketSlic[0].GetIndex()
		bucketName := fmt.Sprintf("%v#LIST", uid)
		// infoLog.Printf("ProcHisMntService hashName=%s lastIndex=%v", bucketName, lastIndex)
		// 如果lastIndex 为 0 说明这个桶已经取完了直接continue
		if lastIndex == 0 {
			infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v continue", bucketName, lastIndex)
			result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
			rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("success"),
				},
				Bucket: &ht_moment.BucketInfo{
					BucketList: []*ht_moment.BucketBody{
						&ht_moment.BucketBody{
							BucketName: []byte(bucketName),
							Index:      proto.Uint64(0),
						},
					},
				},
				IdList: nil,
			}
			return true
		}

		bucketItem, outIndex, err := proSyncReq.GetBucketHisListAndIndex(head, bucketName, lastIndex, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcHisMntService proSyncReq.GetBucketHisListAndIndex failed uid=%v bucketName=%s lastIndex=%v",
				head.Uid,
				bucketName,
				lastIndex)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal err"),
				},
			}
			return false
		} else {
			// 遍历所有的mid列表 返回响应
			//将所有的mid按照从大到小逆序排序
			var mids []string
			for k, _ := range bucketItem.MidMap {
				if k == "1" {
					continue
				}
				mids = append(mids, k)
			}
			sort.Sort(sort.Reverse(sort.StringSlice(mids)))
			for _, v := range mids {
				if v == "1" {
					continue
				}
				midResult, err := proSyncReq.GetMidInfo(head, v)
				if err != nil {
					// add static
					attr := "gomntlogic/get_his_user_mid_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ProcHisMntService proSyncReq.GetMidInfo mid=%v err=%s", v, err)
					continue
				}
				if head.Uid == uid { // 拉取自己profile取自己发布所有帖子
					if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_HIDE) ||
						midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
						midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
					}
				} else { // 查看其他人的profile 取他人发布的所有帖子
					if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
						midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
					}
				}
				idList = append(idList, midResult)
			}

			result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
			rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("success"),
				},
				Bucket: &ht_moment.BucketInfo{
					BucketList: []*ht_moment.BucketBody{
						&ht_moment.BucketBody{
							BucketName: []byte(bucketName),
							Index:      proto.Uint64(outIndex),
						},
					},
				},
				IdList: idList,
			}
			return true
		}
	case ht_moment.QUERY_TYPE_CLASSMATE:
		fallthrough
	case ht_moment.QUERY_TYPE_LEARN:
		for _, v := range bucketSlic {
			lastIndex := v.GetIndex()
			bucketName := v.GetBucketName()
			// infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v", bucketName, lastIndex)
			// 如果lastIndex 为 0 说明这个桶已经取完了直接continue
			if lastIndex == 0 {
				infoLog.Printf("ProcHisMntService bucketName=%s lastIndex=%v continue", bucketName, lastIndex)
				continue
			}
			bucketItem, err := proSyncReq.GetBucketHisList(head, string(bucketName), lastIndex, util.DefaultMidPageSize)
			if err != nil {
				infoLog.Printf("ProcHisMntService proSyncReq.GetBucketHisList uid=%v hashName=%s failed err=%s", uid, bucketName, err)
				attr := "gomntlogic/get_his_classmate_mnt_err"
				libcomm.AttrAdd(attr, 1)
			} else {
				// add bucket to list
				buckets = append(buckets, bucketItem)
			}
		}
	}
	if len(buckets) == 0 {
		// add static
		attr := "gomntlogic/get_his_mnt_bucket_empty"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcHisMntService buckets empty uid=%v qtype=%v", uid, qtype)
		result = uint16(ht_moment.RET_CODE_RET_NO_MORE_CONTENT)
		rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("mid list empty"),
			},
		}
		return true
	}
	// Step2: get all userid
	uidSet := common.NewSet()
	// first add self
	uidSet.Add(uid)
	// second add moment relevent uid
	for _, v := range buckets {
		tempMidMap := make(map[string]*util.MidInfoStr)
		for k, t := range v.MidMap {
			creater, err := proSyncReq.GetMidCreateUser(head, k)
			if err != nil {
				// add static
				attr := "gomntlogic/get_mnt_creater_err"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcHisMntService proSyncReq.GetMidCreateUser get mid=%s err=%s", k, err)
				continue
			}
			t.Uid = creater
			tempMidMap[k] = t
			uidSet.Add(creater)
			// infoLog.Printf("ProcHisMntService hashName=%s mid=%s creater=%v", v.Name, k, creater)
		}
		v.MidMap = tempMidMap
	}

	// Step3: 查询帖文作者的UserCache 缓存 为后续过滤做准备
	uidSlic := uidSet.List()
	userInfo, err := userCacheApi.BatchQueryUserInfo(head.Uid, uidSlic)
	if err != nil {
		// add static
		attr := "gomntlogic/get_last_mnt_query_userinfo_err"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcHisMntService userCacheApi.BatchQueryUserInfo from=%v uidLis=%v err=%s", head.Uid, uidSlic, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// for i, v := range userInfo {
	// 	infoLog.Printf("ProcHisMntService userinfo index=%v uid=%v", i, v.GetUserID())
	// }

	// Step3: 查询自己的关注列表 head.Uid 才是真正协议发送者的uid
	selfFollowerList, err := relationApi.GetFollowingLists(head.Uid)
	if err != nil {
		// add static
		attr := "gomntlogic/get_his_mnt_get_following_list_err"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcHisMntService relationApi.GetFollowingLists from=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// infoLog.Printf("ProcLastMntService selfFollowerList=%v", selfFollowerList)
	// Step4: mid 过滤
	var pbBucktList []*ht_moment.BucketBody
	switch qtype {
	case ht_moment.QUERY_TYPE_DEFAULT:
		mids, lastMid, err := MigraHistoryBucketList(head, buckets, userInfo, selfFollowerList, head.Uid, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcHisMntService MigraBucketList reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/get_his_mnt_migra_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		// 如果mids的列表都为空 则将每个通的Seq都减少当前取的量
		if len(mids) == 0 {
			for _, v := range bucketSlic {
				var bucketSeq uint64
				if v.GetIndex() > uint64(util.DefaultMidPageSize*2+util.BaseMntIndex) {
					bucketSeq = v.GetIndex() - uint64(util.DefaultMidPageSize*2)
					infoLog.Printf("ProcHisMntService bucket name=%s originIndex=%v bucketSeq=%v desc", v.GetBucketName(), v.GetIndex(), bucketSeq)
				} else {
					bucketSeq = util.BaseMntIndex
				}
				bucketItem := &ht_moment.BucketBody{
					BucketName: v.GetBucketName(),
					Index:      proto.Uint64(bucketSeq),
				}
				pbBucktList = append(pbBucktList, bucketItem)
				infoLog.Printf("ProcHisMntService bucket name=%s bucketSeq=%v", v.GetBucketName(), bucketSeq)
			}
		} else {
			// for i, v := range mids {
			// 	infoLog.Printf("ProcHisMntService migra idex=%v mid=%s", i, v)
			// }
			for _, v := range buckets {
				var bucketSeq uint64
				if v.Seq == 0 {
					bucketSeq = uint64(v.Size) //如果这次下拉没有取到这个桶的内容，就将index设为桶的size
				} else {
					bucketSeq = v.Seq //否则设为该桶这次取到的id
				}
				bucketItem := &ht_moment.BucketBody{
					BucketName: []byte(v.Name),
					Index:      proto.Uint64(bucketSeq),
				}
				pbBucktList = append(pbBucktList, bucketItem)
				infoLog.Printf("ProcHisMntService bucket name=%s seq=%v size=%v bucketSeq=%v", v.Name, v.Seq, v.Size, bucketSeq)
			}
			totalCnt := 0
			//首先去掉重复的mid
			mids = RemoveDuplicateMid(mids)
			// mids is desc order
			//将所有的mid按照从大到小逆序排序
			sort.Sort(sort.Reverse(sort.StringSlice(mids)))
			for _, v := range mids {
				if v == "1" {
					infoLog.Printf("ProcHisMntService err mid=%s", v)
					continue
				}
				midResult, err := proSyncReq.GetMidInfo(head, v)
				if err != nil {
					// add static
					attr := "gomntlogic/get_last_mid_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ProcHisMntService proSyncReq.GetMidInfo mid=%v err=%s", v, err)
					continue
				}
				// 如果帖子当前的状态为OP_FOR_FOLLOWER_ONLY or OP_HIDE 且在自己发布的桶内这修复
				if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_HIDE) ||
					midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
					if _, ok := selfMidMap[v]; ok {
						midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
					}
				}
				// 如果当前帖子的状态为OP_FOR_FOLLOWER_ONLY 且在following通内这修复帖子的状态
				if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
					if _, ok := followerMidmap[v]; ok {
						midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
					}
				}

				idList = append(idList, midResult)
				totalCnt += 1
				if totalCnt >= util.DefaultMidPageSize || v == lastMid {
					break
				}
			}
		}

	case ht_moment.QUERY_TYPE_PARTNERS: // 对应客户端following 如果帖子当前的状态为OP_FOR_FOLLOWER_ONLY 需要进行修复
		mids, lastMid, err := MigraFollowerBucketList(head, buckets, userInfo, selfFollowerList, head.Uid, util.DefaultMidPageSize, langType)
		if err != nil {
			infoLog.Printf("ProcHisMntService MigraFollowerBucketList reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/get_last_mnt_migrafollower_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		for _, v := range buckets {
			var bucketSeq uint64
			if v.Seq == 0 {
				bucketSeq = uint64(v.Size) //如果这次下拉没有取到这个桶的内容，就将index设为桶的size
			} else {
				bucketSeq = v.Seq //否则设为该桶这次取到的id
			}
			bucketItem := &ht_moment.BucketBody{
				BucketName: []byte(v.Name),
				Index:      proto.Uint64(bucketSeq),
			}
			pbBucktList = append(pbBucktList, bucketItem)
			// infoLog.Printf("ProcHisMntService bucket name=%s seq=%v size=%v bucketSeq=%v", v.Name, v.Seq, v.Size, bucketSeq)
		}

		totalCnt := 0
		// mids is desc order
		//将所有的mid按照从大到小逆序排序
		sort.Sort(sort.Reverse(sort.StringSlice(mids)))
		for _, v := range mids {
			if v == "1" {
				infoLog.Printf("ProcHisMntService err mid=%s", v)
				continue
			}
			midResult, err := proSyncReq.GetMidInfo(head, v)
			if err != nil {
				// add static
				attr := "gomntlogic/get_last_mid_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcHisMntService proSyncReq.GetMidInfo mid=%v err=%s", v, err)
				continue
			}
			// 如果当前帖子的状态为OP_FOR_FOLLOWER_ONLY 且在following通内这修复帖子的状态
			if midResult.GetDeleted() == uint32(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) {
				midResult.Deleted = proto.Uint32(uint32(ht_moment.MntStatusOpType_OP_RESTORE))
			}

			idList = append(idList, midResult)
			totalCnt += 1
			if totalCnt >= util.DefaultMidPageSize || v == lastMid {
				break
			}
		}
	case ht_moment.QUERY_TYPE_CLASSMATE:
		mids, lastMid, err := MigraBucketList(head, buckets, userInfo, selfFollowerList, head.Uid, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcHisMntService MigraBucketList reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/get_his_mnt_migra_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		for _, v := range buckets {
			var bucketSeq uint64
			if v.Seq == 0 {
				bucketSeq = uint64(v.Size) //如果这次下拉没有取到这个桶的内容，就将index设为桶的size
			} else {
				bucketSeq = v.Seq //否则设为该桶这次取到的id
			}
			bucketItem := &ht_moment.BucketBody{
				BucketName: []byte(v.Name),
				Index:      proto.Uint64(bucketSeq),
			}
			pbBucktList = append(pbBucktList, bucketItem)
			// infoLog.Printf("ProcHisMntService bucket name=%s seq=%v size=%v bucketSeq=%v", v.Name, v.Seq, v.Size, bucketSeq)
		}
		totalCnt := 0
		//首先去掉重复的mid
		mids = RemoveDuplicateMid(mids)
		// mids is desc order
		//将所有的mid按照从大到小逆序排序
		sort.Sort(sort.Reverse(sort.StringSlice(mids)))
		for _, v := range mids {
			if v == "1" {
				infoLog.Printf("ProcLastMntService err mid=%s", v)
				continue
			}
			midResult, err := proSyncReq.GetMidInfo(head, v)
			if err != nil {
				// add static
				attr := "gomntlogic/get_his_mid_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcHisMntService proSyncReq.GetMidInfo mid=%v err=%s", v, err)
				continue
			}
			idList = append(idList, midResult)
			totalCnt += 1
			if totalCnt >= util.DefaultMidPageSize || v == lastMid {
				break
			}
		}
	case ht_moment.QUERY_TYPE_LEARN:
		mids, lastMid, err := MigraBucketListExcBlackAndAge(head, buckets, userInfo, head.Uid, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcHisMntService MigraBucketListExcBlackAndAge reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/get_his_mnt_migra_excblack_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		for _, v := range buckets {
			var bucketSeq uint64
			if v.Seq == 0 {
				bucketSeq = uint64(v.Size) //如果这次下拉没有取到这个桶的内容，就将index设为桶的size
			} else {
				bucketSeq = v.Seq //否则设为该桶这次取到的id
			}
			bucketItem := &ht_moment.BucketBody{
				BucketName: []byte(v.Name),
				Index:      proto.Uint64(bucketSeq),
			}
			pbBucktList = append(pbBucktList, bucketItem)
			// infoLog.Printf("ProcHisMntService bucket name=%s seq=%v size=%v bucketSeq=%v", v.Name, v.Seq, v.Size, bucketSeq)
		}
		totalCnt := 0
		//首先去掉重复的mid
		mids = RemoveDuplicateMid(mids)
		// mids is desc order
		//将所有的mid按照从大到小逆序排序
		sort.Sort(sort.Reverse(sort.StringSlice(mids)))
		for _, v := range mids {
			if v == "1" {
				infoLog.Printf("ProcHisMntService err mid=%s", v)
				continue
			}
			midResult, show, err := proSyncReq.GetMidInfoWithShow(head, v)
			if err != nil {
				// add static
				attr := "gomntlogic/get_his_mid_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcHisMntService proSyncReq.GetMidInfoWithShow mid=%v err=%s", v, err)
				continue
			}
			if gShowSwitch && ((show & uint64(ht_moment.MID_BUCKET_STATUS_BS_LEARN)) == uint64(ht_moment.MID_BUCKET_STATUS_BS_LEARN)) {
				infoLog.Printf("ProcHisMntService show=%v drop mid=%s", show, v)
				continue
			}
			idList = append(idList, midResult)
			totalCnt += 1
			if totalCnt >= util.DefaultMidPageSize || v == lastMid {
				break
			}
		}
	default:
		infoLog.Printf("ProcHisMntService uid=%v unknow query type=%v", uid, qtype)
	}
	// step4 :answer first
	hasMore := false

	for _, v := range pbBucktList {
		if v.GetIndex() != util.BaseMntIndex {
			hasMore = true
			infoLog.Printf("ProcHisMntService uid=%v name=%s index=%v no more", uid, v.GetBucketName(), v.GetIndex())
			break
		}
	}

	if !hasMore {
		result = uint16(ht_moment.RET_CODE_RET_NO_MORE_CONTENT)
		infoLog.Printf("ProcHisMntService uid=%v RET_CODE_RET_NO_MORE_CONTENT", uid)
	} else {
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	}
	rspBody.ViewHistoryMntIdRspbody = &ht_moment.ViewHistoryMomentIDRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Bucket: &ht_moment.BucketInfo{
			BucketList: pbBucktList,
		},
		IdList: idList,
	}

	for i, v := range pbBucktList {
		infoLog.Printf("ProcHisMntService index=%v bucket_name=%s bucket_index=%v",
			i,
			v.GetBucketName(),
			v.GetIndex())
	}

	for i, v := range idList {
		infoLog.Printf("ProcHisMntService uid=%v index=%v mid=%s deleted=%v", head.Uid, i, v.GetMid(), v.GetDeleted())
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/get_his_mnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 6.proc delete user service
func ProcDelUserService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcDelUserService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.DeleteUserRspbody = &ht_moment.DeleteUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelUserService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/del_user_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/del_user_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelUserService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DeleteUserRspbody = &ht_moment.DeleteUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDeleteUserReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelUserService GetDeleteUserReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DeleteUserRspbody = &ht_moment.DeleteUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	uidSlice := subReqBody.GetUserid()
	// infoLog.Printf("ProcDelUserService uid=%v del uidSlice=%v", head.Uid, uidSlice)
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_USER_ALL_MNT_STATUS_REQ)
	cacheReqBody := &ht_moment_cache.MntCacheReqBody{
		SetUserAllMntStatReqbody: &ht_moment_cache.SetUserAllMntStatReqBody{
			Userid: uidSlice,
			Stat:   proto.Uint32(uint32(ht_moment_cache.MntStatOpType_OP_DELETE_USER)), // 1: delete 0:restore
		},
	}
	sendPayLoad, err := proto.Marshal(cacheReqBody)
	if err != nil {
		infoLog.Printf("ProcDelUserService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DeleteUserRspbody = &ht_moment.DeleteUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcDelUserService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DeleteUserRspbody = &ht_moment.DeleteUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/del_user_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcDelUserService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DeleteUserRspbody = &ht_moment.DeleteUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/del_user_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcDelUserService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DeleteUserRspbody = &ht_moment.DeleteUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/del_user_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// infoLog.Printf("ProcDelUserService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = dbRespHead.Ret
	rspBody.DeleteUserRspbody = &ht_moment.DeleteUserRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/del_user_inter_err"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 7.proc restore user service
func ProcRestoreUserService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcRestoreUserService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.RestoreUserRspbody = &ht_moment.RestoreUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcRestoreUserService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/restore_user_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/restore_user_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcRestoreUserService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.RestoreUserRspbody = &ht_moment.RestoreUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetRestoreUserReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcRestoreUserService GetRestoreUserReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.RestoreUserRspbody = &ht_moment.RestoreUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	uidSlice := subReqBody.GetUserid()
	// infoLog.Printf("ProcRestoreUserService uid=%v restore uidSlice=%v", head.Uid, uidSlice)
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_USER_ALL_MNT_STATUS_REQ)
	cacheReqBody := &ht_moment_cache.MntCacheReqBody{
		SetUserAllMntStatReqbody: &ht_moment_cache.SetUserAllMntStatReqBody{
			Userid: uidSlice,
			Stat:   proto.Uint32(0),
		},
	}
	sendPayLoad, err := proto.Marshal(cacheReqBody)
	if err != nil {
		infoLog.Printf("ProcRestoreUserService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.RestoreUserRspbody = &ht_moment.RestoreUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcRestoreUserService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.RestoreUserRspbody = &ht_moment.RestoreUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/restore_user_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcRestoreUserService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.RestoreUserRspbody = &ht_moment.RestoreUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/restore_user_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcRestoreUserService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.RestoreUserRspbody = &ht_moment.RestoreUserRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/restore_user_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// infoLog.Printf("ProcRestoreUserService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = dbRespHead.Ret
	rspBody.RestoreUserRspbody = &ht_moment.RestoreUserRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/restore_user_inter_err"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 8.proc modify moment status service
func ProcModMntStatusService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModMntStatusService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment.ModMntStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModMntStatusService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/mod_mnt_stat_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/mod_mnt_stat_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModMntStatusService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment.ModMntStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetModMntStatusReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcModMntStatusService GetRestoreUserReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment.ModMntStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	mid := subReqBody.GetMid()
	opType := subReqBody.GetOpType()
	opReason := subReqBody.GetOpReason()
	infoLog.Printf("ProcModMntStatusService uid=%v mid=%s opType=%v opReason=%s", head.Uid, mid, opType, opReason)
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_MNT_STATUS_REQ)
	cacheReqBody := &ht_moment_cache.MntCacheReqBody{
		ModMntStatusReqbody: &ht_moment_cache.ModMntStatusReqBody{
			Mid:      mid,
			OpType:   ht_moment_cache.MntStatOpType(opType).Enum(),
			OpReason: opReason,
		},
	}
	sendPayLoad, err := proto.Marshal(cacheReqBody)
	if err != nil {
		infoLog.Printf("ProcModMntStatusService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment.ModMntStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcModMntStatusService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment.ModMntStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcModMntStatusService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment.ModMntStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcModMntStatusService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment.ModMntStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// infoLog.Printf("ProcModMntStatusService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = dbRespHead.Ret
	rspBody.ModMntStatusRspbody = &ht_moment.ModMntStatusRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step4 first get moment creater
	// creater, err := proSyncReq.GetMidCreateUser(head, string(mid))
	// if err != nil {
	// 	infoLog.Printf("ProcModMntStatusService GetMidCreateUser failed mid=%s err=%s", mid, err)
	// 	attr := "gomntlogic/get_mid_creater_failed"
	// 	libcomm.AttrAdd(attr, 1)
	// } else {
	// // 异步发送不需要回包
	// if cmntCacheApi != nil && creater != 0 {
	// 	err = cmntCacheApi.ClearUserInfoCache(creater)
	// 	if err != nil {
	// 		infoLog.Printf("ProcModMntStatusService cmntCacheApi.ClearUserInfoCache uid=%v failed err=%s", creater, err)
	// 		// add static
	// 		attr := "gomntlogic/clear_user_info_cache_failed"
	// 		libcomm.AttrAdd(attr, 1)
	// 	}
	// }
	// }

	eventType := NSQ_EVENT_OP_RESTORE
	if opType == ht_moment.MntStatusOpType_OP_RESTORE {
		eventType = NSQ_EVENT_OP_RESTORE
	} else if opType == ht_moment.MntStatusOpType_OP_HIDE {
		eventType = NSQ_EVENT_OP_HIDE
	} else if opType == ht_moment.MntStatusOpType_OP_DELETE {
		eventType = NSQ_EVENT_OP_DELETE
	} else if opType == ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY {
		eventType = NSQ_EVENT_OP_FOLLOWER_ONLY
	}
	err = PublishPostEvent(head.Uid, string(mid), eventType)
	if err != nil {
		// add static
		attr := "gomntlogic/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcModMntStatusService PublishPostEvent uid=%v mid=%s eventType=%v failed",
			head.Uid,
			mid,
			eventType)
	}
	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 9.proc post ad moment
func ProcPostAdMntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcPostAdMntService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcPostAdMntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/post_ad_mnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/post_ad_mnt_count"
	libcomm.AttrAdd(attr, 1)

	// Step1 检查用户是否已被删除
	ret, err := IsDeletedUser(head.Uid)
	if err != nil || ret == true {
		infoLog.Printf("ProcPostAdMntService IsDeletedUser err=%s ret=%v", err, ret)
		// add static
		attr := "gomntlogic/post_ad_mnt_no_auth_count"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment.RET_CODE_RET_NO_AUTH)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("no auth user"),
			},
		}
		return false
	}

	reqBody := new(ht_moment.ReqBody)
	err = proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcPostAdMntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetPostMntReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcPostAdMntService GetPostMntReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	// Step2: 产生mid
	mid := libgenmid.GenMid()
	infoLog.Printf("ProcPostAdMntService uid=%v mid=%v", head.Uid, mid)
	midStr := fmt.Sprintf("%v", mid)
	subReqBody.Moment.Mid = []byte(midStr)

	// Step3: 产生key-value 并更新dbd
	storeBody, err := util.MntReqBodyToStoreBody(subReqBody.GetMoment())
	if err != nil {
		infoLog.Printf("ProcPostAdMntService MntReqBodyToStoreBody err=%s uid=%v cmd=0x%4x seq=%v", err, head.Uid, head.Cmd, head.Seq)
		// add static
		attr := "gomntlogic/ad_mnt_to_store_body_failed"
		libcomm.AttrAdd(attr, 1)

		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change to store body failed"),
			},
		}
		return false
	}
	pbsBuf, err := proto.Marshal(storeBody)
	if err != nil {
		infoLog.Printf("ProcPostAdMntService proto.Marshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	var keyValue []string
	// add content
	keyValue = append(keyValue, util.MntContentKey)
	keyValue = append(keyValue, string(pbsBuf))

	keyValue = append(keyValue, util.MntDeletedKey)
	keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_RESTORE))

	// add operator reason
	keyValue = append(keyValue, util.MntOpReasonKey)
	keyValue = append(keyValue, "")

	// add liked count filed
	keyValue = append(keyValue, util.LikedCountKey)
	keyValue = append(keyValue, "0")

	// add liked ts filed
	keyValue = append(keyValue, util.LikedTsKey)
	keyValue = append(keyValue, "0")

	// add comment count filed
	keyValue = append(keyValue, util.CommentCountKey)
	keyValue = append(keyValue, "0")

	// add comment ts filed
	keyValue = append(keyValue, util.CommentTsKey)
	keyValue = append(keyValue, "0")

	// add total comment count filed
	keyValue = append(keyValue, util.TotalCmntCountKey)
	keyValue = append(keyValue, "0")

	// add show filed
	keyValue = append(keyValue, util.ShowFieldKey)
	keyValue = append(keyValue, "0")

	// 将创建者插入到moment的详细内容中
	keyValue = append(keyValue, util.MomentCreater)
	keyValue = append(keyValue, fmt.Sprintf("%v", head.Uid))

	dbRet, err := UpdateMidHashMapInDb(head, midStr, keyValue)
	if err != nil || dbRet != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("ProcPostAdMntService uid=%v mid=%s err=%s ret=%v", head.Uid, midStr, err, dbRet)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// step4 :answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.PostMntRspbody = &ht_moment.PostMomentRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("post mnt success"),
		},
		Mid:      []byte(midStr),
		PostTime: proto.Uint64(uint64(time.Now().UnixNano()) / 1000000),
	}
	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_ad_mnt_proc_slow"
		libcomm.AttrAdd(attr, 1)

	}
	infoLog.Printf("ProcPostAdMntService uid=%v mid=%v procTime=%v", head.Uid, mid, procTime)
	return true
}

// 10.proc operator uid service
func ProcOperatorUidService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcOperatorUidService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.OpUidRspbody = &ht_moment.OperatorUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcOperatorUidService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/op_uid_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/op_uid_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcOperatorUidService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.OpUidRspbody = &ht_moment.OperatorUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetOpUidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcOperatorUidService GetOpUidReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.OpUidRspbody = &ht_moment.OperatorUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	listType := subReqBody.GetListType() // 列表类型
	opType := subReqBody.GetOpType()     // 操作类型
	opUid := subReqBody.GetOpUserId()    // 被操作的uid
	selfUid := subReqBody.GetUserid()    // 我的uid
	infoLog.Printf("ProcOperatorUidService uid=%v listType=%v opType=%v opUid=%v targetUid=%v",
		head.Uid,
		listType,
		opType,
		opUid,
		selfUid)
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_OP_UID_REQ)
	cacheReqBody := &ht_moment_cache.MntCacheReqBody{
		OpUidReqbody: &ht_moment_cache.OpUidReqBody{
			ListType: ht_moment_cache.OpList(listType).Enum(),
			OpType:   ht_moment_cache.OpType(opType).Enum(),
			OpUserId: proto.Uint32(opUid),
			Userid:   proto.Uint32(selfUid),
		},
	}
	sendPayLoad, err := proto.Marshal(cacheReqBody)
	if err != nil {
		infoLog.Printf("ProcOperatorUidService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.OpUidRspbody = &ht_moment.OperatorUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcOperatorUidService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.OpUidRspbody = &ht_moment.OperatorUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcOperatorUidService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.OpUidRspbody = &ht_moment.OperatorUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcOperatorUidService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.OpUidRspbody = &ht_moment.OperatorUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// infoLog.Printf("ProcOperatorUidService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = dbRespHead.Ret
	rspBody.OpUidRspbody = &ht_moment.OperatorUidRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ListType: listType.Enum(),
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 11.proc get uid list
func ProcGetUidListService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUidListService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUidListService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/get_uid_list_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/get_uid_list_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUidListService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetOpUidListReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUidListService GetGetOpUidListReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	listType := subReqBody.GetListType()
	hideListVer := subReqBody.GetHideListVer()
	notShareVer := subReqBody.GetNotShareVer()
	uid := subReqBody.GetUserid()
	infoLog.Printf("ProcGetUidListService from=%v listType=%v hideListVer=%v notShareVer=%v uid=%v",
		head.Uid,
		listType,
		hideListVer,
		notShareVer,
		uid)
	var mapHideList map[uint32]uint32
	var mapForbitList map[uint32]uint32
	var outListVer uint32
	switch listType {
	case ht_moment.OPERATORLIST_OP_HIDE_LIST:
		outListVer, mapHideList, err = proSyncReq.GetHideOtherMntUidList(head, uid, notShareVer)
		if err != nil {
			infoLog.Printf("ProcGetUidListService GetHideOtherMntUidList() failed uid=%v", head.Uid)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		listRes := uint32(ht_moment.RET_CODE_RET_SUCCESS)
		if notShareVer == outListVer {
			listRes = uint32(ht_moment.RET_CODE_RET_NOT_CHANGE)
		}
		uidList := []*ht_moment.OpUid{}
		for i, v := range mapHideList {
			if i != util.SpecialUid && i != util.VersionFieldInit {
				uidList = append(uidList, &ht_moment.OpUid{
					Uid:         proto.Uint32(i),
					OpTimeStamp: proto.Uint32(v),
				})
			} else {
				infoLog.Printf("ProcGetUidListService hide i=%v v=%v", i, v)
			}
		}
		// step4 :answer first
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			ListType: listType.Enum(),
			HideUidArray: &ht_moment.OpUidList{
				ListVer:  proto.Uint32(outListVer),
				ListRes:  proto.Uint32(listRes),
				UidArray: uidList,
			},
			NotShareArray: nil,
		}
	case ht_moment.OPERATORLIST_OP_NOT_SHARE_LIST:
		outListVer, mapForbitList, err = proSyncReq.GetNotShareToUidList(head, uid, notShareVer)
		if err != nil {
			infoLog.Printf("ProcGetUidListService proSyncReq.GetNotShareToUidList failed uid=%v", head.Uid)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		listRes := uint32(ht_moment.RET_CODE_RET_SUCCESS)
		if notShareVer == outListVer {
			listRes = uint32(ht_moment.RET_CODE_RET_NOT_CHANGE)
		}
		var uidList []*ht_moment.OpUid
		for i, v := range mapForbitList {
			if i != util.SpecialUid && i != util.VersionFieldInit {
				item := &ht_moment.OpUid{
					Uid:         proto.Uint32(i),
					OpTimeStamp: proto.Uint32(v),
				}
				uidList = append(uidList, item)
			} else {
				infoLog.Printf("ProcGetUidListService not share i=%v v=%v", i, v)
			}
		}
		// for i, v := range uidList {
		// 	infoLog.Printf("ProcGetUidListService index=%v uid=%v time=%v", i, v.GetUid(), v.GetOpTimeStamp())
		// }
		// step4 :answer first
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			ListType:     listType.Enum(),
			HideUidArray: nil,
			NotShareArray: &ht_moment.OpUidList{
				ListVer:  proto.Uint32(outListVer),
				ListRes:  proto.Uint32(listRes),
				UidArray: uidList,
			},
		}
	case ht_moment.OPERATORLIST_OP_HIDE_AND_NOT_SHARE_LIST:
		hideVer, mapHideList, err := proSyncReq.GetHideOtherMntUidList(head, uid, notShareVer)
		if err != nil {
			infoLog.Printf("ProcGetUidListService GetHideOtherMntUidList() failed uid=%v", head.Uid)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}

		hideRes := uint32(ht_moment.RET_CODE_RET_SUCCESS)
		if notShareVer == hideVer {
			hideRes = uint32(ht_moment.RET_CODE_RET_NOT_CHANGE)
		}
		hideList := []*ht_moment.OpUid{}
		for i, v := range mapHideList {
			if i != util.SpecialUid && i != util.VersionFieldInit {
				hideList = append(hideList, &ht_moment.OpUid{
					Uid:         proto.Uint32(i),
					OpTimeStamp: proto.Uint32(v),
				})
			} else {
				infoLog.Printf("ProcGetUidListService i=%v v=%v", i, v)
			}
		}

		outNotShareVer, mapForbitList, err := proSyncReq.GetNotShareToUidList(head, uid, notShareVer)
		if err != nil {
			infoLog.Printf("ProcGetUidListService proSyncReq.GetNotShareToUidList failed uid=%v", head.Uid)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}

		notShareRes := uint32(ht_moment.RET_CODE_RET_SUCCESS)
		if notShareVer == outNotShareVer {
			notShareRes = uint32(ht_moment.RET_CODE_RET_NOT_CHANGE)
		}
		notShareList := []*ht_moment.OpUid{}
		for i, v := range mapForbitList {
			if i != util.SpecialUid && i != util.VersionFieldInit {
				notShareList = append(notShareList, &ht_moment.OpUid{
					Uid:         proto.Uint32(i),
					OpTimeStamp: proto.Uint32(v),
				})
			} else {
				infoLog.Printf("ProcGetUidListService not share i=%v v=%v", i, v)
			}
		}
		// step4 :answer first
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.GetOpUidListRspbody = &ht_moment.GetUidListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			ListType: listType.Enum(),
			HideUidArray: &ht_moment.OpUidList{
				ListVer:  proto.Uint32(hideVer),
				ListRes:  proto.Uint32(hideRes),
				UidArray: hideList,
			},
			NotShareArray: &ht_moment.OpUidList{
				ListVer:  proto.Uint32(notShareVer),
				ListRes:  proto.Uint32(notShareRes),
				UidArray: notShareList,
			},
		}

	default:
		infoLog.Printf("ProcGetUidListService from=%v listType=%v not handle", head.Uid, listType)
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/get_uid_list_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 12.proc backend operator uid service
func ProcBackEndOpUidService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBackEndOpUidService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.BackendOpUidRspbody = &ht_moment.BackEndOpUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBackEndOpUidService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/backend_op_uid_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/backned_op_uid_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBackEndOpUidService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.BackendOpUidRspbody = &ht_moment.BackEndOpUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBackendOpUidReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcBackEndOpUidService GetBackendOpUidReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.BackendOpUidRspbody = &ht_moment.BackEndOpUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	listType := subReqBody.GetListType()
	opType := subReqBody.GetOpType()
	opUid := subReqBody.GetOpUserId()
	targetUid := subReqBody.GetUserid()
	opReason := subReqBody.GetOpReason()
	infoLog.Printf("ProcBackEndOpUidService uid=%v listType=%v opType=%v opUid=%v targetUid=%v opReason=%s",
		head.Uid,
		listType,
		opType,
		opUid,
		targetUid,
		opReason)
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_BACK_END_OP_UID_REQ)
	backEndReqBody := &ht_moment_cache.MntCacheReqBody{
		BackEndOpUidReqbody: &ht_moment_cache.BackEndOpUidReqBody{
			ListType: ht_moment_cache.OpList(listType).Enum(),
			OpType:   ht_moment_cache.OpType(opType).Enum(),
			OpUserId: proto.Uint32(opUid),
			Userid:   proto.Uint32(targetUid),
			OpReason: opReason,
		},
	}
	sendPayLoad, err := proto.Marshal(backEndReqBody)
	if err != nil {
		infoLog.Printf("ProcBackEndOpUidService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.BackendOpUidRspbody = &ht_moment.BackEndOpUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcBackEndOpUidService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.BackendOpUidRspbody = &ht_moment.BackEndOpUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/backned_op_uid_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcBackEndOpUidService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.BackendOpUidRspbody = &ht_moment.BackEndOpUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/backned_op_uid_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcBackEndOpUidService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.BackendOpUidRspbody = &ht_moment.BackEndOpUidRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/backned_op_uid_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// infoLog.Printf("ProcBackEndOpUidService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = dbRespHead.Ret
	rspBody.BackendOpUidRspbody = &ht_moment.BackEndOpUidRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("recv db rsp success"),
		},
		ListType: listType.Enum(),
	}

	// 异步发送不需要回包
	// if cmntCacheApi != nil && opUid != 0 {
	// 	err = cmntCacheApi.ClearUserInfoCache(opUid)
	// 	if err != nil {
	// 		infoLog.Printf("ProcBackEndOpUidService cmntCacheApi.ClearUserInfoCache uid=%v failed err=%s", opUid, err)
	// 		// add static
	// 		attr := "gomntlogic/clear_user_info_cache_failed"
	// 		libcomm.AttrAdd(attr, 1)
	// 	}
	// }

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/backned_op_uid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 13.proc query be block service
func ProcQueryBeBlockedService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryBeBlockedService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryBeBlockedService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/query_be_block_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/query_be_block_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryBeBlockedService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryBeblockedReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryBeBlockedService GetQueryBeblockedReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	fromId := subReqBody.GetFromUid()
	targetId := subReqBody.GetTargetUid()
	outListVer, mapNotShareList, err := proSyncReq.GetNotShareToUidList(head, targetId, 0)
	if err != nil {
		infoLog.Printf("ProcQueryBeBlockedService proSyncReq.GetNotShareToUidList failed uid=%v err=%s", targetId, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcQueryBeBlockedService targetId=%v outListVer=%v", targetId, outListVer)
	beBlock := 0 // 默认可以查看帖文
	if _, ok := mapNotShareList[fromId]; ok {
		beBlock = 1 // 不让看
	}

	// step4 :answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("post mnt success"),
		},
		BeBlocked: proto.Uint32(uint32(beBlock)),
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_mnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 14.proc Query Reveal Entry ervice
func ProcQueryRevealEntryService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryRevealEntryService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.QueryRevealentryRspbody = &ht_moment.QueryRevealEntryRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryRevealEntryService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/query_reveal_entry_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/query_reveal_entry_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryRevealEntryService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.QueryRevealentryRspbody = &ht_moment.QueryRevealEntryRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryRevealentryReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryRevealEntryService GetQueryRevealentryReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.QueryRevealentryRspbody = &ht_moment.QueryRevealEntryRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	reqUid := subReqBody.GetReqUserId()
	qtype := subReqBody.GetQtype()
	infoLog.Printf("ProcQueryRevealEntryService reqUid=%v reqUid=%v qtype=%v", head.Uid, reqUid, qtype)

	var buckets []*util.BucketStr
	beReveal := 0 // 默认不展示
	switch qtype[0] {
	case ht_moment.QUERY_TYPE_LEARN:
		// 查询user cache 缓存
		_, _, _, learnLang, err := userCacheApi.QueryLangInfo(reqUid)
		if err != nil {
			infoLog.Printf("ProcQueryRevealEntryService userCacheApi.QueryTeachAndLearnLang failed uid=%v err=%s", reqUid, err)
			// add static
			attr := "gomntlogic/get_teach_and_learn_lang_faild"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.QueryRevealentryRspbody = &ht_moment.QueryRevealEntryRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		} else {
			if len(learnLang) == 0 {
				attr := "gomntlogic/get_teach_and_learn_lang_faild"
				libcomm.AttrAdd(attr, 1)
			}
			for _, v := range learnLang {
				if v == 0 {
					infoLog.Printf("ProcQueryRevealEntryService learn lang ==0")
					continue
				}
				hashName := fmt.Sprintf("%s%v", util.LearnLangBucketPrefix, v)
				// infoLog.Printf("ProcQueryRevealEntryService hashName=%s requid=%v qtype=%v", hashName, reqUid, qtype)
				learnBucket, err := proSyncReq.GetBucketList(head, hashName, util.DefaultMidPageSize)
				if err != nil {
					infoLog.Printf("ProcQueryRevealEntryService proSyncReq.GetBucketList uid=%v hashName=%s failed err=%s", reqUid, hashName, err)
					attr := "gomntlogic/query_reveal_entry_mnt_err"
					libcomm.AttrAdd(attr, 1)
				} else {
					// add bucket to list
					buckets = append(buckets, learnBucket)
				}
				// for i, v := range learnBucket.MidMap {
				// 	infoLog.Printf("ProcQueryRevealEntryService mid=%s uid=%v seq=%s", i, v.Uid, v.Seq)
				// }
			}
		}
	}
	if len(buckets) == 0 {
		// add static
		attr := "gomntlogic/query_reveal_entry_bucket_empty"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcQueryRevealEntryService buckets empty uid=%v qtype=%v", reqUid, qtype)
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.QueryRevealentryRspbody = &ht_moment.QueryRevealEntryRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("mid list empty"),
			},
			ListReveal: []*ht_moment.RevealBody{
				&ht_moment.RevealBody{
					Qtype:    qtype[0].Enum(),
					BeReveal: proto.Uint32(uint32(beReveal)), // 0:不展示 1:展示
				},
			},
		}
		return true
	}
	// Step2: get all userid
	var uidSlic []uint32
	// first add self
	uidSlic = append(uidSlic, reqUid)
	// second add moment relevent uid
	for _, v := range buckets {
		tempMidMap := make(map[string]*util.MidInfoStr)
		for k, t := range v.MidMap {
			creater, err := proSyncReq.GetMidCreateUser(head, k)
			if err != nil {
				// add static
				attr := "gomntlogic/get_mnt_creater_err"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcQueryRevealEntryService proSyncReq.GetMidCreateUser get mid=%s err=%s", k, err)
				continue
			}
			t.Uid = creater
			tempMidMap[k] = t
			uidSlic = append(uidSlic, creater)
		}
		v.MidMap = tempMidMap
	}

	// Step3: 查询帖文作者的UserCache 缓存 为后续过滤做准备
	userInfo, err := userCacheApi.BatchQueryUserInfo(head.Uid, uidSlic)
	if err != nil {
		// add static
		attr := "gomntlogic/query_reveal_entry_query_userinfo_err"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcQueryRevealEntryService userCacheApi.BatchQueryUserInfo from=%v uidLis=%v err=%s", head.Uid, uidSlic, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryRevealentryRspbody = &ht_moment.QueryRevealEntryRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// Step3: mid 过滤
	switch qtype[0] {
	case ht_moment.QUERY_TYPE_LEARN:
		mids, _, err := MigraBucketListExcBlackAndAge(head, buckets, userInfo, head.Uid, util.DefaultMidPageSize)
		if err != nil {
			infoLog.Printf("ProcQueryRevealEntryService MigraBucketListExcBlackAndAge reqUid=%v failed err=%s", head.Uid, err)
			attr := "gomntlogic/query_reveal_entry_migra_excblack_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.QueryRevealentryRspbody = &ht_moment.QueryRevealEntryRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
		// mids需要排序之后在判断
		// ****注意检查******
		sort.Sort(sort.Reverse(sort.StringSlice(mids)))
		if uint32(len(mids)) < gLearnLimit {
			beReveal = 0 // 不展示
		} else {
			isLeagalDate := IsLeagalDate(mids)
			infoLog.Printf("ProcQueryRevealEntryService isLeagalDate=%v midLen=%v gLearnLimit=%v", isLeagalDate, len(mids), gLearnLimit)
			if isLeagalDate {
				beReveal = 1 // 展示
			} else {
				beReveal = 0 // 不展示
			}
		}
	default:
		infoLog.Printf("ProcQueryRevealEntryService uid=%v unknow query type=%v", reqUid, qtype)
	}
	// step4 :answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.QueryRevealentryRspbody = &ht_moment.QueryRevealEntryRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ListReveal: []*ht_moment.RevealBody{
			&ht_moment.RevealBody{
				Qtype:    qtype[0].Enum(),
				BeReveal: proto.Uint32(uint32(beReveal)), // 0:不展示 1:展示
			},
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/query_reveal_entry_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func IsLeagalDate(mids []string) (ret bool) {
	if uint32(len(mids)) < gLearnLimit {
		ret = false
		return ret
	}
	index := int(gLearnLimit) - 1
	mid, err := strconv.ParseUint(mids[index], 10, 64)
	if err != nil {
		infoLog.Printf("IsLeagalDate mid=%s parse error=%s", mids[index], err)
		ret = false
		return ret
	}
	infoLog.Printf("IsLeagalDate index=%v midLen=%v mid=%v", index, len(mids), mid)
	seconds := mid >> 33 // mid left shirt 33 bit get second
	interval := time.Now().Unix() - int64(seconds)
	if interval > (int64(gLearnDay) * 3600 * 24) { // 大于所限制的天数 返回false
		ret = false
	} else { // < 说限制的天数这说明更新频繁 可以展示entry
		ret = true
	}
	return ret
}

// 15.proc modify show field service
func ProcModifyShowFieldService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModifyShowFieldService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModifyShowFieldService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/mid_show_field_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/mid_show_field_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModifyShowFieldService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetOpMidBucketStatusReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcModifyShowFieldService GetOpMidBucketStatusReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	uid := subReqBody.GetUserid()
	mid := subReqBody.GetMid()
	statSlic := subReqBody.GetState()
	opType := subReqBody.GetOpType()
	infoLog.Printf("ProcModifyShowFieldService head.uid=%v uid=%v mid=%s statSlic=%v opType=%v ",
		head.Uid,
		uid,
		mid,
		statSlic,
		opType)

	// 输入参数检查
	if uid == 0 || len(mid) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModifyShowFieldService invalid param uid=%v mid=%v", uid, mid)
		return false
	}
	midStat := make([]ht_moment_cache.MidBucketStatus, len(statSlic))
	for i, _ := range statSlic {
		midStat[i] = ht_moment_cache.MidBucketStatus(statSlic[i])
	}

	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_MNT_SHOW_FIELD_REQ)
	modMidReqBody := &ht_moment_cache.MntCacheReqBody{
		ModMidShowFieldReqbody: &ht_moment_cache.ModMidShowFieldReqBody{
			Userid: proto.Uint32(uid),
			Mid:    mid,
			State:  midStat,
			OpType: ht_moment_cache.OpType(opType).Enum(),
		},
	}
	sendPayLoad, err := proto.Marshal(modMidReqBody)
	if err != nil {
		infoLog.Printf("ProcModifyShowFieldService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// 发送请求到dbd进程
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcModifyShowFieldService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcModifyShowFieldService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcModifyShowFieldService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// infoLog.Printf("ProcModifyShowFieldService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	result = dbRespHead.Ret
	rspBody.OpMidBucketStatusRspbody = &ht_moment.OpMidBucketStatusRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("recv db rsp success"),
		},
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/mod_mnt_stat_inter_err"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 16.proc query index mnt service
func ProcQueryIndexMntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryIndexMntService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetUserIndexMomentRspbody = &ht_moment.GetUserIndexMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryIndexMntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/query_index_mnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/query_index_mnt_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryIndexMntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetUserIndexMomentRspbody = &ht_moment.GetUserIndexMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserIndexMomentReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryIndexMntService GetGetUserIndexMomentReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetUserIndexMomentRspbody = &ht_moment.GetUserIndexMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uid := subReqBody.GetUserid()
	userIndex := subReqBody.GetUserIndex()
	infoLog.Printf("ProcQueryIndexMntService uid=%v userIndexLen=%v", uid, len(userIndex))
	if len(userIndex) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.GetUserIndexMomentRspbody = &ht_moment.GetUserIndexMomentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("empty user index"),
			},
		}
		return true
	}

	var moment []*ht_moment.MomentBody
	for _, v := range userIndex {
		tempUid := v.GetUserid()
		tempSeq := v.GetSeq()
		infoLog.Printf("ProcQueryIndexMntService tempUid=%v tempSeq=%v", tempUid, tempSeq)
		hashMap := fmt.Sprintf("%v#LIST", tempUid)
		mid, err := proSyncReq.GetSpecificSeqMid(head, hashMap, uint32(tempSeq))
		if err != nil {
			infoLog.Printf("ProcQueryIndexMntService hashName=%s seq=%v err=%s", hashMap, tempSeq, err)
			continue
		}
		tempMomentBody, err := proSyncReq.GetMidContent(head, mid)
		if err != nil {
			infoLog.Printf("ProcQueryIndexMntService hashName=%s seq=%v mid=%s err=%s", hashMap, tempSeq, mid, err)
			continue
		}
		moment = append(moment, tempMomentBody)
	}

	// step4 :answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.GetUserIndexMomentRspbody = &ht_moment.GetUserIndexMomentRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Moment: moment,
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_mnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 17.proc post mod mnt bucket service
func ProcModMntBucketService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModMntBucketService not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModMntBucketService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/mod_mnt_bucket_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/mod_mnt_bucket_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetModMntBucketReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcModMntBucketService GetModMntBucketReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	mid := subReqBody.GetMid()
	bucketType := subReqBody.GetBucketType()
	opReason := subReqBody.GetOpReason()
	uid := subReqBody.GetMoment().GetUserid()
	infoLog.Printf("ProcModMntBucketService uid=%v mid=%s type=%v opReason=%s",
		uid,
		mid,
		bucketType,
		opReason)
	midStr := string(mid)
	// Step1: 首先获取帖子之前的deleted 状态
	keys := []string{
		util.MntDeletedKey,
	}
	values, err := proSyncReq.GetSpecificKeyInMidHashMap(head, midStr, keys)
	if err != nil || len(values) == 0 {
		// add static
		attr := "gomntlogic/get_mnt_count_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcModMntBucketService proSyncReq.GetSpecificKeyInMidHashMap hashmap=%s keys=%v err=%s valuesLen=%v", midStr, keys, err, len(values))
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get deleted failed"),
			},
		}
		return false
	}
	// Step2: 根据帖文的deleted 进行调整
	prevDeleted, err := strconv.ParseUint(values[0], 10, 32)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService strconv.ParseUint mid=%s deleted=%s err=%s", midStr, values[0], err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("parse uint failed"),
			},
		}
		return false
	}
	infoLog.Printf("ProcModMntBucketService mid=%s prevDeletd=%v", midStr, prevDeleted)
	if prevDeleted == uint64(ht_moment.MntStatusOpType_OP_HIDE) { // 如果帖文之前的状态为指给自己看 直接返回
		infoLog.Printf("ProcModMntBucketService mid=%s prevDeleted=%v only for me", midStr, prevDeleted)
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("only for me"),
			},
		}
		return true
	}

	if prevDeleted == uint64(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) &&
		bucketType == ht_moment.MntBucketType_INI_TO_ALL {
		infoLog.Printf("ProcModMntBucketService mid=%s prevDeleted=%v follower only type=%v need chage",
			midStr,
			prevDeleted,
			bucketType)
		bucketType = ht_moment.MntBucketType_INI_TO_FOWLLER
	}
	// Step3: 将特定字段写入mid 的 hashmap中
	var keyValue []string
	// add content
	keyValue = append(keyValue, util.MntOpReasonKey)
	keyValue = append(keyValue, string(opReason))
	if bucketType == ht_moment.MntBucketType_INI_TO_ME {
		keyValue = append(keyValue, util.BucketTypeKey)
		keyValue = append(keyValue, "0")
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_HIDE))
	} else if bucketType == ht_moment.MntBucketType_INI_TO_FOWLLER {
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY))
	} else if bucketType == ht_moment.MntBucketType_INI_TO_ALL {
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_RESTORE))
	} else {
		infoLog.Printf("ProcModMntBucketService uid=%v mid=%s type=%v opReason=%s unknow",
			head.Uid,
			midStr,
			bucketType,
			opReason)
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("unhandle bucket type"),
			},
		}
		return true
	}
	infoLog.Printf("ProcModMntBucketService mid=%s type=%v opReason=%s keyValue=%v",
		mid,
		bucketType,
		opReason,
		keyValue)
	dbRet, err := UpdateMidHashMapInDb(head, midStr, keyValue)
	if err != nil || dbRet != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("ProcModMntBucketService uid=%v mid=%s err=%s ret=%v", head.Uid, midStr, err, dbRet)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// Step4: 如果只展示给自己看 直接返回
	if bucketType == ht_moment.MntBucketType_INI_TO_ME {
		infoLog.Printf("ProcModMntBucketService mid=%s bucketType=%v opReason=%s",
			midStr,
			bucketType,
			opReason)
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("only for me"),
			},
		}
		return true
	}
	// step5:answer first
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.ModMntBucketRspbody = &ht_moment.ModMntBucketRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("post mnt success"),
		},
	}
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// step6:发送请求到dbd 做其他业务逻辑更新操作
	dbdRet, err := ModMntBucektToDbd(head, subReqBody)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService uid=%v ModMntBucektToDbd ret err=%s", head.Uid, err)
	}
	// infoLog.Printf("ProcModMntBucketService uid=%v ModMntBucektToDbd ret=%v", head.Uid, dbdRet)
	if dbdRet != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		// add static
		attr := "gomntlogic/mod_mnt_bucket_to_dbd_failed"
		libcomm.AttrAdd(attr, 1)
	}
	// 2017-11-03 发布NSQ事件到队列中
	eventType := NSQ_EVENT_BUCKET_TO_ME
	if bucketType == ht_moment.MntBucketType_INI_TO_ME {
		eventType = NSQ_EVENT_BUCKET_TO_ME
	} else if bucketType == ht_moment.MntBucketType_INI_TO_FOWLLER {
		eventType = NSQ_EVENT_BUCKET_TO_FOLLOW
	} else if bucketType == ht_moment.MntBucketType_INI_TO_ALL {
		eventType = NSQ_EVENT_BUCKET_TO_ALL
	}
	err = PublishPostEvent(uid, midStr, eventType)
	if err != nil {
		// add static
		attr := "gomntlogic/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcDelMnProcModMntBucketServicetService PublishPostEvent uid=%v mid=%s eventType=%v failed",
			uid,
			midStr,
			eventType)
	}

	// step6:更新完成之后删除用户的comment缓存贴子数、评论数
	// 异步发送不需要回包
	// if cmntCacheApi != nil && uid != 0 {
	// 	err = cmntCacheApi.ClearUserInfoCache(uid)
	// 	if err != nil {
	// 		infoLog.Printf("ProcModMntBucketService cmntCacheApi.ClearUserInfoCache uid=%v failed err=%s", head.Uid, err)
	// 		// add static
	// 		attr := "gomntlogic/clear_user_info_cache_failed"
	// 		libcomm.AttrAdd(attr, 1)
	// 	}
	// }
	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcModMntBucketService uid=%v mid=%s totalProcTime=%v", head.Uid, mid, totalProcTime)
	if totalProcTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/mod_mnt_bucket_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func ModMntBucektToDbd(head *common.HeadV2, subReqBody *ht_moment.ModMntBucketReqBody) (ret uint16, err error) {
	if head == nil || subReqBody == nil {
		err = util.ErrInvalidParam
		return ret, err
	}
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_MNT_BUCKET_REQ)
	// infoLog.Printf("ModMntBucektToDbd reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		PostModMntBucketReqbody: &ht_moment_cache.PostModMntBucketReqBody{
			Version:        proto.Uint32(0),
			Mid:            subReqBody.GetMid(),
			BucketType:     ht_moment_cache.PostMntBucketType(subReqBody.GetBucketType()).Enum(),
			ContentByteLen: proto.Uint32(uint32(len(subReqBody.GetMoment().GetContent()))),
			ContentWords:   proto.Uint32(uint32(Utf8StrLen(string(subReqBody.GetMoment().GetContent())))),
			StrLangType:    subReqBody.GetMoment().GetStrLangType(),
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ModMntBucektToDbd proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return ret, err
	}
	dbdPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("ModMntBucektToDbd dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return ret, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("ModMntBucektToDbd change packet failed uid=%v", reqHead.Uid)
		err = util.ErrChagePacket
		return ret, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		infoLog.Printf("ModMntBucektToDbd GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

// 18.proc post comment service
func ProcPostComntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
			infoLog.Printf("ProcPostComntService need not response")
		} else {
			infoLog.Printf("ProcPostComntService not need call")
		}
	}()
	//检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcPostComntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/post_cmnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/post_cmnt_req_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 解析请求的reqbody 进行参数检测
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcPostComntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetPostCmntReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcPostComntService GetPostCmntReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uid := head.Uid
	mid := subReqBody.GetMid()
	infoLog.Printf("ProcPostComntService uid=%v mid=%s", uid, mid)
	if uid == 0 || len(mid) == 0 {
		infoLog.Printf("ProcPostComntService uid=%v mid=%s input param error", uid, mid)
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: 获取帖子的创建者和帖文的状态
	keys := []string{
		util.MntContentKey,
		util.MntDeletedKey,
	}
	values, err := proSyncReq.GetSpecificKeyInMidHashMap(head, string(mid), keys)
	if err != nil || len(values) != 2 {
		infoLog.Printf("ProcPostComntService uid=%v mid=%s GetSpecificKeyInMidHashMap err=%s",
			uid,
			mid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcPostComntService uid=%v mid=%s keys=%v valuesLen=%v",
		uid,
		mid,
		keys,
		len(values))
	var currentDel uint64
	if len(values[1]) <= 0 {
		infoLog.Printf("ProcPostComntService uid=%v mid=%s keys=%v values=%v nil deleted",
			uid,
			mid,
			keys,
			values)
		attr := "gomntlogic/err_hash_map_count"
		libcomm.AttrAdd(attr, 1)
		currentDel = 0 // 设置当前帖子的删除状态为0
	} else {
		currentDel, err = strconv.ParseUint(values[1], 10, 64)
		if err != nil {
			infoLog.Printf("ProcPostComntService uid=%v mid=%s strconv.ParseUint failed err=%s",
				uid,
				mid,
				err)
			attr := "gomntlogic/err_hash_map_count"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}

	if currentDel == uint64(ht_moment.MntStatusOpType_OP_DELETE_BY_USER) ||
		currentDel == uint64(ht_moment.MntStatusOpType_OP_DELETE) {
		infoLog.Printf("ProcPostComntService uid=%v mid=%s keys=%v values=%v curDeletd=%v",
			uid,
			mid,
			keys,
			values,
			currentDel)
	}

	// 获取帖文的创建者
	storeMomentBody := new(ht_moment_store.StoreMomentBody)
	err = proto.Unmarshal([]byte(values[0]), storeMomentBody)
	if err != nil {
		infoLog.Printf("ProcPostComntService proto Unmarshal failed uid=%v mid=%s err=%s",
			uid,
			mid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	creater := storeMomentBody.GetUserid()

	// Step3: 查询帖文的创建者拉黑列表
	blackList, err := userCacheApi.QuerySingleBlackList(creater)
	if err != nil {
		infoLog.Printf("ProcPostComntService uid=%v mid=%s QuerySingleBlackList failed err=%s",
			uid,
			mid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// 如果评论者直接被帖文创建者拉黑直接返回禁止评论
	isBeenBlack := Uint32IsInSlice(blackList, uid)
	if isBeenBlack {
		infoLog.Printf("ProcPostComntService uid=%v mid=%s creater=%v isBeenBlack=%v",
			uid,
			mid,
			creater,
			isBeenBlack)
		result = uint16(ht_moment.RET_CODE_RET_FORBID_CMNT_WHEN_BLOCKED)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("forbid cmnt when blocked"),
			},
		}
		return false
	}
	var replyUidList []*ht_moment_cache.ReplyInfo
	replyList := subReqBody.GetCommentBody().GetToidList()
	for _, v := range replyList {
		item := &ht_moment_cache.ReplyInfo{
			Userid:   proto.Uint32(v.GetUserid()),
			Nickname: v.GetNickname(),
			UserType: proto.Uint32(v.GetUserType()),
			Pos:      proto.Uint32(v.GetPos()),
		}
		replyUidList = append(replyUidList, item)
	}
	//Step4: 产生评论内容
	storeBody, err := util.CmtReqBodyToStoreBody(subReqBody.GetCommentBody())
	if err != nil {
		infoLog.Printf("ProcPostComntService CmtReqBodyToStoreBody err=%s uid=%v cmd=0x%4x seq=%v", err, head.Uid, head.Cmd, head.Seq)
		// add static
		attr := "gomntlogic/cmnt_to_store_body_failed"
		libcomm.AttrAdd(attr, 1)

		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("change to store body failed"),
			},
		}
		return false
	}
	pbsBuf, err := proto.Marshal(storeBody)
	if err != nil {
		infoLog.Printf("ProcPostComntService proto.Marshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	reqHead := new(common.HeadV2)
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_POST_CMNT_REQ)
	postCmntReqBody := &ht_moment_cache.MntCacheReqBody{
		PostCmntReqbody: &ht_moment_cache.PostCmntReqBody{
			Mid:         subReqBody.GetMid(),
			MntCreater:  proto.Uint32(creater),
			MntDeleted:  proto.Uint32(uint32(currentDel)),
			CmntUid:     proto.Uint32(head.Uid),
			CmntContent: pbsBuf,
			CmntType:    ht_moment_cache.COMMENT_TYPE(subReqBody.GetCommentBody().GetCtype()).Enum(),
			CmntDeleted: proto.Uint32(subReqBody.GetCommentBody().GetDeleted()),
			ToidList:    replyUidList,
		},
	}
	sendPayLoad, err := proto.Marshal(postCmntReqBody)
	if err != nil {
		infoLog.Printf("ProcPostComntService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	//Step4: 发送请求到dbd进程进行下一步处理
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcPostComntService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcPostComntService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcPostComntService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// infoLog.Printf("ProcPostComntService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	if dbRespHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		result = dbRespHead.Ret
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), dbRspBody)
	if err != nil {
		infoLog.Printf("ProcPostComntService proto.Unmarshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	dbSubRspBody := dbRspBody.GetPostCmntRspbody()
	if dbSubRspBody == nil {
		infoLog.Printf("ProcPostComntService proto.Unmarshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	notifyList := []*ht_moment.NotifyInfo{}
	for _, v := range dbSubRspBody.GetNotifyList() {
		item := &ht_moment.NotifyInfo{
			Userid: proto.Uint32(v.GetUserid()),
			Type:   ht_moment.NOTIFY_TYPE(v.GetType()).Enum(),
		}
		notifyList = append(notifyList, item)
	}
	rspBody.PostCmntRspbody = &ht_moment.PostCommentRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Mid:        dbSubRspBody.GetMid(),
		Cid:        dbSubRspBody.GetCid(),
		PostTime:   proto.Uint64(dbSubRspBody.GetPostTime()),
		NotifyList: notifyList,
	}
	// 回响应
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 最后下发异步通知到p2pagent进程
	for _, v := range dbSubRspBody.GetNotifyList() {
		// infoLog.Printf("ProcPostComntService mid=%s notify index=%v notify uid=%v",
		// 	mid,
		// 	i,
		// 	v.GetTargetUid())
		pushType := 0
		switch v.GetType() {
		case ht_moment_cache.NOTIFY_TYPE_NOTIFY_CMNT:
			pushType = 20
		case ht_moment_cache.NOTIFY_TYPE_NOTIFY_CMNT_REPLY:
			pushType = 19
		case ht_moment_cache.NOTIFY_TYPE_NOTIFY_MODIFY:
			pushType = 21
		default:
			infoLog.Printf("ProcPostComntService mid=%s notifyType=%v unhandle", mid, v.GetType())
		}
		err = p2pAgentApi.SendPushMsg(head.Uid, v.GetTargetUid(), 1, uint32(pushType), 1)
		if err != nil {
			infoLog.Printf("ProcPostComntService mid=%s cmntUid=%v targetUid=%v notifyType=%v failed err=%s",
				mid,
				head.Uid,
				v.GetTargetUid(),
				pushType,
				err)
			attr := "gomntlogic/sendp2p_failed"
			libcomm.AttrAdd(attr, 1)
		}
	}
	// 发布nsq事件到话题topic
	eventType := NSQ_EVENT_COMMENT
	err = PublishPostEvent(head.Uid, string(mid), eventType)
	if err != nil {
		// add static
		attr := "gomntlogic/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcPostComntService PublishPostEvent uid=%v mid=%s eventType=%v failed",
			head.Uid,
			mid,
			eventType)
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_cmnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcPostComntService fromId=%v userId=%v procTime=%v", head.Uid, uid, procTime)
	return true
}

// 19.proc del comment
func ProcDelComntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
			infoLog.Printf("ProcDelComntService need not response")
		} else {
			infoLog.Printf("ProcDelComntService not need call")
		}
	}()
	//检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcDelComntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/del_cmnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/del_cmnt_req_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 解析请求的reqbody 进行参数检测
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcDelComntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetDelCmntReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcDelComntService GetDelCmntReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uid := head.Uid
	mid := subReqBody.GetMid()
	cid := subReqBody.GetCid()
	infoLog.Printf("ProcDelComntService uid=%v mid=%s cid=%s", uid, mid, cid)
	if uid == 0 || len(mid) == 0 || len(cid) == 0 {
		infoLog.Printf("ProcDelComntService uid=%v mid=%s cid=%s input param error", uid, mid, cid)
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	srvCid, err := GetCidFromClientCid(cid)
	if err != nil {
		infoLog.Printf("ProcDelComntService uid=%v mid=%s cid=%s GetCidFromClientCid failed",
			uid,
			mid,
			cid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step2: 获取评论的发布者
	keys := []string{
		srvCid,
	}
	values, err := proSyncReq.GetSpecificKeyInMidHashMap(head, string(mid), keys)
	if err != nil || len(values) != 1 {
		infoLog.Printf("ProcDelComntService uid=%v mid=%s cid=%s GetSpecificKeyInMidHashMap err=%s",
			uid,
			mid,
			cid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcDelComntService uid=%v mid=%s keys=%v values=%v",
		uid,
		mid,
		keys,
		values)

	storeCmntBody := new(ht_moment_store.StoreCommentBody)
	err = proto.Unmarshal([]byte(values[0]), storeCmntBody)
	if err != nil {
		infoLog.Printf("ProcDelComntService proto Unmarshal failed uid=%v mid=%s cid=%s err=%s",
			uid,
			mid,
			cid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	cmntUid := storeCmntBody.GetUserid()
	// Step3: 判断用户是否有权限修改
	if cmntUid != head.Uid {
		infoLog.Printf("uid=%v mid=%s cid=%s cmntUid=%v have no auth to delete the comment!",
			head.Uid,
			mid,
			cid,
			cmntUid)
		result = uint16(ht_moment.RET_CODE_RET_NO_AUTH)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("no auth user"),
			},
		}
		return false
	}

	// Step4: 判断是否已经删除
	if storeCmntBody.GetDeleted() > 0 {
		infoLog.Printf("uid=%v mid=%s cid=%s is already deleted, why again?",
			head.Uid,
			mid,
			cid)
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("already deleted"),
			},
		}
		return false
	}
	// Step4: 发送请求到dbd处理
	reqHead := new(common.HeadV2)
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_CMNT_STAT_REQ)
	modCmntStateReqBody := &ht_moment_cache.MntCacheReqBody{
		ModCmntStateReqbody: &ht_moment_cache.ModCmntStateReqBody{
			Userid: proto.Uint32(cmntUid),
			Mid:    mid,
			Cid:    []byte(srvCid),
			Ctype:  ht_moment_cache.COMMENT_TYPE(subReqBody.GetCtype()).Enum(),
			Stat:   ht_moment_cache.COMMENT_STAT_STAT_DEL.Enum(),
		},
	}
	sendPayLoad, err := proto.Marshal(modCmntStateReqBody)
	if err != nil {
		infoLog.Printf("ProcDelComntService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	//Step4: 发送请求到dbd进程进行下一步处理
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcDelComntService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcDelComntService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcDelComntService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcDelComntService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响
	dbRspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), dbRspBody)
	if err != nil {
		infoLog.Printf("ProcDelComntService proto.Unmarshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	dbSubRspBody := dbRspBody.GetModCmntStateRspbody()
	if dbSubRspBody == nil {
		infoLog.Printf("ProcDelComntService proto.Unmarshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}

	rspBody.DelCmntRspbody = &ht_moment.DeleteCommentRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(dbSubRspBody.GetStatus().GetCode()),
			Reason: dbSubRspBody.GetStatus().GetReason(),
		},
		Mid: mid,
		Cid: cid,
	}
	// 回响应
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 发布nsq事件到话题topic
	eventType := NSQ_EVENT_DELETE_COMMENT
	err = PublishPostEvent(head.Uid, string(mid), eventType)
	if err != nil {
		// add static
		attr := "gomntlogic/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcDelComntService PublishPostEvent uid=%v mid=%s eventType=%v failed",
			head.Uid,
			mid,
			eventType)
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_cmnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcDelComntService fromId=%v userId=%v procTime=%v", head.Uid, uid, procTime)
	return true
}

// 20.proc set comment status
func ProcSetComntStatService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
			infoLog.Printf("ProcSetComntStatService need not response")
		} else {
			infoLog.Printf("ProcSetComntStatService not need call")
		}
	}()
	//检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetComntStatService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/del_cmnt_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/del_cmnt_req_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 解析请求的reqbody 进行参数检测
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetComntStatService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetCommentStatReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSetComntStatService GetSetCommentStatReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uid := head.Uid
	mid := subReqBody.GetMid()
	cid := subReqBody.GetCid()
	ctype := subReqBody.GetCtype()
	stat := subReqBody.GetStatus()
	infoLog.Printf("ProcSetComntStatService uid=%v mid=%s cid=%s ctype=%v stat=%v",
		uid,
		mid,
		cid,
		ctype,
		stat)
	if uid == 0 || len(mid) == 0 || len(cid) == 0 {
		infoLog.Printf("ProcSetComntStatService uid=%v mid=%s cid=%s input param error", uid, mid, cid)
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}
	srvCid, err := GetCidFromClientCid(cid)
	if err != nil {
		infoLog.Printf("ProcSetComntStatService uid=%v mid=%s cid=%s GetCidFromClientCid failed",
			uid,
			mid,
			cid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step2: 发送请求到dbd处理
	reqHead := new(common.HeadV2)
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_CMNT_STAT_REQ)
	modCmntStateReqBody := &ht_moment_cache.MntCacheReqBody{
		ModCmntStateReqbody: &ht_moment_cache.ModCmntStateReqBody{
			Userid: proto.Uint32(uid),
			Mid:    mid,
			Cid:    []byte(srvCid),
			Ctype:  ht_moment_cache.COMMENT_TYPE(ctype).Enum(),
			Stat:   ht_moment_cache.COMMENT_STAT(stat).Enum(),
		},
	}
	sendPayLoad, err := proto.Marshal(modCmntStateReqBody)
	if err != nil {
		infoLog.Printf("ProcSetComntStatService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	//Step4: 发送请求到dbd进程进行下一步处理
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcSetComntStatService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcSetComntStatService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcSetComntStatService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	infoLog.Printf("ProcSetComntStatService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响
	dbRspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), dbRspBody)
	if err != nil {
		infoLog.Printf("ProcSetComntStatService proto.Unmarshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	dbSubRspBody := dbRspBody.GetModCmntStateRspbody()
	if dbSubRspBody == nil {
		infoLog.Printf("ProcSetComntStatService proto.Unmarshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}

	rspBody.SetCommentStatRspbody = &ht_moment.SetCommentStatRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(dbSubRspBody.GetStatus().GetCode()),
			Reason: dbSubRspBody.GetStatus().GetReason(),
		},
		Mid: mid,
		Cid: cid,
	}
	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_cmnt_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcSetComntStatService fromId=%v userId=%v procTime=%v", head.Uid, uid, procTime)
	return true
}

// 21.Post like service
func ProcPostLikeService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
			infoLog.Printf("ProcPostLikeService need not response")
		} else {
			infoLog.Printf("ProcPostLikeService not need call")
		}
	}()
	//检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcPostLikeService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/post_like_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/post_like_req_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 解析请求的reqbody 进行参数检测
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcPostLikeService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetPostLikeReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcPostLikeService GetPostLikeReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uid := head.Uid
	mid := subReqBody.GetMid()
	infoLog.Printf("ProcPostLikeService uid=%v mid=%s", uid, mid)
	if uid == 0 || len(mid) == 0 {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s input param error", uid, mid)
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	// Step2: 获取帖子的创建者和帖文的状态
	keys := []string{
		util.MntContentKey,
		util.MntDeletedKey,
	}
	values, err := proSyncReq.GetSpecificKeyInMidHashMap(head, string(mid), keys)
	if err != nil || len(values) != 2 {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s GetSpecificKeyInMidHashMap err=%s",
			uid,
			mid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcPostLikeService uid=%v mid=%s keys=%v valuesLen=%v",
		uid,
		mid,
		keys,
		len(values))
	var currentDel uint64
	if len(values[1]) <= 0 {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s keys=%v values=%v nil deleted",
			uid,
			mid,
			keys,
			values)
		attr := "gomntlogic/err_hash_map_count"
		libcomm.AttrAdd(attr, 1)
		currentDel = 0 // 设置当前帖子的删除状态为0
	} else {
		currentDel, err = strconv.ParseUint(values[1], 10, 64)
		if err != nil {
			infoLog.Printf("ProcPostLikeService uid=%v mid=%s strconv.ParseUint failed err=%s",
				uid,
				mid,
				err)
			attr := "gomntlogic/err_hash_map_count"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
			rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}
	//这里为了兼容客户端还要继续往下跑, 除了这个上面的都是异常，所以返回错误
	if currentDel == uint64(ht_moment.MntStatusOpType_OP_DELETE_BY_USER) ||
		currentDel == uint64(ht_moment.MntStatusOpType_OP_DELETE) {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s keys=%v values=%v curDeletd=%v",
			uid,
			mid,
			keys,
			values,
			currentDel)
	}

	// 获取帖文的创建者
	storeMomentBody := new(ht_moment_store.StoreMomentBody)
	err = proto.Unmarshal([]byte(values[0]), storeMomentBody)
	if err != nil {
		infoLog.Printf("ProcPostLikeService proto Unmarshal failed uid=%v mid=%s err=%s",
			uid,
			mid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	creater := storeMomentBody.GetUserid()

	// Step3: 查询帖文的创建者拉黑列表
	blackList, err := userCacheApi.QuerySingleBlackList(creater)
	if err != nil {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s QuerySingleBlackList failed err=%s",
			uid,
			mid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// 如果评论者直接被帖文创建者拉黑直接返回禁止评论
	isBeenBlack := Uint32IsInSlice(blackList, uid)
	if isBeenBlack {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s creater=%v isBeenBlack=%v",
			uid,
			mid,
			creater,
			isBeenBlack)
		result = uint16(ht_moment.RET_CODE_RET_FORBID_CMNT_WHEN_BLOCKED)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("forbid cmnt when blocked"),
			},
		}
		return false
	}
	reqHead := new(common.HeadV2)
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_POST_LIKE_REQ)
	postCmntReqBody := &ht_moment_cache.MntCacheReqBody{
		PostLikeReqbody: &ht_moment_cache.PostLikeReqBody{
			Userid:     proto.Uint32(subReqBody.GetUserid()),
			Mid:        subReqBody.GetMid(),
			LikeOrNot:  proto.Uint32(subReqBody.GetLikeOrNot()),
			MntCreater: proto.Uint32(creater),
			Delete:     proto.Uint32(uint32(currentDel)),
		},
	}
	sendPayLoad, err := proto.Marshal(postCmntReqBody)
	if err != nil {
		infoLog.Printf("ProcPostLikeService Failed to proto.Marshal uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	//Step4: 发送请求到dbd进程进行下一步处理
	dbPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, sendPayLoad)
	if err != nil {
		infoLog.Printf("ProcPostLikeService update db failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// clear user info cache
	// 异步发送不需要回包
	// if cmntCacheApi != nil && creater != 0 {
	// 	err = cmntCacheApi.ClearUserInfoCache(creater)
	// 	if err != nil {
	// 		infoLog.Printf("ProcPostLikeService cmntCacheApi.ClearUserInfoCache uid=%v failed err=%s", creater, err)
	// 		// add static
	// 		attr := "gomntlogic/clear_user_info_cache_failed"
	// 		libcomm.AttrAdd(attr, 1)
	// 	}
	// }

	dbPacketHeadV2, ok := dbPacket.(*common.HeadV2Packet)
	if !ok { // 不是HeadV2Packet报文
		infoLog.Printf("ProcPostLikeService dbpacket can not change to HeadV2packet uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRespHead, err := dbPacketHeadV2.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("ProcPostLikeService dbpacket Get head failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	// infoLog.Printf("ProcPostLikeService dbRespHead ret=%v", dbRespHead.Ret)
	// Step3 读取dbd之后立即返回响应
	if dbRespHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		result = dbRespHead.Ret
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		attr := "gomntlogic/post_cmnt_inter_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	dbRspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbPacketHeadV2.GetBody(), dbRspBody)
	if err != nil {
		infoLog.Printf("ProcPostLikeService proto.Unmarshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	dbSubRspBody := dbRspBody.GetPostLikeRspbody()
	if dbSubRspBody == nil {
		infoLog.Printf("ProcPostLikeService proto.Unmarshal(storeBody) failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto marshal failed"),
			},
		}
		return false
	}
	notifyList := []*ht_moment.NotifyInfo{}
	for _, v := range dbSubRspBody.GetNotifyList() {
		item := &ht_moment.NotifyInfo{
			Userid: proto.Uint32(v.GetUserid()),
			Type:   ht_moment.NOTIFY_TYPE(v.GetType()).Enum(),
		}
		notifyList = append(notifyList, item)
	}
	rspBody.PostLikeRspbody = &ht_moment.PostLikeRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Mid:        dbSubRspBody.GetMid(),
		NotifyList: notifyList,
	}
	// 回响应
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 最后下发异步通知到p2pagent进程
	for _, v := range dbSubRspBody.GetNotifyList() {
		// infoLog.Printf("ProcPostLikeService mid=%s notify index=%v notify uid=%v",
		// 	mid,
		// 	i,
		// 	v.GetTargetUid())
		pushType := 0
		err = p2pAgentApi.SendPushMsg(head.Uid, v.GetTargetUid(), 0, uint32(pushType), 1)
		if err != nil {
			infoLog.Printf("ProcPostLikeService mid=%s cmntUid=%v targetUid=%v notifyType=%v failed err=%s",
				mid,
				head.Uid,
				v.GetTargetUid(),
				pushType,
				err)
			attr := "gomntlogic/sendp2p_failed"
			libcomm.AttrAdd(attr, 1)
		}
	}
	// 发布nsq事件到话题topic
	eventType := NSQ_EVENT_LIKE
	// 真就是点赞，假就是取消赞
	if subReqBody.GetLikeOrNot() != 0 {
		eventType = NSQ_EVENT_LIKE
	} else {
		eventType = NSQ_EVENT_CANCLE_LIKE
	}
	err = PublishPostEvent(head.Uid, string(mid), eventType)
	if err != nil {
		// add static
		attr := "gomntlogic/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcPostLikeService PublishPostEvent uid=%v mid=%s eventType=%v failed",
			head.Uid,
			mid,
			eventType)
	}

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/post_like_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcPostLikeService fromId=%v userId=%v procTime=%v", head.Uid, uid, procTime)
	return true
}

func GetCidFromClientCid(cliCid []byte) (srvClid string, err error) {
	if len(cliCid) == 0 {
		infoLog.Printf("GetCidFromClientCid length error cliCid=%s", cliCid)
		err = util.ErrLengthErr
		return srvClid, err
	}
	cid, err := strconv.ParseUint(string(cliCid), 10, 64)
	if err != nil {
		return srvClid, err
	}
	cid += util.BaseCommentIndex
	srvClid = fmt.Sprintf("%v", cid)
	return srvClid, err
}

// 22.Get user info
func ProcGetUserInfo(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserInfo not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserInfo invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/get_user_info_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/get_user_info_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserInfoReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserInfo GetGetUserInfoReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	targetUid := subReqBody.GetUserid()
	infoLog.Printf("ProcGetUserInfo fromId=%v toIdList=%v", head.Uid, targetUid)
	outListVer, mapNotShareList, err := proSyncReq.GetNotShareToUidList(head, targetUid, 0)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo proSyncReq.GetNotShareToUidList failed uid=%v err=%s", targetUid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcGetUserInfo targetUid=%v outListVer=%v mapNotShareListLen=%v", targetUid, outListVer, len(mapNotShareList))
	beBlock := 0 // 默认可以查看帖文
	if _, ok := mapNotShareList[head.Uid]; ok {
		beBlock = 1 // 不让看
	}
	// beBlock > 0 targetUid不让head.Uid的uid看 直接返回0
	if beBlock > 0 {
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			MomentCount: proto.Uint32(0),
			LikedCount:  proto.Uint32(0),
		}
		return true // return send response
	}

	// Step2: query user base info
	momentCnt, likeCnt, err := proSyncReq.GetUserMomentInfo(head, targetUid)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo GetUserMomentInfo error uid=%s targetUid=%v err=%s",
			head.Uid,
			targetUid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MomentCount: proto.Uint32(momentCnt),
		LikedCount:  proto.Uint32(likeCnt),
	}
	procTime := packet.CalcProcessTime()
	if procTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/get_user_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcGetUserInfo uid=%v targetUid=%v procTime=%v", head.Uid, targetUid, procTime)
	return true
}

// func UpdateRedisCache(head *common.HeadV2, reqPayLoad []byte) {
// 	if head == nil || len(reqPayLoad) == 0 {
// 		infoLog.Printf("UpdateRedisCache input param error")
// 		return
// 	}
// 	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
// 	if err == nil {
// 		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
// 		if ok { // 是HeadV2Packet报文
// 			// head 为一个new出来的对象指针
// 			redisHead, err := redisMasterPacket.GetHead()
// 			if err == nil {
// 				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
// 				infoLog.Printf("UpdateRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)
// 			} else {
// 				infoLog.Printf("UpdateRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)
// 			}
// 		} else {
// 			infoLog.Printf("UpdateRedisCache redisPacket can not change to HeadV2packet")
// 		}
// 	} else {
// 		// add static
// 		attr := "gomntlogic/update_redis_failed"
// 		libcomm.AttrAdd(attr, 1)
// 		infoLog.Printf("UpdateRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
// 	}

// 	redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
// 	if err == nil {
// 		redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
// 		if ok { // 是HeadV2Packet报文
// 			// head 为一个new出来的对象指针
// 			redisHead, err := redisSlavePacket.GetHead()
// 			if err == nil {
// 				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
// 				infoLog.Printf("UpdateRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)
// 			} else {
// 				infoLog.Printf("UpdateRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)
// 			}
// 		} else {
// 			infoLog.Printf("UpdateRedisCache redisPacket can not change to HeadV2packet")
// 		}
// 	} else {
// 		// add static
// 		attr := "gomntlogic/update_redis_failed"
// 		libcomm.AttrAdd(attr, 1)
// 		infoLog.Printf("UpdateRedisCache writeAgentSlaveApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
// 	}
// 	return
// }

func GetHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#account", uid)
	return hashName
}

func PublishPostEvent(uid uint32, mid string, eventType int) (err error) {
	if uid == 0 || len(mid) == 0 {
		infoLog.Printf("PublishPostEvent invalid param uid=%v mid=%s eventType=%v", uid, mid, eventType)
		err = util.ErrDbParam
		return err
	}
	notifyObj := simplejson.New()
	notifyObj.Set("uid", uid)
	notifyObj.Set("ts", time.Now().Unix())
	notifyObj.Set("mid", mid)
	notifyObj.Set("type", eventType)
	notifySlice, err := notifyObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("PublishPostEvent notifyObj.MarshalJson failed uid=%v mid=%s notifyType=%v",
			uid,
			mid,
			eventType)
		return err
	}

	err = globalProducer.Publish(userActionTopic, notifySlice)
	if err != nil {
		// 统计apns失败总量
		attr := "gomntlogic/post_mnt_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("PublishPostEvent Could not connect uid=%v mid=%s notifyEvent=%v",
			uid,
			mid,
			eventType)
		return err
	} else {
		// 统计apns成功的总量
		attr := "gomntlogic/post_mnt_to_nsq_succ"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("PublishPostEvent Publish success uid=%v mid=%s eventType=%v",
			uid,
			mid,
			eventType)
	}
	return nil
}

func MessageHandle(message *nsq.Message) error {
	// 统计总量
	attr := "gomntlogic/nsq_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	select {
	case modMntBucketChan <- message.Body:
		infoLog.Printf("MessageHandle message id=%s timestamp=%v put into chan succ", message.ID, message.Timestamp)
	case <-closeChan: // 进程退出
		infoLog.Printf("MessageHandle gorutine exit")
		return nil
	}
	return nil
}

func MessageHandleLoop(index int) {
	defer func() {
		recover()
	}()
	infoLog.Printf("MessageHandleLoop index=%v", index)
	for {
		select {
		case buf := <-modMntBucketChan:
			packet := common.NewHeadV2Packet(buf)
			head, err := packet.GetHead()
			if err != nil {
				infoLog.Printf("MessageHandleLoop packet Get head failed err=%s", err)
				continue
			}
			body := packet.GetBody()
			switch head.Cmd {
			case uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_BUCKET):
				NsqModMntBucket(head, body)
			default:
				infoLog.Printf("MessageHandleLoop unhandle cmd=%v", head.Cmd)
			}
		case <-closeChan:
			return
		}
	}
}

func NsqModMntBucket(head *common.HeadV2, payLoad []byte) bool {
	beginTime := time.Now().UnixNano()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		infoLog.Printf("NsqModMntBucket invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gomntlogic/mod_mnt_bucket_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gomntlogic/nsq_mod_mnt_bucket_count"
	libcomm.AttrAdd(attr, 1)

	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("NsqModMntBucket proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}
	subReqBody := reqBody.GetModMntBucketReqbody()
	if subReqBody == nil {
		infoLog.Printf("NsqModMntBucket GetModMntBucketReqbody() failed uid=%v", head.Uid)
		return false
	}

	mid := subReqBody.GetMid()
	bucketType := subReqBody.GetBucketType()
	opReason := subReqBody.GetOpReason()
	uid := subReqBody.GetMoment().GetUserid()
	infoLog.Printf("NsqModMntBucket uid=%v mid=%s type=%v opReason=%s",
		uid,
		mid,
		bucketType,
		opReason)
	midStr := string(mid)
	// Step1: 首先获取帖子之前的deleted 状态
	keys := []string{
		util.MntDeletedKey,
	}
	values, err := proSyncReq.GetSpecificKeyInMidHashMap(head, midStr, keys)
	if err != nil || len(values) == 0 {
		// add static
		attr := "gomntlogic/get_mnt_count_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("NsqModMntBucket proSyncReq.GetSpecificKeyInMidHashMap hashmap=%s keys=%v err=%s valuesLen=%v", midStr, keys, err, len(values))
		return false
	}
	// Step2: 根据帖文的deleted 进行调整
	prevDeleted, err := strconv.ParseUint(values[0], 10, 32)
	if err != nil {
		infoLog.Printf("NsqModMntBucket strconv.ParseUint mid=%s deleted=%s err=%s", midStr, values[0], err)
		return false
	}
	infoLog.Printf("NsqModMntBucket mid=%s prevDeletd=%v", midStr, prevDeleted)
	if prevDeleted == uint64(ht_moment.MntStatusOpType_OP_HIDE) { // 如果帖文之前的状态为指给自己看 直接返回
		infoLog.Printf("NsqModMntBucket mid=%s prevDeleted=%v only for me", midStr, prevDeleted)
		return true
	}

	if prevDeleted == uint64(ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY) &&
		bucketType == ht_moment.MntBucketType_INI_TO_ALL {
		infoLog.Printf("NsqModMntBucket mid=%s prevDeleted=%v follower only type=%v need chage",
			midStr,
			prevDeleted,
			bucketType)
		bucketType = ht_moment.MntBucketType_INI_TO_FOWLLER
	}
	// Step3: 将特定字段写入mid 的 hashmap中
	var keyValue []string
	// add content
	keyValue = append(keyValue, util.MntOpReasonKey)
	keyValue = append(keyValue, string(opReason))
	if bucketType == ht_moment.MntBucketType_INI_TO_ME {
		keyValue = append(keyValue, util.BucketTypeKey)
		keyValue = append(keyValue, "0")
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_HIDE))
	} else if bucketType == ht_moment.MntBucketType_INI_TO_FOWLLER {
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_FOR_FOLLOWER_ONLY))
	} else if bucketType == ht_moment.MntBucketType_INI_TO_ALL {
		keyValue = append(keyValue, util.MntDeletedKey)
		keyValue = append(keyValue, fmt.Sprintf("%d", ht_moment.MntStatusOpType_OP_RESTORE))
	} else {
		infoLog.Printf("NsqModMntBucket uid=%v mid=%s type=%v opReason=%s unknow",
			head.Uid,
			midStr,
			bucketType,
			opReason)
		return true
	}
	infoLog.Printf("NsqModMntBucket mid=%s type=%v opReason=%s keyValue=%v",
		mid,
		bucketType,
		opReason,
		keyValue)
	dbRet, err := UpdateMidHashMapInDb(head, midStr, keyValue)
	if err != nil || dbRet != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		infoLog.Printf("NsqModMntBucket uid=%v mid=%s err=%s ret=%v", head.Uid, midStr, err, dbRet)
		return false
	}

	// Step4: 如果只展示给自己看 直接返回
	if bucketType == ht_moment.MntBucketType_INI_TO_ME {
		infoLog.Printf("NsqModMntBucket mid=%s bucketType=%v opReason=%s",
			midStr,
			bucketType,
			opReason)
		return true
	}

	// step5:发送请求到dbd 做其他业务逻辑更新操作
	dbdRet, err := ModMntBucektToDbd(head, subReqBody)
	if err != nil {
		infoLog.Printf("NsqModMntBucket uid=%v ModMntBucektToDbd ret err=%s", head.Uid, err)
	}
	// infoLog.Printf("ProcModMntBucketService uid=%v ModMntBucektToDbd ret=%v", head.Uid, dbdRet)
	if dbdRet != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		// add static
		attr := "gomntlogic/mod_mnt_bucket_to_dbd_failed"
		libcomm.AttrAdd(attr, 1)
	}
	// 2017-11-03 发布NSQ事件到队列中
	eventType := NSQ_EVENT_BUCKET_TO_ME
	if bucketType == ht_moment.MntBucketType_INI_TO_ME {
		eventType = NSQ_EVENT_BUCKET_TO_ME
	} else if bucketType == ht_moment.MntBucketType_INI_TO_FOWLLER {
		eventType = NSQ_EVENT_BUCKET_TO_FOLLOW
	} else if bucketType == ht_moment.MntBucketType_INI_TO_ALL {
		eventType = NSQ_EVENT_BUCKET_TO_ALL
	}
	err = PublishPostEvent(uid, midStr, eventType)
	if err != nil {
		// add static
		attr := "gomntlogic/publish_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("NsqModMntBucket PublishPostEvent uid=%v mid=%s eventType=%v failed",
			uid,
			midStr,
			eventType)
	}

	totalProcTime := (time.Now().UnixNano() - beginTime) / 1000000
	infoLog.Printf("NsqModMntBucket uid=%v mid=%s totalProcTime=%v", head.Uid, mid, totalProcTime)
	if totalProcTime > util.ProcSlowThreshold {
		// add static
		attr := "gomntlogic/nsq_mod_mnt_bucket_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_moment.RspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	// infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)
	// init learn param
	gLearnDay = uint32(cfg.Section("LEARN").Key("learn_day").MustInt(0))
	gLearnLimit = uint32(cfg.Section("LEARN").Key("learn_limit").MustInt(0))
	switchConfig := cfg.Section("LEARN").Key("show_switch").MustInt(0)
	if switchConfig == 0 {
		gShowSwitch = false
	} else {
		gShowSwitch = true
	}
	// init master cache
	// masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	// masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	// infoLog.Printf("cache master ip=%s port=%s", masterIp, masterPort)
	// writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// // init slave cache
	// slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	// slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	// infoLog.Printf("cache slave ip=%s port=%s", slaveIp, slavePort)
	// writeAgentSlaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustString("6379")
	infoLog.Printf("cache dbd ip=%v port=%v", dbdIp, dbdPort)
	dbdAgentApi = common.NewSrvToSrvApiV2(dbdIp, dbdPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init relation api
	relationIp := cfg.Section("RELATION").Key("ip").MustString("127.0.0.1")
	relationPort := cfg.Section("RELATION").Key("port").MustString("6379")
	infoLog.Printf("relation ip=%s port=%s", relationIp, relationPort)
	relationApi = common.NewRelationApi(relationIp, relationPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, 1000)

	// init usercache api
	userCacheIp := cfg.Section("USERCACHE").Key("ip").MustString("127.0.0.1")
	userCachePort := cfg.Section("USERCACHE").Key("port").MustString("6379")
	infoLog.Printf("user cache ip=%s port=%s", userCacheIp, userCachePort)
	userCacheApi = common.NewUserCacheApi(userCacheIp, userCachePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init cmntcache api
	// cmntCacheIp := cfg.Section("CMNTCACHE").Key("ip").MustString("127.0.0.1")
	// cmntCachePort := cfg.Section("CMNTCACHE").Key("port").MustString("18600")
	// infoLog.Printf("cmnt cache ip=%s port=%s", cmntCacheIp, cmntCachePort)
	// cmntCacheApi = common.NewCmntCacheApi(cmntCacheIp, cmntCachePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init p2pworker api
	p2pWorkerIp := cfg.Section("P2PWORKER").Key("ip").MustString("127.0.0.1")
	p2pWorkerPort := cfg.Section("P2PWORKER").Key("port").MustString("18600")
	infoLog.Printf("p2pworker ip=%s port=%s", p2pWorkerIp, p2pWorkerPort)
	p2pAgentApi = common.NewP2PWorkerApiV2(p2pWorkerIp, p2pWorkerPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, 1000)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	redisOperator := util.NewRedisOperator(redisMasterApi, infoLog)
	proSyncReq = util.NewProcSyncSeq(dbdAgentApi, redisOperator, infoLog)

	// nsq producer config
	nsqUrl := cfg.Section("NSQ").Key("url").MustString("127.0.0.1:4150")
	nsqConfig := nsq.NewConfig()
	globalProducer, err = nsq.NewProducer(nsqUrl, nsqConfig)
	if err != nil {
		log.Fatalf("main nsq.NewProcucer failed nsqUrl=%s", nsqUrl)
	}
	userActionTopic = cfg.Section("USERACTION").Key("topic").MustString("usr_act")
	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	consumerTopic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	consumerChannel := cfg.Section("MESSAGE").Key("chan").MustString("ch")
	consumerConfig := nsq.NewConfig()
	q, _ := nsq.NewConsumer(consumerTopic, consumerChannel, consumerConfig)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		infoLog.Printf("main Could not connect to nsqlookup")
		return
	}
	consumerCount := cfg.Section("MESSAGE").Key("count").MustInt(50)
	modMntBucketChan = make(chan []byte) //创建长度为0的chane,如果消费者没有消费生产者就堵塞住
	// go MessageHandleLoop(0)
	for i := 0; i < consumerCount; i += 1 {
		go MessageHandleLoop(i)
	}
	closeChan = make(chan struct{})

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)
	close(closeChan)
	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
