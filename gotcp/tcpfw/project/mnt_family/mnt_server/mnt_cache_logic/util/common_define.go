// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import "errors"

// Error type
var (
	ErrNotExistInReids  = errors.New("err not exist in redis")
	ErrNilDbObject      = errors.New("err not set object current is nil")
	ErrDbParam          = errors.New("err param error")
	ErrChagePacket      = errors.New("err change packet")
	ErrGetResultFromDbd = errors.New("err get result from dbs")
	ErrInvalidParam     = errors.New("err invalid param")
	ErrProtoBuff        = errors.New("err pb error")
	ErrLengthErr        = errors.New("err length error")
)

const (
	MntForFollowerList = "mnt_for_follower_list"
	MntForSelfList     = "mnt_for_self_list"
	CorrectKey         = "#correction_cid_set"
	LikedKey           = "_like_uid_set"
	UserInfoSuffix     = "_user_info"

	MntContentKey     = "0"
	MntDeletedKey     = "deleted"
	MntOpReasonKey    = "op_reason"
	LikedCountKey     = "liked_count"
	LikedTsKey        = "liked_ts"
	CommentCountKey   = "comment_count"
	CommentTsKey      = "comment_ts"
	TotalCmntCountKey = "total_comment_count"
	FeaturedLevel     = "featured_level"

	MomentCountKey          = "moment_count"
	MomentLikeCountKey      = "like_count"
	MomentCountForOther     = "moment_count_for_others"
	MomentLikeCountForOther = "like_count_for_others"
	ShowFieldKey            = "show"

	MaxIndexKey           = "0"
	NotShareMyMntKey      = "#not_share_set"
	HideOtherMntKey       = "#hide_user_set"
	LearnLangBucketPrefix = "LLNANG#"
	MomentCreater         = "moment_creater"
	BucketTypeKey         = "bucket_type"
)

const (
	ProcTimeThresold     = 300000
	MaxMemberCount       = 200000
	SpecialUid           = 0
	VersionFieldInit     = 1
	MidToUidExpireTs     = 7 * 24 * 3600
	MidContentExpireTime = 6 * 3600
	ProcSlowThreshold    = 300
	LastCmntCount        = 10
	BaseCommentIndex     = 1000000
	CmntCountThreshold   = 3
	DefaultMidPageSize   = 30
	BaseMntIndex         = 100000000000 // 100亿
	AdminUid             = 10000
	ExpirePeriod         = 259200 //3 * 86400 = 3 day   units:second
)

type MidInfoStr struct {
	Seq string
	Uid uint32
}

type BucketStr struct {
	Seq    uint64                 //序号
	Name   string                 //桶的名称
	MidMap map[string]*MidInfoStr //mid数组,从大到小排序
	Size   uint64                 //桶的总大小
	MaxMid string                 //本次拉取中最大的mid
	MinMid string                 //本次拉取中最新的mid
}

type UserInfoStr struct {
	MomentCnt      uint32
	LikeCnt        uint32
	OtherMomentCnt uint32
	OtherLikeCnt   uint32
}
