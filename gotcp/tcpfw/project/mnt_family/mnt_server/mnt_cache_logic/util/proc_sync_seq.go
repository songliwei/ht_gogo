// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"fmt"
	"log"
	"strconv"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_cache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/golang/protobuf/proto"
)

type ProcSyncReq struct {
	dbdAgentApi   *common.SrvToSrvApiV2
	redisOperator *RedisOperator
	infoLog       *log.Logger
}

func NewProcSyncSeq(dbdApi *common.SrvToSrvApiV2, redisOpe *RedisOperator, logger *log.Logger) *ProcSyncReq {
	return &ProcSyncReq{
		dbdAgentApi:   dbdApi,
		redisOperator: redisOpe,
		infoLog:       logger,
	}
}

//获取Mnt 指给粉丝看列表
func (this *ProcSyncReq) GetMntForFollowerList(head *common.HeadV2) (mapMntForFollwerList map[uint32]uint32, err error) {
	if head == nil || this.redisOperator == nil || this.dbdAgentApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	mapMntForFollwerList, err = this.redisOperator.GetMntForFollowerList()
	if err == nil {
		this.infoLog.Printf("ProcSyncReq GetMntForFollowerList redisOperator.GetMntForFollowerList success size=%v", len(mapMntForFollwerList))
		return mapMntForFollwerList, nil
	} else {
		this.infoLog.Printf("ProcSyncReq GetMntForFollowerList redisOperator.GetMntForFollowerList return err=%s load from ssdb", err)
		attr := "gomntlogic/get_mnt_for_follower_list_redis_failed"
		libcomm.AttrAdd(attr, 1)

		mapMntForFollwerList, err = this.GetZsetListFromDb(head, MntForFollowerList)
		if err != nil {
			this.infoLog.Printf("ProcSyncReq GetMntForFollowerList this.GetZsetListFromDb faield err=%s", err)
			return nil, err
		}
		return mapMntForFollwerList, nil
	}
}

func (this *ProcSyncReq) GetZsetListFromDb(head *common.HeadV2, zsetName string) (mapZsetList map[uint32]uint32, err error) {
	// send packet to dbd
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_GET_ZSET_LIST)
	// this.infoLog.Printf("GetZsetListFromDb reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		GetZsetListReqbody: &ht_moment_cache.GetZsetListReqBody{
			ZsetName:   proto.String(zsetName),
			IndexStart: proto.Int64(0),
			IndexStop:  proto.Int64(MaxMemberCount),
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("GetZsetListFromDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return nil, err
	}
	dbdPacket, err := this.dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		this.infoLog.Printf("GetZsetListFromDb dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return nil, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		this.infoLog.Printf("GetZsetListFromDb change packet failed uid=%v", reqHead.Uid)
		err = ErrChagePacket
		return nil, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		this.infoLog.Printf("GetZsetListFromDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}

	if rspHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		this.infoLog.Printf("GetZsetListFromDb ret=%v not success", rspHead.Ret)
		err = ErrGetResultFromDbd
		return nil, err
	}

	rspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		this.infoLog.Printf("GetZsetListFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		return nil, err
	}
	subRspBody := rspBody.GetGetZsetListRspbody()
	if subRspBody == nil {
		this.infoLog.Printf("GetZsetListFromDb GetGetZsetListRspbody failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		err = ErrGetResultFromDbd
		return nil, err
	}
	keyScore := subRspBody.GetKeyScore()
	keyScoreLen := len(keyScore)
	mapZsetList = make(map[uint32]uint32)
	for index := 0; index < keyScoreLen; index = index + 2 {
		mapZsetList[keyScore[index]] = keyScore[index+1]
	}
	return mapZsetList, nil
}

// 获取Mnt 指给自己看列表
func (this *ProcSyncReq) GetMntForSelfList(head *common.HeadV2) (mapMntForSelfList map[uint32]uint32, err error) {
	if head == nil || this.redisOperator == nil || this.dbdAgentApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	mapMntForSelfList, err = this.redisOperator.GetMntForSelfList()
	if err == nil {
		this.infoLog.Printf("GetMntForSelfList redisOperator.GetMntForSelfList success size=%v", len(mapMntForSelfList))
		return mapMntForSelfList, nil
	} else {
		this.infoLog.Printf("GetMntForSelfList redisOperator.GetMntForSelfList return err=%s load from ssdb", err)
		attr := "gomntlogic/get_mnt_for_self_list_redis_failed"
		libcomm.AttrAdd(attr, 1)

		mapMntForSelfList, err = this.GetZsetListFromDb(head, MntForSelfList)
		if err != nil {
			this.infoLog.Printf("GetMntForSelfList this.GetZsetListFromDb faield err=%s", err)
			return nil, err
		}
		this.infoLog.Printf("GetMntForSelfList this.GetZsetListFromDb size=%v", len(mapMntForSelfList))
		return mapMntForSelfList, nil
	}
}

//获取Mnt的详细内容
func (this *ProcSyncReq) GetSpecificKeyInMidHashMap(head *common.HeadV2, hashName string, keys []string) (values []string, err error) {
	if len(hashName) == 0 || this.redisOperator == nil || this.dbdAgentApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	// this.infoLog.Printf("GetSpecificKeyInMidHashMap hashName=%s keys=%v", hashName, keys)
	values, err = this.redisOperator.GetSpecificKeyInMidHashMap(hashName, keys)
	if err == nil {
		// this.infoLog.Printf("ProcSyncReq GetSpecificKeyInMidHashMap success hashName=%s keys=%v err=%s",
		// 	hashName,
		// 	keys,
		// 	err)
		return values, nil
	} else {
		this.infoLog.Printf("ProcSyncReq redisOperator.GetSpecificKeyInMidHashMap return err=%s load from ssdb", err)
		attr := "gomntlogic/get_mnt_content_redis_failed"
		libcomm.AttrAdd(attr, 1)
		values, err = this.GetSpecificKeyInMidHashMapFromDb(head, hashName, keys)
		if err != nil {
			this.infoLog.Printf("ProcSyncReq this.GetSpecificKeyInMidHashMapFromDb faield hashName=%s keys=%v err=%s", hashName, keys, err)
			return nil, err
		}
		return values, nil
	}

}

func (this *ProcSyncReq) GetSpecificKeyInMidHashMapFromDb(head *common.HeadV2, hashName string, keys []string) (values []string, err error) {
	// send packet to dbd
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_GET_MID_HASH_MAP)
	// this.infoLog.Printf("GetSpecificKeyInMidHashMapFromDb hashName=%s keys=%v", hashName, keys)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		GetMidHashMapReqbody: &ht_moment_cache.GetMidHashMapReqBody{
			HashName: proto.String(hashName),
			Keys:     keys,
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("GetSpecificKeyInMidHashMapFromDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return nil, err
	}
	dbdPacket, err := this.dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		this.infoLog.Printf("GetSpecificKeyInMidHashMapFromDb dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return nil, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		this.infoLog.Printf("GetSpecificKeyInMidHashMapFromDb change packet failed uid=%v", reqHead.Uid)
		err = ErrChagePacket
		return nil, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		this.infoLog.Printf("GetSpecificKeyInMidHashMapFromDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}

	if rspHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		this.infoLog.Printf("GetSpecificKeyInMidHashMapFromDb ret=%v not success", rspHead.Ret)
		err = ErrGetResultFromDbd
		return nil, err
	}

	rspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		this.infoLog.Printf("GetSpecificKeyInMidHashMapFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		return nil, err
	}
	subRspBody := rspBody.GetGetMidHashMapRspbody()
	if subRspBody == nil {
		this.infoLog.Printf("GetSpecificKeyInMidHashMapFromDb GetGetHashMapRspbody failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		err = ErrGetResultFromDbd
		return nil, err
	}
	values = subRspBody.GetValues()
	return values, nil
}

//获取最新改错的cid list
func (this *ProcSyncReq) ZrangeList(head *common.HeadV2, zsetName string, offset uint64, limit uint64, reverse uint32) (kvs []string, err error) {
	if head == nil || this.dbdAgentApi == nil || len(zsetName) == 0 {
		err = ErrNilDbObject
		return nil, err
	}
	kvs, err = this.ZrangeListFromDb(head, zsetName, offset, limit, reverse)
	if err != nil {
		this.infoLog.Printf("ProcSyncReq ZrangeList failed uid=%v zsetName=%s offset=%v limimt=%v reverse=%v err=%s",
			head.Uid,
			zsetName,
			offset,
			limit,
			reverse,
			err)
		return nil, err
	}
	return kvs, nil
}

func (this *ProcSyncReq) ZrangeListFromDb(head *common.HeadV2, zsetName string, offset uint64, limit uint64, reverse uint32) (kvs []string, err error) {
	// send packet to dbd
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_ZRANGE_ZSET_LIST)
	// this.infoLog.Printf("ZrangeListFromDb reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		ZrangeListReqbody: &ht_moment_cache.ZrangeListReqBody{
			ZsetName: proto.String(zsetName),
			Offset:   proto.Uint64(offset),
			Limit:    proto.Uint64(limit),
			Reverse:  proto.Uint32(reverse),
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("ZrangeListFromDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return nil, err
	}
	dbdPacket, err := this.dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		this.infoLog.Printf("ZrangeListFromDb dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return nil, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		this.infoLog.Printf("ZrangeListFromDb change packet failed uid=%v", reqHead.Uid)
		err = ErrChagePacket
		return nil, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		this.infoLog.Printf("ZrangeListFromDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return nil, err
	}

	if rspHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		this.infoLog.Printf("ZrangeListFromDb ret=%v not success", rspHead.Ret)
		err = ErrGetResultFromDbd
		return nil, err
	}

	rspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		this.infoLog.Printf("ZrangeListFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		return nil, err
	}
	subRspBody := rspBody.GetZrangeListRspbody()
	if subRspBody == nil {
		this.infoLog.Printf("ZrangeListFromDb GetZrangeListRspbody failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		err = ErrGetResultFromDbd
		return nil, err
	}
	keyScore := subRspBody.GetKeyScore()
	for i, v := range keyScore {
		strKey := fmt.Sprintf("%v", i)
		strValue := fmt.Sprintf("%v", v)
		kvs = append(kvs, strKey)
		kvs = append(kvs, strValue)
	}
	return kvs, nil
}

// 获取zset 中的单个key
func (this *ProcSyncReq) Zget(head *common.HeadV2, zsetName, key string) (value int64, err error) {
	if head == nil || this.dbdAgentApi == nil || len(zsetName) == 0 {
		err = ErrNilDbObject
		return value, err
	}

	value, err = this.redisOperator.Zscore(zsetName, key)
	if err == nil {
		this.infoLog.Printf("ProcSyncReq Zget success zstName=%s key=%v value=%v",
			zsetName,
			key,
			value)
		return value, nil
	} else {
		value, err = this.ZgetFromDb(head, zsetName, key)
		if err != nil {
			this.infoLog.Printf("ProcSyncReq ZgetFromDb failed uid=%v zsetName=%s key=%v err=%s",
				head.Uid,
				zsetName,
				key,
				err)
			return value, err
		}
	}
	return value, nil
}

// 获取Db 中 zset的单个key
func (this *ProcSyncReq) ZgetFromDb(head *common.HeadV2, zsetName, key string) (value int64, err error) {
	// send packet to dbd
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_ZGET_REQ)
	// this.infoLog.Printf("ZgetFromDb reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		ZgetReqbody: &ht_moment_cache.ZGetReqBody{
			ZsetName: proto.String(zsetName),
			Key:      proto.String(key),
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("ZgetFromDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return value, err
	}
	dbdPacket, err := this.dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		this.infoLog.Printf("ZgetFromDb dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return value, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		this.infoLog.Printf("ZgetFromDb change packet failed uid=%v", reqHead.Uid)
		err = ErrChagePacket
		return value, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		this.infoLog.Printf("ZgetFromDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return value, err
	}

	if rspHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		this.infoLog.Printf("ZgetFromDb ret=%v not success", rspHead.Ret)
		err = ErrGetResultFromDbd
		return value, err
	}

	rspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		this.infoLog.Printf("ZgetFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		return value, err
	}
	subRspBody := rspBody.GetZgetRspbody()
	if subRspBody == nil {
		this.infoLog.Printf("ZgetFromDb GetZgetRspbody failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		err = ErrGetResultFromDbd
		return value, err
	}
	value = subRspBody.GetScore()
	return value, nil
}

// hrscan hash map 获取最大的index + mid 的列表
func (this *ProcSyncReq) GetBucketList(head *common.HeadV2, hashName string, needCnt int) (bucket *BucketStr, err error) {
	if head == nil || this.dbdAgentApi == nil || len(hashName) == 0 || needCnt == 0 {
		err = ErrNilDbObject
		return nil, err
	}
	bucket, err = this.redisOperator.GetBucketList(hashName, needCnt)
	if err == nil {
		this.infoLog.Printf("GetBucketList this.redisOperator.GetBucketList hashName=%s needCnt=%v success", hashName, needCnt)
		return bucket, nil
	} else {
		this.infoLog.Printf("GetBucketList this.redisOperator.GetBucketList hashName=%s needCnt=%v err=%s", hashName, needCnt, err)
		bucket, _, err = this.GetBucketListFromDb(head, hashName, 0, needCnt)
		if err != nil {
			this.infoLog.Printf("GetBucketList this.GetBucketListFromDb hashName=%s needCnt=%v err=%s", hashName, needCnt, err)
			return nil, err
		}
	}
	return bucket, nil
}

// hrscan hash map 获取最大的index + mid 的列表 根据当前的index进行获取
func (this *ProcSyncReq) GetBucketListAndIndex(head *common.HeadV2, hashName string, needCnt int) (bucket *BucketStr, lastIndex uint64, err error) {
	if head == nil || this.dbdAgentApi == nil || len(hashName) == 0 || needCnt == 0 {
		err = ErrNilDbObject
		return nil, lastIndex, err
	}
	bucket, lastIndex, err = this.redisOperator.GetBucketListAndIndex(hashName, needCnt)
	if err == nil {
		this.infoLog.Printf("GetBucketListAndIndex this.redisOperator.GetBucketList hashName=%s needCnt=%v success", hashName, needCnt)
		return bucket, lastIndex, nil
	} else {
		this.infoLog.Printf("GetBucketListAndIndex this.redisOperator.GetBucketList hashName=%s needCnt=%v err=%s", hashName, needCnt, err)
		var maxIndex uint64
		bucket, maxIndex, err = this.GetBucketListFromDb(head, hashName, 0, needCnt)
		if err != nil {
			this.infoLog.Printf("GetBucketListAndIndex this.GetBucketListFromDb hashName=%s needCnt=%v err=%s", hashName, needCnt, err)
			return nil, lastIndex, err
		}
		if maxIndex-BaseMntIndex == 0 {
			lastIndex = 0
		} else if (maxIndex - BaseMntIndex) < uint64(needCnt) {
			lastIndex = 0
		} else {
			lastIndex = maxIndex - uint64(needCnt)
		}
	}
	return bucket, lastIndex, nil
}

func (this *ProcSyncReq) GetBucketListFromDb(head *common.HeadV2, hashName string, lastIndex uint64, needCnt int) (bucket *BucketStr, maxIndex uint64, err error) {

	// send packet to dbd
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_HSCAN_REQ)
	// this.infoLog.Printf("GetBucketListFromDb reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		HscanReqbody: &ht_moment_cache.HscanReqBody{
			HashName: proto.String(hashName),
			Start:    proto.Int64(0),
			End:      proto.Int64(int64(lastIndex)),
			Limit:    proto.Int64(int64(needCnt)),
			Reverse:  proto.Uint32(1),
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("GetBucketListFromDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return nil, maxIndex, err
	}
	dbdPacket, err := this.dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		this.infoLog.Printf("GetBucketListFromDb dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return nil, maxIndex, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		this.infoLog.Printf("GetBucketListFromDb change packet failed uid=%v", reqHead.Uid)
		err = ErrChagePacket
		return nil, maxIndex, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		this.infoLog.Printf("GetBucketListFromDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return nil, maxIndex, err
	}

	if rspHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		this.infoLog.Printf("GetBucketListFromDb ret=%v not success", rspHead.Ret)
		err = ErrGetResultFromDbd
		return nil, maxIndex, err
	}

	rspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		this.infoLog.Printf("GetBucketListFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		return nil, maxIndex, err
	}
	subRspBody := rspBody.GetHscanRspbody()
	if subRspBody == nil {
		this.infoLog.Printf("GetBucketListFromDb GetHscanRspbody failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		err = ErrGetResultFromDbd
		return nil, maxIndex, err
	}
	keyValues := subRspBody.GetKeyValues()
	midMap := make(map[string]*MidInfoStr, len(keyValues)/2)
	for index := 0; index < len(keyValues); index += 2 {
		this.infoLog.Printf("GetBucketList mid i=%v v=%s", keyValues[index], keyValues[index+1])
		midItem := &MidInfoStr{
			Seq: keyValues[index],
			Uid: 0,
		}
		midMap[keyValues[index+1]] = midItem
	}

	bucket = &BucketStr{
		Seq:    0,
		Name:   hashName,
		MidMap: midMap,
		Size:   subRspBody.GetMaxKey(),
	}
	this.infoLog.Printf("GetBucketListFromDb hashName=%s Size=%v", hashName, subRspBody.GetMaxKey())
	maxIndex = subRspBody.GetMaxKey()
	return bucket, maxIndex, nil
}

// get not share to uid list
func (this *ProcSyncReq) GetNotShareToUidList(head *common.HeadV2, uid uint32, inListVer uint32) (outListVer uint32, mapForbitList map[uint32]uint32, err error) {
	if head == nil || this.dbdAgentApi == nil || uid == 0 {
		err = ErrNilDbObject
		return outListVer, mapForbitList, err
	}
	outListVer, mapForbitList, err = this.redisOperator.GetNotShareToUidList(uid, inListVer)
	if err == nil {
		// this.infoLog.Printf("ProcSyncReq GetNotShareToUidList redisOperator.GetNotShareToUidList success size=%v", len(mapForbitList))
		return outListVer, mapForbitList, nil
	} else {
		this.infoLog.Printf("ProcSyncReq GetNotShareToUidList redisOperator.GetNotShareToUidList return err=%s load from ssdb", err)
		attr := "gomntlogic/get_not_share_list_redis_failed"
		libcomm.AttrAdd(attr, 1)
		mapForbitList, err = this.GetZsetListFromDb(head, fmt.Sprintf("%v%s", uid, NotShareMyMntKey))
		if err != nil {
			this.infoLog.Printf("ProcSyncReq GetNotShareToUidList this.GetZsetListFromDb faield err=%s", err)
			return outListVer, mapForbitList, err
		}
		if ver, ok := mapForbitList[VersionFieldInit]; ok {
			outListVer = ver
		}
		return outListVer, mapForbitList, nil
	}
}

// get hide uid list
func (this *ProcSyncReq) GetHideOtherMntUidList(head *common.HeadV2, uid uint32, inListVer uint32) (outListVer uint32, mapHideUidList map[uint32]uint32, err error) {
	if head == nil || this.dbdAgentApi == nil || uid == 0 {
		err = ErrNilDbObject
		return outListVer, mapHideUidList, err
	}
	outListVer, mapHideUidList, err = this.redisOperator.GetHideOtherMntUidList(uid, inListVer)
	if err == nil {
		// this.infoLog.Printf("ProcSyncReq GetHideOtherMntUidList redisOperator.GetHideOtherMntUidList success size=%v", len(mapHideUidList))
		return outListVer, mapHideUidList, nil
	} else {
		this.infoLog.Printf("ProcSyncReq GetHideOtherMntUidList redisOperator.GetHideOtherMntUidList return err=%s load from ssdb", err)
		attr := "gomntlogic/get_hide_uid_list_redis_failed"
		libcomm.AttrAdd(attr, 1)
		mapHideUidList, err = this.GetZsetListFromDb(head, fmt.Sprintf("%v%s", uid, HideOtherMntKey))
		if err != nil {
			this.infoLog.Printf("ProcSyncReq GetHideOtherMntUidList this.GetZsetListFromDb faield err=%s", err)
			return outListVer, mapHideUidList, err
		}
		if ver, ok := mapHideUidList[VersionFieldInit]; ok {
			outListVer = ver
		}
		return outListVer, mapHideUidList, nil
	}
}

// get mid info
func (this *ProcSyncReq) GetMidInfo(head *common.HeadV2, mid string) (midResult *ht_moment.MomentIdResult, err error) {
	if this.dbdAgentApi == nil || len(mid) == 0 {
		err = ErrNilDbObject
		return nil, err
	}
	keys := []string{
		MntDeletedKey,
		LikedTsKey,
		CommentTsKey,
	}
	values, err := this.GetSpecificKeyInMidHashMap(head, mid, keys)
	if err != nil {
		this.infoLog.Printf("GetMidInfo this.GetSpecificKeyInMidHashMap mid=%s keys=%v err=%s", mid, keys, err)
		return nil, err
	}

	if len(values) != len(keys) {
		this.infoLog.Printf("GetMidInfo this.GetSpecificKeyInMidHashMap mid=%s keysLen=%v valuesLen=%v",
			mid,
			keys,
			values)
		err = ErrGetResultFromDbd
		return nil, err
	}
	deleted, err := strconv.ParseUint(values[0], 10, 32)
	if err != nil {
		this.infoLog.Printf("GetMidInfo strcovn.ParseUint deleted=%s failed mid=%s", values[0], mid)
		deleted = 0
	}
	likedTs, err := strconv.ParseUint(values[1], 10, 64)
	if err != nil {
		this.infoLog.Printf("GetMidInfo strcovn.ParseUint likedTs=%s failed mid=%s", values[1], mid)
		likedTs = 0
	}
	commentTs, err := strconv.ParseUint(values[2], 10, 64)
	if err != nil {
		this.infoLog.Printf("GetMidInfo strcovn.ParseUint commentTs=%s failed mid=%s", values[2], mid)
		commentTs = 0
	}
	midResult = &ht_moment.MomentIdResult{
		Mid:       []byte(mid),
		LikedTs:   proto.Uint64(likedTs),
		CommentTs: proto.Uint64(commentTs),
		Deleted:   proto.Uint32(uint32(deleted)),
	}
	// this.infoLog.Printf("GetMidInfo mid=%s likedTs=%v commentTs=%v deleted=%v",
	// 	mid,
	// 	likedTs,
	// 	commentTs,
	// 	deleted)
	return midResult, nil
}

// get mid info with show field
func (this *ProcSyncReq) GetMidInfoWithShow(head *common.HeadV2, mid string) (midResult *ht_moment.MomentIdResult, show uint64, err error) {
	if this.dbdAgentApi == nil || len(mid) == 0 {
		err = ErrNilDbObject
		return nil, show, err
	}
	keys := []string{
		MntDeletedKey,
		LikedTsKey,
		CommentTsKey,
		ShowFieldKey,
	}
	values, err := this.GetSpecificKeyInMidHashMap(head, mid, keys)
	if err != nil {
		this.infoLog.Printf("GetMidInfoWithShow this.GetSpecificKeyInHashMap mid=%s keys=%v err=%s", mid, keys, err)
		return nil, show, err
	}

	if len(values) != len(keys) {
		this.infoLog.Printf("GetMidInfoWithShow this.GetSpecificKeyInHashMap mid=%s keysLen=%v valuesLen=%v",
			mid,
			keys,
			values)
		err = ErrGetResultFromDbd
		return nil, show, err
	}
	deleted, err := strconv.ParseUint(values[0], 10, 32)
	if err != nil {
		this.infoLog.Printf("GetMidInfoWithShow strcovn.ParseUint deleted=%s failed mid=%s", values[0], mid)
		deleted = 0
	}
	var likedTs uint64
	if len(values[1]) > 0 {
		likedTs, err = strconv.ParseUint(values[1], 10, 64)
		if err != nil {
			this.infoLog.Printf("GetMidInfoWithShow strcovn.ParseUint likedTs=%s failed mid=%s", values[1], mid)
			likedTs = 0
		}
	} else {
		likedTs = 0
	}
	var commentTs uint64
	if len(values[2]) > 0 {
		commentTs, err = strconv.ParseUint(values[2], 10, 64)
		if err != nil {
			this.infoLog.Printf("GetMidInfoWithShow strcovn.ParseUint commentTs=%s failed mid=%s", values[2], mid)
			commentTs = 0
		}
	} else {
		commentTs = 0
	}

	if len(values) == 4 && len(values[3]) > 0 {
		show, err = strconv.ParseUint(values[3], 10, 64)
		if err != nil {
			this.infoLog.Printf("GetMidInfoWithShow strcovn.ParseUint show=%s failed mid=%s", values[3], mid)
			show = 0
		}
	} else {
		show = 0
	}

	midResult = &ht_moment.MomentIdResult{
		Mid:       []byte(mid),
		LikedTs:   proto.Uint64(likedTs),
		CommentTs: proto.Uint64(commentTs),
		Deleted:   proto.Uint32(uint32(deleted)),
	}
	return midResult, show, nil
}

// get mid create user
func (this *ProcSyncReq) GetMidCreateUser(head *common.HeadV2, mid string) (creater uint32, err error) {
	if this.dbdAgentApi == nil || len(mid) == 0 {
		err = ErrNilDbObject
		return creater, err
	}
	creater, err = this.redisOperator.GetMidCreateUser(mid)
	if err == nil && creater != 0 {
		return creater, nil
	} else {
		// add static
		attr := "gomntlogic/get_mnt_creater_cache_miss"
		libcomm.AttrAdd(attr, 1)

		keys := []string{
			MntContentKey,
		}
		value, err := this.GetSpecificKeyInMidHashMap(head, mid, keys)
		if err != nil || len(value) == 0 {
			this.infoLog.Printf("GetMidCreateUser mid=%s err=%s", mid, err)
			return creater, err
		}
		storeMomentBody := new(ht_moment_store.StoreMomentBody)
		err = proto.Unmarshal([]byte(value[0]), storeMomentBody)
		if err != nil {
			this.infoLog.Printf("GetMidCreateUser proto Unmarshal failed err=%s", err)
			return creater, err
		}
		creater = storeMomentBody.GetUserid()
		return creater, nil
	}
}

// get mid create user
func (this *ProcSyncReq) GetMidContent(head *common.HeadV2, mid string) (moment *ht_moment.MomentBody, err error) {
	if this.dbdAgentApi == nil || len(mid) == 0 {
		err = ErrNilDbObject
		return nil, err
	}

	keys := []string{
		MntContentKey,
	}
	value, err := this.GetSpecificKeyInMidHashMap(head, mid, keys)
	if err != nil || len(value) == 0 {
		this.infoLog.Printf("GetMidContent mid=%s err=%s", mid, err)
		return nil, err
	}
	storeMomentBody := new(ht_moment_store.StoreMomentBody)
	err = proto.Unmarshal([]byte(value[0]), storeMomentBody)
	if err != nil {
		this.infoLog.Printf("GetMidCreateUser proto Unmarshal failed err=%s", err)
		return nil, err
	}
	moment, err = MntStoreBodyToReqBody(storeMomentBody)
	return moment, err
}

// hrscan hash map 获取最大的index + mid 的列表
func (this *ProcSyncReq) GetBucketHisList(head *common.HeadV2, hashName string, lastIndex uint64, needCnt int) (bucket *BucketStr, err error) {
	if head == nil || this.dbdAgentApi == nil || len(hashName) == 0 || needCnt == 0 || lastIndex == BaseMntIndex {
		err = ErrInvalidParam
		return nil, err
	}
	bucket, err = this.redisOperator.GetBucketHisList(hashName, lastIndex, needCnt)
	if err == nil {
		this.infoLog.Printf("GetBucketHisList this.redisOperator.GetBucketHisList hashName=%s lastIndex=%v needCnt=%v success", hashName, lastIndex, needCnt)
		return bucket, nil
	} else {
		this.infoLog.Printf("GetBucketHisList this.redisOperator.GetBucketHisList hashName=%s lastIndex=%v needCnt=%v err=%s", hashName, lastIndex, needCnt, err)
		bucket, _, err = this.GetBucketListFromDb(head, hashName, lastIndex, needCnt)
		if err != nil {
			this.infoLog.Printf("GetBucketHisList this.GetBucketListFromDb hashName=%s lastIndex=%v needCnt=%v err=%s", hashName, lastIndex, needCnt, err)
			return nil, err
		}
	}
	return bucket, nil
}

// hrscan hash map 获取最大的index + mid 的列表 根据当前的index进行获取
func (this *ProcSyncReq) GetBucketHisListAndIndex(head *common.HeadV2, hashName string, lastIndex uint64, needCnt int) (bucket *BucketStr, outIndex uint64, err error) {
	if head == nil || this.dbdAgentApi == nil || len(hashName) == 0 || needCnt == 0 || lastIndex == BaseMntIndex {
		err = ErrNilDbObject
		return nil, outIndex, err
	}
	bucket, outIndex, err = this.redisOperator.GetBucketHisListAndIndex(hashName, lastIndex, needCnt)
	if err == nil {
		this.infoLog.Printf("GetBucketListAndIndex this.redisOperator.GetBucketHisListAndIndex hashName=%s needCnt=%v success", hashName, needCnt)
		return bucket, outIndex, nil
	} else {
		this.infoLog.Printf("GetBucketListAndIndex this.redisOperator.GetBucketHisListAndIndex hashName=%s lastIndex=%v needCnt=%v err=%s", hashName, lastIndex, needCnt, err)
		var maxIndex uint64
		bucket, maxIndex, err = this.GetBucketListFromDb(head, hashName, lastIndex, needCnt)
		if err != nil {
			this.infoLog.Printf("GetBucketListAndIndex this.GetBucketListFromDb hashName=%s lastIndex=%v needCnt=%v err=%s", hashName, lastIndex, needCnt, err)
			return nil, outIndex, err
		}
		if maxIndex-BaseMntIndex == 0 {
			outIndex = 0
		} else if (maxIndex - BaseMntIndex) < uint64(needCnt) {
			outIndex = 0
		} else {
			outIndex = maxIndex - uint64(needCnt)
		}
	}
	return bucket, outIndex, nil
}

func (this *ProcSyncReq) GetSpecificSeqMid(head *common.HeadV2, hashName string, seq uint32) (mid string, err error) {
	if head == nil || this.dbdAgentApi == nil || len(hashName) == 0 {
		err = ErrInvalidParam
		return mid, err
	}
	mid, err = this.redisOperator.GetSpecificSeqMid(head, hashName, uint64(seq))
	if err == nil || err == ErrInvalidParam {
		return mid, err
	} else {
		mid, err := this.GetSpecificSeqMidFromDb(head, hashName, seq)
		if err != nil {
			this.infoLog.Printf("GetSpecificSeqMid this.GetSpecificSeqMidFromDb hash=%s seq=%s err=%s", hashName, seq, err)
			return mid, err
		}
	}
	return mid, nil
}

func (this *ProcSyncReq) GetSpecificSeqMidFromDb(head *common.HeadV2, hashName string, seq uint32) (mid string, err error) {
	// send packet to dbd
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_GET_SEQ_MID_REQ)
	// this.infoLog.Printf("GetSpecificSeqMidFromDb reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		GetSeqMidReqbody: &ht_moment_cache.GetSeqMidReqBody{
			HashName: proto.String(hashName),
			Seq:      proto.Uint64(uint64(seq)),
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("GetSpecificSeqMidFromDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return mid, err
	}
	dbdPacket, err := this.dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		this.infoLog.Printf("GetSpecificSeqMidFromDb dbdAgentApi.SendAndRecvPacket send packet failed err=%s", err)
		return mid, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		this.infoLog.Printf("GetSpecificSeqMidFromDb change packet failed uid=%v", reqHead.Uid)
		err = ErrChagePacket
		return mid, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		this.infoLog.Printf("GetSpecificSeqMidFromDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return mid, err
	}

	if rspHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		this.infoLog.Printf("GetSpecificSeqMidFromDb ret=%v not success", rspHead.Ret)
		err = ErrGetResultFromDbd
		return mid, err
	}

	rspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		this.infoLog.Printf("GetSpecificSeqMidFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		return mid, err
	}
	subRspBody := rspBody.GetGetSeqMidRspbody()
	if subRspBody == nil {
		this.infoLog.Printf("GetSpecificSeqMidFromDb GetGetSeqMidRspbody failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		err = ErrGetResultFromDbd
		return mid, err
	}
	mid = string(subRspBody.GetMid())
	return mid, nil
}

func (this *ProcSyncReq) GetUserMomentInfo(head *common.HeadV2, userid uint32) (momentCnt, likeCnt uint32, err error) {
	if head == nil || this.redisOperator == nil || this.dbdAgentApi == nil || userid == 0 {
		err = ErrInvalidParam
		return momentCnt, likeCnt, err
	}

	momentCnt, likeCnt, err = this.redisOperator.GetUserMomentInfo(head, userid)
	if err == nil {
		this.infoLog.Printf("GetUserMomentInfo this.redisOperator.GetUserMomentInfo success uid=%v userid=%v momnetCnt=%v likeCnt=%v",
			head.Uid,
			userid,
			momentCnt,
			likeCnt)
		return momentCnt, likeCnt, nil
	} else {
		this.infoLog.Printf("GetUserMomentInfo this.redisOperator.GetUserMomentInfo uid=%v userid=%v err=%s load from ssdb", head.Uid, userid, err)
		attr := "gomntlogic/get_user_moment_info_redis_failed"
		libcomm.AttrAdd(attr, 1)
		momentCnt, likeCnt, err = this.GetUserMomentInfoFromDb(head, userid)
		if err != nil {
			this.infoLog.Printf("GetUserMomentInfo GetUserMomentInfoFromDb faield uid=%v userid=%v err=%s", head.Uid, userid, err)
			return momentCnt, likeCnt, err
		}
		return momentCnt, likeCnt, nil
	}
}

func (this *ProcSyncReq) GetUserMomentInfoFromDb(head *common.HeadV2, userid uint32) (momentCnt, likeCnt uint32, err error) {
	// send packet to dbd
	if head == nil || userid == 0 {
		this.infoLog.Printf("GetUserMomentInfoFromDb nil param")
		err = ErrInvalidParam
		return momentCnt, likeCnt, err
	}
	// send packet to dbd
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_GET_USER_INFO)
	// this.infoLog.Printf("GetUserMomentInfoFromDb reqHead=%#v", *reqHead)
	reqBody := &ht_moment_cache.MntCacheReqBody{
		GetUserInfoReqbody: &ht_moment_cache.GetUserInfoReqBody{
			Userid: proto.Uint32(userid),
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		this.infoLog.Printf("GetUserMomentInfoFromDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return momentCnt, likeCnt, err
	}
	dbdPacket, err := this.dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		this.infoLog.Printf("GetUserMomentInfoFromDb dbdAgentApi.SendAndRecvPacket send packet failed uid=%v err=%s", reqHead.Uid, err)
		return momentCnt, likeCnt, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		this.infoLog.Printf("GetUserMomentInfoFromDb change packet failed uid=%v", head.Uid)
		err = ErrChagePacket
		return momentCnt, likeCnt, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		this.infoLog.Printf("GetUserMomentInfoFromDb GetHead failed uid=%v err=%s", head.Uid, err)
		return momentCnt, likeCnt, err
	}

	if rspHead.Ret != uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS) {
		this.infoLog.Printf("GetUserMomentInfoFromDb ret=%v not success", head.Ret)
		err = ErrGetResultFromDbd
		return momentCnt, likeCnt, err
	}

	rspBody := new(ht_moment_cache.MntCacheRspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		this.infoLog.Printf("GetUserMomentInfoFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return momentCnt, likeCnt, err
	}
	subRspBody := rspBody.GetGetUserInfoRspbody()
	if subRspBody == nil {
		this.infoLog.Printf("GetUserMomentInfoFromDb GetGetUserInfoRspbody failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		err = ErrGetResultFromDbd
		return momentCnt, likeCnt, err
	}
	momentCnt = subRspBody.GetMomentCount()
	likeCnt = subRspBody.GetLikedCount()
	if likeCnt > 1000000 || momentCnt > 1000000 {
		this.infoLog.Printf("GetUserMomentInfoFromDb uid=%v Fatal error!like_cnt=%v or mnt_cnt = %v is invalid!", head.Uid, likeCnt, momentCnt)
		momentCnt = 0
		likeCnt = 0
	}
	return momentCnt, likeCnt, nil
}
