// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/golang/protobuf/proto"
)

func getTimeMicroSecond() (retMicSecond uint64) {
	retMicSecond = uint64(time.Now().UnixNano())
	retMicSecond = retMicSecond / 1000000 // 将纳秒转换成毫秒
	return retMicSecond
}

func MntReqBodyToStoreBody(reqBody *ht_moment.MomentBody) (storeBody *ht_moment_store.StoreMomentBody, err error) {
	if reqBody == nil {
		return nil, ErrDbParam
	}

	imageBody := make([]*ht_moment_store.StoreImageBody, len(reqBody.GetImages()))
	imageLen := len(reqBody.GetImages())
	reqImages := reqBody.GetImages()
	for index := 0; index < imageLen; index++ {
		imageBody[index] = &ht_moment_store.StoreImageBody{
			ThumbUrl: reqImages[index].GetThumbUrl(),
			BigUrl:   reqImages[index].GetBigUrl(),
			Width:    proto.Int32(int32(reqImages[index].GetWidth())),
			Height:   proto.Int32(int32(reqImages[index].GetHeight())),
		}
	}

	tagBody := []*ht_moment_store.StoreTagBody{}
	reqTags := reqBody.GetTags()
	for _, v := range reqTags {
		item := &ht_moment_store.StoreTagBody{
			Id:       proto.Uint64(v.GetId()),
			Name:     v.GetName(),
			Type:     proto.Uint32(v.GetType()),
			Pos:      proto.Uint32(v.GetPos()),
			LangType: proto.Uint32(v.GetLangType()),
		}
		tagBody = append(tagBody, item)
	}
	storeBody = &ht_moment_store.StoreMomentBody{
		Userid:   proto.Uint32(reqBody.GetUserid()),
		Mid:      reqBody.GetMid(),
		Content:  reqBody.GetContent(),
		PostTime: proto.Uint64(getTimeMicroSecond()),
		Voice: &ht_moment_store.StoreVoiceBody{
			Url:      reqBody.GetVoice().GetUrl(),
			Duration: proto.Uint32(reqBody.GetVoice().GetDuration()),
			Size:     proto.Uint32(reqBody.GetVoice().GetSize()),
		},
		Images:      imageBody,
		Position:    reqBody.GetPosition(),
		Latitude:    proto.Float32(reqBody.GetLatitude()),
		Longitude:   proto.Float32(reqBody.GetLongitude()),
		LangType:    proto.Uint32(reqBody.GetLangType()),
		StrLangType: reqBody.GetStrLangType(),
		UrlInfo: &ht_moment_store.StoreUrlInfo{
			Url:         reqBody.GetUrlInfo().GetUrl(),
			ThumbUrl:    reqBody.GetUrlInfo().GetThumbUrl(),
			Description: reqBody.GetUrlInfo().GetDescription(),
			Title:       reqBody.GetUrlInfo().GetTitle(),
			SubTitle:    reqBody.GetUrlInfo().GetSubTitle(),
			Type:        reqBody.GetUrlInfo().GetType(),
			Weexurl:     reqBody.GetUrlInfo().GetWeexurl(),
		},
		Tags: tagBody,
	}
	return storeBody, nil
}

func MntStoreBodyToReqBody(storeBody *ht_moment_store.StoreMomentBody) (reqBody *ht_moment.MomentBody, err error) {
	if storeBody == nil {
		return nil, ErrDbParam
	}

	storeImages := storeBody.GetImages()
	imageLen := len(storeImages)
	images := make([]*ht_moment.ImageBody, imageLen)
	for index := 0; index < imageLen; index++ {
		images[index] = &ht_moment.ImageBody{
			ThumbUrl: storeImages[index].GetThumbUrl(),
			BigUrl:   storeImages[index].GetBigUrl(),
			Width:    proto.Uint32(uint32(storeImages[index].GetWidth())),
			Height:   proto.Uint32(uint32(storeImages[index].GetHeight())),
		}
	}

	tags := []*ht_moment.TagBody{}
	storeTags := storeBody.GetTags()
	for _, v := range storeTags {
		item := &ht_moment.TagBody{
			Id:       proto.Uint64(v.GetId()),
			Name:     v.GetName(),
			Type:     proto.Uint32(v.GetType()),
			Pos:      proto.Uint32(v.GetPos()),
			LangType: proto.Uint32(v.GetLangType()),
		}
		tags = append(tags, item)
	}
	reqBody = &ht_moment.MomentBody{
		Userid:   proto.Uint32(storeBody.GetUserid()),
		Mid:      storeBody.GetMid(),
		Content:  storeBody.GetContent(),
		PostTime: proto.Uint64(storeBody.GetPostTime()),
		Voice: &ht_moment.VoiceBody{
			Url:      storeBody.GetVoice().GetUrl(),
			Duration: proto.Uint32(storeBody.GetVoice().GetDuration()),
			Size:     proto.Uint32(storeBody.GetVoice().GetSize()),
		},

		Images:      images,
		Position:    storeBody.GetPosition(),
		Latitude:    proto.Float32(storeBody.GetLatitude()),
		Longitude:   proto.Float32(storeBody.GetLongitude()),
		LangType:    proto.Uint32(storeBody.GetLangType()),
		StrLangType: storeBody.GetStrLangType(),
		UrlInfo: &ht_moment.URLInfo{
			Url:         storeBody.GetUrlInfo().GetUrl(),
			ThumbUrl:    storeBody.GetUrlInfo().GetThumbUrl(),
			Description: storeBody.GetUrlInfo().GetDescription(),
			Title:       storeBody.GetUrlInfo().GetTitle(),
			SubTitle:    storeBody.GetUrlInfo().GetSubTitle(),
			Type:        storeBody.GetUrlInfo().GetType(),
			Weexurl:     storeBody.GetUrlInfo().GetWeexurl(),
		},
		Tags: tags,
	}
	return reqBody, nil
}

func CmtStoreBodyToReqBody(storeBody *ht_moment_store.StoreCommentBody) (reqBody *ht_moment.CommentBody, err error) {
	if storeBody == nil {
		return nil, ErrDbParam
	}

	storeCorrectContent := storeBody.GetCorrection().GetCorrectContent()
	correctLen := len(storeCorrectContent)
	correctContentSlice := make([]*ht_moment.CorrectContent, correctLen)
	for index := 0; index < correctLen; index++ {
		correctContentSlice[index] = &ht_moment.CorrectContent{
			Content:    storeCorrectContent[index].GetContent(),
			Correction: storeCorrectContent[index].GetCorrection(),
		}
	}
	var toIdList []*ht_moment.ReplyInfo
	if len(storeBody.GetReplyList()) > 0 {
		for _, v := range storeBody.GetReplyList() {
			item := &ht_moment.ReplyInfo{
				Userid:   proto.Uint32(v.GetUserid()),
				Nickname: nil,
				UserType: nil,
				Pos:      proto.Uint32(v.GetPos()),
			}
			toIdList = append(toIdList, item)
		}
	} else {
		for _, v := range storeBody.GetToidList() {
			item := &ht_moment.ReplyInfo{
				Userid:   proto.Uint32(v),
				Nickname: nil,
				UserType: nil,
			}
			toIdList = append(toIdList, item)
		}
	}

	reqBody = &ht_moment.CommentBody{
		Userid:  proto.Uint32(storeBody.GetUserid()),
		Deleted: proto.Uint32(storeBody.GetDeleted()),
		Cid:     storeBody.GetCid(),
		Ctype:   ht_moment.COMMENT_TYPE(storeBody.GetCtype()).Enum(),
		Content: storeBody.GetContent(),
		Correction: &ht_moment.CorrectBody{
			Content:        storeBody.GetCorrection().GetContent(),
			Correction:     storeBody.GetCorrection().GetCorrection(),
			Note:           storeBody.GetCorrection().GetNote(),
			CorrectContent: correctContentSlice,
		},
		PostTime: proto.Uint64(storeBody.GetPostTime()),
		ToidList: toIdList,
		Voice: &ht_moment.VoiceBody{
			Url:      storeBody.GetVoice().GetUrl(),
			Duration: proto.Uint32(storeBody.GetVoice().GetDuration()),
			Size:     proto.Uint32(storeBody.GetVoice().GetSize()),
		},
	}
	return reqBody, nil
}

func CmtReqBodyToStoreBody(reqBody *ht_moment.CommentBody) (storeBody *ht_moment_store.StoreCommentBody, err error) {
	if reqBody == nil {
		return nil, ErrDbParam
	}

	correctContent := reqBody.GetCorrection().GetCorrectContent()
	correctLen := len(correctContent)
	correctContentSlice := make([]*ht_moment_store.StoreCorrectContent, correctLen)
	for index := 0; index < correctLen; index++ {
		correctContentSlice[index] = &ht_moment_store.StoreCorrectContent{
			Content:    correctContent[index].GetContent(),
			Correction: correctContent[index].GetCorrection(),
		}
	}

	replyInfo := reqBody.GetToidList()
	replyLen := len(replyInfo)
	storeReplyList := make([]*ht_moment_store.StoreReplyInfo, replyLen)
	for index := 0; index < replyLen; index++ {
		// toIdList[index] = replyInfo[index].GetUserid()
		storeReplyList[index] = &ht_moment_store.StoreReplyInfo{
			Userid: proto.Uint32(replyInfo[index].GetUserid()),
			Pos:    proto.Uint32(replyInfo[index].GetPos()),
		}
	}
	storeBody = &ht_moment_store.StoreCommentBody{
		Userid:  proto.Uint32(reqBody.GetUserid()),
		Deleted: proto.Uint32(reqBody.GetDeleted()),
		Cid:     reqBody.GetCid(),
		Ctype:   ht_moment_store.STORE_COMMENT_TYPE(reqBody.GetCtype()).Enum(),
		Content: reqBody.GetContent(),
		Correction: &ht_moment_store.StoreCorrectBody{
			Content:        reqBody.GetCorrection().GetContent(),
			Correction:     reqBody.GetCorrection().GetCorrection(),
			Note:           reqBody.GetCorrection().GetNote(),
			CorrectContent: correctContentSlice,
		},
		PostTime: proto.Uint64(getTimeMicroSecond()),
		// ToidList: toIdList,
		Voice: &ht_moment_store.StoreVoiceBody{
			Url:      reqBody.GetVoice().GetUrl(),
			Duration: proto.Uint32(reqBody.GetVoice().GetDuration()),
			Size:     proto.Uint32(reqBody.GetVoice().GetSize()),
		},
		ReplyList: storeReplyList,
	}
	return storeBody, nil
}
