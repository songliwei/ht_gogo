// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"fmt"
	"log"
	"strconv"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
)

type RedisOperator struct {
	redisMasterApi *common.RedisApi
	infoLog        *log.Logger
}

func NewRedisOperator(redisApi *common.RedisApi, logger *log.Logger) *RedisOperator {
	return &RedisOperator{
		redisMasterApi: redisApi,
		infoLog:        logger,
	}
}

//获取Mnt 指给粉丝看列表
func (this *RedisOperator) GetMntForFollowerList() (mapMntForFollwerList map[uint32]uint32, err error) {
	keyName := MntForFollowerList
	mapMntForFollwerList, err = this.GetZsetList(keyName)
	if err != nil {
		this.infoLog.Printf("GetMntForFollowerList GetZsetList err=%s", err)
	}
	return mapMntForFollwerList, err
}

// 获取Mnt 指给自己看列表
func (this *RedisOperator) GetMntForSelfList() (mapMntForSelfList map[uint32]uint32, err error) {
	keyName := MntForSelfList
	mapMntForSelfList, err = this.GetZsetList(keyName)
	if err != nil {
		this.infoLog.Printf("GetMntForSelfList GetZsetList err=%s", err)
	}
	return mapMntForSelfList, err
}

func (this *RedisOperator) GetZsetList(keyName string) (mapUidToTime map[uint32]uint32, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	ret, err := this.redisMasterApi.Exists(keyName)
	if err != nil {
		this.infoLog.Printf("GetZsetList redisMasterApi.Exists return err=%s", err)
		return nil, err
	}
	if ret != true {
		this.infoLog.Printf("GetZsetList not exist keyName=%s", keyName)
		return nil, ErrNotExistInReids
	}
	keyScore, err := this.redisMasterApi.Zrange(keyName, 0, MaxMemberCount)
	if err != nil {
		this.infoLog.Printf("GetZsetList redisMasterApi.Zrange return err=%s", err)
		return nil, err
	}

	keyScoreLen := len(keyScore)
	if keyScoreLen%2 != 0 {
		this.infoLog.Printf("GetZsetList redisMasterApi.Zrange size error size=%v", keyScoreLen)
		err = ErrLengthErr
		return nil, err
	}
	mapUidToTime = make(map[uint32]uint32)
	for index := 0; index < keyScoreLen; index = index + 2 {
		toId, err := strconv.ParseUint(keyScore[index], 10, 32)
		if err != nil {
			this.infoLog.Printf("GetZsetList strconv.ParseUint uid=%s err=%s failed", keyScore[index], err)
			continue
		}
		// 每次需要取出版本号信息
		if toId == SpecialUid || toId == VersionFieldInit {
			// this.infoLog.Printf("GetZsetList ignore uid=%v", toId)
			continue
		}
		parseScore, err := strconv.ParseUint(keyScore[index+1], 10, 32)
		if err != nil {
			this.infoLog.Printf("GetZsetList strconv.ParseUint score=%s err=%s failed", keyScore[index+1], err)
			continue
		}
		score := 0xffff - uint32(parseScore)
		mapUidToTime[uint32(toId)] = score
	}
	return mapUidToTime, nil
}

// 获取hash map 的详细内容 首先判断redis中是否存在hashmap 如果不存在 直接返回错误
func (this *RedisOperator) GetSpecificKeyInMidHashMap(hashName string, keys []string) (values []string, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	ret, err := this.redisMasterApi.Exists(hashName)
	if err != nil {
		this.infoLog.Printf("GetSpecificKeyInMidHashMap redisMasterApi.Exists return err=%s", err)
		return nil, err
	}
	if ret != true {
		this.infoLog.Printf("GetSpecificKeyInMidHashMap not exist keyName=%s", hashName)
		return nil, ErrNotExistInReids
	}
	values, err = this.redisMasterApi.Hmget(hashName, keys)
	if err != nil {
		this.infoLog.Printf("GetSpecificKeyInMidHashMap GetSpecificKeyInHashMap redisMasterApi.Hmget size error hashName=%s keys=%v err=%s",
			hashName,
			keys,
			err)
	}
	return values, err
}

// 逆序获取当个桶内指定数据的mid
func (this *RedisOperator) GetBucketList(hashName string, needCnt int) (bucket *BucketStr, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	ret, err := this.redisMasterApi.Exists(hashName)
	if err != nil {
		this.infoLog.Printf("GetBucketList redisMasterApi.Exists return err=%s", err)
		return nil, err
	}
	if ret != true {
		this.infoLog.Printf("GetBucketList not exist keyName=%s", hashName)
		return nil, ErrNotExistInReids
	}
	value, err := this.redisMasterApi.Hget(hashName, MaxIndexKey)
	if err != nil {
		this.infoLog.Printf("GetBucketList this.redisMasterApi.Hget hashName=%s key=%s err=%s", hashName, MaxIndexKey, err)
		return nil, err
	}
	maxIndex, err := strconv.ParseUint(value, 10, 64)
	if err != nil {
		this.infoLog.Printf("GetBucketList strconv.ParseUint value=%s err=%s", value, err)
		return nil, err
	}
	// this.infoLog.Printf("GetBucketList hashName=%s maxIndex=%v", hashName, maxIndex)
	// 桶为空直接返回空
	if maxIndex == 0 {
		bucket = &BucketStr{
			Seq:    0,
			Name:   hashName,
			MidMap: nil,
			Size:   0,
		}
		return bucket, nil
	}

	//首先逆序准备好key然后在Hmgt
	realNeedCnt := needCnt
	if maxIndex-BaseMntIndex < uint64(needCnt) {
		realNeedCnt = int(maxIndex - BaseMntIndex)
	}
	keys := make([]string, realNeedCnt)
	curMaxIndex := maxIndex
	for index := realNeedCnt; index > 0; index -= 1 {
		itemKey := fmt.Sprintf("%v", curMaxIndex)
		curMaxIndex -= 1
		keys[realNeedCnt-index] = itemKey
	}

	values, err := this.redisMasterApi.Hmget(hashName, keys)
	if err != nil {
		this.infoLog.Printf("GetBucketList this.redisMasterApi.Hmget hashName=%s keys=%v return err=%s", hashName, keys, err)
		return nil, err
	}

	if len(values) != len(keys) {
		this.infoLog.Printf("GetBucketList keysLen=%v valuesLen=%v not equal", len(keys), len(values))
		err = ErrLengthErr
		return nil, err
	}
	midMap := make(map[string]*MidInfoStr, len(values))
	for i, v := range values {
		this.infoLog.Printf("GetBucketList hashName=%s mid i=%v v=%s", hashName, i, v)
		midItem := &MidInfoStr{
			Seq: keys[i],
			Uid: 0,
		}
		midMap[v] = midItem
	}

	bucket = &BucketStr{
		Seq:    0,
		Name:   hashName,
		MidMap: midMap,
		Size:   maxIndex,
	}
	return bucket, nil
}

// 逆序获取当个桶内指定数据的mid 和下次获取的序号
func (this *RedisOperator) GetBucketListAndIndex(hashName string, needCnt int) (bucket *BucketStr, lastIndex uint64, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return nil, lastIndex, err
	}
	ret, err := this.redisMasterApi.Exists(hashName)
	if err != nil {
		this.infoLog.Printf("GetBucketListAndIndex redisMasterApi.Exists return err=%s", err)
		return nil, lastIndex, err
	}
	if ret != true {
		this.infoLog.Printf("GetBucketListAndIndex not exist keyName=%s", hashName)
		return nil, lastIndex, ErrNotExistInReids
	}
	value, err := this.redisMasterApi.Hget(hashName, MaxIndexKey)
	if err != nil {
		this.infoLog.Printf("GetBucketListAndIndex this.redisMasterApi.Hget hashName=%s key=%s err=%s", hashName, MaxIndexKey, err)
		return nil, lastIndex, err
	}
	maxIndex, err := strconv.ParseUint(value, 10, 64)
	if err != nil {
		this.infoLog.Printf("GetBucketListAndIndex strconv.ParseUint value=%s err=%s", value, err)
		return nil, lastIndex, err
	}
	realNeedCnt := needCnt
	if maxIndex-BaseMntIndex < uint64(needCnt) {
		realNeedCnt = int(maxIndex - BaseMntIndex)
	}

	if maxIndex == BaseMntIndex { // 空的hashmap设置lastIndex 为 0
		lastIndex = 0
		return nil, lastIndex, nil
	} else if (maxIndex - BaseMntIndex) < uint64(needCnt) { // pagesize > hsz 时，一次获取完毕这里设置 0 ，防止获取历史重复
		lastIndex = 0 // 整张hash 表 一次性获取
	} else {
		lastIndex = maxIndex - uint64(needCnt) // 设置下次获取历史的起始位置
	}

	this.infoLog.Printf("GetBucketListAndIndex hashName=%s realNeedCnt=%v lastIndex=%v", hashName, realNeedCnt, lastIndex)
	//首先逆序准备好key然后在Hmgt
	keys := make([]string, realNeedCnt)
	curMaxIndex := maxIndex
	for index := realNeedCnt; index > 0; index -= 1 {
		itemKey := fmt.Sprintf("%v", curMaxIndex)
		curMaxIndex -= 1
		keys[realNeedCnt-index] = itemKey
	}

	values, err := this.redisMasterApi.Hmget(hashName, keys)
	if err != nil {
		this.infoLog.Printf("GetBucketListAndIndex this.redisMasterApi.Hmget hashName=%s keys=%v return err=%s", hashName, keys, err)
		return nil, lastIndex, err
	}
	midMap := make(map[string]*MidInfoStr, len(values))
	for i, v := range values {
		this.infoLog.Printf("GetBucketListAndIndex mid i=%v v=%s", i, v)
		midItem := &MidInfoStr{
			Seq: keys[i],
			Uid: 0,
		}
		midMap[v] = midItem
	}

	bucket = &BucketStr{
		Seq:    0,
		Name:   hashName,
		MidMap: midMap,
		Size:   maxIndex,
	}
	return bucket, lastIndex, nil
}

// 获取帖子的创造者 方式加载到redis中的mid详情 均已解析出了帖文创建者并放入mid的hashmap中
// 如果整个hashmap不存在 会返回ErrNil
func (this *RedisOperator) GetMidCreateUser(mid string) (creater uint32, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return creater, err
	}
	value, err := this.redisMasterApi.Hget(mid, MomentCreater)
	if err != nil {
		this.infoLog.Printf("RedisOperator GetMidCreateUser mid=%s key=%s err=%s", mid, MomentCreater, err)
		return creater, err
	}
	tempCreater, err := strconv.ParseUint(value, 10, 32)
	creater = uint32(tempCreater)
	return creater, err
}

// 获取指定用户隐藏某人的帖文列表
func (this *RedisOperator) GetHideOtherMntUidList(uid, inListVer uint32) (outListVer uint32, mapUidToTime map[uint32]uint32, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return 0, nil, err
	}
	keyName := fmt.Sprintf("%v%s", uid, HideOtherMntKey)
	value, err := this.redisMasterApi.Zscore(keyName, fmt.Sprintf("%v", VersionFieldInit))
	if err != nil {
		return 0, nil, err
	}
	outListVer = uint32(value)
	if outListVer == inListVer {
		this.infoLog.Printf("GetHideOtherMntUidList inListVer=%v outListVer=%v not chage", inListVer, outListVer)
		return outListVer, nil, nil
	}

	mapUidToTime, err = this.GetZsetList(keyName)
	if err != nil {
		this.infoLog.Printf("GetHideOtherMntUidList GetZsetList name=% err=%s", keyName, err)
	}
	return outListVer, mapUidToTime, err
}

// 获取指定用户的不分享列表
func (this *RedisOperator) GetNotShareToUidList(uid, inListVer uint32) (outListVer uint32, mapUidToTime map[uint32]uint32, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return outListVer, nil, err
	}
	keyName := fmt.Sprintf("%v%s", uid, NotShareMyMntKey)
	value, err := this.redisMasterApi.Zscore(keyName, fmt.Sprintf("%v", VersionFieldInit))
	if err != nil {
		return outListVer, nil, err
	}
	outListVer = uint32(value)
	if outListVer == inListVer {
		this.infoLog.Printf("GetNotShareToUidList inListVer=%v outListVer=%v not chage", inListVer, outListVer)
		return outListVer, nil, nil
	}

	mapUidToTime, err = this.GetZsetList(keyName)
	if err != nil {
		this.infoLog.Printf("GetNotShareToUidList GetZsetList name=% err=%s", keyName, err)
	}
	return outListVer, mapUidToTime, err
}

// 逆序获取当个桶内指定index之后的mid
func (this *RedisOperator) GetBucketHisList(hashName string, lastIndex uint64, needCnt int) (bucket *BucketStr, err error) {
	if this.redisMasterApi == nil || needCnt == 0 {
		err = ErrNilDbObject
		return nil, err
	}
	//上次去到的seq 等于BaseMntIndex 说明没有了直接返回空
	if lastIndex == BaseMntIndex {
		this.infoLog.Printf("GetBucketHisList hashName=%s lastIndex=%v needCnt=%v", hashName, lastIndex, needCnt)
		return nil, nil
	}

	ret, err := this.redisMasterApi.Exists(hashName)
	if err != nil {
		this.infoLog.Printf("GetBucketHisList redisMasterApi.Exists return err=%s", err)
		return nil, err
	}
	if ret != true {
		this.infoLog.Printf("GetBucketHisList not exist keyName=%s", hashName)
		return nil, ErrNotExistInReids
	}
	value, err := this.redisMasterApi.Hget(hashName, MaxIndexKey)
	if err != nil {
		this.infoLog.Printf("GetBucketHisList this.redisMasterApi.Hget hashName=%s key=%s err=%s", hashName, MaxIndexKey, err)
		return nil, err
	}
	maxIndex, err := strconv.ParseUint(value, 10, 64)
	if err != nil {
		this.infoLog.Printf("GetBucketHisList strconv.ParseUint value=%s err=%s", value, err)
		return nil, err
	}
	// 如果最大的seq 等于BaseMntIndex
	if maxIndex == BaseMntIndex {
		this.infoLog.Printf("GetBucketHisList hashname=%s maxIndex=%v", hashName, maxIndex)
		return nil, nil
	}

	if maxIndex < lastIndex {
		this.infoLog.Printf("GetBucketHisList maxIndex=%v lastIndex=%v hashName=%s", maxIndex, lastIndex, hashName)
		lastIndex = maxIndex
	}

	//首先逆序准备好key然后再Hmgt
	realNeedCnt := needCnt
	if lastIndex-BaseMntIndex < uint64(needCnt) {
		realNeedCnt = int(lastIndex - BaseMntIndex)
	}
	keys := make([]string, realNeedCnt)
	curMaxIndex := lastIndex
	for index := realNeedCnt; index > 0; index -= 1 {
		itemKey := fmt.Sprintf("%v", curMaxIndex)
		curMaxIndex -= 1
		keys[realNeedCnt-index] = itemKey
	}

	values, err := this.redisMasterApi.Hmget(hashName, keys)
	if err != nil {
		this.infoLog.Printf("GetBucketHisList this.redisMasterApi.Hmget hashName=%s keys=%v return err=%s", hashName, keys, err)
		return nil, err
	}

	if len(values) != len(keys) {
		this.infoLog.Printf("GetBucketHisList keysLen=%v valuesLen=%v not equal", len(keys), len(values))
		err = ErrLengthErr
		return nil, err
	}
	midMap := make(map[string]*MidInfoStr, len(values))
	for i, v := range values {
		this.infoLog.Printf("GetBucketHisList mid i=%v v=%s", i, v)
		midItem := &MidInfoStr{
			Seq: keys[i],
			Uid: 0,
		}
		midMap[v] = midItem
	}

	bucket = &BucketStr{
		Seq:    0,
		Name:   hashName,
		MidMap: midMap,
		Size:   maxIndex,
	}
	return bucket, nil
}

// 逆序获取当个桶内指定index之后的mid
func (this *RedisOperator) GetBucketHisListAndIndex(hashName string, lastIndex uint64, needCnt int) (bucket *BucketStr, outIndex uint64, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return nil, outIndex, err
	}
	if lastIndex == BaseMntIndex { // 空的hashmap设置lastIndex 为 0
		outIndex = 0
		return nil, outIndex, nil
	}
	ret, err := this.redisMasterApi.Exists(hashName)
	if err != nil {
		this.infoLog.Printf("GetBucketHisListAndIndex redisMasterApi.Exists return err=%s", err)
		return nil, outIndex, err
	}
	if ret != true {
		this.infoLog.Printf("GetBucketHisListAndIndex not exist keyName=%s", hashName)
		return nil, outIndex, ErrNotExistInReids
	}
	value, err := this.redisMasterApi.Hget(hashName, MaxIndexKey)
	if err != nil {
		this.infoLog.Printf("GetBucketHisListAndIndex this.redisMasterApi.Hget hashName=%s key=%s err=%s", hashName, MaxIndexKey, err)
		return nil, outIndex, err
	}
	maxIndex, err := strconv.ParseUint(value, 10, 64)
	if err != nil {
		this.infoLog.Printf("GetBucketHisListAndIndex strconv.ParseUint value=%s err=%s", value, err)
		return nil, outIndex, err
	}

	if maxIndex == BaseMntIndex {
		this.infoLog.Printf("GetBucketHisListAndIndex hashname=%s maxIndex=%v", hashName, maxIndex)
		outIndex = 0
		return nil, outIndex, nil
	}

	if maxIndex < lastIndex {
		this.infoLog.Printf("GetBucketHisListAndIndex maxIndex=%v lastIndex=%v hashName=%s", maxIndex, lastIndex, hashName)
		lastIndex = maxIndex
	}

	//首先逆序准备好key然后在Hmgt
	realNeedCnt := needCnt
	if lastIndex-BaseMntIndex < uint64(needCnt) {
		realNeedCnt = int(lastIndex - BaseMntIndex)
	}

	if (lastIndex - BaseMntIndex) < uint64(needCnt) { // pagesize > hsz 时，一次获取完毕这里设置 0 ，防止获取历史重复
		outIndex = 0 // 整张hash 表 一次性获取
	} else {
		outIndex = lastIndex - uint64(needCnt) // 设置下次获取历史的起始位置
	}

	keys := make([]string, realNeedCnt)
	curMaxIndex := lastIndex
	for index := realNeedCnt; index > 0; index -= 1 {
		itemKey := fmt.Sprintf("%v", curMaxIndex)
		curMaxIndex -= 1
		keys[realNeedCnt-index] = itemKey
	}

	values, err := this.redisMasterApi.Hmget(hashName, keys)
	if err != nil {
		this.infoLog.Printf("GetBucketHisListAndIndex this.redisMasterApi.Hmget hashName=%s keys=%v return err=%s", hashName, keys, err)
		return nil, outIndex, err
	}

	if len(values) != len(keys) {
		this.infoLog.Printf("GetBucketHisListAndIndex keysLen=%v valuesLen=%v not equal", len(keys), len(values))
		err = ErrLengthErr
		return nil, outIndex, err
	}
	midMap := make(map[string]*MidInfoStr, len(values))
	for i, v := range values {
		// this.infoLog.Printf("GetBucketHisListAndIndex mid i=%v v=%s", i, v)
		midItem := &MidInfoStr{
			Seq: keys[i],
			Uid: 0,
		}
		midMap[v] = midItem
	}

	bucket = &BucketStr{
		Seq:    0,
		Name:   hashName,
		MidMap: midMap,
		Size:   maxIndex,
	}
	return bucket, outIndex, nil
}

func (this *RedisOperator) GetSpecificSeqMid(head *common.HeadV2, hashName string, seq uint64) (mid string, err error) {
	if this.redisMasterApi == nil {
		err = ErrNilDbObject
		return mid, err
	}
	ret, err := this.redisMasterApi.Exists(hashName)
	if err != nil {
		this.infoLog.Printf("GetSpecificSeqMid redisMasterApi.Exists return err=%s", err)
		return mid, err
	}
	if ret != true {
		this.infoLog.Printf("GetSpecificSeqMid not exist keyName=%s", hashName)
		return mid, ErrNotExistInReids
	}
	value, err := this.redisMasterApi.Hget(hashName, MaxIndexKey)
	if err != nil {
		this.infoLog.Printf("GetSpecificSeqMid this.redisMasterApi.Hget hashName=%s key=%s err=%s", hashName, MaxIndexKey, err)
		return mid, err
	}
	maxIndex, err := strconv.ParseUint(value, 10, 64)
	if err != nil {
		this.infoLog.Printf("GetBucketList strconv.ParseUint value=%s err=%s", value, err)
		return mid, err
	}

	if maxIndex < seq+BaseMntIndex {
		this.infoLog.Printf("GetSpecificSeqMid maxIndex=%v seq=%v not exist", maxIndex, seq)
		return mid, ErrInvalidParam
	}

	seqIndex := fmt.Sprintf("%v", maxIndex-seq+1)
	mid, err = this.redisMasterApi.Hget(hashName, seqIndex)
	if err != nil {
		this.infoLog.Printf("GetSpecificSeqMid hashname=%s seqIndex=%s err=%s", hashName, seqIndex, err)
		return mid, err
	}
	return mid, nil
}

func (this *RedisOperator) GetUserMomentInfo(head *common.HeadV2, userid uint32) (momentCnt, likeCnt uint32, err error) {
	if head == nil || this.redisMasterApi == nil || userid == 0 {
		err = ErrNilDbObject
		return momentCnt, likeCnt, err
	}
	// this.infoLog.Printf("GetUserMomentInfo fromId=%v uid=%v", head.Uid, userid)
	hashName := GetUserInfoHashMapName(userid)
	exists, err := this.redisMasterApi.Exists(hashName)
	if err != nil {
		this.infoLog.Printf("GetUserMomentInfo fromId=%v uid=%v err=%s", head.Uid, userid, err)
		return momentCnt, likeCnt, err
	}
	this.infoLog.Printf("GetUserMomentInfo fromId=%v uid=%v exist=%v", head.Uid, userid, exists)
	if !exists {
		err = ErrNotExistInReids
		this.infoLog.Printf("GetUserMomentInfo fromId=%v uid=%v not exist in redis", head.Uid, userid)
		return momentCnt, likeCnt, err
	}
	userInfo := UserInfoStr{
		MomentCnt:      0,
		LikeCnt:        0,
		OtherMomentCnt: 0,
		OtherLikeCnt:   0,
	}
	keyValue, err := this.redisMasterApi.Hgetall(hashName)
	if err != nil {
		this.infoLog.Printf("GetUserMomentInfo fromId=%v uid=%v redisMasterApi.Hgetall err=%s", head.Uid, userid, err)
		return momentCnt, likeCnt, err
	}
	err = this.redisMasterApi.Expire(hashName, ExpirePeriod)
	if err != nil {
		this.infoLog.Printf("GetUserMomentInfo expire key=%s err=%s", hashName, err)
	}

	// 获取成功
	for k, v := range keyValue {
		switch k {
		case MomentLikeCountKey:
			value, err := strconv.ParseUint(v, 10, 32)
			if err != nil {
				this.infoLog.Printf("GetUserMomentInfo strconv.ParseUint err uid=%v v=%s err=%s",
					head.Uid,
					v,
					err)
				userInfo.LikeCnt = 0 // set moment count zero
			} else {
				userInfo.LikeCnt = uint32(value)
			}
		case MomentCountKey:
			value, err := strconv.ParseUint(v, 10, 32)
			if err != nil {
				this.infoLog.Printf("GetUserMomentInfo strconv.ParseUint err uid=%v v=%s err=%s",
					head.Uid,
					v,
					err)
				userInfo.MomentCnt = 0 // set moment count zero
			} else {
				userInfo.MomentCnt = uint32(value)
			}
		case MomentLikeCountForOther:
			value, err := strconv.ParseUint(v, 10, 32)
			if err != nil {
				this.infoLog.Printf("GetUserMomentInfo strconv.ParseUint err uid=%v v=%s err=%s",
					head.Uid,
					v,
					err)
				userInfo.OtherLikeCnt = 0 // set moment count zero
			} else {
				userInfo.OtherLikeCnt = uint32(value)
			}
		case MomentCountForOther:
			value, err := strconv.ParseUint(v, 10, 32)
			if err != nil {
				this.infoLog.Printf("GetUserMomentInfo strconv.ParseUint err uid=%v v=%s err=%s",
					head.Uid,
					v,
					err)
				userInfo.OtherMomentCnt = 0 // set moment count zero
			} else {
				userInfo.OtherMomentCnt = uint32(value)
			}
		default:
			this.infoLog.Printf("GetUserMomentInfo unknow key=%s value=%s", k, v)
		}
	}

	if head.Uid == userid {
		momentCnt = userInfo.MomentCnt
		likeCnt = userInfo.LikeCnt
	} else {
		momentCnt = userInfo.OtherMomentCnt
		likeCnt = userInfo.OtherLikeCnt
	}

	if likeCnt > 1000000 || momentCnt > 1000000 {
		this.infoLog.Printf("GetUserMomentInfo uid=%v Fatal error!like_cnt=%v or mnt_cnt = %v is invalid!", head.Uid, likeCnt, momentCnt)
		momentCnt = 0
		likeCnt = 0
	}

	return momentCnt, likeCnt, nil
}

func GetUserInfoHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v_user_info", uid)
	return hashName
}

func (this *RedisOperator) Zscore(zsetName, key string) (value int64, err error) {
	if this.redisMasterApi == nil || zsetName == "" {
		err = ErrNilDbObject
		return value, err
	}
	exists, err := this.redisMasterApi.Exists(zsetName)
	if err != nil {
		this.infoLog.Printf("Zscore zsetName=%v key=%s err=%s", zsetName, key, err)
		return value, err
	}
	// this.infoLog.Printf("GetUserMomentInfo fromId=%v uid=%v exist=%v", head.Uid, userid, exists)
	if !exists {
		err = ErrNotExistInReids
		this.infoLog.Printf("Zscore zsetName=%v key=%s not exist in redis", zsetName, key)
		return value, err
	}

	tempValue, err := this.redisMasterApi.Zscore(zsetName, key)
	if err != nil {
		this.infoLog.Printf("Zscore zsetName=%v key=%s redisMasterApi.Zscore err=%s", zsetName, key, err)
		return value, err
	}
	value = int64(tempValue)
	return value, nil
}
