package main

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/HT_GOGO/gotcp"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/proto"
	nsq "github.com/nsqio/go-nsq"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_cache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/HT_GOGO/gotcp/tcpfw/project/mnt_family/mnt_server/mnt_cache_dbd/util"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog             *log.Logger
	ssdbOperator        *util.SsdbOperator
	relationApi         *common.RelationApi
	userCacheApi        *common.UserCacheApi
	writeAgentMastreApi *common.SrvToSrvApiV2
	writeAgentSlaveApi  *common.SrvToSrvApiV2
	globalProducer      *nsq.Producer
	postMntTopic        string
	userActionTopic     string
	gMapShortLangToNum  map[string]uint32
	db                  *sql.DB
	redisApi            *common.RedisApi
)

const (
	SSDB_RET_SUCCESS     = 0
	SSDB_RET_EXEC_FAILED = 1
	SSDB_RET_NOT_EXIST   = 100
)

const (
	NSQ_EVENT_LIKE               = 1
	NSQ_EVENT_CANCLE_LIKE        = 2
	NSQ_EVENT_COMMENT            = 3
	NSQ_EVENT_DELETE_COMMENT     = 4
	NSQ_EVENT_REPORT             = 5
	NSQ_EVENT_FAVORITE           = 6
	NSQ_EVENT_SHARE              = 7
	NSQ_EVENT_BUCKET_TO_ME       = 8
	NSQ_EVENT_BUCKET_TO_FOLLOW   = 9
	NSQ_EVENT_BUCKET_TO_ALL      = 10
	NSQ_EVENT_OP_RESTORE         = 11
	NSQ_EVENT_OP_HIDE            = 12
	NSQ_EVENT_OP_DELETE          = 13
	NSQ_EVENT_OP_FOLLOWER_ONLY   = 14
	NSQ_EVENT_DELETE_USER        = 15
	NSQ_EVENT_RESTORE_USER       = 16
	NSQ_EVENT_DELETE_MNT_BY_USER = 17
	NSQ_EVENT_CANCLE_FAVORITE    = 18
)

func initShortLangToNum() {
	gMapShortLangToNum["en"] = 1
	gMapShortLangToNum["zh"] = 2
	gMapShortLangToNum["zh-cn"] = 2
	gMapShortLangToNum["zh-hans"] = 2
	gMapShortLangToNum["zh-yue"] = 2
	gMapShortLangToNum["yue"] = 2
	gMapShortLangToNum["zh-chs"] = 2
	gMapShortLangToNum["zh-hant"] = 2
	gMapShortLangToNum["zh-cht"] = 2
	gMapShortLangToNum["zh-tw"] = 2
	gMapShortLangToNum["ja"] = 5
	gMapShortLangToNum["ko"] = 6
	gMapShortLangToNum["es"] = 7
	gMapShortLangToNum["fr"] = 8
	gMapShortLangToNum["pt"] = 9
	gMapShortLangToNum["de"] = 10
	gMapShortLangToNum["it"] = 11
	gMapShortLangToNum["ru"] = 12
	gMapShortLangToNum["ar"] = 13
	gMapShortLangToNum["tr"] = 14
	gMapShortLangToNum["fa"] = 15
	gMapShortLangToNum["az"] = 16
	gMapShortLangToNum["th"] = 17
	gMapShortLangToNum["id"] = 18
	gMapShortLangToNum["ms"] = 19
	gMapShortLangToNum["tl"] = 20
	gMapShortLangToNum["vi"] = 21
	gMapShortLangToNum["nl"] = 22
	gMapShortLangToNum["da"] = 23
	gMapShortLangToNum["fi"] = 24
	gMapShortLangToNum["no"] = 25
	gMapShortLangToNum["sv"] = 26
	gMapShortLangToNum["ca"] = 27
	gMapShortLangToNum["he"] = 28
	gMapShortLangToNum["pl"] = 29
	gMapShortLangToNum["el"] = 30
	gMapShortLangToNum["cs"] = 31
	gMapShortLangToNum["uk"] = 32
	gMapShortLangToNum["ro"] = 33
	gMapShortLangToNum["hu"] = 34
	gMapShortLangToNum["bg"] = 35
	gMapShortLangToNum["hr"] = 36
	gMapShortLangToNum["sr"] = 37
	gMapShortLangToNum["sk"] = 38
	gMapShortLangToNum["hi"] = 39
	gMapShortLangToNum["af"] = 40
	gMapShortLangToNum["eo"] = 41
	gMapShortLangToNum["yi"] = 42
	gMapShortLangToNum["lt"] = 43
	gMapShortLangToNum["ur"] = 44
	gMapShortLangToNum["ta"] = 45
	gMapShortLangToNum["bn"] = 46
	gMapShortLangToNum["mn"] = 47
	gMapShortLangToNum["bs"] = 48
	gMapShortLangToNum["pa"] = 49
	gMapShortLangToNum["ne"] = 50
	gMapShortLangToNum["sq"] = 51
	gMapShortLangToNum["hy"] = 52
	gMapShortLangToNum["eu"] = 53
	gMapShortLangToNum["be"] = 54
	gMapShortLangToNum["ceb"] = 55
	gMapShortLangToNum["et"] = 56
	gMapShortLangToNum["gl"] = 57
	gMapShortLangToNum["ka"] = 58
	gMapShortLangToNum["gu"] = 59
	gMapShortLangToNum["ht"] = 60
	gMapShortLangToNum["ha"] = 61
	gMapShortLangToNum["hmn"] = 62
	gMapShortLangToNum["is"] = 63
	gMapShortLangToNum["ig"] = 64
	gMapShortLangToNum["ga"] = 65
	gMapShortLangToNum["jv"] = 66
	gMapShortLangToNum["kn"] = 67
	gMapShortLangToNum["km"] = 68
	gMapShortLangToNum["lo"] = 69
	gMapShortLangToNum["la"] = 70
	gMapShortLangToNum["lv"] = 71
	gMapShortLangToNum["mk"] = 72
	gMapShortLangToNum["mt"] = 73
	gMapShortLangToNum["mi"] = 74
	gMapShortLangToNum["mr"] = 75
	gMapShortLangToNum["sl"] = 76
	gMapShortLangToNum["so"] = 77
	gMapShortLangToNum["sw"] = 78
	gMapShortLangToNum["te"] = 79
	gMapShortLangToNum["cy"] = 80
	gMapShortLangToNum["yo"] = 81
	gMapShortLangToNum["zu"] = 82
	// gMapShortLangToNum["zh-CN"] = 83
	gMapShortLangToNum["ku"] = 84
	gMapShortLangToNum["mz"] = 85
	gMapShortLangToNum["ml"] = 86
	gMapShortLangToNum["or"] = 87
	gMapShortLangToNum["ny"] = 88
	gMapShortLangToNum["mg"] = 89
	gMapShortLangToNum["st"] = 90
	gMapShortLangToNum["my"] = 91
	gMapShortLangToNum["si"] = 92
	gMapShortLangToNum["su"] = 93
	gMapShortLangToNum["kk"] = 94
	gMapShortLangToNum["tg"] = 95
	gMapShortLangToNum["uz"] = 96
	// gMapShortLangToNum["zh-TW"] = 97
	gMapShortLangToNum["am"] = 98
	gMapShortLangToNum["vl"] = 99
	gMapShortLangToNum["nv"] = 100
	gMapShortLangToNum["ky"] = 101
	gMapShortLangToNum["fy"] = 102
	gMapShortLangToNum["tp"] = 103
	gMapShortLangToNum["oj"] = 104
	gMapShortLangToNum["dk"] = 105
	gMapShortLangToNum["ap"] = 106
	gMapShortLangToNum["co"] = 107
	gMapShortLangToNum["xh"] = 108
	gMapShortLangToNum["tn"] = 109
	gMapShortLangToNum["ts"] = 110
	gMapShortLangToNum["ss"] = 111
	gMapShortLangToNum["ve"] = 112
	gMapShortLangToNum["nr"] = 113
	gMapShortLangToNum["lf"] = 114
	gMapShortLangToNum["ia"] = 115
	gMapShortLangToNum["io"] = 116
	gMapShortLangToNum["gd"] = 117
	// gMapShortLangToNum["ku"] = 118
	// gMapShortLangToNum["ku"] = 119
	gMapShortLangToNum["lh"] = 120
	gMapShortLangToNum["do"] = 121
	gMapShortLangToNum["va"] = 122
	gMapShortLangToNum["hw"] = 123
	gMapShortLangToNum["bl"] = 124
	gMapShortLangToNum["ps"] = 125
	gMapShortLangToNum["aa"] = 126
	gMapShortLangToNum["qu"] = 127
	gMapShortLangToNum["nh"] = 128
	return
}

func IsLegalLength(langNum, byteLen, words uint32) (ret bool) {
	// China/Japan/Korea need 30 words
	// zh-CN = 2
	// ja = 5
	// ko = 6
	if langNum == util.ChineseSimple || langNum == util.Japanse || langNum == util.Korean {
		if words >= util.LimitWords {
			return true
		} else {
			return false
		}
	} else {
		if byteLen >= util.LimitByteLen {
			return true
		} else {
			return false
		}
	}
}

func IsLegalNativeLang(national string, langNum uint32) (ret bool) {
	//US = 1
	//UK = 1
	//CA = 1
	//AU = 1
	//NZ = 1

	//BR = 9
	//PT = 9

	//CN = 2
	//TW = 3
	//HK = 3
	//RU = 12
	//JP = 5
	//KR = 6

	switch langNum {
	case 1: // english
		if national == "US" ||
			national == "UK" ||
			national == "CA" ||
			national == "AU" ||
			national == "NZ" {
			return true
		}
	case 2: // 简体中文
		if national == "CN" {
			ret = true
		}
	case 3: // 繁体中文
		if national == "TW" || national == "HK" {
			ret = true
		}
	case 5: // Japanese
		if national == "JP" {
			ret = true
		}
	case 6: //Korana
		if national == "KR" {
			ret = true
		}
	case 9: // Portuges
		if national == "BR" || national == "PT" {
			ret = true
		}
	case 12: // Russia
		if national == "RU" {
			ret = true
		}
	default:
		ret = true
	}
	return ret
}

func Uint32IsInSlice(targetList []uint32, iterm uint32) bool {
	if len(targetList) == 0 {
		return false
	}
	for _, v := range targetList {
		if iterm == v {
			return true
		}
	}
	return false
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	// infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gomntdbd/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch head.Cmd {
	case uint32(ht_moment_cache.CMD_TYPE_CMD_GET_ZSET_LIST):
		go ProcGetZsetList(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_UPDATE_MID_HASH_MAP):
		go ProcUpdateMidHashMap(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_POST_MID_REQ):
		go ProcPostMidReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_GET_MID_HASH_MAP):
		go ProcGetMidHashMap(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_ZRANGE_ZSET_LIST):
		go ProcZrangeList(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_ZGET_REQ):
		go ProcZgetReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_HINC_HASH_MAP):
		go ProcHincReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_HSCAN_REQ):
		go ProcHscanReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_SET_USER_ALL_MNT_STATUS_REQ):
		go ProcSetUserAllMntReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_MNT_STATUS_REQ):
		go ProcModMntStatReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_OP_UID_REQ):
		go ProcOpUidService(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_BACK_END_OP_UID_REQ):
		go ProcBackEndOpUidReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_MNT_SHOW_FIELD_REQ):
		go ProcModMidShowFieldReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_GET_SEQ_MID_REQ):
		go ProcGetSeqMidReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_MNT_BUCKET_REQ):
		go ProcModMntBucketService(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_POST_CMNT_REQ):
		go ProcPostCmntService(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_MOD_CMNT_STAT_REQ):
		go ProcModCmntStatReq(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_POST_LIKE_REQ):
		go ProcPostLikeService(c, head, packet)
	case uint32(ht_moment_cache.CMD_TYPE_CMD_GET_USER_INFO):
		go ProcGetUserInfo(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

// 1.proc get zset list
func ProcGetZsetList(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetZsetList no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetZsetListRspbody = &ht_moment_cache.GetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetZsetList invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/get_zset_list_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetZsetList proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.GetZsetListRspbody = &ht_moment_cache.GetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetZsetListReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcGetZsetList GetGetZsetListReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.GetZsetListRspbody = &ht_moment_cache.GetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// param check
	if len(subReqBody.GetZsetName()) == 0 {
		infoLog.Printf("ProcGetZsetList zsetname lenght is 0 uid=%v", head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetZsetListRspbody = &ht_moment_cache.GetZsetListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("zset name error"),
			},
		}
		return false
	}
	// infoLog.Printf("ProcGetZsetList zsetname=%s indesStart=%v indexStop=%v",
	// 	subReqBody.GetZsetName(),
	// 	subReqBody.GetIndexStart(),
	// 	subReqBody.GetIndexStop())

	keys, scores, err := ssdbOperator.GetZsetList(
		subReqBody.GetZsetName(),
		subReqBody.GetIndexStart(),
		subReqBody.GetIndexStop())
	if err != nil {
		infoLog.Printf("ProcGetZsetList ssdbOperator.GetZsetList return err=%s zsetname=%s indesStart=%v indexStop=%v",
			err,
			subReqBody.GetZsetName(),
			subReqBody.GetIndexStart(),
			subReqBody.GetIndexStop())
		attr := "gomntdbd/get_zset_list_err"
		libcomm.AttrAdd(attr, 1)
		if err == util.ErrNotExistInSsdb {
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_NOT_EXIST_IN_SSDB)
			rspBody.GetZsetListRspbody = &ht_moment_cache.GetZsetListRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb not exist"),
				},
			}
		} else {
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.GetZsetListRspbody = &ht_moment_cache.GetZsetListRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
		}
		return false
	}

	keyLen := len(keys)
	keyScore := make([]uint32, 2*keyLen)
	keyScoreIndex := 0
	for index := 0; index < keyLen; index++ {
		keyUint, err := strconv.ParseUint(keys[index], 10, 32)
		if err != nil {
			infoLog.Printf("ProcGetZsetList strconv.ParseUint err=%s index=%v value=%s", err, index, keys[index])
			continue
		}

		scoreUint, err := strconv.ParseUint(scores[index], 10, 32)
		if err != nil {
			infoLog.Printf("ProcGetZsetList strconv.ParseUint err=%s index=%v value=%s", err, index, scores[index])
			continue
		}
		keyScore[keyScoreIndex] = uint32(keyUint)
		keyScoreIndex = keyScoreIndex + 1
		keyScore[keyScoreIndex] = uint32(scoreUint)
		keyScoreIndex = keyScoreIndex + 1
	}
	// 如果整张列表为空时 添加一个0 进去
	if len(keyScore) == 0 {
		listVersion := 0xffff - uint32(time.Now().Unix())
		keyScore = append(keyScore, util.VersionFieldInt)
		keyScore = append(keyScore, listVersion)
	}
	infoLog.Printf("ProcGetZsetList zsetName=%s indexStart=%v indexEnd=%v keySocreLen=%v",
		subReqBody.GetZsetName(),
		subReqBody.GetIndexStart(),
		subReqBody.GetIndexStop(),
		len(keyScore))
	// send resp
	rspBody.GetZsetListRspbody = &ht_moment_cache.GetZsetListRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		KeyScore: keyScore,
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		SetZsetListReqbody: &ht_moment_cache.SetZsetListReqBody{
			ZsetName: proto.String(subReqBody.GetZsetName()),
			KeyScore: keyScore,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcGetZsetList proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/set_zset_list_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_ZSET_LIST)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcGetZsetList uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/get_zset_list_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.proc update hash map 用于发布信息流详细内容
func ProcUpdateMidHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcUpdateMidHashMap no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.UpdateMidHashMapRspbody = &ht_moment_cache.UpdateMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcUpdateMidHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/update_hash_map_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateMidHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.UpdateMidHashMapRspbody = &ht_moment_cache.UpdateMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetUpdateMidHashMapReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcUpdateMidHashMap GetUpdateMidHashMapReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.UpdateMidHashMapRspbody = &ht_moment_cache.UpdateMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// param check
	hashName := subReqBody.GetHashName()
	keyValues := subReqBody.GetKeyValue()
	if len(hashName) == 0 || len(keyValues) == 0 {
		infoLog.Printf("ProcUpdateMidHashMap invalid param uid=%v hashName=%s keyValueLen=%v", head.Uid, hashName, len(keyValues))
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.UpdateMidHashMapRspbody = &ht_moment_cache.UpdateMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param error"),
			},
		}
		return false
	}
	// infoLog.Printf("ProcUpdateMidHashMap hashName=%s keyValue=%v", hashName, keyValues)
	err = ssdbOperator.MultiHset(hashName, keyValues)
	if err != nil {
		infoLog.Printf("ProcUpdateMidHashMap ssdbOperator.MultiHset return err=%s hashName=%s keyValues=%v",
			err,
			hashName,
			keyValues)
		attr := "gomntdbd/multi_hset_err"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
		rspBody.UpdateMidHashMapRspbody = &ht_moment_cache.UpdateMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	}

	// send resp
	rspBody.UpdateMidHashMapRspbody = &ht_moment_cache.UpdateMidHashMapRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		SetMidHashMapReqbody: &ht_moment_cache.SetMidHashMapReqBody{
			HashName: proto.String(hashName),
			KeyValue: keyValues,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcUpdateMidHashMap proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/set_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_MID_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	totalProcTime := packet.CalcProcessTime()

	infoLog.Printf("ProcUpdateMidHashMap uid=%v hashName=%s totalProcTime=%v", head.Uid, hashName, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/update_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 3.proc post mid req  用于发布mid到相关的桶内
func ProcPostMidReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcPostMidReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostMidRspbody = &ht_moment_cache.PostMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcPostMidReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/post_mid_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcPostMidReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostMidRspbody = &ht_moment_cache.PostMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetPostMidReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcPostMidReq GetPostMidReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostMidRspbody = &ht_moment_cache.PostMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// param check
	mid := subReqBody.GetMid()
	if mid < 10000 {
		infoLog.Printf("ProcPostMidReq invalid param uid=%v mid=%v", head.Uid, mid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostMidRspbody = &ht_moment_cache.PostMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param error"),
			},
		}
		return false
	}

	infoLog.Printf("ProcPostMidReq version=%v mid=%v byteLen=%v words=%v StrLangType=%s",
		subReqBody.GetVersion(),
		mid,
		subReqBody.GetContentByteLen(),
		subReqBody.GetContentWords(),
		subReqBody.GetStrLangType())

	// send resp
	rspBody.PostMidRspbody = &ht_moment_cache.PostMidRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// Step1: inc moment_count and moment_count_for_others need update redis
	// 2017-10-25 首先增加自己的帖子计数
	err = ssdbOperator.StatUserMntCount(head.Uid, 1)
	if err != nil {
		infoLog.Printf("ProcPostMidReq ssdbOperator.StatUserMntCount failed uid=%v err=%s", head.Uid, err)
	}
	//Step2: clear moment_count in redis
	err = ClearUserInfoCacheInRedis(head, head.Uid)
	if err != nil {
		infoLog.Printf("ProcPostMidReq ClearUserInfoCacheInRedis failed err=%s", err)
	}

	//Step3: add to UID#LIST
	var updateList []*ht_moment_cache.HashMapIterm
	hashName := fmt.Sprintf("%v#LIST", head.Uid)
	index, err := ssdbOperator.InsertMidIntoHashMap(hashName, mid)
	if err != nil {
		infoLog.Printf("ProcPostMidReq InsertMidIntoHashMap failed uid=%v mid=%v hashName=%s",
			head.Uid,
			mid,
			hashName)
	} else {
		// add to upate list
		iterm := &ht_moment_cache.HashMapIterm{
			HashName: proto.String(hashName),
			Key:      proto.String(fmt.Sprintf("%v", index)),
			Value:    proto.String(fmt.Sprintf("%v", mid)),
			Expire:   proto.Uint64(util.HashMapExpireTime),
		}
		updateList = append(updateList, iterm)
	}
	//Step4: add to ALL#LIST
	hashName = "ALL#LIST"
	index, err = ssdbOperator.InsertMidIntoHashMap(hashName, mid)
	if err != nil {
		infoLog.Printf("ProcPostMidReq InsertMidIntoHashMap failed uid=%v mid=%v hashName=%s",
			head.Uid,
			mid,
			hashName)
	} else {
		// // add to upate list
		// iterm := &ht_moment_cache.HashMapIterm{
		// 	HashName: proto.String(hashName),
		// 	Key:      proto.String(fmt.Sprintf("%v", index)),
		// 	Value:    proto.String(fmt.Sprintf("%v", mid)),
		// }
		// updateList = append(updateList, iterm)
	}
	// 2017-10-15 在发帖的时候就只给到自己的桶的标示，only_to_me=0表示还是老是逻辑，only_to_me=1 表示只存储到自己的桶
	if subReqBody.GetOnlyToMe() == 1 {
		infoLog.Printf("ProcPostMidReq only to me uid=%v", head.Uid)
		// add static
		attr := "gomntdbd/get_momnet_only_to_me"
		libcomm.AttrAdd(attr, 1)
	} else {
		// 如果是老的逻辑 继续增加他人的帖子计数
		err = ssdbOperator.StatUserMntCountForOther(head.Uid, 1)
		if err != nil {
			infoLog.Printf("ProcPostMidReq ssdbOperator.StatUserMntCountForOther failed uid=%v err=%s", head.Uid, err)
		}
		//Step2: update moment_count in redis
		err = ClearUserInfoCacheInRedis(head, head.Uid)
		if err != nil {
			infoLog.Printf("ProcPostMidReq ClearUserInfoCacheInRedis failed err=%s", err)
		}

		// Step5: Get Follower list
		followerList, err := relationApi.GetFollowerLists(head.Uid)
		if err != nil {
			infoLog.Printf("ProcPostMidReq relationApi.GetFollowerLists failed uid=%v err=%s", head.Uid, err)

			// add static
			attr := "gomntdbd/get_follower_list_faild"
			libcomm.AttrAdd(attr, 1)
		} else {
			//添加粉丝bucket
			// infoLog.Printf("ProcPostMidReq followerListSize=%v", len(followerList))
			for _, v := range followerList {
				// infoLog.Printf("ProcPostMidReq index=%v follower=%v", i, v)
				hashName := fmt.Sprintf("%v#FRIEND", v)
				index, err := ssdbOperator.InsertMidIntoHashMap(hashName, mid)
				if err != nil {
					infoLog.Printf("ProcPostMidReq InsertMidIntoHashMap failed uid=%v mid=%v hashName=%s",
						head.Uid,
						mid,
						hashName)
				} else {
					// add to upate list 存在则更新
					iterm := &ht_moment_cache.HashMapIterm{
						HashName: proto.String(hashName),
						Key:      proto.String(fmt.Sprintf("%v", index)),
						Value:    proto.String(fmt.Sprintf("%v", mid)),
						Expire:   proto.Uint64(util.HashMapExpireTime),
					}
					updateList = append(updateList, iterm)
				}
			}
		}

		// Step6: Get teach and learn lang from user cache
		national, nativeLang, teachLang, learnLang, err := userCacheApi.QueryLangInfo(head.Uid)
		if err != nil {
			infoLog.Printf("ProcPostMidReq userCacheApi.QueryTeachAndLearnLang failed uid=%v err=%s", head.Uid, err)

			// add static
			attr := "gomntdbd/get_teach_and_learn_lang_faild"
			libcomm.AttrAdd(attr, 1)
		} else {
			var teachToLearn []string
			langType := strings.ToLower(string(subReqBody.GetStrLangType()))
			if langNumber, ok := gMapShortLangToNum[langType]; ok {
				if ok := Uint32IsInSlice(teachLang, langNumber); ok {
					for _, learn := range learnLang {
						teachToLearn = append(teachToLearn, fmt.Sprintf("LANG#%v-%v", langNumber, learn))
					}
				} else if ok := Uint32IsInSlice(learnLang, langNumber); ok {
					for _, teach := range teachLang {
						teachToLearn = append(teachToLearn, fmt.Sprintf("LANG#%v-%v", teach, langNumber))
					}
				} else { // 不在学习或教学语言中的放入所有桶中
					// add static
					attr := "gomntdbd/moment_lang_not_match"
					libcomm.AttrAdd(attr, 1)
					for _, teach := range teachLang {
						for _, learn := range learnLang {
							teachToLearn = append(teachToLearn, fmt.Sprintf("LANG#%v-%v", teach, learn))
						}
					}
				}
				// infoLog.Printf("ProcPostMidReq teachToLearn=%v", teachToLearn)
				for _, v := range teachToLearn {
					// infoLog.Printf("ProcPostMidReq index=%v lang=%s", i, v)
					index, err := ssdbOperator.InsertMidIntoHashMap(v, mid)
					if err != nil {
						infoLog.Printf("ProcPostMidReq InsertMidIntoHashMap failed uid=%v mid=%v hashName=%s",
							head.Uid,
							mid,
							v)

					} else {
						// add to upate list
						iterm := &ht_moment_cache.HashMapIterm{
							HashName: proto.String(v),
							Key:      proto.String(fmt.Sprintf("%v", index)),
							Value:    proto.String(fmt.Sprintf("%v", mid)),
							Expire:   proto.Uint64(0),
						}
						updateList = append(updateList, iterm)
					}
				}
			} else if len(langType) == 0 {
				//如果语言为空 例如纯图片或者语言 保持原来的放入关系
				for _, teach := range teachLang {
					for _, learn := range learnLang {
						teachToLearn = append(teachToLearn, fmt.Sprintf("LANG#%v-%v", teach, learn))
					}
				}
				// infoLog.Printf("ProcPostMidReq teachToLearn=%v", teachToLearn)
				for _, v := range teachToLearn {
					// infoLog.Printf("ProcPostMidReq index=%v lang=%s", i, v)
					index, err := ssdbOperator.InsertMidIntoHashMap(v, mid)
					if err != nil {
						infoLog.Printf("ProcPostMidReq InsertMidIntoHashMap failed uid=%v mid=%v hashName=%s",
							head.Uid,
							mid,
							v)

					} else {
						// add to upate list
						iterm := &ht_moment_cache.HashMapIterm{
							HashName: proto.String(v),
							Key:      proto.String(fmt.Sprintf("%v", index)),
							Value:    proto.String(fmt.Sprintf("%v", mid)),
							Expire:   proto.Uint64(0),
						}
						updateList = append(updateList, iterm)
					}
				}
			} else {
				infoLog.Printf("ProcPostMidReq lang match did not found moment lang=%s", langType)
			}

			// add native lang
			nativeHashMap := fmt.Sprintf("LANG#%v", nativeLang)
			index, err = ssdbOperator.InsertMidIntoHashMap(nativeHashMap, mid)
			if err != nil {
				infoLog.Printf("ProcPostMidReq InsertMidIntoHashMap failed uid=%v mid=%v hashName=%s",
					head.Uid,
					mid,
					nativeHashMap)
			} else {
				// add to upate list
				iterm := &ht_moment_cache.HashMapIterm{
					HashName: proto.String(nativeHashMap),
					Key:      proto.String(fmt.Sprintf("%v", index)),
					Value:    proto.String(fmt.Sprintf("%v", mid)),
					Expire:   proto.Uint64(0),
				}
				updateList = append(updateList, iterm)
			}

			// add to learn bucket
			for _, learn := range learnLang {
				learnHashMap := fmt.Sprintf("LANG#-%v", learn)
				index, err = ssdbOperator.InsertMidIntoHashMap(learnHashMap, mid)
				if err != nil {
					infoLog.Printf("ProcPostMidReq InsertMidIntoHashMap failed uid=%v mid=%v hashName=%s",
						head.Uid,
						mid,
						learnHashMap)

				} else {
					// add to upate list
					iterm := &ht_moment_cache.HashMapIterm{
						HashName: proto.String(learnHashMap),
						Key:      proto.String(fmt.Sprintf("%v", index)),
						Value:    proto.String(fmt.Sprintf("%v", mid)),
						Expire:   proto.Uint64(0),
					}
					updateList = append(updateList, iterm)
				}
			}

			// 放入学习桶 国籍 + 母语
			if langNumber, ok := gMapShortLangToNum[langType]; ok {
				if (nativeLang == util.ChineseTraditional || nativeLang == util.ChineseGuangDoHua) && (langNumber == util.ChineseSimple) {
					//发帖人的母语是繁体 且识别语言是简体
					langNumber = nativeLang //修正语言为繁体
				}
				if nativeLang == langNumber { // 首先发帖人的母语跟帖子的语言匹配
					if ok := IsLegalLength(langNumber, subReqBody.GetContentByteLen(), subReqBody.GetContentWords()); ok { // 长度满足条件
						if isLegalNative := IsLegalNativeLang(national, langNumber); isLegalNative {
							hashName := fmt.Sprintf("%s%v", util.LearnLangPrefix, langNumber)
							// infoLog.Printf("ProcPostMidReq hashName=%s", hashName)
							index, err := ssdbOperator.InsertMidIntoHashMap(hashName, mid)
							if err != nil {
								infoLog.Printf("ProcPostMidReq InsertMidIntoHashMap failed uid=%v mid=%v hashName=%s",
									head.Uid,
									mid,
									hashName)
							} else {
								// add to upate list
								iterm := &ht_moment_cache.HashMapIterm{
									HashName: proto.String(hashName),
									Key:      proto.String(fmt.Sprintf("%v", index)),
									Value:    proto.String(fmt.Sprintf("%v", mid)),
									Expire:   proto.Uint64(0),
								}
								updateList = append(updateList, iterm)
							}
						} else {
							infoLog.Printf("ProcPostMidReq national=%s langNum=%v not match", national, langNumber)
						}
					} else {
						infoLog.Printf("ProcPostMidReq not enougth uid=%v mid=%v byteLen=%v words=%v",
							head.Uid,
							mid,
							subReqBody.GetContentByteLen(),
							subReqBody.GetContentWords())
					}
				} else {
					infoLog.Printf("ProcPostMidReq mid=%v nativeLang=%v content lang=%v not match", mid, nativeLang, langNumber)
				}
			} else {
				infoLog.Printf("ProcPostMidReq version=%v UnKnow lang type=%s", subReqBody.GetVersion(), langType)
			}
		}
		// for i, v := range updateList {
		// 	infoLog.Printf("ProcPostMidReq i=%v index=%s mid=%s", i, v.GetKey(), v.GetValue())
		// }
	}
	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		BatchAddMidToHashmapReqbody: &ht_moment_cache.BatchAddMidToHashMapReqBody{
			HashIterms: updateList,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcPostMidReq proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/batch_add_mid_hash_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_BATCH_ADD_MID_TO_HASH)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	// 发布发帖事件到NSQ 等待消费者消费
	if subReqBody.GetOnlyToMe() != 1 {
		err = PublishPostEvent(head.Uid, time.Now().Unix(), fmt.Sprintf("%v", mid))
		if err != nil {
			infoLog.Printf("ProcPostMidReq PublishPostEvent failed uid=%v mid=%s err=%s",
				head.Uid,
				mid,
				err)
		}
	} else {
		infoLog.Printf("ProcPostMidReq onlye_to_me uid=%u mid=%s not need notify",
			head.Uid,
			mid)
	}

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcPostMidReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	return true
}

func StatUserMntCountInRedis(head *common.HeadV2, count int64) (err error) {
	if head == nil {
		err = util.ErrDbParam
		return err
	}
	hashName := fmt.Sprintf("%v_user_info", head.Uid)
	incItemrs := []*ht_moment_cache.HincIterm{
		&ht_moment_cache.HincIterm{
			ItermKey: proto.String(util.MntCountForSelfKey),
			IncCount: proto.Int32(int32(count)),
		},
		// &ht_moment_cache.HincIterm{
		// 	ItermKey: proto.String(util.MntCountForOtherKey),
		// 	IncCount: proto.Int32(int32(count)),
		// },
	}
	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		HincReqbody: &ht_moment_cache.HincReqBody{
			HashName:  proto.String(hashName),
			IncIterms: incItemrs,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("StatUserMntCountInRedis proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/hinc_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_HINC_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	return nil
}

func StatUserMntCountForOtherInRedis(head *common.HeadV2, count int64) (err error) {
	if head == nil {
		err = util.ErrDbParam
		return err
	}
	hashName := fmt.Sprintf("%v_user_info", head.Uid)
	incItemrs := []*ht_moment_cache.HincIterm{
		&ht_moment_cache.HincIterm{
			ItermKey: proto.String(util.MntCountForOtherKey),
			IncCount: proto.Int32(int32(count)),
		},
	}
	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		HincReqbody: &ht_moment_cache.HincReqBody{
			HashName:  proto.String(hashName),
			IncIterms: incItemrs,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("StatUserMntCountForOtherInRedis proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/hinc_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_HINC_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	return nil
}

func ClearUserInfoCacheInRedis(head *common.HeadV2, uid uint32) (err error) {
	if head == nil {
		err = util.ErrDbParam
		return err
	}
	reqHead := new(common.HeadV2)
	*reqHead = *head
	// 更新redis
	reqBody := &ht_moment_cache.MntCacheReqBody{
		ClearUserInfoCacheReqbody: &ht_moment_cache.ClearUserInfoCacheReqBody{
			Userid: proto.Uint32(uid),
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ClearUserInfoCacheInRedis proto.Marshal failed uid=%v err=%s", uid, err)
		attr := "gomntdbd/hinc_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_CLEAR_USER_INFO_CACHE)
	//双写更新Redis
	SetRedisCache(reqHead, reqPayLoad)
	return nil
}

func DeleteKeyInRedis(head *common.HeadV2, uid uint32, key string) (err error) {
	if head == nil {
		err = util.ErrDbParam
		return err
	}
	reqHead := new(common.HeadV2)
	*reqHead = *head
	// 更新redis
	reqBody := &ht_moment_cache.MntCacheReqBody{
		DeleteKeyReqbody: &ht_moment_cache.DeleteKeyReqBody{
			ReqUid: proto.Uint32(uid),
			Key:    proto.String(key),
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("DeleteKeyInRedis proto.Marshal failed uid=%v key=%s err=%s", uid, key, err)
		attr := "gomntdbd/delete_key_proto_err"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_DELETE_KEY_REQ)
	//双写更新Redis
	SetRedisCache(reqHead, reqPayLoad)
	return nil
}

func ReloadZsetInRedis(head *common.HeadV2, uid uint32, zsetName string, ttl uint32) (err error) {
	if head == nil {
		err = util.ErrDbParam
		return err
	}
	reqHead := new(common.HeadV2)
	*reqHead = *head
	// 更新redis
	reqBody := &ht_moment_cache.MntCacheReqBody{
		ReloadZsetReqbody: &ht_moment_cache.ReloadZsetReqBody{
			ReqUid:   proto.Uint32(uid),
			ExpireTs: proto.Uint32(ttl),
			ZsetName: proto.String(zsetName),
		},
	}
	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("ReloadZsetInRedis proto.Marshal failed uid=%v key=%s err=%s", uid, zsetName, err)
		attr := "gomntdbd/reload_zset_proto_err"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	reqHead.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_RELOAD_ZSET_REQ)
	//双写更新Redis
	SetRedisCache(reqHead, reqPayLoad)
	return nil
}

func PublishPostEvent(uid uint32, curTs int64, mid string) (err error) {
	if uid == 0 || len(mid) == 0 {
		infoLog.Printf("PublishPostEvent invalid param uid=%v curTs=%v mid=%s", uid, curTs, mid)
		err = util.ErrDbParam
		return err
	}
	notifyObj := simplejson.New()
	notifyObj.Set("post_uid", uid)
	notifyObj.Set("post_ts", curTs)
	notifyObj.Set("mid", mid)
	notifySlice, err := notifyObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("PublishPostEvent notifyObj.MarshalJson failed uid=%v curTs=%v mid=%s",
			uid,
			curTs,
			mid)
		return err
	}

	err = globalProducer.Publish(postMntTopic, notifySlice)
	if err != nil {
		// 统计apns失败总量
		attr := "gomntdbd/post_mnt_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("PublishPostEvent Could not connect uid=%v curTs=%v mid=%s",
			uid,
			curTs,
			mid)
		return err
	} else {
		// 统计apns成功的总量
		attr := "gomntdbd/post_mnt_to_nsq_succ"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("PublishPostEvent Publish success uid=%v curTs=%v mid=%s",
			uid,
			curTs,
			mid)
	}
	return nil
}

func PublishUserActionEvent(uid uint32, mid string, eventType int) (err error) {
	if uid == 0 || len(mid) == 0 {
		infoLog.Printf("PublishUserActionEvent invalid param uid=%v mid=%s eventType=%v", uid, mid, eventType)
		err = util.ErrDbParam
		return err
	}
	notifyObj := simplejson.New()
	notifyObj.Set("uid", uid)
	notifyObj.Set("ts", time.Now().Unix())
	notifyObj.Set("mid", mid)
	notifyObj.Set("type", eventType)
	notifySlice, err := notifyObj.MarshalJSON()
	if err != nil {
		infoLog.Printf("PublishUserActionEvent notifyObj.MarshalJson failed uid=%v mid=%s notifyType=%v",
			uid,
			mid,
			eventType)
		return err
	}

	err = globalProducer.Publish(userActionTopic, notifySlice)
	if err != nil {
		// 统计apns失败总量
		attr := "gomntdbd/post_mnt_to_nsq_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("PublishUserActionEvent Could not connect uid=%v mid=%s notifyEvent=%v",
			uid,
			mid,
			eventType)
		return err
	} else {
		// 统计apns成功的总量
		attr := "gomntdbd/post_mnt_to_nsq_succ"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("PublishUserActionEvent Publish success uid=%v mid=%s eventType=%s",
			uid,
			mid,
			eventType)
	}
	return nil
}

// 4.proc get specific key in hash map
func ProcGetMidHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetMidHashMap no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetMidHashMapRspbody = &ht_moment_cache.GetMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetMidHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/get_hash_map_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetMidHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.GetMidHashMapRspbody = &ht_moment_cache.GetMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetMidHashMapReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcGetMidHashMap GetGetHashMapReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.GetMidHashMapRspbody = &ht_moment_cache.GetMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	hashName := subReqBody.GetHashName()
	keys := subReqBody.GetKeys()
	// infoLog.Printf("ProcGetMidHashMap hashName=%s keys=%v uid=%v", hashName, keys, head.Uid)
	// param check
	if len(hashName) == 0 || len(keys) == 0 {
		infoLog.Printf("ProcGetMidHashMap hashname=%s key lenght =%v uid=%v", hashName, len(keys), head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetMidHashMapRspbody = &ht_moment_cache.GetMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}

	_, outValues, err := ssdbOperator.MultiHgetSliceArray(hashName, keys)
	if err != nil {
		infoLog.Printf("ProcGetMidHashMap hashName=%s keys=%v uid=%v err=%s", hashName, keys, head.Uid, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetMidHashMapRspbody = &ht_moment_cache.GetMidHashMapRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}
	// send resp
	rspBody.GetMidHashMapRspbody = &ht_moment_cache.GetMidHashMapRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Values: outValues,
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// Step2 reload all content
	outKeys, outValues, err := ssdbOperator.MultiHgetAllSlice(hashName)
	if err != nil {
		infoLog.Printf("ProcGetMidHashMap ssdbOperator.MultiHgetAllSlice hashName=%s err=%s", hashName, err)
		return false
	}
	if len(outKeys) != len(outValues) {
		infoLog.Printf("ProcGetMidHashMap ssdbOperator.MultiHgetAllSlice keyLen=%v valueLen=%v not equal", len(outKeys), len(outValues))
		return false
	}
	keyValues := make([]string, len(outKeys)*2)
	for index := 0; index < len(outKeys); index++ {
		keyValues[2*index] = outKeys[index]
		keyValues[2*index+1] = outValues[index]
	}
	// outKeys 添加完成之后 判断是否存在moment_creater 如果不存在moment_creater 字段则从content中解析出来之后重新设置回ssdb中
	existKey := false
	for _, v := range outKeys {
		if v == util.MomentCreater {
			existKey = true
			break
		}
	}
	// infoLog.Printf("ProcGetMidHashMap hashName=%s existKey=%v", hashName, existKey)
	if !existKey {
		infoLog.Printf("ProcGetMidHashMap hashName=%s not exist moment creater", hashName)
		// Step4 将mid-->uid 添加到redis中
		var contentValue string
		for i, v := range outKeys {
			if v == util.MntContentKey {
				contentValue = outValues[i]
				break
			}
		}
		if len(contentValue) > 0 {
			// infoLog.Printf("ProcGetHashMap hashname=%s contentValue=%s", hashName, contentValue)
			storeMomentBody := new(ht_moment_store.StoreMomentBody)
			err = proto.Unmarshal([]byte(contentValue), storeMomentBody)
			if err != nil {
				attr := "gomntdbd/proto_unmarshal_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcGetMidHashMap proto Unmarshal failed hashname=%s contentValueLen=%v", hashName, len(contentValue))
			} else {
				setValue := fmt.Sprintf("%v", storeMomentBody.GetUserid())
				err = ssdbOperator.Hset(hashName, util.MomentCreater, setValue)
				if err != nil {
					attr := "gomntdbd/ssdb_hset_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ProcGetMidHashMap hset hashName=%s key=%s value=%s err=%s", hashName, util.MomentCreater, setValue, err)
				} else {
					infoLog.Printf("ProcGetMidHashMap hset hashName=%s key=%s valueLen=%v", hashName, util.MomentCreater, len(setValue))
				}
				// 将moment的创建者添加到keyValues中
				keyValues = append(keyValues, util.MomentCreater)
				keyValues = append(keyValues, setValue)
				infoLog.Printf("ProcGetMidHashMap hashName=%s add moment create=%s", hashName, setValue)
			}
		}
	}
	// Stpe3 update redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		SetMidHashMapReqbody: &ht_moment_cache.SetMidHashMapReqBody{
			HashName: proto.String(hashName),
			KeyValue: keyValues,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcGetMidHashMap proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/set_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_MID_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	totalProcTime := packet.CalcProcessTime()

	infoLog.Printf("ProcGetMidHashMap uid=%v hashName=%s totalProcTime=%v", head.Uid, hashName, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/get_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 5.proc hinc req body
func ProcHincReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcHincReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcHincReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/hinc_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcHincReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetHincReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcHincReq GetHincReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// param check
	hashName := subReqBody.GetHashName()
	incItem := subReqBody.GetIncIterms()
	if len(hashName) == 0 || len(incItem) == 0 {
		infoLog.Printf("ProcHincReq invalid param uid=%v hashName=%s incIntemLen=%v", head.Uid, hashName, len(incItem))
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcHincReq hashName=%s incItemLen=%v", hashName, len(incItem))
	for _, v := range incItem {
		_, err := ssdbOperator.Hincr(hashName, v.GetItermKey(), int(v.GetIncCount()))
		if err != nil {
			infoLog.Printf("ProcHincReq ssdbOperator.Hincr return err=%s hashName=%s keys=%v",
				err,
				hashName,
				v.GetItermKey())
			attr := "gomntdbd/hinc_err"
			libcomm.AttrAdd(attr, 1)
			continue
		} else {
			infoLog.Printf("ProcHincReq hincr hashmap=%s key=%s inc=%v", hashName, v.GetItermKey(), v.GetIncCount())
		}
	}

	// send resp
	rspBody.HincRspbody = &ht_moment_cache.HincRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcHincReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/hinc_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	//双写更新Redis 无需改变直接发送到redis即可
	SetRedisCache(head, payLoad)

	// 如果hashname 中包含user_info suffix 则删除redis中的缓存
	if strings.HasSuffix(hashName, util.UserInfoSuffix) {
		var uid uint32
		_, err := fmt.Sscanf(hashName, "%d_user_info", &uid)
		if err == nil {
			infoLog.Printf("ProcHincReq hashName=%s uid=%v", hashName, uid)
			err = ClearUserInfoCacheInRedis(head, uid)
			if err != nil {
				infoLog.Printf("ProcHincReq ClearUserInfoCacheInRedis failed uid=%v err=%s", uid, err)
			}
		} else {
			infoLog.Printf("ProcHincReq fmt.Sscanf hashName=%s error=%s", hashName, err)
		}
	}
	return true
}

// 6.proc zrange list
// 当前仅仅用于加载改错的cid list 所以无需更新redis
func ProcZrangeList(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcZrangeList no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ZrangeListRspbody = &ht_moment_cache.ZrangeListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcZrangeList invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/zrange_list_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcZrangeList proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ZrangeListRspbody = &ht_moment_cache.ZrangeListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetZrangeListReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcZrangeList GetZrangeListRspbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ZrangeListRspbody = &ht_moment_cache.ZrangeListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	zsetName := subReqBody.GetZsetName()
	offset := subReqBody.GetOffset()
	limit := subReqBody.GetLimit()
	reverse := subReqBody.GetReverse() // 0:zrange 1:zrrange
	infoLog.Printf("ProcZrangeList zsetName=%s offset=%v limit=%v reverse=%v",
		zsetName,
		offset,
		limit,
		reverse)
	// param check
	if len(zsetName) == 0 || limit == 0 {
		infoLog.Printf("ProcZrangeList input param error setName=%s offset=%v limit=%v reverse=%v uid=%v",
			zsetName,
			offset,
			limit,
			reverse,
			head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ZrangeListRspbody = &ht_moment_cache.ZrangeListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("zset name error"),
			},
		}
		return false
	}

	val, err := ssdbOperator.ZrangeList(zsetName, int64(offset), int64(limit), reverse)
	if err != nil {
		infoLog.Printf("ProcZrangeList ssdbOperator.ZrangeList return err=%s zsetname=%s offset=%v limit=%v reverse=%v",
			err,
			zsetName,
			offset,
			limit,
			reverse)
		attr := "gomntdbd/zrange_list_err"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
		rspBody.ZrangeListRspbody = &ht_moment_cache.ZrangeListRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	}
	keyScore := make([]uint32, len(val))
	for i, v := range val {
		key, err := strconv.ParseUint(v, 10, 32)
		if err != nil {
			infoLog.Printf("ProcZrangeList strconv.ParseUint i=%s err=%s", i, err)
			continue
		}
		keyScore[i] = uint32(key)
	}

	// send resp
	rspBody.ZrangeListRspbody = &ht_moment_cache.ZrangeListRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		KeyScore: keyScore,
	}
	totalProcTime := packet.CalcProcessTime()
	// infoLog.Printf("ProcZrangeList uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/zrange_list_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 7.proc Hscan req body
// 此协议仅仅用于遍历uid#LIST/uid#FRIENDS/LANG#num-num/LANG#native/LANG#num/LLANG#num
// 这几个桶 因为这些桶的序号都是从100000000000 100亿开始递增的
// 使用redis driver 取到ssdb driver 时 使用Hmget 取代 Hscan
// 不能用于Hscan 其它的桶
// Hscan 时需要将桶加载到redis中
// 对于uid#LIST/uid#FRIENDS 加载全部，设置过期时间
// 对于LANG#num-num/LANG#native/LANG#num/LLANG#num 永不过期 设置桶的大小
func ProcHscanReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcHscanReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.HscanRspbody = &ht_moment_cache.HscanRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcHscanReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/hscan_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcHscanReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.HscanRspbody = &ht_moment_cache.HscanRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetHscanReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcHscanReq GetHscanReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.HscanRspbody = &ht_moment_cache.HscanRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	hashName := subReqBody.GetHashName()
	start := subReqBody.GetStart()
	end := subReqBody.GetEnd()
	limit := subReqBody.GetLimit() // 0:zrange 1:zrrange
	reverse := subReqBody.GetReverse()
	infoLog.Printf("ProcHscanReq hashName=%s start=%v end=%v limit=%v reverse=%v",
		hashName,
		start,
		end,
		limit,
		reverse)
	// param check
	if len(hashName) == 0 || limit == 0 {
		infoLog.Printf("ProcHscanReq input param error hashName=%s start=%v end=%v limit=%v reverse=%v uid=%v",
			hashName,
			start,
			end,
			limit,
			reverse,
			head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.HscanRspbody = &ht_moment_cache.HscanRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}

	// get max key
	hashSize, err := ssdbOperator.Hsize(hashName)
	if err != nil {
		infoLog.Printf("ProcHscanReq ssdbOperator.Hsize return err=%s hashName=%s start=%v end=%v limit=%v reverse=%v",
			err,
			hashName,
			start,
			end,
			limit,
			reverse)
		attr := "gomntdbd/hsize_err"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
		rspBody.HscanRspbody = &ht_moment_cache.HscanRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	}
	// infoLog.Printf("ProcHscanReq hashName=%s hashSize=%v", hashName, hashSize)
	ret, err := ssdbOperator.Hexists(hashName, "0")
	if err == nil && ret {
		hashSize -= 1
		infoLog.Printf("ProcHscanReq hashName=%s exist filed 0", hashName)
	}

	// max Key must add BaseMntIndex
	maxKey := hashSize + util.BaseMntIndex
	infoLog.Printf("ProcHscanReq hashName=%s hashSize=%v maxKey=%v", hashName, hashSize, maxKey)
	//0:顺序 1:逆序
	isRever := false
	if reverse == 1 {
		isRever = true
	}
	outKeys, outValues, err := ssdbOperator.HscanArray(hashName, start, end, limit, isRever)
	if err != nil || len(outKeys) != len(outValues) {
		infoLog.Printf("ProcHscanReq ssdbOperator.HscanArray return err=%s hashName=%s start=%v end=%v limit=%v reverse=%v",
			err,
			hashName,
			start,
			end,
			limit,
			reverse)
		attr := "gomntdbd/hscan_err"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
		rspBody.HscanRspbody = &ht_moment_cache.HscanRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	}
	// 整张列表为空直接返回空的结果
	if len(outKeys) == 0 {
		rspBody.HscanRspbody = &ht_moment_cache.HscanRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			HashName:  proto.String(hashName),
			MaxKey:    proto.Uint64(uint64(maxKey)),
			KeyValues: nil,
		}
	}
	keyValues := make([]string, len(outKeys)*2)
	for i, v := range outKeys {
		keyValues[2*i] = v
		keyValues[2*i+1] = outValues[i]
	}

	// send resp
	rspBody.HscanRspbody = &ht_moment_cache.HscanRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		HashName:  proto.String(hashName),
		MaxKey:    proto.Uint64(uint64(maxKey)),
		KeyValues: keyValues,
	}

	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// Step2 update redis
	var expireTime uint64 = 0
	if strings.HasSuffix(hashName, util.FriendSuffix) ||
		strings.HasSuffix(hashName, util.ListSuffix) {
		expireTime = util.HashMapExpireTime
	}

	setReqBody := &ht_moment_cache.MntCacheReqBody{
		ReloadHashMapReqbody: &ht_moment_cache.ReloadHashMapReqBody{
			HashList: []*ht_moment_cache.HashMapDesc{
				&ht_moment_cache.HashMapDesc{
					HashName: proto.String(hashName),
					Expire:   proto.Uint64(expireTime),
				},
			},
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcHscanReq proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/reload_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_RELOAD_HAHS_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcHscanReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/hscan_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 8.proc set user all mnt stat req
func ProcSetUserAllMntReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcSetUserAllMntReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetUserAllMntReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/set_all_mnt_stat_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetUserAllMntReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetUserAllMntStatReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcSetUserAllMntReq GetSetUserAllMntStatReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uidSlic := subReqBody.GetUserid()
	stat := subReqBody.GetStat()
	// infoLog.Printf("ProcSetUserAllMntReq uidSlic=%v stat=%v", uidSlic, stat)
	// param check
	if len(uidSlic) == 0 {
		infoLog.Printf("ProcSetUserAllMntReq input param error uidSlic=%v stat=%v uid=%v",
			uidSlic,
			stat,
			head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}

	// 循环遍历设置帖子的状态
	strStat := fmt.Sprintf("%v", stat)
	for _, v := range uidSlic {
		hashName := fmt.Sprintf("%v#LIST", v)
		hsize, err := ssdbOperator.Hsize(hashName)
		if err != nil {
			infoLog.Printf("ProcSetUserAllMntReq hsize hashName=%s err=%s", hashName, err)
			continue
		}
		start := int64(util.BaseMntIndex)
		end := hsize + util.BaseMntIndex
		_, outValues, err := ssdbOperator.HscanArray(hashName, start, end, int64(hsize), false)
		if err != nil {
			infoLog.Println("ProcSetUserAllMntReq ssdbOperator.HscanArray hashName=%s failed err=%s", hashName, err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
		}
		for _, mid := range outValues {
			// infoLog.Printf("ProcSetUserAllMntReq set mid=%s", mid)
			if len(mid) == 0 {
				infoLog.Printf("ProcSetUserAllMntReq  mid=%s empty", mid)
				continue
			}
			// 删除用户将除状态为1的帖文外全部设置为4
			// 恢复用户将状态为4的恢复为0 4:后台删除用户的状态
			strDeleted, err := ssdbOperator.Hget(mid, util.MntDeletedKey)
			if err != nil {
				attr := "gomntdbd/set_all_mnt_stat_failed_count"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcSetUserAllMntReq mid=%v Hget deleted failed err=%s",
					mid,
					err)
				continue
			}
			if (stat == 4 && strDeleted != "1") || //删除用户并且之前状态不为1:用户删除
				(stat == 0 && strDeleted == "4") { //恢复用户并且之前状态为4:后台删除用户
				err = ssdbOperator.Hset(mid, util.MntDeletedKey, strStat)
				if err != nil {
					attr := "gomntdbd/set_all_mnt_stat_failed_count"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ProcSetUserAllMntReq set mid=%s err=%s", mid, err)
					continue
				}
				var eventType int
				if stat == 4 { // 删除用户
					// 发布nsq事件到话题topic
					eventType = NSQ_EVENT_DELETE_USER
				} else if stat == 0 {
					eventType = NSQ_EVENT_RESTORE_USER
				}
				err = PublishUserActionEvent(v, mid, eventType)
				if err != nil {
					// add static
					attr := "gomntdbd/publish_to_nsq_failed"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("ProcSetUserAllMntReq PublishPostEvent uid=%v mid=%s eventType=%v failed",
						v,
						mid,
						eventType)
				}
			}
		}
	}

	// send resp
	rspBody.SetUserAllMntStatRspbody = &ht_moment_cache.SetUserAllMntStatRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcSetUserAllMntReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/set_all_mnt_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}

	//双写更新Redis
	SetRedisCache(head, payLoad)
	totalProcTime = packet.CalcProcessTime()
	infoLog.Printf("ProcSetUserAllMntReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	return true
}

// 9.proc modify mnt status
func ProcModMntStatReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModMntStatReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModMntStatReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/mod_mnt_stat_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModMntStatReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetModMntStatusReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcModMntStatReq GetModMntStatusReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	mid := subReqBody.GetMid()
	opType := subReqBody.GetOpType()
	opReason := subReqBody.GetOpReason()
	infoLog.Printf("ProcModMntStatReq mid=%s opType=%v opReason=%v", mid, opType, opReason)
	// param check
	if len(mid) == 0 {
		infoLog.Printf("ProcModMntStatReq input param error mid=%s opType=%v opReason=%v uid=%v", mid, opType, opReason, head.Uid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}

	mntCreater, _, err := ssdbOperator.ModifyMntStat(uint32(opType), string(mid))
	if err != nil {
		infoLog.Printf("ProcModMntStatReq ssdbOperator.ModifyMntStat mid=%s opType=%v opReason=%v err=%s", mid, opType, opReason, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	keyValues := []string{
		util.MntDeletedKey,
		fmt.Sprintf("%d", opType),
		util.MntOpReasonKey,
		string(opReason),
	}
	if opType == ht_moment_cache.MntStatOpType_OP_RESTORE ||
		opType == ht_moment_cache.MntStatOpType_OP_HIDE ||
		opType == ht_moment_cache.MntStatOpType_OP_DELETE ||
		opType == ht_moment_cache.MntStatOpType_OP_FOR_FOLLOWER_ONLY {
		err = ssdbOperator.MultiHset(string(mid), keyValues)
		if err != nil {
			infoLog.Printf("ProcModMntStatReq ssdbOperator.MultiHset mid=%s opType=%v opReason=%v err=%s", mid, opType, opReason, err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	} else {
		infoLog.Printf("ProcModMntStatReq unhandle mid=%s opType=%v opReason=%v", mid, opType, opReason)
	}

	// send resp
	rspBody.ModMntStatusRspbody = &ht_moment_cache.ModMntStatusRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	totalProcTime := packet.CalcProcessTime()
	// infoLog.Printf("ProcModMntStatReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/mod_mnt_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}

	//双写更新Redis
	SetRedisCache(head, payLoad)

	// 更新redis中的xxxx_user_info 缓存
	err = ClearUserInfoCacheInRedis(head, mntCreater)
	if err != nil {
		infoLog.Printf("ProcModMntStatReq ClearUserInfoCacheInRedis uid=%v err=%s", mntCreater, err)
	}
	totalProcTime = packet.CalcProcessTime()
	infoLog.Printf("ProcModMntStatReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	return true
}

// 10.proc op uid service
func ProcOpUidService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcOpUidService no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.OpUidRspbody = &ht_moment_cache.OpUidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcOpUidService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/op_uid_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcOpUidService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.OpUidRspbody = &ht_moment_cache.OpUidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetOpUidReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcOpUidService GetOpUidRepbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.OpUidRspbody = &ht_moment_cache.OpUidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	listType := uint32(subReqBody.GetListType()) // 列表类型
	opType := uint32(subReqBody.GetOpType())     // 操作类型
	opUid := uint32(subReqBody.GetOpUserId())    // 被操作的uid
	selfUid := subReqBody.GetUserid()            // 自己的用户id
	infoLog.Printf("ProcOpUidService fromId=%v opUid=%v listType=%v opType=%v selfUid=%v", head.Uid, opUid, listType, opType, selfUid)
	// param check
	if opUid == 0 || selfUid == 0 {
		infoLog.Printf("ProcOpUidService input param error opUid=%s listType=%v opType=%v selfUid=%v", opUid, listType, opType, selfUid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.OpUidRspbody = &ht_moment_cache.OpUidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}
	var addTime uint64
	switch listType {
	case uint32(ht_moment_cache.OpList_OP_HIDE_LIST):
		addTime, err = ssdbOperator.UpdateHideOtherMntUidList(selfUid, opType, opUid)
		if err != nil {
			infoLog.Printf("ProcOpUidService ssdbOperator.UpdateHideOtherMntUidList opUid=%s listType=%v opType=%v selfUid=%v err=%s", opUid, listType, opType, selfUid, err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.OpUidRspbody = &ht_moment_cache.OpUidRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	case uint32(ht_moment_cache.OpList_OP_NOT_SHARE_LIST):
		addTime, err = ssdbOperator.UpdateNotShareMntUidList(selfUid, opType, opUid)
		if err != nil {
			infoLog.Printf("ProcOpUidService ssdbOperator.UpdateNotShareMntUidList opUid=%s listType=%v opType=%v selfUid=%v err=%s", opUid, listType, opType, selfUid, err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.OpUidRspbody = &ht_moment_cache.OpUidRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	default:
		infoLog.Printf("ProcOpUidService opUid=%s listType=%v opType=%v selfUid=%v unknow type", opUid, listType, opType, selfUid)
	}
	// send resp
	rspBody.OpUidRspbody = &ht_moment_cache.OpUidRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ListType: subReqBody.GetListType().Enum(),
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	totalProcTime := packet.CalcProcessTime()
	// infoLog.Printf("ProcOpUidService uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/mod_mnt_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}

	//双写更新Redis 只有确实更新成功了再更新redis
	if addTime > 0 {
		setReqBody := &ht_moment_cache.MntCacheReqBody{
			OpUidInRedisReqbody: &ht_moment_cache.OpUidInRedisReqBody{
				ListType: subReqBody.GetListType().Enum(),
				OpType:   subReqBody.GetOpType().Enum(),
				OpUserId: proto.Uint32(subReqBody.GetOpUserId()),
				Userid:   proto.Uint32(subReqBody.GetUserid()),
				AddTime:  proto.Uint32(uint32(addTime)),
			},
		}
		reqPayLoad, err := proto.Marshal(setReqBody)
		if err != nil {
			infoLog.Printf("ProcOpUidService proto.Marshal failed uid=%v err=%s", head.Uid, err)
			attr := "gomntdbd/op_uid_proto_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_OP_UID_IN_REDIS_REQ)
		//双写更新Redis
		SetRedisCache(head, reqPayLoad)
	}
	totalProcTime = packet.CalcProcessTime()
	infoLog.Printf("ProcOpUidService uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	return true
}

// 11.proc back end op uid req
func ProcBackEndOpUidReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcBackEndOpUidReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.BackEndOpUidRspbody = &ht_moment_cache.BackEndOpUidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcBackEndOpUidReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/backend_op_uid_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcBackEndOpUidReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.BackEndOpUidRspbody = &ht_moment_cache.BackEndOpUidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetBackEndOpUidReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcBackEndOpUidReq GetBackEndOpUidReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.BackEndOpUidRspbody = &ht_moment_cache.BackEndOpUidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	listType := uint32(subReqBody.GetListType())
	opType := uint32(subReqBody.GetOpType())
	opUid := uint32(subReqBody.GetOpUserId())
	userId := subReqBody.GetUserid()
	opReason := subReqBody.GetOpReason()
	infoLog.Printf("ProcBackEndOpUidReq fromId=%v opUid=%v listType=%v opType=%v userId=%v reason=%s",
		head.Uid,
		opUid,
		listType,
		opType,
		userId,
		opReason)
	// param check
	if opUid == 0 {
		infoLog.Printf("ProcBackEndOpUidReq input param error fromId=%v opUid=%s listType=%v opType=%v userId=%v reason=%s",
			head.Uid,
			opUid,
			listType,
			opType,
			userId,
			opReason)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.BackEndOpUidRspbody = &ht_moment_cache.BackEndOpUidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}
	from := head.Uid
	var addTime uint64
	switch listType {
	case uint32(ht_moment_cache.OpList_OP_MOMENT_FOR_FOLLOWER_LIST):
		addTime, err = ssdbOperator.UpdateMntForFollowerUidList(from, opType, opUid)
		if err != nil {
			infoLog.Printf("ProcBackEndOpUidReq ssdbOperator.UpdateMntForFollowerUidList opUid=%s listType=%v opType=%v fromId=%v err=%s", opUid, listType, opType, head.Uid, err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.BackEndOpUidRspbody = &ht_moment_cache.BackEndOpUidRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	case uint32(ht_moment_cache.OpList_OP_MOMENT_FOR_SELF_LIST):
		addTime, err = ssdbOperator.UpdateMntForSelfUidList(from, opType, opUid)
		if err != nil {
			infoLog.Printf("ProcBackEndOpUidReq ssdbOperator.UpdateMntForSelfUidList opUid=%s listType=%v opType=%v fromId=%v err=%s", opUid, listType, opType, head.Uid, err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.BackEndOpUidRspbody = &ht_moment_cache.BackEndOpUidRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	default:
		infoLog.Printf("ProcBackEndOpUidReq opUid=%v listType=%v opType=%v fromId=%v unknow type", opUid, listType, opType, head.Uid)
	}
	// send resp
	rspBody.BackEndOpUidRspbody = &ht_moment_cache.BackEndOpUidRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ListType: subReqBody.GetListType().Enum(),
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	totalProcTime := packet.CalcProcessTime()
	// infoLog.Printf("ProcBackEndOpUidReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/back_end_op_uid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}

	//双写更新Redis 只有确实更新成功了再更新redis
	if addTime > 0 {
		setReqBody := &ht_moment_cache.MntCacheReqBody{
			BackEndOpUidInRedisReqbody: &ht_moment_cache.BackEndOpUidInRedisReqBody{
				ListType: subReqBody.GetListType().Enum(),
				OpType:   subReqBody.GetOpType().Enum(),
				OpUserId: proto.Uint32(subReqBody.GetOpUserId()),
				Userid:   proto.Uint32(subReqBody.GetUserid()),
				OpReason: subReqBody.GetOpReason(),
				AddTime:  proto.Uint32(uint32(addTime)),
			},
		}
		reqPayLoad, err := proto.Marshal(setReqBody)
		if err != nil {
			infoLog.Printf("ProcBackEndOpUidReq proto.Marshal failed uid=%v err=%s", head.Uid, err)
			attr := "gomntdbd/backend_op_uid_proto_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_BACK_END_OP_UID_IN_REDIS_REQ)
		//双写更新Redis
		SetRedisCache(head, reqPayLoad)
	}

	// 删除xxxx_user_info 缓存
	err = ClearUserInfoCacheInRedis(head, opUid)
	if err != nil {
		infoLog.Printf("ProcBackEndOpUidReq opUid=%v listType=%v opType=%v fromId=%v ClearUserInfoCacheInRedis err=%s",
			opUid,
			listType,
			opType,
			head.Uid,
			err)
	}
	totalProcTime = packet.CalcProcessTime()
	infoLog.Printf("ProcBackEndOpUidReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	return true
}

// 12.proc mod mid show field req
func ProcModMidShowFieldReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModMidShowFieldReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModMidShowFieldRspbody = &ht_moment_cache.ModMidShowFieldRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModMidShowFieldReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/mod_mid_show_field_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModMidShowFieldReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ModMidShowFieldRspbody = &ht_moment_cache.ModMidShowFieldRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetModMidShowFieldReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcModMidShowFieldReq GetModMidShowFieldReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ModMidShowFieldRspbody = &ht_moment_cache.ModMidShowFieldRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	uid := subReqBody.GetUserid()
	mid := subReqBody.GetMid()
	statSlic := subReqBody.GetState()
	OpType := subReqBody.GetOpType()
	infoLog.Printf("ProcModMidShowFieldReq reqUid=%v uid=%v mid=%s statSlic=%v OpType=%v",
		head.Uid,
		uid,
		mid,
		statSlic,
		OpType)
	// param check
	if uid == 0 || len(mid) == 0 {
		infoLog.Printf("ProcModMidShowFieldReq input param error reqUid=%v uid=%v mid=%s statSlic=%v OpType=%v",
			head.Uid,
			uid,
			mid,
			statSlic,
			OpType)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModMidShowFieldRspbody = &ht_moment_cache.ModMidShowFieldRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}
	value, err := ssdbOperator.Hget(string(mid), util.ShowField)
	if err != nil {
		infoLog.Println("ProcModMidShowFieldReq ssdbOperator.Hget() failed mid=%s err=%s", mid, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMidShowFieldRspbody = &ht_moment_cache.ModMidShowFieldRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	showValue, err := strconv.ParseUint(value, 10, 64)
	if err != nil {
		infoLog.Println("ProcModMidShowFieldReq strconv.Parse failed mid=%s value=%s err=%s", mid, value, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMidShowFieldRspbody = &ht_moment_cache.ModMidShowFieldRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	switch OpType {
	case ht_moment_cache.OpType_OP_ADD:
		for _, v := range statSlic {
			showValue |= uint64(v)
		}
	case ht_moment_cache.OpType_OP_DEL:
		for _, v := range statSlic {
			showValue &= ^(uint64(v))
		}
	default:
		infoLog.Printf("ProcModMidShowFieldReq Unhandle opType=%v reqUid=%v uid=%v mid=%s", OpType, head.Uid, uid, mid)
	}

	err = ssdbOperator.Hset(string(mid), util.ShowField, fmt.Sprintf("%v", showValue))
	if err != nil {
		infoLog.Println("ProcModMidShowFieldReq strconv.Parse failed mid=%s value=%s err=%s", mid, value, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModMidShowFieldRspbody = &ht_moment_cache.ModMidShowFieldRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// send resp
	rspBody.ModMidShowFieldRspbody = &ht_moment_cache.ModMidShowFieldRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}

	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	totalProcTime := packet.CalcProcessTime()
	// infoLog.Printf("ProcModMidShowFieldReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/back_end_op_uid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		SetMidHashMapReqbody: &ht_moment_cache.SetMidHashMapReqBody{
			HashName: proto.String(string(mid)),
			KeyValue: []string{
				util.ShowField,
				fmt.Sprintf("%v", showValue),
			},
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcModMidShowFieldReq proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/mod_mid_show_field_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_MID_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	totalProcTime = packet.CalcProcessTime()

	infoLog.Printf("ProcModMidShowFieldReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/mod_mid_show_field_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 13.proc get seq mid req body
func ProcGetSeqMidReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetSeqMidReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetSeqMidRspbody = &ht_moment_cache.GetSeqMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetSeqMidReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/get_seq_mid_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetSeqMidReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.GetSeqMidRspbody = &ht_moment_cache.GetSeqMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetSeqMidReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcGetSeqMidReq GetGetSeqMidReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.GetSeqMidRspbody = &ht_moment_cache.GetSeqMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	hashName := subReqBody.GetHashName()
	seq := subReqBody.GetSeq()
	infoLog.Printf("ProcGetSeqMidReq reqUid=%v hashName=%v seq=%v",
		head.Uid,
		hashName,
		seq)
	// param check
	if hashName == "" {
		infoLog.Printf("ProcGetSeqMidReq input param error reqUid=%v hashName=%v seq=%v",
			head.Uid,
			hashName,
			seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetSeqMidRspbody = &ht_moment_cache.GetSeqMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}

	count, err := ssdbOperator.Hsize(hashName)
	if err != nil {
		infoLog.Println("ProcGetSeqMidReq ssdbOperator.Hsize() failed hashmap=%s err=%s", hashName, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetSeqMidRspbody = &ht_moment_cache.GetSeqMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	if uint64(count) < seq {
		infoLog.Println("ProcGetSeqMidReq hashmap=%s err=%s", hashName, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetSeqMidRspbody = &ht_moment_cache.GetSeqMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	seq = util.BaseMntIndex + uint64(count) - seq + 1
	seqKey := fmt.Sprintf("%v", seq)
	midValue, err := ssdbOperator.Hget(hashName, seqKey)
	if err != nil {
		infoLog.Println("ProcGetSeqMidReq ssdbOperator.Hget failed hashMap=%s seqKey=%s err=%s", hashName, seqKey, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetSeqMidRspbody = &ht_moment_cache.GetSeqMidRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// send resp
	rspBody.GetSeqMidRspbody = &ht_moment_cache.GetSeqMidRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Mid: []byte(midValue),
	}

	totalProcTime := packet.CalcProcessTime()
	// infoLog.Printf("ProcGetSeqMidReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/get_seq_mid_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}

	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	outKeys, outValues, err := ssdbOperator.MultiHgetAllSlice(hashName)
	if err == nil {
		keyValues := make([]string, 2*len(outKeys))
		for i, v := range outKeys {
			keyValues[2*i] = v
			keyValues[2*i+1] = outValues[i]
		}
		setReqBody := &ht_moment_cache.MntCacheReqBody{
			SetMidHashMapReqbody: &ht_moment_cache.SetMidHashMapReqBody{
				HashName: proto.String(hashName),
				KeyValue: keyValues,
			},
		}
		reqPayLoad, err := proto.Marshal(setReqBody)
		if err != nil {
			infoLog.Printf("ProcGetSeqMidReq proto.Marshal failed uid=%v err=%s", head.Uid, err)
			attr := "gomntdbd/get_seq_mid_proto_err"
			libcomm.AttrAdd(attr, 1)
			return false
		}
		head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_MID_HASH_MAP)
		//双写更新Redis
		SetRedisCache(head, reqPayLoad)
		totalProcTime := packet.CalcProcessTime()

		infoLog.Printf("ProcGetSeqMidReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
		if totalProcTime > util.ProcTimeThresold {
			// add static
			attr := "gomntdbd/get_seq_mid_proc_slow"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		infoLog.Printf("ProcGetSeqMidReq ssdbOperator.MultiHgetAllSlice hashmap=%s err=%s", hashName, err)
	}
	return true
}

// 14.proc mod mid show field req
func ProcZgetReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcZgetReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ZgetRspbody = &ht_moment_cache.ZGetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcZgetReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/zget_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcZgetReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ZgetRspbody = &ht_moment_cache.ZGetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetZgetReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcZgetReq GetZgetReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ZgetRspbody = &ht_moment_cache.ZGetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	zsetName := subReqBody.GetZsetName()
	key := subReqBody.GetKey()

	infoLog.Printf("ProcZgetReq reqUid=%v zsetName=%s key=%s",
		head.Uid,
		zsetName,
		key)
	// param check
	if len(key) == 0 || len(zsetName) == 0 {
		infoLog.Printf("ProcZgetReq input param error reqUid=%v zsetName=%s key=%s",
			head.Uid,
			zsetName,
			key)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ZgetRspbody = &ht_moment_cache.ZGetRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}

	score, err := ssdbOperator.Zget(zsetName, key)
	if err != nil {
		infoLog.Printf("ProcZgetReq ssdbOperator.Zget() failed zsetName=%s key=%s err=%s", zsetName, key, err)
		if err == redis.ErrNil {
			// send resp
			rspBody.ZgetRspbody = &ht_moment_cache.ZGetRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("success"),
				},
				Score: proto.Int64(0),
			}
			return true
		} else {
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.ZgetRspbody = &ht_moment_cache.ZGetRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}
	// send resp
	rspBody.ZgetRspbody = &ht_moment_cache.ZGetRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Score: proto.Int64(int64(score)),
	}

	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	err = ReloadZsetInRedis(head, head.Uid, zsetName, util.ZSetExpireTime)
	if err != nil {
		infoLog.Printf("ProcZgetReq ReloadZsetInRedis failed zsetName=%s key=%s err=%s", zsetName, key, err)
	}

	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcZgetReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/zget_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 15.proc mod mnt bucket  用于发布mid到相关的桶内
func ProcModMntBucketService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModMntBucketService no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostModMntBucketRspbody = &ht_moment_cache.PostModMntBucketRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModMntBucketService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/mod_mnt_bucket_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostModMntBucketRspbody = &ht_moment_cache.PostModMntBucketRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetPostModMntBucketReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcModMntBucketService GetPostModMntBucketReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostModMntBucketRspbody = &ht_moment_cache.PostModMntBucketRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// param check
	mid := subReqBody.GetMid()
	if len(mid) < 6 {
		infoLog.Printf("ProcModMntBucketService invalid param uid=%v mid=%s buckeyType=%v byteLen=%v words=%v StrLangType=%s",
			head.Uid,
			mid,
			subReqBody.GetBucketType(),
			subReqBody.GetContentByteLen(),
			subReqBody.GetContentWords(),
			subReqBody.GetStrLangType())
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostModMntBucketRspbody = &ht_moment_cache.PostModMntBucketRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param error"),
			},
		}
		return false
	}

	infoLog.Printf("ProcModMntBucketService version=%v mid=%s buckeyType=%v byteLen=%v words=%v StrLangType=%s",
		subReqBody.GetVersion(),
		mid,
		subReqBody.GetBucketType(),
		subReqBody.GetContentByteLen(),
		subReqBody.GetContentWords(),
		subReqBody.GetStrLangType())

	// send resp
	rspBody.PostModMntBucketRspbody = &ht_moment_cache.PostModMntBucketRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// Step1: get moment creater
	uid, err := ssdbOperator.GetUidByMid(string(mid))
	if err != nil {
		infoLog.Printf("ProcModMntBucketService GetUidByMid mid=%s buckeyType=%v byteLen=%v words=%v StrLangType=%s err=%s",
			mid,
			subReqBody.GetBucketType(),
			subReqBody.GetContentByteLen(),
			subReqBody.GetContentWords(),
			subReqBody.GetStrLangType(),
			err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.PostModMntBucketRspbody = &ht_moment_cache.PostModMntBucketRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// 将帖子的创建者保存在Head中 后面获取语言桶时仍然会使用到
	head.Uid = uid
	// Step1: inc moment_count and moment_count_for_others need update redis
	// 2017-10-25 增加创建者给他人看的帖子计数
	err = ssdbOperator.StatUserMntCountForOther(uid, 1)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService ssdbOperator.StatUserMntCountForOther failed uid=%v mid=%s err=%s", uid, mid, err)
	}
	//Step2: update moment_count in redis
	err = ClearUserInfoCacheInRedis(head, uid)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService ClearUserInfoCacheInRedis failed err=%s", err)
	}

	var updateList []*ht_moment_cache.HashMapIterm
	// Step5: Get Follower list
	followerList, err := relationApi.GetFollowerLists(head.Uid)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService relationApi.GetFollowerLists failed uid=%v err=%s", head.Uid, err)
		// add static
		attr := "gomntdbd/get_follower_list_faild"
		libcomm.AttrAdd(attr, 1)
	} else {
		//添加粉丝bucket
		// infoLog.Printf("ProcModMntBucketService followerListSize=%v", len(followerList))
		for _, v := range followerList {
			// infoLog.Printf("ProcModMntBucketService index=%v follower=%v", i, v)
			hashName := fmt.Sprintf("%v#FRIEND", v)
			index, err := ssdbOperator.InsertStrMidIntoHashMap(hashName, string(mid))
			if err != nil {
				infoLog.Printf("ProcPostMidReq InsertStrMidIntoHashMap failed uid=%v mid=%v hashName=%s",
					head.Uid,
					mid,
					hashName)
			} else {
				// add to upate list 存在则更新
				iterm := &ht_moment_cache.HashMapIterm{
					HashName: proto.String(hashName),
					Key:      proto.String(fmt.Sprintf("%v", index)),
					Value:    proto.String(string(mid)),
					Expire:   proto.Uint64(util.HashMapExpireTime),
				}
				updateList = append(updateList, iterm)
			}
		}
	}

	// Step6: Get teach and learn lang from user cache
	national, nativeLang, teachLang, learnLang, err := userCacheApi.QueryLangInfo(head.Uid)
	infoLog.Printf("ProcModMntBucketService uid=%v national=%s nativeLang=%v teachLang=%v learnLang=%v",
		uid,
		national,
		nativeLang,
		teachLang,
		learnLang)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService userCacheApi.QueryTeachAndLearnLang failed uid=%v err=%s", head.Uid, err)
		// add static
		attr := "gomntdbd/get_teach_and_learn_lang_faild"
		libcomm.AttrAdd(attr, 1)
	} else {
		var teachToLearn []string
		langType := strings.ToLower(string(subReqBody.GetStrLangType()))
		if langNumber, ok := gMapShortLangToNum[langType]; ok {
			if ok := Uint32IsInSlice(teachLang, langNumber); ok {
				for _, learn := range learnLang {
					teachToLearn = append(teachToLearn, fmt.Sprintf("LANG#%v-%v", langNumber, learn))
				}
			} else if ok := Uint32IsInSlice(learnLang, langNumber); ok {
				for _, teach := range teachLang {
					teachToLearn = append(teachToLearn, fmt.Sprintf("LANG#%v-%v", teach, langNumber))
				}
			} else { // 不在学习或教学语言中的放入所有桶中
				// add static
				attr := "gomntdbd/moment_lang_not_match"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("ProcModMntBucketService lang_not_match uid=%v national=%s nativeLang=%v teachLang=%v learnLang=%v langType=%v",
					uid,
					national,
					nativeLang,
					teachLang,
					learnLang,
					langType)
				for _, teach := range teachLang {
					for _, learn := range learnLang {
						teachToLearn = append(teachToLearn, fmt.Sprintf("LANG#%v-%v", teach, learn))
					}
				}
			}
			infoLog.Printf("ProcModMntBucketService uid=%v teachToLearn=%v", uid, teachToLearn)
			for _, v := range teachToLearn {
				// infoLog.Printf("ProcModMntBucketService index=%v lang=%s", i, v)
				index, err := ssdbOperator.InsertStrMidIntoHashMap(v, string(mid))
				if err != nil {
					infoLog.Printf("ProcModMntBucketService InsertStrMidIntoHashMap failed uid=%v mid=%v hashName=%s",
						head.Uid,
						mid,
						v)

				} else {
					// add to upate list
					iterm := &ht_moment_cache.HashMapIterm{
						HashName: proto.String(v),
						Key:      proto.String(fmt.Sprintf("%v", index)),
						Value:    proto.String(string(mid)),
						Expire:   proto.Uint64(0),
					}
					updateList = append(updateList, iterm)
				}
			}
		} else if len(langType) == 0 {
			//如果语言为空 例如纯图片或者语言 保持原来的放入关系
			for _, teach := range teachLang {
				for _, learn := range learnLang {
					teachToLearn = append(teachToLearn, fmt.Sprintf("LANG#%v-%v", teach, learn))
				}
			}
			// infoLog.Printf("ProcModMntBucketService teachToLearn=%v", teachToLearn)
			for _, v := range teachToLearn {
				// infoLog.Printf("ProcModMntBucketService index=%v lang=%s", i, v)
				index, err := ssdbOperator.InsertStrMidIntoHashMap(v, string(mid))
				if err != nil {
					infoLog.Printf("ProcModMntBucketService InsertStrMidIntoHashMap failed uid=%v mid=%v hashName=%s",
						head.Uid,
						mid,
						v)

				} else {
					// add to upate list
					iterm := &ht_moment_cache.HashMapIterm{
						HashName: proto.String(v),
						Key:      proto.String(fmt.Sprintf("%v", index)),
						Value:    proto.String(string(mid)),
						Expire:   proto.Uint64(0),
					}
					updateList = append(updateList, iterm)
				}
			}
		} else {
			infoLog.Printf("ProcModMntBucketService lang match did not found moment lang=%s", langType)
		}

		// add native lang
		nativeHashMap := fmt.Sprintf("LANG#%v", nativeLang)
		index, err := ssdbOperator.InsertStrMidIntoHashMap(nativeHashMap, string(mid))
		if err != nil {
			infoLog.Printf("ProcModMntBucketService InsertStrMidIntoHashMap failed uid=%v mid=%v hashName=%s",
				head.Uid,
				mid,
				nativeHashMap)
		} else {
			// add to upate list
			iterm := &ht_moment_cache.HashMapIterm{
				HashName: proto.String(nativeHashMap),
				Key:      proto.String(fmt.Sprintf("%v", index)),
				Value:    proto.String(string(mid)),
				Expire:   proto.Uint64(0),
			}
			updateList = append(updateList, iterm)
		}

		// add to learn bucket
		for _, learn := range learnLang {
			learnHashMap := fmt.Sprintf("LANG#-%v", learn)
			index, err = ssdbOperator.InsertStrMidIntoHashMap(learnHashMap, string(mid))
			if err != nil {
				infoLog.Printf("ProcModMntBucketService InsertStrMidIntoHashMap failed uid=%v mid=%v hashName=%s",
					head.Uid,
					mid,
					learnHashMap)

			} else {
				// add to upate list
				iterm := &ht_moment_cache.HashMapIterm{
					HashName: proto.String(learnHashMap),
					Key:      proto.String(fmt.Sprintf("%v", index)),
					Value:    proto.String(string(mid)),
					Expire:   proto.Uint64(0),
				}
				updateList = append(updateList, iterm)
			}
		}

		// 放入学习桶 国籍 + 母语
		if langNumber, ok := gMapShortLangToNum[langType]; ok {
			if (nativeLang == util.ChineseTraditional || nativeLang == util.ChineseGuangDoHua) && (langNumber == util.ChineseSimple) {
				//发帖人的母语是繁体 且识别语言是简体
				langNumber = nativeLang //修正语言为繁体
			}
			if nativeLang == langNumber { // 首先发帖人的母语跟帖子的语言匹配
				if ok := IsLegalLength(langNumber, subReqBody.GetContentByteLen(), subReqBody.GetContentWords()); ok { // 长度满足条件
					if isLegalNative := IsLegalNativeLang(national, langNumber); isLegalNative {
						hashName := fmt.Sprintf("%s%v", util.LearnLangPrefix, langNumber)
						// infoLog.Printf("ProcModMntBucketService hashName=%s", hashName)
						index, err := ssdbOperator.InsertStrMidIntoHashMap(hashName, string(mid))
						if err != nil {
							infoLog.Printf("ProcModMntBucketService InsertStrMidIntoHashMap failed uid=%v mid=%v hashName=%s",
								head.Uid,
								mid,
								hashName)
						} else {
							// add to upate list
							iterm := &ht_moment_cache.HashMapIterm{
								HashName: proto.String(hashName),
								Key:      proto.String(fmt.Sprintf("%v", index)),
								Value:    proto.String(string(mid)),
								Expire:   proto.Uint64(0),
							}
							updateList = append(updateList, iterm)
						}
					} else {
						infoLog.Printf("ProcModMntBucketService national=%s langNum=%v not match", national, langNumber)
					}
				} else {
					infoLog.Printf("ProcModMntBucketService not enougth uid=%v mid=%s byteLen=%v words=%v",
						head.Uid,
						mid,
						subReqBody.GetContentByteLen(),
						subReqBody.GetContentWords())
				}
			} else {
				infoLog.Printf("ProcModMntBucketService mid=%s uid=%v nativeLang=%v content lang=%v not match", mid, uid, nativeLang, langNumber)
			}
		} else {
			infoLog.Printf("ProcModMntBucketService version=%v UnKnow lang type=%s", subReqBody.GetVersion(), langType)
		}
	}
	// for i, v := range updateList {
	// 	infoLog.Printf("ProcModMntBucketService i=%v index=%s mid=%s", i, v.GetKey(), string(v.GetValue()))
	// }
	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		BatchAddMidToHashmapReqbody: &ht_moment_cache.BatchAddMidToHashMapReqBody{
			HashIterms: updateList,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcModMntBucketService proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/batch_add_mid_hash_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_BATCH_ADD_MID_TO_HASH)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	// 发布发帖事件到NSQ 等待消费者消费
	err = PublishPostEvent(head.Uid, time.Now().Unix(), string(mid))
	if err != nil {
		infoLog.Printf("ProcModMntBucketService PublishPostEvent failed uid=%v mid=%s err=%s",
			head.Uid,
			mid,
			err)
	}
	totalProcTime := packet.CalcProcessTime()
	infoLog.Printf("ProcModMntBucketService uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	return true
}

// 16.proc post cmnt service 用于发布cmnt相关内容
func ProcPostCmntService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcPostCmntService no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcPostCmntService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/post_cmnt_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcPostCmntService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetPostCmntReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcPostCmntService GetPostCmntReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step1: param check
	mid := subReqBody.GetMid()
	mntCreater := subReqBody.GetMntCreater()
	mntDeleted := subReqBody.GetMntDeleted()
	cmntUid := subReqBody.GetCmntUid()
	cmntType := subReqBody.GetCmntType()
	cmntDeleted := subReqBody.GetCmntDeleted()
	replyList := subReqBody.GetToidList()
	if len(mid) < 6 || mntCreater == 0 || cmntUid == 0 {
		infoLog.Printf("ProcPostCmntService invalid param uid=%v mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v",
			head.Uid,
			mid,
			mntCreater,
			mntDeleted,
			cmntUid,
			cmntType,
			cmntDeleted)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v toIdList=%v",
		mid,
		mntCreater,
		mntDeleted,
		cmntUid,
		cmntType,
		cmntDeleted,
		replyList)
	// Step2: 生成cid
	maxCid, err := ssdbOperator.Hincr(string(mid), util.TotalCmntCountKey, 1)
	if err != nil {
		infoLog.Printf("ProcPostCmntService ssdbOperator.Hincr return err=%s mid=%s key=%s",
			err,
			mid,
			util.TotalCmntCountKey)
		attr := "gomntdbd/hinc_err"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	}
	// Step2 将cid保存到stComnt中
	storeCmntBody := new(ht_moment_store.StoreCommentBody)
	err = proto.Unmarshal(subReqBody.GetCmntContent(), storeCmntBody)
	if err != nil {
		infoLog.Printf("ProcPostCmntService proto Unmarshal failed mid=%s midCreater=%v cmntUid=%v", mid, mntCreater, cmntUid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	storeCmntBody.Cid = []byte(fmt.Sprintf("%v", maxCid))
	storeCmntBody.PostTime = proto.Uint64(getTimeMicroSecond())
	storeCmntSlic, err := proto.Marshal(storeCmntBody)
	if err != nil {
		infoLog.Printf("ProcPostCmntService proto.Marshal(storeCmntBody) failed mid=%s midCreater=%v cmntUid=%v", mid, mntCreater, cmntUid)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}

	// 定义更新redis的keyvalues
	var keyValues []string
	// add max cid
	keyValues = append(keyValues, util.TotalCmntCountKey)
	keyValues = append(keyValues, fmt.Sprintf("%v", maxCid))
	// Step3: 存储评论信息
	strCid := fmt.Sprintf("%v", maxCid+util.BaseCmntIndex)
	err = ssdbOperator.Hset(string(mid), strCid, string(storeCmntSlic))
	if err != nil {
		infoLog.Printf("ProcPostCmntService Hset failed mid=%s strCid=%s uid=%v err=%s",
			mid,
			strCid,
			head.Uid,
			err)
		attr := "gomntdbd/hset_err"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	}
	// add comment content
	keyValues = append(keyValues, strCid)
	keyValues = append(keyValues, string(storeCmntSlic))

	// Step4: 改错的cid单独存在一个set里, 用于置顶
	if cmntType == ht_moment_cache.COMMENT_TYPE_CORRECT {
		zsetName := GetCorrectSetName(string(mid))
		zsetKey := fmt.Sprintf("%v", maxCid)
		err = ssdbOperator.Zset(zsetName, zsetKey, time.Now().Unix())
		if err != nil {
			infoLog.Printf("ProcPostCmntService Zset failed zsetName=%s zsetKey=%s uid=%v err=%s",
				zsetName,
				zsetKey,
				head.Uid,
				err)
			attr := "gomntdbd/zset_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
		// 写ssdb成功之后再去更新学习点数
		// 先查redis，如果redis中存在则双写redis和mysql, 否则单写mysql
		pointName := "CTCR_" + fmt.Sprintf("%v", cmntUid)
		pointKey := "correct"
		isExist, err := redisApi.Hexists(pointName, pointKey)
		if err != nil {
			infoLog.Printf("ProcPostCmntService mid=%s pointName=%s pointKey=%s redisApi.Hexists err=%s",
				mid,
				pointName,
				pointKey,
				err)
		} else {
			if isExist > 0 { // 存在
				_, err = redisApi.Hincrby(pointName, pointKey, 1)
				if err != nil {
					infoLog.Printf("ProcPostCmntService mid=%s pointName=%s pointKey=%s redisApi.Hincrby err=%s",
						mid,
						pointName,
						pointKey,
						err)
				}
			} else { // 不存在
				infoLog.Printf("ProcPostCmntService mid=%s pointName=%s pointKey=%s not exist isExist=%v",
					mid,
					pointName,
					pointKey,
					isExist)
			}
		}
		// 更新mysql
		err = UpdateCorrectPoint(cmntUid)
		if err != nil {
			infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v UpdateCorrectPoint failed err=%s",
				mid,
				mntCreater,
				mntDeleted,
				cmntUid,
				cmntType,
				cmntDeleted,
				err)
		}
	}
	// Step5:只有评论的状态为0 才增加评论的计数
	if cmntDeleted == 0 {
		cmntCnt, err := ssdbOperator.Hincr(string(mid), util.CommentCountKey, 1)
		if err != nil {
			infoLog.Printf("ProcPostCmntService Hset failed mid=%s strCid=%s uid=%v err=%s",
				mid,
				strCid,
				head.Uid,
				err)
			attr := "gomntdbd/hset_err"
			libcomm.AttrAdd(attr, 1)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		} else {
			keyValues = append(keyValues, util.CommentCountKey)
			keyValues = append(keyValues, fmt.Sprintf("%v", cmntCnt))
		}
	}
	// Step6: 更新最后一次评论时间
	curMicroSecond := getTimeMicroSecond()
	strCurTime := fmt.Sprintf("%v", curMicroSecond)
	err = ssdbOperator.Hset(string(mid), util.CommentTsKey, strCurTime)
	if err != nil {
		infoLog.Printf("ProcPostCmntService Hset failed mid=%s strKey=%s strValue=%s uid=%v err=%s",
			mid,
			util.CommentTsKey,
			strCurTime,
			head.Uid,
			err)
		attr := "gomntdbd/hset_err"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
		rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	} else {
		keyValues = append(keyValues, util.CommentTsKey)
		keyValues = append(keyValues, strCurTime)
	}
	// Step7: 如果帖子已经删除，则直接返回 如果评论被隐藏 直接返回不添加事件通知
	needNotify := true
	if (mntDeleted > 0 && mntDeleted != uint32(ht_moment_cache.MntStatOpType_OP_FOR_FOLLOWER_ONLY)) || cmntDeleted > 0 {
		infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v",
			mid,
			mntCreater,
			mntDeleted,
			cmntUid,
			cmntType,
			cmntDeleted)
		needNotify = false
	}
	//Step8: 写到通知列表, 先放到这里吧，以后移到逻辑进程异步处理
	notifyList := []*ht_moment_cache.NotifyInfo{}
	notifyCid := fmt.Sprintf("%v", maxCid)
	if needNotify {
		notifyType := ht_moment_store.STORE_NOTIFY_TYPE_NOTIFY_CMNT
		if cmntType == ht_moment_cache.COMMENT_TYPE_CORRECT {
			notifyType = ht_moment_store.STORE_NOTIFY_TYPE_NOTIFY_MODIFY
		}
		notifyList, err = AddNotifyEvent(mntCreater, cmntUid, mid, []byte(notifyCid), notifyType, replyList)
		if err != nil {
			infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v AddNotifyEvent failed err=%s",
				mid,
				mntCreater,
				mntDeleted,
				cmntUid,
				cmntType,
				cmntDeleted,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
		//Step9: // 如果发布评论和发布帖子的是同一个用户 且当前帖子没有@任何用户 则通知所有已近评论过的用户
		if mntCreater == cmntUid && len(replyList) == 0 {
			cmntUidSetName := string(mid) + util.CmntUidSetSuffix
			// infoLog.Printf("cmntUidSetName=%s", cmntUidSetName)
			cmntUidList, err := ssdbOperator.ZrangeListWithOutScore(cmntUidSetName, 0, util.ZsetKeysSize, 0)
			if err != nil {
				infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v zrangelist failed err=%s",
					mid,
					mntCreater,
					mntDeleted,
					cmntUid,
					cmntType,
					cmntDeleted,
					err)
			}
			var notifyUidList []uint32
			for _, v := range cmntUidList {
				tempUid, err := strconv.ParseUint(v, 10, 32)
				if err != nil {
					infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v strconv.ParseUint failed err=%s",
						mid,
						mntCreater,
						mntDeleted,
						cmntUid,
						cmntType,
						cmntDeleted,
						err)
					continue
				}
				if mntCreater == uint32(tempUid) {
					infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v equal",
						mid,
						mntCreater,
						mntDeleted,
						cmntUid,
						cmntType,
						cmntDeleted)
					continue
				}
				// step1:产生通知 写入SSDB中
				notifyUidList = append(notifyUidList, uint32(tempUid))
				// step2:发送推送
				notifyItem := &ht_moment_cache.NotifyInfo{
					TargetUid: proto.Uint32(uint32(tempUid)),
					Type:      ht_moment_cache.NOTIFY_TYPE_NOTIFY_CMNT_REPLY.Enum(),
				}
				notifyList = append(notifyList, notifyItem)
			}
			if len(notifyUidList) > 0 {
				err = AddNotifyEventToCmntUser(mntCreater, cmntUid, mid, []byte(notifyCid), notifyUidList)
				if err != nil {
					infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v cmntType=%v cmntDeleted=%v AddNotifyEventToCmntUser err=%s",
						mid,
						mntCreater,
						mntDeleted,
						cmntUid,
						cmntType,
						cmntDeleted,
						err)
				}
			}
		}

		//Step11: 将评论用户添加到评论用户列表中
		strcmntUid := fmt.Sprintf("%v", cmntUid)
		cmntUidSetName := string(mid) + util.CmntUidSetSuffix
		infoLog.Printf("ProcPostCmntService mid=%s mntCreater=%v cmntUid=%v strcmntUid=%s",
			mid,
			mntCreater,
			cmntUid,
			strcmntUid)
		err = ssdbOperator.Zset(cmntUidSetName, strcmntUid, time.Now().Unix())
		if err != nil {
			infoLog.Printf("ProcPostCmntService ssdbOperator.Zset failed mid=%s midCreater=%v cmntUid=%v err=%s", mid, mntCreater, cmntUid, err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
	}
	// send resp
	result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody.PostCmntRspbody = &ht_moment_cache.PostCmntRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Mid:        mid,
		Cid:        []byte(notifyCid),
		PostTime:   proto.Uint64(curMicroSecond),
		NotifyList: notifyList,
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		SetMidHashMapReqbody: &ht_moment_cache.SetMidHashMapReqBody{
			HashName: proto.String(string(mid)),
			KeyValue: keyValues,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcPostCmntService proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/set_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_MID_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	totalProcTime := packet.CalcProcessTime()

	infoLog.Printf("ProcPostCmntService uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/update_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func UpdateCorrectPoint(uid uint32) (err error) {
	if db == nil || uid == 0 {
		err = util.ErrNilDbObject
		return err
	}
	_, err = db.Exec("INSERT INTO MG_USER_COUNTINFO (USERID, MOMENT_CORRECT) VALUES (?, 1) ON DUPLICATE KEY UPDATE MOMENT_CORRECT=MOMENT_CORRECT+VALUES(MOMENT_CORRECT);", uid)
	if err != nil {
		infoLog.Printf("UpdateCorrectPoint failed uid=%v err=%v", uid, err)
		return err
	} else {
		return nil
	}
}

// 17.Proc mod cmnt stat
func ProcModCmntStatReq(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcModCmntStatReq no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcModCmntStatReq invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/mod_cmnt_stat_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcModCmntStatReq proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetModCmntStateReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcModCmntStatReq GetModCmntStateReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	opUid := subReqBody.GetUserid()
	mid := subReqBody.GetMid()
	cid := subReqBody.GetCid()
	ctype := subReqBody.GetCtype()
	modStat := subReqBody.GetStat()
	infoLog.Printf("ProcModCmntStatReq opUid=%v mid=%s cid=%s ctype=%v modStat=%v",
		opUid,
		mid,
		cid,
		ctype,
		modStat)
	// Step1: param check
	if len(mid) == 0 || len(cid) == 0 {
		infoLog.Printf("ProcModCmntStatReq input param error opUid=%v mid=%s cid=%s ctype=%v modStat=%v",
			opUid,
			mid,
			cid,
			ctype,
			modStat)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("param error"),
			},
		}
		return false
	}
	var keyValues []string
	prevStat, cmntValue, err := ssdbOperator.UpdateCmntStat(string(mid), string(cid), modStat)
	if err != nil {
		infoLog.Printf("ProcModCmntStatReq ssdbOperator.UpdateCmntStat opUid=%v mid=%s cid=%s ctype=%v modStat=%v err=%s",
			opUid,
			mid,
			cid,
			ctype,
			modStat,
			err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// Step2: add key and value to slic
	keyValues = append(keyValues, string(cid))
	keyValues = append(keyValues, cmntValue)

	// Step3: update cmntCnt
	if prevStat == 0 && modStat != 0 { // 0-->other desc comment count especial 0->1
		cmntCnt, err := ssdbOperator.Hincr(string(mid), util.CommentCountKey, -1)
		if err != nil {
			infoLog.Printf("ProcModCmntStatReq ssdbOperator.Hincr desc count opUid=%v mid=%s cid=%s ctype=%v modStat=%v err=%s",
				opUid,
				mid,
				cid,
				ctype,
				modStat,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		} else {
			infoLog.Printf("ProcModCmntStatReq ssdbOperator.Hincr opUid=%v mid=%s cid=%s ctype=%v modStat=%v cmntCnt=%v",
				opUid,
				mid,
				cid,
				ctype,
				modStat,
				cmntCnt)
		}
		keyValues = append(keyValues, util.CommentCountKey)
		keyValues = append(keyValues, fmt.Sprintf("%v", cmntCnt))
	} else if prevStat != 0 && modStat == 0 {
		cmntCnt, err := ssdbOperator.Hincr(string(mid), util.CommentCountKey, 1)
		if err != nil {
			infoLog.Printf("ProcModCmntStatReq ssdbOperator.Hincr inc count opUid=%v mid=%s cid=%s ctype=%v modStat=%v err=%s",
				opUid,
				mid,
				cid,
				ctype,
				modStat,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		} else {
			infoLog.Printf("ProcModCmntStatReq ssdbOperator.Hincr opUid=%v mid=%s cid=%s ctype=%v modStat=%v cmntCnt=%v",
				opUid,
				mid,
				cid,
				ctype,
				modStat,
				cmntCnt)
		}
		keyValues = append(keyValues, util.CommentCountKey)
		keyValues = append(keyValues, fmt.Sprintf("%v", cmntCnt))
	} else {
		infoLog.Printf("ProcModCmntStatReq opUid=%v mid=%s cid=%s ctype=%v modStat=%v prevStat=%v",
			opUid,
			mid,
			cid,
			ctype,
			modStat,
			prevStat)
	}
	// Step5: update last comment time
	curTime := fmt.Sprintf("%v", getTimeMicroSecond())
	err = ssdbOperator.Hset(string(mid), util.CommentTsKey, curTime)
	if err != nil {
		infoLog.Printf("ProcModCmntStatReq ssdbOperator.Hset opUid=%v mid=%s cid=%s ctype=%v modStat=%v err=%s",
			opUid,
			mid,
			cid,
			ctype,
			modStat,
			err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	keyValues = append(keyValues, util.CommentTsKey)
	keyValues = append(keyValues, curTime)

	// Step6: delete cmnt uid
	if prevStat == 0 && modStat == ht_moment_cache.COMMENT_STAT_STAT_DEL { // 0-->1 删除comment
		cmntUidSetName := string(mid) + util.CmntUidSetSuffix
		_, err := ssdbOperator.Zdel(cmntUidSetName, fmt.Sprintf("%v", opUid))
		if err != nil {
			infoLog.Printf("ProcModCmntStatReq ssdbOperator.Zdel opUid=%v mid=%s cid=%s ctype=%v modStat=%v err=%s",
				opUid,
				mid,
				cid,
				ctype,
				modStat,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
			rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("internal error"),
				},
			}
			return false
		}
	}

	//Step7: desc correct count
	if ctype == ht_moment_cache.COMMENT_TYPE_CORRECT {
		if prevStat == 0 && modStat != 0 { // 0->other desc count
			err = DescCorrectPoint(opUid)
			if err != nil {
				infoLog.Printf("ProcModCmntStatReq DescCorrectPoint opUid=%v mid=%s cid=%s ctype=%v modStat=%v err=%s",
					opUid,
					mid,
					cid,
					ctype,
					modStat,
					err)
				attr := "gomntdbd/update_correct_point_failed"
				libcomm.AttrAdd(attr, 1)
			}
		} else if prevStat != 0 && modStat == 0 { // other-->0 inc count
			err = IncCorrectPoint(opUid)
			if err != nil {
				infoLog.Printf("ProcModCmntStatReq IncCorrectPoint opUid=%v mid=%s cid=%s ctype=%v modStat=%v err=%s",
					opUid,
					mid,
					cid,
					ctype,
					modStat,
					err)
				attr := "gomntdbd/update_correct_point_failed"
				libcomm.AttrAdd(attr, 1)
			}
		} else {
			infoLog.Printf("ProcModCmntStatReq opUid=%v mid=%s cid=%s ctype=%v modStat=%v prevStat=%v no need update comment count",
				opUid,
				mid,
				cid,
				ctype,
				modStat,
				err)
		}
	}
	// send resp
	rspBody.ModCmntStateRspbody = &ht_moment_cache.ModCmntStateRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		SetMidHashMapReqbody: &ht_moment_cache.SetMidHashMapReqBody{
			HashName: proto.String(string(mid)),
			KeyValue: keyValues,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcModCmntStatReq proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/set_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_MID_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	totalProcTime := packet.CalcProcessTime()

	infoLog.Printf("ProcModCmntStatReq uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/mod_comnt_stat_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 18.proc post like service 用于处理用户的点赞
func ProcPostLikeService(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcPostLikeService no need call")
		}
	}()
	payLoad := packet.GetBody()
	// 检查输入参数是否为空
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcPostLikeService invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/post_like_req_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcPostLikeService proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetPostLikeReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcPostLikeService GetPostCmntReqbody() failed")
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	// Step1: param check
	uid := subReqBody.GetUserid()
	mid := subReqBody.GetMid()
	likeOrNot := subReqBody.GetLikeOrNot()
	mntCreater := subReqBody.GetMntCreater()
	mntDelete := subReqBody.GetDelete()
	if len(mid) < 6 || uid == 0 {
		infoLog.Printf("ProcPostLikeService invalid param uid=%v mid=%s likeOrNot=%v mntCreater=%v delete=%v",
			uid,
			mid,
			likeOrNot,
			mntCreater,
			mntDelete)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param error"),
			},
		}
		return false
	}
	infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v mntCreater=%v delete=%v",
		uid,
		mid,
		likeOrNot,
		mntCreater,
		mntDelete)
	// Step2: 判断是否已经点过赞
	likeSetName := string(mid) + util.LikeUidSetSuffix
	uidKey := fmt.Sprintf("%v", uid)
	isAlready := false
	score, err := ssdbOperator.Zget(likeSetName, uidKey)
	if err != nil {
		infoLog.Printf("ProcPostLikeService ssdbOperator.Zget return err=%s likeSetName=%s key=%s mntCreater=%v delete=%v",
			err,
			likeSetName,
			uidKey,
			mntCreater,
			mntDelete)
		// 没有点过赞 不需要更新isAlready
		if err != redis.ErrNil { // 访问redis出错
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
	} else { //已经点过赞了
		isAlready = true
		infoLog.Printf("ProcPostLikeService ssdbOperator.Zget likeSetName=%s key=%s score=%v mntCreater=%v delete=%v",
			likeSetName,
			uidKey,
			score,
			mntCreater,
			mntDelete)
	}
	// Step3 增删like_uid_set
	incrBy := 0
	if likeOrNot > 0 {
		if isAlready {
			infoLog.Printf("uid=%v mid=%s likeOrNot=%v already like!",
				uid,
				mid,
				likeOrNot)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("success"),
				},
				Mid:        mid,
				NotifyList: nil,
			}
			return true
		}
		// add like
		err = ssdbOperator.Zset(likeSetName, uidKey, time.Now().Unix())
		if err != nil {
			infoLog.Printf("uid=%v mid=%s likeOrNot=%v ssdbOperator.Zset err=%s",
				uid,
				mid,
				likeOrNot,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
		incrBy = 1
	} else {
		if !isAlready {
			infoLog.Printf("uid=%v mid=%s likeOrNot=%v mntCreater=%v delete=%v Never liked, why cancel?",
				uid,
				mid,
				likeOrNot,
				mntCreater,
				mntDelete)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("success"),
				},
				Mid:        mid,
				NotifyList: nil,
			}
			return true
		}
		// remove like
		_, err = ssdbOperator.Zdel(likeSetName, uidKey)
		if err != nil {
			infoLog.Printf("uid=%v mid=%s likeOrNot=%v ssdbOperator.Zdel err=%s",
				uid,
				mid,
				likeOrNot,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
		incrBy = -1
	}
	// Step4: 点赞数量加1
	// 定义更新redis的keyvalues
	var keyValues []string
	if incrBy != 0 {
		likeCount, err := ssdbOperator.Hincr(string(mid), util.LikedCountKey, incrBy)
		if err != nil {
			infoLog.Printf("ProcPostLikeService ssdbOperator.Hincr mid=%s key=%s incrBy=%v",
				mid,
				util.LikedCountKey,
				incrBy)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}

		infoLog.Printf("ProcPostLikeService ssdbOperator.Hincr mid=%s key=%s likeCount=%v ",
			mid,
			util.LikedCountKey,
			likeCount)
		keyValues = append(keyValues, util.LikedCountKey)
		keyValues = append(keyValues, fmt.Sprintf("%v", likeCount))
	}
	// Step5: 更新最后一次点赞时间
	curMicroSecond := getTimeMicroSecond()
	strCurTime := fmt.Sprintf("%v", curMicroSecond)
	err = ssdbOperator.Hset(string(mid), util.LikedTsKey, strCurTime)
	if err != nil {
		infoLog.Printf("ProcPostLikeService Hset failed mid=%s strKey=%s strValue=%s uid=%v err=%s",
			mid,
			util.LikedTsKey,
			strCurTime,
			head.Uid,
			err)
		attr := "gomntdbd/hset_err"
		libcomm.AttrAdd(attr, 1)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
		rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		return false
	} else {
		keyValues = append(keyValues, util.LikedTsKey)
		keyValues = append(keyValues, strCurTime)
	}
	// Step6: 更新总的点赞数量
	userInfoName := fmt.Sprintf("%v", mntCreater) + util.UserInfoSuffix
	infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v mntCreater=%v delete=%v userInfoName=%s",
		uid,
		mid,
		likeOrNot,
		mntCreater,
		mntDelete,
		userInfoName)
	if mntDelete == uint32(ht_moment_cache.MntStatOpType_OP_RESTORE) ||
		mntDelete == uint32(ht_moment_cache.MntStatOpType_OP_FOR_FOLLOWER_ONLY) {
		totalLikeCnt, err := ssdbOperator.Hincr(userInfoName, util.MntLikeCountKey, incrBy)
		if err != nil {
			infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v ssdbOperator.Hincr err=%s",
				uid,
				mid,
				likeOrNot,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}

		otherLikeCnt, err := ssdbOperator.Hincr(userInfoName, util.MntLikeCountForOther, incrBy)
		if err != nil {
			infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v ssdbOperator.Hincr err=%s",
				uid,
				mid,
				likeOrNot,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
		if totalLikeCnt < 0 || otherLikeCnt < 0 {
			infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v totalLikeCnt=%v otherLikeCnt=%v",
				uid,
				mid,
				likeOrNot,
				totalLikeCnt,
				otherLikeCnt)
		}
	} else if mntDelete == uint32(ht_moment_cache.MntStatOpType_OP_HIDE) {
		totalLikeCnt, err := ssdbOperator.Hincr(userInfoName, util.MntLikeCountKey, incrBy)
		if err != nil {
			infoLog.Printf("uid=%v mid=%s likeOrNot=%v ssdbOperator.Hincr err=%s",
				uid,
				mid,
				likeOrNot,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
		if totalLikeCnt < 0 {
			infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v totalLikeCnt=%v",
				uid,
				mid,
				likeOrNot,
				totalLikeCnt)
		}
	}
	needNotify := true
	if likeOrNot == 0 || (likeOrNot > 0 && isAlready) {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v mntCreater=%v delete=%v need not notify",
			uid,
			mid,
			likeOrNot,
			mntCreater,
			mntDelete)
		needNotify = false
	}
	//Step8: 写到通知列表, 先放到这里吧，以后移到逻辑进程异步处理
	notifyList := []*ht_moment_cache.NotifyInfo{}
	if needNotify {
		notifyCid := ""
		notifyType := ht_moment_store.STORE_NOTIFY_TYPE_NOTIFY_LIKE
		notifyList, err = AddNotifyEvent(mntCreater, uid, mid, []byte(notifyCid), notifyType, nil)
		if err != nil {
			infoLog.Printf("ProcPostLikeService mid=%s mntCreater=%v mntDeleted=%v cmntUid=%v AddNotifyEvent failed err=%s",
				mid,
				mntCreater,
				mntDelete,
				err)
			result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SSDB_ERR)
			rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
				Status: &ht_moment_cache.MntCacheHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("ssdb error"),
				},
			}
			return false
		}
	}
	// send resp
	result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody.PostLikeRspbody = &ht_moment_cache.PostLikeRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		Mid:        mid,
		NotifyList: notifyList,
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	// 更新redis
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		SetMidHashMapReqbody: &ht_moment_cache.SetMidHashMapReqBody{
			HashName: proto.String(string(mid)),
			KeyValue: keyValues,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcPostLikeService proto.Marshal failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/set_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_MID_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	// 删除xxxx_user_info 缓存
	err = ClearUserInfoCacheInRedis(head, mntCreater)
	if err != nil {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v mntCreater=%v delete=%v ClearUserInfoCacheInRedis err=%s",
			uid,
			mid,
			likeOrNot,
			mntCreater,
			mntDelete,
			err)
	}

	err = DeleteKeyInRedis(head, uid, likeSetName)
	if err != nil {
		infoLog.Printf("ProcPostLikeService uid=%v mid=%s likeOrNot=%v mntCreater=%v delete=%v DeleteKeyInRedis key=%s err=%s",
			uid,
			mid,
			likeOrNot,
			mntCreater,
			mntDelete,
			likeSetName,
			err)
	}

	totalProcTime := packet.CalcProcessTime()

	infoLog.Printf("ProcPostLikeService uid=%v totalProcTime=%v", head.Uid, totalProcTime)
	if totalProcTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/update_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 19.proc get user info
func ProcGetUserInfo(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment_cache.MntCacheRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserInfo no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment_cache.GetUserInfoRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserInfo invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gomntdbd/get_user_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment_cache.MntCacheReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment_cache.GetUserInfoRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserInfoReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserInfo GetGetUserInfoRspbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_PB_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment_cache.GetUserInfoRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	userId := subReqBody.GetUserid()
	hashName := GetUserInfoHashMapName(userId)
	outKeys, outValues, err := ssdbOperator.MultiHgetAllSlice(hashName)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo ssdbOperator.MultiHgetAllSlice failed userId=%v hashName=%s err=%s", userId, hashName, err)
		result = uint16(ht_moment_cache.MNT_RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment_cache.GetUserInfoRspBody{
			Status: &ht_moment_cache.MntCacheHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		attr := "gomntdbd/get_user_info_db_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	infoLog.Printf("ProcGetUserInfo userId=%v outKeys=%v outValues=%v", userId, outKeys, outValues)
	var momentCnt, likeCnt, otherMomentCnt, otherLikeCnt uint32
	keyValuesLen := len(outKeys)
	for index := 0; index < keyValuesLen; index = index + 1 {
		key := outKeys[index]
		value, err := strconv.ParseUint(outValues[index], 10, 32)
		if err != nil {
			infoLog.Printf("ProcGetUserInfo strconv.ParseUint failed key=%s values=%s", key, outValues[index])
			value = 0
		}
		switch key {
		case util.MntCountForSelfKey:
			momentCnt = uint32(value)
		case util.MntLikeCountKey:
			likeCnt = uint32(value)
		case util.MntCountForOtherKey:
			otherMomentCnt = uint32(value)
		case util.MntLikeCountForOther:
			otherLikeCnt = uint32(value)
		default:
			infoLog.Printf("ProcGetUserInfo unhandle key=%s value=%s", key, outValues[index])
		}
	}
	var setMomentCnt, setLikeCnt uint32
	if head.Uid == userId { // see self moment count
		setMomentCnt = momentCnt
		setLikeCnt = likeCnt
	} else { // see other moment count
		setMomentCnt = otherMomentCnt
		setLikeCnt = otherLikeCnt
	}

	result = uint16(ht_moment_cache.MNT_RET_CODE_RET_SUCCESS)
	rspBody.GetUserInfoRspbody = &ht_moment_cache.GetUserInfoRspBody{
		Status: &ht_moment_cache.MntCacheHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MomentCount: proto.Uint32(setMomentCnt),
		LikedCount:  proto.Uint32(setLikeCnt),
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 notify write agent to update redis
	// init redis keys and value
	var redisKeyValues []string
	redisKeyValues = append(redisKeyValues, util.MntCountForSelfKey)
	redisKeyValues = append(redisKeyValues, fmt.Sprintf("%v", momentCnt))
	redisKeyValues = append(redisKeyValues, util.MntLikeCountKey)
	redisKeyValues = append(redisKeyValues, fmt.Sprintf("%v", likeCnt))
	redisKeyValues = append(redisKeyValues, util.MntCountForOtherKey)
	redisKeyValues = append(redisKeyValues, fmt.Sprintf("%v", otherMomentCnt))
	redisKeyValues = append(redisKeyValues, util.MntLikeCountForOther)
	redisKeyValues = append(redisKeyValues, fmt.Sprintf("%v", otherLikeCnt))
	setReqBody := &ht_moment_cache.MntCacheReqBody{
		SetHashMapReqbody: &ht_moment_cache.SetHashMapReqBody{
			HashName:  proto.String(hashName),
			KeyValues: redisKeyValues,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo proto.Marshal set hash map req failed uid=%v err=%s", head.Uid, err)
		attr := "gomntdbd/set_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment_cache.CMD_TYPE_CMD_SET_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	procTime := packet.CalcProcessTime()
	if procTime > util.ProcTimeThresold {
		// add static
		attr := "gomntdbd/set_hash_map_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

func GetUserInfoHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v_user_info", uid)
	return hashName
}
func DescCorrectPoint(uid uint32) (err error) {
	if db == nil || uid == 0 {
		err = util.ErrNilDbObject
		return err
	}
	_, err = db.Exec("update MG_USER_COUNTINFO set MOMENT_CORRECT=MOMENT_CORRECT-1 where MOMENT_CORRECT > 0 and USERID=?;", uid)
	if err != nil {
		infoLog.Printf("DescCorrectPoint failed uid=%v err=%v", uid, err)
		return err
	} else {
		return nil
	}
}

func IncCorrectPoint(uid uint32) (err error) {
	if db == nil || uid == 0 {
		err = util.ErrNilDbObject
		return err
	}
	_, err = db.Exec("update MG_USER_COUNTINFO set MOMENT_CORRECT=MOMENT_CORRECT+1 where USERID=?;", uid)
	if err != nil {
		infoLog.Printf("IncCorrectPoint failed uid=%v err=%v", uid, err)
		return err
	} else {
		return nil
	}
}

func AddNotifyEvent(mntCreater, cmntUid uint32,
	mid, cid []byte,
	notifyType ht_moment_store.STORE_NOTIFY_TYPE,
	replyIdList []*ht_moment_cache.ReplyInfo) (notifySlic []*ht_moment_cache.NotifyInfo, err error) {
	if mntCreater == 0 || cmntUid == 0 || len(mid) == 0 {
		infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v input error",
			mntCreater,
			cmntUid,
			mid,
			cid,
			notifyType,
			replyIdList)
		err := util.ErrDbParam
		return nil, err
	}
	storeReplyList := make([]*ht_moment_store.StoreReplyInfo, len(replyIdList))
	isInAtList := false //帖子主人是否在@列表里
	for i := 0; i < len(replyIdList); i += 1 {
		item := &ht_moment_store.StoreReplyInfo{
			Userid: proto.Uint32(replyIdList[i].GetUserid()),
			Pos:    proto.Uint32(replyIdList[i].GetPos()),
		}
		storeReplyList[i] = item
		//帖子主人是否在@列表里
		if mntCreater == replyIdList[i].GetUserid() {
			isInAtList = true
		}
	}
	infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v",
		mntCreater,
		cmntUid,
		mid,
		cid,
		notifyType)
	// infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v storeReplyList=%v",
	// 	mntCreater,
	// 	cmntUid,
	// 	mid,
	// 	cid,
	// 	notifyType,
	// 	replyIdList,
	// 	storeReplyList)
	// Step1: 帖子主人的通知信息
	storeNotifyInfo := &ht_moment_store.StoreNotifyInfo{
		Userid:    proto.Uint32(cmntUid),
		Type:      notifyType.Enum(),
		Mid:       mid,
		Cid:       cid,
		PostTime:  proto.Uint64(getTimeMicroSecond()),
		ReplyList: storeReplyList,
	}

	if mntCreater != cmntUid && !isInAtList {
		/*写NOTIFY_LIST的同时双写NOTIFY_HASHTABLE*/
		notifyHashName := GetNotifyHashName(mntCreater)
		maxIdHashKey := util.HashNotifyMaxIdKey
		curMaxMsgId, err := ssdbOperator.Hincr(notifyHashName, maxIdHashKey, 1)
		if err != nil {
			infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v hincr error=%s",
				mntCreater,
				cmntUid,
				mid,
				cid,
				notifyType,
				replyIdList,
				err)
			attr := "gomntdbd/add_notify_failed"
			libcomm.AttrAdd(attr, 1)
			return nil, err
		}
		storeNotifyInfo.MsgId = proto.Uint32(uint32(curMaxMsgId))
		storeNotifySlic, err := proto.Marshal(storeNotifyInfo)
		if err != nil {
			infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v proto.Marshal error=%s",
				mntCreater,
				cmntUid,
				mid,
				cid,
				notifyType,
				replyIdList,
				err)
			attr := "gomntdbd/add_notify_failed"
			libcomm.AttrAdd(attr, 1)
			return nil, err
		}
		strCurMsgkey := fmt.Sprintf("%v", curMaxMsgId+util.BaseNotifyIndex)
		err = ssdbOperator.Hset(notifyHashName, strCurMsgkey, string(storeNotifySlic))
		if err != nil {
			infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v Hset hashName=%s key=%s error=%s",
				mntCreater,
				cmntUid,
				mid,
				cid,
				notifyType,
				replyIdList,
				notifyHashName,
				strCurMsgkey,
				err)
			attr := "gomntdbd/add_notify_failed"
			libcomm.AttrAdd(attr, 1)
			return nil, err
		}
		// 写NOTIFY_LIST
		listName := GetNotifyListName(mntCreater)
		ret, err := ssdbOperator.QpushFront(listName, string(storeNotifySlic))
		if err != nil {
			infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v QpushFront listName=%s error=%s",
				mntCreater,
				cmntUid,
				mid,
				cid,
				notifyType,
				replyIdList,
				listName,
				err)
			return nil, err
		}
		infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v QpushFront listName=%s size=%v",
			mntCreater,
			cmntUid,
			mid,
			cid,
			notifyType,
			listName,
			ret)
		notifyItem := &ht_moment_cache.NotifyInfo{
			TargetUid: proto.Uint32(mntCreater),
			Type:      ht_moment_cache.NOTIFY_TYPE(notifyType).Enum(),
		}
		notifySlic = append(notifySlic, notifyItem)
	}

	//通知帖子主人的同时通知被回复的人
	if len(replyIdList) > 0 {
		for _, v := range replyIdList {
			if v.GetUserid() != cmntUid { // 评论者自己不通知
				notifyHashName := GetNotifyHashName(v.GetUserid())
				maxIdHashKey := util.HashNotifyMaxIdKey
				curMaxMsgId, err := ssdbOperator.Hincr(notifyHashName, maxIdHashKey, 1)
				if err != nil {
					infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v hincr error=%s",
						mntCreater,
						cmntUid,
						mid,
						cid,
						notifyType,
						replyIdList,
						err)
					attr := "gomntdbd/add_notify_failed"
					libcomm.AttrAdd(attr, 1)
					continue
				}
				storeNotifyInfo.Type = ht_moment_store.STORE_NOTIFY_TYPE_NOTIFY_CMNT_REPLY.Enum()
				storeNotifyInfo.MsgId = proto.Uint32(uint32(curMaxMsgId))
				storeNotifySlic, err := proto.Marshal(storeNotifyInfo)
				if err != nil {
					infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v proto.Marshal error=%s",
						mntCreater,
						cmntUid,
						mid,
						cid,
						notifyType,
						replyIdList,
						err)
					attr := "gomntdbd/add_notify_failed"
					libcomm.AttrAdd(attr, 1)
					continue
				}
				strCurMsgKey := fmt.Sprintf("%v", curMaxMsgId+util.BaseNotifyIndex)
				err = ssdbOperator.Hset(notifyHashName, strCurMsgKey, string(storeNotifySlic))
				if err != nil {
					infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v Hset hashName=%s key=%s error=%s",
						mntCreater,
						cmntUid,
						mid,
						cid,
						notifyType,
						replyIdList,
						notifyHashName,
						strCurMsgKey,
						err)
					attr := "gomntdbd/add_notify_failed"
					libcomm.AttrAdd(attr, 1)
					continue
				}
				// 写NOTIFY_LIST
				listName := GetNotifyListName(v.GetUserid())
				ret, err := ssdbOperator.QpushFront(listName, string(storeNotifySlic))
				if err != nil {
					infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v QpushFront listName=%s error=%s",
						mntCreater,
						cmntUid,
						mid,
						cid,
						notifyType,
						replyIdList,
						listName,
						err)
				}
				infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v replyIdList=%v QpushFront listName=%s size=%v",
					mntCreater,
					cmntUid,
					mid,
					cid,
					notifyType,
					replyIdList,
					listName,
					ret)
				notifyItem := &ht_moment_cache.NotifyInfo{
					TargetUid: proto.Uint32(v.GetUserid()),
					Type:      ht_moment_cache.NOTIFY_TYPE_NOTIFY_CMNT_REPLY.Enum(),
				}
				notifySlic = append(notifySlic, notifyItem)
			}
		}
	}
	infoLog.Printf("AddNotifyEvent mntCreter=%v cmntUid=%v mid=%s cid=%s notifyType=%v notifySlicLen=%v",
		mntCreater,
		cmntUid,
		mid,
		cid,
		notifyType,
		len(notifySlic))

	return notifySlic, nil
}

func AddNotifyEventToCmntUser(mntCreater, cmntUid uint32,
	mid, cid []byte,
	toIdList []uint32) (err error) {
	if mntCreater == 0 || cmntUid == 0 || len(mid) == 0 || len(cid) == 0 {
		infoLog.Printf("AddNotifyEventToCmntUser mntCreter=%v cmntUid=%v mid=%s cid=%s toIdList=%v input error",
			mntCreater,
			cmntUid,
			mid,
			cid,
			toIdList)
		err := util.ErrDbParam
		return err
	}

	var storeReplyList []*ht_moment_store.StoreReplyInfo
	for i := 0; i < len(toIdList); i += 1 {
		item := &ht_moment_store.StoreReplyInfo{
			Userid: proto.Uint32(toIdList[i]),
			Pos:    proto.Uint32(0), // 当群主回复时如果没有@任何用户直接在第0字节位置@所有人
		}
		storeReplyList = append(storeReplyList, item)
	}
	// Step1: 帖子主人的通知信息
	storeNotifyInfo := &ht_moment_store.StoreNotifyInfo{
		Userid:    proto.Uint32(cmntUid),
		Type:      ht_moment_store.STORE_NOTIFY_TYPE_NOTIFY_CMNT_REPLY.Enum(),
		Mid:       mid,
		Cid:       cid,
		PostTime:  proto.Uint64(getTimeMicroSecond()),
		ReplyList: storeReplyList,
	}

	//通知帖子主人的同时通知被回复的人
	if len(toIdList) > 0 {
		for _, v := range toIdList {
			if v != mntCreater { // 评论者自己不通知
				notifyHashName := GetNotifyHashName(v)
				maxIdHashKey := util.HashNotifyMaxIdKey
				curMaxMsgId, err := ssdbOperator.Hincr(notifyHashName, maxIdHashKey, 1)
				if err != nil {
					infoLog.Printf("AddNotifyEventToCmntUser mntCreter=%v cmntUid=%v mid=%s cid=%s replyIdList=%v hincr error=%s",
						mntCreater,
						cmntUid,
						mid,
						cid,
						toIdList,
						err)
					attr := "gomntdbd/add_notify_failed"
					libcomm.AttrAdd(attr, 1)
					continue
				}
				storeNotifyInfo.Type = ht_moment_store.STORE_NOTIFY_TYPE_NOTIFY_CMNT_REPLY.Enum()
				storeNotifyInfo.MsgId = proto.Uint32(uint32(curMaxMsgId))
				storeNotifySlic, err := proto.Marshal(storeNotifyInfo)
				if err != nil {
					infoLog.Printf("AddNotifyEventToCmntUser mntCreter=%v cmntUid=%v mid=%s cid=%s replyIdList=%v proto.Marshal error=%s",
						mntCreater,
						cmntUid,
						mid,
						cid,
						toIdList,
						err)
					attr := "gomntdbd/add_notify_failed"
					libcomm.AttrAdd(attr, 1)
					continue
				}
				strCurMsgKey := fmt.Sprintf("%v", curMaxMsgId+util.BaseNotifyIndex)
				err = ssdbOperator.Hset(notifyHashName, strCurMsgKey, string(storeNotifySlic))
				if err != nil {
					infoLog.Printf("AddNotifyEventToCmntUser mntCreter=%v cmntUid=%v mid=%s cid=%s replyIdList=%v Hset hashName=%s key=%s error=%s",
						mntCreater,
						cmntUid,
						mid,
						cid,
						toIdList,
						notifyHashName,
						strCurMsgKey,
						err)
					attr := "gomntdbd/add_notify_failed"
					libcomm.AttrAdd(attr, 1)
					continue
				}
				// 写NOTIFY_LIST
				listName := GetNotifyListName(v)
				ret, err := ssdbOperator.QpushFront(listName, string(storeNotifySlic))
				if err != nil {
					infoLog.Printf("AddNotifyEventToCmntUser mntCreter=%v cmntUid=%v mid=%s cid=%s replyIdList=%v QpushFront listName=%s error=%s",
						mntCreater,
						cmntUid,
						mid,
						cid,
						toIdList,
						listName,
						err)
				}
				infoLog.Printf("AddNotifyEventToCmntUser mntCreter=%v cmntUid=%v mid=%s cid=%s replyIdList=%v QpushFront listName=%s size=%v",
					mntCreater,
					cmntUid,
					mid,
					cid,
					toIdList,
					listName,
					ret)
			}
		}
	}
	infoLog.Printf("AddNotifyEventToCmntUser mntCreter=%v cmntUid=%v mid=%s cid=%s replyIdList=%v",
		mntCreater,
		cmntUid,
		mid,
		cid,
		toIdList)

	return nil
}

func GetCorrectSetName(mid string) (zsetName string) {
	zsetName = mid + util.CorrectCidSetSuffix
	return zsetName
}

func GetNotifyHashName(mntCreater uint32) (hashName string) {
	hashName = fmt.Sprintf("%v%s", mntCreater, util.NotifyHashTableSuffix)
	return hashName
}

func GetNotifyListName(mntCreater uint32) (listName string) {
	listName = fmt.Sprintf("%v%s", mntCreater, util.NotifyListSuffix)
	return listName
}

func getTimeMicroSecond() (retMicSecond uint64) {
	retMicSecond = uint64(time.Now().UnixNano())
	retMicSecond = retMicSecond / 1000000 // 将纳秒转换成毫秒
	return retMicSecond
}

func SetRedisCache(head *common.HeadV2, reqPayLoad []byte) {
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			_, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				// infoLog.Printf("SetRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)
			} else {
				infoLog.Printf("SetRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)
			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")
		}

	} else {
		// add static
		attr := "gomntdbd/update_redis_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
	}

	redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	if err == nil {
		redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			_, err := redisSlavePacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				// infoLog.Printf("SetRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)
			} else {
				infoLog.Printf("SetRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)
			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")
		}

	} else {
		// add static
		attr := "gomntdbd/update_redis_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)
	}
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_moment_cache.MntCacheRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	// infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init gMapShortLangToNum
	gMapShortLangToNum = make(map[string]uint32)
	initShortLangToNum()
	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%v port=%v", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init slave cache
	slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	infoLog.Printf("cache slave ip=%v port=%v", slaveIp, slavePort)
	writeAgentSlaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// init ssdboperator
	ssdbIp := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	ssdbMinPoolSize := cfg.Section("SSDB").Key("ssdb_min_size").MustInt(5)
	ssdbMaxPoolSize := cfg.Section("SSDB").Key("ssdb_max_size").MustInt(50)
	infoLog.Printf("ssdb ip=%v port=%v minSize=%v maxSize=%v", ssdbIp, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	redisApi = common.NewRedisApi(ssdbIp + ":" + strconv.Itoa(ssdbPort))
	if err != nil {
		infoLog.Printf("common.NewRedisApi failed err=%s", err)
		return
	}
	ssdbOperator = util.NewSsdbOperator(redisApi, infoLog)

	// init relation api
	relationIp := cfg.Section("RELATION").Key("ip").MustString("127.0.0.1")
	relationPort := cfg.Section("RELATION").Key("port").MustString("6379")
	infoLog.Printf("relation ip=%s port=%s", relationIp, relationPort)
	relationApi = common.NewRelationApi(relationIp, relationPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, 1000)

	// init usercache api
	userCacheIp := cfg.Section("USERCACHE").Key("ip").MustString("127.0.0.1")
	userCachePort := cfg.Section("USERCACHE").Key("port").MustString("6379")
	infoLog.Printf("user cache ip=%s port=%s", userCacheIp, userCachePort)
	userCacheApi = common.NewUserCacheApi(userCacheIp, userCachePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// nsq producer config
	nsqUrl := cfg.Section("NSQ").Key("url").MustString("127.0.0.1:4150")
	nsqConfig := nsq.NewConfig()
	globalProducer, err = nsq.NewProducer(nsqUrl, nsqConfig)
	if err != nil {
		log.Fatalf("main nsq.NewProcucer failed nsqUrl=%s", nsqUrl)
	}
	postMntTopic = cfg.Section("POSTMNT").Key("topic").MustString("post_mnt")
	userActionTopic = cfg.Section("USERACTION").Key("topic").MustString("usr_act")

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	// 学习点数的redis
	redisIp := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis ip=%v port=%v", redisIp, redisPort)
	redisApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
