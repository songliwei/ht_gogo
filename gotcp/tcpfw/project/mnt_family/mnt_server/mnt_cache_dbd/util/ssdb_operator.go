// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_cache"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/golang/protobuf/proto"
)

type SsdbOperator struct {
	redisApi *common.RedisApi
	infoLog  *log.Logger
}

func NewSsdbOperator(targetRedisApi *common.RedisApi, logger *log.Logger) *SsdbOperator {
	return &SsdbOperator{
		redisApi: targetRedisApi,
		infoLog:  logger,
	}
}

//获取Mnt 指给粉丝看列表
func (this *SsdbOperator) GetZsetList(setName string, keyStart, keyStop int64) (keys []string, scores []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	kvs, err := this.redisApi.Zrange(setName, keyStart, keyStop)
	if err != nil {
		this.infoLog.Printf("GetZsetList redisApi.Zrange return err=%s setName=%s keyStart=%v keyStop=%v",
			err,
			setName,
			keyStart,
			keyStop)
	}
	for i, v := range kvs {
		if i%2 == 0 {
			keys = append(keys, v)
		} else {
			scores = append(scores, v)
		}
	}
	return keys, scores, err
}

func (this *SsdbOperator) MultiHset(setName string, keyValue []string) (err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return err
	}

	err = this.redisApi.Hmset(setName, keyValue)
	if err != nil {
		this.infoLog.Printf("MultiHset redisApi.Hmset return err=%s setName=%s keyValue=%v",
			err,
			setName,
			keyValue)
	}
	return err
}

func (this *SsdbOperator) Hset(setName string, key, value string) (err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return err
	}

	err = this.redisApi.Hset(setName, key, value)
	if err != nil {
		this.infoLog.Printf("Hset redisApi.Hset return err=%s setName=%s key=%v value=%v",
			err,
			setName,
			key,
			value)
	}
	return err
}

func (this *SsdbOperator) Hget(setName string, key string) (value string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return value, err
	}

	value, err = this.redisApi.Hget(setName, key)
	if err != nil {
		this.infoLog.Printf("Hget redisApi.Hget return err=%s setName=%s key=%v",
			err,
			setName,
			key)
	}
	return value, err
}

func (this *SsdbOperator) MultiHgetSliceArray(setName string, keys []string) (outKeys []string, outValues []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	outValues, err = this.redisApi.Hmget(setName, keys)
	if err != nil {
		this.infoLog.Printf("MultiHgetSliceArray hashName=%s keys=%s err=%s", setName, keys, err)
		return nil, nil, err
	}
	return outKeys, outValues, nil
}

// 仅用于取mid的详细内容
// 当元素个数<100 直接取全部
// 当元素个数>100 用hkeys取全部的keys再遍历全部的key分批100大小取
func (this *SsdbOperator) MultiHgetAllSlice(setName string) (outKeys []string, outValues []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	keyLen, err := this.redisApi.Hlen(setName)
	if err != nil {
		this.infoLog.Printf("MultiHgetAllSlice redisApi.Hlen hashName=%s err=%s", setName, err)
		return nil, nil, err
	}
	if keyLen < HGetAllCount {
		kvs, err := this.redisApi.Hgetall(setName)
		if err != nil {
			this.infoLog.Printf("MultiHgetAllSlice redisApi.Hgetall setName=%s err=%s", setName, err)
			return nil, nil, err
		} else {
			for k, v := range kvs {
				outKeys = append(outKeys, k)
				outValues = append(outValues, v)
			}
			return outKeys, outValues, nil
		}
	} else {
		keys, err := this.redisApi.Hkeys(setName)
		if err != nil {
			this.infoLog.Printf("MultiHgetAllSlice hashName=%s err=%s", setName, err)
			return nil, nil, err
		}
		keyCount := len(keys)
		if keyCount < HGetAllCount {
			kvs, err := this.redisApi.Hgetall(setName)
			if err != nil {
				this.infoLog.Printf("MultiHgetAllSlice redisApi.Hgetall setName=%s err=%s", setName, err)
				return nil, nil, err
			} else {
				for k, v := range kvs {
					outKeys = append(outKeys, k)
					outValues = append(outValues, v)
				}
				return outKeys, outValues, nil
			}
		}

		// keys 的长度确实大于100 分批获取
		beginIndex := 0
		endIndex := HGetAllCount
		for {
			if endIndex >= keyCount {
				endIndex = keyCount
			}
			itemKeys := keys[beginIndex:endIndex]
			itemValues, err := this.redisApi.Hmget(setName, itemKeys)
			if err != nil {
				this.infoLog.Printf("MultiHgetAllSlice hashName=%s beginIndex=%v endIndex=%v err=%s itemKeys=%v ",
					setName,
					beginIndex,
					endIndex,
					err,
					itemKeys)
				return nil, nil, err
			}
			outKeys = append(outKeys, itemKeys...)
			outValues = append(outValues, itemValues...)
			// beginIndex 往前移动HGetAllCount
			beginIndex += HGetAllCount
			endIndex += HGetAllCount
			if beginIndex >= keyCount {
				this.infoLog.Printf("MultiHgetAllSlice hashName=%s keyCount=%v beginIndex=%v complete",
					setName,
					keyCount,
					beginIndex)
				break
			}
		}
	}
	return outKeys, outValues, nil
}

func (this *SsdbOperator) Hincr(setName, key string, num int) (val int64, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return val, err
	}
	val, err = this.redisApi.Hincrby(setName, key, num)
	if err != nil {
		this.infoLog.Printf("Hincr hashmap=%s key=%s num=%v", setName, key, num)
	}
	return val, err
}

// 仅仅用于遍历uid#LIST/uid#FRIENDS/LANG#num-num/LANG#num/LLANG#num
// 这几个桶 因为这些桶的序号都是从100000000001 100亿零1开始递增的
// 使用redis driver 取到ssdb driver 时 使用Hmget 取代 Hscan
// 不能用于Hscan 其它的桶
func (this *SsdbOperator) HscanArray(setName string, keyStart, keyEnd, limit int64, reverse bool) (outKeys []string, outValues []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	// Step1 检查输入、输出参数(keyStart, keyEnd]
	// 所有的序号从100000000001 开始
	if keyStart == 0 {
		keyStart = BaseMntIndex
	}
	if keyEnd == 0 {
		keyEnd, err = this.GetMaxSeqId(setName)
		if err != nil {
			this.infoLog.Printf("hashName=%s GetMaxSeqId failed err=%s", setName, err)
			return nil, nil, err
		}
	}

	// 如果keyStart == keyEnd 直接返回空的结果
	if keyStart == keyEnd {
		this.infoLog.Printf("HscanArray setName=%s keyStart=%v keyEnd=%v equal return",
			setName,
			keyStart,
			keyEnd)
		return nil, nil, nil
	}

	if reverse == true { // 逆序遍历
		for i := keyEnd; i > keyStart; i -= 1 {
			outKeys = append(outKeys, fmt.Sprintf("%v", i))
			if len(outKeys) >= int(limit) {
				this.infoLog.Printf("HscanArray hashName=%s limit=%v current=%v break",
					setName,
					limit,
					i)
				break
			}
		}
	} else { // 顺序遍历
		keyEnd += 1 //需要能够取到keyEnd
		for i := keyStart + 1; i < keyEnd; i += 1 {
			outKeys = append(outKeys, fmt.Sprintf("%v", i))
			if len(outKeys) >= int(limit) {
				this.infoLog.Printf("HscanArray hashName=%s limit=%v current=%v break",
					setName,
					limit,
					i)
				break
			}
		}
	}
	// this.infoLog.Printf("HscanArray hashName=%s keyStart=%v end=%v limit=%v reverse=%v outKeys=%v",
	// 	setName,
	// 	keyStart,
	// 	keyEnd,
	// 	limit,
	// 	reverse,
	// 	outKeys)
	outValues, err = this.redisApi.Hmget(setName, outKeys)
	if err != nil {
		this.infoLog.Printf("HscanArray hashName=%s keyStart=%v end=%v limit=%v reverse=%v err=%s",
			setName,
			keyStart,
			keyEnd,
			limit,
			reverse,
			err)
		return nil, nil, err
	}

	return outKeys, outValues, nil
}

func (this *SsdbOperator) Hsize(hashName string) (value int64, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return value, err
	}
	value, err = this.redisApi.Hlen(hashName)
	if err != nil {
		this.infoLog.Printf("SsdbOperator.Hsize hashName=%s err=%s", hashName, err)
	}
	return value, err
}

func (this *SsdbOperator) GetMaxSeqId(hashName string) (value int64, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return value, err
	}
	exist, err := this.redisApi.Hexists(hashName, BucketMaxSeqKey)
	if err != nil {
		this.infoLog.Printf("GetMaxSeqId hashName=%s redisApi.Hexists err=%s",
			hashName,
			err)
		return value, err
	}
	if exist == 0 { // ssdb 中不存在最大的seqId这个key 先Hsize 再Hset回去
		val, err := this.redisApi.Hlen(hashName)
		if err != nil {
			this.infoLog.Printf("GetMaxSeqId hashName=%s hsize failed err=%v",
				hashName,
				err)
			return value, err
		}
		value = int64(BaseMntIndex) + val
		indexStr := fmt.Sprintf("%v", value)
		err = this.redisApi.Hset(hashName, BucketMaxSeqKey, indexStr)
		if err != nil {
			this.infoLog.Printf("GetMaxSeqId hashName=%s Hset failed err=%v",
				hashName,
				err)
			return value, err
		}
	} else { // ssdb 中存在最大的seqId 直接获取
		value, err = this.redisApi.HgetInt64(hashName, BucketMaxSeqKey)
		if err != nil {
			this.infoLog.Printf("GetMaxSeqId hashName=%s HgetInt64 failed err=%v",
				hashName,
				err)
			return value, err
		}
	}
	return value, err
}

func (this *SsdbOperator) StatUserMntCount(uid uint32, inc int) (err error) {
	if uid == 0 {
		err := ErrDbParam
		return err
	}

	hashName := fmt.Sprintf("%v_user_info", uid)
	val, err := this.redisApi.Hincrby(hashName, MntCountForSelfKey, inc)
	if err != nil {
		// add static
		attr := "gomntdbd/hinc_hash_map_failed"
		libcomm.AttrAdd(attr, 1)
	}
	this.infoLog.Printf("StatUserMntCount key=%s val=%v", MntCountForSelfKey, val)
	// val, err = this.redisApi.Hincrby(hashName, MntCountForOtherKey, inc)
	// if err != nil {
	// 	// add static
	// 	attr := "gomntdbd/hinc_hash_map_failed"
	// 	libcomm.AttrAdd(attr, 1)
	// }
	// this.infoLog.Printf("StatUserMntCount key=%s val=%v", MntCountForOtherKey, val)
	return nil
}

func (this *SsdbOperator) StatUserMntCountForOther(uid uint32, inc int) (err error) {
	if uid == 0 {
		err := ErrDbParam
		return err
	}

	hashName := fmt.Sprintf("%v_user_info", uid)
	val, err := this.redisApi.Hincrby(hashName, MntCountForOtherKey, inc)
	if err != nil {
		// add static
		attr := "gomntdbd/hinc_hash_map_failed"
		libcomm.AttrAdd(attr, 1)
	}
	this.infoLog.Printf("StatUserMntCountForOther key=%s val=%v", MntCountForOtherKey, val)
	return nil
}

func (this *SsdbOperator) InsertMidIntoHashMap(hashName string, mid uint64) (index int64, err error) {
	if len(hashName) == 0 || mid < 10000 {
		this.infoLog.Printf("InsertMidIntoHashMap invalid param hashName=%s mid=%v", hashName, mid)
		err = ErrDbParam
		return index, ErrDbParam
	}
	// this.infoLog.Printf("InsertMidIntoHashMap hashName=%s mid=%v", hashName, mid)
	exist, err := this.redisApi.Hexists(hashName, BucketMaxSeqKey)
	if err != nil {
		this.infoLog.Printf("InsertMidIntoHashMap hashName=%s mid=%v redisApi.Hexists err=%s",
			hashName,
			mid,
			err)
		return index, err
	}
	if exist == 0 { // ssdb 中不存在最大的seqId这个key 先Hsize 再Hset回去
		val, err := this.redisApi.Hlen(hashName)
		if err != nil {
			this.infoLog.Printf("InsertMidIntoHashMap hashName=%s hsize failed err=%v",
				hashName,
				err)
			return index, err
		}
		midIndex := uint64(int64(BaseMntIndex) + val)
		indexStr := fmt.Sprintf("%v", midIndex)
		err = this.redisApi.Hset(hashName, BucketMaxSeqKey, indexStr)
		if err != nil {
			this.infoLog.Printf("InsertMidIntoHashMap hashName=%s Hset failed err=%v",
				hashName,
				err)
			return index, err
		}
	}
	// 否则已经存在maxSeqId 这个项 直接将它增加1再将新的mid设置回去
	index, err = this.redisApi.Hincrby(hashName, BucketMaxSeqKey, 1)
	if err != nil {
		this.infoLog.Printf("InsertMidIntoHashMap hashName=%s redisApi.Hincrby failed err=%v",
			hashName,
			err)
		return index, err
	}
	indexStr := fmt.Sprintf("%v", index)
	midStr := fmt.Sprintf("%v", mid)
	err = this.redisApi.Hset(hashName, indexStr, midStr)
	if err != nil {
		this.infoLog.Printf("InsertMidIntoHashMap hashName=%s mid=%v Hset failed err=%v",
			hashName,
			mid,
			err)
		return index, err
	}
	return index, nil
}

func (this *SsdbOperator) InsertStrMidIntoHashMap(hashName string, mid string) (index int64, err error) {
	if len(hashName) == 0 || len(mid) < 6 {
		this.infoLog.Printf("InsertStrMidIntoHashMap invalid param hashName=%s mid=%s", hashName, mid)
		err = ErrDbParam
		return index, ErrDbParam
	}
	// this.infoLog.Printf("InsertStrMidIntoHashMap hashName=%s mid=%s", hashName, mid)
	exist, err := this.redisApi.Hexists(hashName, BucketMaxSeqKey)
	if err != nil {
		this.infoLog.Printf("InsertStrMidIntoHashMap hashName=%s mid=%v redisApi.Hexists err=%s",
			hashName,
			mid,
			err)
		return index, err
	}
	if exist == 0 { // ssdb 中不存在最大的seqId这个key 先Hsize 再Hset回去
		val, err := this.redisApi.Hlen(hashName)
		if err != nil {
			this.infoLog.Printf("InsertStrMidIntoHashMap hashName=%s hsize failed err=%v",
				hashName,
				err)
			return index, err
		}
		midIndex := uint64(int64(BaseMntIndex) + val)
		indexStr := fmt.Sprintf("%v", midIndex)
		err = this.redisApi.Hset(hashName, BucketMaxSeqKey, indexStr)
		if err != nil {
			this.infoLog.Printf("InsertStrMidIntoHashMap hashName=%s Hset failed err=%v",
				hashName,
				err)
			return index, err
		}
	}
	// 否则已经存在maxSeqId 这个项 直接将它增加1再将新的mid设置回去
	index, err = this.redisApi.Hincrby(hashName, BucketMaxSeqKey, 1)
	if err != nil {
		this.infoLog.Printf("InsertStrMidIntoHashMap hashName=%s redisApi.Hincrby failed err=%v",
			hashName,
			err)
		return index, err
	}
	indexStr := fmt.Sprintf("%v", index)
	err = this.redisApi.Hset(hashName, indexStr, mid)
	if err != nil {
		this.infoLog.Printf("InsertStrMidIntoHashMap hashName=%s mid=%s Hset failed err=%v",
			hashName,
			mid,
			err)
		return index, err
	}
	return index, nil
}

func (this *SsdbOperator) ZrangeList(setName string, offset, limit int64, reverse uint32) (keyScore []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return keyScore, err
	}
	//0:zrange 1:zrrange
	start := offset
	stop := offset + limit
	if reverse == 0 {
		keyScore, err = this.redisApi.Zrange(setName, start, stop)
	} else {
		keyScore, err = this.redisApi.Zrevrange(setName, start, stop)
	}
	if err != nil {
		this.infoLog.Printf("ZrangeList setName=%s offset=%v limint=%v reverse=%v err=%s", setName, offset, limit, reverse, err)
	}
	return keyScore, err
}

func (this *SsdbOperator) ZrangeListWithOutScore(setName string, offset, limit int64, reverse uint32) (keys []string, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return keys, err
	}
	//0:zrange 1:zrrange
	start := offset
	stop := offset + limit
	if reverse == 0 {
		keys, err = this.redisApi.ZrangeWithOutScore(setName, start, stop)
	} else {
		keys, err = this.redisApi.ZrevrangeWithOutScore(setName, start, stop)
	}
	if err != nil {
		this.infoLog.Printf("ZrangeList setName=%s offset=%v limint=%v reverse=%v err=%s", setName, offset, limit, reverse, err)
	}
	return keys, err
}

func (this *SsdbOperator) GetUidByMid(mid string) (uid uint32, err error) {
	if this.redisApi == nil {
		err = ErrNilDbObject
		return uid, err
	}
	if len(mid) == 0 {
		err = ErrDbParam
		return uid, err
	}
	value, err := this.redisApi.Hget(mid, MntContentKey)
	if err != nil {
		return uid, err
	}
	reqBody := new(ht_moment_store.StoreMomentBody)
	err = proto.Unmarshal([]byte(value), reqBody)
	if err != nil {
		this.infoLog.Printf("GetUidByMid proto Unmarshal failed mid=%s", mid)
		return uid, err
	}
	uid = reqBody.GetUserid()
	return uid, nil
}

func (this *SsdbOperator) ModifyMntStat(op uint32, mid string) (mntCreater uint32, prevStat uint32, err error) {
	//获取mid对应uid
	mntCreater, err = this.GetUidByMid(mid)
	if err != nil {
		this.infoLog.Printf("this.GetUidByMid mid=%s err=%s", mid, err)
		return mntCreater, prevStat, err
	}
	midKey := []string{
		LikedCountKey,
		MntDeletedKey,
	}
	_, outValues, err := this.MultiHgetSliceArray(mid, midKey)
	if err != nil {
		return mntCreater, prevStat, err
	}
	if len(outValues) != 2 {
		this.infoLog.Printf("ModifyMntStat outValues Len=%v not enouth", len(outValues))
		err = ErrLegthErr
		return mntCreater, prevStat, err
	}
	likeCount, err := strconv.ParseUint(outValues[0], 10, 32)
	if err != nil {
		return mntCreater, prevStat, err
	}
	tempPrevStat, err := strconv.ParseUint(outValues[1], 10, 32)
	if err != nil {
		return mntCreater, prevStat, err
	}
	prevStat = uint32(tempPrevStat)
	if prevStat == uint32(ht_moment_cache.MntStatOpType_OP_DELETE_BY_USER) ||
		prevStat == uint32(ht_moment_cache.MntStatOpType_OP_DELETE) {
		this.infoLog.Printf("ModifyMntStat prev deleted=%v can not change state", prevStat)
		return mntCreater, prevStat, nil
	}

	userInfo := fmt.Sprintf("%v_user_info", mntCreater)
	switch ht_moment_cache.MntStatOpType(prevStat) {
	case ht_moment_cache.MntStatOpType_OP_RESTORE:
		{
			if op == uint32(ht_moment_cache.MntStatOpType_OP_HIDE) { // 0-->2
				_, err = this.Hincr(userInfo, MntLikeCountForOther, int(0-likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForOtherKey, -1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else if op == uint32(ht_moment_cache.MntStatOpType_OP_DELETE) { // 0-->3
				_, err = this.Hincr(userInfo, MntLikeCountKey, int(0-likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntLikeCountForOther, int(0-likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForSelfKey, -1)
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForOtherKey, -1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else if op == uint32(ht_moment_cache.MntStatOpType_OP_FOR_FOLLOWER_ONLY) { // 0-->10
				this.infoLog.Printf("ModifyMntStat op=%v do not chang count", op)
			} else {
				this.infoLog.Printf("ModifyMntStat UnKnow op=%d", op)
			}
		}
	case ht_moment_cache.MntStatOpType_OP_HIDE:
		{
			if op == uint32(ht_moment_cache.MntStatOpType_OP_RESTORE) { // 2-->0
				_, err = this.Hincr(userInfo, MntLikeCountForOther, int(likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForOtherKey, 1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else if op == uint32(ht_moment_cache.MntStatOpType_OP_DELETE) { // 2-->3
				_, err = this.Hincr(userInfo, MntLikeCountKey, int(0-likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForSelfKey, -1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else if op == uint32(ht_moment_cache.MntStatOpType_OP_FOR_FOLLOWER_ONLY) { // 2-->10
				_, err = this.Hincr(userInfo, MntLikeCountForOther, int(likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForOtherKey, 1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else {
				this.infoLog.Printf("ModifyMntStat UnKnow op=%v", op)
			}
		}
	case ht_moment_cache.MntStatOpType_OP_DELETE:
		{
			if op == uint32(ht_moment_cache.MntStatOpType_OP_RESTORE) { // 3-->0
				_, err = this.Hincr(userInfo, MntLikeCountKey, int(likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntLikeCountForOther, int(likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForSelfKey, 1)
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForOtherKey, 1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else if op == uint32(ht_moment_cache.MntStatOpType_OP_HIDE) { // 3-->2
				_, err = this.Hincr(userInfo, MntLikeCountKey, int(likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForSelfKey, 1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else if op == uint32(ht_moment_cache.MntStatOpType_OP_FOR_FOLLOWER_ONLY) { // 3-->10
				_, err = this.Hincr(userInfo, MntLikeCountKey, int(likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntLikeCountForOther, int(likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForSelfKey, 1)
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForOtherKey, 1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else {
				this.infoLog.Printf("ModifyMntStat UnKnow op=%v", op)
			}
		}
	case ht_moment_cache.MntStatOpType_OP_FOR_FOLLOWER_ONLY:
		{
			if op == uint32(ht_moment_cache.MntStatOpType_OP_RESTORE) { // 10-->0
				this.infoLog.Printf("ModifyMntStat prev stat=%v cur stat=%v", prevStat, op)
			} else if op == uint32(ht_moment_cache.MntStatOpType_OP_HIDE) { // 10-->2
				_, err = this.Hincr(userInfo, MntLikeCountForOther, int(0-likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForOtherKey, -1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else if op == uint32(ht_moment_cache.MntStatOpType_OP_DELETE) { // 10-->3
				_, err = this.Hincr(userInfo, MntLikeCountKey, int(0-likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntLikeCountForOther, int(0-likeCount))
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForSelfKey, -1)
				if err != nil {
					return mntCreater, prevStat, err
				}
				_, err = this.Hincr(userInfo, MntCountForOtherKey, -1)
				if err != nil {
					return mntCreater, prevStat, err
				}
			} else {
				this.infoLog.Printf("ModifyMntStat UnKnow op=%v", op)
			}
		}
	default:
		this.infoLog.Printf("ModifyMntStat Unknow mntCreater=%v Prev Stat=%v", mntCreater, prevStat)
	}
	return mntCreater, prevStat, nil
}

func (this *SsdbOperator) UpdateHideOtherMntUidList(fromId, opType, opUid uint32) (addTime uint64, err error) {
	if this.redisApi == nil || fromId == 0 || opUid == 0 {
		err = ErrNilDbObject
		return addTime, err
	}
	zsetName := fmt.Sprintf("%v%s", fromId, HideOtherMntKey)
	zsetKey := fmt.Sprintf("%v", opUid)
	tempAddTime, err := this.UpdateZsetList(zsetName, zsetKey, opType)
	addTime = uint64(tempAddTime)
	return addTime, err
}

func (this *SsdbOperator) UpdateNotShareMntUidList(fromId, opType, opUid uint32) (addTime uint64, err error) {
	if this.redisApi == nil || fromId == 0 || opUid == 0 {
		err = ErrNilDbObject
		return addTime, err
	}
	zsetName := fmt.Sprintf("%v%s", fromId, NotShareMyMntKey)
	zsetKey := fmt.Sprintf("%v", opUid)
	tempAddTime, err := this.UpdateZsetList(zsetName, zsetKey, opType)
	addTime = uint64(tempAddTime)
	return addTime, err
}

func (this *SsdbOperator) UpdateMntForFollowerUidList(fromId, opType, opUid uint32) (addTime uint64, err error) {
	if this.redisApi == nil || fromId == 0 || opUid == 0 {
		err = ErrNilDbObject
		return addTime, err
	}
	zsetName := MntForFollowerList
	zsetKey := fmt.Sprintf("%v", opUid)
	tempAddTime, err := this.UpdateZsetList(zsetName, zsetKey, opType)
	addTime = uint64(tempAddTime)
	return addTime, err
}

func (this *SsdbOperator) UpdateMntForSelfUidList(fromId, opType, opUid uint32) (addTime uint64, err error) {
	if this.redisApi == nil || fromId == 0 || opUid == 0 {
		err = ErrNilDbObject
		return addTime, err
	}
	zsetName := MntForSelfList
	zsetKey := fmt.Sprintf("%v", opUid)
	tempAddTime, err := this.UpdateZsetList(zsetName, zsetKey, opType)
	addTime = uint64(tempAddTime)
	return addTime, err
}

func (this *SsdbOperator) UpdateZsetList(zsetName, zsetKey string, opType uint32) (addTime uint32, err error) {
	if this.redisApi == nil || len(zsetName) == 0 || len(zsetKey) == 0 {
		err = ErrNilDbObject
		return addTime, err
	}
	switch opType {
	case uint32(ht_moment_cache.OpType_OP_ADD):
		// 添加元素
		addTime = 0xffff - uint32(time.Now().Unix())
		_, err = this.redisApi.Zadd(zsetName, int64(addTime), zsetKey)
		if err != nil {
			this.infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v err=%s", zsetName, zsetKey, opType, err)
			return addTime, err
		}
		// 更新当前版本
		_, err = this.redisApi.Zadd(zsetName, int64(addTime), VersionField)
		if err != nil {
			this.infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v err=%s", zsetName, zsetKey, opType, err)
			return addTime, err
		}
	case uint32(ht_moment_cache.OpType_OP_DEL):
		// 删除元素
		addTime = 0xffff - uint32(time.Now().Unix())
		_, err = this.redisApi.Zrem(zsetName, zsetKey)
		if err != nil {
			this.infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v err=%s", zsetName, zsetKey, opType, err)
			return addTime, err
		}
		// 更新版本
		_, err = this.redisApi.Zadd(zsetName, int64(addTime), VersionField)
		if err != nil {
			this.infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v err=%s", zsetName, zsetKey, opType, err)
			return addTime, err
		}
	default:
		this.infoLog.Printf("UpdateZsetList zsetName=%s zsetKey=%s opType=%v unhandle", zsetName, zsetKey, opType)
	}
	return addTime, nil
}

func (this *SsdbOperator) Zget(zsetName, zsetKey string) (score uint64, err error) {
	if this.redisApi == nil || len(zsetName) == 0 || len(zsetKey) == 0 {
		err = ErrNilDbObject
		return score, err
	}
	score, err = this.redisApi.Zscore(zsetName, zsetKey)
	return score, err
}

func (this *SsdbOperator) Zset(zsetName, zsetKey string, score int64) (err error) {
	if this.redisApi == nil || len(zsetName) == 0 || len(zsetKey) == 0 {
		err = ErrNilDbObject
		return err
	}
	_, err = this.redisApi.Zadd(zsetName, score, zsetKey)
	return err
}

func (this *SsdbOperator) Zdel(zsetName, zsetKey string) (removeCnt int64, err error) {
	if this.redisApi == nil || len(zsetName) == 0 || len(zsetKey) == 0 {
		err = ErrNilDbObject
		return removeCnt, err
	}
	removeCnt, err = this.redisApi.Zrem(zsetName, zsetKey)
	return removeCnt, err
}

func (this *SsdbOperator) Hexists(hashName, key string) (ret bool, err error) {
	if this.redisApi == nil || len(hashName) == 0 || len(key) == 0 {
		err = ErrNilDbObject
		return ret, err
	}
	result, err := this.redisApi.Hexists(hashName, key)
	if err != nil {
		return ret, err
	}
	if result > 0 {
		ret = true
	} else {
		ret = false
	}
	return ret, nil
}

func (this *SsdbOperator) QpushFront(listName, value string) (ret int64, err error) {
	if this.redisApi == nil || len(listName) == 0 || len(value) == 0 {
		err = ErrNilDbObject
		return ret, err
	}
	ret, err = this.redisApi.Lpush(listName, value)
	return ret, err
}

func (this *SsdbOperator) UpdateCmntStat(mid, cid string, modStat ht_moment_cache.COMMENT_STAT) (prevStat uint32, strCmnt string, err error) {
	if this.redisApi == nil || len(mid) == 0 || len(cid) == 0 {
		err = ErrNilDbObject
		return prevStat, strCmnt, err
	}
	prevCmnt, err := this.redisApi.Hget(mid, cid)
	if err != nil {
		this.infoLog.Printf("UpdateCmntStat mid=%s cid=%v modStat=%v Hget err=%s",
			mid,
			cid,
			modStat,
			err)
		return prevStat, strCmnt, err
	}

	cmntReqBody := new(ht_moment_store.StoreCommentBody)
	err = proto.Unmarshal([]byte(prevCmnt), cmntReqBody)
	if err != nil {
		this.infoLog.Printf("UpdateCmntStat proto Unmarshal failed mid=%s cid=%v modStat=%v err=%s",
			mid,
			cid,
			modStat,
			err)
		return prevStat, strCmnt, err
	}
	prevStat = cmntReqBody.GetDeleted()
	cmntReqBody.Deleted = proto.Uint32(uint32(modStat))
	cmntSlic, err := proto.Marshal(cmntReqBody)
	if err != nil {
		this.infoLog.Printf("UpdateCmntStat roto.Marshal failed mid=%s cid=%s modStat=%v err=%s",
			mid,
			cid,
			modStat,
			err)
		return prevStat, strCmnt, err
	}
	strCmnt = string(cmntSlic)
	err = this.redisApi.Hset(mid, cid, strCmnt)
	if err != nil {
		this.infoLog.Printf("UpdateCmntStat redisApi.Set mid=%s cid=%s modStat=%v err=%s",
			mid,
			cid,
			modStat,
			err)
	}
	return prevStat, strCmnt, err
}
