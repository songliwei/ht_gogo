// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import "errors"

// Error type
var (
	ErrNotExistInSsdb = errors.New("not exist in redis")
	ErrNilDbObject    = errors.New("not set object current is nil")
	ErrDbParam        = errors.New("err param error")
	ErrLegthErr       = errors.New("err length is not enouth")
)

const (
	MntForFollowerList  = "mnt_for_follower_list"
	MntForSelfList      = "mnt_for_self_list"
	MntCountForSelfKey  = "moment_count"
	MntCountForOtherKey = "moment_count_for_others"
	LearnLangPrefix     = "LLNANG#"
	LangPrefix          = "LANG#"
	FriendSuffix        = "#FRIEND"
	ListSuffix          = "#LIST"

	MntLikeCountKey      = "like_count"
	MntLikeCountForOther = "like_count_for_others"
	ShowFieldKey         = "show"

	MntContentKey     = "0"
	BucketMaxSeqKey   = "0"
	MntDeletedKey     = "deleted"
	MntOpReasonKey    = "op_reason"
	LikedCountKey     = "liked_count"
	LikedTsKey        = "liked_ts"
	CommentCountKey   = "comment_count"
	CommentTsKey      = "comment_ts"
	TotalCmntCountKey = "total_comment_count"

	HideOtherMntKey       = "#hide_user_set"
	NotShareMyMntKey      = "#not_share_set"
	VersionField          = "1"
	VersionFieldInt       = 1
	ShowField             = "show"
	MomentCreater         = "moment_creater"
	CorrectCidSetSuffix   = "#correction_cid_set" //每个帖子的改错cid集合
	NotifyHashTableSuffix = "#notify_hashtable"
	HashNotifyMaxIdKey    = "0"
	NotifyListSuffix      = "_notify_list"
	CmntUidSetSuffix      = "_cmnt_uid_set"
	LikeUidSetSuffix      = "_like_uid_set" //每个帖子的点赞uid集合
	UserInfoSuffix        = "_user_info"
)

const (
	ProcTimeThresold   = 300000
	BaseMntIndex       = 100000000000 // 100亿
	BaseCmntIndex      = 1000000      //hashmap中comment id从1000000开始
	BaseNotifyIndex    = 1000000000
	ChineseSimple      = 2
	ChineseTraditional = 3
	ChineseGuangDoHua  = 4
	Japanse            = 5
	Korean             = 6
	HashMapExpireTime  = 1 * 24 * 3600
	ZSetExpireTime     = 3 * 3600
	LimitWords         = 30
	LimitByteLen       = 90
	HScanOperaterCount = 1000
	HGetAllCount       = 100
	ZsetKeysSize       = 200000
	SpecialUid         = 0
)
