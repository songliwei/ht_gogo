// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/golang/protobuf/proto"
)

// Error type
var (
	ErrNotExistInReids = errors.New("not exist in redis")
	ErrNilDbObject     = errors.New("not set object current is nil")
)

const (
	HashNameNotifyHashTable = "#notify_hashtable"
	ListNameNotifyList      = "_notify_list" //通知列表
	HashKeyNotifyMaxId      = "0"            //当前notify_list的分配出去的最大ID
	HashKeyNotifyIdBase     = 1000000000
)

type SsdbOperator struct {
	redisApi *common.RedisApi
}

func NewSsdbOperator(targetRedisApi *common.RedisApi) *SsdbOperator {
	return &SsdbOperator{
		redisApi: targetRedisApi,
	}
}

// 添加通知事件
func (this *SsdbOperator) AddNotifyEvent(postUid, notifyUid uint32,
	strMid string,
	notifyType ht_moment_store.STORE_NOTIFY_TYPE) (err error) {
	if this.redisApi == nil || postUid == 0 || notifyType == 0 {
		err = ErrNilDbObject
		return err
	}
	log.Printf("AddNotifyEvent postUid=%v notifyUid=%v strMid=%s notifyType=%v",
		postUid,
		notifyUid,
		strMid,
		notifyType)

	notifyInfo := &ht_moment_store.StoreNotifyInfo{
		Userid:   proto.Uint32(postUid),
		Type:     notifyType.Enum(),
		Mid:      []byte(strMid),
		PostTime: proto.Uint64(uint64(time.Now().UnixNano() / 1000000)),
	}

	notifyHashName := fmt.Sprintf("%v%s", notifyUid, HashNameNotifyHashTable)
	curMaxMsgId, err := this.redisApi.Hincrby(notifyHashName, HashKeyNotifyMaxId, 1)
	if err != nil {
		log.Printf("AddNotifyEvent postUid=%v notifyUid=%v strMid=%s notifyType=%v Hincr failed err=%s",
			postUid,
			notifyUid,
			strMid,
			notifyType,
			err)
		return err
	}
	// set current mas msg id
	notifyInfo.MsgId = proto.Uint32(uint32(curMaxMsgId))
	notifySlic, err := proto.Marshal(notifyInfo)
	if err != nil {
		log.Printf("AddNotifyEvent postUid=%v notifyUid=%v strMid=%s notifyType=%v marshal failed err=%s",
			postUid,
			notifyUid,
			strMid,
			notifyType,
			err)
		return err
	}
	// add notify msg
	curMsgKey := fmt.Sprintf("%v", curMaxMsgId+HashKeyNotifyIdBase)
	err = this.redisApi.Hset(notifyHashName, curMsgKey, string(notifySlic))
	if err != nil {
		log.Printf("AddNotifyEvent  postUid=%v notifyUid=%v strMid=%s notifyType=%v Hset failed err=%s",
			postUid,
			notifyUid,
			strMid,
			notifyType,
			err)
		return err
	}

	// add to notify list
	notifyList := fmt.Sprintf("%v%s", notifyUid, ListNameNotifyList)
	size, err := this.redisApi.Lpush(notifyList, string(notifySlic))
	if err != nil {
		log.Printf("AddNotifyEvent postUid=%v notifyUid=%v strMid=%s notifyType=%v Qpush_front failed err=%s",
			postUid,
			notifyUid,
			strMid,
			notifyType,
			err)
		return err
	} else {
		log.Printf("AddNotifyEvent postUid=%v notifyUid=%v strMid=%s notifyType=%v Qpush_front size=%v",
			postUid,
			notifyUid,
			strMid,
			notifyType,
			size)
	}
	return nil
}

func (this *SsdbOperator) HGet(hashName, key string) (value string, err error) {
	if this.redisApi == nil || len(key) == 0 {
		err = ErrNilDbObject
		return value, err
	}
	value, err = this.redisApi.Hget(hashName, key)
	return value, err
}
