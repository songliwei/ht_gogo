package main

import (
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"sync"
	"syscall"
	"time"
	"unicode/utf8"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment_store"
	"github.com/HT_GOGO/gotcp/tcpfw/project/mnt_family/mnt_consumer/util"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"github.com/nsqio/go-nsq"
	"gopkg.in/ini.v1"
)

var wg *sync.WaitGroup
var (
	relationApi    *common.RelationApi
	mntApi         *common.MntApi
	p2pWorkerApiV2 *common.P2PWorkerApiV2
	ssdbOperator   *util.SsdbOperator
	textLength     int
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`
	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

const (
	POST_UID         = "post_uid"
	POST_TS          = "post_ts"
	MID              = "mid"
	MID_CONTENT_KEY  = "0"
	PUSH_POST_MOMENT = 23
)

var options Options
var parser = flags.NewParser(&options, flags.Default)

func MessageHandle(message *nsq.Message) error {
	// 统计gcm处理总量
	attr := "mnt_consumer/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	// log.Printf("MessageHandle Got a message: %v", message)
	wg.Add(1)
	go func() {
		ProcData(message)
		wg.Done()
	}()

	return nil
}

func UidIsInSlice(uidList []uint32, uid uint32) bool {
	if uid == 0 || len(uidList) == 0 {
		return false
	}
	for _, v := range uidList {
		if uid == v {
			return true
		}
	}

	return false
}

func ProcData(message *nsq.Message) error {
	// 然后进行下一步无处理
	rootObj, err := simplejson.NewJson(message.Body)
	if err != nil {
		log.Printf("ProcData simplejson new packet error", err)
		return err
	}
	// log.Printf("ProcData rootObj=%#v", rootObj)
	postUid := uint32(rootObj.Get(POST_UID).MustUint64(0))
	postTs := uint32(rootObj.Get(POST_TS).MustUint64(0))
	mid := rootObj.Get(MID).MustString("")
	if postUid == 0 || mid == "" {
		log.Printf("ProcData param error postUid=%v postTs=%v mid=%s", postUid, postTs, mid)
		attr := "mnt_consumer/param_error_count"
		libcomm.AttrAdd(attr, 1)
		return nil
	}
	log.Printf("ProcData postUid=%v postTs=%v mid=%s", postUid, postTs, mid)
	// get all follower and filter special following
	specialFollowerList, err := relationApi.GetSpecialFollowerLists(postUid)
	if err != nil {
		log.Printf("ProcData postUid=%v postTs=%s GetSpecialFollowerLists err=%s", postUid, postTs, err)
		return err
	}
	_, _, notShareList, err := mntApi.GetUidList(postUid, ht_moment.OPERATORLIST_OP_NOT_SHARE_LIST)
	if err != nil {
		log.Printf("ProcData postUid=%v postTs=%s mntApi.GetUidList err=%s",
			postUid,
			postTs,
			err)
		return err
	}
	log.Printf("ProcData postUid=%v notShareToList=%v", postUid, notShareList)

	// STEP 2.1 读取moment的内容
	midContent, err := ssdbOperator.HGet(mid, MID_CONTENT_KEY)
	if err != nil {
		log.Printf("ssdbOperator.HGet postUid=%v postTs=%v mid=%s err=%s", postUid, postTs, mid, err)
		attr := "mnt_consumer/hget_mid_content_failed_count"
		libcomm.AttrAdd(attr, 1)
		return err
	}
	storeMomentBody := new(ht_moment_store.StoreMomentBody)
	err = proto.Unmarshal([]byte(midContent), storeMomentBody)
	if err != nil {
		attr := "mnt_consumer/proto_unmarshal_failed"
		libcomm.AttrAdd(attr, 1)
		log.Printf("proto Unmarshal failed hashname=%s contentValueLen=%v err=%s", mid, len(midContent), err)
		return err
	}
	// STEP 2.2 如果内容中包含文本，则必须携带文本；如果内容中既有图片又有语音 者只需语言；如果只有图片就取图片
	notifyParam := simplejson.New()
	// 添加mid
	notifyParam.Set("mid", mid)
	text := storeMomentBody.GetContent()
	voice := storeMomentBody.GetVoice()
	imageSlic := storeMomentBody.GetImages()
	// 长度>0 才设置
	if utf8.RuneCountInString(string(text)) > textLength {
		trimStr := SubString(string(text), 0, textLength) + "..."
		notifyParam.Set("text", trimStr)
	} else {
		notifyParam.Set("text", string(text))
	}

	// 设置语音
	if voice != nil && len(voice.GetUrl()) > 0 {
		voiceObj := simplejson.New()
		voiceObj.Set("url", string(voice.GetUrl()))
		voiceObj.Set("duration", voice.GetDuration())
		notifyParam.Set("voice", voiceObj)
	} else if len(imageSlic) > 0 { // 如果没有语音则判断是否有图片
		image := imageSlic[0]
		notifyParam.Set("image", string(image.GetBigUrl()))
	}
	// STEP 2.3 取出内容后拼成json结构并序列化
	paramSlic, err := notifyParam.MarshalJSON()
	if err != nil {
		log.Printf("simplejson marshal failed postUid=%v postTs=%v mid=%s err=%s", postUid, postTs, mid, err)
		attr := "mnt_consumer/json_marshal_failed_count"
		libcomm.AttrAdd(attr, 1)
		return err
	}

	log.Printf("text=%s parmaSlic=%s", text, paramSlic)

	// send notify to push producer
	for i, v := range specialFollowerList {
		// Step1: 如果发帖人不分享帖文给某人看这不用通知
		if UidIsInSlice(notShareList, v) {
			log.Printf("ProcData postUid=%v index=%v followerUid=%v notShareList=%v",
				postUid,
				i,
				v,
				notShareList)
			continue
		}
		// 首先过滤掉不看某人的帖子
		_, hideList, _, err := mntApi.GetUidList(v, ht_moment.OPERATORLIST_OP_HIDE_LIST)
		if err != nil {
			log.Printf("ProcData postUid=%v index=%v followerUid=%v GetHideList failed err=%s",
				postUid,
				i,
				v,
				err)
		} else {
			log.Printf("ProcData postUid=%v index=%v followerUid=%v hideList=%v",
				postUid,
				i,
				v,
				hideList)
		}
		if UidIsInSlice(hideList, postUid) {
			log.Printf("ProcData postUid=%v index=%v followerUid=%v hideList=%v hide",
				postUid,
				i,
				v,
				hideList)
			continue
		}
		// STEP 1 存储NOtify 到特别关注用户的列表中
		log.Printf("ProcData postUid=%v index=%v followerUid=%v", postUid, i, v)
		err = ssdbOperator.AddNotifyEvent(postUid, v, mid, ht_moment_store.STORE_NOTIFY_TYPE_NOTIFY_POST_MNT)
		if err != nil {
			log.Printf("ssdbOperator.AddNotifyEvent postUid=%v postTs=%v mid=%s err=%s", postUid, postTs, mid, err)
			attr := "mnt_consumer/add_notify_failed_count"
			libcomm.AttrAdd(attr, 1)
			return nil
		}
		// STEP 2 发送通知到p2p_agent进程(在线直接下发通知，不在线推送到push 进程,通知不存离线)
		err = p2pWorkerApiV2.SendNotifyMsg(postUid, v, string(paramSlic), 1, PUSH_POST_MOMENT, 1)
		if err != nil {
			log.Printf("p2pWorkerApiV2.SendNotifyMsg postUid=%v postTs=%v mid=%s err=%s", postUid, postTs, mid, err)
			attr := "mnt_consumer/send_p2p_failed_count"
			libcomm.AttrAdd(attr, 1)
			return err
		}
	}
	return nil
}

func SubString(str string, begin, length int) (substr string) {
	// 将字符串的转换成[]rune
	rs := []rune(str)
	lth := len(rs)

	// 简单的越界判断
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length
	if end > lth {
		end = lth
	}

	// 返回子串
	return string(rs[begin:end])
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	// relation api
	relationIp := cfg.Section("RELATION").Key("ip").MustString("127.0.0.1")
	relationPort := cfg.Section("RELATION").Key("port").MustString("11410")
	log.Printf("relation ip=%s port=%s", relationIp, relationPort)
	relationApi = common.NewRelationApi(relationIp, relationPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, 1000)

	// moment api
	mntIp := cfg.Section("MOMENT").Key("ip").MustString("127.0.0.1")
	mntPort := cfg.Section("MOMENT").Key("port").MustString("11410")
	log.Printf("MOMENT ip=%s port=%s", mntIp, mntPort)
	mntApi = common.NewMntApi(mntIp, mntPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// read ssdb config
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	// ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	// ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	log.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	redisApi := common.NewRedisApi(ssdbHost + ":" + strconv.Itoa(ssdbPort))
	if err != nil {
		log.Printf("common.NewRedisApi failed err=%s", err)
		return
	}

	ssdbOperator = util.NewSsdbOperator(redisApi)

	// push server api
	p2pServerIp := cfg.Section("P2PSERVER").Key("ip").MustString("127.0.0.1")
	p2pServerPort := cfg.Section("P2PSERVER").Key("port").MustString("11410")
	log.Printf("p2p server ip=%s port=%s", p2pServerIp, p2pServerPort)
	p2pWorkerApiV2 = common.NewP2PWorkerApiV2(p2pServerIp, p2pServerPort, 1*time.Second, 1*time.Second, &common.HeadV3Protocol{}, 1000)

	// text length config
	textLength = cfg.Section("TEXTLIMIT").Key("length").MustInt(50)

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	wg = &sync.WaitGroup{}
	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
