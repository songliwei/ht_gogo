package main

import (
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/HT_GOGO/gotcp/tcpfw/project/mnt_family/cmt_server/cmnt_dbd/util"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog             *log.Logger
	ssdbOperator        *util.SsdbOperator
	writeAgentMastreApi *common.SrvToSrvApiV2
	// writeAgentSlaveApi  *common.SrvToSrvApiV2
)

const (
	ProcSlowThreshold = 300000
	HashMapSizeLimit  = 1000
)

const (
	HASH_KEY_USER_LIKE_CNT       = "like_count"              //个人信息中的赞的数量
	HASH_KEY_USER_MNT_CNT        = "moment_count"            //个人信息中的帖子数量
	HASH_KEY_USER_OTHER_LIKE_CNT = "like_count_for_others"   //个人信息中的赞的数量(不包含隐藏的帖子)
	HASH_KEY_USER_OTHER_MNT_CNT  = "moment_count_for_others" //个人信息中的帖子数量(不包含隐藏的帖子)
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	// 统计总的请求量
	attr := "gocmntdbd/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_moment.CMD_TYPE_CMD_QUERY_BEBLOCKED):
		go ProcQueryBeBlock(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INFO):
		go ProcGetUserInfo(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_CLEAR_USER_INFO_CACHE):
		go ProcClearUserInfo(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
	}
	return true
}

func GetUserInfoHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v_user_info", uid)
	return hashName
}

func GetNotShareSetZSetName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#not_share_set", uid)
	return hashName
}

// 1.proc inc chat record
func ProcQueryBeBlock(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcQueryBeBlock no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcQueryBeBlock invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocmntdbd/query_be_block_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcQueryBeBlock proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetQueryBeblockedReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcQueryBeBlock GetQueryBeblockedReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	fromId := subReqBody.GetFromUid()
	targetUid := subReqBody.GetTargetUid()
	infoLog.Printf("ProcQueryBeBlock fromId=%v targetUid=%v", fromId, targetUid)
	mapUidToTime, err := ssdbOperator.GetNotShareToUidList(targetUid)
	if err != nil {
		infoLog.Printf("ProcQueryBeBlock ssdbOperator.GetNotShareToUidList() failed uid=%v cmd=0x%4x seq=%v err=%s",
			head.Uid,
			head.Cmd,
			head.Seq,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	strFromUid := fmt.Sprint("%v", fromId)
	var setValue uint32
	if _, ok := mapUidToTime[strFromUid]; ok {
		setValue = 1
	} else {
		setValue = 0
	}

	rspBody.QueryBeblockedRspbody = &ht_moment.QueryBeBlockedRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		BeBlocked: proto.Uint32(setValue),
	}
	// Step1 answer first
	bNeedCall = false
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	SendRsp(c, head, rspBody, result)
	// Step2 notify write agent to update redis
	//双写更新Redis
	setReqBody := new(ht_moment.ReqBody)
	var notShareToList []*ht_moment.NotShareUid
	for k, v := range mapUidToTime {
		item := &ht_moment.NotShareUid{
			StrUid: proto.String(k),
			Score:  proto.Uint32(v),
		}
		notShareToList = append(notShareToList, item)
	}
	if len(notShareToList) == 0 {
		item := &ht_moment.NotShareUid{
			StrUid: proto.String("0"),
			Score:  proto.Uint32(0),
		}
		notShareToList = append(notShareToList, item)
	}
	setReqBody.SetNotShareToListReqbody = &ht_moment.SetNotShareToListReqBody{
		FromId:         proto.Uint32(targetUid),
		NotShareToList: notShareToList,
	}

	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcQueryBeBlock proto.Marshal SetNotShareToListReqbody failed uid=%v err=%s", head.Uid, err)
		attr := "gocmntdbd/set_not_share_list_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_SET_NOT_SHARE_TO_LIST)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocmntdbd/query_be_block_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 2.proc get chat record
func ProcGetUserInfo(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserInfo no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserInfo invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocmntdbd/get_user_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserInfoReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserInfo GetGetUserInfoRspbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	userId := subReqBody.GetUserid()
	hashName := GetUserInfoHashMapName(userId)
	keyValues, err := ssdbOperator.HgetAll(hashName)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo ssdbOperator.HgetAll failed userId=%v hashName=%s err=%s", userId, hashName, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("ssdb error"),
			},
		}
		attr := "gocmntdbd/get_user_info_db_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	infoLog.Printf("ProcGetUserInfo userId=%v keysValues=%v", userId, keyValues)
	var momentCnt, likeCnt, otherMomentCnt, otherLikeCnt uint32
	keyValuesLen := len(keyValues)
	for index := 0; index < keyValuesLen; index = index + 2 {
		key := keyValues[index]
		value, err := strconv.ParseUint(keyValues[index+1], 10, 32)
		if err != nil {
			infoLog.Printf("ProcGetUserInfo strconv.ParseUint failed key=%s values=%s", key, keyValues[index+1])
			value = 0
		}
		switch key {
		case HASH_KEY_USER_MNT_CNT:
			momentCnt = uint32(value)
		case HASH_KEY_USER_LIKE_CNT:
			likeCnt = uint32(value)
		case HASH_KEY_USER_OTHER_MNT_CNT:
			otherMomentCnt = uint32(value)
		case HASH_KEY_USER_OTHER_LIKE_CNT:
			otherLikeCnt = uint32(value)
		default:
			infoLog.Printf("ProcGetUserInfo unhandle key=%s value=%s", key, keyValues[index+1])
		}
	}
	var setMomentCnt, setLikeCnt uint32
	if head.Uid == userId { // see self moment count
		setMomentCnt = momentCnt
		setLikeCnt = likeCnt
	} else { // see other moment count
		setMomentCnt = otherMomentCnt
		setLikeCnt = otherLikeCnt
	}

	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MomentCount: proto.Uint32(setMomentCnt),
		LikedCount:  proto.Uint32(setLikeCnt),
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)
	// Step2 notify write agent to update redis
	// init redis keys and value
	var redisKeyValues []string
	redisKeyValues = append(redisKeyValues, HASH_KEY_USER_MNT_CNT)
	redisKeyValues = append(redisKeyValues, fmt.Sprintf("%v", momentCnt))
	redisKeyValues = append(redisKeyValues, HASH_KEY_USER_LIKE_CNT)
	redisKeyValues = append(redisKeyValues, fmt.Sprintf("%v", likeCnt))
	redisKeyValues = append(redisKeyValues, HASH_KEY_USER_OTHER_MNT_CNT)
	redisKeyValues = append(redisKeyValues, fmt.Sprintf("%v", otherMomentCnt))
	redisKeyValues = append(redisKeyValues, HASH_KEY_USER_OTHER_LIKE_CNT)
	redisKeyValues = append(redisKeyValues, fmt.Sprintf("%v", otherLikeCnt))
	setReqBody := &ht_moment.ReqBody{
		SetHashMapReqbody: &ht_moment.SetHashMapReqBody{
			HashName:  proto.String(hashName),
			KeyValues: redisKeyValues,
		},
	}
	reqPayLoad, err := proto.Marshal(setReqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo proto.Marshal set hash map req failed uid=%v err=%s", head.Uid, err)
		attr := "gocmntdbd/set_hash_map_proto_err"
		libcomm.AttrAdd(attr, 1)
		return false
	}
	head.Cmd = uint32(ht_moment.CMD_TYPE_CMD_SET_HASH_MAP)
	//双写更新Redis
	SetRedisCache(head, reqPayLoad)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocmntdbd/get_chat_record_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	return true
}

// 3.proc get hash map
func ProcClearUserInfo(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcClearUserInfo no need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcClearUserInfo invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocmntdbd/clear_user_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcClearUserInfo proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetClearUserInfoCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcClearUserInfo GetClearUserInfoCacheReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	userId := subReqBody.GetUserid()
	if userId == 0 {
		infoLog.Printf("ProcClearUserInfo subReqBody.GetUserid() uid=%v userId=%v cmd=0x%4x seq=%v", head.Uid, userId, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param error"),
			},
		}
		return false
	}

	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	// Step1 answer first
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	//双写更新Redis
	SetRedisCache(head, payLoad)
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocmntdbd/clear_user_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	// no need to update redis
	return true
}

func SetRedisCache(head *common.HeadV2, reqPayLoad []byte) {
	redisPacket, err := writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	if err != nil {
		redisPacket, err = writeAgentMastreApi.SendAndRecvPacket(head, reqPayLoad)
	}
	if err == nil {
		redisMasterPacket, ok := redisPacket.(*common.HeadV2Packet)
		if ok { // 是HeadV2Packet报文
			// head 为一个new出来的对象指针
			redisHead, err := redisMasterPacket.GetHead()
			if err == nil {
				//SendResp(c, head, uint16(ERR_INVALID_PARAM))
				infoLog.Printf("SetRedisCache redisMasterPacket uid=%v ret=%v", head.Uid, redisHead.Ret)

			} else {
				infoLog.Printf("SetRedisCache Get redisMasterPacket head failed uid=%v err=%s", head.Uid, err)

			}
		} else {
			infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

		}
	} else {
		infoLog.Printf("SetRedisCache writeAgentMastreApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	}

	// redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	// if err != nil {
	// 	redisPacket, err = writeAgentSlaveApi.SendAndRecvPacket(head, reqPayLoad)
	// }
	// if err == nil {
	// 	redisSlavePacket, ok := redisPacket.(*common.HeadV2Packet)
	// 	if ok { // 是HeadV2Packet报文
	// 		// head 为一个new出来的对象指针
	// 		redisHead, err := redisSlavePacket.GetHead()
	// 		if err == nil {
	// 			//SendResp(c, head, uint16(ERR_INVALID_PARAM))
	// 			infoLog.Printf("SetRedisCache redisSlavePacket uid=%v ret=%v", head.Uid, redisHead.Ret)

	// 		} else {
	// 			infoLog.Printf("SetRedisCache Get redisSlavePacket head failed uid=%v err=%s", head.Uid, err)

	// 		}
	// 	} else {
	// 		infoLog.Printf("SetRedisCache redisPacket can not change to HeadV2packet")

	// 	}

	// } else {
	// 	infoLog.Printf("SetRedisCache writeAgentSlaveApi.SendAndRecvPacket failed uid=%v err=%s", head.Uid, err)

	// }
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_moment.RspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// read profile config path
	// cpuProfile = cfg.Section("PROFILE").Key("cpu_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/cpu.prof")
	// memProfile = cfg.Section("PROFILE").Key("mem_prof").MustString("/home/ht/goproj/cache_family/chat_record/dbd/bin/mem.out")
	// memProfileRate = cfg.Section("PROFILE").Key("mem_rate").MustInt(512 * 1024)
	// read ssdb config
	ssdbHost := cfg.Section("SSDB").Key("ssdb_ip").MustString("127.0.0.1")
	ssdbPort := cfg.Section("SSDB").Key("ssdb_port").MustInt(8888)
	ssdbMinPoolSize := cfg.Section("SSDB").Key("min_pool_size").MustInt(5)
	ssdbMaxPoolSize := cfg.Section("SSDB").Key("max_pool_size").MustInt(500)
	infoLog.Printf("ssdb host=%s port=%v", ssdbHost, ssdbPort)
	ssdbApi, err := common.NewSsdbApi(ssdbHost, ssdbPort, ssdbMinPoolSize, ssdbMaxPoolSize)
	if err != nil {
		infoLog.Printf("common.NewSsdbApi failed err=%s", err)
		checkError(err)
		return
	}

	ssdbOperator = util.NewSsdbOperator(ssdbApi, infoLog)

	// init master cache
	masterIp := cfg.Section("CACHEMASTER").Key("ip").MustString("127.0.0.1")
	masterPort := cfg.Section("CACHEMASTER").Key("port").MustString("6379")
	infoLog.Printf("cache master ip=%v port=%v", masterIp, masterPort)
	writeAgentMastreApi = common.NewSrvToSrvApiV2(masterIp, masterPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)
	// init slave cache
	// slaveIp := cfg.Section("CACHESLAVE").Key("ip").MustString("127.0.0.1")
	// slavePort := cfg.Section("CACHESLAVE").Key("port").MustString("6379")
	// infoLog.Printf("cache slave ip=%v port=%v", slaveIp, slavePort)
	// writeAgentSlaveApi = common.NewSrvToSrvApiV2(slaveIp, slavePort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	//startCPUProfile()
	//startMemProfile()
	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	//stopCPUProfile()
	//stopMemProfile()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
