// Copyright 2017 songliwei
//
// HelloTalk.inc

package util

import (
	"errors"
	"fmt"
	"log"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
)

// Error type
var (
	ErrNotExistInReids = errors.New("not exist in redis")
	ErrNilDbObject     = errors.New("not set object current is nil")
	ErrSsdbError       = errors.New("ssdb error")
)

var (
	NOT_SHARE_MY_MNT_KEY = "#not_share_set"
)

const (
	kZsetKeysSize = 200000
)

type SsdbOperator struct {
	ssdbApi *common.SsdbApi
	infoLog *log.Logger
}

func NewSsdbOperator(targetSsdbApi *common.SsdbApi, logger *log.Logger) *SsdbOperator {
	return &SsdbOperator{
		ssdbApi: targetSsdbApi,
		infoLog: logger,
	}
}

// 获取指定uid不分享给某人看的列表
func (this *SsdbOperator) GetNotShareToUidList(fromId uint32) (mapUidToTime map[string]uint32, err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	zsetName := fmt.Sprintf("%v%s", fromId, NOT_SHARE_MY_MNT_KEY)
	keys, scores, err := this.GetZsetList(zsetName)
	if err != nil {
		return nil, err
	}

	if len(keys) != len(scores) {
		err = ErrSsdbError
		return nil, err
	}

	mapUidToTime = make(map[string]uint32, len(keys))
	for i, v := range keys {
		mapUidToTime[v] = uint32(scores[i])
	}
	return mapUidToTime, nil
}

// 获取指定用户
func (this *SsdbOperator) GetZsetList(zsetName string) (keys []string, scores []int64, err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return nil, nil, err
	}
	keys, scores, err = this.ssdbApi.Zscan(zsetName, "0", 0, (1<<32 - 1), kZsetKeysSize)
	if err != nil {
		this.infoLog.Printf("HgetChatRecord GetZsetList failed zsetName=%s err=%s",
			zsetName,
			err)
		return nil, nil, err
	}
	return keys, scores, nil
}

// 获取全部key 的计数
func (this *SsdbOperator) HgetAll(hashName string) (keyValues []string, err error) {
	if this.ssdbApi == nil {
		err = ErrNilDbObject
		return nil, err
	}
	outKeys, outValues, err := this.ssdbApi.MultiHgetAllSlice(hashName)
	if err != nil {
		this.infoLog.Printf("HgetAll MultiHgetSliceArray failed hashname=%s err=%s",
			hashName,
			err)
		return nil, err
	}
	if len(outKeys) != len(outValues) {
		this.infoLog.Printf("HgetAll keysLen=%v valueLen=%v length error", len(outKeys), len(outValues))
		err = ErrSsdbError
		return nil, err
	}

	keyValues = make([]string, 2*len(outValues))
	for i := 0; i < len(keyValues); i += 2 {
		keyValues[i] = outKeys[i/2]
		keyValues[i+1] = outValues[i/2].String()
	}
	return keyValues, nil
}
