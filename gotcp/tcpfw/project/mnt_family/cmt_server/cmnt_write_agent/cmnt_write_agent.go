package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	redisMasterApi *common.RedisApi
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrSsdbObj      = errors.New("err nil ssdb object")
)

const (
	ExpirePeriod = 259200 //3 * 86400 = 3 day   units:second
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		// 收到错误报文 关闭连接
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gocmntagent/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	switch uint32(head.Cmd) {
	case uint32(ht_moment.CMD_TYPE_CMD_SET_NOT_SHARE_TO_LIST):
		go ProcSetNotShareToList(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_SET_HASH_MAP):
		go ProcSetHashMap(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_CLEAR_USER_INFO_CACHE):
		go ProcClearUserInfoCache(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
		// 无法处理的命令 关闭连接
		c.Close()
	}
	return true
}

// 1.proc set user account cache
func ProcSetNotShareToList(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetNotShareToListRspbody = &ht_moment.SetNotShareToListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetNotShareToList invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocmntagent/set_not_share_to_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetNotShareToList proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.SetNotShareToListRspbody = &ht_moment.SetNotShareToListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetNotShareToListReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSetNotShareToList GetIncChatRecordReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.SetNotShareToListRspbody = &ht_moment.SetNotShareToListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	fromId := subReqBody.GetFromId()
	notShareList := subReqBody.GetNotShareToList()
	infoLog.Printf("ProcSetNotShareToList fromId=%v notShareList=%v", fromId, notShareList)
	if fromId == 0 || len(notShareList) == 0 {
		infoLog.Printf("ProcSetNotShareToList fromId=%v notShareList=%v input param error", fromId, notShareList)
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetNotShareToListRspbody = &ht_moment.SetNotShareToListRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("input param failed"),
			},
		}
		return false
	}
	zsetName := GetNotShareSetZSetName(fromId)
	for _, v := range notShareList {
		infoLog.Printf("ProcSetNotShareToList fromId=%v member=%s score=%v", fromId, v.GetStrUid(), v.GetScore())
		_, err := redisMasterApi.Zadd(zsetName, v.GetScore(), v.GetStrUid())
		if err != nil {
			// add static
			attr := "gocmntagent/zadd_redis_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("ProcSetNotShareToList fromId=%v member=%s score=%v update redis failed err=%s",
				fromId,
				v.GetStrUid(),
				v.GetScore(),
				err)
		}
	}

	err = redisMasterApi.Expire(zsetName, ExpirePeriod)
	if err != nil {
		// add static
		attr := "gocmntagent/set_key_expire_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcSetNotShareToList redisMasterApi.Expire key=%s err=%s failed", zsetName, err)
	}

	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.SetNotShareToListRspbody = &ht_moment.SetNotShareToListRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func GetUserInfoHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v_user_info", uid)
	return hashName
}

func GetNotShareSetZSetName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#not_share_set", uid)
	return hashName
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_moment.RspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

// 2.proc set hash map
func ProcSetHashMap(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.SetHashMapRspbody = &ht_moment.SetHashMapRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcSetHashMap invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocmntagent/set_hash_map_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcSetHashMap proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.SetHashMapRspbody = &ht_moment.SetHashMapRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetSetHashMapReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcSetHashMap GetSetHashMapReqbody() uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.SetHashMapRspbody = &ht_moment.SetHashMapRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	hashName := subReqBody.GetHashName()
	keyValues := subReqBody.GetKeyValues()
	infoLog.Printf("ProcSetHashMap hashName=%s keyValues=%v", hashName, keyValues)
	err = redisMasterApi.Hmset(hashName, keyValues)
	if err != nil {
		infoLog.Printf("ProcSetHashMap  GetSetHashMapReqbody() uid=%v cmd=0x%4x seq=%v err=%s", head.Uid, head.Cmd, head.Seq, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.SetHashMapRspbody = &ht_moment.SetHashMapRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	// 设置过期时间为3天
	err = redisMasterApi.Expire(hashName, ExpirePeriod)
	if err != nil {
		// add static
		attr := "gocmntagent/set_key_expire_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcSetHashMap redisMasterApi.Expire key=%s err=%s failed", hashName, err)
	}

	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.SetHashMapRspbody = &ht_moment.SetHashMapRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

// 3.proc clear uer info cache
func ProcClearUserInfoCache(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcClearUserInfoCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gocmntagent/clear_user_info_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcClearUserInfoCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetClearUserInfoCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcClearUserInfoCache GetClearUserInfoCacheReqbody() uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	userId := subReqBody.GetUserid()
	infoLog.Printf("ProcClearUserInfoCache uid=%v", userId)
	hashName := GetUserInfoHashMapName(userId)
	err = redisMasterApi.Del(hashName)
	if err != nil {
		// add static
		attr := "gocmntagent/delete_hashmap_failed_count"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcClearUserInfoCache redisMasterApi.Del hashName=%s err=%s", hashName, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
	}
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
