package main

import (
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	dbdAgentApi    *common.SrvToSrvApiV2
	redisMasterApi *common.RedisApi
)

const (
	ProcSlowThreshold = 300000
	AdminUid          = 10000
	ExpirePeriod      = 259200 //3 * 86400 = 3 day   units:second
)

const (
	HASH_KEY_USER_LIKE_CNT       = "like_count"              //个人信息中的赞的数量
	HASH_KEY_USER_MNT_CNT        = "moment_count"            //个人信息中的帖子数量
	HASH_KEY_USER_OTHER_LIKE_CNT = "like_count_for_others"   //个人信息中的赞的数量(不包含隐藏的帖子)
	HASH_KEY_USER_OTHER_MNT_CNT  = "moment_count_for_others" //个人信息中的帖子数量(不包含隐藏的帖子)
)

var (
	ErrInvalidParam     = errors.New("err invalid param")
	ErrProtoBuff        = errors.New("pb error")
	ErrInternalErr      = errors.New("internal err")
	ErrNotExistInReids  = errors.New("err not exist in redis")
	ErrNilDbObject      = errors.New("err not set object current is nil")
	ErrDbParam          = errors.New("err param error")
	ErrChagePacket      = errors.New("err change packet")
	ErrGetResultFromDbd = errors.New("err get result from dbs")
)

type UserInfoStr struct {
	MomentCnt      uint32
	LikeCnt        uint32
	OtherMomentCnt uint32
	OtherLikeCnt   uint32
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	// infoLog.Printf("OnMessage:[head:%#v] bodyLen=%v \n", *head, len(packet.GetBody()))
	// infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gocmnt/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INFO):
		go ProcGetUserInfo(c, head, packet)
	case uint32(ht_moment.CMD_TYPE_CMD_CLEAR_USER_INFO_CACHE):
		go ProcClearUserInfoCache(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =", head.Cmd)
		c.Close()
	}
	return true
}

func GetUserInfoHashMapName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v_user_info", uid)
	return hashName
}

func GetNotShareSetZSetName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("%v#not_share_set", uid)
	return hashName
}

// 1.proc store moment info
func ProcGetUserInfo(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcGetUserInfo not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcGetUserInfo invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gocmnt/get_user_info_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gocmnt/get_user_info_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetGetUserInfoReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcGetUserInfo GetGetUserInfoReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}

	targetUid := subReqBody.GetUserid()
	infoLog.Printf("ProcGetUserInfo fromId=%v toIdList=%v", head.Uid, targetUid)
	isBlock, err := IsBlockUserToSeeMoment(head, targetUid)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo IsBlockUserToSeeMoment error uid=%s targetUid=%v err=%s",
			head.Uid,
			targetUid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	if isBlock {
		result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("success"),
			},
			MomentCount: proto.Uint32(0),
			LikedCount:  proto.Uint32(0),
		}
		return true // return send response
	}
	// Step2: query user base info
	momentCnt, likeCnt, err := GetUserMomentInfo(head, targetUid, payLoad)
	if err != nil {
		infoLog.Printf("ProcGetUserInfo GetUserMomentInfo error uid=%s targetUid=%v err=%s",
			head.Uid,
			targetUid,
			err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	result = uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody.GetUserInfoRspbody = &ht_moment.GetUserInfoRspBody{
		Status: &ht_moment.Header{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		MomentCount: proto.Uint32(momentCnt),
		LikedCount:  proto.Uint32(likeCnt),
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocmnt/get_user_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcGetUserInfo uid=%v targetUid=%v procTime=%v", head.Uid, targetUid, procTime)
	return true
}

//检查是否屏蔽用户查看个人信息流
func IsBlockUserToSeeMoment(head *common.HeadV2, userid uint32) (result bool, err error) {
	if head == nil || redisMasterApi == nil || dbdAgentApi == nil {
		err = ErrNilDbObject
		return result, err
	}

	result, err = IsBlockUserToSeeMomentFromRedis(head, userid)
	if err == nil {
		infoLog.Printf("IsBlockUserToSeeMoment GetMntForFollowerList redisOperator.GetMntForFollowerList success uid=%v userid=%v result=%v", head.Uid, userid, result)
		return result, nil
	} else {
		infoLog.Printf("IsBlockUserToSeeMoment IsBlockUserToSeeMomentFromRedis uid=%v userid=%v return err=%s load from ssdb", head.Uid, userid, err)
		attr := "gocmnt/get_not_share_to_uid_redis_failed"
		libcomm.AttrAdd(attr, 1)
		result, err = IsBlockUserToSeeMomentFromDb(head, userid)
		if err != nil {
			infoLog.Printf("IsBlockUserToSeeMoment IsBlockUserToSeeMomentFromDb faield uid=%v userid=%v err=%s", head.Uid, userid, err)
			return result, err
		}
		return result, nil
	}
}

// true: block userid to see moment
// false: not block userid to see moment
func IsBlockUserToSeeMomentFromRedis(head *common.HeadV2, userid uint32) (result bool, err error) {
	if head == nil || redisMasterApi == nil {
		err = ErrNilDbObject
		return result, err
	}
	infoLog.Printf("IsBlockUserToSeeMomentFromRedis fromId=%v uid=%v", head.Uid, userid)
	zSetName := GetNotShareSetZSetName(userid)
	exists, err := redisMasterApi.Exists(zSetName)
	if err != nil {
		infoLog.Printf("IsBlockUserToSeeMomentFromRedis fromId=%v uid=%v err=%s", head.Uid, userid, err)
		return result, err
	}
	infoLog.Printf("IsBlockUserToSeeMomentFromRedis fromId=%v uid=%v exist=%v", head.Uid, userid, exists)
	if !exists {
		err = ErrNotExistInReids
		infoLog.Printf("IsBlockUserToSeeMomentFromRedis fromId=%v uid=%v not exist in redis", head.Uid, userid)
		return result, err
	}

	err = redisMasterApi.Expire(zSetName, ExpirePeriod)
	if err != nil {
		infoLog.Printf("IsBlockUserToSeeMomentFromRedis expire key=%s err=%s", zSetName, err)
		attr := "gocmnt/expire_key_slow"
		libcomm.AttrAdd(attr, 1)
	}

	member := fmt.Sprintf("%v", head.Uid)
	val, err := redisMasterApi.Zscore(zSetName, member)
	if err == nil {
		// check whether get member score
		if len(val) > 0 { // targetUid not share moment to head.Uid return
			result = true // return send response
		} else {
			result = false
		}
	} else if err == redis.ErrNil {
		result = false
	} else {
		infoLog.Printf("IsBlockUserToSeeMomentFromRedis zSetName=%s Zscore return err=%s", zSetName, err)
		return result, err
	}

	return result, nil
}

func IsBlockUserToSeeMomentFromDb(head *common.HeadV2, userid uint32) (result bool, err error) {
	// send packet to dbd
	reqHead := new(common.HeadV2)
	*reqHead = *head
	reqHead.Cmd = uint32(ht_moment.CMD_TYPE_CMD_QUERY_BEBLOCKED)
	infoLog.Printf("IsBlockUserToSeeMomentFromDb reqHead=%#v", *reqHead)
	reqBody := &ht_moment.ReqBody{
		QueryBeblockedReqbody: &ht_moment.QueryBeBlockedReqBody{
			FromUid:   proto.Uint32(head.Uid),
			TargetUid: proto.Uint32(userid),
		},
	}

	reqPayLoad, err := proto.Marshal(reqBody)
	if err != nil {
		infoLog.Printf("IsBlockUserToSeeMomentFromDb proto.Marshal failed uid=%v err=%s", head.Uid, err)
		return result, err
	}
	dbdPacket, err := dbdAgentApi.SendAndRecvPacket(reqHead, reqPayLoad)
	if err != nil {
		infoLog.Printf("IsBlockUserToSeeMomentFromDb dbdAgentApi.SendAndRecvPacket send packet failed uid=%v err=%s", head.Uid, err)
		return result, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("IsBlockUserToSeeMomentFromDb change packet failed uid=%v", reqHead.Uid)
		err = ErrChagePacket
		return result, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		infoLog.Printf("IsBlockUserToSeeMomentFromDb GetHead failed uid=%v err=%s", reqHead.Uid, err)
		return result, err
	}

	if rspHead.Ret != uint16(ht_moment.RET_CODE_RET_SUCCESS) {
		infoLog.Printf("IsBlockUserToSeeMomentFromDb ret=%v not success", rspHead.Ret)
		err = ErrGetResultFromDbd
		return result, err
	}

	rspBody := new(ht_moment.RspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		infoLog.Printf("IsBlockUserToSeeMomentFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		return result, err
	}
	subRspBody := rspBody.GetQueryBeblockedRspbody()
	if subRspBody == nil {
		infoLog.Printf("IsBlockUserToSeeMomentFromDb GetQueryBeblockedRspbody failed uid=%v cmd=0x%4x seq=%v", reqHead.Uid, reqHead.Cmd, reqHead.Seq)
		err = ErrGetResultFromDbd
		return result, err
	}
	beBlock := subRspBody.GetBeBlocked()
	if beBlock > 0 {
		result = true
	} else {
		result = false
	}
	return result, nil
}

//查看个人信息流条数和点赞数
func GetUserMomentInfo(head *common.HeadV2, userid uint32, payLoad []byte) (momentCnt, likeCnt uint32, err error) {
	if head == nil || redisMasterApi == nil || dbdAgentApi == nil || userid == 0 {
		err = ErrNilDbObject
		return momentCnt, likeCnt, err
	}

	momentCnt, likeCnt, err = GetUserMomentInfoFromRedis(head, userid)
	if err == nil {
		infoLog.Printf("GetUserMomentInfo GetUserMomentInfoFromRedis success uid=%v userid=%v momnetCnt=%v likeCnt=%v",
			head.Uid,
			userid,
			momentCnt,
			likeCnt)
		return momentCnt, likeCnt, nil
	} else {
		infoLog.Printf("GetUserMomentInfo GetUserMomentInfoFromRedis uid=%v userid=%v err=%s load from ssdb", head.Uid, userid, err)
		attr := "gocmnt/get_user_moment_info_redis_failed"
		libcomm.AttrAdd(attr, 1)
		momentCnt, likeCnt, err = GetUserMomentInfoFromDb(head, payLoad)
		if err != nil {
			infoLog.Printf("GetUserMomentInfo GetUserMomentInfoFromDb faield uid=%v userid=%v err=%s", head.Uid, userid, err)
			return momentCnt, likeCnt, err
		}
		return momentCnt, likeCnt, nil
	}
}

func GetUserMomentInfoFromRedis(head *common.HeadV2, userid uint32) (momentCnt, likeCnt uint32, err error) {
	if head == nil || redisMasterApi == nil || userid == 0 {
		err = ErrNilDbObject
		return momentCnt, likeCnt, err
	}
	infoLog.Printf("GetUserMomentInfoFromRedis fromId=%v uid=%v", head.Uid, userid)
	hashName := GetUserInfoHashMapName(userid)
	exists, err := redisMasterApi.Exists(hashName)
	if err != nil {
		infoLog.Printf("GetUserMomentInfoFromRedis fromId=%v uid=%v err=%s", head.Uid, userid, err)
		return momentCnt, likeCnt, err
	}
	infoLog.Printf("GetUserMomentInfoFromRedis fromId=%v uid=%v exist=%v", head.Uid, userid, exists)
	if !exists {
		err = ErrNotExistInReids
		infoLog.Printf("GetUserMomentInfoFromRedis fromId=%v uid=%v not exist in redis", head.Uid, userid)
		return momentCnt, likeCnt, err
	}
	userInfo := UserInfoStr{
		MomentCnt:      0,
		LikeCnt:        0,
		OtherMomentCnt: 0,
		OtherLikeCnt:   0,
	}
	keyValue, err := redisMasterApi.Hgetall(hashName)
	if err != nil {
		infoLog.Printf("GetUserMomentInfoFromRedis fromId=%v uid=%v redisMasterApi.Hgetall err=%s", head.Uid, userid, err)
		return momentCnt, likeCnt, err
	}
	err = redisMasterApi.Expire(hashName, ExpirePeriod)
	if err != nil {
		infoLog.Printf("GetUserMomentInfoFromRedis expire key=%s err=%s", hashName, err)
		attr := "gocmnt/expire_key_slow"
		libcomm.AttrAdd(attr, 1)
	}

	// 获取成功
	for k, v := range keyValue {
		switch k {
		case HASH_KEY_USER_LIKE_CNT:
			value, err := strconv.ParseUint(v, 10, 32)
			if err != nil {
				attr := "gocmnt/strconv_parse_error"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("GetUserMomentInfoFromRedis strconv.ParseUint err uid=%v v=%s err=%s",
					head.Uid,
					v,
					err)
				userInfo.LikeCnt = 0 // set moment count zero
			} else {
				userInfo.LikeCnt = uint32(value)
			}
		case HASH_KEY_USER_MNT_CNT:
			value, err := strconv.ParseUint(v, 10, 32)
			if err != nil {
				attr := "gocmnt/strconv_parse_error"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("GetUserMomentInfoFromRedis strconv.ParseUint err uid=%v v=%s err=%s",
					head.Uid,
					v,
					err)
				userInfo.MomentCnt = 0 // set moment count zero
			} else {
				userInfo.MomentCnt = uint32(value)
			}
		case HASH_KEY_USER_OTHER_LIKE_CNT:
			value, err := strconv.ParseUint(v, 10, 32)
			if err != nil {
				attr := "gocmnt/strconv_parse_error"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("GetUserMomentInfoFromRedis strconv.ParseUint err uid=%v v=%s err=%s",
					head.Uid,
					v,
					err)
				userInfo.OtherLikeCnt = 0 // set moment count zero
			} else {
				userInfo.OtherLikeCnt = uint32(value)
			}
		case HASH_KEY_USER_OTHER_MNT_CNT:
			value, err := strconv.ParseUint(v, 10, 32)
			if err != nil {
				attr := "gocmnt/strconv_parse_error"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("GetUserMomentInfoFromRedis strconv.ParseUint err uid=%v v=%s err=%s",
					head.Uid,
					v,
					err)
				userInfo.OtherMomentCnt = 0 // set moment count zero
			} else {
				userInfo.OtherMomentCnt = uint32(value)
			}
		default:
			infoLog.Printf("GetUserMomentInfoFromRedis unknow key=%s value=%s", k, v)
		}
	}

	if head.Uid == userid {
		momentCnt = userInfo.MomentCnt
		likeCnt = userInfo.LikeCnt
	} else {
		momentCnt = userInfo.OtherMomentCnt
		likeCnt = userInfo.OtherLikeCnt
	}

	if likeCnt > 100000 || momentCnt > 100000 {
		infoLog.Printf("GetUserMomentInfoFromRedis uid=%v Fatal error!like_cnt=%v or mnt_cnt = %v is invalid!", head.Uid, likeCnt, momentCnt)
		momentCnt = 0
		likeCnt = 0
	}

	return momentCnt, likeCnt, nil
}

func GetUserMomentInfoFromDb(head *common.HeadV2, payLoad []byte) (momentCnt, likeCnt uint32, err error) {
	// send packet to dbd
	if head == nil || len(payLoad) == 0 {
		infoLog.Printf("GetUserMomentInfoFromDb nil param")
		err = ErrInvalidParam
		return momentCnt, likeCnt, err
	}

	dbdPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("GetUserMomentInfoFromDb dbdAgentApi.SendAndRecvPacket send packet failed uid=%v err=%s", head.Uid, err)
		return momentCnt, likeCnt, err
	}

	dbdPacketV2, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("GetUserMomentInfoFromDb change packet failed uid=%v", head.Uid)
		err = ErrChagePacket
		return momentCnt, likeCnt, err
	}

	rspHead, err := dbdPacketV2.GetHead()
	if err != nil {
		infoLog.Printf("GetUserMomentInfoFromDb GetHead failed uid=%v err=%s", head.Uid, err)
		return momentCnt, likeCnt, err
	}

	if rspHead.Ret != uint16(ht_moment.RET_CODE_RET_SUCCESS) {
		infoLog.Printf("GetUserMomentInfoFromDb ret=%v not success", head.Ret)
		err = ErrGetResultFromDbd
		return momentCnt, likeCnt, err
	}

	rspBody := new(ht_moment.RspBody)
	err = proto.Unmarshal(dbdPacketV2.GetBody(), rspBody)
	if err != nil {
		infoLog.Printf("GetUserMomentInfoFromDb proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return momentCnt, likeCnt, err
	}
	subRspBody := rspBody.GetGetUserInfoRspbody()
	if subRspBody == nil {
		infoLog.Printf("GetUserMomentInfoFromDb GetGetUserInfoRspbody failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		err = ErrGetResultFromDbd
		return momentCnt, likeCnt, err
	}
	momentCnt = subRspBody.GetMomentCount()
	likeCnt = subRspBody.GetLikedCount()
	if likeCnt > 100000 || momentCnt > 100000 {
		infoLog.Printf("GetUserMomentInfoFromDb uid=%v Fatal error!like_cnt=%v or mnt_cnt = %v is invalid!", head.Uid, likeCnt, momentCnt)
		momentCnt = 0
		likeCnt = 0
	}
	return momentCnt, likeCnt, nil
}

// 2.proc clear user info cache
func ProcClearUserInfoCache(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_moment.RET_CODE_RET_SUCCESS)
	rspBody := new(ht_moment.RspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			// SendRsp(c, head, rspBody, result)
			infoLog.Printf("ProcClearUserInfoCache need not response")
		} else {
			infoLog.Printf("ProcClearUserInfoCache not need call")
		}
	}()
	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_moment.RET_CODE_RET_INPUT_PARAM_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcClearUserInfoCache invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		attr := "gocmnt/clear_user_info_invalid_param"
		libcomm.AttrAdd(attr, 1)
		return false
	}

	// add static
	attr := "gocmnt/clear_user_info_count"
	libcomm.AttrAdd(attr, 1)
	// Step1 读取redisMaster上的缓存如果Master直接命中则返回
	reqBody := new(ht_moment.ReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcClearUserInfoCache proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetClearUserInfoCacheReqbody()
	if subReqBody == nil {
		infoLog.Printf("ProcClearUserInfoCache GetClearUserInfoCacheReqbody() failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_PB_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	userId := subReqBody.GetUserid()
	infoLog.Printf("ProcClearUserInfoCache fromId=%v userId=%v", head.Uid, userId)
	dbdPacket, err := dbdAgentApi.SendAndRecvPacket(head, payLoad)
	if err != nil {
		infoLog.Printf("ProcClearUserInfoCache dbdAgentApi.SendAndRecvPacket send packet failed uid=%v err=%s", head.Uid, err)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	_, ok := dbdPacket.(*common.HeadV2Packet)
	if !ok {
		infoLog.Printf("ProcClearUserInfoCache change packet failed uid=%v", head.Uid)
		result = uint16(ht_moment.RET_CODE_RET_INTERNAL_ERR)
		rspBody.ClearUserInfoCacheRspbody = &ht_moment.ClearUserInfoCacheRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}
	bNeedCall = false
	// send no response
	// SendRspPacket(c, dbdPacketV2)

	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		// add static
		attr := "gocmnt/clear_user_info_proc_slow"
		libcomm.AttrAdd(attr, 1)
	}
	infoLog.Printf("ProcClearUserInfoCache fromId=%v userId=%v procTime=%v", head.Uid, userId, procTime)
	return true
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_moment.RspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init redis api
	redisIp := cfg.Section("REDISMASTER").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDISMASTER").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis master ip=%v port=%v", redisIp, redisPort)
	redisMasterApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init dbd api
	dbdIp := cfg.Section("CACHEDBD").Key("ip").MustString("127.0.0.1")
	dbdPort := cfg.Section("CACHEDBD").Key("port").MustString("6379")
	infoLog.Printf("cache dbd ip=%v port=%v", dbdIp, dbdPort)
	dbdAgentApi = common.NewSrvToSrvApiV2(dbdIp, dbdPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}
func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
