package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/project/lbs_headquarter/org"
	apicommon "github.com/HT_GOGO/gotcp/webapi/common"
	_ "github.com/go-sql-driver/mysql"
	"github.com/nsqio/go-nsq"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"
)

type Callback struct{}

var (
	infoLog          *log.Logger
	db               *sql.DB
	userInfoCacheApi *common.UserInfoCacheApi
	nsqPublisher     *nsq.Producer
	nsqLBSTopic      string
	lbsCacheRedis    *common.RedisApi
	options          apicommon.Options
	GeoDbClient      *org.GeoDbAPI
	GOOGLE_API_KEY   string
)

const (
	CMD_UPDATE_LOCATION       = 0x2007
	CMD_UPDATE_LOCATION_ACK   = 0x2008
	CMD_GET_USER_LOCATION     = 0x2009
	CMD_GET_USER_LOCATION_ACK = 0x200A
	CMD_REFRESH_LOCATION      = 0x2035
	CMD_REFRESH_LOCATION_ACK  = 0x2036
)
const (
	DB_RET_SUCCESS     = 0
	DB_RET_EXEC_FAILED = 1
	DB_RET_NOT_EXIST   = 100
)

const (
	RET_SUCCESS            = 0
	ERR_SYSERR_START       = 100
	ERR_SERVER_BUSY        = 100
	ERR_INTERNAL_ERROR     = 101
	ERR_UNFORMATTED_PACKET = 102
	ERR_NO_ACCESS          = 103
	ERR_INVALID_CLIENT     = 104
	ERR_INVALID_SESSION    = 105
	ERR_INVALID_PARAM      = 106
)

const (
	STRGOOGLE         = "google"
	MULTILANGNAMEHMAP = "country#lang"
	PROJECT_NAME      = "golbs"
)

func MatchLocationPlaceIdInRedis(englishPlace *org.TagPlaceInformation) (placeId int64) {

	field := org.GetLocationPlaceIdKey(englishPlace)
	r, err := lbsCacheRedis.HgetInt64(org.LOCATIONTOPLACEHMAP, field)
	if err != nil {
		infoLog.Printf("MatchLocationPlaceIdInRedis() Reids exec HGET failed,%s,field=[%s] err=%v", org.LOCATIONTOPLACEHMAP, field, err)
		return
	} else {
		placeId = r
		infoLog.Printf("MatchLocationPlaceIdInRedis()  Redis exec HGet succ ,%s,placeId=[%d],field=[%s]", org.LOCATIONTOPLACEHMAP, placeId, field)
		return
	}
}

func MatchLocationPlaceId(locationInfo *org.TagLocationInfo, canWriteDB bool, languageType string) (placeId int64) {

	field := org.GetLocationPlaceIdKey(&locationInfo.PlaceInfo)

	r, err := lbsCacheRedis.HgetInt64(org.LOCATIONTOPLACEHMAP, field)

	if err != nil {

		infoLog.Printf("MatchLocationPlaceId() Reids exec HGET Uid=%d field=%s err=%v", locationInfo.Uid, field, err)
		// load from db
		placeId = MatchLocationPlaceIdInDb(locationInfo, canWriteDB)

		//if new location is not english ,get multilang location immedieately
		shortLang := org.MatchLanguageShortName(languageType)

		if shortLang != "en" {

			code, multiPlace, err := GeoDbClient.GetCoordinateFromGoogleApi(locationInfo.Uid, locationInfo.Latitude, locationInfo.Longtitude, shortLang, "MatchLocationPlaceId")

			if code == org.GOOGLE_API_OK && multiPlace != nil && err == nil {
				// 查询db 得到MultiPlaceInfo 后重新将插入redis
				field := org.GetMultiPlaceInfoKey(placeId, shortLang)
				value := multiPlace.GetCacheString()
				libcomm.ProAttrAdd(PROJECT_NAME, "google_maps_api_ok", 1)
				err = lbsCacheRedis.Hset(org.MULTIPLACEINFOHMAP, field, value)
				if err != nil {
					infoLog.Printf("MatchLocationPlaceId() Reids Hset failed,%s,field=[%s] err=%v", org.MULTIPLACEINFOHMAP, field, err)
					return
				}
			} else {
				libcomm.ProAttrAdd(PROJECT_NAME, "google_maps_api_fail", 1)
				infoLog.Println("MatchLocationPlaceId() GetCoordinateFromGoogleApi err=", err.Error(), locationInfo.PlaceId)
			}
		}

		return
	} else {
		infoLog.Printf("MatchLocationPlaceId() Redis exec HGet succ placeId=%d,Uid=%d,field=%s", placeId, locationInfo.Uid, field)
		placeId = r
		return
	}
}

func MatchLocationPlaceIdInDb(locationInfo *org.TagLocationInfo, canWriteTodb bool) (placeId int64) {
	if locationInfo == nil {
		infoLog.Println("MatchLocationPlaceIdInDb input param err")
		placeId = 0 // placeId must not be 0
		return
	}
	err := db.QueryRow("select PLACEID from HT_LOCATION_PLACE WHERE COUNTRY=? and ADMINISTRATIVE1=? and ADMINISTRATIVE2=? and "+
		"ADMINISTRATIVE3=? and LOCALITY=? and SUBLOCALITY=? and NEIGHBORHOOD=?",
		locationInfo.PlaceInfo.Country,
		locationInfo.PlaceInfo.Admin1,
		locationInfo.PlaceInfo.Admin2,
		locationInfo.PlaceInfo.Admin3,
		locationInfo.PlaceInfo.Locality,
		locationInfo.PlaceInfo.SubLocality,
		locationInfo.PlaceInfo.Neighborhood).Scan(&placeId)
	switch {
	case err == sql.ErrNoRows:
		if canWriteTodb == false {
			infoLog.Println("MatchLocationPlaceIdInDb cannot write to db")
			return 0
		}
		// 执行插入语句 使用mysql 的函数 UTC_TIMESTAMP()获取当前时间戳
		r, err := db.Exec("insert into HT_LOCATION_PLACE (STATE,COUNTRY,ADMINISTRATIVE1,ADMINISTRATIVE2,ADMINISTRATIVE3,"+
			"LOCALITY,SUBLOCALITY,NEIGHBORHOOD,LATITUDE,LONGITUDE,UPDATETIME) value"+
			"(0, ?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP())",
			locationInfo.PlaceInfo.Country,
			locationInfo.PlaceInfo.Admin1,
			locationInfo.PlaceInfo.Admin2,
			locationInfo.PlaceInfo.Admin3,
			locationInfo.PlaceInfo.Locality,
			locationInfo.PlaceInfo.SubLocality,
			locationInfo.PlaceInfo.Neighborhood,
			locationInfo.Latitude,
			locationInfo.Longtitude)
		if err != nil {
			infoLog.Println("MatchLocationPlaceIdInDb() insert faield err=", err, locationInfo.Uid)
			placeId = 0
			return
		} else {
			placeId, err = r.LastInsertId()
			if err != nil {
				infoLog.Println("MatchLocationPlaceIdInDb() Get last insert id faied err =", err, locationInfo.Uid)
				placeId = 0
				return
			}

			locationInfo.IsNewPlace = 1
			locationInfo.PlaceId = uint32(placeId)
			PublishEventForLBSInfo(locationInfo.Uid, "new_locatioin_pace", locationInfo)

			infoLog.Printf("MatchLocationPlaceIdInDb() new HT_LOCATION_PLACE ok,placeid=%d,Uid=%d,Country=%s", placeId, locationInfo.Uid, locationInfo.PlaceInfo.Country)
			// 查询db 得到id 后重新将id 插入redis
			field := org.GetLocationPlaceIdKey(&locationInfo.PlaceInfo)
			err = lbsCacheRedis.HsetInt64(org.LOCATIONTOPLACEHMAP, field, placeId)
			if err != nil {
				infoLog.Printf("MatchLocationPlaceIdInDb() HSET location#palceid field=%s place=%v faield err=%v", field, placeId, err)
			} else {
				infoLog.Printf("MatchLocationPlaceIdInDb() HSET locatio#palceid field=%s place=%v succ", field, placeId)
			}
		}
	case err != nil:
		infoLog.Println("MatchLocationPlaceIdInDb() rows.Scan failed err =", err)
		placeId = 0
		return
	default: // 查询成功将查结果存入redis并返回插入结果

		locationInfo.PlaceId = uint32(placeId)
		// 查询db 得到id 后重新将id 插入redis
		field := org.GetLocationPlaceIdKey(&locationInfo.PlaceInfo)

		err = lbsCacheRedis.HsetInt64(org.LOCATIONTOPLACEHMAP, field, placeId)

		if err != nil {
			infoLog.Printf("MatchLocationPlaceIdInDb() HSET location#palceid field=%s place=%v faield err=%v", field, placeId, err)
		} else {
			infoLog.Printf("MatchLocationPlaceIdInDb() HSET location#palceid field=%s place=%v succ", field, placeId)
		}
		return
	}
	return
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.XTHeadPacket)
	if !ok { // 不是XTHeadPacket报文
		infoLog.Println("packet can not change to xtpacket")
		return false
	}

	head, err := packet.GetHead()
	if err != nil {
		//SendXTResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Println("OnMessage() Get head failed", err)
		return false
	}
	libcomm.ProAttrAdd(PROJECT_NAME, "recv_req_count", 1)

	//infoLog.Printf("OnMessage:[%#v] len=%v payLoad=%v\n", head, len(packet.GetBody()), packet.GetBody())
	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckXTPacketValid()
	if err != nil {
		common.SendXTResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Println("OnMessage() Invalid packet", err)
		return false
	}

	switch head.Cmd {
	case CMD_UPDATE_LOCATION:
		go ProcUpdateLocation(c, p)
	case CMD_GET_USER_LOCATION:
		go ProcGetUserLocation(c, p)
	case CMD_REFRESH_LOCATION:
		go ProcRefreshLocation(c, p)
	default:
		infoLog.Println("OnMessage() UnHandle Cmd =", head.Cmd)
	}
	return true
}

func ProcUpdateLocation(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.XTHeadPacket)
	if !ok { // 不是XTHeadPacket报文
		infoLog.Println("ProcUpdateLocation() packet can not change to xtpacket")
		return false
	}

	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("ProcUpdateLocation()  get packet failed\n")
		return false
	}

	libcomm.ProAttrAdd(PROJECT_NAME, "update_location_req_count", 1)

	body := packet.GetBody()
	var locationInfo org.TagLocationInfo
	locationInfo.Uid = common.UnMarshalUint32(&body)
	languageType := common.UnMarshalSliceToString(&body)
	locationInfo.Allowed = common.UnMarshalUint8(&body)
	if locationInfo.Allowed > 1 || locationInfo.Uid != head.From {
		common.SendXTResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateLocation()  error allowed =%v uid=%v from=%v", locationInfo.Allowed, locationInfo.Uid, head.From)
		return false
	}

	//保留字段填充的是 客户端类型
	locationInfo.Ostype = head.TermType
	uid := locationInfo.Uid
	//从客户端数据里面提取数据
	var needUpdate int
	if locationInfo.Allowed == 1 {
		locationInfo.Latitude = common.UnMarshalSliceToString(&body)
		locationInfo.Longtitude = common.UnMarshalSliceToString(&body)
		locationInfo.PlaceInfo.Country = common.UnMarshalSliceToString(&body)
		locationInfo.PlaceInfo.Admin1 = common.UnMarshalSliceToString(&body)
		locationInfo.PlaceInfo.Admin2 = common.UnMarshalSliceToString(&body)
		locationInfo.PlaceInfo.Admin3 = common.UnMarshalSliceToString(&body)
		locationInfo.PlaceInfo.Locality = common.UnMarshalSliceToString(&body)
		locationInfo.PlaceInfo.SubLocality = common.UnMarshalSliceToString(&body)
		locationInfo.PlaceInfo.Neighborhood = common.UnMarshalSliceToString(&body)
		locationInfo.Source = common.UnMarshalSliceToString(&body)
		needUpdate = strings.Compare(STRGOOGLE, (locationInfo.Source))
	}
	infoLog.Printf("ProcUpdateLocation() uid=%v lang=%s allow=%d lat=%s lng=%s country=%s"+
		" admin1=%s admin2=%s admin3=%s locality=%s subloc=%s neibhood=%s source=%s needUpdate=%v",
		locationInfo.Uid,
		languageType,
		locationInfo.Allowed,
		locationInfo.Latitude,
		locationInfo.Longtitude,
		locationInfo.PlaceInfo.Country,
		locationInfo.PlaceInfo.Admin1,
		locationInfo.PlaceInfo.Admin2,
		locationInfo.PlaceInfo.Admin3,
		locationInfo.PlaceInfo.Locality,
		locationInfo.PlaceInfo.SubLocality,
		locationInfo.PlaceInfo.Neighborhood,
		locationInfo.Source,
		needUpdate)
	// 检查参数是否合法
	if !IsValidParam(head, &locationInfo) {
		common.SendXTResp(c, head, uint8(ERR_INVALID_PARAM))
		infoLog.Printf("ProcUpdateLocation() error input allowed =%v uid=%v from=%v", locationInfo.Allowed, locationInfo.Uid, head.From)
		return false
	}

	//比较看客户端提交的和内存的是否有区别

	locationNeedChanged := true
	strLocation := GetSingleUserLocationFromRedis(head.From, uid)

	if len(strLocation) != 0 {
		var oldLocationInfo org.TagLocationInfo
		needRefreshRedis, decodeErr := org.StrLocationToTagLocationInfo(uid, strLocation, &oldLocationInfo)
		if decodeErr != nil {
			infoLog.Printf("ProcUpdateLocation StrLocationToTagLocationInfo from=%d to=%d err=%s", head.From, head.To, decodeErr.Error())

		} else {

			//如果定位没有变化 直接返回
			if needRefreshRedis == false && locationInfo.Allowed == oldLocationInfo.Allowed && locationInfo.PlaceInfo == oldLocationInfo.PlaceInfo {
				libcomm.ProAttrAdd(PROJECT_NAME, "update_not_changed_loc", 1)
				locationNeedChanged = false
				locationInfo.CityPlaceId = oldLocationInfo.CityPlaceId
				locationInfo.TimeStamp = oldLocationInfo.TimeStamp
				infoLog.Printf("ProcUpdateLocation  Uid=%d place not changed,CityPlaceId=%d,Country=%s", uid, locationInfo.CityPlaceId, locationInfo.PlaceInfo.Country)

				if oldLocationInfo.CityPlaceId == 0 && oldLocationInfo.Allowed == 1 && locationInfo.PlaceInfo.Country != "" {
					locationInfo.CityPlaceId = GeoDbClient.GetCityPlaceIdFromDb(locationInfo.PlaceId, uid, &locationInfo.PlaceInfo)
					infoLog.Printf("ProcUpdateLocation  Uid=%d place not changed,reload CityPlaceId=%d,Country=%s", uid, locationInfo.CityPlaceId, locationInfo.PlaceInfo.Country)
					locationNeedChanged = true
				}
			}

		}
	}
	//客户端没有提交定位的时候 从本地db 获取，db没有去google api 获取。
	if locationInfo.Allowed == 1 &&
		len(locationInfo.PlaceInfo.Country) == 0 &&
		len(locationInfo.Latitude) > 3 &&
		len(locationInfo.Longtitude) > 3 {

		libcomm.ProAttrAdd(PROJECT_NAME, "client_update_lbs_empty_country", 1)

		sphinxPlaceId, GeodbPlace, errsp := GeoDbClient.GetPlaceInfoFromSphinx(locationInfo.Uid, locationInfo.Latitude, locationInfo.Longtitude, 80500)
		// GetCoordinateFromGoogleApi(locationInfo.Uid, locationInfo.Latitude, locationInfo.Longtitude, Lang)
		if errsp != nil {

			infoLog.Printf("ProcUpdateLocation() uid=%v,GetPlaceInfoFromSphinx err=%v", locationInfo.Uid, errsp)

			// load place from google api
			if sphinxPlaceId == 0 {

				libcomm.ProAttrAdd(PROJECT_NAME, "update_lbs_but_place_from_google_api", 1)

				code, multiPlace, err := GeoDbClient.GetCoordinateFromGoogleApi(locationInfo.Uid, locationInfo.Latitude, locationInfo.Longtitude, "en", "ProcUpdateLocation")

				if code == org.GOOGLE_API_OK && multiPlace != nil && err == nil {
					libcomm.ProAttrAdd(PROJECT_NAME, "google_maps_api_ok", 1)
					if multiPlace.Country != "" {
						locationInfo.PlaceInfo = *multiPlace
						locationInfo.Source = org.STRGOOGLE
						needUpdate = 1
					}
				} else {
					libcomm.ProAttrAdd(PROJECT_NAME, "google_maps_api_fail", 1)
				}
			}

		} else {

			libcomm.ProAttrAdd(PROJECT_NAME, "update_lbs_but_place_from_sphinx", 1)

			if GeodbPlace != nil && sphinxPlaceId > 0 {
				locationInfo.PlaceInfo = *GeodbPlace
			}
		}
	}

	//提取客户端数据的placeid
	var placeId int64
	if (locationInfo.Allowed == 1) &&
		locationNeedChanged &&
		(len(locationInfo.PlaceInfo.Country) != 0) { // update if Country is not empty

		//  (needUpdate == 0)  means update only if google
		placeId = MatchLocationPlaceId(&locationInfo, needUpdate == 1, languageType)
		// 从redis或数据库加载placeId 仍然加载失败直接返回内部错误
		if placeId == 0 { // load failed
			common.SendXTResp(c, head, uint8(ERR_INTERNAL_ERROR))
			infoLog.Println("ProcUpdateLocation()  load placeId failed [from, allow] =", head.From, locationInfo.Allowed)
			return false
		} else {
			locationInfo.PlaceId = uint32(placeId)
		}

		if locationInfo.CityPlaceId == 0 {
			locationInfo.CityPlaceId = GeoDbClient.GetCityPlaceIdFromDb(locationInfo.PlaceId, locationInfo.Uid, &locationInfo.PlaceInfo)
		}

		// 如果是一个新的地址，并且取到的 cityplaceid ==0，可是这可能是个新的城市，直接用 placeid 作为  cityplaceid
		if locationInfo.IsNewPlace == 1 && locationInfo.CityPlaceId == 0 {
			locationInfo.CityPlaceId = locationInfo.PlaceId
		}

		if locationInfo.CityPlaceId == 0 {
			libcomm.ProAttrAdd(PROJECT_NAME, "city_placeid_from_db_zero", 1)
		}
	}

	//需要更新才处理，否则只更新内存
	if locationNeedChanged {

		locationInfo.PlaceId = uint32(placeId)
		locationInfo.TimeStamp = uint64(apicommon.GetMilliNow() / 1000)

		// 更新 HT_USER_LOCATION2 和 HT_USER_PROPERTY表
		err = GeoDbClient.UpdateLocationInDb(&locationInfo)
		if err != nil {
			infoLog.Printf("ProcUpdateLocation() UpdateLocationInDb failed uid=%v err=%s", head.From, err)
		} else {
			err = userInfoCacheApi.UpdateLocationVer(head.From, locationInfo.TimeStamp)
			if err != nil {
				libcomm.ProAttrAdd(PROJECT_NAME, "update_user_info_cache_failed", 1)
				infoLog.Printf("ProcUpdateLocation() UpdateLocationVer failed uid=%v err=%s", head.From, err)
			}
		}

		// 更新user info cache 删除Redis中的位置信息
		UpdateSingleUserLocationInRedis(&locationInfo)
	} else {

		SetTagLocationInfoToRedis(&locationInfo, org.KTTLInterval)
	}

	// 获取多语言的位置
	multiLocation := locationInfo
	if locationInfo.Allowed == 1 {
		if strings.Compare(languageType, "English") != 0 {

			multiLocation.PlaceInfo = GetMultiPlaceInfo(&(locationInfo.PlaceInfo), locationInfo.CityPlaceId, languageType)
			// 如果查询db 多语言位置也失败 这使用English地理位置
			if len(multiLocation.PlaceInfo.Country) == 0 {
				multiLocation.PlaceInfo = locationInfo.PlaceInfo
			}
		}
	}

	multiLocation.PlaceInfo.ReformCountryName(languageType)
	var respPayLoad []byte
	common.MarshalUint8(uint8(0), &respPayLoad)
	common.MarshalUint8(locationInfo.Allowed, &respPayLoad)
	city := multiLocation.PlaceInfo.ReformLocationCityName()

	if locationInfo.Allowed == 1 {
		common.MarshalStringToSlice(multiLocation.PlaceInfo.FullCountry, &respPayLoad)
		common.MarshalStringToSlice(city, &respPayLoad)
	}
	common.SendXTRespWithPayLoad(c, head, respPayLoad)
	infoLog.Printf("ProcUpdateLocation() from=%v to=%v cmd=%v seq=%v ret=%d allow=%d country=%s full=%s city=%s\n",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		0,
		locationInfo.Allowed,
		multiLocation.PlaceInfo.Country,
		multiLocation.PlaceInfo.FullCountry,
		city)
	return true
}

func GetMultiPlaceInfo(englishPlace *org.TagPlaceInformation, CityPlaceId uint32, languageType string) (multiPlace org.TagPlaceInformation) {

	shortLang := org.MatchLanguageShortName(languageType)

	//避免不存在的语言
	if shortLang == "en" {
		return *englishPlace
	}

	placeId := int64(CityPlaceId)
	if CityPlaceId == 0 {
		libcomm.ProAttrAdd(PROJECT_NAME, "map_placeid_from_redis", 1)
		placeId = MatchLocationPlaceIdInRedis(englishPlace) //之前更新地址位置时会立刻更新redis 所以子查询redis即可
	} else {
		libcomm.ProAttrAdd(PROJECT_NAME, "use_cityplaceid", 1)
	}
	if placeId > 0 {
		field := org.GetMultiPlaceInfoKey((placeId), shortLang)
		strPlace, err := lbsCacheRedis.Hget(org.MULTIPLACEINFOHMAP, field)
		if err != nil {
			infoLog.Printf("GetMultiPlaceInfo() Reids exec HGET key=%s field=%s err=%v", org.MULTIPLACEINFOHMAP, field, err)
			// load from db
			var ret bool
			multiPlace, ret = GetMultiPlaceInfoInDb((placeId), shortLang)
			libcomm.ProAttrAdd(PROJECT_NAME, "get_multi_place_from_mysql", 1)
			if !ret {
				infoLog.Printf("GetMultiPlaceInfo()  MultiPlaceInfoInDb failed ,err placeId=[%d], shortLang=[%s]", placeId, shortLang)
				return
			}

		} else {
			fieldArry := strings.Split(strPlace, "#")

			infoLog.Printf("GetMultiPlaceInfo() Redis exec HGet succ strPalce=%s,size=%d,placeId=%d", strPlace, len(fieldArry), placeId)
			for i, v := range fieldArry {
				// infoLog.Printf("index=%v value=%s", i, v)
				switch i {
				case 0:
					multiPlace.Country = (v)
					// infoLog.Println("Country =", multiPlace.Country)
				case 1:
					multiPlace.Admin1 = (v)
					// infoLog.Println("Admin1 =", multiPlace.Admin1)
				case 2:
					multiPlace.Admin2 = (v)
					// infoLog.Println("Admin2 =", multiPlace.Admin2)
				case 3:
					multiPlace.Admin3 = (v)
					// infoLog.Println("Admin3 =", multiPlace.Admin3)
				case 4:
					multiPlace.Locality = (v)
					// infoLog.Println("Locality =", multiPlace.Locality)
				case 5:
					multiPlace.SubLocality = (v)
					// infoLog.Println("SubLocality =", multiPlace.SubLocality)
				case 6:
					multiPlace.Neighborhood = (v)
					// infoLog.Println("Neighborhood =", multiPlace.Neighborhood)
				default:
					infoLog.Printf("GetMultiPlaceInfo() unknow filed index=%v value=%v", i, v)
				}
			}
			return
		}

	} else {
		infoLog.Println("GetMultiPlaceInfo() failed placeId=0")
	}
	return
}

func GetMultiPlaceInfoInDb(placeId int64, shortLang string) (multiPlace org.TagPlaceInformation, ret bool) {

	var country, admin1, admin2, admin3, loca, subLoca, neigh string
	err := db.QueryRow("select COUNTRY,ADMINISTRATIVE1,ADMINISTRATIVE2,ADMINISTRATIVE3,LOCALITY,SUBLOCALITY,NEIGHBORHOOD from HT_MULTILANG_PLACE where PLACEID=? and LANGTYPE=? AND STATE>0", placeId, shortLang).Scan(&country, &admin1, &admin2, &admin3, &loca, &subLoca, &neigh)
	switch {
	case err == sql.ErrNoRows:
		infoLog.Println("GetMultiPlaceInfoInDb() Db mysql Query empty [palce shortLang] =", placeId, shortLang)
		ret = false
		return
	case err != nil:
		infoLog.Println("GetMultiPlaceInfoInDb rows.Scan failed [place shortLang err] =", placeId, shortLang, err)
		ret = false
		return
	default: // 查询成功将查结果存入redis并返回插入结果
		infoLog.Printf("GetMultiPlaceInfoInDb() palceId=%v shortLang=%s country=%s admin1=%s admin2=%s admin3=%s loca=%s subLoac=%s neigh=%s \n", placeId, shortLang, country, admin1, admin2, admin3, loca, subLoca, neigh)
		// 设置返回值
		multiPlace.SetPlace(country, admin1, admin2, admin3, loca, subLoca, neigh)
		ret = true
		// 查询db 得到MultiPlaceInfo 后重新将插入redis
		field := org.GetMultiPlaceInfoKey(placeId, shortLang)
		value := multiPlace.GetCacheString()

		err = lbsCacheRedis.Hset(org.MULTIPLACEINFOHMAP, field, value)

		if err != nil {
			infoLog.Printf("GetMultiPlaceInfoInDb() HSET placeid#lang field=%s value=%v failed err=%v", field, value, err)
		} else {
			infoLog.Printf("GetMultiPlaceInfoInDb() HSET palceid#lang field=%s value=%v succ", field, value)
		}
		return
	}
	return
}

func IsValidParam(head *common.XTHead, locationInfo *org.TagLocationInfo) bool {
	if head == nil || locationInfo == nil {
		return false
	}
	if head.From != locationInfo.Uid {
		return false
	}

	return locationInfo.IsValid()

}

func ProcGetUserLocation(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.XTHeadPacket)
	if !ok { // 不是XTHeadPacket报文
		infoLog.Println("packet can not change to xtpacket")
		return false
	}
	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("ProcGetUserLocation get head failed\n")
		return false
	}

	libcomm.ProAttrAdd(PROJECT_NAME, "get_location_req_count", 1)

	body := packet.GetBody()
	languageType := common.UnMarshalSliceToString(&body)
	userCount := common.UnMarshalUint16(&body)
	infoLog.Printf("ProcGetUserLocation() Recv from=%v to=%v cmd=%v seq=%v count=%v lang=%s\n",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		userCount,
		languageType)

	if userCount == 0 || userCount > 200 {
		common.SendXTResp(c, head, uint8(ERR_INTERNAL_ERROR))
		infoLog.Println("ProcGetUserLocation input err count =", userCount)
		return false
	}

	listUserId := make([]uint32, userCount)
	for i := 0; i < int(userCount); i++ {
		uid := common.UnMarshalUint32(&body)
		if uid != 0 {
			listUserId[i] = uid
		} else {
			infoLog.Printf("ProcGetUserLocation input err uid = 0 from=%v\n", head.From)
		}
	}
	if len(listUserId) == 0 {
		infoLog.Printf("ProcGetUserLocation uid list empty from=%v\n", head.From)
		common.SendXTResp(c, head, uint8(RET_SUCCESS))
		return false
	}

	var listLocationInfo []org.TagLocationInfo
	var missUidList []uint32
	strLocation := GetUserLocationFromRedis(head.From, listUserId)
	if len(strLocation) == 0 {
		infoLog.Println("ProcGetUserLocation GetUserLocationFromRedis failed empty result load from db")
		missUidList = listUserId // 一个都没有找到直接从数据库中查询全部
	} else { // 获取到部分位置信息
		for i, v := range strLocation {
			if len(v) == 0 { // 长度为0 说明没有找到此用户地址位置信息添加到missUidList中
				missUidList = append(missUidList, listUserId[i])
			} else {

				var locational org.TagLocationInfo
				needRefreshRedis, decodeErr := org.StrLocationToTagLocationInfo(0, v, &locational)
				if decodeErr != nil {
					infoLog.Printf("ProcGetUserLocation From=%d v=[%s],err =[%s],needRef=[%v]", head.From, v, decodeErr.Error(), needRefreshRedis)
					missUidList = append(missUidList, listUserId[i])
					continue
				}

				listLocationInfo = append(listLocationInfo, locational) // 得到一个用户的地址位置添加到结果集中
			}
		}
	}

	if len(missUidList) != 0 { // 有用户cache miss 从DB中load
		ret := GetUserLocationFromDb(missUidList, &listLocationInfo)
		if !ret {
			infoLog.Println("GetUserLocationFromDb failed")
		}
	}

	for i, v := range listLocationInfo {
		if v.Allowed == 1 {
			showLang := languageType
			if (languageType == "Chinese_yy" || "Chinese" == languageType) && v.PlaceInfo.Country == "JP" {
				showLang = "Japanese"
			}

			if strings.Compare(showLang, "English") != 0 { // 获取多语言的位置
				multiPlace := GetMultiPlaceInfo(&(v.PlaceInfo), v.CityPlaceId, showLang)
				if len(multiPlace.Country) != 0 { //存在多语言的位置才替换
					listLocationInfo[i].PlaceInfo = multiPlace
				}
			}
			listLocationInfo[i].PlaceInfo.ReformCountryName(showLang)
		}
	}
	// 返回响应
	var respPayLoad []byte
	common.MarshalUint8(0, &respPayLoad) // rsp code
	common.MarshalUint16(uint16(len(listLocationInfo)), &respPayLoad)
	cityName := ""
	for k, v := range listLocationInfo {
		var subRspPayLoad []byte
		common.MarshalUint32(v.Uid, &subRspPayLoad)
		common.MarshalUint64(v.TimeStamp, &subRspPayLoad)
		common.MarshalUint8(v.Allowed, &subRspPayLoad)
		if v.Allowed == 1 {
			common.MarshalStringToSlice(v.Latitude, &subRspPayLoad)
			common.MarshalStringToSlice(v.Longtitude, &subRspPayLoad)
			common.MarshalStringToSlice(v.PlaceInfo.FullCountry, &subRspPayLoad)
			cityName = v.PlaceInfo.ReformLocationCityName()
			common.MarshalStringToSlice((cityName), &subRspPayLoad)
		}
		if k == 0 {
			infoLog.Printf("ProcGetUserLocation() Uid=%d,Allowed=%d,Country=%s,cityName=%s", v.Uid, v.Allowed, v.PlaceInfo.Country, cityName)
		}
		common.MarshalSlice(subRspPayLoad, &respPayLoad)
	}
	bret, errSend := common.SendXTRespWithPayLoad(c, head, respPayLoad)
	infoLog.Printf("ProcGetUserLocation() ret=%v count=%v\n", bret, len(listLocationInfo))
	if errSend != nil {
		infoLog.Printf("ProcGetUserLocation() from=%v to=%v SendXTRespWithPayLoad err=%s \n", head.From, head.To, errSend.Error())
	}
	return true
}

func GetUserLocationFromRedis(From uint32, listUserId []uint32) (strLocation []string) {
	if len(listUserId) == 0 {
		infoLog.Println("GetUserLocationFromRedis() listUserId empty")
		return
	}

	strUidList := make([]string, len(listUserId))
	for i, _ := range listUserId {
		strUidList[i] = org.GetUserPlaceKey(listUserId[i])
	}
	infoLog.Println("GetUserLocationFromRedis() [len strUidList] =", From, len(strUidList), strUidList)
	s := make([]interface{}, len(strUidList))
	for i, v := range strUidList {
		s[i] = v
		// infoLog.Printf("GetUserLocationFromRedis() index=%v key=%s", i, v)
	}

	strLocation, err := lbsCacheRedis.Mget(strUidList)
	if err != nil {
		infoLog.Println("GetUserLocationFromRedis() Reids failed MGet err =", err)
		return
	}

	infoLog.Println("GetUserLocationFromRedis() result size= ", len(strLocation), From)
	for i, v := range strLocation {
		infoLog.Printf("GetUserLocationFromRedis() From=%d index=[%d] result=[%v]\n", From, i, v)
	}
	return
}

func GetUserLocationFromDb(listUserId []uint32, listLocationInfo *[]org.TagLocationInfo) bool {
	if len(listUserId) == 0 {
		return true
	}
	strUids := apicommon.JoinUint32(listUserId, ",")

	infoLog.Println("GetUserLocationFromDb uids =", strUids)
	rows, err := db.Query("select t1.USERID, t1.ALLOWED, t1.LATITUDE, t1.LONGITUDE, t1.COUNTRY," +
		"t1.ADMINISTRATIVE1, t1.ADMINISTRATIVE2, t1.ADMINISTRATIVE3, t1.LOCALITY," +
		"t1.SUBLOCALITY, t1.NEIGHBORHOOD,CITYPLACEID, UNIX_TIMESTAMP(t1.UPDATETIME)  from HT_USER_LOCATION2 " +
		"as t1 where t1.USERID in (" + strUids + ");")
	if err != nil {
		infoLog.Printf("GetUserLocationFromDb uids=%s failed\n", strUids)
		return false
	}
	defer rows.Close()
	for rows.Next() {
		var intUid, intAllow, intVer, CityPlaceId uint32
		var strLa, strLong, strCo, strA1, strA2, strA3, strLoc, strSubLoc, strNe string
		if err := rows.Scan(&intUid, &intAllow, &strLa, &strLong, &strCo, &strA1, &strA2, &strA3, &strLoc, &strSubLoc, &strNe, &CityPlaceId, &intVer); err != nil {
			infoLog.Println("GetUserLocationFromDb rows.Scan failed")
			continue
		}

		loca := org.TagLocationInfo{Uid: intUid,
			Allowed:    uint8(intAllow),
			Latitude:   strLa,
			Longtitude: strLong,
			PlaceInfo: org.TagPlaceInformation{Admin1: strA1,
				Admin2:       strA2,
				Admin3:       strA3,
				Country:      strCo,
				Locality:     strLoc,
				SubLocality:  strSubLoc,
				Neighborhood: strNe,
			},
			CityPlaceId: CityPlaceId,
			TimeStamp:   uint64(intVer),
		}

		SetTagLocationInfoToRedis(&loca, org.ViewKTTLInterval)

		*listLocationInfo = append(*listLocationInfo, loca)
	}
	return true
}

func SetTagLocationInfoToRedis(loca *org.TagLocationInfo, ttl uint32) {

	value := loca.GetUserCacheString()

	strKey := org.GetUserPlaceKey(loca.Uid)

	err := lbsCacheRedis.Setex(strKey, value, ttl)

	if err != nil {
		infoLog.Printf("SetTagLocationInfoToRedis() %d SETEX key=%s ttl=%d value=[%s] failed,err=%s", loca.Uid, strKey, ttl, value, err.Error())
	} else {
		infoLog.Printf("SetTagLocationInfoToRedis() %d SETEX key=%s ttl=%d value=[%s] succ", loca.Uid, strKey, ttl, value)
	}
}

func ProcRefreshLocation(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.XTHeadPacket)
	if !ok { // 不是XTHeadPacket报文
		infoLog.Println("packet can not change to xtpacket")
		return false
	}
	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("ProcRefreshLocation get head failed\n")
		return false
	}

	libcomm.ProAttrAdd(PROJECT_NAME, "refersh_location_req_count", 1)

	body := packet.GetBody()
	languageType := common.UnMarshalSliceToString(&body)
	uid := common.UnMarshalUint32(&body)
	ts := common.UnMarshalUint64(&body)
	infoLog.Printf("ProcRefreshLocation from=%v to=%v cmd=%v seq=%v uid=%v ts=%v\n",
		head.From,
		head.To,
		head.Cmd,
		head.Seq,
		uid,
		ts)
	if head.From != uid {
		infoLog.Println("ProcRefreshLocation [from uid] not equal", head.From, uid)
	}

	var locationInfo org.TagLocationInfo
	strLocation := GetSingleUserLocationFromRedis(head.From, uid)
	var isNeedLoadFromDb bool = true
	if len(strLocation) != 0 {

		needRefreshRedis, decodeErr := org.StrLocationToTagLocationInfo(uid, strLocation, &locationInfo)
		if decodeErr != nil {
			infoLog.Printf("ProcRefreshLocation StrLocationToTagLocationInfo from=%d to=%d err=%s", head.From, head.To, decodeErr.Error())
			goto LOADFROMDB
		}
		isNeedLoadFromDb = false // get from redis succ no need load from db

		if needRefreshRedis {
			libcomm.ProAttrAdd(PROJECT_NAME, "refresh_redis_add_cityid", 1)
			isNeedLoadFromDb = true
		}
	}
LOADFROMDB:
	if isNeedLoadFromDb { // load from db
		ret := GetSingleUserLocationFromDb(uid, ts, &locationInfo)
		switch ret {
		case DB_RET_NOT_EXIST:
			infoLog.Printf("ProcRefreshLocation from=%v to=%v cmd=%v seq=%v ts=%v no need to update\n",
				head.From,
				head.To,
				head.Cmd,
				head.Seq,
				ts)
			respPayLoad := make([]byte, 1)
			common.MarshalUint8(uint8(1), &respPayLoad)
			msg := []byte("not need to update")
			common.MarshalSlice(msg, &respPayLoad)
			common.SendXTRespWithPayLoad(c, head, respPayLoad)
			return true
		case DB_RET_EXEC_FAILED:
			infoLog.Printf("ProcRefreshLocation from=%v to=%v cmd=%v seq=%v ts=%v db exec failed\n",
				head.From,
				head.To,
				head.Cmd,
				head.Seq,
				ts)
			common.SendXTResp(c, head, uint8(ERR_INTERNAL_ERROR))
			return true
		default: // get succ
			infoLog.Println("ProcRefreshLocation succ uid", uid)
		}
	}
	multiLocation := locationInfo
	if multiLocation.Allowed == 1 {
		if strings.Compare(languageType, "English") != 0 {

			showLang := languageType
			if (languageType == "Chinese_yy" || "Chinese" == languageType) && locationInfo.PlaceInfo.Country == "JP" {
				showLang = "Japanese"
			}

			multiLocation.PlaceInfo = GetMultiPlaceInfo(&locationInfo.PlaceInfo, locationInfo.CityPlaceId, showLang)
			// 多语言地址位置为空继续使用English位置
			if len(multiLocation.PlaceInfo.Country) == 0 {
				multiLocation.PlaceInfo = locationInfo.PlaceInfo
			}
		}
		multiLocation.PlaceInfo.ReformCountryName(languageType)
	}
	// 返回响应
	var respPayLoad []byte
	common.MarshalUint8(uint8(0), &respPayLoad)
	var subRspPayLoad []byte
	common.MarshalUint32(multiLocation.Uid, &subRspPayLoad)
	common.MarshalUint64(multiLocation.TimeStamp, &subRspPayLoad)
	common.MarshalUint8(multiLocation.Allowed, &subRspPayLoad)
	if multiLocation.Allowed == 1 {
		common.MarshalStringToSlice(multiLocation.Latitude, &subRspPayLoad)
		common.MarshalStringToSlice(multiLocation.Longtitude, &subRspPayLoad)
		common.MarshalStringToSlice(multiLocation.PlaceInfo.FullCountry, &subRspPayLoad)
		cityName := multiLocation.PlaceInfo.ReformLocationCityName()
		common.MarshalStringToSlice((cityName), &subRspPayLoad)
	}
	common.MarshalSlice(subRspPayLoad, &respPayLoad)

	ret, err := common.SendXTRespWithPayLoad(c, head, respPayLoad)
	infoLog.Printf("ProcRefreshLocation ret=%v From=%d %v", ret, head.From, err)
	return true
}

//用户提交定位之后，立即更新内存里面的数据
func UpdateSingleUserLocationInRedis(loca *org.TagLocationInfo) (ret bool) {

	SetTagLocationInfoToRedis(loca, org.KTTLInterval)

	PublishEventForLBSInfo(loca.Uid, "user_update_location", loca)

	return
}

func GetSingleUserLocationFromRedis(Fromid, uid uint32) string {
	key := org.GetUserPlaceKey(uid)

	strLocation, err := lbsCacheRedis.Get(key)

	if err != nil {
		infoLog.Printf("GetSingleUserLocationFromRedis() Reids failed,Fromid=%d key=[%s],strLocation=[%s]", Fromid, key, err.Error())
	} else {
		infoLog.Printf("GetSingleUserLocationFromRedis() OK ,Fromid=%d key=[%s],strLocation=[%s]", Fromid, key, strLocation)
	}

	return strLocation
}

func GetSingleUserLocationFromDb(uid uint32, ts uint64, locationInfo *org.TagLocationInfo) (ret uint32) {

	infoLog.Println("GetSingleUserLocationFromDb uid =", uid, ts)

	var CityPlaceId, intAllow, showUid, intVer uint32
	var strLa, strLong, strCo, strA1, strA2, strA3, strLoc, strSubLoc, strNe string
	err := db.QueryRow("select t1.USERID, t1.ALLOWED, t1.LATITUDE, t1.LONGITUDE, t1.COUNTRY,"+
		"t1.ADMINISTRATIVE1, t1.ADMINISTRATIVE2, t1.ADMINISTRATIVE3, t1.LOCALITY,"+
		"t1.SUBLOCALITY, t1.NEIGHBORHOOD, UNIX_TIMESTAMP(t1.UPDATETIME) as lastts,CITYPLACEID from HT_USER_LOCATION2 "+
		"as t1 where t1.USERID = ? AND UNIX_TIMESTAMP(t1.UPDATETIME) > ?;", uid, ts).Scan(&showUid, &intAllow, &strLa, &strLong, &strCo, &strA1, &strA2, &strA3, &strLoc, &strSubLoc, &strNe, &intVer, &CityPlaceId)
	switch {
	case err == sql.ErrNoRows:
		infoLog.Printf("GetSingleUserLocationFromDb not found uid=%v ts=%v\n", uid, ts)
		ret = uint32(DB_RET_NOT_EXIST)
		return
	case err != nil:
		infoLog.Println("GetSingleUserLocationFromDb [uid  ts err] =", uid, ts, err)
		ret = uint32(DB_RET_EXEC_FAILED)
		return
	default:
		infoLog.Printf("GetSingleUserLocationFromDb() uid=%d allow=%d la=%s long=%s co=%s A1=%s A2=%s A3=%s Loc=%s SubLod=%s Ne=%s Ver=%d,CityPlaceId=%d\n",
			showUid,
			intAllow,
			strLa,
			strLong,
			strCo,
			strA1,
			strA2,
			strA3,
			strLoc,
			strSubLoc,
			strNe,
			intVer, CityPlaceId)

		locationInfo.Uid = uid
		locationInfo.Allowed = uint8(intAllow)
		locationInfo.Latitude = (strLa)
		locationInfo.Longtitude = (strLong)
		locationInfo.CityPlaceId = CityPlaceId
		locationInfo.PlaceInfo.SetPlace(strCo, strA1, strA2, strA3, strLoc, strSubLoc, strNe)
		locationInfo.TimeStamp = uint64(intVer)
		// set redis
		value := locationInfo.GetUserCacheString()
		strKey := org.GetUserPlaceKey(uid)
		err = lbsCacheRedis.Setex(strKey, value, org.KTTLInterval)

		if err != nil {
			infoLog.Printf("GetSingleUserLocationFromDb SETEX key=%s value=%s faield err=%v", strKey, value, err)
		} else {
			infoLog.Printf("GetSingleUserLocationFromDb SETEX key=%s value=%s succ", strKey, value)
		}
		// return result
		ret = uint32(DB_RET_SUCCESS) // only set here

	}
	return
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

func PublishEventForLBSInfo(Userid uint32, Type string, info *org.TagLocationInfo) (uint32, error) {

	event := org.EventForLBSInfo{
		Type: Type,
		Data: info,
	}

	buff, err := json.Marshal(event)

	if err != nil {
		return 0, err
	}

	err = nsqPublisher.Publish(nsqLBSTopic, buff)

	if err != nil {
		libcomm.ProAttrAdd(PROJECT_NAME, "to_nsq_failed", 1)
		return 0, err
	}

	return 0, err
}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := apicommon.InitLogAndOption(&options, &infoLog)

	db, err = apicommon.InitMySqlFromSection(cfg, infoLog, "MYSQL", 4, 1)
	apicommon.CheckError(err)
	defer db.Close()

	infoLog.Println("InitMultiLangCountryMapForDb start....")
	err = org.InitMultiLangCountryMapForDb(db, infoLog)
	fmt.Println("InitMultiLangCountryMapForDb end.... err=", err)

	sphinx_host := cfg.Section("SERVICE_CONFIG").Key("sphinx_host").MustString("127.0.0.1")
	sphinx_port := cfg.Section("SERVICE_CONFIG").Key("sphinx_port").MustInt(9306)

	GOOGLE_API_KEY = cfg.Section("SERVICE_CONFIG").Key("GOOGLE_API_KEY").MustString("127.0.0.1")
	sphinx_location_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_location_index").MustString("location")

	infoLog.Println("NewGeoDbAPI start....", sphinx_host, sphinx_port, sphinx_location_index)
	GeoDbClient = org.NewGeoDbAPI(sphinx_host, sphinx_port, sphinx_location_index, infoLog, db, GOOGLE_API_KEY)
	infoLog.Println("NewGeoDbAPI end....")

	sphinxPlaceId, GeodbPlace, errsp := GeoDbClient.GetPlaceInfoFromSphinx(90086, "22.540941", "113.946780", 80500)
	fmt.Println("GetPlaceInfoFromSphinx end.... err=", sphinxPlaceId, GeodbPlace, errsp)
	// init redis pool
	redisIp := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustString("6379")

	infoLog.Printf("lbsCacheRedis ip=%v port=%v", redisIp, redisPort)
	lbsCacheRedis = common.NewRedisApi(redisIp + ":" + redisPort)

	nsq_host := cfg.Section("NSQ_CONFIG").Key("host").MustString("10.144.89.163:4150")
	nsqLBSTopic = cfg.Section("NSQ_CONFIG").Key("topic").MustString("lbs_update")
	infoLog.Printf("Nsq NewProducer nsq_host=%v port=%v", nsq_host, nsqLBSTopic)
	nsqconfig := nsq.NewConfig()
	nsqPublisher, err = nsq.NewProducer(nsq_host, nsqconfig)
	apicommon.CheckError(err)
	defer nsqPublisher.Stop()

	// user info cache api
	userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	infoLog.Printf("user info cache ip=%v port=%v", userInfoIp, userInfoPort)
	userInfoCacheApi = common.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &common.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	apicommon.CheckError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	apicommon.CheckError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}
	srv := gotcp.NewServer(config, &Callback{}, &common.XTHeadProtocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
	db.Close()
}
