package main

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/HT_GOGO/gotcp"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/golang/protobuf/proto"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_purchase_new"
	"github.com/HT_GOGO/gotcp/tcpfw/project/purchase_new/util"
	"github.com/dogenzaka/go-iap/appstore"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

type Callback struct{}

var (
	infoLog        *log.Logger
	appstoreConfig appstore.Config
	appstoreClient appstore.Client
	DbUtil         *util.DbUtil
)

const (
	ProcSlowThreshold = 300000
	AdminUid          = 10000
	ExpireReceiptCode = 21006
	VerifySuccessCode = 0
)

// 与数据库中STATE的定义一致
const (
	E_STATE_INIT    = 0 // 未验证
	E_STATE_SUCC    = 1 // 验证成功
	E_STATE_INVALID = 2 // 验证失败
	E_STATE_EXPIRED = 3 // 验证过期
)

const (
	YEAR_NORMAL = 0 //'0-非年会员 1-年会员'
	YEAR_VIP    = 1
)

const (
	E_NOT_VIP       = 0   // 非会员
	E_DAYS_VIP      = 1   // 按天计数的会员(旧版本购买的月、年会员)
	E_MONTHAUTO_VIP = 2   // 月自动续费的会员
	E_YEARAUTO_VIP  = 3   // 年自动续费的会员
	E_LIFETIME_VIP  = 100 // 终生会员 (50年后过期)
)

const (
	ClientCommit = 0
	ServerCommit = 1
)

const (
	TypePurchase = 0 //0-购买
	TypeRenew    = 1 //1-续订
	TRYTIMES     = 3
)

const (
	PURCHASE_PWD = "f2e40870103240f7b485b6f186fa141a"
	PACKAGE_NAME = "com.hellotalk"
)

const (
	PRODUCT_ONE_MONTH = 1
	// "com.hellotalk.onemonth1"
	PRODUCT_ONE_MONTHAUTO = 2
	// "com.hellotalk.onemonthauto"
	// or GooglePlay="com.hellotalk.onemonth1auto"
	// 2016-07-16 android use "com.hellotalk.1monthautorenew"
	// 2016-08-08  iOS use "com.hellotalk.monthauto"
	PRODUCT_ONE_YEAR = 3
	// "com.hellotalk.oneyear"
	PRODUCT_THREE_MONTH = 4
	// "com.hellotalk.3months"
	PRODUCT_YEAR_AUTO = 5
	// "com.hellotalk.yearauto"
	PRODUCT_LIFT_TIME = 6
	// "com.hellotalk.lifetime"
	// product id for upgrade vip price
	PRODUCT_ONE_MONTH_SUB_PLAN = 7
	// iOS:"com.hellotalk.onemonthsubscriptionplan"
	// And:"com.hellotalk.onemonthsubscriptionplan2"
	PRODUCT_ONE_YEAR_SUB_PLAN = 8
	// iOS:"com.hellotalk.oneyearsubscriptionplan"
	// iOS:"com.hellotalk.yearauto2"
	// And:"com.hellotalk.oneyearsubscriptionplan2"
	PRODUCT_LIFE_TIME_SUB_PLAN = 9
	// "com.hellotalk.lifetimeplan"

	PRODUCT_GIFT_ONEMONTH = 11
	// "com.hellotalk.Gift1M" or GooglePlay="com.hellotalk.g1m"
	PRODUCT_GIFT_THREEMONTH = 12
	// "com.hellotalk.Gift3M" or GooglePlay="com.hellotalk.g3m"
	PRODUCT_GIFT_ONEYEAR = 13
	// "com.hellotalk.Gift1Y" or GooglePlay="com.hellotalk.g1y"
	// product id for trial membership
	PRODUCT_ONE_MONTH_SUB = 14
	// iOS:"com.hellotalk.onemonthsubscription"
	// iOS:"com.hellotalk.monthauto_freetrial"
	// And:"com.hellotalk.onemonthsubscription2"
	PRODUCT_ONE_YEAR_SUB = 15
	// iOS:"com.hellotalk.oneyearsubscription"
	// iOS:"com.hellotalk.yearauto2_freetrial"
	// And:"com.hellotalk.oneyearsubscription2"
	PRODUCT_ONE_MULITLANG = 21
	// "com.hellotalk.1moreLang"
	PRODUCT_ONE_MONTH_AUTO_B = 22
	// "com.hellotalk.onemonthauto.b"
	// iOS:"com.hellotalk.super1monthauto"
	PRODUCT_ONE_MONTH_AUTO_C = 23
	// "com.hellotalk.onemonthauto.c"
	PRODUCT_YEAR_AUTO_B = 24
	// "com.hellotalk.yearauto.b"
	// iOS:"com.hellotalk.super1yearauto"
	PRODUCT_YEAR_AUTO_C = 25
	// "com.hellotalk.yearauto.c"
	PRODUCT_LIFE_TIME_B = 26
	// "com.hellotalk.lifetime.b"
	// iOS:"com.hellotalk.super1lifetime"
	PRODUCT_LIFE_TIME_C = 27
	// "com.hellotalk.lifetime.c"
	PRODUCT_OPEN_ENGLISH_YEAR_AUTO = 30
	//com.hellotalk.openenglish1year
	PRODUCT_ITEM_END = 31
	// product item end
)

var (
	ErrInvalidParam = errors.New("err invalid param")
	ErrProtoBuff    = errors.New("pb error")
	ErrInternalErr  = errors.New("internal err")
	ErrInvalidRsp   = errors.New("err invalid respones")
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		c.Close()
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		c.Close()
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet err=%s", err)
		c.Close()
		return false
	}

	// 统计总的请求量
	attr := "gonewpur/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)
	switch head.Cmd {
	case uint32(ht_purchase_new.PURCHASE_NEW_CMD_TYPE_CMD_COMMIT_PURCHASE_ORDER_NEW_REQ):
		go ProcCommitPurchaseOrder(c, head, packet)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd=%v", head.Cmd)
		c.Close()
	}
	return true
}

// 1.proc store moment info
func ProcCommitPurchaseOrder(c *gotcp.Conn, head *common.HeadV2, packet *common.HeadV2Packet) bool {
	// parse packet
	result := uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_SUCCESS)
	rspBody := new(ht_purchase_new.PurchaseNewRspBody)
	bNeedCall := true
	defer func() {
		if bNeedCall {
			SendRsp(c, head, rspBody, result)
		} else {
			infoLog.Printf("ProcCommitPurchaseOrder not need call")
		}
	}()

	// 检查输入参数是否为空
	payLoad := packet.GetBody()
	if head == nil || len(payLoad) == 0 {
		result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_INVALID_PARAM)
		rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
			Status: &ht_purchase_new.PurchaseNewHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		infoLog.Printf("ProcCommitPurchaseOrder invalid param uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		return false
	}

	// add static
	attr := "gonewpur/commit_purchase_count"
	libcomm.AttrAdd(attr, 1)
	reqBody := new(ht_purchase_new.PurchaseNewReqBody)
	err := proto.Unmarshal(payLoad, reqBody)
	if err != nil {
		infoLog.Printf("ProcCommitPurchaseOrder proto Unmarshal failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_PB_ERR)
		rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
			Status: &ht_purchase_new.PurchaseNewHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetCommitPurchaseOrderNewReqbody()
	if subReqBody == nil {
		infoLog.Println("ProcCommitPurchaseOrder GetCommitPurchaseOrderNewReqbody() failed uid=%v cmd=0x%4x seq=%v", head.Uid, head.Cmd, head.Seq)
		result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_INVALID_PARAM)
		rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
			Status: &ht_purchase_new.PurchaseNewHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	payType := subReqBody.GetPayType()
	userId := subReqBody.GetUserId()
	toId := subReqBody.GetToId()
	itemCode := subReqBody.GetItemCode()
	infoLog.Printf("ProcCommitPurchaseOrder payType=%v userId=%v toId=%v itemCode=%v",
		payType,
		userId,
		toId,
		itemCode)
	if userId == 0 ||
		toId == 0 ||
		itemCode == 0 ||
		itemCode >= PRODUCT_ITEM_END ||
		uint32(payType) == 0 ||
		payType < ht_purchase_new.PAY_TYPE_PAY_TYPE_APP_STORE ||
		payType > ht_purchase_new.PAY_TYPE_PAY_TYPE_WECHAT_PAY {
		infoLog.Printf("ProcCommitPurchaseOrder invalid param uid=%v cmd=0x%4x seq=%v payType=%v userId=%v toId=%v itemCode=%v",
			head.Uid,
			head.Cmd,
			head.Seq,
			payType,
			userId,
			toId,
			itemCode)
		result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_INVALID_PARAM)
		rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
			Status: &ht_purchase_new.PurchaseNewHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}

	historyRecord := &util.PurchaseHistory{
		UserId:   userId,
		ToId:     toId,
		ItemCode: itemCode,
		PayType:  uint8(payType),
	}
	orderType := ht_purchase_new.ORDER_TYPE_E_NORMAL_ORDER
	var utilTimeStampRenew uint64  // 自动续费的到期时间戳(自动续费才有,以秒为单位)
	var pruchasePeriodRenew uint64 // 订单的持续时间=订单过期时间戳-订单购买时间戳(自动续费才有,以秒为单位))
	switch payType {
	case ht_purchase_new.PAY_TYPE_PAY_TYPE_APP_STORE:
		appleOrder := subReqBody.GetAppleOrder()
		outTransactionId := string(appleOrder.GetTransactionId())
		infoLog.Printf("ProcCommitPurchaseOrder appleOrder product_id=%s store_area=%s transaction_id=%s purchase_date_gmt=%s purchase_date=%s currency=%s pay_money=%s jailbroken=%v",
			appleOrder.GetProductId(),
			appleOrder.GetStoreArea(),
			appleOrder.GetTransactionId(),
			appleOrder.GetPurchaseDateGmt(),
			appleOrder.GetPurchaseDate(),
			appleOrder.GetCurrency(),
			appleOrder.GetPayMoney(),
			appleOrder.GetJailbroken())
		if itemCode != EnumProductCode(string(appleOrder.GetProductId())) {
			infoLog.Printf("ProcCommitPurchaseOrder invalid param uid=%v cmd=0x%4x seq=%v payType=%v userId=%v toId=%v itemCode=%v",
				head.Uid,
				head.Cmd,
				head.Seq,
				payType,
				userId,
				toId,
				itemCode)
			result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_INVALID_PARAM)
			rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
				Status: &ht_purchase_new.PurchaseNewHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("invalid param"),
				},
			}
			return false
		}
		// 自动续费的逻辑处理
		if itemCode == PRODUCT_OPEN_ENGLISH_YEAR_AUTO {
			orderType, utilTimeStampRenew, pruchasePeriodRenew, outTransactionId, err = CheckAutoRenewApple(userId, itemCode, appleOrder.GetReceiptData(), string(appleOrder.GetCurrency()), string(appleOrder.GetPayMoney()))
			infoLog.Printf("ProcCommitPurchaseOrder appstore orderType=%v utilTimeStamp=%v purchasePeriod=%v outTransactionId=%v err=%s", orderType, utilTimeStampRenew, pruchasePeriodRenew, outTransactionId, err)
			if err != nil {
				result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_INTERNAL_ERR)
				rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
					Status: &ht_purchase_new.PurchaseNewHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("internal error"),
					},
				}
				return false
			}
		} else {
			infoLog.Printf("ProcCommitPurchaseOrder appleOrder product_id=%s store_area=%s transaction_id=%s purchase_date_gmt=%s purchase_date=%s currency=%s pay_money=%s jailbroken=%v productCode error",
				appleOrder.GetProductId(),
				appleOrder.GetStoreArea(),
				appleOrder.GetTransactionId(),
				appleOrder.GetPurchaseDateGmt(),
				appleOrder.GetPurchaseDate(),
				appleOrder.GetCurrency(),
				appleOrder.GetPayMoney(),
				appleOrder.GetJailbroken())

			result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_INVALID_PARAM)
			rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
				Status: &ht_purchase_new.PurchaseNewHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("param error"),
				},
			}
			return false
		}

		historyRecord.OrderId = outTransactionId
		historyRecord.ProductId = string(appleOrder.GetProductId())
		historyRecord.Currency = string(appleOrder.GetCurrency())
		historyRecord.PayMoney = string(appleOrder.GetPayMoney())
		historyRecord.BuyTime = string(appleOrder.GetPurchaseDateGmt())
	default:
		infoLog.Printf("ProcCommitPurchaseOrder unkonw payType=%v userId=%v", payType, userId)
	}

	if orderType == ht_purchase_new.ORDER_TYPE_E_FAKE_ORDER {
		infoLog.Printf("ProcCommitPurchaseOrder userId=%v orderType=%v orderId=%s err=%s", userId, orderType, itemCode, err)
		result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_INVALID_ORDER)
		rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
			Status: &ht_purchase_new.PurchaseNewHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid order"),
			},
		}
		return false
	} else if orderType == ht_purchase_new.ORDER_TYPE_E_EXPIRED_ORDER {
		infoLog.Printf("ProcCommitPurchaseOrder userId=%v orderType=%v itemCode=%v err=%s", userId, orderType, itemCode, err)
		result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_EXPIRE_ORDER)
		rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
			Status: &ht_purchase_new.PurchaseNewHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("expire order"),
			},
		}
		return false
	}

	var bRepeatCommit bool
	if orderType == ht_purchase_new.ORDER_TYPE_E_REPEAT_ORDER {
		bRepeatCommit = true
		infoLog.Printf("ProcCommitPurchaseOrder payType=%v userId=%v toId=%v itemCode=%v repeatCommit=%v",
			payType,
			userId,
			toId,
			itemCode,
			bRepeatCommit)
	}
	var expiredBefore, expiredAfter uint64
	// step3: 根据订单详情修改会员过期天数
	tryCount := 0
	for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
		expiredBefore, expiredAfter, err = UpdatePurchaseExpiredTime(toId, itemCode, utilTimeStampRenew, pruchasePeriodRenew, bRepeatCommit)
		if err != nil {
			log.Printf("ProcCommitPurchaseOrder UpdatePurchaseExpiredTime failed uid=%v err=%v", toId, err)
			time.Sleep(5 * time.Second) // 休眠5秒再重试
			continue
		}
		break
	}
	if tryCount == 0 {
		attr := "gonewpur/update_vip_expired_time_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcCommitPurchaseOrder UpdatePurchaseExpiredTime failed uid=%v tryCount=%v", userId, tryCount)
		result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_INTERNAL_ERR)
		rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
			Status: &ht_purchase_new.PurchaseNewHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("internal error"),
			},
		}
		return false
	}

	// history purchase order
	historyRecord.UtilBefore = expiredBefore
	historyRecord.UtilAfter = expiredAfter
	historyRecord.ClientOrServer = ClientCommit // 0: client 1:server

	rspBody.CommitPurchaseOrderNewRspbody = &ht_purchase_new.CommitPurchaseOrderNewRspBody{
		Status: &ht_purchase_new.PurchaseNewHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("commit success"),
		},
		OrderType: orderType.Enum(),
		ExpireTs:  proto.Uint64(expiredAfter),
	}
	result = uint16(ht_purchase_new.PURCHASE_NEW_RET_CODE_RET_SUCCESS)
	bNeedCall = false
	SendRsp(c, head, rspBody, result)

	tryCount = 0
	for tryCount = TRYTIMES; tryCount > 0; tryCount-- {
		err = DbUtil.WritePurchaseHistory(historyRecord)
		if err != nil {
			infoLog.Printf("ProcCommitPurchaseOrder DbUtil.WritePurchaseHistory failed uid=%v err=%v", userId, err)
			time.Sleep(5 * time.Second) // 休眠5秒再重试
			continue
		}
		break
	}
	if tryCount == 0 {
		attr := "gonewpur/wirte_purchase_history_order_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcCommitPurchaseOrder DbUtil.WritePurchaseHistory failed uid=%v tryCount=%v", userId, tryCount)
	}
	procTime := packet.CalcProcessTime()
	if procTime > ProcSlowThreshold {
		attr := "gonewpur/commit_purchase_proc_slow"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("ProcCommitPurchaseOrder userId=%v itemCode=%v payType=%v procTime=%v", userId, itemCode, payType, procTime)
	}
	return true
}

func EnumProductCode(productId string) (productCode uint32) {
	if productId == "com.hellotalk.onemonth1" {
		productCode = PRODUCT_ONE_MONTH
	} else if productId == "com.hellotalk.onemonthauto" {
		productCode = PRODUCT_ONE_MONTHAUTO
	} else if productId == "com.hellotalk.onemonthsubscriptionplan" ||
		productId == "com.hellotalk.onemonthsubscriptionplan2" {
		productCode = PRODUCT_ONE_MONTH_SUB_PLAN
	} else if productId == "com.hellotalk.onemonth1auto" ||
		productId == "com.hellotalk.1monthautorenew" ||
		productId == "com.hellotalk.monthauto" {
		productCode = PRODUCT_ONE_MONTHAUTO
	} else if productId == "com.hellotalk.oneyear" ||
		productId == "com.hellotalk.oneyeardiscount" {
		productCode = PRODUCT_ONE_YEAR
	} else if productId == "com.hellotalk.3months" {
		productCode = PRODUCT_THREE_MONTH
	} else if productId == "com.hellotalk.Gift1M" {
		productCode = PRODUCT_GIFT_ONEMONTH
	} else if productId == "com.hellotalk.g1m" {
		productCode = PRODUCT_GIFT_ONEMONTH
	} else if productId == "com.hellotalk.Gift3M" {
		productCode = PRODUCT_GIFT_THREEMONTH
	} else if productId == "com.hellotalk.g3m" {
		productCode = PRODUCT_GIFT_THREEMONTH
	} else if productId == "com.hellotalk.Gift1Y" {
		productCode = PRODUCT_GIFT_ONEYEAR
	} else if productId == "com.hellotalk.g1y" {
		productCode = PRODUCT_GIFT_ONEYEAR
	} else if productId == "com.hellotalk.1morelang" ||
		productId == "com.hellotalk.1moreLang2" {
		productCode = PRODUCT_ONE_MULITLANG
	} else if productId == "com.hellotalk.yearauto" {
		productCode = PRODUCT_YEAR_AUTO
	} else if productId == "com.hellotalk.oneyearsubscriptionplan" ||
		productId == "com.hellotalk.oneyearsubscriptionplan2" ||
		productId == "com.hellotalk.yearauto2" {
		productCode = PRODUCT_ONE_YEAR_SUB_PLAN
	} else if productId == "com.hellotalk.lifetime" {
		productCode = PRODUCT_LIFT_TIME
	} else if productId == "com.hellotalk.lifetimeplan" {
		productCode = PRODUCT_LIFE_TIME_SUB_PLAN
	} else if productId == "com.hellotalk.onemonthsubscription" ||
		productId == "com.hellotalk.onemonthsubscription2" ||
		productId == "com.hellotalk.monthauto_freetrial" {
		productCode = PRODUCT_ONE_MONTH_SUB
	} else if productId == "com.hellotalk.oneyearsubscription" ||
		productId == "com.hellotalk.oneyearsubscription2" ||
		productId == "com.hellotalk.yearauto2_freetrial" {
		productCode = PRODUCT_ONE_YEAR_SUB
	} else if productId == "com.hellotalk.onemonthauto.b" ||
		productId == "com.hellotalk.super1monthauto" {
		productCode = PRODUCT_ONE_MONTH_AUTO_B
	} else if productId == "com.hellotalk.onemonthauto.c" {
		productCode = PRODUCT_ONE_MONTH_AUTO_C
	} else if productId == "com.hellotalk.yearauto.b" ||
		productId == "com.hellotalk.super1yearauto" {
		productCode = PRODUCT_YEAR_AUTO_B
	} else if productId == "com.hellotalk.yearauto.c" {
		productCode = PRODUCT_YEAR_AUTO_C
	} else if productId == "com.hellotalk.lifetime.b" ||
		productId == "com.hellotalk.super1lifetime" {
		productCode = PRODUCT_LIFE_TIME_B
	} else if productId == "com.hellotalk.lifetime.c" {
		productCode = PRODUCT_LIFE_TIME_C
	} else if productId == "com.hellotalk.openenglish1year" {
		productCode = PRODUCT_OPEN_ENGLISH_YEAR_AUTO
	}
	return productCode
}

func CheckAutoRenewApple(userId, itemCode uint32, receiptData []byte, currency, payMoney string) (orderType ht_purchase_new.ORDER_TYPE, utilTimeStamp, purchasePeriod uint64, outTransactionId string, err error) {
	if userId == 0 || itemCode == 0 || len(receiptData) == 0 {
		err = ErrInvalidParam
		infoLog.Printf("CheckAutoRenewApple userId=%v itemCode=%v receipts=%v currency=%s payMoney=%s", userId, itemCode, receiptData, currency, payMoney)
		return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
	}
	// base64Str := base64.StdEncoding.EncodeToString(receiptData)
	req := appstore.IAPRequest{
		ReceiptData: string(receiptData),
		Password:    PURCHASE_PWD,
	}
	resp := &appstore.IAPResponse{}
	err = appstoreClient.Verify(req, resp)
	if err != nil {
		infoLog.Printf("CheckAutoRenewApple userId=%v receipts=%v currency=%s payMoney=%s err=%s", userId, receiptData, currency, payMoney, err)
		return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
	}
	infoLog.Printf("CheckAutoRenewApple resp=%#v", resp)
	// copy the transaction
	receipt := resp.Receipt
	latestReceiptInfo := resp.LatestReceiptInfo
	if resp.Status == ExpireReceiptCode {
		latestReceiptInfo = resp.LatestExpireReceiptInfo
	}
	outTransactionId = string(latestReceiptInfo.TransactionID)
	verifyRspStr, err := GenAppStoreAutoRenewVerifyRspStr(resp)
	if err != nil {
		attr := "gonewpur/appstore_json_rsp_failed"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("CheckAutoRenewApple GenAppStoreAutoRenewVerifyRspStr failed rsp=%#v err=%s", resp, err)
	}
	if string(latestReceiptInfo.Bid) != util.HelloTalkBid ||
		string(latestReceiptInfo.ProductID) != util.ProductOpenEnglishOneYearAuto {
		// add static
		attr := "gonewpur/appstore_faker_order"
		libcomm.AttrAdd(attr, 1)
		infoLog.Printf("CheckAutoRenewApple faker order uid=%v bid=%s pid=%s", userId, latestReceiptInfo.Bid, latestReceiptInfo.ProductID)
		orderType = ht_purchase_new.ORDER_TYPE_E_FAKE_ORDER
		err = DbUtil.WriteAutoRenewProductRecordApple(
			userId,
			itemCode,
			E_STATE_INVALID,
			0,
			latestReceiptInfo.TransactionID,
			latestReceiptInfo.ProductID,
			latestReceiptInfo.OriginalPurchaseDate.OriginalPurchaseDate,
			latestReceiptInfo.OriginalTransactionID,
			latestReceiptInfo.PurchaseDate.PurchaseDate,
			latestReceiptInfo.PurchaseDateMS,
			latestReceiptInfo.ExpiresDateFormatted,
			latestReceiptInfo.ExpiresDate.ExpiresDate,
			resp.LatestReceipt,
			verifyRspStr,
			currency,
			payMoney)
		if err != nil {
			attr := "gonewpur/appstore_write_auto_renew_failed"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("CheckAutoRenewApple WriteAutoRenewRecordApple failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s err=%s",
				userId,
				resp.Status,
				latestReceiptInfo.TransactionID,
				latestReceiptInfo.PurchaseDate,
				err)
		}
		return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
	}

	// 判断验证结果
	if resp.Status == ExpireReceiptCode { // 续订过期
		orderType = ht_purchase_new.ORDER_TYPE_E_EXPIRED_ORDER
		err = DbUtil.WriteAutoRenewProductRecordApple(
			userId,
			itemCode,
			E_STATE_EXPIRED,
			0,
			latestReceiptInfo.TransactionID,
			latestReceiptInfo.ProductID,
			latestReceiptInfo.OriginalPurchaseDate.OriginalPurchaseDate,
			latestReceiptInfo.OriginalTransactionID,
			latestReceiptInfo.PurchaseDate.PurchaseDate,
			latestReceiptInfo.PurchaseDateMS,
			latestReceiptInfo.ExpiresDateFormatted,
			latestReceiptInfo.ExpiresDate.ExpiresDate,
			resp.LatestReceipt,
			verifyRspStr,
			currency,
			payMoney)
		if err != nil {
			mysqlerr, ok := err.(*mysql.MySQLError)
			if ok && mysqlerr.Number == 1062 {
				infoLog.Printf("CheckAutoRenewApple WriteAutoRenewRecordApple deuplicate order uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					userId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
			} else {
				attr := "gonewpur/appstore_write_auto_renew_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("CheckAutoRenewApple WriteAutoRenewRecordApple failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					userId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
				return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
			}
		}
		return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
	} else if resp.Status == VerifySuccessCode {
		receiptExpireTs, err := strconv.ParseUint(receipt.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("CheckAutoRenewApple strconv.ParseUint receipt.ExpiresDate=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				receipt.ExpiresDate,
				userId,
				receiptData,
				currency,
				payMoney,
				err)
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
		}

		latestExpireTs, err := strconv.ParseUint(latestReceiptInfo.ExpiresDate.ExpiresDate, 10, 64)
		if err != nil {
			infoLog.Printf("CheckAutoRenewApple strconv.ParseUint latestReceiptInfo.ExpiresDate=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				latestReceiptInfo.ExpiresDate,
				userId,
				receiptData,
				currency,
				payMoney,
				err)
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
		}

		latestPurchTs, err := strconv.ParseUint(latestReceiptInfo.PurchaseDateMS, 10, 64)
		if err != nil {
			infoLog.Printf("CheckAutoRenewApple strconv.ParseUint latestReceiptInfo.PurchaseDateMS=%s faield userId=%v receipts=%v currency=%s payMoney=%s err=%s",
				latestReceiptInfo.PurchaseDateMS,
				userId,
				receiptData,
				currency,
				payMoney,
				err)
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
		}
		if receiptExpireTs == latestExpireTs {
			// auto renew not change
			orderType = ht_purchase_new.ORDER_TYPE_E_NORMAL_ORDER
			utilTimeStamp = latestExpireTs / 1000 // chage ms to second
			purchasePeriod = (latestExpireTs - latestPurchTs) / 1000
			infoLog.Printf("check Renew Receipt is not change. uid=%v utilTimeStamp=%v purchasePeriod=%v expireDate=%s",
				userId,
				utilTimeStamp,
				purchasePeriod,
				latestReceiptInfo.ExpiresDateFormatted)
		} else {
			orderType = ht_purchase_new.ORDER_TYPE_E_NORMAL_ORDER
			utilTimeStamp = latestExpireTs / 1000 // change ms to second
			purchasePeriod = (latestExpireTs - latestPurchTs) / 1000
			infoLog.Printf("Renew is successfully. uid=%v utilTimeStamp=%v purchasePeriod=%v expireDate=%s",
				userId,
				utilTimeStamp,
				purchasePeriod,
				latestReceiptInfo.ExpiresDateFormatted)
		}
		err = DbUtil.WriteAutoRenewProductRecordApple(
			userId,
			itemCode,
			E_STATE_SUCC,
			0,
			latestReceiptInfo.TransactionID,
			latestReceiptInfo.ProductID,
			latestReceiptInfo.OriginalPurchaseDate.OriginalPurchaseDate,
			latestReceiptInfo.OriginalTransactionID,
			latestReceiptInfo.PurchaseDate.PurchaseDate,
			latestReceiptInfo.PurchaseDateMS,
			latestReceiptInfo.ExpiresDateFormatted,
			latestReceiptInfo.ExpiresDate.ExpiresDate,
			resp.LatestReceipt,
			verifyRspStr,
			currency,
			payMoney)
		if err != nil {
			mysqlerr, ok := err.(*mysql.MySQLError)
			if ok && mysqlerr.Number == 1062 {
				orderType = ht_purchase_new.ORDER_TYPE_E_REPEAT_ORDER
				infoLog.Printf("CheckAutoRenewApple Repeat AutoRenew deuplicate order uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					userId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
				lastUid, err := DbUtil.AppleStoreGetUserIdByRepeateCommitOrderId(string(latestReceiptInfo.TransactionID))
				if err == nil {
					if lastUid != userId {
						orderType = ht_purchase_new.ORDER_TYPE_E_FAKE_ORDER
						infoLog.Printf("CheckAutoRenewApple purchase uid=%v lastUid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s",
							userId,
							lastUid,
							resp.Status,
							latestReceiptInfo.TransactionID,
							latestReceiptInfo.PurchaseDate,
							verifyRspStr)
					}
					return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
				} else {
					infoLog.Printf("CheckAutoRenewApple DbUtil.AppleStoreGetUserIdByRepeateCommitOrderId uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
						userId,
						resp.Status,
						latestReceiptInfo.TransactionID,
						latestReceiptInfo.PurchaseDate,
						verifyRspStr,
						err)
					return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
				}
			} else {
				attr := "gonewpur/appstore_write_auto_renew_failed"
				libcomm.AttrAdd(attr, 1)
				infoLog.Printf("CheckAutoRenewApple WriteAutoRenewRecordApple failed uid=%v resp.Status=%v transationID=%s purchaseDate=%s verifyRspStr=%s err=%s",
					userId,
					resp.Status,
					latestReceiptInfo.TransactionID,
					latestReceiptInfo.PurchaseDate,
					verifyRspStr,
					err)
				return orderType, utilTimeStamp, purchasePeriod, outTransactionId, err
			}
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
		} else {
			return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
		}
	}
	return orderType, utilTimeStamp, purchasePeriod, outTransactionId, nil
}

func GenAppStoreAutoRenewVerifyRspStr(rsp *appstore.IAPResponse) (verifyRspStr string, err error) {
	if rsp == nil {
		err = ErrInvalidParam
		return verifyRspStr, err
	}
	rspReceipt := rsp.Receipt

	receipt := simplejson.New()
	receipt.Set("expires_date_formatted", rspReceipt.ExpiresDateFormatted)
	receipt.Set("original_purchase_date_pst", rspReceipt.OriginalPurchaseDatePST)
	receipt.Set("unique_identifier", rspReceipt.UniqueIdentifier)
	receipt.Set("original_transaction_id", rspReceipt.OriginalTransactionID)
	receipt.Set("expires_date", rspReceipt.ExpiresDate)
	receipt.Set("app_item_id", rspReceipt.AppItemID)
	receipt.Set("transaction_id", rspReceipt.TransactionID)
	receipt.Set("quantity", rspReceipt.Quantity)
	receipt.Set("expires_date_formatted_pst", rspReceipt.ExpiresDateFormattedPST)
	receipt.Set("product_id", rspReceipt.ProductID)
	receipt.Set("bvrs", rspReceipt.Bvrs)
	receipt.Set("unique_vendor_identifier", rspReceipt.UniqueVendorIdentifier)
	receipt.Set("web_order_line_item_id", rspReceipt.WebOrderLineItemID)
	receipt.Set("original_purchase_date_ms", rspReceipt.OriginalPurchaseDateMS)
	receipt.Set("version_external_identifier", rspReceipt.VersionExternalIdentifier)
	receipt.Set("bid", rspReceipt.Bid)
	receipt.Set("purchase_date_ms", rspReceipt.PurchaseDateMS)
	receipt.Set("purchase_date", rspReceipt.PurchaseDate)
	receipt.Set("purchase_date_pst", rspReceipt.PurchaseDatePST)
	receipt.Set("original_purchase_date", rspReceipt.OriginalPurchaseDate)
	receipt.Set("item_id", rspReceipt.ItemId)

	rspJsonObj := simplejson.New()
	rspJsonObj.Set("receipt", receipt)
	rspJsonObj.Set("status", rsp.Status)
	rspJsonObj.Set("latest_receipt", rsp.LatestReceipt)

	latestReceiptInfo := simplejson.New()
	if rsp.Status == ExpireReceiptCode {
		rspLatestReceiptInfo := rsp.LatestExpireReceiptInfo
		latestReceiptInfo.Set("original_purchase_date_pst", rspLatestReceiptInfo.OriginalPurchaseDatePST)
		latestReceiptInfo.Set("unique_identifier", rspLatestReceiptInfo.UniqueIdentifier)
		latestReceiptInfo.Set("original_transaction_id", rspLatestReceiptInfo.OriginalTransactionID)
		latestReceiptInfo.Set("expires_date", rspLatestReceiptInfo.ExpiresDate)
		latestReceiptInfo.Set("app_item_id", rspLatestReceiptInfo.AppItemID)
		latestReceiptInfo.Set("transaction_id", rspLatestReceiptInfo.TransactionID)
		latestReceiptInfo.Set("quantity", rspLatestReceiptInfo.Quantity)
		latestReceiptInfo.Set("product_id", rspLatestReceiptInfo.ProductID)
		latestReceiptInfo.Set("bvrs", rspLatestReceiptInfo.Bvrs)
		latestReceiptInfo.Set("bid", rspLatestReceiptInfo.Bid)
		latestReceiptInfo.Set("unique_vendor_identifier", rspLatestReceiptInfo.UniqueVendorIdentifier)
		latestReceiptInfo.Set("web_order_line_item_id", rspLatestReceiptInfo.WebOrderLineItemID)
		latestReceiptInfo.Set("original_purchase_date_ms", rspLatestReceiptInfo.OriginalPurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted", rspLatestReceiptInfo.ExpiresDateFormatted)
		latestReceiptInfo.Set("purchase_date", rspLatestReceiptInfo.PurchaseDate)
		latestReceiptInfo.Set("purchase_date_ms", rspLatestReceiptInfo.PurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted_pst", rspLatestReceiptInfo.ExpiresDateFormattedPST)
		latestReceiptInfo.Set("purchase_date_pst", rspLatestReceiptInfo.PurchaseDatePST)
		latestReceiptInfo.Set("original_purchase_date", rspLatestReceiptInfo.OriginalPurchaseDate)
		latestReceiptInfo.Set("item_id", rspLatestReceiptInfo.ItemId)
		rspJsonObj.Set("latest_expired_receipt_info", latestReceiptInfo)
	} else {
		rspLatestReceiptInfo := rsp.LatestReceiptInfo
		latestReceiptInfo.Set("original_purchase_date_pst", rspLatestReceiptInfo.OriginalPurchaseDatePST)
		latestReceiptInfo.Set("unique_identifier", rspLatestReceiptInfo.UniqueIdentifier)
		latestReceiptInfo.Set("original_transaction_id", rspLatestReceiptInfo.OriginalTransactionID)
		latestReceiptInfo.Set("expires_date", rspLatestReceiptInfo.ExpiresDate)
		latestReceiptInfo.Set("app_item_id", rspLatestReceiptInfo.AppItemID)
		latestReceiptInfo.Set("transaction_id", rspLatestReceiptInfo.TransactionID)
		latestReceiptInfo.Set("quantity", rspLatestReceiptInfo.Quantity)
		latestReceiptInfo.Set("product_id", rspLatestReceiptInfo.ProductID)
		latestReceiptInfo.Set("bvrs", rspLatestReceiptInfo.Bvrs)
		latestReceiptInfo.Set("bid", rspLatestReceiptInfo.Bid)
		latestReceiptInfo.Set("unique_vendor_identifier", rspLatestReceiptInfo.UniqueVendorIdentifier)
		latestReceiptInfo.Set("web_order_line_item_id", rspLatestReceiptInfo.WebOrderLineItemID)
		latestReceiptInfo.Set("original_purchase_date_ms", rspLatestReceiptInfo.OriginalPurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted", rspLatestReceiptInfo.ExpiresDateFormatted)
		latestReceiptInfo.Set("purchase_date", rspLatestReceiptInfo.PurchaseDate)
		latestReceiptInfo.Set("purchase_date_ms", rspLatestReceiptInfo.PurchaseDateMS)
		latestReceiptInfo.Set("expires_date_formatted_pst", rspLatestReceiptInfo.ExpiresDateFormattedPST)
		latestReceiptInfo.Set("purchase_date_pst", rspLatestReceiptInfo.PurchaseDatePST)
		latestReceiptInfo.Set("original_purchase_date", rspLatestReceiptInfo.OriginalPurchaseDate)
		latestReceiptInfo.Set("item_id", rspLatestReceiptInfo.ItemId)
		rspJsonObj.Set("latest_receipt_info", latestReceiptInfo)
	}

	verifyRspSlic, err := rspJsonObj.Encode()
	verifyRspStr = string(verifyRspSlic)
	return verifyRspStr, err
}

// 根据订单信息修改会员的过期天数
func UpdatePurchaseExpiredTime(userId, itermCode uint32, utilTimestamp uint64, purchPeriod uint64, repeatCommit bool) (expiredBefore, expiredAfter uint64, err error) {
	// 购买会员成功更新会员天数
	infoLog.Printf("UpdatePurchaseExpiredTime userId=%v itermCode=%v utilTimestamp=%v purchPeroid=%v repeatcommit=%v",
		userId,
		itermCode,
		utilTimestamp,
		purchPeriod,
		repeatCommit)

	lastIterm, expiredTS, err := DbUtil.GetUserPurchaseInfo(userId)
	if err != nil {
		infoLog.Printf("UpdatePurchaseExpiredTime exec GetUserVIPInfo failed userUid=%v", userId)
		return expiredBefore, expiredAfter, err
	}
	infoLog.Printf("UpdatePurchaseExpiredTime lastIterm=%v expiredTS=%v", lastIterm, expiredTS)
	expiredBefore = expiredTS

	if repeatCommit == true {
		expiredAfter = expiredTS
		return expiredBefore, expiredAfter, nil
	}

	timeStampNow := uint64(time.Now().Unix())

	// 更改到期时间戳
	countSecond := CountPostponeSeconds(itermCode)
	infoLog.Printf("UpdatePurchaseExpiredTime countSecond=%v purchPeriod=%v", countSecond, purchPeriod)
	if expiredTS < timeStampNow {
		// 已经过期或是第一次购买的 月费试用会员和年费试用会员都是自动续费类型
		if itermCode == PRODUCT_OPEN_ENGLISH_YEAR_AUTO {
			// 自动续费的以续费过期时间为准
			expiredTS = utilTimestamp
		} else {
			// 其他的认为在当前时间往后延长
			expiredTS = timeStampNow + countSecond
		}
	} else {
		// 还没有过期则在原来的到期时间戳上延长
		expiredTS += countSecond
	}

	infoLog.Printf("UpdatePurchaseExpiredTime uid=%v lastIterm=%v expiredTS=%v",
		userId,
		lastIterm,
		expiredTS)
	err = DbUtil.UpdateUserPurchaseInfo(userId, itermCode, expiredTS)
	if err != nil {
		infoLog.Printf("UpdatePurchaseExpiredTime UpdateUserVIPInfo failed lastIterm=%v expiredTS=%v err=%s",
			lastIterm,
			expiredTS,
			err)
	} else {
		infoLog.Printf("UpdatePurchaseExpiredTime UpdateUserVIPInfo ok lastIterm=%v expiredTS=%v", lastIterm, expiredTS)
	}

	// 更新成功返回
	expiredAfter = expiredTS
	return expiredBefore, expiredAfter, nil
}

func TimeStampToSqlTime(ts int64) (outTime string) {
	tm := time.Unix(ts, 0)
	tm = tm.UTC()
	outTime = fmt.Sprintf("%4d-%02d-%02d %02d:%02d:%02d", tm.Year(), tm.Month(), tm.Day(), tm.Hour(), tm.Minute(), tm.Second())
	return outTime
}

func CountPostponeSeconds(productCode uint32) (outSecond uint64) {
	if productCode == PRODUCT_ONE_MONTH ||
		productCode == PRODUCT_ONE_MONTHAUTO ||
		productCode == PRODUCT_GIFT_ONEMONTH ||
		productCode == PRODUCT_ONE_MONTH_SUB_PLAN ||
		productCode == PRODUCT_ONE_MONTH_AUTO_B ||
		productCode == PRODUCT_ONE_MONTH_AUTO_C {
		outSecond = 31 * 24 * 3600
	} else if productCode == PRODUCT_THREE_MONTH ||
		productCode == PRODUCT_GIFT_THREEMONTH {
		outSecond = 92 * 24 * 3600
	} else if productCode == PRODUCT_ONE_YEAR ||
		productCode == PRODUCT_GIFT_ONEYEAR ||
		productCode == PRODUCT_YEAR_AUTO ||
		productCode == PRODUCT_ONE_YEAR_SUB_PLAN ||
		productCode == PRODUCT_YEAR_AUTO_B ||
		productCode == PRODUCT_YEAR_AUTO_C ||
		productCode == PRODUCT_OPEN_ENGLISH_YEAR_AUTO {
		outSecond = 365 * 24 * 3600
	} else if productCode == PRODUCT_LIFT_TIME ||
		productCode == PRODUCT_LIFE_TIME_SUB_PLAN ||
		productCode == PRODUCT_LIFE_TIME_B ||
		productCode == PRODUCT_LIFE_TIME_C {
		// 终生会员做50年处理
		outSecond = 50 * 365 * 24 * 3600
	} else if productCode == PRODUCT_ONE_MONTH_SUB {
		outSecond = 7 * 24 * 3600 // 月会员试用 返回的天数UpdatePurchaseExpiredTime没有使用
	} else if productCode == PRODUCT_ONE_YEAR_SUB {
		outSecond = 31 * 24 * 3600 // 年会员试用 返回的天数UpdatePurchaseExpiredTime没有使用
	}
	return outSecond
}

func SendRsp(c *gotcp.Conn, reqHead *common.HeadV2, resp *ht_purchase_new.PurchaseNewRspBody, ret uint16) bool {
	head := new(common.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}
	outBuf, err := proto.Marshal(resp)
	if err != nil {
		infoLog.Printf("SendRsp Failed to proto.Marshal err=%s", err)
		return false
	}

	head.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	head.Len = common.HeadV2Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, head.Len)
	sendBuf[0] = common.HTV2MagicBegin
	err = common.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		infoLog.Printf("SendRsp SerialHeadV2ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV2Len:], outBuf)
	sendBuf[head.Len-1] = common.HTV2MagicEnd
	infoLog.Printf("SendRsp ret=%v, len=%v\n", ret, head.Len)
	respPacket := common.NewHeadV2Packet(sendBuf)
	c.AsyncWritePacket(respPacket, time.Second)
	return true
}

func SendRspPacket(c *gotcp.Conn, packet *common.HeadV2Packet) bool {
	if c == nil || packet == nil {
		infoLog.Printf("SendRspPacket err nil param c=%v packet=%v", c, packet)
		return false
	}
	c.AsyncWritePacket(packet, time.Second)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Printf("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// read appstore config
	productEnv := cfg.Section("APPSTORE").Key("product").MustBool(true)
	appTimeOut := cfg.Section("APPSTORE").Key("time_out").MustInt(2)
	infoLog.Printf("APPSTORE product=%v time_out=%v", productEnv, appTimeOut)
	appstoreConfig = appstore.Config{
		IsProduction: productEnv,
		TimeOut:      time.Second * time.Duration(appTimeOut),
	}
	appstoreClient = appstore.NewWithConfig(appstoreConfig)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	DbUtil = util.NewDbUtil(db, infoLog)
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
