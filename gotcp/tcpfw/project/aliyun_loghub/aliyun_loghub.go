package main

import (
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	sls "github.com/aliyun/aliyun-log-go-sdk"
	"github.com/bitly/go-simplejson"
	"github.com/gogo/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
)

const (
	// 业务流程
	CMD_LOGIN       = "login"
	CMD_FIRST_LOGIN = "first_login"
	CMD_FORCE_LOGIN = "force_login"
	CMD_RECONNECT   = "reconnect"
	CMD_VOIP        = "voip"
	CMD_VIDEO       = "video"
	CMD_WNS_CONNECT = "wns_connect"
	// IM请求
	CMD_0X1001        = "0x1001"
	CMD_0X1003        = "0x1003"
	CMD_0X1005        = "0x1005"
	CMD_0X4019        = "0x4019"
	CMD_0X401D        = "0x401d"
	CMD_0X401F        = "0x401f"
	CMD_0X4021        = "0x4021"
	CMD_0X4023        = "0x4023"
	CMD_0X411D        = "0x411d"
	CMD_WNS_CONN_STAT = "wns_connect_status"
	CMD_CONN_VOIP_SDK = "conn_voip_sdk"
	//客户端事件
	CMD_POPBUY                    = "pop_buy"
	CMD_CLICKBUY                  = "click_buy"
	CMD_USE_HELLO_TALK_TOTAL      = "use_hellotalk_total"
	CMD_USE_HELLO_TALK_TAB        = "use_hellotalk_tab"
	CMD_USE_SEARCH_TAB            = "use_search_tab"
	CMD_USE_MOMENT_TAB            = "use_moment_tab"
	CMD_USE_MOMENT_DEFAULT_TAB    = "use_moment_default_tab"
	CMD_USE_MOMENT_LEARN_TAB      = "use_moment_learn_tab"
	CMD_USE_MOMENT_FOLLOWING_TAB  = "use_moment_following_tab"
	CMD_USE_MOMENT_CLASSMATES_TAB = "use_moment_classmates_tab"
	CMD_USE_MOMENT_DETAIL         = "use_moment_detail"
	CMD_USE_OTHER_PROFILE_TAB     = "use_other_profile_tab"
	CMD_USE_SELF_PROFILE_TAB      = "use_self_profile_tab"

	//开发需求
	CMD_DOWN_CHAT_PIC     = "down_chat_pic"
	CMD_UP_CHAT_PIC       = "up_chat_pic"
	CMD_DOWN_MOMENT_PIC   = "down_moment_pic"
	CMD_UP_MOMENT_PIC     = "up_moment_pic"
	CMD_DOWN_CHAT_VOICE   = "down_chat_voice"
	CMD_UP_CHAT_VOICE     = "up_chat_voice"
	CMD_DOWN_SELF_VOICE   = "down_self_voice"
	CMD_UP_SELF_VOICE     = "up_self_voice"
	CMD_DOWN_MOMENT_VOICE = "down_moment_voice"
	CMD_UP_MOMENT_VOICE   = "up_moment_voice"
	CMD_DOWN_VIDEO        = "down_video"
	CMD_UP_VIDEO          = "up_video"
	CMD_DOWN_PORTRAIT     = "down_portrait"
	CMD_UP_PORTRAIT       = "up_portrait"
	CMD_REG1              = "reg1"
	CMD_REG2              = "reg2"
	CMD_TRANSLATE         = "translate"
	CMD_SOUNDMARK         = "soundmark"
	CMD_SOUND2TEXT        = "sound2text"
	CMD_POST_MOMENT       = "post_moment"
	CMD_MOMENT_DOWN       = "moment_down"
	CMD_MOMENT_UP         = "moment_up"
	CMD_GET_PUSH_TOKEN    = "get_push_token"

	LOGSTASH_QUEUE = "client_log"
	LOGHUB_VERSION = "2"
)

const (
	DAY_SECOND  = 86400
	RETRY_TIMES = 3
)

type Callback struct{}

var (
	ErrInitFailed = errors.New("init failed")
	ErrInputParam = errors.New("error input param")
)

var (
	infoLog       *log.Logger
	logStoreName  string
	logStore      *sls.LogStore
	project       *sls.LogProject
	logSource     string
	logGroupChane chan *sls.LogGroup
	closeChan     chan struct{} // close chanel
)

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {

	headV2Packet := p.(*common.HeadV2Packet)
	// infoLog.Printf("OnMessage:[%v] [%v]\n", headV2Packet.GetLength(), string(headV2Packet.GetBody()))
	head, err := headV2Packet.GetHead()
	// infoLog.Println("OnMessage head =", *head)
	SendResp(c, head, 0)
	_, err = headV2Packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Println("Invalid packet", err)
		return false
	}
	go ProcData(headV2Packet.GetBody())
	return true
}

func SendResp(c *gotcp.Conn, reqHead *common.HeadV2, ret uint16) bool {
	head := new(common.HeadV2)
	head.Version = reqHead.Version
	head.Cmd = reqHead.Cmd
	head.Seq = reqHead.Seq
	head.Uid = reqHead.Uid
	head.Len = common.EmptyPacktV2Len
	head.Ret = ret
	head.SysType = reqHead.SysType
	head.Echo[0] = reqHead.Echo[0]
	head.Echo[1] = reqHead.Echo[1]
	head.Echo[2] = reqHead.Echo[2]
	head.Echo[3] = reqHead.Echo[3]
	head.Echo[4] = reqHead.Echo[4]
	head.Echo[5] = reqHead.Echo[5]
	head.Echo[6] = reqHead.Echo[6]
	head.Echo[7] = reqHead.Echo[7]

	payLoad := []byte("acho hi")
	head.Len = uint32(common.PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = common.HTV2MagicBegin
	err := common.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV2ToSlice failed")
		return false
	}
	copy(buf[common.PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = common.HTV2MagicEnd
	resp := common.NewHeadV2Packet(buf)
	c.AsyncWritePacket(resp, time.Second)
	return true
}

func ProcData(bodyData []byte) bool {
	rootObj, err := simplejson.NewJson(bodyData)
	if err != nil {
		infoLog.Println("simplejson new packet error", err)
		return false
	}

	var uid string
	uid, err = rootObj.Get("userid").String()
	if err != nil {
		iUid := rootObj.Get("userid").MustInt()
		uid = strconv.Itoa(iUid)
	}
	terminal, err := rootObj.Get("terminaltype").String()
	if err != nil {
		iTerminal := rootObj.Get("terminaltype").MustInt()
		terminal = strconv.Itoa(iTerminal)
	}

	version := rootObj.Get("version").MustString()
	clientIp := rootObj.Get("client_ip").MustString()
	v := rootObj.Get("data").MustArray()
	count := len(v)
	logs := []*sls.Log{}
	content := []*sls.LogContent{}
	for i := 0; i < count; i++ {
		vv := rootObj.Get("data").GetIndex(i).MustArray()
		vvLen := len(vv)
		for j := 0; j < vvLen; j++ {
			subValue := rootObj.Get("data").GetIndex(i).GetIndex(j)
			formatTs, err := subValue.Get("ts").String()
			if err != nil {
				// 客户端上传ts是毫秒 64位
				tempTs := subValue.Get("ts").MustInt64(time.Now().Unix() * 1000)
				// 将毫秒转换成秒
				formatTs = fmt.Sprintf("%v", tempTs)
				// infoLog.Printf("format ts=%v to formatTs=%s", tempTs, formatTs)
				// strSubVale, err := subValue.MarshalJSON()
				// if err == nil {
				// 	infoLog.Printf("format ts=%v to formatTs=%s subValue=%s", tempTs, formatTs, strSubVale)
				// } else {
				// 	infoLog.Printf("format ts=%v to formatTs=%s", tempTs, formatTs)
				// }

			}

			u64Ts, err := strconv.ParseUint(formatTs, 10, 64)
			if err != nil {
				infoLog.Printf("strconv.ParseUint formatTs=%s faield bodyData=%s", formatTs, bodyData)
				continue
			}
			// change second to micro second
			nowMicSecond := uint64(time.Now().Unix() * 1000)
			if u64Ts == 0 || (u64Ts+uint64(DAY_SECOND*60*1000) < nowMicSecond) {
				//infoLog.Printf("time is illegal formats=%s u64Ts=%v nowMicSecond=%v", formatTs, u64Ts, nowMicSecond)
				//strSubVale, err := subValue.MarshalJSON()
				//if err == nil {
				//	infoLog.Printf("time is illegal formats=%s u64Ts=%v nowMicSecond=%v subValue=%s", formatTs, u64Ts, nowMicSecond, strSubVale)
				//} else {
				infoLog.Printf("time is illegal formats=%s u64Ts=%v nowMicSecond=%v bodyData=%s", formatTs, u64Ts, nowMicSecond, bodyData)
				//}
				sName := "loghub/loghub_recv_illegal_ts"
				libcomm.AttrAdd(sName, 1)
				// continue
				formatTs = fmt.Sprintf("%v", nowMicSecond)
			} else if u64Ts > nowMicSecond {
				formatTs = fmt.Sprintf("%v", nowMicSecond)
			}
			// set formatTs
			subValue.Set("ts", formatTs)

			var sPrevCmd, sPrevTs, sPrevCost, sPrevRet, sPrevRetry string
			outObj := simplejson.New()
			cmd := subValue.Get("cmd").MustString()

			//add monitor
			sName := "loghub/loghub_recv_req_" + cmd
			libcomm.AttrAdd(sName, 1)
			if subValue.Get("ret").MustString() != "" && subValue.Get("ret").MustString() != "0" {
				sName = "loghub/loghub_recv_req_" + cmd + "_ret_err"
				libcomm.AttrAdd(sName, 1)
			}
			if subValue.Get("wns_code").MustString() != "" && subValue.Get("wns_code").MustString() != "0" {
				sName = "loghub/loghub_recv_req_" + cmd + "_wns_err"
				libcomm.AttrAdd(sName, 1)
			}
			if subValue.Get("wns_code").MustString() == "7" {
				sName = "loghub/loghub_recv_req_" + cmd + "_wns_err_7"
				libcomm.AttrAdd(sName, 1)
			}

			if (cmd == CMD_LOGIN) ||
				(cmd == CMD_FORCE_LOGIN) ||
				(cmd == CMD_FIRST_LOGIN) ||
				(cmd == CMD_RECONNECT) ||
				(cmd == CMD_VOIP) ||
				(cmd == CMD_VIDEO) ||
				(cmd == CMD_WNS_CONNECT) {
				outObj.Set("ret", subValue.Get("ret").MustString())
				outObj.Set("cost", subValue.Get("cost").MustString())
				outObj.Set("ts", subValue.Get("ts").MustString())
				outObj.Set("cmd", subValue.Get("cmd").MustString())
				outObj.Set("nt", subValue.Get("nt").MustString())
			} else if (cmd == CMD_0X1001) ||
				(cmd == CMD_0X1003) ||
				(cmd == CMD_0X1005) ||
				(cmd == CMD_WNS_CONN_STAT) {
				outObj.Set("ret", subValue.Get("ret").MustString())
				outObj.Set("cost", subValue.Get("cost").MustString())
				outObj.Set("ts", subValue.Get("ts").MustString())
				outObj.Set("cmd", subValue.Get("cmd").MustString())
				outObj.Set("nt", subValue.Get("nt").MustString())
				outObj.Set("retry", subValue.Get("retry").MustString())
				outObj.Set("wns_code", subValue.Get("wns_code").MustString())
				outObj.Set("wns_ocip", subValue.Get("wns_ocip").MustString())
			} else if (cmd == CMD_0X4019) ||
				(cmd == CMD_0X401D) ||
				(cmd == CMD_0X401F) ||
				(cmd == CMD_0X4021) ||
				(cmd == CMD_0X4023) ||
				(cmd == CMD_0X411D) ||
				(cmd == CMD_CONN_VOIP_SDK) {
				outObj.Set("ret", subValue.Get("ret").MustString())
				outObj.Set("cost", subValue.Get("cost").MustString())
				outObj.Set("ts", subValue.Get("ts").MustString())
				outObj.Set("cmd", subValue.Get("cmd").MustString())
				outObj.Set("nt", subValue.Get("nt").MustString())
				outObj.Set("retry", subValue.Get("retry").MustString())
				outObj.Set("wns_code", subValue.Get("wns_code").MustString())
				outObj.Set("wns_ocip", subValue.Get("wns_ocip").MustString())
				outObj.Set("roomid", subValue.Get("roomid").MustString())
			} else if (cmd == CMD_POPBUY) || (cmd == CMD_CLICKBUY) {
				outObj.Set("ts", subValue.Get("ts").MustString())
				outObj.Set("cmd", subValue.Get("cmd").MustString())
				outObj.Set("pos", subValue.Get("pos").MustString())
			} else if (cmd == CMD_USE_HELLO_TALK_TOTAL) ||
				(cmd == CMD_USE_HELLO_TALK_TAB) ||
				(cmd == CMD_USE_SEARCH_TAB) ||
				(cmd == CMD_USE_MOMENT_TAB) ||
				(cmd == CMD_USE_MOMENT_DEFAULT_TAB) ||
				(cmd == CMD_USE_MOMENT_LEARN_TAB) ||
				(cmd == CMD_USE_MOMENT_FOLLOWING_TAB) ||
				(cmd == CMD_USE_MOMENT_CLASSMATES_TAB) ||
				(cmd == CMD_USE_MOMENT_DETAIL) ||
				(cmd == CMD_USE_OTHER_PROFILE_TAB) ||
				(cmd == CMD_USE_SELF_PROFILE_TAB) {
				outObj.Set("ts", subValue.Get("ts").MustString())
				outObj.Set("cmd", subValue.Get("cmd").MustString())
				outObj.Set("cost", subValue.Get("cost").MustString())
				strCost := subValue.Get("cost").MustString()
				if u64Cost, err := strconv.ParseUint(strCost, 10, 64); err == nil {
					outObj.Set("cost_u64", u64Cost)
				} else {
					outObj.Set("cost_u64", 0)
					//infoLog.Printf("strconv.ParseUint strCost=%s failed err=%s", strCost, err)
				}
			} else if (cmd == CMD_DOWN_CHAT_PIC) ||
				(cmd == CMD_UP_CHAT_PIC) ||
				(cmd == CMD_DOWN_MOMENT_PIC) ||
				(cmd == CMD_UP_MOMENT_PIC) ||
				(cmd == CMD_DOWN_CHAT_VOICE) ||
				(cmd == CMD_UP_CHAT_VOICE) ||
				(cmd == CMD_DOWN_SELF_VOICE) ||
				(cmd == CMD_UP_SELF_VOICE) ||
				(cmd == CMD_DOWN_MOMENT_VOICE) ||
				(cmd == CMD_UP_MOMENT_VOICE) ||
				(cmd == CMD_DOWN_VIDEO) ||
				(cmd == CMD_UP_VIDEO) ||
				(cmd == CMD_DOWN_PORTRAIT) ||
				(cmd == CMD_UP_PORTRAIT) ||
				(cmd == CMD_REG1) ||
				(cmd == CMD_REG2) ||
				(cmd == CMD_TRANSLATE) ||
				(cmd == CMD_SOUNDMARK) ||
				(cmd == CMD_SOUND2TEXT) ||
				(cmd == CMD_POST_MOMENT) ||
				(cmd == CMD_MOMENT_DOWN) ||
				(cmd == CMD_MOMENT_UP) ||
				(cmd == CMD_GET_PUSH_TOKEN) {
				outObj.Set("ret", subValue.Get("ret").MustString())
				strCost := subValue.Get("cost").MustString()
				if u64Cost, err := strconv.ParseUint(strCost, 10, 64); err == nil {
					outObj.Set("cost_u64", u64Cost)
				} else {
					outObj.Set("cost_u64", 0)
					//infoLog.Printf("strconv.ParseUint strCost=%s failed err=%s", strCost, err)
				}
				outObj.Set("cost", strCost)
				outObj.Set("ts", subValue.Get("ts").MustString())
				outObj.Set("cmd", subValue.Get("cmd").MustString())
				outObj.Set("nt", subValue.Get("nt").MustString())
				outObj.Set("retry", subValue.Get("retry").MustString())
				outObj.Set("http_code", subValue.Get("http_code").MustString())
				outObj.Set("err_msg", subValue.Get("err_msg").MustString())
			} else {
				// infoLog.Println("Unknow cmd=", cmd)
				continue
			}

			msec := transferMsec(outObj.Get("ts").MustString())
			outObj.Set("userid", uid)
			outObj.Set("version", version)
			outObj.Set("terminaltype", terminal)
			outObj.Set("client_ip", clientIp)
			outObj.Set("@version", LOGHUB_VERSION)
			outObj.Set("@timestamp", msec)

			sPacket, err := outObj.MarshalJSON()
			if err != nil {
				infoLog.Println("MarshalJSON failed outObj=", outObj)
				continue
			}
			content = append(content,
				&sls.LogContent{
					Key:   proto.String(fmt.Sprintf("col_%v", time.Now().Nanosecond())),
					Value: proto.String(string(sPacket)),
				})
			// else {
			// infoLog.Printf("redis exec RPUSH ret=%v", r)
			// }

			// infoLog.Printf("Uid=%v Record=[j:%d:%s]", uid, j, sPacket)
			/*生成一条记录, 只有时间，类型，耗时和前一条不同*/
			if j >= 2 {
				newRecord := simplejson.New()
				if cmd != sPrevCmd {
					sNewCmd := sPrevCmd + "-" + cmd
					iPrevTs, err := strconv.ParseUint(sPrevTs, 10, 64)
					if err != nil {
						infoLog.Printf("strconv.ParseUint failed prevTs=%s", sPrevTs)
						continue
					}

					iPrevCost, err := strconv.ParseUint(sPrevCost, 10, 64)
					if err != nil {
						infoLog.Printf("strconv.ParseUint failed sPrevCost=%s", sPrevCost)
						continue
					}

					iCurTs, err := strconv.ParseUint(subValue.Get("ts").MustString(), 10, 64)
					if err != nil {
						infoLog.Printf("strconv.ParseUint failed curTs=%s", subValue.Get("ts").MustString())
						continue
					}

					sNewTs := strconv.FormatUint(iPrevTs+iPrevCost, 10)
					sCurCost := strconv.FormatUint(iCurTs-(iPrevTs+iPrevCost), 10)
					newRecord.Set("cmd", sNewCmd)
					newRecord.Set("ts", sNewTs)
					newRecord.Set("cost", sCurCost)

					sNewPacket, err := newRecord.MarshalJSON()
					if err != nil {
						infoLog.Println("MarshalJSON failed newRecord=", newRecord)
						continue
					}

					content = append(content,
						&sls.LogContent{
							Key:   proto.String(fmt.Sprintf("col_%v", time.Now().Nanosecond())),
							Value: proto.String(string(sNewPacket)),
						})
					infoLog.Printf("Uid|%v|Record_add[j:%d:%s]", uid, j, sNewPacket)
				} else {
					if strings.EqualFold(subValue.Get("ret").MustString(), "0") && strings.EqualFold(sPrevRet, "0") {
						infoLog.Printf("Uid|%v|Fatal erorr!Please check the ret field!", uid)
					}
					if strings.EqualFold(subValue.Get("retry").MustString(), sPrevRetry) {
						infoLog.Printf("Uid|%v|Fatal erorr!Please check the retry field!", uid)
					}
					curRetry, _ := strconv.Atoi(subValue.Get("retry").MustString())
					prevRetry, _ := strconv.Atoi(sPrevRetry)
					if curRetry+1 < prevRetry {
						infoLog.Printf("Uid|%v|Fatal erorr!Please check the retry field!front[%s], rear[%s]", uid, subValue.Get("retry").MustString(), sPrevRetry)
					}
				}
			}

			sPrevCmd = cmd
			sPrevTs = subValue.Get("ts").MustString()
			sPrevCost = subValue.Get("cost").MustString()
			sPrevRet = subValue.Get("ret").MustString()
			sPrevRetry = subValue.Get("retry").MustString()
		}
	}
	log := &sls.Log{
		Time:     proto.Uint32(uint32(time.Now().Unix())),
		Contents: content,
	}
	logs = append(logs, log)
	loggroup := &sls.LogGroup{
		Topic:  proto.String(uid),
		Source: proto.String(logSource),
		Logs:   logs,
	}
	select {
	case logGroupChane <- loggroup:
		// infoLog.Printf("ProcData message uid=%s timestamp=%v put into chan succ", uid, time.Now().Unix())
	case <-closeChan: // 进程退出
		infoLog.Printf("ProcData gorutine exit")
		return false
	}
	return true
}

func transferMsec(timeStr string) (str string) {
	u, err := strconv.ParseUint(timeStr, 10, 64)
	if err != nil {
		infoLog.Printf("transferMsec strconv failed time=%s", timeStr)
		return str
	}

	msec := u - (u/1000)*1000
	nsec := msec * 1000 * 1000
	sec := (u - msec) / 1000
	return time.Unix(int64(sec), int64(nsec)).Format(time.RFC3339)
}

func MessageHandleLoop(index int) {
	defer func() {
		recover()
	}()
	infoLog.Printf("MessageHandleLoop index=%v", index)
	for {
		select {
		case logTask := <-logGroupChane:
			for retry_times := 0; retry_times < RETRY_TIMES; retry_times++ {
				err := logStore.PutLogs(logTask)
				if err == nil {
					// infoLog.Printf("MessageHandleLoop success, retry=%v", retry_times)
					break
				} else {
					infoLog.Printf("MessageHandleLoop fail, retry=%v, err=%s", retry_times, err)
					//handle exception here, you can add retryable erorrCode, set appropriate put_retry
					if strings.Contains(err.Error(), sls.WRITE_QUOTA_EXCEED) || strings.Contains(err.Error(), sls.PROJECT_QUOTA_EXCEED) || strings.Contains(err.Error(), sls.SHARD_WRITE_QUOTA_EXCEED) {
						//mayby you should split shard
						time.Sleep(1000 * time.Millisecond)
					} else if strings.Contains(err.Error(), sls.INTERNAL_SERVER_ERROR) || strings.Contains(err.Error(), sls.SERVER_BUSY) {
						time.Sleep(200 * time.Millisecond)
					}
				}
			}
		case <-closeChan:
			return
		}
	}
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func InitAliYunLogStore(project *sls.LogProject, logStoreName string) (logStore *sls.LogStore, err error) {
	if project == nil || logStoreName == "" {
		infoLog.Printf("InitAliYunLogStore param error project=%#v logStoreName=%s", *project, logStoreName)
		err = ErrInputParam
		return nil, err
	}

	for retry_times := 0; ; retry_times++ {
		if retry_times > RETRY_TIMES {
			err = ErrInitFailed
			return nil, err
		}
		logStore, err = project.GetLogStore(logStoreName)
		if err != nil {
			infoLog.Printf("InitAliYunLogStore GetLogStore fail, retry=%v, err=%s", retry_times, err)
			if strings.Contains(err.Error(), sls.PROJECT_NOT_EXIST) {
				return nil, err
			} else if strings.Contains(err.Error(), sls.LOGSTORE_NOT_EXIST) {
				err = project.CreateLogStore(logStoreName, 1, 2)
				if err != nil {
					infoLog.Printf("InitAliYunLogStore CreateLogStore fail logStoreName=%s err=%s", logStoreName, err)
				} else {
					infoLog.Println("InitAliYunLogStore CreateLogStore success logStoreName=%s", logStoreName)
				}
			}
		} else {
			infoLog.Printf("InitAliYunLogStore GetLogStore success, retry=%v, name=%s, ttl=%v, shardCount=%v, createTime=%v, lastModifyTime=%v",
				retry_times,
				logStore.Name,
				logStore.TTL,
				logStore.ShardCount,
				logStore.CreateTime,
				logStore.LastModifyTime)
			return logStore, nil
		}
		time.Sleep(200 * time.Millisecond)
	}
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// get store name
	logStoreName = cfg.Section("ALIYUN_LOG_SERVICE").Key("log_store").MustString("test-logstore")
	logSource = cfg.Section("ALIYUN_LOG_SERVICE").Key("log_source").MustString("127.0.0.1")
	projectName := cfg.Section("ALIYUN_LOG_SERVICE").Key("project_name").MustString("hellotalk")
	endPoint := cfg.Section("ALIYUN_LOG_SERVICE").Key("end_point").MustString("cn-hongkong.log.aliyuncs.com")
	accessKeyID := cfg.Section("ALIYUN_LOG_SERVICE").Key("access_key_id").MustString("")
	accessKeySecret := cfg.Section("ALIYUN_LOG_SERVICE").Key("access_key_secret").MustString("")
	project = &sls.LogProject{
		Name:            projectName,
		Endpoint:        endPoint,
		AccessKeyID:     accessKeyID,
		AccessKeySecret: accessKeySecret,
	}

	logStore, err = InitAliYunLogStore(project, logStoreName)
	checkError(err)

	// create consumer
	consumerCount := cfg.Section("CONSUMER").Key("count").MustInt(50)
	logGroupChane = make(chan *sls.LogGroup) //创建长度为0的chane,如果消费者没有消费生产者就堵塞住
	// go MessageHandleLoop(0)
	for i := 0; i < consumerCount; i += 1 {
		go MessageHandleLoop(i)
	}
	closeChan = make(chan struct{})
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}
	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)
	close(closeChan)
	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		infoLog.Printf("checkError err=%s", err)
		log.Fatal(err)
	}
}
