package main

import (
	"crypto/tls"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"sync"
	"syscall"

	"github.com/HT_GOGO/gotcp/libcomm"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/jessevdk/go-flags"
	"github.com/nsqio/go-nsq"
	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/certificate"
	"gopkg.in/ini.v1"
)

var wg *sync.WaitGroup
var clientManager *apns2.ClientManager
var myCert tls.Certificate

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

const (
	//CerPassWd = "yike@helloTalk"
	CerPassWd = "123456"
	TopicId   = "com.helloTalk.helloTalk"
)

const (
	PUSH_PARAM_CALLINCOMING = "voip_call_incoming"
	PUSH_PARAM_CALLIMISS    = "voip_call_miss"
	PUSH_PARAM_CALLICANCEL  = "voip_call_cancel"
)

const (
	APNS_PUSH_CALLINCOMING = "Incoming Call"
	APNS_PUSH_CALLMISS     = "Call Canceled"
	APNS_PUSH_CALLCANCEL   = "Call Missed"
	APNS_PUSH_CALLEND      = "Call End"
	APNS_PUSH_SYC_OK       = "Call SYNC Ok"
	CALL_START             = "start"
	CALL_END               = "end"
	CALL_SYNC_OK           = "timing"
)

const (
	PT_TEXT              = 0
	PT_VOICE             = 1
	PT_PHOTO             = 2
	PT_INDRODUCE         = 3
	PT_LOCATION          = 4
	PT_FRIEND_INVITE     = 5
	PT_LANGUAGE_EXCHANGE = 6
	PT_CORRECT_SENTENCE  = 7
	PT_STICKERS          = 8
	PT_DOODLE            = 9
	PT_GIFT              = 10
	PT_VOIP              = 11
	PT_INVITE_ACCEPT     = 12
	PT_VIDEO             = 13

	PT_GVOIP               = 15
	PT_LINK                = 16
	PT_CARD                = 17
	PT_FOLLOW              = 18
	PT_REPLY_YOUR_COMMENT  = 19
	PT_COMMENTED_YOUR_POST = 20
	PT_CORRECTED_YOUR_POST = 21
	PT_MOMENT_LIKE         = 22
)

const (
	NOT_BEEN_AT = 0 //default not at
	BEEN_AT     = 1 //
)

const (
	CTP2P = 0
	CTMUC = 1
)

var options Options
var parser = flags.NewParser(&options, flags.Default)

func MessageHandle(message *nsq.Message) error {
	// 统计apns总量
	attr := "pushkit/total_recv_req_count"
	libcomm.AttrAdd(attr, 1)

	// log.Printf("MessageHandle Got a message: %s", message.Body)
	wg.Add(1)
	go func() {
		PostData(message)
		wg.Done()
	}()

	return nil
}

func PostData(message *nsq.Message) error {
	log.Printf("PostData enter msgId=%v timestamp=%v body=%s", message.ID, message.Timestamp, message.Body)
	// 然后进行下一步无处理
	rootObj, err := simplejson.NewJson(message.Body)
	if err != nil {
		log.Printf("PostData simplejson new packet error", err)
		return err
	}
	// log.Printf("PostData rootObj=%#v", rootObj)
	token := rootObj.Get("token").MustString("")
	pushType := rootObj.Get("push_type").MustInt(PT_TEXT)
	preview := rootObj.Get("preview").MustInt(0)
	pushParam := rootObj.Get("push_param").MustString("")
	pushParam = strings.Trim(pushParam, "\u0000")
	callType := CALL_END
	log.Printf("PostData pushParam=%s", pushParam)
	if strings.HasPrefix(pushParam, APNS_PUSH_CALLINCOMING) {
		callType = CALL_START
	} else if strings.HasPrefix(pushParam, APNS_PUSH_CALLCANCEL) {
		callType = CALL_END
		log.Printf("PostData pushParam=%s has APNS_PUSH_CALLCANCEL", pushParam)
	} else if strings.HasPrefix(pushParam, APNS_PUSH_SYC_OK) {
		callType = CALL_SYNC_OK
		log.Printf("PostData pushParam=%s has APNS_PUSH_SYC_OK", pushParam)
	} else {
		callType = CALL_END
	}
	nickName := rootObj.Get("nick_name").MustString("")
	nickName = strings.Trim(nickName, "\u0000")
	// byAt := rootObj.Get("by_at").MustInt(NOT_BEEN_AT)

	alertObj := simplejson.New()
	var alertString string
	if preview == 0 {
		alertObj.Set("loc-key", "Message Preview Example No")
	} else {
		switch pushType {
		case PT_VOIP:
			alertObj.Set("loc-key", "%@: "+pushParam)
			alertObj.Set("loc-args", []interface{}{nickName})
		default:
			log.Printf("UnHandle pushType=%v", pushType)
		}
	}
	apsObj := simplejson.New()
	// add alert obj
	if len(alertString) != 0 {
		apsObj.Set("alert", alertString)
	} else {
		apsObj.Set("alert", alertObj)
	}
	// add badge
	badgeNum := rootObj.Get("badge_num").MustInt(0)
	if badgeNum <= 0 {
		badgeNum = 1
	}
	apsObj.Set("badge", badgeNum)
	//add sound
	apsObj.Set("sound", rootObj.Get("sound").MustString("default"))

	pushMsgObj := simplejson.New()
	// add aps param
	pushMsgObj.Set("aps", apsObj)
	pushMsgObj.Set("userid", rootObj.Get("from_id").MustInt(0))
	pushMsgObj.Set("voiproomid", rootObj.Get("voip_room_id").MustString(""))
	pushMsgObj.Set("type", callType)

	//add action id
	rootObj.Set("actionid", rootObj.Get("action_id").MustInt(0))
	pushSlice, err := pushMsgObj.MarshalJSON()
	if err != nil {
		log.Printf("PostData MarshalJSON failed")
		return nil
	}

	log.Printf("PostData push message=%s", pushSlice)
	notification := &apns2.Notification{}
	notification.DeviceToken = token
	//notification.Topic = TopicId
	notification.Payload = pushSlice // See Payload section below

	toId := rootObj.Get("to_id").MustInt(0)
	var client *apns2.Client
	//if toId == 900419 || toId == 78360 {
	if toId == 900419 {
		client = clientManager.Get(myCert).Development()
		log.Printf("use Development push kit uid=%v", toId)
	} else {
		client = clientManager.Get(myCert).Production()
	}
	defer clientManager.Add(client)
	res, err := client.Push(notification)
	log.Printf("ProcData client.Push ret = %#v", res)
	if err == nil && res.StatusCode == apns2.StatusSent {
		// 统计apns成功总量
		attr := "pushkit/total_push_succ_count"
		libcomm.AttrAdd(attr, 1)
	} else {
		// 统计apns失败总量
		attr := "pushkit/total_push_failed_count"
		libcomm.AttrAdd(attr, 1)
		log.Printf("postData client.Push failed err=%v", err)
	}
	return nil
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")
	cerPath := cfg.Section("CER").Key("path").MustString("./cer.p12")
	myCert, err = certificate.FromP12File(cerPath, CerPassWd)
	if err != nil {
		log.Fatal("Get Cert error:", err)
	}
	clientManager = apns2.NewClientManager()

	// init ansp consumter
	wg = &sync.WaitGroup{}
	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
