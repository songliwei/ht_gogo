package main

import (
	"database/sql"
	"errors"
	"fmt"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/garyburd/redigo/redis"
	"github.com/go-ini/ini"
	_ "github.com/go-sql-driver/mysql"
	flags "github.com/jessevdk/go-flags"

	"log"
	"os"
	"runtime"
	"strconv"
	"time"
)

type Callback struct{}

const (
	favorDB          = "favorite"
	favorRecordTable = "favor_record"
	favorTsTable     = "favor_ts"
	favTableUidDiv   = 500000
)

var (
	ErrCloseExplicitly = errors.New("Closed explicitly")
)

var (
	allFavCountKeyName = "collect"
)

func newRedisPool(redisServer string) *redis.Pool {
	return &redis.Pool{MaxIdle: 10,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", redisServer)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}

var (
	infoLog   *log.Logger
	redisApi  *common.RedisApi
	db        *sql.DB
	mongoSess *mgo.Session
)

type FavorTs struct {
	Uid      uint32
	LastTs   uint32
	FavCount uint32
}

func IncAllFovoriteCount(uid uint32) (err error) {
	err = ChangeAllFovoriteCount(uid, 1)
	return err
}

func DescAllFovoriteCount(uid uint32) (err error) {
	err = ChangeAllFovoriteCount(uid, -1)
	return err
}

func ChangeAllFovoriteCount(uid uint32, changeCount int) (err error) {
	if uid == 0 || changeCount == 0 {
		infoLog.Printf("ChangeAllFovoriteCount invalid param uid=%v count=%v", uid, changeCount)
		err = errors.New("invalid param")
		return err
	}
	// first update db
	err = ChangeAllFovoriteCountInDb(uid, changeCount)
	if err != nil {
		infoLog.Printf("ChangeAllFovoriteCount in db faield uid=%v changeCount=%v err=%s", uid, changeCount, err)
		attrName := "gofav/change_fav_count_in_db_failed"
		libcomm.AttrAdd(attrName, 1)
	}
	// second update redis
	err = ChangeAllFovoriteCountInRedis(uid, changeCount)
	if err != nil {
		attrName := "gofav/change_fav_count_in_redis_failed"
		libcomm.AttrAdd(attrName, 1)
		infoLog.Printf("ChangeAllFovoriteCount in redis faield uid=%v changeCount=%v err=%s", uid, changeCount, err)
	}
	return err
}

func GetRedisCTCRHashName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("CTCR_%v", uid)
	return hashName
}

func ChangeAllFovoriteCountInRedis(uid uint32, changeCount int) (err error) {
	if uid == 0 || changeCount == 0 {
		infoLog.Printf("ChangeAllFovoriteCountInRedis invalid param uid=%v count=%v", uid, changeCount)
		err = errors.New("invalid param")
		return err
	}
	hashName := GetRedisCTCRHashName(uid)
	r, err := redisApi.Exists(hashName)
	if err == nil && r == true {
		value, err := redisApi.Hincrby(hashName, allFavCountKeyName, changeCount)
		if err != nil {
			infoLog.Printf("ChangeAllFovoriteCountInRedis Reids exec HINCRBY hashName=%s fieldname=%s count=%v err=%v",
				hashName,
				allFavCountKeyName,
				changeCount,
				err)
		} else {
			infoLog.Printf("ChangeAllFovoriteCountInRedis Redis exec HINCRBY succ hashName=%s fieldname=%s allFavCount=%v",
				hashName,
				allFavCountKeyName,
				value)
		}
	} else {
		infoLog.Printf("ChangeAllFovoriteCountInRedis uid=%v changeCont=%v hashName=%s not exist", uid, changeCount, hashName)
	}

	return err
}

func ChangeAllFovoriteCountInDb(uid uint32, changeCount int) (err error) {
	if uid == 0 || changeCount == 0 {
		infoLog.Printf("ChangeAllFovoriteCountInDb invalid param uid=%v count=%v", uid, changeCount)
		err = errors.New("invalid param")
		return err
	}

	if changeCount > 0 {
		r, err := db.Exec("UPDATE MG_USER_COUNTINFO SET ALLCOUNT = ALLCOUNT + ? WHERE USERID =?;", changeCount, uid)
		if err != nil {
			infoLog.Printf("ChangeAllFovoriteCountInDb failed uid=%v count=%v err=%v", uid, changeCount, err)
			return err
		} else {
			affectRow, err := r.RowsAffected()
			if err != nil {
				infoLog.Printf("ChangeAllFovoriteCountInDb exec RowsAffected failed err=%v", err)
			} else {
				infoLog.Printf("ChangeAllFovoriteCountInDb affectRow=%v", affectRow)
			}
			return nil
		}
	} else {
		r, err := db.Exec("UPDATE MG_USER_COUNTINFO SET ALLCOUNT = ALLCOUNT - ? WHERE USERID =?;", -changeCount, uid)
		if err != nil {
			infoLog.Printf("ChangeAllFovoriteCountInDb failed uid=%v count=%v err=%v", uid, changeCount, err)
			return err
		} else {
			affectRow, err := r.RowsAffected()
			if err != nil {
				infoLog.Printf("ChangeAllFovoriteCountInDb exec RowsAffected failed err=%v", err)
			} else {
				infoLog.Printf("ChangeAllFovoriteCountInDb affectRow=%v", affectRow)
			}
			return nil
		}
	}
}

// 将FavoriteCount清零
func ResetAllFovoriteCount(uid uint32) (err error) {
	if uid == 0 {
		infoLog.Printf("ResetAllFovoriteCount invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return err
	}
	// first update db
	err = ResetAllFovoriteCountInDb(uid)
	if err != nil {
		infoLog.Printf("ResetAllFovoriteCountInDb in db faield uid=%v err=%s", uid, err)
		attrName := "gofav/reset_fav_count_in_db_failed"
		libcomm.AttrAdd(attrName, 1)
	}
	// second update redis
	err = ResetAllFovoriteCountInRedis(uid)
	if err != nil {
		infoLog.Printf("ResetAllFovoriteCountInRedis in redis faield uid=%v err=%s", uid, err)
		attrName := "gofav/reset_fav_count_in_redis_failed"
		libcomm.AttrAdd(attrName, 1)
	}
	return err
}

func ResetAllFovoriteCountInDb(uid uint32) (err error) {
	if uid == 0 {
		infoLog.Printf("ResetAllFovoriteCountInDb invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return err
	}

	r, err := db.Exec("UPDATE MG_USER_COUNTINFO SET ALLCOUNT = 0 WHERE USERID =?;", uid)
	if err != nil {
		infoLog.Printf("ResetAllFovoriteCountInDb failed uid=%v err=%v", uid, err)
		return err
	} else {
		affectRow, err := r.RowsAffected()
		if err != nil {
			infoLog.Printf("ResetAllFovoriteCountInDb exec RowsAffected failed err=%v", err)
		} else {
			infoLog.Printf("ResetAllFovoriteCountInDb affectRow=%v", affectRow)
		}
		return nil
	}
}

func ResetAllFovoriteCountInRedis(uid uint32) (err error) {
	if uid == 0 {
		infoLog.Printf("ResetAllFovoriteCountInRedis invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return err
	}

	hashName := GetRedisCTCRHashName(uid)
	r, err := redisApi.Exists(hashName)
	if err == nil && r == true {
		err = redisApi.HsetInt64(hashName, allFavCountKeyName, int64(0))
		if err != nil {
			infoLog.Printf("ResetAllFovoriteCountInRedis Reids exec HSet hashName=%s fieldname=%s err=%v",
				hashName,
				allFavCountKeyName,
				err)
			attrName := "gofav/fav_rest_fav_count_inredis_failed"
			libcomm.AttrAdd(attrName, 1)
		} else {
			infoLog.Printf("ResetAllFovoriteCountInRedis Redis exec Hset succ hashName=%s fieldname=%s",
				hashName,
				allFavCountKeyName)
		}
	} else {
		infoLog.Printf("ResetAllFovoriteCountInRedis uid=%v hashName=%s not exist", uid, hashName)
	}
	return err
}

func GetFavTable(uid uint32) (table string) {
	index := uid / favTableUidDiv
	table = fmt.Sprintf("%s_%v", favorRecordTable, index)
	return table
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	//runtime.GOMAXPROCS(runtime.NumCPU())
	runtime.GOMAXPROCS(1)

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags | log.Lshortfile)

	// 创建mongodb对象
	mongo_url := cfg.Section("MONGO").Key("url").MustString("localhost")
	infoLog.Println(mongo_url)
	mongoSess, err = mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
	}

	// init redis pool
	redisIp := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis ip=%v port=%v", redisIp, redisPort)
	//	pool = newRedisPool(redisIp + ":" + strconv.Itoa(redisPort))
	redisApi = common.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Println("open mysql failed err=%s", err)
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	uidList := []uint32{5129484}
	//uidList := []uint32{2061891}
	for i, v := range uidList {
		favorTable := GetFavTable(v)
		mongoConn := mongoSess.DB(favorDB).C(favorTable)
		totalCount, err := mongoConn.Find(bson.M{"uid": v, "favstat": bson.M{"$ne": 1}}).Count()
		if err != nil {
			infoLog.Printf("mongoConn.Find index i=%v uid=%v count=%v", i, v, totalCount)
			continue
		}
		err = ResetAllFovoriteCount(v)
		if err != nil {
			infoLog.Printf("ResetAllFovoriteCount index=%v uid=%v failed", i, v)
			continue
		}

		err = ChangeAllFovoriteCount(v, totalCount)
		if err != nil {
			infoLog.Printf("ChangeAllFovoriteCount index=%v uid=%v failed", i, v)
			continue
		}
		// hashName := GetRedisCTCRHashName(v)
		// strCount, err := redisApi.Hget(hashName, allFavCountKeyName)
		// if err != nil {
		// 	infoLog.Printf("redisApi.Hget hashName=%s allFavCountKeyName=%s failed err=%s",
		// 		hashName,
		// 		allFavCountKeyName)
		// 	continue
		// }
		// totalCount, err := strconv.ParseUint(strCount, 10, 64)
		// if err != nil {
		// 	infoLog.Printf("strconv.ParseUint failed uid=%v totalcount=%s err=%s", v, strCount, err)
		// 	continue
		// }
		// // first reset count
		// err = ResetAllFovoriteCountInDb(v)
		// if err != nil {
		// 	infoLog.Printf("index i=%v uid=%v ResetAllFovoriteCountInDb failed err=%s", i, v, err)
		// 	continue
		// }

		// // second set to the new count
		// err = ChangeAllFovoriteCountInDb(v, totalCount)
		// if err != nil {
		// 	infoLog.Printf("index i=%v uid=%v ChangeAllFovoriteCountInDb failed err=%s", i, v, err)
		// 	continue
		// }

		infoLog.Printf("index i=%v uid=%v totalCount=%v", i, v, totalCount)
	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
