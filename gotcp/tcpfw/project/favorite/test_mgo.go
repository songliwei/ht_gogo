package main

import (
	"fmt"

	flags "github.com/jessevdk/go-flags"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"log"
	"runtime"
)

const (
	favorDB          = "favorite"
	favorRecordTable = "favor_record_3"
	favorTsTable     = "favor_ts"
	modifyTable      = "modify_record"
)

type TextInfo struct {
	Content       string
	Translate     string
	ContentTran   string
	TranslateTran string
}

type VoiceInfo struct {
	Url         string
	Duration    uint32
	Size        uint32
	VoiceToText TextInfo
}

type ImageInfo struct {
	ThumbUrl string
	BigUrl   string
	Width    uint32
	Height   uint32
}

type CorrectInfo struct {
	Content    string
	Correction string
}

type FavorRecord struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"-"`
	ClientId   uint64
	Uid        uint32
	Md5        string
	SrcUid     uint32
	Type       uint32
	Text       TextInfo
	Voice      VoiceInfo
	Correction CorrectInfo
	Image      ImageInfo
	Mid        string
	Tags       []string
	FavTs      uint32
}
type FavorModify struct {
	Uid   uint32
	Obid  string
	Type  uint32
	Index uint64
	ModTs uint32
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func GetHex(str string) (hex string) {
	count, err := fmt.Sscan(str, `ObjectIdHex(%s)`, &hex)
	if err != nil {
		fmt.Printf("GetHex str=%s err=%s", str, err)
	} else {
		fmt.Printf("GetHex count=%v hex=%s", count, hex)
	}
	return hex
}

func main() {
	//runtime.GOMAXPROCS(runtime.NumCPU())
	runtime.GOMAXPROCS(1)

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	// if options.ServerConf == "" {
	// 	log.Fatalln("Must input config file name")
	// }

	// 创建mongodb对象
	mongo_url := "localhost"
	fmt.Println(mongo_url)
	mongoSess, err := mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
		return
	}
	defer mongoSess.Close()
	mongoSess.SetMode(mgo.Monotonic, true)

	//	id := bson.NewObjectId()
	//	idStr := id.String()
	//	idHex := id.Hex()
	//	fmt.Printf("id=%v idStr=%s idHex=%s\n", id, idStr, idHex)

	// write mongodb
	// Step1 删除收藏列表中的记录
	//	mongoConn := mongoSess.DB(favorDB).C(favorRecordTable)
	//	obid := bson.ObjectIdHex("58943e784cd5966c468d6261")
	//TEST
	//	query := mongoConn.Find(bson.M{})
	retModList := []FavorModify{}
	mongoConn := mongoSess.DB(favorDB).C(modifyTable)
	// obid := bson.ObjectIdHex(getReqBody.GetMaxObid())

	//	err = mongoConn.Find(bson.M{"uid": 1603139,
	//		"index": bson.M{"$gt": 0},
	//		"obid":  bson.M{"$lt": "0"}}).Sort("index").All(&retModList)
	err = mongoConn.Find(bson.M{"uid": 1603139, "index": bson.M{"$gt": 0}}).Sort("index").All(&retModList)

	if err != nil {
		fmt.Printf("procSyncModifyReq find modifyTable failed uid=%v", 1603139)
	}
	modLen := len(retModList)
	fmt.Printf("procSyncModifyReq modLen=%v\n", modLen)
	//	var result []FavorRecord
	//	err = query.All(&result)
	//	for i, v := range result {
	//		fmt.Printf("procDelReq index=%v v.id=%v\n", i, v.Id)
	//		hex := GetHex(v.Id)
	//		fmt.Printf("procDelReq hex=%s\n", hex)
	//		query = mongoConn.FindId(v.Id)
	//		count, err := query.Count()
	//		if err != nil {
	//			fmt.Printf("procDelReq FindId=%v err=%s\n", v.Id, err)
	//		} else {
	//			fmt.Printf("procDelReq FindId=%v count=%v\n", v.Id, count)
	//		}
	//	}

	//	query = mongoConn.FindId(obid.String())
	//	count, err := query.Count()
	//	if err != nil {
	//		fmt.Printf("procDelReq FindId=%v err=%s\n", obid, err)
	//	} else {
	//		fmt.Printf("procDelReq FindId=%v count=%v\n", obid, count)
	//	}
	//	err = mongoConn.RemoveId(obid.String())
	//	if err != nil {
	//		fmt.Printf("procDelReq RemoveId=%v failed\n", obid)
	//	} else {
	//		fmt.Printf("procDelReq RemoveId=%v success\n", obid)
	//	}
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
