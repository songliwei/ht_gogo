package main

import (
	// "fmt"
	// "github.com/bitly/go-simplejson"
	"fmt"
	"net"
	"os"
	"strconv"
	"time"

	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_favorite"

	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	// "strings"
	// "time"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	Cmd string `short:"c" long:"cmd" description:"command" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		fmt.Println("parse cmd line failed!")
		os.Exit(1)
	}
	cmd, _ := strconv.Atoi(options.Cmd)

	// 读取配置文件
	cfg, err := ini.Load([]byte(""), "test_config.ini")
	if err != nil {
		fmt.Println("load config file failed")
		return
	}
	// 配置文件只读 设置次标识提升性能
	cfg.BlockMode = false
	// 读取ip+port
	serverIp := cfg.Section("OUTER_SERVER").Key("server_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("OUTER_SERVER").Key("server_port").MustInt(8990)

	fmt.Printf("server_ip=%v server_port=%v\n", serverIp, serverPort)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	defer conn.Close()
	checkError(err)
	headV3Protocol := &common.HeadV3Protocol{}
	var head *common.HeadV3
	head = &common.HeadV3{Flag: 0xF0, TermType: 1, Cmd: uint16(cmd), From: 1946613, To: 1946612}
	reqBody := new(ht_favorite.ReqBody)

	switch ht_favorite.CMD_TYPE(cmd) {
	case ht_favorite.CMD_TYPE_CMD_ADD:
		favorType := ht_favorite.TYPE_FAVORATE_TYPE_TEXT
		senCor := &ht_favorite.SentenceCorrect{
			Source: []byte("你是大SBlelel"),
			Target: []byte("大SB是你hahahduni"),
		}
		element := &ht_favorite.FavoriteContent{
			SrcUid: proto.Uint32(1946612),
			Type:   &favorType,
			Text: &ht_favorite.TextBody{
				Content:       []byte("Cold weatcher today"),
				Translate:     []byte("今天天气真冷hahahaadaddbvsvsfvs"),
				ContentTran:   []byte("abcdefg"),
				TranslateTran: []byte("hijklmn"),
			},
			Voice: &ht_favorite.VoiceBody{
				Url:      []byte("www.voice.com"),
				Duration: proto.Uint32(10),
				Size:     proto.Uint32(10240),
			},

			Correction: &ht_favorite.CorrectContent{
				Comment: []byte("Hello SB"),
				Body: []*ht_favorite.SentenceCorrect{
					senCor,
				},
			},
			Moment: &ht_favorite.MntBody{
				Mid:          []byte("124000000"),
				PictureCount: proto.Uint32(2),
			},
			Tags:  []string{"abc", "def"},
			AddTs: proto.Uint64(uint64(time.Now().Unix())),
			Image: &ht_favorite.ImageBody{
				ThumbUrl: []byte("www.hellotalk.com"),
				BigUrl:   []byte("www.hellotalk.com"),
				Width:    proto.Uint32(3),
				Height:   proto.Uint32(4),
			},
			ClientId: proto.Uint64(1),
		}
		reqBody.AddReqbody = &ht_favorite.AddReqBody{
			Content: []*ht_favorite.FavoriteContent{
				element,
			},
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			fmt.Println("marshaling error: ", err)
			return
		}

		head.Len = uint32(common.PacketV3HeadLen) + uint32(len(payLoad)) + 1
		buf := make([]byte, head.Len)
		buf[0] = common.HTV3MagicBegin
		err = common.SerialHeadV3ToSlice(head, buf[1:])
		if err != nil {
			fmt.Println("SerialHeadV3ToSlice failed")
			return
		}
		copy(buf[common.PacketV3HeadLen:], payLoad)
		buf[head.Len-1] = common.HTV3MagicEnd
		//fmt.Printf("sendpkg's len=%v\n", len(common.NewHeadV3Packet(buf).Serialize()))

		// write
		conn.Write(common.NewHeadV3Packet(buf).Serialize())

		// read
		p, err := headV3Protocol.ReadPacket(conn)
		if err != nil {
			fmt.Println("ReadPacket failed!")
			return
		}
		packet := p.(*common.HeadV3Packet)
		rspHead, err := packet.GetHead()
		if err != nil {
			fmt.Println("get HTV3Head failed err =", err)
			return
		}
		fmt.Printf("ret=%v, len=%v\n", rspHead.Ret, rspHead.Len)
		if rspHead.Ret != 0 {
			return
		}

		rspBody := new(ht_favorite.RspBody)
		err = proto.Unmarshal(packet.GetBody(), rspBody)
		if err != nil {
			fmt.Println("proto Unmarshal failed")
			return
		}
		addRspBody := rspBody.GetAddRspbody()
		if addRspBody == nil {
			fmt.Println("GetAddRspbody() failed")
			return
		}
		idList := addRspBody.GetIdList()
		for i, v := range idList {
			fmt.Printf("index=%v clientId=%v objId=%s, lastts[%v]\n", i, v.GetClientId(), v.GetObid(), addRspBody.GetLastTs())
		}

	case ht_favorite.CMD_TYPE_CMD_DEL:
		reqBody.DelReqbody = &ht_favorite.DelReqBody{
			ObidList: []string{"58b279364cd5965a8f68fcc8"},
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			fmt.Println("marshaling error: ", err)
			return
		}

		head.Len = uint32(common.PacketV3HeadLen) + uint32(len(payLoad)) + 1
		buf := make([]byte, head.Len)
		buf[0] = common.HTV3MagicBegin
		err = common.SerialHeadV3ToSlice(head, buf[1:])
		if err != nil {
			fmt.Println("SerialHeadV3ToSlice failed")
			return
		}
		copy(buf[common.PacketV3HeadLen:], payLoad)
		buf[head.Len-1] = common.HTV3MagicEnd
		//fmt.Printf("sendpkg's len=%v\n", len(common.NewHeadV3Packet(buf).Serialize()))

		// write
		conn.Write(common.NewHeadV3Packet(buf).Serialize())

		// read
		p, err := headV3Protocol.ReadPacket(conn)
		if err != nil {
			fmt.Println("ReadPacket failed!")
			return
		}
		packet := p.(*common.HeadV3Packet)
		rspHead, err := packet.GetHead()
		if err != nil {
			fmt.Println("get HTV3Head failed err =", err)
			return
		}
		fmt.Printf("ret=%v, len=%v\n", rspHead.Ret, rspHead.Len)
		if rspHead.Ret != 0 {
			return
		}

		rspBody := new(ht_favorite.RspBody)
		err = proto.Unmarshal(packet.GetBody(), rspBody)
		if err != nil {
			fmt.Println("proto Unmarshal failed")
			return
		}
		delRspBody := rspBody.GetDelRspbody()
		if delRspBody == nil {
			fmt.Println("GetDelRspbody() failed")
			return
		}
		fmt.Printf("DelRspBody code=%v msg=%s lastTs=%v \n",
			delRspBody.GetStatus().GetCode(),
			delRspBody.GetStatus().GetReason(),
			delRspBody.GetLastTs())
		delResList := delRspBody.GetDelStat()
		for i, v := range delResList {
			fmt.Printf("DelRspBody index=%v obid=%s stat=%v", i, v.GetObid(), v.GetRes())
		}

	case ht_favorite.CMD_TYPE_CMD_GET_LATEST_FAV_OBID:
		reqBody.GetLatestFavObidReqbody = &ht_favorite.GetLatestFavObidReqBody{
			Obid:   proto.String("0"),
			LastTs: proto.Uint32(0),
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			fmt.Println("marshaling error: ", err)
			return
		}

		head.Len = uint32(common.PacketV3HeadLen) + uint32(len(payLoad)) + 1
		buf := make([]byte, head.Len)
		buf[0] = common.HTV3MagicBegin
		err = common.SerialHeadV3ToSlice(head, buf[1:])
		if err != nil {
			fmt.Println("SerialHeadV3ToSlice failed")
			return
		}
		copy(buf[common.PacketV3HeadLen:], payLoad)
		buf[head.Len-1] = common.HTV3MagicEnd
		//fmt.Printf("sendpkg's len=%v\n", len(common.NewHeadV3Packet(buf).Serialize()))

		// write
		conn.Write(common.NewHeadV3Packet(buf).Serialize())

		// read
		p, err := headV3Protocol.ReadPacket(conn)
		if err != nil {
			fmt.Println("ReadPacket failed!")
			return
		}
		packet := p.(*common.HeadV3Packet)
		rspHead, err := packet.GetHead()
		if err != nil {
			fmt.Println("get HTV3Head failed err =", err)
			return
		}
		fmt.Printf("ret=%v, len=%v\n", rspHead.Ret, rspHead.Len)
		if rspHead.Ret != 0 {
			return
		}

		rspBody := new(ht_favorite.RspBody)
		err = proto.Unmarshal(packet.GetBody(), rspBody)
		if err != nil {
			fmt.Println("proto Unmarshal failed")
			return
		}
		getRspBody := rspBody.GetGetLatestFavObidRspbody()
		if getRspBody == nil {
			fmt.Println("GetGetLatestFavObidRspbody() failed")
			return
		}
		fmt.Printf("GetGetLatestFavObidRspbody code=%v msg=%s Obid=%s More=%v LastTs=%v\n",
			getRspBody.GetStatus().GetCode(),
			getRspBody.GetStatus().GetReason(),
			getRspBody.GetMore(),
			getRspBody.GetLastTs())
		favList := getRspBody.GetObidList()
		for i, v := range favList {
			fmt.Printf("index=%v Obid=%s stat=%v ver=%v\n",
				i,
				v.GetObid(),
				v.GetStat(),
				v.GetVersion())
		}

	case ht_favorite.CMD_TYPE_CMD_UPDATE_FAV:
		reqBody.UpdateReqbody = &ht_favorite.UpdateReqBody{
			Obid: proto.String("58ae626d4cd5966886b9c06f"),
			Type: ht_favorite.TYPE_UPDATE_TYPE_UPDATE_TEXT.Enum(),
			Text: &ht_favorite.TextBody{
				Content:       []byte("You are son of bitch"),
				Translate:     []byte("你老妈是谁"),
				ContentTran:   []byte("xxxooooo"),
				TranslateTran: []byte("niuidasabi"),
			},
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			fmt.Println("marshaling error: ", err)
			return
		}

		head.Len = uint32(common.PacketV3HeadLen) + uint32(len(payLoad)) + 1
		buf := make([]byte, head.Len)
		buf[0] = common.HTV3MagicBegin
		err = common.SerialHeadV3ToSlice(head, buf[1:])
		if err != nil {
			fmt.Println("SerialHeadV3ToSlice failed")
			return
		}
		copy(buf[common.PacketV3HeadLen:], payLoad)
		buf[head.Len-1] = common.HTV3MagicEnd
		//fmt.Printf("sendpkg's len=%v\n", len(common.NewHeadV3Packet(buf).Serialize()))

		// write
		conn.Write(common.NewHeadV3Packet(buf).Serialize())

		// read
		p, err := headV3Protocol.ReadPacket(conn)
		if err != nil {
			fmt.Println("ReadPacket failed!")
			return
		}
		packet := p.(*common.HeadV3Packet)
		rspHead, err := packet.GetHead()
		if err != nil {
			fmt.Println("get HTV3Head failed err =", err)
			return
		}
		fmt.Printf("ret=%v, len=%v\n", rspHead.Ret, rspHead.Len)
		if rspHead.Ret != 0 {
			return
		}

		rspBody := new(ht_favorite.RspBody)
		err = proto.Unmarshal(packet.GetBody(), rspBody)
		if err != nil {
			fmt.Println("proto Unmarshal failed")
			return
		}
		updateRspBody := rspBody.GetUpdateRspbody()
		if updateRspBody == nil {
			fmt.Println("GetUpdaterRspbody() failed")
			return
		}
		fmt.Printf("GetUpdaterRspbody code=%v msg=%s LastTs=%s\n",
			updateRspBody.GetStatus().GetCode(),
			updateRspBody.GetStatus().GetReason(),
			updateRspBody.GetLastTs())

	case ht_favorite.CMD_TYPE_CMD_GET_BY_OBID_FAV:
		reqBody.GetByObidReqbody = &ht_favorite.GetFavByObidReqBody{
			ObidList: []string{"58ae626d4cd5966886b9c06f", "58ae870e4cd5966886b9c0ab"},
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			fmt.Println("marshaling error: ", err)
			return
		}

		head.Len = uint32(common.PacketV3HeadLen) + uint32(len(payLoad)) + 1
		buf := make([]byte, head.Len)
		buf[0] = common.HTV3MagicBegin
		err = common.SerialHeadV3ToSlice(head, buf[1:])
		if err != nil {
			fmt.Println("SerialHeadV3ToSlice failed")
			return
		}
		copy(buf[common.PacketV3HeadLen:], payLoad)
		buf[head.Len-1] = common.HTV3MagicEnd
		//fmt.Printf("sendpkg's len=%v\n", len(common.NewHeadV3Packet(buf).Serialize()))

		// write
		conn.Write(common.NewHeadV3Packet(buf).Serialize())

		// read
		p, err := headV3Protocol.ReadPacket(conn)
		if err != nil {
			fmt.Println("ReadPacket failed!")
			return
		}
		packet := p.(*common.HeadV3Packet)
		rspHead, err := packet.GetHead()
		if err != nil {
			fmt.Println("get HTV3Head failed err =", err)
			return
		}
		fmt.Printf("ret=%v, len=%v\n", rspHead.Ret, rspHead.Len)
		if rspHead.Ret != 0 {
			return
		}

		rspBody := new(ht_favorite.RspBody)
		err = proto.Unmarshal(packet.GetBody(), rspBody)
		if err != nil {
			fmt.Println("proto Unmarshal failed")
			return
		}
		obidRspBody := rspBody.GetGetByObidRspbody()
		if obidRspBody == nil {
			fmt.Println("GetByObidRspBody() failed")
			return
		}
		fmt.Printf("GetByObidRspBody code=%v msg=%s\n",
			obidRspBody.GetStatus().GetCode(),
			obidRspBody.GetStatus().GetReason())
		favList := obidRspBody.GetContentList()
		for i, v := range favList {
			fmt.Printf("index=%v Obid=%s SrcUid=%v Type=%v Tags=%v AddTs=%v ClientId=%v\n",
				i,
				v.GetObid(),
				v.GetSrcUid(),
				v.GetType(),
				v.GetTags(),
				v.GetAddTs(),
				v.GetClientId())
			favType := v.GetType()
			switch favType {
			case ht_favorite.TYPE_FAVORATE_TYPE_TEXT:
				fmt.Printf("GetByObidRspBody index=%v Content=%s Translate=%s ContentTran=%s TranslateTran=%s\n",
					i,
					v.GetText().GetContent(),
					v.GetText().GetTranslate(),
					v.GetText().GetContentTran(),
					v.GetText().GetTranslateTran())
			case ht_favorite.TYPE_FAVORATE_TYPE_VOICE:
				fmt.Printf("GetByObidRspBody index=%v Url=%s Duration=%v Size=%v Content=%s Translate=%s ContentTran=%s TranslateTran=%s\n",
					i,
					v.GetVoice().GetUrl(),
					v.GetVoice().GetDuration(),
					v.GetVoice().GetSize(),
					v.GetVoice().GetVoiceToText().GetContent(),
					v.GetVoice().GetVoiceToText().GetTranslate(),
					v.GetVoice().GetVoiceToText().GetContentTran(),
					v.GetVoice().GetVoiceToText().GetTranslateTran())
			case ht_favorite.TYPE_FAVORATE_TYPE_MNT:
				fmt.Printf("GetByObidRspBody index=%v mid=%s\n", i, v.GetMoment().GetMid())
			case ht_favorite.TYPE_FAVORATE_TYPE_CORR:
				fmt.Printf("GetByObidRspBody index=%v Content=%s Correction=%s\n",
					i,
					v.GetCorrection().GetComment)
				for index, value := range v.GetCorrection().GetBody() {
					fmt.Printf("index=%v Source=%s Target=%s\n", index, value.Source, value.Target)
				}
			}
		}

	case ht_favorite.CMD_TYPE_CMD_GET_HISTORY_FAV_OBID:
		reqBody.GetHistoryFavReqbody = &ht_favorite.GetHistoryFavObidReqBody{
			Obid: proto.String("58ae626d4cd5966886b9c06f"),
		}

		payLoad, err := proto.Marshal(reqBody)
		if err != nil {
			fmt.Println("marshaling error: ", err)
			return
		}

		head.Len = uint32(common.PacketV3HeadLen) + uint32(len(payLoad)) + 1
		buf := make([]byte, head.Len)
		buf[0] = common.HTV3MagicBegin
		err = common.SerialHeadV3ToSlice(head, buf[1:])
		if err != nil {
			fmt.Println("SerialHeadV3ToSlice failed")
			return
		}
		copy(buf[common.PacketV3HeadLen:], payLoad)
		buf[head.Len-1] = common.HTV3MagicEnd
		//fmt.Printf("sendpkg's len=%v\n", len(common.NewHeadV3Packet(buf).Serialize()))

		// write
		conn.Write(common.NewHeadV3Packet(buf).Serialize())

		// read
		p, err := headV3Protocol.ReadPacket(conn)
		if err != nil {
			fmt.Println("ReadPacket failed!")
			return
		}
		packet := p.(*common.HeadV3Packet)
		rspHead, err := packet.GetHead()
		if err != nil {
			fmt.Println("get HTV3Head failed err =", err)
			return
		}
		fmt.Printf("ret=%v, len=%v\n", rspHead.Ret, rspHead.Len)
		if rspHead.Ret != 0 {
			return
		}

		rspBody := new(ht_favorite.RspBody)
		err = proto.Unmarshal(packet.GetBody(), rspBody)
		if err != nil {
			fmt.Println("proto Unmarshal failed")
			return
		}
		getHistoryRspBody := rspBody.GetGetHistoryFavRspbody()
		if getHistoryRspBody == nil {
			fmt.Println("GetGetHistoryFavRspbody() failed")
			return
		}
		fmt.Printf("GetGetHistoryFavRspbody code=%v msg=%s Obid=%s More=%v \n",
			getHistoryRspBody.GetStatus().GetCode(),
			getHistoryRspBody.GetStatus().GetReason(),
			getHistoryRspBody.GetMore())
		favList := getHistoryRspBody.GetObidList()
		for i, v := range favList {
			fmt.Printf("index=%v Obid=%s stat=%v ver=%v\n",
				i,
				v.GetObid(),
				v.GetStat(),
				v.GetVersion())
		}
	default:
		fmt.Println("Unknown cmd")
	}

}

func checkError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
