package main

import (
	"crypto/md5"
	"database/sql"
	"errors"
	"fmt"
	"io"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_favorite"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/garyburd/redigo/redis"
	"github.com/go-ini/ini"
	_ "github.com/go-sql-driver/mysql"
	flags "github.com/jessevdk/go-flags"
	nsq "github.com/nsqio/go-nsq"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/golang/protobuf/proto"
)

type Callback struct{}

var (
	ErrCloseExplicitly = errors.New("Closed explicitly")
)

const (
	favorDB                   = "favorite"
	favorRecordTable          = "favor_record"
	favorTsTable              = "favor_ts"
	pageSize                  = 100
	favTableUidDiv            = 500000
	allFavCountKeyName        = "collect"
	NSQ_EVENT_FAVORITE        = 6
	NSQ_EVENT_CANCLE_FAVORITE = 18
)

func newRedisPool(redisServer string) *redis.Pool {
	return &redis.Pool{MaxIdle: 10,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", redisServer)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}

var (
	infoLog             *log.Logger
	mongoSess           *mgo.Session
	pool                *redis.Pool
	db                  *sql.DB
	globalProducer      *nsq.Producer
	nsqTopic            string
	updateCountTaskChan chan *UpdateCountTask
)

// 在mgo中已近定义了
// var (
// 	ErrNotFound = errors.New("not found")
// )

type WordBody struct {
	Content     string
	Translate   string
	ContentTran string
}

type TextInfo struct {
	Content       string
	Translate     string
	ContentTran   string
	TranslateTran string
}

type VoiceInfo struct {
	Url         string
	Duration    uint32
	Size        uint32
	VoiceToText TextInfo
}

type ImageInfo struct {
	ThumbUrl string
	BigUrl   string
	Width    uint32
	Height   uint32
}

type SentenceCorrect struct {
	Source string
	Target string
}

type CorrectInfo struct {
	Comment string
	Body    []SentenceCorrect
}

type UrlInfo struct {
	Url         string
	ThumbUrl    string
	Description string
	Title       string
}

type MntInfo struct {
	Mid          string
	PictureCount uint32
	Url          UrlInfo
}

type FavorRecord struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"-"`
	ClientId   int64
	Uid        uint32
	Md5        string
	SrcUid     uint32
	Type       uint32
	Text       TextInfo
	Word       WordBody
	Voice      VoiceInfo
	Correction CorrectInfo
	Image      ImageInfo
	Mid        MntInfo
	CommentId  string
	Tags       []string
	FavTs      uint64
	FavStat    uint32
	FavVer     uint32
}

type FavorTs struct {
	Uid      uint32
	LastTs   uint32
	FavCount uint32
}

type UpdateCountTask struct {
	Uid   uint32
	Count int
}

func GetBeginHexString() string {
	timeBegin := time.Date(2016, time.Month(12), 30, 0, 0, 0, 0, time.Local)
	objId := bson.NewObjectIdWithTime(timeBegin)
	return objId.Hex()
}

// Get end hex 50 year
func GetEndHexString() string {
	timeEnd := time.Date(2067, time.Month(12), 30, 0, 0, 0, 0, time.Local)
	objId := bson.NewObjectIdWithTime(timeEnd)
	return objId.Hex()
}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	attrName := "gofav/favorite_recv_req"
	libcomm.AttrAdd(attrName, 1)
	packet := p.(*common.HeadV3Packet)
	head, err := packet.GetHead()
	_, err = packet.CheckPacketValid()
	if err != nil {
		SendRsp(c, head, nil, uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INVALID_PARAM))
		infoLog.Println("Invalid packet", err)
		return false
	}

	go ProcData(c, p)
	return true
}

func SendRsp(c *gotcp.Conn, head *common.HeadV3, body *ht_favorite.FavRspBody, ret uint16) bool {
	rspHead := new(common.HeadV3)
	if head != nil {
		*rspHead = *head
	}
	outBuf, err := proto.Marshal(body)
	if err != nil {
		infoLog.Println("Failed to encode address book:", err)
		return false
	}

	rspHead.Ret = ret
	//rspHead.Len = len(rspHead) + 2 + body.GetLenth()
	rspHead.Len = common.HeadV3Len + 2 + uint32(len(outBuf))
	sendBuf := make([]byte, rspHead.Len)
	sendBuf[0] = common.HTV3MagicBegin
	err = common.SerialHeadV3ToSlice(rspHead, sendBuf[1:])
	if err != nil {
		infoLog.Println("SerialHeadV3ToSlice failed")
		return false
	}
	copy(sendBuf[1+common.HeadV3Len:], outBuf)
	sendBuf[rspHead.Len-1] = common.HTV3MagicEnd
	infoLog.Printf("ret=%v, len=%v\n", ret, rspHead.Len)
	resp := common.NewHeadV3Packet(sendBuf)
	c.AsyncWritePacket(resp, time.Second)

	return true
}

func ProcData(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*common.HeadV3Packet)
	if !ok {
		infoLog.Println("Convert to HeadV3Packet failed")
		attrName := "gofav/favorite_conv_packet_failed"
		libcomm.AttrAdd(attrName, 1)
		result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		SendRsp(c, nil, nil, result)
		return false
	}
	head, err := packet.GetHead()
	if err != nil {
		infoLog.Println("ProcData Get head faild")
		attrName := "gofav/favorite_get_head_failed"
		libcomm.AttrAdd(attrName, 1)
		result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		SendRsp(c, nil, nil, result)
		return false
	}
	infoLog.Printf("ProcData head.Len=%v protopacket len=%v", head.Len, len(packet.GetBody()))

	switch ht_favorite.CMD_TYPE(head.Cmd) {
	case ht_favorite.CMD_TYPE_CMD_ADD:
		go procAddReq(c, packet)
	case ht_favorite.CMD_TYPE_CMD_DEL:
		go procDelReq(c, packet)
	case ht_favorite.CMD_TYPE_CMD_GET_LATEST_FAV_OBID:
		go procGetLatestFavObidReq(c, packet)
	case ht_favorite.CMD_TYPE_CMD_UPDATE_FAV:
		go procUpdateReq(c, packet)
	case ht_favorite.CMD_TYPE_CMD_GET_BY_OBID_FAV:
		go procGetFavByObidReq(c, packet)
	case ht_favorite.CMD_TYPE_CMD_GET_HISTORY_FAV_OBID:
		go procGetHistoryFavObidReq(c, packet)
	default:
		infoLog.Println("Recv unkown cmd:", head.Cmd)
	}
	return true
}

func RefreshSession(err error) {
	if err != mgo.ErrNotFound {
		attrName := "gofav/mongodb_error_count"
		libcomm.AttrAdd(attrName, 1)
	}
	mongoSess.Refresh()
	infoLog.Printf("RefreshSession input err=%s", err)
}

func GetFavTable(uid uint32) (table string) {
	index := uid / favTableUidDiv
	table = fmt.Sprintf("%s_%v", favorRecordTable, index)
	return table
}

func procAddReq(c *gotcp.Conn, packet *common.HeadV3Packet) bool {
	attrName := "gofav/favorite_rec_add_req"
	libcomm.AttrAdd(attrName, 1)
	// parse packet
	rspBody := new(ht_favorite.FavRspBody)

	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
	var head *common.HeadV3
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()
	head, err := packet.GetHead()
	if err != nil {
		infoLog.Println("procAddReq Get head faild")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		rspBody.AddRspbody = &ht_favorite.AddRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get head failed"),
			},
		}
		return false
	}
	reqBody := new(ht_favorite.FavReqBody)
	err = proto.Unmarshal(packet.GetBody(), reqBody)
	if err != nil {
		infoLog.Println("procAddReq proto Unmarshal failed")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.AddRspbody = &ht_favorite.AddRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	subReqBody := reqBody.GetAddReqbody()
	if subReqBody == nil {
		infoLog.Println("procAddReq GetAddReqbody() failed")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.AddRspbody = &ht_favorite.AddRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	content := subReqBody.GetContent()
	for _, v := range content {
		infoLog.Printf("procAddReq uid[%v],type[%v],text[%s],mid[%s]", head.From, v.GetType(), v.GetText(), v.GetMoment())
	}

	var favId []*ht_favorite.FavoriteId
	var lastTs uint32
	tableName := GetFavTable(head.From)
	infoLog.Printf("procAddReq from=%v tableName=%s", head.From, tableName)
	mongoConn := mongoSess.DB(favorDB).C(tableName)
	mongoConn2 := mongoSess.DB(favorDB).C(favorTsTable)
	var duplicateCount int
	duplicateCount = 0
	for _, v := range content {
		// write mongodb
		record := new(FavorRecord)
		record.Id = bson.NewObjectId()
		record.ClientId = int64(v.GetClientId())
		record.Uid = head.From
		md5sum := md5.Sum([]byte(v.String()))
		var md5slice []byte = md5sum[:]
		record.Md5 = fmt.Sprintf("%x", string(md5slice))
		record.SrcUid = v.GetSrcUid()
		record.Type = uint32(v.GetType())
		record.Text = TextInfo{
			Content:       string(v.GetText().GetContent()),
			Translate:     string(v.GetText().GetTranslate()),
			ContentTran:   string(v.GetText().GetContentTran()),
			TranslateTran: string(v.GetText().GetTranslateTran()),
		}
		record.Word = WordBody{
			Content:     string(v.GetText().GetWordBody().GetContent()),
			Translate:   string(v.GetText().GetWordBody().GetTranslate()),
			ContentTran: string(v.GetText().GetWordBody().GetContentTran()),
		}
		record.Voice = VoiceInfo{
			Url:      string(v.GetVoice().GetUrl()),
			Duration: v.GetVoice().GetDuration(),
			Size:     v.GetVoice().GetSize(),
			VoiceToText: TextInfo{
				Content:       string(v.GetVoice().GetVoiceToText().GetContent()),
				Translate:     string(v.GetVoice().GetVoiceToText().GetTranslate()),
				ContentTran:   string(v.GetVoice().GetVoiceToText().GetContentTran()),
				TranslateTran: string(v.GetVoice().GetVoiceToText().GetTranslateTran()),
			},
		}
		record.Image = ImageInfo{
			ThumbUrl: string(v.GetImage().GetThumbUrl()),
			BigUrl:   string(v.GetImage().GetBigUrl()),
			Width:    v.GetImage().GetWidth(),
			Height:   v.GetImage().GetHeight(),
		}
		var cor CorrectInfo
		cor.Comment = string(v.GetCorrection().GetComment())
		for _, v := range v.GetCorrection().GetBody() {
			correctIterm := SentenceCorrect{
				Source: string(v.GetSource()),
				Target: string(v.GetTarget()),
			}
			cor.Body = append(cor.Body, correctIterm)
		}

		record.Correction = cor
		record.Mid = MntInfo{
			Mid:          string(v.GetMoment().GetMid()),
			PictureCount: v.GetMoment().GetPictureCount(),
			Url: UrlInfo{
				Url:         string(v.GetMoment().GetUrlInfo().GetUrl()),
				ThumbUrl:    string(v.GetMoment().GetUrlInfo().GetThumbUrl()),
				Description: string(v.GetMoment().GetUrlInfo().GetDescription()),
				Title:       string(v.GetMoment().GetUrlInfo().GetTitle()),
			},
		}
		record.CommentId = string(v.GetCommentId())
		record.Tags = v.GetTags()
		record.FavTs = v.GetAddTs()
		record.FavStat = uint32(ht_favorite.FAV_STAT_TYPE_FAV_NORMAL)
		record.FavVer = uint32(time.Now().Unix())
		// infoLog.Printf("ProcAddReq record=%#v", record)
		err = mongoConn.Insert(record)
		// 如果MD5重复则返回重复收藏的错误
		if err != nil {
			infoLog.Printf("procAddReq Insert failed uid=%v err=%s", head.From, err)
			if err == io.EOF || err == ErrCloseExplicitly {
				RefreshSession(err)
				err = mongoConn.Insert(record)
				if err != nil {
					infoLog.Printf("procAddReq retry Insert failed uid=%v err=%s", head.From, err)
					// 一条插入失败不返回继续插入剩余记录
					continue
				}
			} else { // 记录重复或其他失败原因
				// 一条插入失败不返回继续插入剩余记录
				var storeIterm FavorRecord
				err = mongoConn.Find(bson.M{"md5": record.Md5}).One(&storeIterm)
				if err == nil {
					// 保存clientid-->serverid的映射关系 注意将id转成string
					iterm := &ht_favorite.FavoriteId{
						ClientId: proto.Uint64(v.GetClientId()),
						Obid:     proto.String(storeIterm.Id.Hex()),
					}
					favId = append(favId, iterm)
					duplicateCount += 1
					attrName := "gofav/get_duplicate_id_req_count"
					libcomm.AttrAdd(attrName, 1)
					infoLog.Printf("procAddReq insert failed and exec get duplicate id success uid=%v err=%s",
						head.From,
						err)
				} else {
					attrName := "gofav/get_duplicate_id_failed"
					libcomm.AttrAdd(attrName, 1)
					infoLog.Printf("procAddReq insert failed and exec get duplicate id failed uid=%v err=%s",
						head.From,
						err)
				}
				continue
			}
		}
		// Step2 每次将帖子的计数 递增1
		change := mgo.Change{
			Update:    bson.M{"$inc": bson.M{"favcount": 1}},
			ReturnNew: true,
		}

		var favTs FavorTs
		_, err = mongoConn2.Find(bson.M{"uid": head.From}).Apply(change, &favTs)
		if err != nil {
			infoLog.Printf("procAddReq uid=%v obid=%s inc favcount failed err=%v",
				head.From,
				record.Id.String(),
				err)
			if err == io.EOF {
				RefreshSession(err)
				_, err = mongoConn2.Find(bson.M{"uid": head.From}).Apply(change, &favTs)
				if err != nil {
					infoLog.Printf("procAddReq uid=%v obid=%s retry inc favcount failed err=%v",
						head.From,
						record.Id.String(),
						err)
				}
			} else if err == mgo.ErrNotFound {
				// 更新失败尝试插入记录
				recordTs := FavorTs{
					Uid:      head.From,
					LastTs:   uint32(time.Now().Unix()),
					FavCount: 1,
				}
				err = mongoConn2.Insert(recordTs)
				if err != nil {
					RefreshSession(err)
					infoLog.Printf("procAddReq Insert favorTsTable failed uid=%v err=%s", recordTs.Uid, err)
				}
				// 将mysql 中和Redis中的收藏计数清0
				err = ResetAllFovoriteCount(head.From)
				if err != nil {
					infoLog.Printf("procAddReq ResetAllFovoriteCount uid=%v failed err=%s", head.From, err)
					attrName := "gofav/fav_rest_fav_count_failed"
					libcomm.AttrAdd(attrName, 1)
				}

			} else {
				infoLog.Printf("procAddReq uid=%v obid=%s inc favcount failed err=%v",
					head.From,
					record.Id.String(),
					err)
			}
		}
		// 保存clientid-->serverid的映射关系 注意将id转成string
		iterm := &ht_favorite.FavoriteId{
			ClientId: proto.Uint64(v.GetClientId()),
			Obid:     proto.String(record.Id.Hex()),
		}
		favId = append(favId, iterm)

		if v.GetType() == ht_favorite.TYPE_FAVORATE_TYPE_MNT && len(v.GetMoment().GetMid()) > 0 {
			// 发布nsq notify event
			notifyObj := simplejson.New()
			notifyObj.Set("uid", head.From)
			notifyObj.Set("mid", string(v.GetMoment().GetMid()))
			notifyObj.Set("type", NSQ_EVENT_FAVORITE)
			notifyObj.Set("ts", time.Now().Unix())
			notifySlice, err := notifyObj.MarshalJSON()
			if err != nil {
				infoLog.Printf("procAddReq notifyObj.MarshalJSON failed uid=%v mid=%s err=%s",
					head.From,
					v.GetMoment().GetMid(),
					err)
			} else {
				err = globalProducer.Publish(nsqTopic, notifySlice)
				if err != nil {
					// 统计apns失败总量
					attr := "gofav/add_fav_post_nsq_faild"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("procAddReq pubilsh nsq event failed uid=%v mid=%s err=%s",
						head.From,
						v.GetMoment().GetMid(),
						err)
				} else {
					// 统计apns成功的总量
					attr := "gofav/add_fav_post_nsq_succ"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("procAddReq pubilsh nsq event failed uid=%v mid=%s",
						head.From,
						v.GetMoment().GetMid())
				}
			}
		}
	}

	// 更新版本
	lastTs = uint32(time.Now().Unix())
	err = mongoConn2.Update(bson.M{"uid": head.From}, bson.M{"$set": bson.M{"lastts": lastTs}})
	if err != nil {
		infoLog.Printf("procAddReq Update favorTsTable failed uid=%v err=%s", head.From, err)
		if err == mgo.ErrNotFound {
			// 更新失败尝试插入记录
			recordTs := FavorTs{
				Uid:      head.From,
				LastTs:   uint32(time.Now().Unix()),
				FavCount: 1,
			}
			err = mongoConn2.Insert(recordTs)
			if err != nil {
				RefreshSession(err)
				infoLog.Printf("procAddReq update table ts Insert favorTsTable failed uid=%v err=%s", recordTs.Uid, err)
			}
		} else {
			RefreshSession(err)
		}
	}

	// add all favorite count
	// err = ChangeAllFovoriteCount(head.From, len(favId)-duplicateCount)
	// if err != nil {
	// 	infoLog.Printf("procAddReq ChangeAllFovoriteCount failed uid=%v changeCount=%v err=%v", head.From, len(favId), err)
	// }
	task := &UpdateCountTask{
		Uid:   head.From,
		Count: len(favId) - duplicateCount,
	}
	select {
	case updateCountTaskChan <- task:
		infoLog.Printf("procAddReq message uid=%v count=%v put into chan succ", head.From, len(favId)-duplicateCount)
	}

	infoLog.Printf("procAddReq uid=%v ret=%v idList=%v lastTs=%v", head.From, result, favId, lastTs)
	//send response packet
	rspBody.AddRspbody = &ht_favorite.AddRspBody{
		Status: &ht_favorite.FavHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		IdList: favId,
		LastTs: proto.Uint32(lastTs),
	}
	return true
}

func procDelReq(c *gotcp.Conn, packet *common.HeadV3Packet) bool {
	attrName := "gofav/favorite_rec_del_req"
	libcomm.AttrAdd(attrName, 1)
	rspBody := new(ht_favorite.FavRspBody)
	// parse packet
	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
	var head *common.HeadV3
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("procDelReq Get head faild")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		rspBody.DelRspbody = &ht_favorite.DelRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto get head failed"),
			},
		}
		return false
	}
	reqBody := &ht_favorite.FavReqBody{}
	err = proto.Unmarshal(packet.GetBody(), reqBody)
	if err != nil {
		infoLog.Printf("ProcDelReq proto Unmarshal failed")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.DelRspbody = &ht_favorite.DelRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto unmarshal failed"),
			},
		}
		return false
	}
	delReqBody := reqBody.GetDelReqbody()
	if delReqBody == nil {
		infoLog.Printf("procDelReq GetDelReqbody() failked")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		rspBody.DelRspbody = &ht_favorite.DelRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("GetDelReqbody failed"),
			},
		}
		return false
	}
	infoLog.Printf("procDelReq uid[%v],obidlist[%v]", head.From, delReqBody.GetObidList())

	// write mongodb
	// Step1 删除收藏列表中的记录
	tableName := GetFavTable(head.From)
	mongoConn := mongoSess.DB(favorDB).C(tableName)
	mongoConn2 := mongoSess.DB(favorDB).C(favorTsTable)
	delResList := make([]*ht_favorite.ElementDelStat, len(delReqBody.GetObidList()))
	var lastTimeStamp uint32
	// 只有有一条记录删除出错则返回错误，客户端需要重新请求
	for i, v := range delReqBody.GetObidList() {
		infoLog.Printf("procDelReq index=%v obid=%s", i, v)
		bIsHex := bson.IsObjectIdHex(v)
		if !bIsHex {
			infoLog.Printf("procDelReq obid=%s is not objectIdHex continue", v)
			delResList[i] = &ht_favorite.ElementDelStat{
				Obid: proto.String(v),
				Res:  ht_favorite.TYPE_DEL_RES_TYPE_DEL_FAILD.Enum(),
			}
			continue
		}

		obid := bson.ObjectIdHex(v)
		//TEST
		// query := mongoConn.FindId(obid.String())
		// count, err := query.Count()
		// if err != nil {
		// 	infoLog.Printf("procDelReq uid=%v FindId=%v err=%s", head.From, obid, err)
		// } else {
		// 	infoLog.Printf("procDelReq uid=%v FindId=%v count=%v", head.From, obid, count)
		// }
		// 删除只是更新收藏的状态并不真正的将收藏删除
		err = mongoConn.Update(bson.M{"_id": obid}, bson.M{"$set": bson.M{"favstat": uint32(ht_favorite.FAV_STAT_TYPE_FAV_DELETED), "favver": uint32(time.Now().Unix())}})
		//需要判断一下是否存在 todo
		if err != nil {
			infoLog.Printf("procDelReq uid=%v Id=%v tableName=%s failed err=%s", head.From, obid, tableName, err)
			RefreshSession(err)
			if err == mgo.ErrNotFound {
				infoLog.Printf("procDelReq obid=%s tableName=%s not found continue", v, tableName)
				delResList[i] = &ht_favorite.ElementDelStat{
					Obid: proto.String(v),
					Res:  ht_favorite.TYPE_DEL_RES_TYPE_DEL_SUCCESS.Enum(),
				}
				continue
			} else {
				result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
				rspBody.DelRspbody = &ht_favorite.DelRspBody{
					Status: &ht_favorite.FavHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("mongodb remove failed"),
					},
				}
				return false
			}
		}
		lastTimeStamp = uint32(time.Now().Unix())
		// Step3 update timestamp
		err = mongoConn2.Update(bson.M{"uid": head.From}, bson.M{"$set": bson.M{"lastts": uint32(lastTimeStamp)}})
		if err != nil {
			infoLog.Printf("procDelReq Update favorTsTable failed uid=%v serverId=%s", head.From, obid)
			if err == io.EOF {
				RefreshSession(err)
				err = mongoConn2.Update(bson.M{"uid": head.From}, bson.M{"$set": bson.M{"lastts": uint32(lastTimeStamp)}})
				if err != nil {
					infoLog.Printf("procDelReq Update favorTsTable retry update failed uid=%v serverId=%s err=%s", head.From, obid.Hex(), err)
					result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
					rspBody.DelRspbody = &ht_favorite.DelRspBody{
						Status: &ht_favorite.FavHeader{
							Code:   proto.Uint32(uint32(result)),
							Reason: []byte("update favorTsTable failed"),
						},
					}
					return true
				}
			} else {
				result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
				rspBody.DelRspbody = &ht_favorite.DelRspBody{
					Status: &ht_favorite.FavHeader{
						Code:   proto.Uint32(uint32(result)),
						Reason: []byte("update favorTsTable failed"),
					},
				}
				return true
			}
		}

		// 记录删除成功
		delResList[i] = &ht_favorite.ElementDelStat{
			Obid: proto.String(v),
			Res:  ht_favorite.TYPE_DEL_RES_TYPE_DEL_SUCCESS.Enum(),
		}
		// 判断删除的是否是帖文 如果是帖文打一个事件到NSQ
		retRecord := FavorRecord{}
		err = mongoConn.Find(bson.M{"_id": obid, "type": 5}).One(&retRecord)
		if err == nil && len(retRecord.Mid.Mid) > 0 {
			// 发布nsq notify event
			notifyObj := simplejson.New()
			notifyObj.Set("uid", head.From)
			notifyObj.Set("mid", string(retRecord.Mid.Mid))
			notifyObj.Set("type", NSQ_EVENT_CANCLE_FAVORITE)
			notifyObj.Set("ts", time.Now().Unix())
			notifySlice, err := notifyObj.MarshalJSON()
			if err != nil {
				infoLog.Printf("procDelReq notifyObj.MarshalJSON failed uid=%v mid=%s err=%s",
					head.From,
					retRecord.Mid.Mid,
					err)
			} else {
				err = globalProducer.Publish(nsqTopic, notifySlice)
				if err != nil {
					// 统计apns失败总量
					attr := "gofav/add_fav_post_nsq_faild"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("procDelReq pubilsh nsq event failed uid=%v mid=%s err=%s",
						head.From,
						retRecord.Mid.Mid,
						err)
				} else {
					// 统计apns成功的总量
					attr := "gofav/add_fav_post_nsq_succ"
					libcomm.AttrAdd(attr, 1)
					infoLog.Printf("procDelReq pubilsh nsq event failed uid=%v mid=%s",
						head.From,
						retRecord.Mid.Mid)
				}
			}
		} else {
			infoLog.Printf("procDelReq obid=%s find err=%s", v, err)
		}

	}
	// del all favorite count
	// err = ChangeAllFovoriteCount(head.From, -(len(delResList)))
	// if err != nil {
	// 	infoLog.Printf("procDelReq ChangeAllFovoriteCount failed uid=%v changeConnt=%v err=%s",
	// 		head.From,
	// 		-(len(delResList)),
	// 		err)
	// }
	task := &UpdateCountTask{
		Uid:   head.From,
		Count: -(len(delResList)),
	}
	select {
	case updateCountTaskChan <- task:
		infoLog.Printf("procDelReq message uid=%v count=%v put into chan succ", head.From, -(len(delResList)))
	}

	//Step5 send response packet
	rspBody.DelRspbody = &ht_favorite.DelRspBody{
		Status: &ht_favorite.FavHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("procDelReq success"),
		},
		LastTs:  proto.Uint32(lastTimeStamp),
		DelStat: delResList,
	}
	return true
}

// func procSyncModifyReq(c *gotcp.Conn, packet *common.HeadV3Packet) bool {
// 	attrName := "favorite_sync_modify_req"
// 	libcomm.AttrAdd(attrName, 1)

// 	// parse packet
// 	rspBody := new(ht_favorite.FavRspBody)
// 	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
// 	var head *common.HeadV3
// 	defer func() {
// 		SendRsp(c, head, rspBody, result)
// 	}()

// 	head, err := packet.GetHead()
// 	if err != nil {
// 		infoLog.Printf("procSyncModifyReq Get head faild")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
// 		rspBody.SyncRspbody = &ht_favorite.SyncModifyRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("proto get head failed"),
// 			},
// 		}
// 		return false
// 	}

// 	reqBody := &ht_favorite.FavReqBody{}
// 	err = proto.Unmarshal(packet.GetBody(), reqBody)
// 	if err != nil {
// 		infoLog.Printf("procSyncModifyReq proto Unmarshal failed")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
// 		rspBody.SyncRspbody = &ht_favorite.SyncModifyRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("proto get head failed"),
// 			},
// 		}
// 		return false
// 	}

// 	syncReqBody := reqBody.GetSyncReqbody()
// 	if syncReqBody == nil {
// 		infoLog.Printf("procSyncModifyReq GetSyncReqbody() failked")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
// 		rspBody.SyncRspbody = &ht_favorite.SyncModifyRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("proto get head failed"),
// 			},
// 		}
// 		return false
// 	}
// 	infoLog.Printf("uid[%v], max_order[%v], max_index[%v], last_ts[%v]",
// 		head.From,
// 		syncReqBody.GetMaxObid(),
// 		syncReqBody.GetMaxIndex(),
// 		syncReqBody.GetLastTs())

// 	if syncReqBody.GetMaxObid() == "" {
// 		infoLog.Printf("procSyncModifyReq invalid param obid empty uid=%v", head.From)
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INVALID_PARAM)
// 		rspBody.SyncRspbody = &ht_favorite.SyncModifyRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("invalid obid"),
// 			},
// 		}
// 		return true
// 	}

// 	// 第一步，比较时间戳
// 	retTs := new(FavorTs)
// 	mongoConn2 := mongoSess.DB(favorDB).C(favorTsTable)
// 	err = mongoConn2.Find(bson.M{"uid": head.From}).One(retTs)
// 	if err != nil {
// 		infoLog.Printf("procSyncModifyReq Find failed!")
// 		RefreshSession(err)
// 		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
// 		rspBody.SyncRspbody = &ht_favorite.SyncModifyRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("not find in favorTsTable"),
// 			},
// 		}
// 		return false
// 		//如果找不到时间戳，就认为时间戳是0，还没有初始化过
// 	} else {
// 		// 判断是否还有改动没有同步 使用max_index 即可
// 		if retTs.ModifyIndex == syncReqBody.GetMaxIndex() {
// 			result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_NOT_CHANGE)
// 			rspBody.SyncRspbody = &ht_favorite.SyncModifyRspBody{
// 				Status: &ht_favorite.FavHeader{
// 					Code:   proto.Uint32(uint32(result)),
// 					Reason: []byte("modify table not change"),
// 				},
// 			}
// 			return true
// 		}
// 	}

// 	// 第二步，查询记录，使用limit控制每页条数
// 	retModList := []FavorModify{}
// 	mongoConn := mongoSess.DB(favorDB).C(modifyTable)
// 	// obid := bson.ObjectIdHex(getReqBody.GetMaxObid())
// 	err = mongoConn.Find(bson.M{"uid": head.From,
// 		"index": bson.M{"$gt": syncReqBody.GetMaxIndex()}}).Sort("index").All(&retModList)

// 	if err != nil {
// 		infoLog.Printf("procSyncModifyReq find modifyTable failed uid=%v", head.From)
// 		RefreshSession(err)
// 		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
// 		rspBody.SyncRspbody = &ht_favorite.SyncModifyRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(uint32(result)),
// 				Reason: []byte("find failed"),
// 			},
// 		}
// 		return true
// 	}
// 	modLen := len(retModList)
// 	infoLog.Printf("procSyncModifyReq modLen=%v", modLen)
// 	modifyList := make([]*ht_favorite.ModifyBody, modLen)
// 	var curIndex uint64
// 	var curTs uint32
// 	for i := 0; i < modLen; i++ {
// 		curIndex = retModList[i].Index
// 		curTs = retModList[i].ModTs
// 		modifyType := ht_favorite.TYPE_MODIFY(retModList[i].Type)
// 		modifyList[i] = &ht_favorite.ModifyBody{
// 			Obid:     proto.String(retModList[i].Obid),
// 			ModifyTs: proto.Uint32(curTs),
// 			Index:    proto.Uint64(curIndex),
// 			Type:     &modifyType,
// 		}
// 	}
// 	rspBody.SyncRspbody = &ht_favorite.SyncModifyRspBody{
// 		Status: &ht_favorite.FavHeader{
// 			Code:   proto.Uint32(uint32(result)),
// 			Reason: []byte("success"),
// 		},
// 		ModifyList: modifyList,
// 		LastTs:     proto.Uint32(curTs),
// 		MaxIndex:   proto.Uint64(curIndex),
// 	}

// 	return true
// }

func procGetLatestFavObidReq(c *gotcp.Conn, packet *common.HeadV3Packet) bool {
	attrName := "gofav/favorite_recv_get_req"
	libcomm.AttrAdd(attrName, 1)
	// parse packet
	rspBody := new(ht_favorite.FavRspBody)
	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
	var head *common.HeadV3
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("procGetLatestFavObidReq Get head faild")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		rspBody.GetLatestFavObidRspbody = &ht_favorite.GetLatestFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto get head failed"),
			},
		}
		return false
	}

	reqBody := &ht_favorite.FavReqBody{}
	err = proto.Unmarshal(packet.GetBody(), reqBody)
	if err != nil {
		infoLog.Printf("procGetLatestFavObidReq proto Unmarshal failed")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.GetLatestFavObidRspbody = &ht_favorite.GetLatestFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto Unmarshal failed"),
			},
		}
		return false
	}

	getReqBody := reqBody.GetGetLatestFavObidReqbody()
	if getReqBody == nil {
		infoLog.Printf("procGetReq GetGetLatestFavObidReqbody() failked")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.GetLatestFavObidRspbody = &ht_favorite.GetLatestFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(uint32(result))),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	infoLog.Printf("procGetLatestFavObidReq uid[%v],boid[%s], last_ts[%v]", head.From, getReqBody.GetObid(), getReqBody.GetLastTs())

	// 第一步，比较时间戳
	ts := time.Now().Unix()
	retTs := new(FavorTs)
	bTsExist := false
	mongoConn2 := mongoSess.DB(favorDB).C(favorTsTable)
	err = mongoConn2.Find(bson.M{"uid": head.From}).One(retTs)
	if err != nil {
		bTsExist = false
		infoLog.Printf("procGetLatestFavObidReq Find failed! err=%v", err)
		RefreshSession(err)
		//如果找不到时间戳，就认为时间戳是0，还没有初始化过
	} else {
		bTsExist = true
		if retTs.LastTs == getReqBody.GetLastTs() {
			result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_NOT_CHANGE)
			rspBody.GetLatestFavObidRspbody = &ht_favorite.GetLatestFavObidRspBody{
				Status: &ht_favorite.FavHeader{
					Code:   proto.Uint32(uint32(result)),
					Reason: []byte("favorTable not change"),
				},
			}
			return true
		}
	}

	// 第二步，查询记录，使用limit控制每页条数
	// 检查是否是objectId 的hex表达式
	//bIsHex := bson.IsObjectIdHex(getReqBody.GetObid())
	//if !bIsHex {
	//	infoLog.Printf("procGetLatestFavObidReq uid=%v obid=%s is illegal", head.From, getReqBody.GetObid())
	//	if getReqBody.GetObid() != "0" {
	//		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INVALID_PARAM)
	//		rspBody.GetLatestFavObidRspbody = &ht_favorite.GetLatestFavObidRspBody{
	//			Status: &ht_favorite.FavHeader{
	//				Code:   proto.Uint32(uint32(result)),
	//				Reason: []byte("invalid param"),
	//			},
	//		}
	//		return false
	//	} else {
	//		beginHex := GetBeginHexString()
	//		getReqBody.Obid = proto.String(beginHex)
	//		infoLog.Printf("procGetLatestFavObidReq uid=%v set dump hex=%s", head.From, beginHex)
	//	}
	//}
	//当TS已近发生变更了，说明已近同步回去的内容发生了变更所以需要重新将obid做了哪种变更返回去
	getReqBody.Obid = proto.String(GetBeginHexString())

	retRecList := []FavorRecord{}
	tableName := GetFavTable(head.From)
	mongoConn := mongoSess.DB(favorDB).C(tableName)
	obid := bson.ObjectIdHex(getReqBody.GetObid())

	//	err = mongoConn.Find(bson.M{"uid": head.From, "_id": bson.M{"$gt": obid}}).Sort("-_id").Limit(pageSize).All(&retRecList)
	err = mongoConn.Find(bson.M{"uid": head.From, "_id": bson.M{"$gt": obid}}).Sort("-favts").Limit(pageSize).All(&retRecList)
	if err != nil {
		infoLog.Printf("procGetLatestFavObidReq find failed")
		RefreshSession(err)
		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
		rspBody.GetLatestFavObidRspbody = &ht_favorite.GetLatestFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("favorTable find failed"),
			},
		}
		return true
	}

	if len(retRecList) == 0 {
		infoLog.Printf("procGetFavByObidReq no more uid=%v", head.From)
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_NO_MORE)
		rspBody.GetLatestFavObidRspbody = &ht_favorite.GetLatestFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("favorTable no more"),
			},
		}
		return true
	} else {
		infoLog.Printf("procGetLatestFavObidReq Get size=%v", len(retRecList))
		for i, v := range retRecList {
			infoLog.Printf("procGetLatestFavObidReq index=%v obid=%v", i, v.Id.Hex())
		}
	}

	//step3，update last_ts
	var lastTs uint32
	if !bTsExist {
		lastTs = uint32(ts)
	} else {
		lastTs = retTs.LastTs
	}
	var hasMore uint32
	if len(retRecList) == pageSize {
		hasMore = uint32(1)
	} else {
		hasMore = uint32(0)
	}
	contenList := make([]*ht_favorite.ObidStat, len(retRecList))
	for i, v := range retRecList {
		favStat := ht_favorite.FAV_STAT(v.FavStat)
		contenList[i] = &ht_favorite.ObidStat{
			Obid:    proto.String(v.Id.Hex()),
			Stat:    favStat.Enum(),
			Version: proto.Uint32(v.FavVer),
		}
	}

	rspBody.GetLatestFavObidRspbody = &ht_favorite.GetLatestFavObidRspBody{
		Status: &ht_favorite.FavHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ObidList: contenList,
		More:     proto.Uint32(hasMore),
		LastTs:   proto.Uint32(lastTs),
	}

	return true
}

func procUpdateReq(c *gotcp.Conn, packet *common.HeadV3Packet) bool {
	attrName := "gofav/favorite_recv_update_req"
	libcomm.AttrAdd(attrName, 1)
	// parse packet
	rspBody := new(ht_favorite.FavRspBody)

	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
	var head *common.HeadV3
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("procUpdateReq Get head faild")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto get head failed"),
			},
		}
		return false
	}

	reqBody := new(ht_favorite.FavReqBody)
	err = proto.Unmarshal(packet.GetBody(), reqBody)
	if err != nil {
		infoLog.Println("procUpdateReq proto Unmarshal failed")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto Unmarshal failed"),
			},
		}
		return false
	}
	updateReq := reqBody.GetUpdateReqbody()
	if updateReq == nil {
		infoLog.Printf("procUpdateReq GetUpdateReqBody() failked uid=%v", head.From)
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("GetUpdateReqBody failed"),
			},
		}
		return false
	}
	// 检查是否是objectId 的hex表达式
	bIsHex := bson.IsObjectIdHex(updateReq.GetObid())
	if !bIsHex {
		infoLog.Printf("procUpdateReq uid=%v obid=%s is illegal", head.From, updateReq.GetObid())
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INVALID_PARAM)
		rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}

	obid := bson.ObjectIdHex(updateReq.GetObid())
	updateType := updateReq.GetType()
	text := updateReq.GetText()
	voice := updateReq.GetVoice()
	tags := updateReq.GetTags()
	infoLog.Printf("procUpdateReq obid=%s type=%v text=%#v voice=%#v tags=%v",
		obid.String(),
		updateType,
		text,
		voice,
		tags)

	if updateReq.GetObid() == "" {
		infoLog.Printf("procUpdateReq uid=%v empty obid", head.From)
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INVALID_PARAM)
		rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}

	retRecord := FavorRecord{}
	tableName := GetFavTable(head.From)
	mongoConn := mongoSess.DB(favorDB).C(tableName)
	err = mongoConn.FindId(obid).One(&retRecord)
	// 这里要判断是否找到了
	if err != nil {
		infoLog.Printf("procUpdateReq find failed uid=%v obid=%s", head.From, obid.String())
		RefreshSession(err)
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_REPEAT_ADD)
		rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("find failed"),
			},
		}
		return true
	}
	switch updateType {
	case ht_favorite.TYPE_UPDATE_TYPE_UPDATE_TEXT:
		retRecord.Text = TextInfo{
			Content:       string(text.GetContent()),
			Translate:     string(text.GetTranslate()),
			ContentTran:   string(text.GetContentTran()),
			TranslateTran: string(text.GetTranslateTran()),
		}
	case ht_favorite.TYPE_UPDATE_TYPE_UPDATE_VOICE:
		retRecord.Voice = VoiceInfo{
			Url:      string(voice.GetUrl()),
			Duration: voice.GetDuration(),
			Size:     voice.GetSize(),
			VoiceToText: TextInfo{
				Content:       string(voice.GetVoiceToText().GetContent()),
				Translate:     string(voice.GetVoiceToText().GetTranslate()),
				ContentTran:   string(voice.GetVoiceToText().GetContentTran()),
				TranslateTran: string(voice.GetVoiceToText().GetTranslateTran()),
			},
		}

	case ht_favorite.TYPE_UPDATE_TYPE_UPDATE_TAGS:
		retRecord.Tags = tags

	case ht_favorite.TYPE_UPDATE_TYPE_UPDATE_ALL:
		retRecord.Text = TextInfo{
			Content:       string(text.GetContent()),
			Translate:     string(text.GetTranslate()),
			ContentTran:   string(text.GetContentTran()),
			TranslateTran: string(text.GetTranslateTran()),
		}
		retRecord.Voice = VoiceInfo{
			Url:      string(voice.GetUrl()),
			Duration: voice.GetDuration(),
			Size:     voice.GetSize(),
			VoiceToText: TextInfo{
				Content:       string(voice.GetVoiceToText().GetContent()),
				Translate:     string(voice.GetVoiceToText().GetTranslate()),
				ContentTran:   string(voice.GetVoiceToText().GetContentTran()),
				TranslateTran: string(voice.GetVoiceToText().GetTranslateTran()),
			},
		}
		retRecord.Tags = tags
	}
	retRecord.FavStat = uint32(ht_favorite.FAV_STAT_TYPE_FAV_MODIFY)
	retRecord.FavVer = uint32(time.Now().Unix())
	_, err = mongoConn.Upsert(bson.M{"_id": obid}, retRecord)
	if err != nil {
		infoLog.Printf("procUpdateReq Upsert favor record failed uid=%v obid=%s", head.From, obid.String())
		RefreshSession(err)
		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
		rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("update favor failed"),
			},
		}
		return false
	}
	mongoConn2 := mongoSess.DB(favorDB).C(favorTsTable)
	recordTs := &FavorTs{
		Uid:      head.From,
		LastTs:   uint32(time.Now().Unix()),
		FavCount: 0,
	}
	err = mongoConn2.Update(bson.M{"uid": recordTs.Uid}, bson.M{"$set": bson.M{"lastts": recordTs.LastTs}})
	if err != nil {
		infoLog.Printf("procUpdateReq Update favorTsTable failed uid=%v serverId=%s", recordTs.Uid, obid)
		RefreshSession(err)
		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
		rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("update favorTsTable failed"),
			},
		}
		return true
	}

	//send response packet
	rspBody.UpdateRspbody = &ht_favorite.UpdateRspBody{
		Status: &ht_favorite.FavHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		LastTs: proto.Uint32(recordTs.LastTs),
	}

	return true
}

func procGetFavByObidReq(c *gotcp.Conn, packet *common.HeadV3Packet) bool {
	attrName := "gofav/favorite_get_by_orbid_req"
	libcomm.AttrAdd(attrName, 1)
	// parse packet
	rspBody := new(ht_favorite.FavRspBody)
	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
	var head *common.HeadV3
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("procGetByObidReq Get head faild")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		rspBody.GetByObidRspbody = &ht_favorite.GetFavByObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto get head failed"),
			},
		}
		return false
	}

	reqBody := &ht_favorite.FavReqBody{}
	err = proto.Unmarshal(packet.GetBody(), reqBody)
	if err != nil {
		infoLog.Printf("procGetByObidReq proto Unmarshal failed")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.GetByObidRspbody = &ht_favorite.GetFavByObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto Unmarshal failed"),
			},
		}
		return false
	}

	subReqBody := reqBody.GetGetByObidReqbody()
	if subReqBody == nil {
		infoLog.Printf("procGetByObidReq GetGetByObidReqbody() failked")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.GetByObidRspbody = &ht_favorite.GetFavByObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	idList := subReqBody.GetObidList()
	var contList []*ht_favorite.FavoriteContent
	tableName := GetFavTable(head.From)
	mongoConn := mongoSess.DB(favorDB).C(tableName)
	for i, v := range idList {
		infoLog.Printf("procGetByObidReq uid[%v],index[%v], id[%s]", head.From, i, v)
		// 第二步，查询记录，使用limit控制每页条数
		retRecord := FavorRecord{}
		// 检查是否是objectId 的hex表达式
		bIsHex := bson.IsObjectIdHex(v)
		if !bIsHex {
			infoLog.Printf("procGetByObidReq uid=%v obid=%s is illegal", head.From, v)
			continue
		}

		obid := bson.ObjectIdHex(v)
		err = mongoConn.FindId(obid).One(&retRecord)
		if err != nil {
			infoLog.Printf("procGetByObidReq find failed uid=%v obid=%s", head.From, v)
			RefreshSession(err)
			continue
		}
		favType := ht_favorite.TYPE_FAVORATE(retRecord.Type)
		cor := new(ht_favorite.FavCorrectContent)
		cor.Comment = []byte(retRecord.Correction.Comment)
		for _, element := range retRecord.Correction.Body {
			sentCor := &ht_favorite.SentenceCorrect{
				Source: []byte(element.Source),
				Target: []byte(element.Target),
			}
			cor.Body = append(cor.Body, sentCor)
		}
		favIterm := &ht_favorite.FavoriteContent{
			Obid:   proto.String(retRecord.Id.Hex()),
			SrcUid: proto.Uint32(retRecord.SrcUid),
			Type:   &favType,
			Text: &ht_favorite.FavTextBody{
				Content:       []byte(retRecord.Text.Content),
				Translate:     []byte(retRecord.Text.Translate),
				ContentTran:   []byte(retRecord.Text.ContentTran),
				TranslateTran: []byte(retRecord.Text.TranslateTran),
				WordBody: &ht_favorite.FavWordBody{
					Content:     []byte(retRecord.Word.Content),
					Translate:   []byte(retRecord.Word.Translate),
					ContentTran: []byte(retRecord.Word.ContentTran),
				},
			},
			Voice: &ht_favorite.FavVoiceBody{
				Url:      []byte(retRecord.Voice.Url),
				Duration: proto.Uint32(retRecord.Voice.Duration),
				Size:     proto.Uint32(retRecord.Voice.Size),
				VoiceToText: &ht_favorite.FavTextBody{
					Content:       []byte(retRecord.Voice.VoiceToText.Content),
					Translate:     []byte(retRecord.Voice.VoiceToText.Translate),
					ContentTran:   []byte(retRecord.Voice.VoiceToText.ContentTran),
					TranslateTran: []byte(retRecord.Voice.VoiceToText.TranslateTran),
				},
			},
			Correction: cor,
			Moment: &ht_favorite.MntBody{
				Mid:          []byte(retRecord.Mid.Mid),
				PictureCount: proto.Uint32(retRecord.Mid.PictureCount),
				UrlInfo: &ht_favorite.FavURLInfo{
					Url:         []byte(retRecord.Mid.Url.Url),
					ThumbUrl:    []byte(retRecord.Mid.Url.ThumbUrl),
					Description: []byte(retRecord.Mid.Url.Description),
					Title:       []byte(retRecord.Mid.Url.Title),
				},
			},
			CommentId: proto.String(retRecord.CommentId),
			Image: &ht_favorite.FavImageBody{
				ThumbUrl: []byte(retRecord.Image.ThumbUrl),
				BigUrl:   []byte(retRecord.Image.BigUrl),
				Width:    proto.Uint32(retRecord.Image.Width),
				Height:   proto.Uint32(retRecord.Image.Height),
			},
			Tags:     retRecord.Tags,
			AddTs:    proto.Uint64(retRecord.FavTs),
			ClientId: proto.Uint64(uint64(retRecord.ClientId)),
		}
		// 添加到返回列表中
		contList = append(contList, favIterm)
	}
	rspBody.GetByObidRspbody = &ht_favorite.GetFavByObidRspBody{
		Status: &ht_favorite.FavHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ContentList: contList,
	}
	return true
}

func procGetHistoryFavObidReq(c *gotcp.Conn, packet *common.HeadV3Packet) bool {
	attrName := "gofav/favorite_recv_get_history_fav_req"
	libcomm.AttrAdd(attrName, 1)
	// parse packet
	rspBody := new(ht_favorite.FavRspBody)
	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
	var head *common.HeadV3
	defer func() {
		SendRsp(c, head, rspBody, result)
	}()

	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("procGetHistoryFavReq Get head faild")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
		rspBody.GetHistoryFavRspbody = &ht_favorite.GetHistoryFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto get head failed"),
			},
		}
		return false
	}

	reqBody := &ht_favorite.FavReqBody{}
	err = proto.Unmarshal(packet.GetBody(), reqBody)
	if err != nil {
		infoLog.Printf("procGetHistoryFavReq proto Unmarshal failed")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.GetHistoryFavRspbody = &ht_favorite.GetHistoryFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("proto Unmarshal failed"),
			},
		}
		return false
	}

	getHistoryFavReqBody := reqBody.GetGetHistoryFavReqbody()
	if getHistoryFavReqBody == nil {
		infoLog.Printf("procGetHistoryFavReq GetGetReqbody() failked")
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
		rspBody.GetHistoryFavRspbody = &ht_favorite.GetHistoryFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(uint32(result))),
				Reason: []byte("get req body failed"),
			},
		}
		return false
	}
	infoLog.Printf("procGetHistoryFavReq uid[%v],obid[%s]", head.From, getHistoryFavReqBody.GetObid())

	// 第二步，查询记录，使用limit控制每页条数
	// 检查是否是objectId 的hex表达式
	bIsHex := bson.IsObjectIdHex(getHistoryFavReqBody.GetObid())
	if !bIsHex {
		infoLog.Printf("procGetHistoryFavReq uid=%v obid=%s is illegal", head.From, getHistoryFavReqBody.GetObid())
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INVALID_PARAM)
		rspBody.GetHistoryFavRspbody = &ht_favorite.GetHistoryFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("invalid param"),
			},
		}
		return false
	}

	// 第三步 通过id来查询对应记录的时间戳
	tableName := GetFavTable(head.From)
	mongoConn := mongoSess.DB(favorDB).C(tableName)
	obid := bson.ObjectIdHex(getHistoryFavReqBody.GetObid())
	var favTs uint64
	var storeIterm FavorRecord
	err = mongoConn.Find(bson.M{"_id": obid}).One(&storeIterm)
	if err != nil {
		attrName := "gofav/get_history_ts_by_id_failed"
		libcomm.AttrAdd(attrName, 1)
		infoLog.Printf("procGetHistoryFavReq get ts by id failed uid=%v obid=%s err=%s", head.From, getHistoryFavReqBody.GetObid(), err)
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
		rspBody.GetHistoryFavRspbody = &ht_favorite.GetHistoryFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("empty history"),
			},
		}
		return false
	} else {
		favTs = storeIterm.FavTs
		infoLog.Printf("procGetHistoryFavReq uid=%v obid=%s ts=%v", head.From, getHistoryFavReqBody.GetObid(), favTs)
	}
	retRecList := []FavorRecord{}
	//	err = mongoConn.Find(bson.M{"uid": head.From, "_id": bson.M{"$lt": obid}}).Sort("-_id").Limit(pageSize).All(&retRecList)
	err = mongoConn.Find(bson.M{"uid": head.From, "favts": bson.M{"$lt": favTs}, "_id": bson.M{"$ne": obid}}).Sort("-favts").Limit(pageSize).All(&retRecList)
	if err != nil {
		infoLog.Printf("procGetHistoryFavReq find failed")
		RefreshSession(err)
		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
		rspBody.GetHistoryFavRspbody = &ht_favorite.GetHistoryFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("favorTable find failed"),
			},
		}
		return true
	}

	if len(retRecList) == 0 {
		infoLog.Printf("procGetHistoryFavReq no more uid=%v", head.From)
		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_NO_MORE)
		rspBody.GetHistoryFavRspbody = &ht_favorite.GetHistoryFavObidRspBody{
			Status: &ht_favorite.FavHeader{
				Code:   proto.Uint32(uint32(result)),
				Reason: []byte("favorTable no more"),
			},
		}
		return true
	} else {
		infoLog.Printf("procGetHistoryFavReq Get size=%v", len(retRecList))
	}

	var hasMore uint32
	if len(retRecList) == pageSize {
		hasMore = uint32(1)
	} else {
		hasMore = uint32(0)
	}
	obidList := make([]*ht_favorite.ObidStat, len(retRecList))
	for i, v := range retRecList {
		favStat := ht_favorite.FAV_STAT(v.FavStat)
		obidList[i] = &ht_favorite.ObidStat{
			Obid:    proto.String(v.Id.Hex()),
			Stat:    favStat.Enum(),
			Version: proto.Uint32(v.FavVer),
		}
	}

	rspBody.GetHistoryFavRspbody = &ht_favorite.GetHistoryFavObidRspBody{
		Status: &ht_favorite.FavHeader{
			Code:   proto.Uint32(uint32(result)),
			Reason: []byte("success"),
		},
		ObidList: obidList,
		More:     proto.Uint32(hasMore),
	}

	return true
}

// func procUpdateTagReq(c *gotcp.Conn, p gotcp.Packet) bool {
// 	// parse packet
// 	rspBody := new(ht_favorite.FavRspBody)

// 	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
// 	var head *common.HeadV3
// 	defer func() {
// 		SendRsp(c, head, rspBody, result)
// 	}()
// 	packet, ok := p.(*common.HeadV3Packet)
// 	if !ok {
// 		infoLog.Println("Convert to HeadV3Packet failed")
// 		result = uint16(ht_favorite.RET_CODE_RET_INPUT_PARAM_ERR)
// 		return false
// 	}
// 	head, err := packet.GetHead()
// 	if err != nil {
// 		infoLog.Println("ProcData Get head faild")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
// 		return false
// 	}
// 	reqBody := new(ht_favorite.FavReqBody)
// 	err = proto.Unmarshal(packet.GetBody(), reqBody)
// 	if err != nil {
// 		infoLog.Println("proto Unmarshal failed")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
// 		return false
// 	}
// 	updateTagReqBody := reqBody.GetUpdateTagReqbody()
// 	if updateTagReqBody == nil {
// 		infoLog.Println("GetAddReqbody() failked")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
// 		return false
// 	}
// 	obid := bson.ObjectIdHex(string(updateTagReqBody.GetObid()))
// 	retRecord := FavorRecord{}
// 	tableName := GetFavTable(head.From)
// 	mongoConn := mongoSess.DB(favorDB).C(tableName)
// 	err = mongoConn.FindId(obid).One(&retRecord)
// 	// 这里要判断是否找到了
// 	if err != nil {
// 		infoLog.Println("find failed")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_REPEAT_ADD)
// 		return true
// 	}

// 	retRecord.Tags = updateTagReqBody.Tags
// 	_, err = mongoConn.Upsert(bson.M{"_id": obid}, retRecord)
// 	if err != nil {
// 		infoLog.Println("Upsert favor record failed")
// 		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
// 		return false
// 	}
// 	ts := time.Now().Unix()
// 	recordTs := new(FavorTs)
// 	recordTs.Uid = head.From
// 	recordTs.LastTs = uint32(ts)
// 	mongoConn2 := mongoSess.DB(favorDB).C(favorTsTable)
// 	_, err = mongoConn2.Upsert(bson.M{"uid": recordTs.Uid}, recordTs)
// 	if err != nil {
// 		infoLog.Println("Upsert ts failed")
// 		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
// 		return true
// 	}

// 	//send response packet
// 	UpdateTagRspBody := rspBody.GetAddRspbody()
// 	UpdateTagRspBody.LastTs = proto.Uint32(uint32(ts))

// 	return true
// }

// func procSearchTagReq(c *gotcp.Conn, packet common.HeadV3Packet) bool {
// 	// parse packet
// 	rspBody := new(ht_favorite.FavRspBody)
// 	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
// 	var head *common.HeadV3
// 	defer func() {
// 		SendRsp(c, head, rspBody, result)
// 	}()

// 	head, err := packet.GetHead()
// 	if err != nil {
// 		infoLog.Println("procSearchTagReq Get head faild")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
// 		rspBody.SearchTagRspbody = &ht_favorite.SearchTagRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(result),
// 				Reason: []byte("proto get head failed"),
// 			},
// 		}
// 		return false
// 	}

// 	reqBody := &ht_favorite.FavReqBody{}
// 	err = proto.Unmarshal(packet.GetBody(), reqBody)
// 	if err != nil {
// 		infoLog.Println("proto Unmarshal failed")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
// 		rspBody.SearchTagRspbody = &ht_favorite.SearchTagRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(result),
// 				Reason: []byte("proto Unmarshal failed"),
// 			},
// 		}
// 		return false
// 	}

// 	subReqBody := reqBody.GetSearchTagReqbody()
// 	if subReqBody == nil {
// 		infoLog.Println("GetSearchTagReqbody() failked")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
// 		rspBody.SearchTagRspbody = &ht_favorite.SearchTagRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(result),
// 				Reason: []byte("GetSearchTagReqbody failed"),
// 			},
// 		}
// 		return false
// 	}
// 	infoLog.Printf("uid[%v],search_tag[%v], last_ts[%v]", head.From, subReqBody.GetTag(), subReqBody.GetLastTs())

// 	// 第一步，比较时间戳，服务端的时间戳可以从objectid中获取
// 	ts := time.Now().Unix()
// 	retTs := new(FavorTs)
// 	bTsExist := false
// 	mongoConn2 := mongoSess.DB(favorDB).C(favorTsTable)
// 	err = mongoConn2.Find(bson.M{"uid": head.From}).One(retTs)
// 	if err != nil {
// 		bTsExist = false
// 		infoLog.Println("Find failed!")
// 		//如果找不到时间戳，就认为时间戳是0，还没有初始化过
// 	} else {
// 		bTsExist = true
// 		if retTs.LastTs == searchTagReqBody.GetLastTs() {
// 			result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_NOT_CHANGE)
// 			rspBody.SearchTagRspbody = &ht_favorite.SearchTagRspBody{
// 				Status: &ht_favorite.FavHeader{
// 					Code:   proto.Uint32(result),
// 					Reason: []byte("fav list not change"),
// 				},
// 			}
// 			return true
// 		}
// 	}

// 	// 第二步，查询记录，使用limit控制每页条数
// 	retRecList := []FavorRecord{}
// 	tableName := GetFavTable(head.From)
// 	mongoConn := mongoSess.DB(favorDB).C(tableName)
// 	obid := searchTagReqBody.GetIndex()
// 	if obid == "" {
// 		err = mongoConn.Find(bson.M{"uid": head.From, "$text": bson.M{"$search": searchTagReqBody.GetTag()}}).Sort("-_id").Limit(pageSize).All(&retRecList)
// 	} else {
// 		err = mongoConn.Find(bson.M{"uid": head.From, "$text": bson.M{"$search": searchTagReqBody.GetTag()}, "_id": bson.M{"$lte": obid}}).Sort("-_id").Limit(pageSize).All(&retRecList)
// 	}
// 	if err != nil {
// 		infoLog.Printf("procSearchTagReq search failed failed")
// 		RefreshSession(err)
// 		result = uint16(ht_favorite.FAV_RET_CODE_Fav_RET_DB_ERR)
// 		rspBody.SearchTagRspbody = &ht_favorite.SearchTagRspBody{
// 			Status: &ht_favorite.FavHeader{
// 				Code:   proto.Uint32(result),
// 				Reason: []byte("search failed"),
// 			},
// 		}
// 		return true
// 	}

// 	//step3，update last_ts
// 	searchTagRspBody := rspBody.GetSearchTagRspbody()
// 	if !bTsExist {
// 		searchTagRspBody.LastTs = new(uint32)
// 		*searchTagRspBody.LastTs = uint32(ts)
// 	} else {
// 		searchTagRspBody.LastTs = new(uint32)
// 		*searchTagRspBody.LastTs = retTs.LastTs
// 	}
// 	if len(retRecList) == pageSize {
// 		searchTagRspBody.More = new(uint32)
// 		*searchTagRspBody.More = 1
// 	} else {
// 		searchTagRspBody.More = new(uint32)
// 		*searchTagRspBody.More = 0
// 	}
// 	if len(retRecList) == 0 {
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_NO_MORE)
// 		searchTagRspBody.Status = &ht_favorite.FavHeader{
// 			Code:   proto.Uint32(result),
// 			Reason: []byte("search failed"),
// 		}
// 		searchTagRspBody.Index = proto.String(retRecList[len(retRecList)-1].Id.String())
// 		searchTagRspBody.ContentList = make([]*ht_favorite.FavoriteContent, len(retRecList))
// 		for i := 0; i < len(retRecList); i++ {
// 			searchTagRspBody.ContentList[i] = new(ht_favorite.FavoriteContent)
// 			searchTagRspBody.ContentList[i].SrcUid = proto.Uint32(retRecList[i].SrcUid)
// 			searchTagRspBody.ContentList[i].Type = new(ht_favorite.TYPE_FAVORATE)
// 			*searchTagRspBody.ContentList[i].Type = ht_favorite.TYPE_FAVORATE(retRecList[i].Type)
// 			searchTagRspBody.ContentList[i].Text = []byte(retRecList[i].Text)
// 			searchTagRspBody.ContentList[i].Voice = new(ht_favorite.VoiceBody)
// 			searchTagRspBody.ContentList[i].Voice.Url = []byte(retRecList[i].Voice.Url)
// 			searchTagRspBody.ContentList[i].Voice.Duration = proto.Uint32(retRecList[i].Voice.Duration)
// 			searchTagRspBody.ContentList[i].Voice.Size = proto.Uint32(retRecList[i].Voice.Size)
// 			searchTagRspBody.ContentList[i].Image = new(ht_favorite.FavImageBody)
// 			searchTagRspBody.ContentList[i].Image.ThumbUrl = []byte(retRecList[i].Image.ThumbUrl)
// 			searchTagRspBody.ContentList[i].Image.BigUrl = []byte(retRecList[i].Image.BigUrl)
// 			searchTagRspBody.ContentList[i].Image.Width = proto.Uint32(retRecList[i].Image.Width)
// 			searchTagRspBody.ContentList[i].Image.Height = proto.Uint32(retRecList[i].Image.Height)
// 			searchTagRspBody.ContentList[i].Mid = []byte(retRecList[i].Mid)
// 			searchTagRspBody.ContentList[i].Tags = retRecList[i].Tags
// 		}
// 	}

// 	return true
// }

// func procSearchTextReq(c *gotcp.Conn, p gotcp.Packet) bool {
// 	// parse packet
// 	rspBody := new(ht_favorite.FavRspBody)
// 	result := uint16(ht_favorite.FAV_RET_CODE_FAV_RET_SUCCESS)
// 	var head *common.HeadV3
// 	defer func() {
// 		SendRsp(c, head, rspBody, result)
// 	}()
// 	packet, ok := p.(*common.HeadV3Packet)
// 	if !ok {
// 		infoLog.Println("Convert to HeadV3Packet failed")
// 		result = uint16(ht_favorite.RET_CODE_RET_INPUT_PARAM_ERR)
// 		return false
// 	}
// 	head, err := packet.GetHead()
// 	if err != nil {
// 		infoLog.Println("ProcData Get head faild")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_INTERNAL_ERR)
// 		return false
// 	}

// 	reqBody := &ht_favorite.FavReqBody{}
// 	err = proto.Unmarshal(packet.GetBody(), reqBody)
// 	if err != nil {
// 		infoLog.Println("proto Unmarshal failed")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
// 		return false
// 	}

// 	searchTextReqBody := reqBody.GetSearchTextReqbody()
// 	if searchTextReqBody == nil {
// 		infoLog.Println("GetSearchTagReqbody() failked")
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_PB_ERR)
// 		return false
// 	}
// 	infoLog.Printf("uid[%v],search_text[%v], last_ts[%v]", packet.GetFromUid(), searchTextReqBody.GetText(), searchTextReqBody.GetLastTs())

// 	// 第一步，比较时间戳，服务端的时间戳可以从objectid中获取
// 	ts := time.Now().Unix()
// 	retTs := new(FavorTs)
// 	bTsExist := false
// 	mongoConn2 := mongoSess.DB(favorDB).C(favorTsTable)
// 	err = mongoConn2.Find(bson.M{"uid": head.From}).One(retTs)
// 	if err != nil {
// 		bTsExist = false
// 		infoLog.Println("Find failed!")
// 		//如果找不到时间戳，就认为时间戳是0，还没有初始化过
// 	} else {
// 		bTsExist = true
// 		if retTs.LastTs == searchTextReqBody.GetLastTs() {
// 			result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_NOT_CHANGE)
// 			return true
// 		}
// 	}

// 	// 第二步，查询记录，使用limit控制每页条数
// 	retRecList := []FavorRecord{}
// 	mongoConn := mongoSess.DB(favorDB).C(favorRecordTable)
// 	obid := searchTextReqBody.GetIndex()
// 	if obid == "" {
// 		err = mongoConn.Find(bson.M{"uid": head.From, "$text": bson.M{"$search": searchTextReqBody.GetText()}}).Sort("-_id").Limit(pageSize).All(&retRecList)
// 	} else {
// 		err = mongoConn.Find(bson.M{"uid": head.From, "$text": bson.M{"$search": searchTextReqBody.GetText()}, "_id": bson.M{"$lte": obid}}).Sort("-_id").Limit(pageSize).All(&retRecList)
// 	}
// 	if err != nil {
// 		infoLog.Println("Insert failed")
// 	}

// 	//step3，update last_ts
// 	searchTextRspBody := rspBody.GetSearchTagRspbody()
// 	if !bTsExist {
// 		searchTextRspBody.LastTs = new(uint32)
// 		*searchTextRspBody.LastTs = uint32(ts)
// 	} else {
// 		searchTextRspBody.LastTs = new(uint32)
// 		*searchTextRspBody.LastTs = retTs.LastTs
// 	}
// 	if len(retRecList) == pageSize {
// 		searchTextRspBody.More = new(uint32)
// 		*searchTextRspBody.More = 1
// 	} else {
// 		searchTextRspBody.More = new(uint32)
// 		*searchTextRspBody.More = 0
// 	}
// 	if len(retRecList) == 0 {
// 		result = uint16(ht_favorite.FAV_RET_CODE_FAV_RET_NO_MORE)
// 	} else {
// 		searchTextRspBody.Index = proto.String(retRecList[len(retRecList)-1].Id)
// 		searchTextRspBody.ContentList = make([]*ht_favorite.FavoriteContent, len(retRecList))
// 		for i := 0; i < len(retRecList); i++ {
// 			searchTextRspBody.ContentList[i] = new(ht_favorite.FavoriteContent)
// 			searchTextRspBody.ContentList[i].SrcUid = proto.Uint32(retRecList[i].SrcUid)
// 			searchTextRspBody.ContentList[i].Type = new(ht_favorite.TYPE_FAVORATE)
// 			*searchTextRspBody.ContentList[i].Type = ht_favorite.TYPE_FAVORATE(retRecList[i].Type)
// 			searchTextRspBody.ContentList[i].Text = []byte(retRecList[i].Text)
// 			searchTextRspBody.ContentList[i].Voice = new(ht_favorite.VoiceBody)
// 			searchTextRspBody.ContentList[i].Voice.Url = []byte(retRecList[i].Voice.Url)
// 			searchTextRspBody.ContentList[i].Voice.Duration = proto.Uint32(retRecList[i].Voice.Duration)
// 			searchTextRspBody.ContentList[i].Voice.Size = proto.Uint32(retRecList[i].Voice.Size)
// 			searchTextRspBody.ContentList[i].Image = new(ht_favorite.FavImageBody)
// 			searchTextRspBody.ContentList[i].Image.ThumbUrl = []byte(retRecList[i].Image.ThumbUrl)
// 			searchTextRspBody.ContentList[i].Image.BigUrl = []byte(retRecList[i].Image.BigUrl)
// 			searchTextRspBody.ContentList[i].Image.Width = proto.Uint32(retRecList[i].Image.Width)
// 			searchTextRspBody.ContentList[i].Image.Height = proto.Uint32(retRecList[i].Image.Height)
// 			searchTextRspBody.ContentList[i].Mid = []byte(retRecList[i].Mid)
// 			searchTextRspBody.ContentList[i].Tags = retRecList[i].Tags
// 		}
// 	}

// 	return true
// }

func IncAllFovoriteCount(uid uint32) (err error) {
	err = ChangeAllFovoriteCount(uid, 1)
	return err
}

func DescAllFovoriteCount(uid uint32) (err error) {
	err = ChangeAllFovoriteCount(uid, -1)
	return err
}

func ChangeAllFovoriteCount(uid uint32, changeCount int) (err error) {
	if uid == 0 || changeCount == 0 {
		infoLog.Printf("ChangeAllFovoriteCount invalid param uid=%v count=%v", uid, changeCount)
		err = errors.New("invalid param")
		return err
	}
	if changeCount < 0 {
		// first get all favorite count
		total, err := GetAllFovoriteCount(uid)
		if err == nil {
			if int(total)+changeCount < 0 {
				infoLog.Printf("ChangeAllFovoriteCount reset to total uid=%v total=%v changeCount=%v",
					uid,
					total,
					changeCount)
				changeCount = -int(total)
			}
		} else {
			infoLog.Printf("ChangeAllFovoriteCount GetAllFovoriteCount failed")
			attrName := "gofav/get_all_fav_count_failed"
			libcomm.AttrAdd(attrName, 1)
		}
	}

	// first update db
	err = ChangeAllFovoriteCountInDb(uid, changeCount)
	if err != nil {
		infoLog.Printf("ChangeAllFovoriteCount in db faield uid=%v changeCount=%v err=%s", uid, changeCount, err)
		attrName := "gofav/change_fav_count_in_db_failed"
		libcomm.AttrAdd(attrName, 1)
	}
	// second update redis
	err = ChangeAllFovoriteCountInRedis(uid, changeCount)
	if err != nil {
		attrName := "gofav/change_fav_count_in_redis_failed"
		libcomm.AttrAdd(attrName, 1)
		infoLog.Printf("ChangeAllFovoriteCount in redis faield uid=%v changeCount=%v err=%s", uid, changeCount, err)
	}
	return err
}

func GetRedisCTCRHashName(uid uint32) (hashName string) {
	hashName = fmt.Sprintf("CTCR_%v", uid)
	return hashName
}

func GetAllFovoriteCount(uid uint32) (total int64, err error) {
	if uid == 0 {
		infoLog.Printf("GetAllFovoriteCount invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return total, err
	}
	// second update redis
	total, err = GetAllFovoriteCountInRedis(uid)
	if err != nil {
		attrName := "gofav/get_fav_count_in_redis_failed"
		libcomm.AttrAdd(attrName, 1)
		infoLog.Printf("GetAllFovoriteCount in redis faield uid=%v err=%s", uid, err)
		// first update db
		total, err = GetAllFovoriteCountInDb(uid)
		if err != nil {
			infoLog.Printf("GetAllFovoriteCount in db faield uid=%v err=%s", uid, err)
			attrName := "gofav/get_fav_count_in_db_failed"
			libcomm.AttrAdd(attrName, 1)
			total = 0
		}
	}
	return total, err
}

func GetAllFovoriteCountInRedis(uid uint32) (total int64, err error) {
	if uid == 0 {
		infoLog.Printf("GetAllFovoriteCountInRedis invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return total, err
	}
	// 获取一条Redis连接
	redisConn := pool.Get()
	defer redisConn.Close()
	hashName := GetRedisCTCRHashName(uid)
	r, err := redisConn.Do("EXISTS", hashName)
	bExist, err := redis.Int64(r, err)
	if bExist != 1 {
		infoLog.Printf("GetAllFovoriteCountInRedis uid=%v hashName=%s not exist", uid, hashName)
		err = errors.New("not exist")
		return total, err
	}

	r, err = redisConn.Do("HGET", hashName, allFavCountKeyName)
	total, err = redis.Int64(r, err)
	if err != nil {
		infoLog.Printf("GetAllFovoriteCountInRedis Reids exec HGET hashName=%s fieldname=%s err=%v",
			hashName,
			allFavCountKeyName,
			err)
		return total, err
	}

	infoLog.Printf("GetAllFovoriteCountInRedis Redis exec HGET succ hashName=%s fieldname=%s allFavCount=%v",
		hashName,
		allFavCountKeyName,
		total)
	return total, nil
}

func ChangeAllFovoriteCountInRedis(uid uint32, changeCount int) (err error) {
	if uid == 0 || changeCount == 0 {
		infoLog.Printf("ChangeAllFovoriteCountInRedis invalid param uid=%v count=%v", uid, changeCount)
		err = errors.New("invalid param")
		return err
	}
	// 获取一条Redis连接
	redisConn := pool.Get()
	defer redisConn.Close()
	hashName := GetRedisCTCRHashName(uid)
	r, err := redisConn.Do("EXISTS", hashName)
	bExist, err := redis.Int64(r, err)
	if bExist == 1 {
		r, err = redisConn.Do("HINCRBY", hashName, allFavCountKeyName, changeCount)
		total, err := redis.Int64(r, err)
		if err != nil {
			infoLog.Printf("ChangeAllFovoriteCountInRedis Reids exec HINCRBY hashName=%s fieldname=%s count=%v err=%v",
				hashName,
				allFavCountKeyName,
				changeCount,
				err)
		} else {
			infoLog.Printf("ChangeAllFovoriteCountInRedis Redis exec HINCRBY succ hashName=%s fieldname=%s allFavCount=%v",
				hashName,
				allFavCountKeyName,
				total)
		}
	} else {
		infoLog.Printf("ChangeAllFovoriteCountInRedis uid=%v changeCont=%v hashName=%s not exist", uid, changeCount, hashName)
	}
	return err
}

func ChangeAllFovoriteCountInDb(uid uint32, changeCount int) (err error) {
	if uid == 0 || changeCount == 0 {
		infoLog.Printf("ChangeAllFovoriteCountInDb invalid param uid=%v count=%v", uid, changeCount)
		err = errors.New("invalid param")
		return err
	}

	if changeCount > 0 {
		r, err := db.Exec("UPDATE MG_USER_COUNTINFO SET ALLCOUNT = ALLCOUNT + ? WHERE USERID =?;", changeCount, uid)
		if err != nil {
			infoLog.Printf("ChangeAllFovoriteCountInDb failed uid=%v count=%v err=%v", uid, changeCount, err)
			return err
		} else {
			affectRow, err := r.RowsAffected()
			if err != nil {
				infoLog.Printf("ChangeAllFovoriteCountInDb exec RowsAffected failed err=%v", err)
			} else {
				infoLog.Printf("ChangeAllFovoriteCountInDb affectRow=%v", affectRow)
			}
			return nil
		}
	} else {
		r, err := db.Exec("UPDATE MG_USER_COUNTINFO SET ALLCOUNT = ALLCOUNT - ? WHERE USERID =?;", -changeCount, uid)
		if err != nil {
			infoLog.Printf("ChangeAllFovoriteCountInDb failed uid=%v count=%v err=%v", uid, changeCount, err)
			return err
		} else {
			affectRow, err := r.RowsAffected()
			if err != nil {
				infoLog.Printf("ChangeAllFovoriteCountInDb exec RowsAffected failed err=%v", err)
			} else {
				infoLog.Printf("ChangeAllFovoriteCountInDb affectRow=%v", affectRow)
			}
			return nil
		}
	}
}

func GetAllFovoriteCountInDb(uid uint32) (total int64, err error) {
	if uid == 0 {
		infoLog.Printf("GetAllFovoriteCountInDb invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return total, err
	}

	err = db.QueryRow("SELECT ALLCOUNT FROM MG_USER_COUNTINFO WHERE USERID = ?", uid).Scan(&total)
	switch {
	case err == sql.ErrNoRows:
		infoLog.Printf("GetAllFovoriteCountInDb not found uid=%v", uid)
		break
	case err != nil:
		infoLog.Printf("GetAllFovoriteCountInDb exec failed uid=%v err=%s", uid, err)
		break
	default:
		infoLog.Printf("GetAllFovoriteCountInDb uid=%v total=%v", uid, total)
	}
	return total, err
}

// 将FavoriteCount清零
func ResetAllFovoriteCount(uid uint32) (err error) {
	if uid == 0 {
		infoLog.Printf("ResetAllFovoriteCount invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return err
	}
	// first update db
	err = ResetAllFovoriteCountInDb(uid)
	if err != nil {
		infoLog.Printf("ResetAllFovoriteCountInDb in db faield uid=%v err=%s", uid, err)
		attrName := "gofav/reset_fav_count_in_db_failed"
		libcomm.AttrAdd(attrName, 1)
	}
	// second update redis
	err = ResetAllFovoriteCountInRedis(uid)
	if err != nil {
		infoLog.Printf("ResetAllFovoriteCountInRedis in redis faield uid=%v err=%s", uid, err)
		attrName := "gofav/reset_fav_count_in_redis_failed"
		libcomm.AttrAdd(attrName, 1)
	}
	return err
}

func ResetAllFovoriteCountInDb(uid uint32) (err error) {
	if uid == 0 {
		infoLog.Printf("ResetAllFovoriteCountInDb invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return err
	}

	r, err := db.Exec("UPDATE MG_USER_COUNTINFO SET ALLCOUNT = 0 WHERE USERID =?;", uid)
	if err != nil {
		infoLog.Printf("ResetAllFovoriteCountInDb failed uid=%v err=%v", uid, err)
		return err
	} else {
		affectRow, err := r.RowsAffected()
		if err != nil {
			infoLog.Printf("ResetAllFovoriteCountInDb exec RowsAffected failed err=%v", err)
		} else {
			infoLog.Printf("ResetAllFovoriteCountInDb affectRow=%v", affectRow)
		}
		return nil
	}
}

func ResetAllFovoriteCountInRedis(uid uint32) (err error) {
	if uid == 0 {
		infoLog.Printf("ResetAllFovoriteCountInRedis invalid param uid=%v", uid)
		err = errors.New("invalid param")
		return err
	}
	// 获取一条Redis连接
	redisConn := pool.Get()
	defer redisConn.Close()
	hashName := GetRedisCTCRHashName(uid)
	r, err := redisConn.Do("EXISTS", hashName)
	bExist, err := redis.Int64(r, err)
	if bExist == 1 {
		r, err = redisConn.Do("HSET", hashName, allFavCountKeyName, 0)
		total, err := redis.Int64(r, err)
		if err != nil {
			infoLog.Printf("ResetAllFovoriteCountInRedis Reids exec HINCRBY hashName=%s fieldname=%s err=%v",
				hashName,
				allFavCountKeyName,
				err)
			attrName := "gofav/fav_rest_fav_count_inredis_failed"
			libcomm.AttrAdd(attrName, 1)
		} else {
			infoLog.Printf("ResetAllFovoriteCountInRedis Redis exec HINCRBY succ hashName=%s fieldname=%s ret=%v",
				hashName,
				allFavCountKeyName,
				total)
		}
	} else {
		infoLog.Printf("ResetAllFovoriteCountInRedis uid=%v hashName=%s not exist", uid, hashName)
	}
	return err
}

func MessageHandleLoop() {
	defer func() {
		recover()
	}()
	infoLog.Printf("MessageHandleLoop")
	for {
		select {
		case task := <-updateCountTaskChan:
			uid := task.Uid
			count := task.Count
			infoLog.Printf("MessageHandleLoop uid=%v count=%v", uid, count)
			err := ChangeAllFovoriteCount(uid, count)
			if err != nil {
				infoLog.Printf("MessageHandleLoop ChangeAllFovoriteCount failed uid=%v changeCount=%v err=%v", uid, count, err)
			}
		}
	}
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var options Options

var parser = flags.NewParser(&options, flags.Default)

func main() {
	//runtime.GOMAXPROCS(runtime.NumCPU())
	runtime.GOMAXPROCS(1)

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Printf("load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false
	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("/home/ht/server.log")
	logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	defer logFile.Close()
	if err != nil {
		log.Fatalln("open file error !")
		return
	}

	// 创建一个日志对象
	infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags | log.Lshortfile)

	// 创建mongodb对象
	mongo_url := cfg.Section("MONGO").Key("url").MustString("localhost")
	infoLog.Println(mongo_url)
	mongoSess, err = mgo.Dial(mongo_url)
	if err != nil {
		log.Fatalln("connect mongodb failed")
		return
	}
	defer mongoSess.Close()
	mongoSess.SetMode(mgo.Monotonic, true)

	// init redis pool
	redisIp := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustInt(6379)
	infoLog.Printf("redis ip=%v port=%v", redisIp, redisPort)
	pool = newRedisPool(redisIp + ":" + strconv.Itoa(redisPort))

	// nsq producer config
	nsqUrl := cfg.Section("NSQ").Key("url").MustString("127.0.0.1:4150")
	nsqConfig := nsq.NewConfig()
	globalProducer, err = nsq.NewProducer(nsqUrl, nsqConfig)
	if err != nil {
		infoLog.Fatalf("main nsq.NewProcucer failed nsqUrl=%s", nsqUrl)
	}
	nsqTopic = cfg.Section("NSQTOPIC").Key("topic").MustString("usr_act")

	// update db count chant limit
	chanLimit := cfg.Section("CHAN").Key("len_limit").MustInt(1000)
	updateCountTaskChan = make(chan *UpdateCountTask, chanLimit)
	// 只用单个协成消费整个队列
	go MessageHandleLoop()
	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Println("open mysql failed err=%s", err)
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}
	srv := gotcp.NewServer(config, &Callback{}, &common.HeadV3Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
