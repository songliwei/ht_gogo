package common

import (
	"errors"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_user"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type UserCacheApi struct {
	userCachePool *Pool
}

func newUserCachePool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewUserCacheApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *UserCacheApi {
	pool := newUserCachePool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &UserCacheApi{
		userCachePool: pool,
	}
}

func (c *UserCacheApi) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userCachePool.Get()
	defer conn.Close()

	rsp, err := conn.Do(packet)
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *UserCacheApi) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet = NewHeadV2Packet(buf)
	conn := c.userCachePool.Get()
	defer conn.Close()

	packet, err = conn.Do(packet)
	return packet, nil
}

func (c *UserCacheApi) QueryNickNameAndUserName(fromId, toId uint32) (nickName, userName []byte, err error) {
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_user.ProtoCmd_CMD_GET_USERINFO),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := new(ht_user.ReqBody)
	reqBody.User = []*ht_user.UserInfoBody{&ht_user.UserInfoBody{
		UserID: proto.Uint32(toId),
	},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userCachePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userCachePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_user.ResultCode_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, nil, err
	}

	if rspHead.Cmd != uint32(ht_user.ProtoCmd_CMD_GET_USERINFO) {
		err = ErrExpectedResp
		return nil, nil, err
	}

	rspBody := &ht_user.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, nil, err
	}
	subRspBody := rspBody.GetUser()
	if len(subRspBody) < 1 {
		return nil, nil, nil
	}
	nickName = subRspBody[0].GetNickname()
	userName = subRspBody[0].GetUserName()
	return nickName, userName, nil
}

func (c *UserCacheApi) QueryLangInfo(uid uint32) (national string, nativeLang uint32, teachLang []uint32, learnLang []uint32, err error) {
	if uid == 0 {
		err = errors.New("error input param")
		return national, nativeLang, nil, nil, err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_user.ProtoCmd_CMD_GET_USERINFO),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      uid,
	}

	reqBody := new(ht_user.ReqBody)
	reqBody.User = []*ht_user.UserInfoBody{
		&ht_user.UserInfoBody{
			UserID: proto.Uint32(uid),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return national, nativeLang, nil, nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return national, nativeLang, nil, nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userCachePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userCachePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return national, nativeLang, nil, nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return national, nativeLang, nil, nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_user.ResultCode_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return national, nativeLang, nil, nil, err
	}

	if rspHead.Cmd != uint32(ht_user.ProtoCmd_CMD_GET_USERINFO) {
		err = ErrExpectedResp
		return national, nativeLang, nil, nil, err
	}

	rspBody := &ht_user.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return national, nativeLang, nil, nil, err
	}
	subRspBody := rspBody.GetUser()
	if len(subRspBody) < 1 {
		return national, nativeLang, nil, nil, nil
	}
	userInfo := subRspBody[0]
	national = string(userInfo.GetNational())
	nativeLang = userInfo.GetNativeLang()
	if nativeLang != 0 {
		teachLang = append(teachLang, nativeLang)
	}
	if userInfo.GetTeachLang2() != 0 {
		teachLang = append(teachLang, userInfo.GetTeachLang2())
	}
	if userInfo.GetTeachLang3() != 0 {
		teachLang = append(teachLang, userInfo.GetTeachLang3())
	}

	if userInfo.GetLearnLang1() != 0 {
		learnLang = append(learnLang, userInfo.GetLearnLang1())
	}
	if userInfo.GetLearnLang2() != 0 {
		learnLang = append(learnLang, userInfo.GetLearnLang2())
	}
	if userInfo.GetLearnLang3() != 0 {
		learnLang = append(learnLang, userInfo.GetLearnLang3())
	}
	return national, nativeLang, teachLang, learnLang, nil
}

func (c *UserCacheApi) BatchQueryUserInfo(from uint32, uidList []uint32) (userInfo []*ht_user.UserInfoBody, err error) {
	if len(uidList) == 0 {
		err = errors.New("error input param")
		return nil, err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_user.ProtoCmd_CMD_GET_USERINFO),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      from,
	}

	var reqUserInfo []*ht_user.UserInfoBody
	for _, v := range uidList {
		item := &ht_user.UserInfoBody{
			UserID: proto.Uint32(v),
		}
		reqUserInfo = append(reqUserInfo, item)
	}
	reqBody := &ht_user.ReqBody{
		User: reqUserInfo,
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userCachePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userCachePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_user.ResultCode_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}

	if rspHead.Cmd != uint32(ht_user.ProtoCmd_CMD_GET_USERINFO) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_user.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	userInfo = rspBody.GetUser()
	return userInfo, nil
}

func (c *UserCacheApi) QuerySingleBlackList(uid uint32) (blackList []uint32, err error) {
	if uid == 0 {
		err = errors.New("error input param")
		return nil, err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_user.ProtoCmd_CMD_GET_USERINFO),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      uid,
	}

	reqBody := new(ht_user.ReqBody)
	reqBody.User = []*ht_user.UserInfoBody{
		&ht_user.UserInfoBody{
			UserID: proto.Uint32(uid),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userCachePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userCachePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_user.ResultCode_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}

	if rspHead.Cmd != uint32(ht_user.ProtoCmd_CMD_GET_USERINFO) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_user.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetUser()
	if len(subRspBody) < 1 {
		return nil, nil
	}
	userInfo := subRspBody[0]
	blackList = userInfo.GetBlackidList()
	return blackList, nil
}
