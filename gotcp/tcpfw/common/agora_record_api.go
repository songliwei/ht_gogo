package common

import (
	"errors"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_record"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type AgoraRecordApi struct {
	agoraRecordPool *Pool
}

func newAgoraRecordPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewAgoraRecordApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *AgoraRecordApi {
	pool := newAgoraRecordPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &AgoraRecordApi{
		agoraRecordPool: pool,
	}
}

func (c *AgoraRecordApi) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	headV2Packet := NewHeadV2Packet(buf)
	conn := c.agoraRecordPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV2Packet)
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *AgoraRecordApi) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	headV2Packet := NewHeadV2Packet(buf)
	// add twice retry
	conn := c.agoraRecordPool.Get()
	packet, err = conn.Do(headV2Packet)
	if err != nil {
		conn.Close() //close connection
		conn = c.agoraRecordPool.Get()
		packet, err = conn.Do(headV2Packet)
	}
	conn.Close()
	return packet, err
}

func (c *AgoraRecordApi) StartNewRecord(fromId, roomId uint32, channelId []byte) (err error) {
	if fromId == 0 || roomId == 0 {
		err = errors.New("error input param")
		return err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_record.RECORD_CMD_TYPE_CMD_START_NEW_RECORD),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := &ht_record.RecordReqBody{
		StartNewRecordReqbody: &ht_record.StartNewRecordReqBody{
			RoomId:    proto.Uint32(roomId),
			ChannelId: channelId,
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.agoraRecordPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.agoraRecordPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_record.RECORD_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}

	if rspHead.Cmd != uint32(ht_record.RECORD_CMD_TYPE_CMD_START_NEW_RECORD) {
		err = ErrExpectedResp
		return err
	}
	return nil
}

func (c *AgoraRecordApi) EndRecord(fromId, roomId uint32, channelId []byte, recordObid []byte) (err error) {
	if fromId == 0 || roomId == 0 {
		err = errors.New("error input param")
		return err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_record.RECORD_CMD_TYPE_CMD_END_RECORD),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := &ht_record.RecordReqBody{
		EndRecordReqbody: &ht_record.EndRecordReqBody{
			RoomId:     proto.Uint32(roomId),
			ChannelId:  channelId,
			RecordObid: recordObid,
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.agoraRecordPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.agoraRecordPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_record.RECORD_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}

	if rspHead.Cmd != uint32(ht_record.RECORD_CMD_TYPE_CMD_END_RECORD) {
		err = ErrExpectedResp
		return err
	}
	return nil
}
