package common

import (
	"time"

	"github.com/HT_GOGO/gotcp"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type SrvToSrvApiV2 struct {
	srvToSrvpool *Pool
}

func newSrvToSrvPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewSrvToSrvApiV2(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *SrvToSrvApiV2 {
	pool := newSrvToSrvPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &SrvToSrvApiV2{
		srvToSrvpool: pool,
	}
}

func (c *SrvToSrvApiV2) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	headV2Packet := NewHeadV2Packet(buf)
	conn := c.srvToSrvpool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV2Packet)
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *SrvToSrvApiV2) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	headV2Packet := NewHeadV2Packet(buf)
	// add twice retry
	conn := c.srvToSrvpool.Get()
	packet, err = conn.Do(headV2Packet)
	if err != nil {
		conn.Close() //close connection
		conn = c.srvToSrvpool.Get()
		packet, err = conn.Do(headV2Packet)
	}
	conn.Close()
	return packet, err
}
