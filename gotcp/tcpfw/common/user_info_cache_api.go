package common

import (
	"errors"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_cacheinterface"
	"github.com/golang/protobuf/proto"
)

type CacheLanguageInfo struct {
	NativeLang      uint8
	LearnLang1      uint8
	LearnLevel1     uint8
	LearnLang2      uint8
	LearnLevel2     uint8
	LearnLang3      uint8
	LearnLevel3     uint8
	LearnLang4      uint8
	LearnLevel4     uint8
	LearnAllowCount uint8 //购买的语言数
	TeachLang2      uint8
	TeachLevel2     uint8
	TeachLang3      uint8
	TeachLevel3     uint8

	//偷个懒 直接把性别和国籍和年龄一次性返回
	Sex         uint32
	Nationality string
	Birthday    string
	TimeZone_48 uint32
	RegTime     uint64
	RegFrom     uint32
	EmailVerify uint32
}

func (this *CacheLanguageInfo) BuildTeachArray() []uint32 {

	langArr := []uint32{}

	if this.NativeLang > 0 {
		langArr = append(langArr, uint32(this.NativeLang))
	}

	if this.TeachLang2 > 0 {
		langArr = append(langArr, uint32(this.TeachLang2))
	}

	if this.TeachLang3 > 0 {
		langArr = append(langArr, uint32(this.TeachLang3))
	}

	return langArr
}

func (this *CacheLanguageInfo) BuildLearnArray() []uint32 {

	langArr := []uint32{}

	if this.LearnLang1 > 0 {
		langArr = append(langArr, uint32(this.LearnLang1))
	}

	if this.LearnLang2 > 0 {
		langArr = append(langArr, uint32(this.LearnLang2))
	}

	if this.LearnLang3 > 0 {
		langArr = append(langArr, uint32(this.LearnLang3))
	}

	return langArr

}

// Conn exposes a set of callbacks for the various events that occur on a connection
type UserInfoCacheApi struct {
	userInfoPool *Pool
}

func newUserInfoPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewUserInfoCacheApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *UserInfoCacheApi {
	pool := newUserInfoPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &UserInfoCacheApi{
		userInfoPool: pool,
	}
}

func (c *UserInfoCacheApi) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	headV2Packet := NewHeadV2Packet(buf)
	conn := c.userInfoPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV2Packet)
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *UserInfoCacheApi) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	headV2Packet := NewHeadV2Packet(buf)
	// add twice retry
	conn := c.userInfoPool.Get()
	packet, err = conn.Do(headV2Packet)
	if err != nil {
		conn.Close() //close connection
		conn = c.userInfoPool.Get()
		packet, err = conn.Do(headV2Packet)
	}
	conn.Close()
	return packet, err
}

func (c *UserInfoCacheApi) UpdateLocationVer(fromId uint32, locationVer uint64) (err error) {
	if fromId == 0 {
		err = errors.New("error input param")
		return err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_PROPERTY),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := &ht_cache_interface.CacheInterfaceReqBody{
		UpdateUserPropertyReqbody: &ht_cache_interface.UpdateUserPropertyReqBody{
			ModType:     ht_cache_interface.MOD_PROPERTY_TYPE_MOD_LOCATION_VER.Enum(),
			LocationVer: proto.Uint64(locationVer),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userInfoPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userInfoPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}

	if rspHead.Cmd != uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_PROPERTY) {
		err = ErrExpectedResp
		return err
	}
	return nil
}

func (c *UserInfoCacheApi) SendCacheInfoBody(head *HeadV2, reqBody *ht_cache_interface.CacheInterfaceReqBody) (*HeadV2, *ht_cache_interface.CacheInterfaceRspBody, error) {

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userInfoPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userInfoPool.Get()
		rsp, err = conn.Do(packet)
		if err != nil {
			conn.Close()
			return nil, nil, err
		}
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		return nil, nil, ErrChangePacketFailed
	}

	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, nil, err
	}

	if rspHead.Cmd != head.Cmd {
		err = ErrExpectedResp
		return nil, nil, err
	}

	rspBody := &ht_cache_interface.CacheInterfaceRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, nil, ErrGetRespPacketFaild
	}

	return head, rspBody, nil
}

func (c *UserInfoCacheApi) GetUserLanguageSetting(fromId uint32) (langInfo *CacheLanguageInfo, err error) {
	if fromId == 0 {
		err = errors.New("error input param")
		return nil, err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_cache_interface.CMD_TYPE_CMD_GET_USER_CACHE),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := &ht_cache_interface.CacheInterfaceReqBody{
		GetUserCacheReqbody: &ht_cache_interface.GetUserCacheReqBody{
			UidList: []uint32{fromId},
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userInfoPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userInfoPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}

	if rspHead.Cmd != uint32(ht_cache_interface.CMD_TYPE_CMD_GET_USER_CACHE) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_cache_interface.CacheInterfaceRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetGetUserCacheRspbody()
	itermList := subRspBody.GetItermList()
	if len(itermList) < 1 {
		err = ErrExpectedResp
		return nil, err
	}
	langInfo = &CacheLanguageInfo{
		NativeLang:      uint8(itermList[0].GetBaseCache().GetNativeLang()),
		LearnLang1:      uint8(itermList[0].GetBaseCache().GetLearnLang1()),
		LearnLevel1:     uint8(itermList[0].GetBaseCache().GetLearnLang1Level()),
		LearnLang2:      uint8(itermList[0].GetBaseCache().GetLearnLang2()),
		LearnLevel2:     uint8(itermList[0].GetBaseCache().GetLearnLang2Level()),
		LearnLang3:      uint8(itermList[0].GetBaseCache().GetLearnLang3()),
		LearnLevel3:     uint8(itermList[0].GetBaseCache().GetLearnLang3Level()),
		LearnLang4:      uint8(itermList[0].GetBaseCache().GetLearnLang4()),
		LearnLevel4:     uint8(itermList[0].GetBaseCache().GetLearnLang4Level()),
		LearnAllowCount: uint8(itermList[0].GetBaseCache().GetAllowCount()),
		TeachLang2:      uint8(itermList[0].GetBaseCache().GetTeachLang2()),
		TeachLevel2:     uint8(itermList[0].GetBaseCache().GetTeachLang2Level()),
		TeachLang3:      uint8(itermList[0].GetBaseCache().GetTeachLang3()),
		TeachLevel3:     uint8(itermList[0].GetBaseCache().GetTeachLang3Level()),
		Sex:             itermList[0].GetBaseCache().GetSex(),
		Nationality:     itermList[0].GetBaseCache().GetNationality(),
		TimeZone_48:     itermList[0].GetBaseCache().GetTimeZone_48(),
		RegTime:         itermList[0].GetAccountCache().GetRegTime(),
		RegFrom:         itermList[0].GetAccountCache().GetRegFrom(),
		EmailVerify:     itermList[0].GetAccountCache().GetVerify(),
		Birthday:        itermList[0].GetBaseCache().GetBirthday(),
	}
	return langInfo, nil
}

func (c *UserInfoCacheApi) UpdateUserLanguageSetting(fromId uint32, langInfo *CacheLanguageInfo) (err error) {
	if fromId == 0 || langInfo == nil {
		err = errors.New("error input param")
		return err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_LANG_SETTING),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := &ht_cache_interface.CacheInterfaceReqBody{
		UpdateUserLangSettingReqbody: &ht_cache_interface.UpdateUserLangSettingReqBody{
			LangInfo: &ht_cache_interface.LanguageInfo{
				NativeLang:      proto.Uint32(uint32(langInfo.NativeLang)),
				TeachLang2:      proto.Uint32(uint32(langInfo.TeachLang2)),
				TeachLang2Level: proto.Uint32(uint32(langInfo.TeachLevel2)),
				TeachLang3:      proto.Uint32(uint32(langInfo.TeachLang3)),
				TeachLang3Level: proto.Uint32(uint32(langInfo.TeachLevel3)),
				LearnLang1:      proto.Uint32(uint32(langInfo.LearnLang1)),
				LearnLang1Level: proto.Uint32(uint32(langInfo.LearnLevel1)),
				LearnLang2:      proto.Uint32(uint32(langInfo.LearnLang2)),
				LearnLang2Level: proto.Uint32(uint32(langInfo.LearnLevel2)),
				LearnLang3:      proto.Uint32(uint32(langInfo.LearnLang3)),
				LearnLang3Level: proto.Uint32(uint32(langInfo.LearnLevel3)),
				LearnLang4:      proto.Uint32(uint32(langInfo.LearnLang4)),
				LearnLang4Level: proto.Uint32(uint32(langInfo.LearnLevel4)),
			},
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userInfoPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userInfoPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}

	if rspHead.Cmd != uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_LANG_SETTING) {
		err = ErrExpectedResp
		return err
	}
	return nil
}

func (c *UserInfoCacheApi) UpdateUserLessonVer(fromId uint32) (err error) {
	if fromId == 0 {
		err = errors.New("error input param")
		return err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_LESSON_VER),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := &ht_cache_interface.CacheInterfaceReqBody{
		UpdateUserLessonVerReqbody: &ht_cache_interface.UpdateUserLessonVerReqBody{
			CreateUid: proto.Uint32(fromId),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userInfoPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userInfoPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}

	if rspHead.Cmd != uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_LESSON_VER) {
		err = ErrExpectedResp
		return err
	}
	return nil
}

func (c *UserInfoCacheApi) UpdateUserWalletVer(fromId uint32) (err error) {
	if fromId == 0 {
		err = errors.New("error input param")
		return err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_WALLET_VER),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := &ht_cache_interface.CacheInterfaceReqBody{
		UpdateUserWalletVerReqbody: &ht_cache_interface.UpdateUserWalletVerReqBody{
			CreateUid: proto.Uint32(fromId),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userInfoPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userInfoPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}

	if rspHead.Cmd != uint32(ht_cache_interface.CMD_TYPE_CMD_UPDATE_USER_WALLET_VER) {
		err = ErrExpectedResp
		return err
	}
	return nil
}

func (c *UserInfoCacheApi) GetUserNickName(uid uint32) (userName, nickName string, err error) {
	if uid == 0 {
		err = errors.New("error input param")
		return userName, nickName, err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_cache_interface.CMD_TYPE_CMD_GET_USER_NICK_NAME),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      uid,
	}

	reqBody := &ht_cache_interface.CacheInterfaceReqBody{
		GetUserNickNameReqbody: &ht_cache_interface.GetUserNickNameReqBody{},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return userName, nickName, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return userName, nickName, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.userInfoPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.userInfoPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return userName, nickName, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return userName, nickName, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_cache_interface.CACHE_RET_CODE_CACHE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return userName, nickName, err
	}

	if rspHead.Cmd != uint32(ht_cache_interface.CMD_TYPE_CMD_GET_USER_NICK_NAME) {
		err = ErrExpectedResp
		return userName, nickName, err
	}

	rspBody := &ht_cache_interface.CacheInterfaceRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return userName, nickName, err
	}
	subRspBody := rspBody.GetGetUserNickNameRspbody()
	userName = subRspBody.GetUserName()
	nickName = subRspBody.GetNickName()
	return userName, nickName, nil
}
