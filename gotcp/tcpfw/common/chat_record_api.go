package common

import (
	"errors"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_chatrecord"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type ChatRecordApi struct {
	chatRecordPool *Pool
}

func newChatRecordPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewChatRecordApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *ChatRecordApi {
	pool := newChatRecordPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &ChatRecordApi{
		chatRecordPool: pool,
	}
}

func (c *ChatRecordApi) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	headV2Packet := NewHeadV2Packet(buf)
	conn := c.chatRecordPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV2Packet)
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *ChatRecordApi) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	headV2Packet := NewHeadV2Packet(buf)
	// add twice retry
	conn := c.chatRecordPool.Get()
	packet, err = conn.Do(headV2Packet)
	if err != nil {
		conn.Close() //close connection
		conn = c.chatRecordPool.Get()
		packet, err = conn.Do(headV2Packet)
	}
	conn.Close()
	return packet, err
}

func (c *ChatRecordApi) GetChatCount(fromId, toId uint32) (count uint32, err error) {
	if fromId == 0 || toId == 0 {
		err = errors.New("error input param")
		return count, err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_chat_record.CMD_TYPE_CMD_GET_CHAT_RECORD),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := &ht_chat_record.ChatRecordReqBody{
		GetChatRecordReqbody: &ht_chat_record.GetChatRecordReqBody{
			FromId: proto.Uint32(fromId),
			ToIdList: []uint32{
				toId,
			},
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return count, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return count, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.chatRecordPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.chatRecordPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return count, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return count, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return count, err
	}

	if rspHead.Cmd != uint32(ht_chat_record.CMD_TYPE_CMD_GET_CHAT_RECORD) {
		err = ErrExpectedResp
		return count, err
	}

	rspBody := &ht_chat_record.ChatRecordRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return count, err
	}
	subRspBody := rspBody.GetGetChatRecordRspbody()
	ackList := subRspBody.GetAckList()
	if len(ackList) < 1 {
		count = 0
		return count, nil
	}
	for _, v := range ackList {
		if v.GetToId() == toId {
			count = v.GetTotalCount()
			break
		}
	}
	return count, nil
}

// 获取用户的聊天记录中 我说或者别人所说 count i speak  simple speak
func (c *ChatRecordApi) GetAllChatCount(userid uint32) (i_speak_count, s_speak_count uint32, err error) {
	if userid == 0 {
		err = errors.New("error input param")
		return i_speak_count, s_speak_count, err
	}
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_chat_record.CMD_TYPE_CMD_GET_ALL_CHAT_UID),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      userid,
	}
	reqBody := &ht_chat_record.ChatRecordReqBody{
		GetAllChatUidReqbody: &ht_chat_record.GetAllChatUidReqBody{
			FromId: proto.Uint32(userid),
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return i_speak_count, s_speak_count, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return i_speak_count, s_speak_count, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.chatRecordPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.chatRecordPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return i_speak_count, s_speak_count, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return i_speak_count, s_speak_count, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_chat_record.CHAT_RECORD_RET_CODE_CHAT_RECORD_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return i_speak_count, s_speak_count, err
	}

	if rspHead.Cmd != uint32(ht_chat_record.CMD_TYPE_CMD_GET_ALL_CHAT_UID) {
		err = ErrExpectedResp
		return i_speak_count, s_speak_count, err
	}

	rspBody := &ht_chat_record.ChatRecordRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return i_speak_count, s_speak_count, err
	}
	subRspBody := rspBody.GetGetAllChatUidRspbody()

	i_speak_s := subRspBody.GetISpeakToSimple()
	if len(i_speak_s) < 1 {
		i_speak_count = 0
	} else {
		for _, v := range i_speak_s {

			i_speak_count += *v.TotalCount
		}
	}

	s_speak_i := subRspBody.GetSSpeakMeSimple()

	if len(s_speak_i) < 1 {
		s_speak_count = 0
	} else {
		for _, v := range i_speak_s {

			s_speak_count += *v.TotalCount
		}
	}

	return i_speak_count, s_speak_count, nil
}
