package common

import (
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type CmntCacheApi struct {
	cmntCachePool *Pool
}

func newCmntCachePool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewCmntCacheApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *CmntCacheApi {
	pool := newCmntCachePool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &CmntCacheApi{
		cmntCachePool: pool,
	}
}

func (c *CmntCacheApi) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.cmntCachePool.Get()
	defer conn.Close()

	rsp, err := conn.Do(packet)
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *CmntCacheApi) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet = NewHeadV2Packet(buf)
	conn := c.cmntCachePool.Get()
	defer conn.Close()

	packet, err = conn.Do(packet)
	return packet, nil
}

// 发送清空用户基本信息缓存 不需要回响应
func (c *CmntCacheApi) ClearUserInfoCache(userId uint32) (err error) {
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_CLEAR_USER_INFO_CACHE),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      userId,
	}

	reqBody := new(ht_moment.ReqBody)
	reqBody.ClearUserInfoCacheReqbody = &ht_moment.ClearUserInfoCacheReqBody{
		Userid: proto.Uint32(userId),
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.cmntCachePool.Get()
	err = conn.Send(packet)
	if err != nil {
		conn.Close()
		conn = c.cmntCachePool.Get()
		err = conn.Send(packet)
	}
	conn.Close()
	return err
}
