package common

import (
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_lesson_msg"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type LessonMsgStoreApi struct {
	lessonMsgStorePool *Pool
}

func newLessonMsgPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewLessonMsgStoreApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *LessonMsgStoreApi {
	pool := newLessonMsgPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &LessonMsgStoreApi{
		lessonMsgStorePool: pool,
	}
}

func (c *LessonMsgStoreApi) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.lessonMsgStorePool.Get()
	defer conn.Close()

	rsp, err := conn.Do(packet)
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *LessonMsgStoreApi) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet = NewHeadV2Packet(buf)
	conn := c.lessonMsgStorePool.Get()
	defer conn.Close()

	packet, err = conn.Do(packet)
	return packet, nil
}

func (c *LessonMsgStoreApi) StoreGroupLessonMsg(roomId, fromId, cmd uint32, groupLessonObid []byte, timeStamp uint64, msg []byte) (msgIndex []byte, err error) {
	if roomId == 0 || fromId == 0 || len(msg) == 0 {
		err = ErrInputParamErr
		return nil, err
	}

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_lesson_msg.CMD_TYPE_CMD_STORE_LESSON_MESSAGE_REQ),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := new(ht_lesson_msg.LessonMsgReqBody)
	reqBody.StoreLessonMessageReqbody = &ht_lesson_msg.StoreLessonMessageReqBody{
		FromId:     proto.Uint32(fromId),
		RoomId:     proto.Uint32(roomId),
		LessonObid: groupLessonObid,
		Cmd:        proto.Uint32(cmd),
		Format:     proto.Uint32(0),
		Content:    msg,
		MsgTime:    proto.Uint64(timeStamp),
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.lessonMsgStorePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.lessonMsgStorePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}

	if rspHead.Cmd != uint32(ht_lesson_msg.CMD_TYPE_CMD_STORE_LESSON_MESSAGE_REQ) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	rspBody := &ht_lesson_msg.LessonMsgRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetStoreLessonMessageRspbody()
	msgIndex = subRspBody.GetMsgIndex()
	return msgIndex, nil
}

func (c *LessonMsgStoreApi) BatchGetLatestLessonMsg(roomId, opUid uint32, groupLessonObid []byte, cliSeq uint64) (maxMsgId uint64, hasMore uint32, outMsgList [][]byte, err error) {
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_lesson_msg.CMD_TYPE_CMD_GET_LATEST_LESSON_MESSAGE_REQ),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      opUid,
	}

	reqBody := new(ht_lesson_msg.LessonMsgReqBody)
	reqBody.GetLatestLessonMessageReqbody = &ht_lesson_msg.GetLatestLessonMessageReqBody{
		RoomId:     proto.Uint32(roomId),
		LessonObid: groupLessonObid,
		CliSeqId:   proto.Uint64(cliSeq),
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return maxMsgId, hasMore, nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return maxMsgId, hasMore, nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.lessonMsgStorePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.lessonMsgStorePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return maxMsgId, hasMore, nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return maxMsgId, hasMore, nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_lesson_msg.LESSON_MSG_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return maxMsgId, hasMore, nil, err
	}

	if rspHead.Cmd != uint32(ht_lesson_msg.CMD_TYPE_CMD_GET_LATEST_LESSON_MESSAGE_REQ) {
		err = ErrExpectedResp
		return maxMsgId, hasMore, nil, err
	}

	rspBody := &ht_lesson_msg.LessonMsgRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return maxMsgId, hasMore, nil, err
	}
	subRspBody := rspBody.GetGetLatestLessonMessageRspbody()
	maxMsgId = subRspBody.GetMaxSeqId()
	hasMore = subRspBody.GetHasMore()
	msgList := subRspBody.GetMsgList()
	for _, v := range msgList {
		outMsgList = append(outMsgList, v.GetContent())
	}
	return maxMsgId, hasMore, outMsgList, nil
}
