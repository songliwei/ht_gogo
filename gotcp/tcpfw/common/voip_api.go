package common

import (
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcrypto"
)

const (
	CMD_QUERY_GROUP_VOIP_REQ = 0x4030
	CMD_QUERY_GROUP_VOIP_ACK = 0x4031
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type VoipApi struct {
	voipPool *Pool
}

func newVoipPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewVoipApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *VoipApi {
	pool := newVoipPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &VoipApi{
		voipPool: pool,
	}
}

func (c *VoipApi) SendXTPacket(head *XTHead, payLoad []uint8) (err error) {
	head.Len = uint32(len(payLoad))
	head.Cmd = cmd_s2s_message_push
	buf := make([]byte, XTHeadLen+head.Len)
	err = SerialXTHeadToSlice(head, buf[:])
	if err != nil {
		return err
	}
	copy(buf[XTHeadLen:], payLoad) // return code
	reqPacket := NewXTHeadPacket(buf)

	conn := c.voipPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(reqPacket)
	if err != nil { // 发送失败
		return err
	}

	rspPacket := rsp.(*XTHeadPacket)
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}
	if head.Cmd == rspHead.Cmd {
		return nil
	} else {
		return nil
	}
}

func (c *VoipApi) GetRoomIsVoipping(head *XTHead, roomId uint32) (isVoipping bool, err error) {
	var reqPayLoad []byte
	MarshalUint32(roomId, &reqPayLoad)
	// 使用Server key 加密
	cryptoText := libcrypto.TEAEncrypt(string(reqPayLoad), SERVER_COMM_KEY)
	head.Len = uint32(len(cryptoText))
	head.Cmd = CMD_QUERY_GROUP_VOIP_REQ
	buf := make([]byte, XTHeadLen+head.Len)
	err = SerialXTHeadToSlice(head, buf[:])
	if err != nil {
		return isVoipping, err
	}
	copy(buf[XTHeadLen:], []byte(cryptoText)) // return code
	reqPacket := NewXTHeadPacket(buf)

	conn := c.voipPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(reqPacket)
	if err != nil { // 发送失败
		return isVoipping, err
	}

	rspPacket := rsp.(*XTHeadPacket)
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return isVoipping, err
	}
	if rspHead.Cmd != CMD_QUERY_GROUP_VOIP_ACK {
		err = ErrExpectedResp
		return isVoipping, err
	}
	// get response
	plainText := libcrypto.TEADecrypt(string(rspPacket.GetBody()), SERVER_COMM_KEY)
	plainSlice := []byte(plainText)
	if len(plainSlice) > 0 {
		ret := UnMarshalUint8(&plainSlice)
		if ret > 0 {
			isVoipping = true
		} else {
			isVoipping = false
		}
	} else {
		err = ErrExpectedResp
		return isVoipping, err
	}
	return isVoipping, nil
}
