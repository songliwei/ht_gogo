package common

import (
	"time"

	"github.com/HT_GOGO/gotcp"
)

const (
	cmd_s2s_message_push     = 0x8027
	cmd_s2s_message_push_ack = 0x8028
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type PushServerApi struct {
	pool *Pool
}

func newPushPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewPushServerApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *PushServerApi {
	pool := newPushPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &PushServerApi{
		pool: pool,
	}
}

func (c *PushServerApi) SendXTPacket(head *XTHead, payLoad []uint8) (err error) {
	head.Len = uint32(len(payLoad))
	head.Cmd = cmd_s2s_message_push
	buf := make([]byte, XTHeadLen+head.Len)
	err = SerialXTHeadToSlice(head, buf[:])
	if err != nil {
		return err
	}
	copy(buf[XTHeadLen:], payLoad) // return code
	reqPacket := NewXTHeadPacket(buf)

	conn := c.pool.Get()
	defer conn.Close()

	rsp, err := conn.Do(reqPacket)
	if err != nil { // 发送失败
		return err
	}

	rspPacket := rsp.(*XTHeadPacket)
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}
	if head.Cmd == rspHead.Cmd {
		return nil
	} else {
		return nil
	}
}

func (c *PushServerApi) JustSendXTPacket(head *XTHead, payLoad []uint8) (err error) {
	head.Len = uint32(len(payLoad))
	head.Cmd = cmd_s2s_message_push
	buf := make([]byte, XTHeadLen+head.Len)
	err = SerialXTHeadToSlice(head, buf[:])
	if err != nil {
		return err
	}
	copy(buf[XTHeadLen:], payLoad) // return code
	reqPacket := NewXTHeadPacket(buf)

	conn := c.pool.Get()
	defer conn.Close()

	err = conn.Send(reqPacket)
	if err != nil { // 发送失败
		return err
	} else {
		return nil
	}
}
