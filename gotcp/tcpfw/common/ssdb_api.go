package common

import (
	"github.com/seefan/gossdb"
	"github.com/seefan/gossdb/conf"
)

//"github.com/seefan/gossdb/conf"

// Conn exposes a set of callbacks for the various events that occur on a connection
type SsdbApi struct {
	ssdbPool *gossdb.Connectors
}

// minPoolSize maxPoolSize must be ultiple of five
func NewSsdbApi(ssdbIp string, ssdbPort, minPoolSize, maxPoolSize int) (ssbApi *SsdbApi, err error) {
	pool, err := gossdb.NewPool(&conf.Config{
		//pool, err := gossdb.NewPool(&gossdb.Config{
		Host:             ssdbIp,
		Port:             ssdbPort,
		MinPoolSize:      minPoolSize,
		MaxPoolSize:      maxPoolSize,
		AcquireIncrement: 5,
	})
	if err != nil {
		return nil, err
	}
	return &SsdbApi{ssdbPool: pool}, nil
}

func (api *SsdbApi) Get(key string) (value gossdb.Value, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return value, err
		}
	}
	// 成功取得连接
	value, err = ssdbConn.Get(key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return value, err
		}
		// 成功取得连接
		value, err = ssdbConn.Get(key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return value, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return value, nil
}

func (api *SsdbApi) MultiGetSlice(inKeys []string) (outKeys []string, outValues []gossdb.Value, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return nil, nil, err
		}
	}
	// 成功取得连接
	outKeys, outValues, err = ssdbConn.MultiGetSliceArray(inKeys)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return nil, nil, err
		}
		// 成功取得连接
		outKeys, outValues, err = ssdbConn.MultiGetSliceArray(inKeys)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return nil, nil, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return outKeys, outValues, nil
}

func (api *SsdbApi) Set(key string, value interface{}, ttl ...int64) (err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return err
		}
	}
	// 成功取得连接
	if len(ttl) > 0 {
		err = ssdbConn.Set(key, value, ttl...)
	} else {
		err = ssdbConn.Set(key, value)
	}
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return err
		}
		// 成功取得连接
		if len(ttl) > 0 {
			err = ssdbConn.Set(key, value, ttl...)
		} else {
			err = ssdbConn.Set(key, value)
		}
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return nil
}

func (api *SsdbApi) Del(key string) (err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return err
		}
	}
	// 成功取得连接
	err = ssdbConn.Del(key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return err
		}
		// 成功取得连接
		err = ssdbConn.Del(key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return nil
}

func (api *SsdbApi) Exists(key string) (ret bool, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return ret, err
		}
	}
	// 成功取得连接
	ret, err = ssdbConn.Exists(key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return ret, err
		}
		// 成功取得连接
		ret, err = ssdbConn.Exists(key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return ret, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return ret, nil
}

// 设置Key的生存时间 使用UnixTimeStamp 设置
func (api *SsdbApi) Expire(key string, ttl int64) (ret bool, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return ret, err
		}
	}
	// 成功取得连接
	ret, err = ssdbConn.Expire(key, ttl)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return ret, err
		}
		// 成功取得连接
		ret, err = ssdbConn.Expire(key, ttl)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return ret, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return ret, nil
}

func (api *SsdbApi) Hget(setName, key string) (value string, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return value, err
		}
	}
	// 成功取得连接
	val, err := ssdbConn.Hget(setName, key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return value, err
		}
		// 成功取得连接
		val, err = ssdbConn.Hget(setName, key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return value, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	value = val.String()
	return value, nil
}

func (api *SsdbApi) HgetInt64(setName, key string) (value int64, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return value, err
		}
	}
	// 成功取得连接
	val, err := ssdbConn.Hget(setName, key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return value, err
		}
		// 成功取得连接
		val, err = ssdbConn.Hget(setName, key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return value, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	value = val.Int64()
	return value, nil
}

func (api *SsdbApi) Hset(setName, key string, value interface{}) (err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return err
		}
	}
	// 成功取得连接
	err = ssdbConn.Hset(setName, key, value)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return err
		}
		// 成功取得连接
		err = ssdbConn.Hset(setName, key, value)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return nil
}

func (api *SsdbApi) Hdel(setName, key string) (err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return err
		}
	}
	// 成功取得连接
	err = ssdbConn.Hdel(setName, key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return err
		}
		// 成功取得连接
		err = ssdbConn.Hdel(setName, key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return nil
}

func (api *SsdbApi) Hsize(setName string) (value int64, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return value, err
		}
	}
	// 成功取得连接
	value, err = ssdbConn.Hsize(setName)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return value, err
		}
		// 成功取得连接
		value, err = ssdbConn.Hsize(setName)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return value, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return value, nil
}

func (api *SsdbApi) Hclear(setName string) (err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return err
		}
	}
	// 成功取得连接
	err = ssdbConn.Hclear(setName)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return err
		}
		// 成功取得连接
		err = ssdbConn.Hclear(setName)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return nil
}

func (api *SsdbApi) MultiHget(setName string, key ...string) (val map[string]gossdb.Value, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return val, err
		}
	}
	// 成功取得连接
	val, err = ssdbConn.MultiHget(setName, key...)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return val, err
		}
		// 成功取得连接
		val, err = ssdbConn.MultiHget(setName, key...)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return val, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return val, nil
}

func (api *SsdbApi) MultiHgetSliceArray(setName string, key []string) (keys []string, values []gossdb.Value, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return keys, values, err
		}
	}
	// 成功取得连接
	keys, values, err = ssdbConn.MultiHgetSliceArray(setName, key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return keys, values, err
		}
		// 成功取得连接
		keys, values, err = ssdbConn.MultiHgetSliceArray(setName, key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return keys, values, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return keys, values, nil
}

// Hmset 使用key-value 键值对组成的slice
func (api *SsdbApi) MultiHset(setName string, kvs map[string]interface{}) (err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return err
		}
	}
	// 成功取得连接
	err = ssdbConn.MultiHset(setName, kvs)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return err
		}
		// 成功取得连接
		err = ssdbConn.MultiHset(setName, kvs)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return nil
}

func (api *SsdbApi) MultiHgetAll(setName string) (val map[string]gossdb.Value, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return val, err
		}
	}
	// 成功取得连接
	val, err = ssdbConn.MultiHgetAll(setName)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return val, err
		}
		// 成功取得连接
		val, err = ssdbConn.MultiHgetAll(setName)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return val, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return val, nil
}

func (api *SsdbApi) MultiHgetAllSlice(setName string) (keys []string, values []gossdb.Value, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return keys, values, err
		}
	}
	// 成功取得连接
	keys, values, err = ssdbConn.MultiHgetAllSlice(setName)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return keys, values, err
		}
		// 成功取得连接
		keys, values, err = ssdbConn.MultiHgetAllSlice(setName)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return keys, values, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return keys, values, nil
}

func (api *SsdbApi) Hincr(setName, key string, num int64) (val int64, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return val, err
		}
	}
	// 成功取得连接
	val, err = ssdbConn.Hincr(setName, key, num)
	if val == -1 || err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return val, err
		}
		// 成功取得连接
		val, err = ssdbConn.Hincr(setName, key, num)
		if val == -1 || err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return val, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return val, nil
}

func (api *SsdbApi) Hexists(setName, key string) (ret bool, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return ret, err
		}
	}
	// 成功取得连接
	ret, err = ssdbConn.Hexists(setName, key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return ret, err
		}
		// 成功取得连接
		ret, err = ssdbConn.Hexists(setName, key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return ret, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return ret, nil
}

func (api *SsdbApi) Hscan(setName string, keyStart, keyEnd string, limit int64, reverse bool) (val map[string]gossdb.Value, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return val, err
		}
	}
	// 成功取得连接
	val, err = ssdbConn.Hscan(setName, keyStart, keyEnd, limit, reverse)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return val, err
		}
		// 成功取得连接
		val, err = ssdbConn.Hscan(setName, keyStart, keyEnd, limit, reverse)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return val, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return val, nil
}

func (api *SsdbApi) HscanArray(setName string, keyStart, keyEnd string, limit int64, reverse bool) (keys []string, values []gossdb.Value, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return keys, values, err
		}
	}
	// 成功取得连接
	keys, values, err = ssdbConn.HscanArray(setName, keyStart, keyEnd, limit, reverse)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return keys, values, err
		}
		// 成功取得连接
		keys, values, err = ssdbConn.HscanArray(setName, keyStart, keyEnd, limit, reverse)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return keys, values, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return keys, values, nil
}

func (api *SsdbApi) Zscan(setName string, keyStart string, scoreStart, scoreEnd interface{}, limit int64) (keys []string, scores []int64, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return keys, scores, err
		}
	}
	// 成功取得连接
	keys, scores, err = ssdbConn.Zscan(setName, keyStart, scoreStart, scoreEnd, limit)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return keys, scores, err
		}
		// 成功取得连接
		keys, scores, err = ssdbConn.Zscan(setName, keyStart, scoreStart, scoreEnd, limit)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return keys, scores, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return keys, scores, nil
}

func (api *SsdbApi) Zget(setName string, key string) (score int64, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return score, err
		}
	}
	// 成功取得连接
	score, err = ssdbConn.Zget(setName, key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return score, err
		}
		// 成功取得连接
		score, err = ssdbConn.Zget(setName, key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return score, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return score, nil
}

func (api *SsdbApi) Zset(setName, key string, score int64) (err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return err
		}
	}
	// 成功取得连接
	err = ssdbConn.Zset(setName, key, score)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return err
		}
		// 成功取得连接
		err = ssdbConn.Zset(setName, key, score)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return nil
}

func (api *SsdbApi) Zdel(setName, key string) (err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return err
		}
	}
	// 成功取得连接
	err = ssdbConn.Zdel(setName, key)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return err
		}
		// 成功取得连接
		err = ssdbConn.Zdel(setName, key)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return nil
}

func (api *SsdbApi) Zrange(setName string, offset, limit int64) (val map[string]int64, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return val, err
		}
	}
	// 成功取得连接
	val, err = ssdbConn.Zrange(setName, offset, limit)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return val, err
		}
		// 成功取得连接
		val, err = ssdbConn.Zrange(setName, offset, limit)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return val, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return val, nil
}

func (api *SsdbApi) Zrrange(setName string, offset, limit int64) (val map[string]int64, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return val, err
		}
	}
	// 成功取得连接
	val, err = ssdbConn.Zrrange(setName, offset, limit)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return val, err
		}
		// 成功取得连接
		val, err = ssdbConn.Zrrange(setName, offset, limit)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return val, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return val, nil
}

func (api *SsdbApi) Qpush_front(name string, value string) (size int64, err error) {
	// 获取一条Redis连接
	ssdbConn, err := api.ssdbPool.NewClient()
	if err != nil {
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			return size, err
		}
	}
	// 成功取得连接
	size, err = ssdbConn.Qpush_front(name, value)
	if err != nil {
		// 出错关闭连接
		ssdbConn.Close()
		ssdbConn, err = api.ssdbPool.NewClient()
		if err != nil {
			// 获取连接失败直接返回
			return size, err
		}
		// 成功取得连接
		size, err = ssdbConn.Qpush_front(name, value)
		if err != nil {
			// 出错关闭连接 返回错误
			ssdbConn.Close()
			return size, err
		}
	}
	// 关闭连接
	ssdbConn.Close()
	return size, nil
}
