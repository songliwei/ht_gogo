package common

import (
	"errors"
	"fmt"
	"io"
	"time"

	"github.com/garyburd/redigo/redis"
)

var (
	ErrRedisExecFailed = errors.New("redis exec failed")
)

type handleListLoopMessageFunc func(message *string) (code uint32, err error)

// Conn exposes a set of callbacks for the various events that occur on a connection
type RedisApi struct {
	redisPool   *redis.Pool
	redisServer string
}

func newRedisPool(redisServer string) *redis.Pool {
	return &redis.Pool{
		// Maximum number of idle connections in the pool.
		MaxIdle: 10,
		// Maximum number of connections allocated by the pool at a given time.
		// When zero, there is no limit on the number of connections in the pool
		//
		// Close connections after remaining idle for this duration. If the value
		// is zero, then idle connections are not closed. Applications should set
		// the timeout to a value less than the server's timeout.
		IdleTimeout: 240 * time.Second,
		// Dial is an application supplied function for creating and configuring a
		// connection.
		//
		// The connection returned from Dial must not be in a special state
		// (subscribed to pubsub channel, transaction started, ...).
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", redisServer)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		// TestOnBorrow is an optional application supplied function for checking
		// the health of an idle connection before the connection is used again by
		// the application. Argument t is the time that the connection was returned
		// to the pool. If the function returns an error, then the connection is
		// closed.
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}

func NewRedisApi(redisServer string) *RedisApi {
	pool := newRedisPool(redisServer)
	return &RedisApi{
		redisPool:   pool,
		redisServer: redisServer,
	}
}

func (api *RedisApi) Get(key string) (value string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("GET", key)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("GET", key)
	}
	value, err = redis.String(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) Mget(field []string) (value []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	var args []interface{}
	for _, v := range field {
		args = append(args, v)
	}
	r, err := redisConn.Do("MGET", args...)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("MGET", args...)
	}
	value, err = redis.Strings(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) Scan(index string) (values []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("scan", index)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("scan", index)
	}
	values, err = redis.Strings(r, err)
	redisConn.Close()
	return values, err
}

func (api *RedisApi) GetInt64(key string) (value int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("GET", key)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("GET", key)
	}
	value, err = redis.Int64(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) Setex(key, value string, expireTs uint32) (err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("SETEX", key, expireTs, value)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("SETEX", key, expireTs, value)
	}
	retValue, err := redis.String(r, err)
	if err == nil && retValue == "OK" {
	} else if err == nil {
		err = ErrRedisExecFailed
	}
	redisConn.Close()
	return err
}

func (api *RedisApi) Set(key, value string) (err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("SET", key, value)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("SET", key, value)
	}
	retValue, err := redis.String(r, err)
	if err == nil && retValue == "OK" {
	} else if err == nil {
		err = ErrRedisExecFailed
	}
	redisConn.Close()
	return err
}

func (api *RedisApi) Del(key string) (err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("DEL", key)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("DEL", key)
	}
	delCount, err := redis.Int(r, err)
	if err == nil && delCount == 1 {
		err = nil
	} else if err == nil && delCount == 0 {
		err = nil
	}
	redisConn.Close()
	return err
}

func (api *RedisApi) Exists(key string) (result bool, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("EXISTS", key)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("EXISTS", key)
	}
	count, err := redis.Int(r, err)
	if err == nil {
		if count == 1 {
			result = true
		} else {
			result = false
		}
	} else {
		result = false
	}
	redisConn.Close()
	return result, err
}

// 检查key的过期时间
func (api *RedisApi) TTL(key string) (result int, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("TTL", key)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("TTL", key)
	}
	result, err = redis.Int(r, err)
	redisConn.Close()
	return result, err
}

// 设置Key的生存时间
func (api *RedisApi) Expire(key string, seconds uint64) (err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("EXPIRE", key, seconds)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("EXPIRE", key, seconds)
	}
	retValue, err := redis.Int(r, err)
	if err == nil && retValue == 1 {
		err = nil
	} else if err == nil {
		err = ErrRedisExecFailed
	}
	redisConn.Close()
	return err
}

// 设置Key的生存时间 使用UnixTimeStamp 设置
func (api *RedisApi) Expireat(key string, timeStamp int64) (err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("EXPIREAT", key, timeStamp)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("EXPIREAT", key, timeStamp)
	}
	retValue, err := redis.Int(r, err)
	if err == nil && retValue == 1 {
		err = nil
	} else if err == nil {
		err = ErrRedisExecFailed
	}
	redisConn.Close()
	return err
}

func (api *RedisApi) Keys(pattern string) (value []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("KEYS", pattern)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("KEYS", pattern)
	}
	value, err = redis.Strings(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) Hget(key, field string) (value string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HGET", key, field)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HGET", key, field)
	}
	value, err = redis.String(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) HgetInt64(key, field string) (value int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HGET", key, field)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HGET", key, field)
	}
	value, err = redis.Int64(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) Hset(key, field, value string) (err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HSET", key, field, value)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HSET", key, field, value)
	}
	retValue, err := redis.Int(r, err)
	if err == nil && (retValue == 1 || retValue == 0) {
		err = nil
	} else if err == nil {
		err = ErrRedisExecFailed
	}
	redisConn.Close()
	return err
}

func (api *RedisApi) HsetInt64(key, field string, value int64) (err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HSET", key, field, value)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HSET", key, field, value)
	}
	retValue, err := redis.Int(r, err)
	if err == nil && (retValue == 1 || retValue == 0) {
		err = nil
	} else if err == nil {
		err = ErrRedisExecFailed
	}
	redisConn.Close()
	return err
}

func (api *RedisApi) Hdel(key, field string) (result bool, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HDEL", key, field)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HDEL", key, field)
	}
	delCount, err := redis.Int(r, err)
	if err == nil && delCount == 1 {
		result = true
	} else if err == nil {
		err = ErrRedisExecFailed
		result = false
	}
	redisConn.Close()
	return result, err
}

func (api *RedisApi) HBatchDel(key string, field []string) (delCount int, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	var args []interface{}
	args = append(args, key)
	for _, v := range field {
		args = append(args, v)
	}
	r, err := redisConn.Do("HDEL", args...)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HDEL", args...)
	}
	delCount, err = redis.Int(r, err)
	redisConn.Close()
	return delCount, err
}

func (api *RedisApi) Hmget(key string, field []string) (value []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	var args []interface{}
	args = append(args, key)
	for _, v := range field {
		args = append(args, v)
	}
	r, err := redisConn.Do("HMGET", args...)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HMGET", args...)
	}
	value, err = redis.Strings(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) Hlen(key string) (count int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HLEN", key)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HLEN", key)
	}
	count, err = redis.Int64(r, err)
	redisConn.Close()
	return count, err
}

// Hmset 使用key-value 键值对组成的slice
func (api *RedisApi) Hmset(key string, keyValue []string) (err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	var args []interface{}
	args = append(args, key)
	for _, v := range keyValue {
		args = append(args, v)
	}
	r, err := redisConn.Do("HMSET", args...)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HMSET", args...)
	}
	retValue, err := redis.String(r, err)
	if err == nil && retValue == "OK" {
	} else if err == nil {
		err = ErrRedisExecFailed
	}
	redisConn.Close()
	return err
}

func (api *RedisApi) Hgetall(key string) (keyValue map[string]string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HGETALL", key)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HGETALL", key)
	}
	keyValue, err = redis.StringMap(r, err)
	redisConn.Close()
	return keyValue, err
}

func (api *RedisApi) Hincrby(key, field string, increment int) (value int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HINCRBY", key, field, increment)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HINCRBY", key, field, increment)
	}
	value, err = redis.Int64(r, err)
	redisConn.Close()
	return value, err
}

// hscan 当hash map 较小时返回整个hashmap,当hashmap 较大时，返回前面的10条
func (api *RedisApi) Hscan(key string, cursor int64) (items []string, outCursor int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	values, err := redis.Values(redisConn.Do("HSCAN", key, cursor))
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		values, err = redis.Values(redisConn.Do("HSCAN", key, cursor))
	}
	_, err = redis.Scan(values, &outCursor, &items)
	redisConn.Close()
	return items, outCursor, err
}

func (api *RedisApi) Hexists(key, field string) (ret int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("HEXISTS", key, field)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("HEXISTS", key, field)
	}
	ret, err = redis.Int64(r, err)
	redisConn.Close()
	return ret, err
}

func (api *RedisApi) Hkeys(key string) (value []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("Hkeys", key)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("Hkeys", key)
	}
	value, err = redis.Strings(r, err)
	redisConn.Close()
	return value, err
}

// zset method
// 返回 被成功添加的新成员的数量，不包括那些被更新的、已经存在的成员
func (api *RedisApi) Zadd(key string, score int64, member string) (value int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("ZADD", key, score, member)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZADD", key, score, member)
	}
	value, err = redis.Int64(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) ZaddSlice(key string, keyScore []uint32) (value int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	var args []interface{}
	args = append(args, key)
	keyScoreLne := len(keyScore)
	for i := 0; i < keyScoreLne; i = i + 2 { //key-score pari
		member := fmt.Sprintf("%v", keyScore[i])
		score := keyScore[i+1]
		args = append(args, score)
		args = append(args, member)
	}
	r, err := redisConn.Do("ZADD", args...)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZADD", args...)
	}
	value, err = redis.Int64(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) ZaddStringSlice(key string, keyScore []string) (value int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	var args []interface{}
	args = append(args, key)
	keyScoreLne := len(keyScore)
	for i := 0; i < keyScoreLne; i = i + 2 { //key-score pari
		member := keyScore[i]
		score := keyScore[i+1]
		args = append(args, score)
		args = append(args, member)
	}
	r, err := redisConn.Do("ZADD", args...)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZADD", args...)
	}
	value, err = redis.Int64(r, err)
	redisConn.Close()
	return value, err
}

// 返回 被成功移除的成员的数量，不包括被忽略的成员。
func (api *RedisApi) Zrem(key, member string) (value int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("ZREM", key, member)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZREM", key, member)
	}
	value, err = redis.Int64(r, err)
	redisConn.Close()
	return value, err
}

// 返回 指定区间内，带有 score 值(可选)的有序集成员的列表
func (api *RedisApi) Zrange(key string, start, stop int64) (valSlice []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("ZRANGE", key, start, stop, "WITHSCORES")
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZRANGE", key, start, stop, "WITHSCORES")
	}

	valSlice, err = redis.Strings(r, err)
	redisConn.Close()
	return valSlice, err
}

func (api *RedisApi) ZrangeWithOutScore(key string, start, stop int64) (keys []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("ZRANGE", key, start, stop)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZRANGE", key, start, stop)
	}

	keys, err = redis.Strings(r, err)
	redisConn.Close()
	return keys, err
}

func (api *RedisApi) Zrevrange(key string, start, stop int64) (valSlice []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("ZREVRANGE", key, start, stop, "WITHSCORES")
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZREVRANGE", key, start, stop, "WITHSCORES")
	}

	valSlice, err = redis.Strings(r, err)
	redisConn.Close()
	return valSlice, err
}

func (api *RedisApi) ZrevrangeWithOutScore(key string, start, stop int64) (keys []string, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("ZREVRANGE", key, start, stop)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZREVRANGE", key, start, stop)
	}

	keys, err = redis.Strings(r, err)
	redisConn.Close()
	return keys, err
}

// 返回 member 成员的 score 值，以字符串形式表示
func (api *RedisApi) Zscore(key, member string) (value uint64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("ZSCORE", key, member)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("ZSCORE", key, member)
	}

	value, err = redis.Uint64(r, err)
	redisConn.Close()
	return value, err
}

func (api *RedisApi) Lpush(key, value string) (listSize int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("LPUSH", key, value)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("LPUSH", key, value)
	}
	listSize, err = redis.Int64(r, err)
	redisConn.Close()
	return listSize, err
}

func (api *RedisApi) Rpush(key, value string) (listSize int64, err error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("RPUSH", key, value)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("RPUSH", key, value)
	}
	listSize, err = redis.Int64(r, err)
	redisConn.Close()
	return listSize, err
}

///blpop 操作
func (api *RedisApi) BLpop(key string, timeout uint8) (string, error) {
	// 获取一条Redis连接
	redisConn := api.redisPool.Get()
	r, err := redisConn.Do("blpop", key, timeout)
	if err == io.EOF {
		redisConn.Close()
		redisConn = api.redisPool.Get()
		r, err = redisConn.Do("blpop", key, timeout)
	}
	var value []string
	value, err = redis.Strings(r, err)
	redisConn.Close()
	if value == nil {
		return "", err
	}

	return value[len(value)-1], err
}

func (api *RedisApi) StartLoop(
	key string,
	timeout uint8,
	max_goroutine_limit uint32,
	callback handleListLoopMessageFunc) {

	for i := uint32(0); i < max_goroutine_limit; i++ {
		go func() {

			for {
				info, rerr := api.BLpop(key, timeout)
				if rerr != nil {
					fmt.Println("StartLoop() redis blpop error", rerr, api.redisServer)
					continue
				}
				callback(&info)
			}
		}()

	}
}
