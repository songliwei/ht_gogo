package common

import (
	// "sort"
	"sync"
)

type Set struct {
	m map[uint32]bool
	sync.RWMutex
}

func NewSet() *Set {
	return &Set{
		m: map[uint32]bool{},
	}
}

func (s *Set) Add(item uint32) {
	s.Lock()
	defer s.Unlock()
	s.m[item] = true
}

func (s *Set) Remove(item uint32) {
	s.Lock()
	defer s.Unlock()
	delete(s.m, item)
}

func (s *Set) Has(item uint32) bool {
	s.RLock()
	defer s.RUnlock()
	_, ok := s.m[item]
	return ok
}

func (s *Set) Len() int {
	return len(s.List())
}

func (s *Set) Clear() {
	s.Lock()
	defer s.Unlock()
	s.m = map[uint32]bool{}
}

func (s *Set) IsEmpty() bool {
	if s.Len() == 0 {
		return true
	}
	return false
}

func (s *Set) List() []uint32 {
	s.RLock()
	defer s.RUnlock()
	list := []uint32{}
	for item := range s.m {
		list = append(list, item)
	}
	return list
}

// func (s *Set) SortList() []uint32 {
// 	s.RLock()
// 	defer s.RUnlock()
// 	list := []uint32{}
// 	for item := range s.m {
// 		list = append(list, item)
// 	}
// 	sort.Ints(list)
// 	return list
// }
