package common

import (
	"errors"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/golang/protobuf/proto"
)

const (
	ERR_CODE_INTERNAL = 500
)

var (
	ErrPacketCmdNotMatch = errors.New("cmd not match")
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type MntApi struct {
	mntPool *Pool
}

func newMntPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewMntApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *MntApi {
	pool := newMntPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &MntApi{
		mntPool: pool,
	}
}

func (c *MntApi) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.mntPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(packet)

	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}

	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *MntApi) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet = NewHeadV2Packet(buf)
	conn := c.mntPool.Get()
	defer conn.Close()

	packet, err = conn.Do(packet)
	return packet, nil
}

func (c *MntApi) GetUidList(opUid uint32, listType ht_moment.OPERATORLIST) (outType ht_moment.OPERATORLIST, hideList []uint32, notShareList []uint32, err error) {
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_GET_OPERATOR_UID_LIST),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      opUid,
	}

	reqBody := new(ht_moment.ReqBody)
	reqBody.GetOpUidListReqbody = &ht_moment.GetUidListReqBody{
		ListType:    listType.Enum(),
		HideListVer: proto.Uint32(0),
		NotShareVer: proto.Uint32(0),
		Userid:      proto.Uint32(opUid),
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return outType, nil, nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return outType, nil, nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.mntPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.mntPool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return outType, nil, nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return outType, nil, nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_moment.RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return outType, nil, nil, err
	}

	if rspHead.Cmd != uint32(ht_moment.CMD_TYPE_CMD_GET_OPERATOR_UID_LIST) {
		err = ErrExpectedResp
		return outType, nil, nil, err
	}

	rspBody := &ht_moment.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return outType, nil, nil, err
	}
	subRspBody := rspBody.GetGetOpUidListRspbody()
	outType = subRspBody.GetListType()
	hideArray := subRspBody.GetHideUidArray().GetUidArray()
	for _, v := range hideArray {
		hideList = append(hideList, v.GetUid())
	}
	notShareArray := subRspBody.GetNotShareArray().GetUidArray()
	for _, v := range notShareArray {
		notShareList = append(notShareList, v.GetUid())
	}
	return outType, hideList, notShareList, nil
}

//后台信息流控制
// enum MntStatusOpType {
//      OP_RESTORE                      = 0;
//      OP_DELETE_BY_USER       = 1; //用户主动删除
//      OP_HIDE                         = 2; //后台隐藏

//      OP_DELETE                       = 3; //后台删除

//      // backend server op type
//      OP_FOR_FOLLOWER_ONLY = 10;      // 只给粉丝看帖子
//      OP_FOR_SELF_ONLY     = 11;      // 只给自己看
// }

func (c *MntApi) ModMntStatus(Mid string, opType uint32, opReason string) (uint32, error) {

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_STATUS),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      10000,
	}

	reqBody := new(ht_moment.ReqBody)

	opTypeEnum := ht_moment.MntStatusOpType(opType)

	reqBody.ModMntStatusReqbody = &ht_moment.ModMntStatusReqBody{
		Mid:      []byte(Mid),
		OpType:   opTypeEnum.Enum(),
		OpReason: []byte(opReason),
	}

	_, rspBody, err := c.SendMomentBody(head, reqBody)

	if err != nil {
		return ERR_CODE_INTERNAL, err
	}

	modRspBody := rspBody.GetModMntStatusRspbody()

	pbHeader := modRspBody.GetStatus()

	if *pbHeader.Code == 0 {
		return *pbHeader.Code, nil
	} else {
		return *pbHeader.Code, errors.New(string(pbHeader.Reason))
	}

}

// 将用户的帖子从个人桶移动到 语言 或者 follower桶

/*
enum MntBucketType {
	INI_TO_ME 			=	0;
	INI_TO_FOWLLER		=	1;
	INI_TO_ALL			=	2;
}
*/

func (c *MntApi) ModMntBucket(Mid string, BucketType uint32, opReason string, momentBody *ht_moment.MomentBody) (uint32, error) {

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_BUCKET),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      10000,
	}

	reqBody := new(ht_moment.ReqBody)

	opTypeEnum := ht_moment.MntBucketType(BucketType)

	reqBody.ModMntBucketReqbody = &ht_moment.ModMntBucketReqBody{
		Mid:        []byte(Mid),
		BucketType: opTypeEnum.Enum(),
		OpReason:   []byte(opReason),
		Moment:     momentBody,
	}

	_, rspBody, err := c.SendMomentBody(head, reqBody)

	if err != nil {
		return ERR_CODE_INTERNAL, err
	}

	modRspBody := rspBody.GetModMntBucketRspbody()

	pbHeader := modRspBody.GetStatus()

	if *pbHeader.Code == 0 {
		return *pbHeader.Code, nil
	} else {
		return *pbHeader.Code, errors.New(string(pbHeader.Reason))
	}

}

func (c *MntApi) DeleteUser(userids []uint32) (uint32, error) {

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_DELETE_USER),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      10000,
	}

	reqBody := new(ht_moment.ReqBody)

	reqBody.DeleteUserReqbody = &ht_moment.DeleteUserReqBody{
		Userid: userids,
	}

	_, rspBody, err := c.SendMomentBody(head, reqBody)

	if err != nil {
		return ERR_CODE_INTERNAL, err
	}

	modRspBody := rspBody.GetDeleteUserRspbody()

	pbHeader := modRspBody.GetStatus()

	if *pbHeader.Code == 0 {
		return *pbHeader.Code, nil
	} else {
		return *pbHeader.Code, errors.New(string(pbHeader.Reason))
	}

}

// opType OP_ADD                                                =       1; // 添加到列表
// opType OP_DEL                                                =       2; // 从列表中删除

// listType OP_MOMENT_FOR_FOLLOWER_LIST = 8;    // Momnet 只能给粉丝看用户列表
// listType OP_MOMENT_FOR_SELF_LIST             = 9;    // Momnet 只能给自己看

func (c *MntApi) SetUserMomentSelfOrFollower(opUserid uint32, listType uint32, opType uint32, opReason string) (uint32, error) {

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_BACKEND_OP_UID),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      10000,
	}

	reqBody := new(ht_moment.ReqBody)

	listTypeEnum := ht_moment.OPERATORLIST(listType)
	opTypeEnum := ht_moment.OPERATORTYPE(opType)

	//wait to be added

	reqBody.BackendOpUidReqbody = &ht_moment.BackEndOpUidReqBody{
		ListType: listTypeEnum.Enum(),
		OpType:   opTypeEnum.Enum(),
		OpUserId: proto.Uint32(opUserid),
		Userid:   proto.Uint32(10000),
		OpReason: []byte(opReason),
	}

	_, rspBody, err := c.SendMomentBody(head, reqBody)

	if err != nil {
		return ERR_CODE_INTERNAL, err
	}

	modRspBody := rspBody.GetBackendOpUidRspbody()

	if modRspBody == nil {
		return ERR_CODE_INTERNAL, errors.New("GetModMntStatusRspbody nil returned")
	}

	pbHeader := modRspBody.GetStatus()

	if pbHeader == nil {
		return ERR_CODE_INTERNAL, errors.New("GetStatus nil returned")
	}

	if *pbHeader.Code == 0 {
		return *pbHeader.Code, nil
	} else {
		return *pbHeader.Code, errors.New(string(pbHeader.Reason))
	}

}

//通过Mid 获取 Mid的内容。
func (c *MntApi) ViewMomentByMid(Userid uint32, allMid []string, areCode string) (uint32, []*ht_moment.MomentBlockInfo, error) {

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_GET_MID_CONTENT),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      Userid,
	}

	reqBody := new(ht_moment.ReqBody)
	list := []*ht_moment.MomentBlockInfo{}

	Mids := [][]byte{}

	for _, v := range allMid {
		Mids = append(Mids, []byte(v))
	}

	reqBody.ViewMntReqbody = &ht_moment.ViewMomentReqBody{
		Mid:      Mids,
		Userid:   proto.Uint32(Userid),
		AreaCode: []byte(areCode),
	}

	_, rspBody, err := c.SendMomentBody(head, reqBody)

	if err != nil {
		return ERR_CODE_INTERNAL, list, err
	}

	modRspBody := rspBody.GetViewMntRspbody()

	if modRspBody == nil {
		return ERR_CODE_INTERNAL, list, errors.New("GetViewMntRspbody nil returned")
	}

	pbHeader := modRspBody.GetStatus()

	if pbHeader == nil {
		return ERR_CODE_INTERNAL, list, errors.New("GetStatus nil returned")
	}

	if *pbHeader.Code == 0 {
		return *pbHeader.Code, modRspBody.GetMomentList(), nil
	} else {
		return *pbHeader.Code, modRspBody.GetMomentList(), errors.New(string(pbHeader.Reason))
	}

}

//获取用户帖子总数以及点赞数
func (c *MntApi) GetUserMomentCountInfo(Userid uint32) (mntCount uint32, likedCount uint32, err error) {

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INFO),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      Userid,
	}

	reqBody := new(ht_moment.ReqBody)

	reqBody.GetUserInfoReqbody = &ht_moment.GetUserInfoReqBody{
		Userid: proto.Uint32(Userid),
	}

	_, rspBody, err := c.SendMomentBody(head, reqBody)

	if err != nil {
		return 0, 0, err
	}

	modRspBody := rspBody.GetGetUserInfoRspbody()

	if modRspBody == nil {
		return 0, 0, errors.New("GetUserInfoRspBody nil returned")
	}

	pbHeader := modRspBody.GetStatus()

	if pbHeader == nil {
		return 0, 0, errors.New("GetStatus nil returned")
	}

	if *pbHeader.Code == 0 {
		return modRspBody.GetMomentCount(), modRspBody.GetLikedCount(), nil
	} else {
		return 0, 0, errors.New(string(pbHeader.Reason))
	}

}

//获取用户的第几天moment
func (c *MntApi) GetUserIndexedMoment(Userid uint32, Index uint32) (uint32, []*ht_moment.MomentBody, error) {

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_GET_USER_INDEX_MOMENT),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      Userid,
	}

	list := []*ht_moment.MomentBody{}

	reqBody := new(ht_moment.ReqBody)

	reqBody.GetUserIndexMomentReqbody = &ht_moment.GetUserIndexMomentReqBody{

		Userid: proto.Uint32(Userid),
		UserIndex: []*ht_moment.MomentUserIndexInfo{
			&ht_moment.MomentUserIndexInfo{
				Userid: proto.Uint32(Userid),
				Seq:    proto.Uint32(Index),
			},
		},
	}

	_, rspBody, err := c.SendMomentBody(head, reqBody)

	if err != nil {
		return ERR_CODE_INTERNAL, list, err
	}

	modRspBody := rspBody.GetGetUserIndexMomentRspbody()

	if modRspBody == nil {
		return ERR_CODE_INTERNAL, list, errors.New("GetGetUserIndexMomentRspbody nil returned")
	}

	pbHeader := modRspBody.GetStatus()

	if pbHeader == nil {
		return ERR_CODE_INTERNAL, list, errors.New("GetStatus nil returned")
	}

	if *pbHeader.Code == 0 {
		return *pbHeader.Code, modRspBody.GetMoment(), nil
	} else {
		return *pbHeader.Code, modRspBody.GetMoment(), errors.New(string(pbHeader.Reason))
	}

}

func (c *MntApi) BuildMomentV2Body(head *HeadV2, reqBody *ht_moment.ReqBody) (*HeadV2Packet, error) {

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)

	return packet, nil
}

func (c *MntApi) SendMomentBody(head *HeadV2, reqBody *ht_moment.ReqBody) (*HeadV2, *ht_moment.RspBody, error) {

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.mntPool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.mntPool.Get()
		rsp, err = conn.Do(packet)
		if err != nil {
			conn.Close()
			return nil, nil, err
		}
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		return nil, nil, ErrChangePacketFailed
	}

	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_moment.RET_CODE_RET_SUCCESS) {
		return nil, nil, ErrExpectedResp
	}

	if rspHead.Cmd != head.Cmd {
		return nil, nil, ErrPacketCmdNotMatch
	}

	rspBody := &ht_moment.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, nil, ErrGetRespPacketFaild
	}

	return head, rspBody, nil
}
