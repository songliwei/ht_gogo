package common

import (
	"bytes"
	"compress/zlib"
	"fmt"
	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_group_lesson"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
	"time"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type GroupLessonApi struct {
	groupLessonPool *Pool
}

func newGroupLessonPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewGroupLessonApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *GroupLessonApi {
	pool := newGroupLessonPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &GroupLessonApi{
		groupLessonPool: pool,
	}
}

func (c *GroupLessonApi) SendPacket(head *HeadV3, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.groupLessonPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *GroupLessonApi) SendAndRecvPacket(head *HeadV3, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.groupLessonPool.Get()
	defer conn.Close()

	packet, err = conn.Do(headV3Packet)
	return packet, nil
}

func (c *GroupLessonApi) JustSendPacket(head *HeadV3, payLoad []uint8) (err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.groupLessonPool.Get()
	defer conn.Close()

	err = conn.Send(headV3Packet)
	return err
}

func (c *GroupLessonApi) UpdateGroupLessonRecordUrl(roomId uint32, recordObid []byte, url []byte) (err error) {
	head := &HeadV3{
		From:    0,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_group_lesson.GL_CMD_TYPE_CMD_UPDATE_LESSON_RECORD_URL_REQ),
		SysType: uint16(ht_group_lesson.GROUP_LESSON_SYS_TYPE_GROUP_LESSON_SYS_IM),
	}

	reqBody := &ht_group_lesson.GroupLessonReqBody{
		UpdateGroupLessonRecordReqbody: &ht_group_lesson.UpdateGroupLessonRecordUrlReqBody{
			RoomId:          proto.Uint32(roomId),
			GroupLessonObid: recordObid,
			RecordUrl:       url,
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.groupLessonPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}
	if rspHead.Cmd != uint16(ht_group_lesson.GL_CMD_TYPE_CMD_UPDATE_LESSON_RECORD_URL_REQ) {
		err = ErrExpectedResp
		return err
	}
	return nil
}

func (c *GroupLessonApi) DelGroupLessonByUid(roomId, reqUid uint32) (err error) {
	head := &HeadV3{
		From:    0,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_group_lesson.GL_CMD_TYPE_CMD_DEL_GROUP_LESSON_BY_UID),
		SysType: uint16(ht_group_lesson.GROUP_LESSON_SYS_TYPE_GROUP_LESSON_SYS_IM),
	}

	reqBody := &ht_group_lesson.GroupLessonReqBody{
		DelGroupLessonByUidReqbody: &ht_group_lesson.DelGroupLessonByUidReqBody{
			RoomId:    proto.Uint32(roomId),
			CreateUid: proto.Uint32(reqUid),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.groupLessonPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}
	if rspHead.Cmd != uint16(ht_group_lesson.GL_CMD_TYPE_CMD_DEL_GROUP_LESSON_BY_UID) {
		err = ErrExpectedResp
		return err
	}
	return nil
}

/**
 * 获取群组课程信息
 * @param recordObid []byte [description]
 * @return {[type]}   [description]
 */
func (c *GroupLessonApi) GetGroupLessonInfo(reqUid uint32, recordObid []byte) (detailInfo *ht_group_lesson.LessonDetailInfo, err error) {
	// 构造头
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_LESSON_BY_OBID),
		SysType: uint16(ht_group_lesson.GROUP_LESSON_SYS_TYPE_GROUP_LESSON_SYS_IM), // 这个抄上面的
	}
	// 构造请求体
	reqBody := &ht_group_lesson.GroupLessonReqBody{
		GetLessonByObidReqbody: &ht_group_lesson.GetLessonByObidReqBody{
			ReqUid:              proto.Uint32(reqUid),
			RoomId:              proto.Uint32(0),
			GroupLessonObidList: [][]byte{recordObid},
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.groupLessonPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	if rspHead.Cmd != uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_LESSON_BY_OBID) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_group_lesson.GroupLessonRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	// subRspBody := rspBody.GetGetChatRecordRspbody()
	subRspBody := rspBody.GetGetLessonByObidRspbody()
	lessonList := subRspBody.GetLessonInfoList()
	if len(lessonList) < 1 {
		return nil, nil
	}
	// 只取第一个
	detailInfo = lessonList[0]
	// groupLesson = detailInfo.GetGroupLesson()
	return detailInfo, nil

}

// func (c *GroupLessonApi) UncompressBuff() {

// }

// 获取历史聊天记录
func (c *GroupLessonApi) GetRecordChatMsgList(recordObid []byte, reqUid, roomId uint32, cliSeq uint64) (maxSeq uint64, msgListStr []*simplejson.Json, err error) {
	// 构造头
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_LATEST_LESSON_MSG_REQ),
		SysType: uint16(ht_group_lesson.GROUP_LESSON_SYS_TYPE_GROUP_LESSON_SYS_IM), // 这个抄上面的
	}
	// 构造请求体
	reqBody := &ht_group_lesson.GroupLessonReqBody{
		GetLatestGroupLessonMsgReqbody: &ht_group_lesson.GetLatestGroupLessonMsgReqBody{
			ReqUid:     proto.Uint32(reqUid),
			RoomId:     proto.Uint32(roomId),
			CliSeq:     proto.Uint64(cliSeq),
			RecordObid: recordObid,
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return 0, nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return 0, nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.groupLessonPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return 0, nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return 0, nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return 0, nil, err
	}
	if rspHead.Cmd != uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_LATEST_LESSON_MSG_REQ) {
		err = ErrExpectedResp
		return 0, nil, err
	}

	rspBody := &ht_group_lesson.GroupLessonRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return 0, nil, err
	}

	// 下面从RspBody中获取数据
	subRspBody := rspBody.GetGetLatestGroupLessonMsgRspbody()
	msgListByte := subRspBody.GetMsgList()

	msgListStr = make([]*simplejson.Json, len(msgListByte))

	for k, v := range msgListByte {
		// 解压消息
		compressBuff := bytes.NewBuffer(v)
		r, err := zlib.NewReader(compressBuff)
		defer r.Close()
		if err != nil {
			fmt.Printf("GetRecordChatMsgList zlib.NewReader failed err=%v", err)
			return 0, nil, err
		}
		// 解压缩
		unCompressSlice, err := ioutil.ReadAll(r)
		if err != nil {
			fmt.Printf("GetRecordChatMsgList zlib. unCompress failed")
			return 0, nil, err
		}
		// 解压之后的内容构造json
		msgListStr[k], err = simplejson.NewJson(unCompressSlice)
		if err != nil {
			fmt.Printf("GetRecordChatMsgList UnmarshalJSON err : %+v\n", err)
			return 0, nil, err
		}
	}

	maxSeq = subRspBody.GetCurMaxSeq()

	// fmt.Printf("msgListStr: %+v", msgListStr)
	return maxSeq, msgListStr, nil
}

func (c *GroupLessonApi) GetRecordInfo(recordObid []byte, reqUid uint32) (recordInfo *ht_group_lesson.GetRecordByObidRspBody, err error) {
	// 构造头
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_RECORD_BY_OBID_REQ),
		SysType: uint16(ht_group_lesson.GROUP_LESSON_SYS_TYPE_GROUP_LESSON_SYS_IM), // 这个抄上面的
	}
	// 构造请求体
	reqBody := &ht_group_lesson.GroupLessonReqBody{
		GetRecordByObidReqbody: &ht_group_lesson.GetRecordByObidReqBody{
			ReqUid:     proto.Uint32(reqUid),
			RecordObid: recordObid,
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.groupLessonPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_group_lesson.GROUP_LESSON_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	if rspHead.Cmd != uint16(ht_group_lesson.GL_CMD_TYPE_CMD_GET_RECORD_BY_OBID_REQ) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_group_lesson.GroupLessonRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}

	// 下面从RspBody中获取数据
	subRspBody := rspBody.GetGetRecordByObidRspbody()
	recordInfo = subRspBody
	return recordInfo, nil
}
