package common

import (
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_wallet"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type WalletApi struct {
	walletPool *Pool
}

func newWalletPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewWalletApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *WalletApi {
	pool := newWalletPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &WalletApi{
		walletPool: pool,
	}
}

func (c *WalletApi) SendPacket(head *HeadV3, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.walletPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *WalletApi) SendAndRecvPacket(head *HeadV3, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.walletPool.Get()
	defer conn.Close()

	packet, err = conn.Do(headV3Packet)
	return packet, nil
}

func (c *WalletApi) JustSendPacket(head *HeadV3, payLoad []uint8) (err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.walletPool.Get()
	defer conn.Close()

	err = conn.Send(headV3Packet)
	return err
}

func (c *WalletApi) GetExchangeRate(reqUid uint32, currency string) (rate uint64, err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_EXCHANGE_REATE_REQ),
		SysType: uint16(ht_wallet.WALLET_SYS_TYPE_WALLET_SYS_IM),
	}

	reqBody := &ht_wallet.WalletReqBody{
		QueryExchangeRateReqbody: &ht_wallet.QueryExchangeRateReqBody{
			ReqUid:          proto.Uint32(reqUid),
			SrcCurrencyType: proto.String(currency),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return rate, err
	}
	rsp, err := c.SendAndRecvPacket(head, payLoad)
	if err != nil {
		return rate, err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return rate, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return rate, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return rate, err
	}
	if rspHead.Cmd != uint16(ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_EXCHANGE_REATE_REQ) {
		err = ErrExpectedResp
		return rate, err
	}

	rspBody := &ht_wallet.WalletRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return rate, err
	}
	subRspBody := rspBody.GetQueryExchangeRateRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return rate, err
	}
	rate = subRspBody.GetCorrespondingDollar()
	return rate, nil
}

func (c *WalletApi) GetChargingInfoById(reqUid uint32, chargingId uint64) (chargingInfo *ht_wallet.ChargingInfo, err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_CHARGING_INFO_BY_ID_REQ),
		SysType: uint16(ht_wallet.WALLET_SYS_TYPE_WALLET_SYS_IM),
	}

	reqBody := &ht_wallet.WalletReqBody{
		QueryChargingInfoByIdReqbody: &ht_wallet.QueryChargingInfoByIdReqBody{
			ReqUid:     proto.Uint32(reqUid),
			ChargingId: proto.Uint64(chargingId),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}
	rsp, err := c.SendAndRecvPacket(head, payLoad)
	if err != nil {
		return nil, err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	if rspHead.Cmd != uint16(ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_CHARGING_INFO_BY_ID_REQ) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_wallet.WalletRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetQueryChargingInfoByIdRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	chargingInfo = subRspBody.GetCharginInfo()
	return chargingInfo, nil
}

func (c *WalletApi) SuccessPurchaseLesson(reqUid uint32,
	nickName string,
	headUrl string,
	national string,
	payTs uint64,
	chargingId uint64,
	payType uint32,
	transId string,
	remarks string) (code uint32, err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_wallet.WALLET_CMD_TYPE_CMD_SUCC_PURCHASE_LESSON_REQ),
		SysType: uint16(ht_wallet.WALLET_SYS_TYPE_WALLET_SYS_IM),
	}

	reqBody := &ht_wallet.WalletReqBody{
		SuccessPurchaseLessonReqbody: &ht_wallet.SuccessPurchaseLessonReqBody{
			ReqUid:        proto.Uint32(reqUid),
			NickName:      proto.String(nickName),
			HeadUrl:       proto.String(headUrl),
			National:      proto.String(national),
			PayTs:         proto.Uint64(payTs),
			ChargingId:    proto.Uint64(chargingId),
			AccountType:   ht_wallet.AccountType(payType).Enum(),
			TransactionId: proto.String(transId),
			Remarks:       proto.String(remarks),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}
	rsp, err := c.SendAndRecvPacket(head, payLoad)
	if err != nil {
		return code, err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return code, err
	}
	if rspHead.Cmd != uint16(ht_wallet.WALLET_CMD_TYPE_CMD_SUCC_PURCHASE_LESSON_REQ) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_wallet.WalletRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetSuccessPurchaseLessonRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *WalletApi) GetRoomExistLessonCharging(roomId, reqUid uint32) (exist bool, err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_ROOM_EXIST_LESSON_CHARGE_REQ),
		SysType: uint16(ht_wallet.WALLET_SYS_TYPE_WALLET_SYS_IM),
	}

	reqBody := &ht_wallet.WalletReqBody{
		QueryRoomExistLessonReqbody: &ht_wallet.QueryRoomExistLessonReqBody{
			ReqUid: proto.Uint32(reqUid),
			RoomId: proto.Uint32(roomId),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return exist, err
	}
	rsp, err := c.SendAndRecvPacket(head, payLoad)
	if err != nil {
		return exist, err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return exist, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return exist, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return exist, err
	}
	if rspHead.Cmd != uint16(ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_ROOM_EXIST_LESSON_CHARGE_REQ) {
		err = ErrExpectedResp
		return exist, err
	}

	rspBody := &ht_wallet.WalletRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return exist, err
	}
	subRspBody := rspBody.GetQueryRoomExistLessonRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return exist, err
	}
	exist = subRspBody.GetHasLessonCharge()
	return exist, nil
}

func (c *WalletApi) QueryLessonIsCharging(roomId, reqUid uint32, lessonObid string) (exist bool, err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_LESSON_IS_CHARGE_REQ),
		SysType: uint16(ht_wallet.WALLET_SYS_TYPE_WALLET_SYS_IM),
	}

	reqBody := &ht_wallet.WalletReqBody{
		QueryLessonIsChargingReqbody: &ht_wallet.QueryLessonIsChargingReqBody{
			ReqUid:     proto.Uint32(reqUid),
			RoomId:     proto.Uint32(roomId),
			LessonObid: proto.String(lessonObid),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return exist, err
	}
	rsp, err := c.SendAndRecvPacket(head, payLoad)
	if err != nil {
		return exist, err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return exist, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return exist, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return exist, err
	}
	if rspHead.Cmd != uint16(ht_wallet.WALLET_CMD_TYPE_CMD_QUERY_LESSON_IS_CHARGE_REQ) {
		err = ErrExpectedResp
		return exist, err
	}

	rspBody := &ht_wallet.WalletRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return exist, err
	}
	subRspBody := rspBody.GetQueryLessonIsChargingRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return exist, err
	}
	exist = subRspBody.GetIsCharge()
	return exist, nil
}

func (c *WalletApi) BatchStopLessonCharging(reqUid uint32, groupLessonSlic []*ht_wallet.StopLessonCharginItem) (err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_STOP_LESSON_CHARGING),
		SysType: uint16(ht_wallet.WALLET_SYS_TYPE_WALLET_SYS_IM),
	}

	reqBody := &ht_wallet.WalletReqBody{
		BatchStopLessonChargingReqbody: &ht_wallet.BatchStopLessonChargingReqBody{
			ReqUid:     proto.Uint32(reqUid),
			LessonList: groupLessonSlic,
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return err
	}
	rsp, err := c.SendAndRecvPacket(head, payLoad)
	if err != nil {
		return err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_wallet.WALLET_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return err
	}
	if rspHead.Cmd != uint16(ht_wallet.WALLET_CMD_TYPE_CMD_BATCH_STOP_LESSON_CHARGING) {
		err = ErrExpectedResp
		return err
	}

	rspBody := &ht_wallet.WalletRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return err
	}
	subRspBody := rspBody.GetBatchStopLessonChargingRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return err
	}
	return nil
}
