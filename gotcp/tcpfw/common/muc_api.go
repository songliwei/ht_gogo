package common

import (
	"fmt"
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type MucApi struct {
	mucPool *Pool
}

func newMucPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewMucApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *MucApi {
	pool := newMucPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &MucApi{
		mucPool: pool,
	}
}

func (c *MucApi) SendPacket(head *HeadV3, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *MucApi) SendAndRecvPacket(head *HeadV3, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.mucPool.Get()
	defer conn.Close()

	packet, err = conn.Do(headV3Packet)
	return packet, nil
}

func (c *MucApi) JustSendPacket(head *HeadV3, payLoad []uint8) (err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.mucPool.Get()
	defer conn.Close()

	err = conn.Send(headV3Packet)
	return err
}

func (c *MucApi) IsUserAdmin(roomId, uid uint32) (isAdmin bool, err error) {
	head := &HeadV3{
		From:    uid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_IS_USER_ADMIN),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		QueryUserIsAdminReqbody: &ht_muc.QueryUserIsAdminReqBody{
			RoomId: proto.Uint32(roomId),
			Uid:    proto.Uint32(uid),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return isAdmin, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return isAdmin, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return isAdmin, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return isAdmin, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return isAdmin, err
	}
	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_IS_USER_ADMIN) {
		err = ErrExpectedResp
		return isAdmin, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return isAdmin, err
	}
	subRspBody := rspBody.GetQueryUserIsAdminRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return isAdmin, err
	}
	if subRspBody.GetResult() > 0 {
		isAdmin = true
	} else {
		isAdmin = false
	}
	return isAdmin, nil
}

func (c *MucApi) BroadCastTeachingRoomCreated(roomId, wBid uint32,
	createUid uint32,
	nickName []byte,
	headPhotoUrl []byte,
	country []byte,
	notifyUidList []uint32,
	mapedUidList []uint32,
	lessonName []byte,
	lessonAbstarct []byte,
	lessonCover []byte,
	createTime uint64,
	obid []byte) (code uint32, err error) {

	head := &HeadV3{
		From:    createUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_NOTIFY_CREATE_TEACHING_ROOM),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		NotifyCreateTeachingRoomReqbody: &ht_muc.NotifyCreateTeachingRoomReqBody{
			ChatRoomId:   proto.Uint32(roomId),
			WhiteBoardId: proto.Uint32(wBid),
			CreateUid: &ht_muc.RoomMemberInfo{
				Uid:          proto.Uint32(createUid),
				NickName:     nickName,
				HeadPhotoUrl: headPhotoUrl,
				Country:      country,
			},
			NotifyUidList:   notifyUidList,
			WbUidList:       mapedUidList,
			LessonName:      lessonName,
			LessonAbstract:  lessonAbstarct,
			LessonCoverUrl:  lessonCover,
			CreateTimeStamp: proto.Uint64(createTime),
			Obid:            obid,
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_NOTIFY_CREATE_TEACHING_ROOM) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetNotifyCreateTeachingRoomRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) BroadCastRoomStat(roomId, wBid, reqUid uint32,
	handsUpStat, whiteBoardStat int32,
	alreadyInSlic, handsUpSlic, connSlic []uint32,
	sessionTime uint64) (code uint32, err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_TEACHING_ROOM_STAT_BC_REQ),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		TeachingRoomStatBroadCastReqbody: &ht_muc.TeachingRoomStatBroadCastReqBody{
			RoomId:             proto.Uint32(roomId),
			WhiteBoardId:       proto.Uint32(roomId),
			HandsUpStat:        ht_muc.STAT_OP_TYPE(handsUpStat).Enum(),
			WhiteBoardStat:     ht_muc.STAT_OP_TYPE(whiteBoardStat).Enum(),
			AlreadyInList:      alreadyInSlic,
			HandsUpList:        handsUpSlic,
			MicrophoneConnList: connSlic,
			SessionTime:        proto.Uint64(sessionTime),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_TEACHING_ROOM_STAT_BC_REQ) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetTeachingRoomStatBroadCastRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) BroadCastTeachingRoomEnd(roomId, wBid, createUid uint32, notifyUidList []uint32, createTime uint64) (code uint32, err error) {
	head := &HeadV3{
		From:    createUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_NOTIFY_TEACHING_ROOM_END),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		NotifyTeachingRoomEndReqbody: &ht_muc.NotifyTeachingRoomEndReqBody{
			ChatRoomId:    proto.Uint32(roomId),
			WhiteBoardId:  proto.Uint32(wBid),
			NotifyUidList: notifyUidList,
			TimeStamp:     proto.Uint64(createTime),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_NOTIFY_TEACHING_ROOM_END) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetNotifyTeachingRoomEndRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) IsUserAlreadyInRoom(roomId, uid uint32) (isInRoom bool, err error) {
	head := &HeadV3{
		From:    uid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_QUREY_USR_IS_IN_ROOM),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		QueryUserIsAlreadyInReqbody: &ht_muc.QueryUserIsAlreadyInRoomReqBody{
			RoomId: proto.Uint32(roomId),
			OpUid:  proto.Uint32(uid),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return isInRoom, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return isInRoom, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return isInRoom, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return isInRoom, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return isInRoom, err
	}
	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_QUREY_USR_IS_IN_ROOM) {
		err = ErrExpectedResp
		return isInRoom, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return isInRoom, err
	}
	subRspBody := rspBody.GetQueryUserIsAlreadyInRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return isInRoom, err
	}
	isInRoom = subRspBody.GetIsInRoom()
	return isInRoom, nil
}

func (c *MucApi) BroadCastBeginGroupLesson(roomId uint32,
	teacherUid uint32,
	teacherName []byte,
	channelId []byte,
	groupLessonObid []byte,
	firstUrl []byte,
	lessonTitle []byte,
	lessonAbstract []byte,
	teacherAbstract []byte,
	lessonCoursewareUrl []byte,
	beginTime uint64,
	lessonTime []byte) (code uint32, err error) {

	head := &HeadV3{
		From:    teacherUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_BEGIN_GROUP_LESSON_BROADCASE_REQ),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		NotifyBeginGroupLessonReqbody: &ht_muc.NotifyBeginGroupLessonReqBody{
			TeacherUid:          proto.Uint32(teacherUid),
			TeacherName:         teacherName,
			RoomId:              proto.Uint32(roomId),
			ChannelId:           channelId,
			GroupLessonObid:     groupLessonObid,
			FirstUrl:            firstUrl,
			LessonTitle:         lessonTitle,
			LessonAbstract:      lessonAbstract,
			TeacherAbstract:     teacherAbstract,
			LessonCoursewareUrl: lessonCoursewareUrl,
			BeginTimeStamp:      proto.Uint64(beginTime),
			LessonTime:          lessonTime,
		},
	}
	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_BEGIN_GROUP_LESSON_BROADCASE_REQ) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetNotifyBeginGroupLessonRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) BroadCastEndGroupLesson(roomId uint32,
	teacherUid uint32,
	teacherName []byte,
	channelId []byte,
	endTime uint64) (code uint32, err error) {

	head := &HeadV3{
		From:    teacherUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_END_GROUP_LESSON_BROADCASE_REQ),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		NotifyEndGroupLessonReqbody: &ht_muc.NotifyEndGroupLessonReqBody{
			TeacherUid:  proto.Uint32(teacherUid),
			TeacherName: teacherName,
			RoomId:      proto.Uint32(roomId),
			ChannelId:   channelId,
			TimeStamp:   proto.Uint64(endTime),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_END_GROUP_LESSON_BROADCASE_REQ) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetNotifyEndGroupLessonRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) BroadCastGroupLessonStatChange(roomId, reqUid uint32,
	memberList []uint32,
	currentUrl []byte,
	timeStamp uint64) (code uint32, err error) {

	head := &HeadV3{
		From:    reqUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GROUP_STAT_CHANGE_BROADCASE_REQ),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		NotifyGroupLessonStatChangeReqbody: &ht_muc.NotifyGroupLessonStatChangeReqBody{
			RoomId:               proto.Uint32(roomId),
			StudentList:          memberList,
			CurrentCoursewareUrl: currentUrl,
			TimeStamp:            proto.Uint64(timeStamp),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GROUP_STAT_CHANGE_BROADCASE_REQ) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetNotifyGroupLessonStatChangeRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) BroadCastGroupLessonMessage(roomId uint32,
	senderUid uint32,
	recordObid []byte,
	message []byte,
	memberList []uint32,
	timeStamp uint64) (code uint32, err error) {

	head := &HeadV3{
		From:    senderUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GROUP_LESSON_MESSAGE_BROADCASE_REQ),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		NotifyGroupLessonMessageReqbody: &ht_muc.NotifyGroupLessonMessageReqBody{
			RoomId:     proto.Uint32(roomId),
			FromId:     proto.Uint32(senderUid),
			RecordObid: recordObid,
			Message:    message,
			MemberList: memberList,
			TimeStamp:  proto.Uint64(timeStamp),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GROUP_LESSON_MESSAGE_BROADCASE_REQ) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetNotifyGroupLessonMessageRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) GetRoomInfo(roomId uint32, reqUid uint32, RoomTimeStamp uint64) (pb *ht_muc.RoomInfoBody, err error) {

	head := &HeadV3{
		From:    reqUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GET_MUC_ROOM_INFO),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		GetRoomInfoReqbody: &ht_muc.GetRoomInfoReqBody{
			OpUid:         proto.Uint32(reqUid),
			RoomId:        proto.Uint32(roomId),
			RoomTimestamp: proto.Uint64(RoomTimeStamp),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GET_MUC_ROOM_INFO) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetGetRoomInfoRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	code := subRspBody.GetStatus().GetCode()

	if code == 0 {
		return subRspBody.GetRoomInfo(), nil
	}
	return nil, fmt.Errorf("error with code=%d", code)
}

func (c *MucApi) UpdateRoomAvatar(roomId uint32, reqUid uint32, headUrl string) (code uint32, err error) {

	head := &HeadV3{
		From:    reqUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_SET_ROOM_AVATAR),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		SetRoomAvatarReqbody: &ht_muc.SetRoomAvatarReqBody{
			ReqUid:     proto.Uint32(reqUid),
			RoomId:     proto.Uint32(roomId),
			RoomAvatar: []byte(headUrl),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_SET_ROOM_AVATAR) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetSetRoomAvatarRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) GetRoomAdminList(roomId, uid uint32) (adminList []uint32, err error) {
	head := &HeadV3{
		From:    uid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GET_ADMIN_LIST_REQ),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		GetAdminListReqbody: &ht_muc.GetAdminListReqBody{
			RoomId: proto.Uint32(roomId),
			Uid:    proto.Uint32(uid),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_muc.MUC_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_GET_ADMIN_LIST_REQ) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetGetAdminListRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	adminList = subRspBody.GetAdminList()
	return adminList, nil
}

func (c *MucApi) BroadCastStatLessonChange(roomId uint32,
	reqUid uint32,
	nickName string,
	chargeId uint64,
	currency string,
	amount uint64,
	dollerAmount uint64,
	symbol string,
	lessonName string,
	lessonObid string,
	msgId string,
	rateMap map[string]uint64) (code uint32, err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_START_LESSON_CHARGE_REQ),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	var rateSlic []*ht_muc.RateType
	for k, v := range rateMap {
		item := &ht_muc.RateType{
			CurrencyType: proto.String(k),
			RealRate:     proto.Uint64(v),
		}
		rateSlic = append(rateSlic, item)
	}

	reqBody := &ht_muc.MucReqBody{
		StartLessonChargeReqbody: &ht_muc.StartLessonChargeReqBody{
			RoomId:   proto.Uint32(roomId),
			ReqUid:   proto.Uint32(reqUid),
			NickName: proto.String(nickName),
			ChargeId: proto.Uint64(chargeId),
			CurrencyInfo: &ht_muc.CurrencyInfoType{
				CurrencyType:        proto.String(currency),
				Amount:              proto.Uint64(amount),
				CorrespondingDollar: proto.Uint64(dollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			LessonName: proto.String(lessonName),
			LessonObid: proto.String(lessonObid),
			MsgId:      proto.String(msgId),
			RateSlic:   rateSlic,
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_START_LESSON_CHARGE_REQ) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetStartLessonChargeRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}

func (c *MucApi) BroadCastStopLessonChange(roomId uint32,
	reqUid uint32,
	nickName string,
	chargeId uint64,
	currency string,
	amount uint64,
	dollerAmount uint64,
	symbol string,
	lessonName string,
	lessonObid string,
	msgId string,
	explictSelf bool) (code uint32, err error) {
	head := &HeadV3{
		From:    reqUid,
		To:      roomId,
		Seq:     0,
		Cmd:     uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_STOP_LESSON_CHARGE_REQ),
		SysType: uint16(ht_muc.MUC_SYS_TYPE_SYS_CGI),
	}

	reqBody := &ht_muc.MucReqBody{
		StopLessonChargeReqbody: &ht_muc.StopLessonChargeReqBody{
			RoomId:   proto.Uint32(roomId),
			ReqUid:   proto.Uint32(reqUid),
			NickName: proto.String(nickName),
			ChargeId: proto.Uint64(chargeId),
			CurrencyInfo: &ht_muc.CurrencyInfoType{
				CurrencyType:        proto.String(currency),
				Amount:              proto.Uint64(amount),
				CorrespondingDollar: proto.Uint64(dollerAmount),
				CurrencySymbol:      proto.String(symbol),
			},
			LessonName:  proto.String(lessonName),
			LessonObid:  proto.String(lessonObid),
			MsgId:       proto.String(msgId),
			ExplictSelf: proto.Bool(explictSelf),
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return code, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return code, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd
	headV3Packet := NewHeadV3Packet(buf)

	conn := c.mucPool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return code, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return code, err
	}

	if rspHead.Cmd != uint16(ht_muc.MUC_CMD_TYPE_GO_CMD_STOP_LESSON_CHARGE_REQ) {
		err = ErrExpectedResp
		return code, err
	}

	rspBody := &ht_muc.MucRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return code, err
	}
	subRspBody := rspBody.GetStopLessonChargeRspbody()
	if subRspBody == nil {
		err = ErrGetRespPacketFaild
		return code, err
	}
	code = subRspBody.GetStatus().GetCode()
	return code, nil
}
