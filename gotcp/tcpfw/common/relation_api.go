package common

import (
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_relation"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type RelationApi struct {
	pool *Pool
}

func newRelationPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewRelationApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *RelationApi {
	pool := newRelationPool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &RelationApi{
		pool: pool,
	}
}

func (c *RelationApi) SendPacket(head *HeadV3, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.pool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *RelationApi) QueryRemarkName(fromId, toId uint32) (remarName []byte, err error) {
	head := &HeadV3{
		From:    fromId,
		To:      toId,
		Seq:     0,
		Cmd:     uint16(ht_relation.CMD_TYPE_CMD_GET_FOLLOWING_DETAIL),
		SysType: uint16(ht_relation.SYS_TYPE_SYS_PUSH),
	}

	reqBody := &ht_relation.ReqBody{
		GetFollowingDetailReqbody: &ht_relation.GetFollowingDetailReqBody{
			FollowingList: []uint32{
				toId,
			},
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.pool.Get()
	defer conn.Close()

	rsp, err := conn.Do(headV3Packet)
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_relation.RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	if rspHead.Cmd != uint16(ht_relation.CMD_TYPE_CMD_GET_FOLLOWING_DETAIL) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_relation.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetGetFollowingDetailRspbody()
	infoList := subRspBody.GetInfoList()
	if infoList == nil {
		return nil, nil
	}

	detailist := infoList.GetDetailList()
	if len(detailist) < 1 {
		return nil, nil
	}

	detailInfo := detailist[0]
	if detailInfo.GetNotExist() != 0 {
		return nil, nil
	}
	remarName = detailInfo.GetRemarkName()
	return remarName, nil
}

func (c *RelationApi) GetFollowerLists(uid uint32) (followerList []uint32, err error) {
	head := &HeadV3{
		From:    uid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_relation.CMD_TYPE_CMD_GET_FOLLOWER_LIST),
		SysType: uint16(ht_relation.SYS_TYPE_SYS_MOMENT),
	}

	reqBody := &ht_relation.ReqBody{
		GetFollowerListReqbody: &ht_relation.GetFollowerListReqBody{
			UidList: []uint32{
				uid,
			},
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.pool.Get()
	rsp, err := conn.Do(headV3Packet)
	if err != nil {
		conn.Close()
		conn := c.pool.Get()
		rsp, err = conn.Do(headV3Packet)
	}
	conn.Close()
	if err != nil {
		return nil, err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_relation.RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	if rspHead.Cmd != uint16(ht_relation.CMD_TYPE_CMD_GET_FOLLOWER_LIST) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_relation.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetGetFollowerListRspbody()
	uidList := subRspBody.GetFollowerList()
	if uidList == nil || len(uidList) == 0 {
		return nil, nil
	}

	if uidList[0].GetUid() != uid {
		err = ErrExpectedResp
		return nil, err
	}

	uidListIterm := uidList[0]
	baseList := uidListIterm.GetBaseList()
	followerList = make([]uint32, len(baseList))
	for i, v := range baseList {
		followerList[i] = v.GetUid()
	}
	return followerList, nil
}

func (c *RelationApi) GetFollowingLists(uid uint32) (followingList []uint32, err error) {
	head := &HeadV3{
		From:    uid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_relation.CMD_TYPE_CMD_GET_FOLLOWING_LIST),
		SysType: uint16(ht_relation.SYS_TYPE_SYS_MOMENT),
	}

	reqBody := &ht_relation.ReqBody{
		GetFollowingListReqbody: &ht_relation.GetFollowingListReqBody{
			UidList: []uint32{
				uid,
			},
			LastTimestamp: []uint32{
				0, //如果时间戳没有变化，服务端本次请求不返回数据.如果想重新获取数据，请填0
			},
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.pool.Get()
	rsp, err := conn.Do(headV3Packet)
	if err != nil {
		conn.Close()
		conn := c.pool.Get()
		rsp, err = conn.Do(headV3Packet)
	}
	conn.Close()
	if err != nil {
		return nil, err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_relation.RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	if rspHead.Cmd != uint16(ht_relation.CMD_TYPE_CMD_GET_FOLLOWING_LIST) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_relation.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetGetFollowingListRspbody()
	uidList := subRspBody.GetFollowingList()
	if uidList == nil || len(uidList) == 0 {
		return nil, nil
	}

	if uidList[0].GetUid() != uid {
		err = ErrExpectedResp
		return nil, err
	}

	uidListIterm := uidList[0]
	baseList := uidListIterm.GetBaseList()
	followingList = make([]uint32, len(baseList))
	for i, v := range baseList {
		followingList[i] = v.GetUid()
	}
	return followingList, nil
}

func (c *RelationApi) GetSpecialFollowerLists(uid uint32) (followerList []uint32, err error) {
	head := &HeadV3{
		From:    uid,
		To:      0,
		Seq:     0,
		Cmd:     uint16(ht_relation.CMD_TYPE_CMD_GET_FOLLOWER_LIST),
		SysType: uint16(ht_relation.SYS_TYPE_SYS_MOMENT),
	}

	reqBody := &ht_relation.ReqBody{
		GetFollowerListReqbody: &ht_relation.GetFollowerListReqBody{
			UidList: []uint32{
				uid,
			},
		},
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV3HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV3MagicBegin
	err = SerialHeadV3ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV3HeadLen:], payLoad)
	buf[head.Len-1] = HTV3MagicEnd

	headV3Packet := NewHeadV3Packet(buf)
	conn := c.pool.Get()
	rsp, err := conn.Do(headV3Packet)
	if err != nil {
		conn.Close()
		conn := c.pool.Get()
		rsp, err = conn.Do(headV3Packet)
	}
	conn.Close()
	if err != nil {
		return nil, err
	}
	rspPacket, ok := rsp.(*HeadV3Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}
	ret := rspHead.Ret
	if ret != uint16(ht_relation.RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	if rspHead.Cmd != uint16(ht_relation.CMD_TYPE_CMD_GET_FOLLOWER_LIST) {
		err = ErrExpectedResp
		return nil, err
	}

	rspBody := &ht_relation.RspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetGetFollowerListRspbody()
	uidList := subRspBody.GetFollowerList()
	if uidList == nil || len(uidList) == 0 {
		return nil, nil
	}

	if uidList[0].GetUid() != uid {
		err = ErrExpectedResp
		return nil, err
	}

	uidListIterm := uidList[0]
	baseList := uidListIterm.GetBaseList()
	for _, v := range baseList {
		if len(v.GetGroupInfo()) > 0 {
			followerList = append(followerList, v.GetUid())
		}
	}
	return followerList, nil
}
