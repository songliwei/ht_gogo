package common

import (
	"time"

	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc_store"
	"github.com/golang/protobuf/proto"
)

// Conn exposes a set of callbacks for the various events that occur on a connection
type MucMsgStoreApi struct {
	mucMsgStorePool *Pool
}

func newMucMsgPool(ip, port string, readTimeout, writeTimeout time.Duration, maxConn int, proto gotcp.Protocol) *Pool {
	return &Pool{
		MaxIdle:     maxConn,
		MaxActive:   maxConn,
		IdleTimeout: 240 * time.Second,
		Dial: func() (Conn, error) {
			c, err := Dial(ip, port, proto, DialReadTimeout(readTimeout), DialWriteTimeout(writeTimeout))
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}

func NewMucMsgStoreApi(ip, port string, readTimeout, writeTimeout time.Duration, protocol gotcp.Protocol, maxConn int) *MucMsgStoreApi {
	pool := newUserCachePool(ip, port, readTimeout, writeTimeout, maxConn, protocol)
	return &MucMsgStoreApi{
		mucMsgStorePool: pool,
	}
}

func (c *MucMsgStoreApi) SendPacket(head *HeadV2, payLoad []uint8) (ret uint16, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		ret = uint16(CRetSendFailed)
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.mucMsgStorePool.Get()
	defer conn.Close()

	rsp, err := conn.Do(packet)
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		ret := uint16(CRetSendFailed)
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		ret := uint16(CRetUnMarshallFailed)
		return ret, err
	}
	ret = rspHead.Ret
	return ret, nil
}

func (c *MucMsgStoreApi) SendAndRecvPacket(head *HeadV2, payLoad []uint8) (packet gotcp.Packet, err error) {
	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet = NewHeadV2Packet(buf)
	conn := c.mucMsgStorePool.Get()
	defer conn.Close()

	packet, err = conn.Do(packet)
	return packet, nil
}

func (c *MucMsgStoreApi) StoreMucMsg(roomId, fromId, cmd uint32, timeStamp uint64, msg []byte) (msgIndex []byte, err error) {
	if roomId == 0 || fromId == 0 || len(msg) == 0 {
		err = ErrInputParamErr
		return nil, err
	}

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_muc_store.CMD_TYPE_CMD_STORE_MUC_MESSAGE_REQ),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := new(ht_muc_store.MucStoreReqBody)
	reqBody.StoreMucMessageReqbody = &ht_muc_store.StoreMucMessageReqBody{
		FromId:  proto.Uint32(fromId),
		RoomId:  proto.Uint32(roomId),
		Cmd:     proto.Uint32(cmd),
		Format:  proto.Uint32(0),
		Content: msg,
		MsgTime: proto.Uint64(timeStamp),
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.mucMsgStorePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.mucMsgStorePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return nil, err
	}
	rspBody := &ht_muc_store.MucStoreRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return nil, err
	}
	subRspBody := rspBody.GetStoreMucMessageRspbody()
	msgIndex = subRspBody.GetMsgIndex()
	return msgIndex, nil
}

func (c *MucMsgStoreApi) StoreMucMsgIndex(fromId uint32, msgIndex []byte, uidList []uint32) (ret uint16, err error) {
	if len(msgIndex) == 0 || len(uidList) == 0 {
		err = ErrInputParamErr
		return ret, err
	}

	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_muc_store.CMD_TYPE_CMD_STORE_MUC_INDEX_REQ),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      fromId,
	}

	reqBody := new(ht_muc_store.MucStoreReqBody)
	reqBody.StoreMessageIndexReqbody = &ht_muc_store.StoreMessageIndexReqBody{
		MsgIndex: msgIndex,
		UidList:  []uint32(uidList),
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return ret, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return ret, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.mucMsgStorePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.mucMsgStorePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return ret, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return ret, err
	}

	ret = rspHead.Ret
	return ret, nil
}

func (c *MucMsgStoreApi) BatchGetMucMsg(opUid uint32, cliSeq uint64) (maxMsgId uint64, hasMore uint32, outMsgList [][]byte, err error) {
	head := &HeadV2{
		Version:  CVerMmedia,
		Cmd:      uint32(ht_muc_store.CMD_TYPE_CMD_BATCH_GET_MUC_MESSAGE_REQ),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      opUid,
	}

	reqBody := new(ht_muc_store.MucStoreReqBody)
	reqBody.BatchGetMucMessageReqbody = &ht_muc_store.BatchGetMucMessageReqBody{
		OpUid:    proto.Uint32(opUid),
		CliSeqId: proto.Uint64(cliSeq),
	}

	payLoad, err := proto.Marshal(reqBody)
	if err != nil {
		return maxMsgId, hasMore, nil, err
	}

	head.Len = uint32(PacketV2HeadLen) + uint32(len(payLoad)) + 1
	buf := make([]byte, head.Len)
	buf[0] = HTV2MagicBegin
	err = SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return maxMsgId, hasMore, nil, err
	}
	copy(buf[PacketV2HeadLen:], payLoad)
	buf[head.Len-1] = HTV2MagicEnd

	packet := NewHeadV2Packet(buf)
	conn := c.mucMsgStorePool.Get()
	rsp, err := conn.Do(packet)
	if err != nil {
		conn.Close()
		conn = c.mucMsgStorePool.Get()
		rsp, err = conn.Do(packet)
	}
	conn.Close()
	rspPacket, ok := rsp.(*HeadV2Packet)
	if !ok {
		err = ErrChangePacketFailed
		return maxMsgId, hasMore, nil, err
	}
	rspHead, err := rspPacket.GetHead()
	if err != nil {
		return maxMsgId, hasMore, nil, err
	}

	ret := rspHead.Ret
	if ret != uint16(ht_muc_store.MUC_STORE_RET_CODE_RET_SUCCESS) {
		err = ErrGetRespPacketFaild
		return maxMsgId, hasMore, nil, err
	}

	if rspHead.Cmd != uint32(ht_muc_store.CMD_TYPE_CMD_BATCH_GET_MUC_MESSAGE_REQ) {
		err = ErrExpectedResp
		return maxMsgId, hasMore, nil, err
	}

	rspBody := &ht_muc_store.MucStoreRspBody{}
	err = proto.Unmarshal(rspPacket.GetBody(), rspBody)
	if err != nil {
		return maxMsgId, hasMore, nil, err
	}
	subRspBody := rspBody.GetBatchGetMucMessageRspbody()
	maxMsgId = subRspBody.GetMaxSeqId()
	hasMore = subRspBody.GetHasMore()
	msgList := subRspBody.GetMsgList()
	for _, v := range msgList {
		outMsgList = append(outMsgList, v.GetContent())
	}
	return maxMsgId, hasMore, outMsgList, nil
}
