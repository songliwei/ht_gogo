// Code generated by protoc-gen-go. DO NOT EDIT.
// source: HT_purchase_new_body.proto

package ht_purchase_new

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// 购买或续费的订单验证状态
type ORDER_TYPE int32

const (
	ORDER_TYPE_E_NORMAL_ORDER  ORDER_TYPE = 0
	ORDER_TYPE_E_REPEAT_ORDER  ORDER_TYPE = 1
	ORDER_TYPE_E_FAKE_ORDER    ORDER_TYPE = 2
	ORDER_TYPE_E_EXPIRED_ORDER ORDER_TYPE = 3
)

var ORDER_TYPE_name = map[int32]string{
	0: "E_NORMAL_ORDER",
	1: "E_REPEAT_ORDER",
	2: "E_FAKE_ORDER",
	3: "E_EXPIRED_ORDER",
}
var ORDER_TYPE_value = map[string]int32{
	"E_NORMAL_ORDER":  0,
	"E_REPEAT_ORDER":  1,
	"E_FAKE_ORDER":    2,
	"E_EXPIRED_ORDER": 3,
}

func (x ORDER_TYPE) Enum() *ORDER_TYPE {
	p := new(ORDER_TYPE)
	*p = x
	return p
}
func (x ORDER_TYPE) String() string {
	return proto.EnumName(ORDER_TYPE_name, int32(x))
}
func (x *ORDER_TYPE) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(ORDER_TYPE_value, data, "ORDER_TYPE")
	if err != nil {
		return err
	}
	*x = ORDER_TYPE(value)
	return nil
}
func (ORDER_TYPE) EnumDescriptor() ([]byte, []int) { return fileDescriptor1, []int{0} }

type PAY_TYPE int32

const (
	PAY_TYPE_PAY_TYPE_APP_STORE   PAY_TYPE = 1
	PAY_TYPE_PAY_TYPE_GOOGLE_PLAY PAY_TYPE = 2
	PAY_TYPE_PAY_TYPE_PAY_PAL     PAY_TYPE = 3
	PAY_TYPE_PAY_TYPE_ALI_PAY     PAY_TYPE = 4
	PAY_TYPE_PAY_TYPE_WECHAT_PAY  PAY_TYPE = 5
)

var PAY_TYPE_name = map[int32]string{
	1: "PAY_TYPE_APP_STORE",
	2: "PAY_TYPE_GOOGLE_PLAY",
	3: "PAY_TYPE_PAY_PAL",
	4: "PAY_TYPE_ALI_PAY",
	5: "PAY_TYPE_WECHAT_PAY",
}
var PAY_TYPE_value = map[string]int32{
	"PAY_TYPE_APP_STORE":   1,
	"PAY_TYPE_GOOGLE_PLAY": 2,
	"PAY_TYPE_PAY_PAL":     3,
	"PAY_TYPE_ALI_PAY":     4,
	"PAY_TYPE_WECHAT_PAY":  5,
}

func (x PAY_TYPE) Enum() *PAY_TYPE {
	p := new(PAY_TYPE)
	*p = x
	return p
}
func (x PAY_TYPE) String() string {
	return proto.EnumName(PAY_TYPE_name, int32(x))
}
func (x *PAY_TYPE) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(PAY_TYPE_value, data, "PAY_TYPE")
	if err != nil {
		return err
	}
	*x = PAY_TYPE(value)
	return nil
}
func (PAY_TYPE) EnumDescriptor() ([]byte, []int) { return fileDescriptor1, []int{1} }

type PurchaseNewHeader struct {
	Code             *uint32 `protobuf:"varint,1,opt,name=code" json:"code,omitempty"`
	Reason           []byte  `protobuf:"bytes,2,opt,name=reason" json:"reason,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *PurchaseNewHeader) Reset()                    { *m = PurchaseNewHeader{} }
func (m *PurchaseNewHeader) String() string            { return proto.CompactTextString(m) }
func (*PurchaseNewHeader) ProtoMessage()               {}
func (*PurchaseNewHeader) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{0} }

func (m *PurchaseNewHeader) GetCode() uint32 {
	if m != nil && m.Code != nil {
		return *m.Code
	}
	return 0
}

func (m *PurchaseNewHeader) GetReason() []byte {
	if m != nil {
		return m.Reason
	}
	return nil
}

type AppStoreOrder struct {
	ProductId        []byte  `protobuf:"bytes,1,opt,name=product_id,json=productId" json:"product_id,omitempty"`
	StoreArea        []byte  `protobuf:"bytes,2,opt,name=store_area,json=storeArea" json:"store_area,omitempty"`
	TransactionId    []byte  `protobuf:"bytes,3,opt,name=transaction_id,json=transactionId" json:"transaction_id,omitempty"`
	PurchaseDateGmt  []byte  `protobuf:"bytes,4,opt,name=purchase_date_gmt,json=purchaseDateGmt" json:"purchase_date_gmt,omitempty"`
	PurchaseDate     []byte  `protobuf:"bytes,5,opt,name=purchase_date,json=purchaseDate" json:"purchase_date,omitempty"`
	ReceiptData      []byte  `protobuf:"bytes,6,opt,name=receipt_data,json=receiptData" json:"receipt_data,omitempty"`
	Currency         []byte  `protobuf:"bytes,7,opt,name=currency" json:"currency,omitempty"`
	PayMoney         []byte  `protobuf:"bytes,8,opt,name=pay_money,json=payMoney" json:"pay_money,omitempty"`
	Jailbroken       *uint32 `protobuf:"varint,9,opt,name=jailbroken" json:"jailbroken,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *AppStoreOrder) Reset()                    { *m = AppStoreOrder{} }
func (m *AppStoreOrder) String() string            { return proto.CompactTextString(m) }
func (*AppStoreOrder) ProtoMessage()               {}
func (*AppStoreOrder) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{1} }

func (m *AppStoreOrder) GetProductId() []byte {
	if m != nil {
		return m.ProductId
	}
	return nil
}

func (m *AppStoreOrder) GetStoreArea() []byte {
	if m != nil {
		return m.StoreArea
	}
	return nil
}

func (m *AppStoreOrder) GetTransactionId() []byte {
	if m != nil {
		return m.TransactionId
	}
	return nil
}

func (m *AppStoreOrder) GetPurchaseDateGmt() []byte {
	if m != nil {
		return m.PurchaseDateGmt
	}
	return nil
}

func (m *AppStoreOrder) GetPurchaseDate() []byte {
	if m != nil {
		return m.PurchaseDate
	}
	return nil
}

func (m *AppStoreOrder) GetReceiptData() []byte {
	if m != nil {
		return m.ReceiptData
	}
	return nil
}

func (m *AppStoreOrder) GetCurrency() []byte {
	if m != nil {
		return m.Currency
	}
	return nil
}

func (m *AppStoreOrder) GetPayMoney() []byte {
	if m != nil {
		return m.PayMoney
	}
	return nil
}

func (m *AppStoreOrder) GetJailbroken() uint32 {
	if m != nil && m.Jailbroken != nil {
		return *m.Jailbroken
	}
	return 0
}

type GoogleOrder struct {
	OrderId          []byte  `protobuf:"bytes,1,opt,name=order_id,json=orderId" json:"order_id,omitempty"`
	ProductId        []byte  `protobuf:"bytes,2,opt,name=product_id,json=productId" json:"product_id,omitempty"`
	PurchaseTime     *uint32 `protobuf:"varint,3,opt,name=purchase_time,json=purchaseTime" json:"purchase_time,omitempty"`
	PurchaseState    *uint32 `protobuf:"varint,4,opt,name=purchase_state,json=purchaseState" json:"purchase_state,omitempty"`
	PurchaseCountry  []byte  `protobuf:"bytes,5,opt,name=purchase_country,json=purchaseCountry" json:"purchase_country,omitempty"`
	Currency         []byte  `protobuf:"bytes,6,opt,name=currency" json:"currency,omitempty"`
	PayMoney         []byte  `protobuf:"bytes,7,opt,name=pay_money,json=payMoney" json:"pay_money,omitempty"`
	PurchaseToken    []byte  `protobuf:"bytes,8,opt,name=purchase_token,json=purchaseToken" json:"purchase_token,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *GoogleOrder) Reset()                    { *m = GoogleOrder{} }
func (m *GoogleOrder) String() string            { return proto.CompactTextString(m) }
func (*GoogleOrder) ProtoMessage()               {}
func (*GoogleOrder) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{2} }

func (m *GoogleOrder) GetOrderId() []byte {
	if m != nil {
		return m.OrderId
	}
	return nil
}

func (m *GoogleOrder) GetProductId() []byte {
	if m != nil {
		return m.ProductId
	}
	return nil
}

func (m *GoogleOrder) GetPurchaseTime() uint32 {
	if m != nil && m.PurchaseTime != nil {
		return *m.PurchaseTime
	}
	return 0
}

func (m *GoogleOrder) GetPurchaseState() uint32 {
	if m != nil && m.PurchaseState != nil {
		return *m.PurchaseState
	}
	return 0
}

func (m *GoogleOrder) GetPurchaseCountry() []byte {
	if m != nil {
		return m.PurchaseCountry
	}
	return nil
}

func (m *GoogleOrder) GetCurrency() []byte {
	if m != nil {
		return m.Currency
	}
	return nil
}

func (m *GoogleOrder) GetPayMoney() []byte {
	if m != nil {
		return m.PayMoney
	}
	return nil
}

func (m *GoogleOrder) GetPurchaseToken() []byte {
	if m != nil {
		return m.PurchaseToken
	}
	return nil
}

type AliOrder struct {
	PurchaseDate     []byte `protobuf:"bytes,1,opt,name=purchase_date,json=purchaseDate" json:"purchase_date,omitempty"`
	TradeNo          []byte `protobuf:"bytes,2,opt,name=trade_no,json=tradeNo" json:"trade_no,omitempty"`
	Partner          []byte `protobuf:"bytes,3,opt,name=partner" json:"partner,omitempty"`
	Seller           []byte `protobuf:"bytes,4,opt,name=seller" json:"seller,omitempty"`
	Subject          []byte `protobuf:"bytes,5,opt,name=subject" json:"subject,omitempty"`
	Success          []byte `protobuf:"bytes,6,opt,name=success" json:"success,omitempty"`
	TotalFee         []byte `protobuf:"bytes,7,opt,name=total_fee,json=totalFee" json:"total_fee,omitempty"`
	XXX_unrecognized []byte `json:"-"`
}

func (m *AliOrder) Reset()                    { *m = AliOrder{} }
func (m *AliOrder) String() string            { return proto.CompactTextString(m) }
func (*AliOrder) ProtoMessage()               {}
func (*AliOrder) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{3} }

func (m *AliOrder) GetPurchaseDate() []byte {
	if m != nil {
		return m.PurchaseDate
	}
	return nil
}

func (m *AliOrder) GetTradeNo() []byte {
	if m != nil {
		return m.TradeNo
	}
	return nil
}

func (m *AliOrder) GetPartner() []byte {
	if m != nil {
		return m.Partner
	}
	return nil
}

func (m *AliOrder) GetSeller() []byte {
	if m != nil {
		return m.Seller
	}
	return nil
}

func (m *AliOrder) GetSubject() []byte {
	if m != nil {
		return m.Subject
	}
	return nil
}

func (m *AliOrder) GetSuccess() []byte {
	if m != nil {
		return m.Success
	}
	return nil
}

func (m *AliOrder) GetTotalFee() []byte {
	if m != nil {
		return m.TotalFee
	}
	return nil
}

type WeCharOrder struct {
	PurchaseDate     []byte `protobuf:"bytes,1,opt,name=purchase_date,json=purchaseDate" json:"purchase_date,omitempty"`
	TradeNo          []byte `protobuf:"bytes,2,opt,name=trade_no,json=tradeNo" json:"trade_no,omitempty"`
	Partner          []byte `protobuf:"bytes,3,opt,name=partner" json:"partner,omitempty"`
	Seller           []byte `protobuf:"bytes,4,opt,name=seller" json:"seller,omitempty"`
	Subject          []byte `protobuf:"bytes,5,opt,name=subject" json:"subject,omitempty"`
	Success          []byte `protobuf:"bytes,6,opt,name=success" json:"success,omitempty"`
	TotalFee         []byte `protobuf:"bytes,7,opt,name=total_fee,json=totalFee" json:"total_fee,omitempty"`
	XXX_unrecognized []byte `json:"-"`
}

func (m *WeCharOrder) Reset()                    { *m = WeCharOrder{} }
func (m *WeCharOrder) String() string            { return proto.CompactTextString(m) }
func (*WeCharOrder) ProtoMessage()               {}
func (*WeCharOrder) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{4} }

func (m *WeCharOrder) GetPurchaseDate() []byte {
	if m != nil {
		return m.PurchaseDate
	}
	return nil
}

func (m *WeCharOrder) GetTradeNo() []byte {
	if m != nil {
		return m.TradeNo
	}
	return nil
}

func (m *WeCharOrder) GetPartner() []byte {
	if m != nil {
		return m.Partner
	}
	return nil
}

func (m *WeCharOrder) GetSeller() []byte {
	if m != nil {
		return m.Seller
	}
	return nil
}

func (m *WeCharOrder) GetSubject() []byte {
	if m != nil {
		return m.Subject
	}
	return nil
}

func (m *WeCharOrder) GetSuccess() []byte {
	if m != nil {
		return m.Success
	}
	return nil
}

func (m *WeCharOrder) GetTotalFee() []byte {
	if m != nil {
		return m.TotalFee
	}
	return nil
}

type PurchaseHistory struct {
	OrderId          []byte  `protobuf:"bytes,1,opt,name=order_id,json=orderId" json:"order_id,omitempty"`
	ItemCode         *uint32 `protobuf:"varint,2,opt,name=item_code,json=itemCode" json:"item_code,omitempty"`
	ProductId        []byte  `protobuf:"bytes,3,opt,name=product_id,json=productId" json:"product_id,omitempty"`
	GiftDays         *uint32 `protobuf:"varint,4,opt,name=gift_days,json=giftDays" json:"gift_days,omitempty"`
	UserId           *uint32 `protobuf:"varint,5,opt,name=user_id,json=userId" json:"user_id,omitempty"`
	ToId             *uint32 `protobuf:"varint,6,opt,name=to_id,json=toId" json:"to_id,omitempty"`
	Currency         []byte  `protobuf:"bytes,7,opt,name=currency" json:"currency,omitempty"`
	PayMoney         []byte  `protobuf:"bytes,8,opt,name=pay_money,json=payMoney" json:"pay_money,omitempty"`
	PayType          *uint32 `protobuf:"varint,9,opt,name=pay_type,json=payType" json:"pay_type,omitempty"`
	BuyTime          []byte  `protobuf:"bytes,10,opt,name=buy_time,json=buyTime" json:"buy_time,omitempty"`
	UtilBefore       *uint64 `protobuf:"varint,11,opt,name=util_before,json=utilBefore" json:"util_before,omitempty"`
	UtilAfter        *uint64 `protobuf:"varint,12,opt,name=util_after,json=utilAfter" json:"util_after,omitempty"`
	ClientOrSrv      *uint32 `protobuf:"varint,13,opt,name=client_or_srv,json=clientOrSrv" json:"client_or_srv,omitempty"`
	RetryTime        *uint32 `protobuf:"varint,14,opt,name=retry_time,json=retryTime" json:"retry_time,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *PurchaseHistory) Reset()                    { *m = PurchaseHistory{} }
func (m *PurchaseHistory) String() string            { return proto.CompactTextString(m) }
func (*PurchaseHistory) ProtoMessage()               {}
func (*PurchaseHistory) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{5} }

func (m *PurchaseHistory) GetOrderId() []byte {
	if m != nil {
		return m.OrderId
	}
	return nil
}

func (m *PurchaseHistory) GetItemCode() uint32 {
	if m != nil && m.ItemCode != nil {
		return *m.ItemCode
	}
	return 0
}

func (m *PurchaseHistory) GetProductId() []byte {
	if m != nil {
		return m.ProductId
	}
	return nil
}

func (m *PurchaseHistory) GetGiftDays() uint32 {
	if m != nil && m.GiftDays != nil {
		return *m.GiftDays
	}
	return 0
}

func (m *PurchaseHistory) GetUserId() uint32 {
	if m != nil && m.UserId != nil {
		return *m.UserId
	}
	return 0
}

func (m *PurchaseHistory) GetToId() uint32 {
	if m != nil && m.ToId != nil {
		return *m.ToId
	}
	return 0
}

func (m *PurchaseHistory) GetCurrency() []byte {
	if m != nil {
		return m.Currency
	}
	return nil
}

func (m *PurchaseHistory) GetPayMoney() []byte {
	if m != nil {
		return m.PayMoney
	}
	return nil
}

func (m *PurchaseHistory) GetPayType() uint32 {
	if m != nil && m.PayType != nil {
		return *m.PayType
	}
	return 0
}

func (m *PurchaseHistory) GetBuyTime() []byte {
	if m != nil {
		return m.BuyTime
	}
	return nil
}

func (m *PurchaseHistory) GetUtilBefore() uint64 {
	if m != nil && m.UtilBefore != nil {
		return *m.UtilBefore
	}
	return 0
}

func (m *PurchaseHistory) GetUtilAfter() uint64 {
	if m != nil && m.UtilAfter != nil {
		return *m.UtilAfter
	}
	return 0
}

func (m *PurchaseHistory) GetClientOrSrv() uint32 {
	if m != nil && m.ClientOrSrv != nil {
		return *m.ClientOrSrv
	}
	return 0
}

func (m *PurchaseHistory) GetRetryTime() uint32 {
	if m != nil && m.RetryTime != nil {
		return *m.RetryTime
	}
	return 0
}

// clinet--->chat_record_logic--->chat_record_dbd
type CommitPurchaseOrderNewReqBody struct {
	PayType          *PAY_TYPE      `protobuf:"varint,1,opt,name=pay_type,json=payType,enum=ht.purchase_new.PAY_TYPE" json:"pay_type,omitempty"`
	UserId           *uint32        `protobuf:"varint,2,opt,name=user_id,json=userId" json:"user_id,omitempty"`
	ToId             *uint32        `protobuf:"varint,3,opt,name=to_id,json=toId" json:"to_id,omitempty"`
	ItemCode         *uint32        `protobuf:"varint,4,opt,name=item_code,json=itemCode" json:"item_code,omitempty"`
	AppleOrder       *AppStoreOrder `protobuf:"bytes,5,opt,name=apple_order,json=appleOrder" json:"apple_order,omitempty"`
	GoogleOrder      *GoogleOrder   `protobuf:"bytes,6,opt,name=google_order,json=googleOrder" json:"google_order,omitempty"`
	AliOrder         *AliOrder      `protobuf:"bytes,7,opt,name=ali_order,json=aliOrder" json:"ali_order,omitempty"`
	WechatOrder      *WeCharOrder   `protobuf:"bytes,8,opt,name=wechat_order,json=wechatOrder" json:"wechat_order,omitempty"`
	XXX_unrecognized []byte         `json:"-"`
}

func (m *CommitPurchaseOrderNewReqBody) Reset()                    { *m = CommitPurchaseOrderNewReqBody{} }
func (m *CommitPurchaseOrderNewReqBody) String() string            { return proto.CompactTextString(m) }
func (*CommitPurchaseOrderNewReqBody) ProtoMessage()               {}
func (*CommitPurchaseOrderNewReqBody) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{6} }

func (m *CommitPurchaseOrderNewReqBody) GetPayType() PAY_TYPE {
	if m != nil && m.PayType != nil {
		return *m.PayType
	}
	return PAY_TYPE_PAY_TYPE_APP_STORE
}

func (m *CommitPurchaseOrderNewReqBody) GetUserId() uint32 {
	if m != nil && m.UserId != nil {
		return *m.UserId
	}
	return 0
}

func (m *CommitPurchaseOrderNewReqBody) GetToId() uint32 {
	if m != nil && m.ToId != nil {
		return *m.ToId
	}
	return 0
}

func (m *CommitPurchaseOrderNewReqBody) GetItemCode() uint32 {
	if m != nil && m.ItemCode != nil {
		return *m.ItemCode
	}
	return 0
}

func (m *CommitPurchaseOrderNewReqBody) GetAppleOrder() *AppStoreOrder {
	if m != nil {
		return m.AppleOrder
	}
	return nil
}

func (m *CommitPurchaseOrderNewReqBody) GetGoogleOrder() *GoogleOrder {
	if m != nil {
		return m.GoogleOrder
	}
	return nil
}

func (m *CommitPurchaseOrderNewReqBody) GetAliOrder() *AliOrder {
	if m != nil {
		return m.AliOrder
	}
	return nil
}

func (m *CommitPurchaseOrderNewReqBody) GetWechatOrder() *WeCharOrder {
	if m != nil {
		return m.WechatOrder
	}
	return nil
}

type CommitPurchaseOrderNewRspBody struct {
	Status           *PurchaseNewHeader `protobuf:"bytes,1,opt,name=status" json:"status,omitempty"`
	OrderType        *ORDER_TYPE        `protobuf:"varint,2,opt,name=order_type,json=orderType,enum=ht.purchase_new.ORDER_TYPE" json:"order_type,omitempty"`
	ExpireTs         *uint64            `protobuf:"varint,3,opt,name=expire_ts,json=expireTs" json:"expire_ts,omitempty"`
	XXX_unrecognized []byte             `json:"-"`
}

func (m *CommitPurchaseOrderNewRspBody) Reset()                    { *m = CommitPurchaseOrderNewRspBody{} }
func (m *CommitPurchaseOrderNewRspBody) String() string            { return proto.CompactTextString(m) }
func (*CommitPurchaseOrderNewRspBody) ProtoMessage()               {}
func (*CommitPurchaseOrderNewRspBody) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{7} }

func (m *CommitPurchaseOrderNewRspBody) GetStatus() *PurchaseNewHeader {
	if m != nil {
		return m.Status
	}
	return nil
}

func (m *CommitPurchaseOrderNewRspBody) GetOrderType() ORDER_TYPE {
	if m != nil && m.OrderType != nil {
		return *m.OrderType
	}
	return ORDER_TYPE_E_NORMAL_ORDER
}

func (m *CommitPurchaseOrderNewRspBody) GetExpireTs() uint64 {
	if m != nil && m.ExpireTs != nil {
		return *m.ExpireTs
	}
	return 0
}

func init() {
	proto.RegisterType((*PurchaseNewHeader)(nil), "ht.purchase_new.PurchaseNewHeader")
	proto.RegisterType((*AppStoreOrder)(nil), "ht.purchase_new.AppStoreOrder")
	proto.RegisterType((*GoogleOrder)(nil), "ht.purchase_new.GoogleOrder")
	proto.RegisterType((*AliOrder)(nil), "ht.purchase_new.AliOrder")
	proto.RegisterType((*WeCharOrder)(nil), "ht.purchase_new.WeCharOrder")
	proto.RegisterType((*PurchaseHistory)(nil), "ht.purchase_new.PurchaseHistory")
	proto.RegisterType((*CommitPurchaseOrderNewReqBody)(nil), "ht.purchase_new.CommitPurchaseOrderNewReqBody")
	proto.RegisterType((*CommitPurchaseOrderNewRspBody)(nil), "ht.purchase_new.CommitPurchaseOrderNewRspBody")
	proto.RegisterEnum("ht.purchase_new.ORDER_TYPE", ORDER_TYPE_name, ORDER_TYPE_value)
	proto.RegisterEnum("ht.purchase_new.PAY_TYPE", PAY_TYPE_name, PAY_TYPE_value)
}

func init() { proto.RegisterFile("HT_purchase_new_body.proto", fileDescriptor1) }

var fileDescriptor1 = []byte{
	// 1020 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xdc, 0x55, 0xcd, 0x6e, 0xdb, 0x46,
	0x10, 0xae, 0x7e, 0x2c, 0x91, 0x43, 0xc9, 0x56, 0xd6, 0x41, 0x42, 0xc7, 0x4d, 0xea, 0xaa, 0x28,
	0xe0, 0xfa, 0xe0, 0x83, 0x51, 0xf4, 0x90, 0x8b, 0xc1, 0xd8, 0x8c, 0x2d, 0xd4, 0xb1, 0x04, 0x5a,
	0x40, 0x6a, 0xf4, 0xb0, 0x58, 0x91, 0x6b, 0x9b, 0xa9, 0xc4, 0x65, 0x97, 0xab, 0xb8, 0x3c, 0xf6,
	0x39, 0x0a, 0xf4, 0xd6, 0x37, 0xe8, 0x0b, 0xf4, 0x01, 0x0a, 0xf4, 0x91, 0x8a, 0x1d, 0x2e, 0x65,
	0x4a, 0x4a, 0x72, 0xe8, 0xb1, 0xb7, 0x9d, 0x6f, 0x66, 0x96, 0x3b, 0xdf, 0x37, 0x33, 0x84, 0x67,
	0xe7, 0x63, 0x9a, 0xce, 0x65, 0x78, 0xc7, 0x32, 0x4e, 0x13, 0x7e, 0x4f, 0x27, 0x22, 0xca, 0x0f,
	0x53, 0x29, 0x94, 0x20, 0x5b, 0x77, 0xea, 0xb0, 0xea, 0xeb, 0x1f, 0xc3, 0xa3, 0x91, 0xb1, 0x2f,
	0xf9, 0xfd, 0x39, 0x67, 0x11, 0x97, 0x84, 0x40, 0x33, 0x14, 0x11, 0x77, 0x6b, 0x7b, 0xb5, 0xfd,
	0x6e, 0x80, 0x67, 0xf2, 0x04, 0x5a, 0x92, 0xb3, 0x4c, 0x24, 0x6e, 0x7d, 0xaf, 0xb6, 0xdf, 0x09,
	0x8c, 0xd5, 0xff, 0xab, 0x0e, 0x5d, 0x2f, 0x4d, 0xaf, 0x94, 0x90, 0x7c, 0x28, 0x75, 0xf6, 0x73,
	0x80, 0x54, 0x8a, 0x68, 0x1e, 0x2a, 0x1a, 0x47, 0x78, 0x47, 0x27, 0xb0, 0x0d, 0x32, 0x88, 0xb4,
	0x3b, 0xd3, 0xc1, 0x94, 0x49, 0xce, 0xcc, 0x65, 0x36, 0x22, 0x9e, 0xe4, 0x8c, 0x7c, 0x0d, 0x9b,
	0x4a, 0xb2, 0x24, 0x63, 0xa1, 0x8a, 0x45, 0xa2, 0x6f, 0x68, 0x60, 0x48, 0xb7, 0x82, 0x0e, 0x22,
	0x72, 0x00, 0x8f, 0x16, 0x75, 0x44, 0x4c, 0x71, 0x7a, 0x3b, 0x53, 0x6e, 0x13, 0x23, 0xb7, 0x4a,
	0xc7, 0x29, 0x53, 0xfc, 0x6c, 0xa6, 0xc8, 0x57, 0xd0, 0x5d, 0x8a, 0x75, 0x37, 0x30, 0xae, 0x53,
	0x8d, 0x23, 0x5f, 0x42, 0x47, 0xf2, 0x90, 0xc7, 0xa9, 0xd2, 0x31, 0xcc, 0x6d, 0x61, 0x8c, 0x63,
	0xb0, 0x53, 0xa6, 0x18, 0x79, 0x06, 0x56, 0x38, 0x97, 0x92, 0x27, 0x61, 0xee, 0xb6, 0xd1, 0xbd,
	0xb0, 0xc9, 0x2e, 0xd8, 0x29, 0xcb, 0xe9, 0x4c, 0x24, 0x3c, 0x77, 0xad, 0xc2, 0x99, 0xb2, 0xfc,
	0x8d, 0xb6, 0xc9, 0x0b, 0x80, 0x77, 0x2c, 0x9e, 0x4e, 0xa4, 0xf8, 0x89, 0x27, 0xae, 0x8d, 0xac,
	0x56, 0x90, 0xfe, 0xef, 0x75, 0x70, 0xce, 0x84, 0xb8, 0x9d, 0x1a, 0x06, 0x77, 0xc0, 0x12, 0xfa,
	0xf0, 0xc0, 0x5f, 0x1b, 0xed, 0x82, 0xbd, 0x0a, 0xb9, 0xf5, 0x55, 0x72, 0xab, 0xa5, 0xaa, 0x78,
	0xc6, 0x91, 0xbc, 0xee, 0x43, 0xa9, 0xe3, 0x78, 0xc6, 0x35, 0xc5, 0x8b, 0xa0, 0x4c, 0x69, 0x42,
	0x9a, 0x18, 0xb5, 0x48, 0xbd, 0xd2, 0x20, 0xf9, 0x06, 0x7a, 0x8b, 0xb0, 0x50, 0xcc, 0x13, 0x25,
	0x73, 0xc3, 0xdc, 0x82, 0xe1, 0x93, 0x02, 0x5e, 0x62, 0xa6, 0xf5, 0x29, 0x66, 0xda, 0x2b, 0xcc,
	0x54, 0x9f, 0xa2, 0x90, 0x9d, 0x82, 0xbb, 0xc5, 0x53, 0xc6, 0x48, 0xd0, 0xdf, 0x35, 0xb0, 0xbc,
	0x69, 0x5c, 0xb0, 0xb3, 0x26, 0x67, 0xed, 0x03, 0x72, 0xee, 0x80, 0xa5, 0x24, 0x8b, 0x38, 0x4d,
	0x84, 0x61, 0xa9, 0x8d, 0xf6, 0xa5, 0x20, 0x2e, 0xb4, 0x53, 0x26, 0x55, 0xc2, 0xa5, 0x69, 0xad,
	0xd2, 0xd4, 0x3d, 0x9e, 0xf1, 0xe9, 0x94, 0x4b, 0xd3, 0x49, 0xc6, 0xd2, 0x19, 0xd9, 0x7c, 0xf2,
	0x8e, 0x87, 0xca, 0x10, 0x50, 0x9a, 0x85, 0x27, 0x0c, 0x79, 0x96, 0x99, 0xba, 0x4b, 0x53, 0x97,
	0xad, 0x84, 0x62, 0x53, 0x7a, 0xc3, 0x79, 0x59, 0x36, 0x02, 0xaf, 0x39, 0xef, 0xff, 0x53, 0x03,
	0xe7, 0x2d, 0x3f, 0xb9, 0x63, 0xf2, 0x7f, 0x53, 0xd2, 0x1f, 0x0d, 0xd8, 0x2a, 0x37, 0xc9, 0x79,
	0xac, 0xe7, 0x39, 0xff, 0x54, 0x1f, 0xef, 0x82, 0x1d, 0x2b, 0x3e, 0xa3, 0xb8, 0x67, 0xea, 0xd8,
	0x7e, 0x96, 0x06, 0x4e, 0xf4, 0xae, 0x59, 0x6e, 0xf2, 0xc6, 0x6a, 0x93, 0xef, 0x82, 0x7d, 0x1b,
	0xdf, 0xe8, 0x39, 0xcd, 0x33, 0xd3, 0xba, 0x96, 0x06, 0x4e, 0x59, 0x9e, 0x91, 0xa7, 0xd0, 0x9e,
	0x67, 0xc5, 0x27, 0x37, 0xd0, 0xd5, 0xd2, 0xe6, 0x20, 0x22, 0xdb, 0xb0, 0xa1, 0x84, 0x86, 0x5b,
	0xc5, 0x56, 0x53, 0x62, 0x10, 0xfd, 0xf7, 0x91, 0xde, 0x01, 0x7d, 0xa6, 0x2a, 0x4f, 0xb9, 0x19,
	0xe8, 0x76, 0xca, 0xf2, 0x71, 0x9e, 0xa2, 0x4e, 0x93, 0x79, 0x5e, 0x8c, 0x1f, 0x14, 0x55, 0x4f,
	0xe6, 0x39, 0x4e, 0xde, 0x17, 0xe0, 0xcc, 0x55, 0x3c, 0xa5, 0x13, 0x7e, 0x23, 0x24, 0x77, 0x9d,
	0xbd, 0xda, 0x7e, 0x33, 0x00, 0x0d, 0xbd, 0x42, 0x44, 0x57, 0x8e, 0x01, 0xec, 0x46, 0x71, 0xe9,
	0x76, 0xd0, 0x6f, 0x6b, 0xc4, 0xd3, 0x00, 0xe9, 0x43, 0x37, 0x9c, 0xc6, 0x3c, 0x51, 0x54, 0x48,
	0x9a, 0xc9, 0xf7, 0x6e, 0x17, 0x3f, 0xed, 0x14, 0xe0, 0x50, 0x5e, 0xc9, 0xf7, 0xfa, 0x0a, 0xc9,
	0x95, 0x34, 0x0f, 0xd8, 0xc4, 0x00, 0x1b, 0x11, 0xfd, 0x84, 0xfe, 0x6f, 0x0d, 0x78, 0x7e, 0x22,
	0x66, 0xb3, 0x58, 0x95, 0x6a, 0x61, 0x0b, 0x5e, 0xf2, 0xfb, 0x80, 0xff, 0xfc, 0x4a, 0x44, 0x39,
	0xf9, 0xb6, 0x52, 0x9a, 0x56, 0x6d, 0xf3, 0x68, 0xe7, 0x70, 0xe5, 0xb7, 0x71, 0x38, 0xf2, 0xae,
	0xe9, 0xf8, 0x7a, 0xe4, 0x3f, 0x54, 0x5d, 0xe1, 0xbd, 0xfe, 0x61, 0xde, 0x1b, 0x15, 0xde, 0x97,
	0xe4, 0x6f, 0xae, 0xc8, 0x7f, 0x0c, 0x0e, 0x4b, 0xd3, 0x29, 0xa7, 0xd8, 0x2c, 0x28, 0xa3, 0x73,
	0xf4, 0x62, 0xed, 0x0d, 0x4b, 0x7f, 0x9d, 0x00, 0x30, 0xa5, 0x18, 0xa7, 0x63, 0xe8, 0xdc, 0xe2,
	0x3a, 0x35, 0x37, 0xb4, 0xf0, 0x86, 0xcf, 0xd7, 0x6e, 0xa8, 0xec, 0xdc, 0xc0, 0xb9, 0xad, 0x2c,
	0xe0, 0xef, 0xc0, 0x66, 0xd3, 0xd8, 0x64, 0xb7, 0x31, 0x7b, 0x9d, 0x83, 0x72, 0x21, 0x05, 0x16,
	0x2b, 0x57, 0xd3, 0x31, 0x74, 0xee, 0x79, 0x78, 0xc7, 0x94, 0x49, 0xb5, 0x3e, 0xf2, 0xe1, 0xca,
	0xec, 0x07, 0x4e, 0x91, 0x81, 0x46, 0xff, 0xcf, 0xda, 0x47, 0xd5, 0xc9, 0x52, 0x54, 0xe7, 0x25,
	0xb4, 0xf4, 0xce, 0x9e, 0x67, 0xa8, 0x8d, 0x73, 0xd4, 0x5f, 0xd7, 0x66, 0xf5, 0x7f, 0x1e, 0x98,
	0x0c, 0xf2, 0x12, 0xa0, 0x98, 0x47, 0xd4, 0xb6, 0x8e, 0xda, 0xee, 0xae, 0xe5, 0x0f, 0x83, 0x53,
	0x3f, 0x28, 0xd4, 0xb5, 0x31, 0x1c, 0xf5, 0xdd, 0x05, 0x9b, 0xff, 0x92, 0xc6, 0x92, 0x53, 0x95,
	0xa1, 0x94, 0xcd, 0xc0, 0x2a, 0x80, 0x71, 0x76, 0xf0, 0x23, 0xc0, 0x43, 0x16, 0x21, 0xb0, 0xe9,
	0xd3, 0xcb, 0x61, 0xf0, 0xc6, 0xbb, 0xa0, 0x08, 0xf7, 0x3e, 0x2b, 0xb0, 0xc0, 0x1f, 0xf9, 0xde,
	0xd8, 0x60, 0x35, 0xd2, 0x83, 0x8e, 0x4f, 0x5f, 0x7b, 0xdf, 0xfb, 0x06, 0xa9, 0x93, 0x6d, 0xd8,
	0xf2, 0xa9, 0xff, 0xc3, 0x68, 0x10, 0xf8, 0xa7, 0x06, 0x6c, 0x1c, 0xfc, 0x5a, 0x03, 0xab, 0xec,
	0x37, 0xf2, 0x04, 0x48, 0x79, 0xa6, 0xde, 0x68, 0x44, 0xaf, 0xc6, 0xc3, 0xc0, 0xef, 0xd5, 0x88,
	0x0b, 0x8f, 0x17, 0xf8, 0xd9, 0x70, 0x78, 0x76, 0xe1, 0xd3, 0xd1, 0x85, 0x77, 0xdd, 0xab, 0x93,
	0xc7, 0xd0, 0x5b, 0x78, 0xf4, 0x61, 0xe4, 0x5d, 0xf4, 0x1a, 0x4b, 0xa8, 0x77, 0x31, 0xd0, 0x9e,
	0x5e, 0x93, 0x3c, 0x85, 0xed, 0x05, 0xfa, 0xd6, 0x3f, 0x39, 0xf7, 0xc6, 0xe8, 0xd8, 0xf8, 0x37,
	0x00, 0x00, 0xff, 0xff, 0x5e, 0x5e, 0x52, 0xde, 0x54, 0x09, 0x00, 0x00,
}
