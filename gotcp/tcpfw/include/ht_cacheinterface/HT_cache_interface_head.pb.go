// Code generated by protoc-gen-go. DO NOT EDIT.
// source: HT_cache_interface_head.proto

/*
Package ht_cache_interface is a generated protocol buffer package.

It is generated from these files:
	HT_cache_interface_head.proto
	HT_cache_interface_body.proto

It has these top-level messages:
	CacheInterfaceReqBody
	CacheInterfaceRspBody
	CacheInterfaceHeader
	UserBaseCache
	AccountCache
	GetUserCacheReqBody
	UserInfoCacheIterm
	GetUserCacheRspBody
	ReloadUserCacheReqBody
	ReloadUserCacheRspBody
	UpdateUserNameReqBody
	UpdateUserNameRspBody
	UpdateUserSummerTimeReqBody
	UpdateUserSummerTimeRspBody
	UpdateUserTimeZoneReqBody
	UpdateUserTimeZoneRspBody
	GetUserNickNameReqBody
	GetUserNickNameRspBody
	UpdateUserPrivateReqBody
	UpdateUserPrivateRspBody
	UpdateLearnMultiLangReqBody
	UpdateLearnMultiLangRspBody
	BaseUserInfo
	LanguageInfo
	UpdateUserLangSettingReqBody
	UpdateUserLangSettingRspBody
	UpdateUserProfileReqBody
	UpdateUserProfileRspBody
	UpdateUserPropertyReqBody
	UpdateUserPropertyRspBody
	UpdateUserPassWdReqBody
	UpdateUserPassWdRspBody
	UpdateUserEmailReqBody
	UpdateUserEmailRspBody
	UnregisterAccountReqBody
	UnregisterAccountRspBody
	UpdateUserLessonVerReqBody
	UpdateUserLessonVerRspBody
	UpdateUserWalletVerReqBody
	UpdateUserWalletVerRspBody
*/
package ht_cache_interface

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

// 根据不同的cmd_type来使用不同的body
type CMD_TYPE int32

const (
	CMD_TYPE_CMD_GET_USER_CACHE             CMD_TYPE = 1
	CMD_TYPE_CMD_RELOAD_USER_CACHE          CMD_TYPE = 2
	CMD_TYPE_CMD_UPDATE_USER_NAME           CMD_TYPE = 3
	CMD_TYPE_CMD_UPDATE_SUMMER_TIME_SETTING CMD_TYPE = 4
	CMD_TYPE_CMD_UPDATE_USER_TIMEZONE       CMD_TYPE = 5
	CMD_TYPE_CMD_GET_USER_NICK_NAME         CMD_TYPE = 6
	CMD_TYPE_CMD_UPDATE_USER_PRIVATE        CMD_TYPE = 7
	CMD_TYPE_CMD_UPDATE_LEARN_MULTI_LANG    CMD_TYPE = 8
	CMD_TYPE_CMD_UPDATE_USER_LANG_SETTING   CMD_TYPE = 9
	CMD_TYPE_CMD_UPDATE_USER_PROFILE        CMD_TYPE = 10
	CMD_TYPE_CMD_UPDATE_USER_PROPERTY       CMD_TYPE = 11
	CMD_TYPE_CMD_UPDATE_USER_PASSWD         CMD_TYPE = 12
	CMD_TYPE_CMD_UPDATE_USER_EMAIL          CMD_TYPE = 13
	CMD_TYPE_CMD_UNREGISTER_USER_ACCOUNT    CMD_TYPE = 14
	CMD_TYPE_CMD_UPDATE_USER_LESSON_VER     CMD_TYPE = 15
	CMD_TYPE_CMD_UPDATE_USER_WALLET_VER     CMD_TYPE = 16
)

var CMD_TYPE_name = map[int32]string{
	1:  "CMD_GET_USER_CACHE",
	2:  "CMD_RELOAD_USER_CACHE",
	3:  "CMD_UPDATE_USER_NAME",
	4:  "CMD_UPDATE_SUMMER_TIME_SETTING",
	5:  "CMD_UPDATE_USER_TIMEZONE",
	6:  "CMD_GET_USER_NICK_NAME",
	7:  "CMD_UPDATE_USER_PRIVATE",
	8:  "CMD_UPDATE_LEARN_MULTI_LANG",
	9:  "CMD_UPDATE_USER_LANG_SETTING",
	10: "CMD_UPDATE_USER_PROFILE",
	11: "CMD_UPDATE_USER_PROPERTY",
	12: "CMD_UPDATE_USER_PASSWD",
	13: "CMD_UPDATE_USER_EMAIL",
	14: "CMD_UNREGISTER_USER_ACCOUNT",
	15: "CMD_UPDATE_USER_LESSON_VER",
	16: "CMD_UPDATE_USER_WALLET_VER",
}
var CMD_TYPE_value = map[string]int32{
	"CMD_GET_USER_CACHE":             1,
	"CMD_RELOAD_USER_CACHE":          2,
	"CMD_UPDATE_USER_NAME":           3,
	"CMD_UPDATE_SUMMER_TIME_SETTING": 4,
	"CMD_UPDATE_USER_TIMEZONE":       5,
	"CMD_GET_USER_NICK_NAME":         6,
	"CMD_UPDATE_USER_PRIVATE":        7,
	"CMD_UPDATE_LEARN_MULTI_LANG":    8,
	"CMD_UPDATE_USER_LANG_SETTING":   9,
	"CMD_UPDATE_USER_PROFILE":        10,
	"CMD_UPDATE_USER_PROPERTY":       11,
	"CMD_UPDATE_USER_PASSWD":         12,
	"CMD_UPDATE_USER_EMAIL":          13,
	"CMD_UNREGISTER_USER_ACCOUNT":    14,
	"CMD_UPDATE_USER_LESSON_VER":     15,
	"CMD_UPDATE_USER_WALLET_VER":     16,
}

func (x CMD_TYPE) Enum() *CMD_TYPE {
	p := new(CMD_TYPE)
	*p = x
	return p
}
func (x CMD_TYPE) String() string {
	return proto.EnumName(CMD_TYPE_name, int32(x))
}
func (x *CMD_TYPE) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(CMD_TYPE_value, data, "CMD_TYPE")
	if err != nil {
		return err
	}
	*x = CMD_TYPE(value)
	return nil
}
func (CMD_TYPE) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

type CACHE_RET_CODE int32

const (
	CACHE_RET_CODE_CACHE_RET_SUCCESS          CACHE_RET_CODE = 0
	CACHE_RET_CODE_CACHE_RET_REPEAT_ADD       CACHE_RET_CODE = 100
	CACHE_RET_CODE_CACHE_RET_NOT_CHANGE       CACHE_RET_CODE = 101
	CACHE_RET_CODE_CACHE_RET_NO_MORE          CACHE_RET_CODE = 102
	CACHE_RET_CODE_CACHE_RET_INVALID_PARAM    CACHE_RET_CODE = 103
	CACHE_RET_CODE_CACHE_RET_DUPLICATE_RECORD CACHE_RET_CODE = 104
	CACHE_RET_CODE_CACHE_RET_PB_ERR           CACHE_RET_CODE = 500
	CACHE_RET_CODE_CACHE_RET_INTERNAL_ERR     CACHE_RET_CODE = 501
	CACHE_RET_CODE_CACHE_RET_SESS_TIMEOUT_ERR CACHE_RET_CODE = 502
	CACHE_RET_CODE_CACHE_RET_INPUT_PARAM_ERR  CACHE_RET_CODE = 503
	CACHE_RET_CODE_CACHE_RET_DB_ERR           CACHE_RET_CODE = 504
	CACHE_RET_CODE_CACHE_RET_NOT_EXIST        CACHE_RET_CODE = 505
	CACHE_RET_CODE_CACHE_RET_REDIS_ERR        CACHE_RET_CODE = 506
)

var CACHE_RET_CODE_name = map[int32]string{
	0:   "CACHE_RET_SUCCESS",
	100: "CACHE_RET_REPEAT_ADD",
	101: "CACHE_RET_NOT_CHANGE",
	102: "CACHE_RET_NO_MORE",
	103: "CACHE_RET_INVALID_PARAM",
	104: "CACHE_RET_DUPLICATE_RECORD",
	500: "CACHE_RET_PB_ERR",
	501: "CACHE_RET_INTERNAL_ERR",
	502: "CACHE_RET_SESS_TIMEOUT_ERR",
	503: "CACHE_RET_INPUT_PARAM_ERR",
	504: "CACHE_RET_DB_ERR",
	505: "CACHE_RET_NOT_EXIST",
	506: "CACHE_RET_REDIS_ERR",
}
var CACHE_RET_CODE_value = map[string]int32{
	"CACHE_RET_SUCCESS":          0,
	"CACHE_RET_REPEAT_ADD":       100,
	"CACHE_RET_NOT_CHANGE":       101,
	"CACHE_RET_NO_MORE":          102,
	"CACHE_RET_INVALID_PARAM":    103,
	"CACHE_RET_DUPLICATE_RECORD": 104,
	"CACHE_RET_PB_ERR":           500,
	"CACHE_RET_INTERNAL_ERR":     501,
	"CACHE_RET_SESS_TIMEOUT_ERR": 502,
	"CACHE_RET_INPUT_PARAM_ERR":  503,
	"CACHE_RET_DB_ERR":           504,
	"CACHE_RET_NOT_EXIST":        505,
	"CACHE_RET_REDIS_ERR":        506,
}

func (x CACHE_RET_CODE) Enum() *CACHE_RET_CODE {
	p := new(CACHE_RET_CODE)
	*p = x
	return p
}
func (x CACHE_RET_CODE) String() string {
	return proto.EnumName(CACHE_RET_CODE_name, int32(x))
}
func (x *CACHE_RET_CODE) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(CACHE_RET_CODE_value, data, "CACHE_RET_CODE")
	if err != nil {
		return err
	}
	*x = CACHE_RET_CODE(value)
	return nil
}
func (CACHE_RET_CODE) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

type CACHE_SYS_TYPE int32

const (
	CACHE_SYS_TYPE_CACHE_SYS_LUA       CACHE_SYS_TYPE = 1
	CACHE_SYS_TYPE_CACHE_SYS_TYPE_TOOL CACHE_SYS_TYPE = 20
)

var CACHE_SYS_TYPE_name = map[int32]string{
	1:  "CACHE_SYS_LUA",
	20: "CACHE_SYS_TYPE_TOOL",
}
var CACHE_SYS_TYPE_value = map[string]int32{
	"CACHE_SYS_LUA":       1,
	"CACHE_SYS_TYPE_TOOL": 20,
}

func (x CACHE_SYS_TYPE) Enum() *CACHE_SYS_TYPE {
	p := new(CACHE_SYS_TYPE)
	*p = x
	return p
}
func (x CACHE_SYS_TYPE) String() string {
	return proto.EnumName(CACHE_SYS_TYPE_name, int32(x))
}
func (x *CACHE_SYS_TYPE) UnmarshalJSON(data []byte) error {
	value, err := proto.UnmarshalJSONEnum(CACHE_SYS_TYPE_value, data, "CACHE_SYS_TYPE")
	if err != nil {
		return err
	}
	*x = CACHE_SYS_TYPE(value)
	return nil
}
func (CACHE_SYS_TYPE) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

type CacheInterfaceReqBody struct {
	GetUserCacheReqbody          *GetUserCacheReqBody          `protobuf:"bytes,1,opt,name=get_user_cache_reqbody" json:"get_user_cache_reqbody,omitempty"`
	ReloadUserCacheReqbody       *ReloadUserCacheReqBody       `protobuf:"bytes,2,opt,name=reload_user_cache_reqbody" json:"reload_user_cache_reqbody,omitempty"`
	UpdateUserNameReqbody        *UpdateUserNameReqBody        `protobuf:"bytes,3,opt,name=update_user_name_reqbody" json:"update_user_name_reqbody,omitempty"`
	UpdateUserSummerTimeReqbody  *UpdateUserSummerTimeReqBody  `protobuf:"bytes,4,opt,name=update_user_summer_time_reqbody" json:"update_user_summer_time_reqbody,omitempty"`
	UpdateUserTimeZoneReqbody    *UpdateUserTimeZoneReqBody    `protobuf:"bytes,5,opt,name=update_user_time_zone_reqbody" json:"update_user_time_zone_reqbody,omitempty"`
	GetUserNickNameReqbody       *GetUserNickNameReqBody       `protobuf:"bytes,6,opt,name=get_user_nick_name_reqbody" json:"get_user_nick_name_reqbody,omitempty"`
	UpdateUserPrivateReqbody     *UpdateUserPrivateReqBody     `protobuf:"bytes,7,opt,name=update_user_private_reqbody" json:"update_user_private_reqbody,omitempty"`
	UpdateLearnMultiLangReqbody  *UpdateLearnMultiLangReqBody  `protobuf:"bytes,8,opt,name=update_learn_multi_lang_reqbody" json:"update_learn_multi_lang_reqbody,omitempty"`
	UpdateUserLangSettingReqbody *UpdateUserLangSettingReqBody `protobuf:"bytes,9,opt,name=update_user_lang_setting_reqbody" json:"update_user_lang_setting_reqbody,omitempty"`
	UpdateUserProfileReqbody     *UpdateUserProfileReqBody     `protobuf:"bytes,10,opt,name=update_user_profile_reqbody" json:"update_user_profile_reqbody,omitempty"`
	UpdateUserPropertyReqbody    *UpdateUserPropertyReqBody    `protobuf:"bytes,11,opt,name=update_user_property_reqbody" json:"update_user_property_reqbody,omitempty"`
	UpdateUserPassWdReqbody      *UpdateUserPassWdReqBody      `protobuf:"bytes,12,opt,name=update_user_pass_wd_reqbody" json:"update_user_pass_wd_reqbody,omitempty"`
	UpdateUserEmailReqbody       *UpdateUserEmailReqBody       `protobuf:"bytes,13,opt,name=update_user_email_reqbody" json:"update_user_email_reqbody,omitempty"`
	UnregisterAccountReqbody     *UnregisterAccountReqBody     `protobuf:"bytes,14,opt,name=unregister_account_reqbody" json:"unregister_account_reqbody,omitempty"`
	UpdateUserLessonVerReqbody   *UpdateUserLessonVerReqBody   `protobuf:"bytes,15,opt,name=update_user_lesson_ver_reqbody" json:"update_user_lesson_ver_reqbody,omitempty"`
	UpdateUserWalletVerReqbody   *UpdateUserWalletVerReqBody   `protobuf:"bytes,16,opt,name=update_user_wallet_ver_reqbody" json:"update_user_wallet_ver_reqbody,omitempty"`
	XXX_unrecognized             []byte                        `json:"-"`
}

func (m *CacheInterfaceReqBody) Reset()                    { *m = CacheInterfaceReqBody{} }
func (m *CacheInterfaceReqBody) String() string            { return proto.CompactTextString(m) }
func (*CacheInterfaceReqBody) ProtoMessage()               {}
func (*CacheInterfaceReqBody) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *CacheInterfaceReqBody) GetGetUserCacheReqbody() *GetUserCacheReqBody {
	if m != nil {
		return m.GetUserCacheReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetReloadUserCacheReqbody() *ReloadUserCacheReqBody {
	if m != nil {
		return m.ReloadUserCacheReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserNameReqbody() *UpdateUserNameReqBody {
	if m != nil {
		return m.UpdateUserNameReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserSummerTimeReqbody() *UpdateUserSummerTimeReqBody {
	if m != nil {
		return m.UpdateUserSummerTimeReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserTimeZoneReqbody() *UpdateUserTimeZoneReqBody {
	if m != nil {
		return m.UpdateUserTimeZoneReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetGetUserNickNameReqbody() *GetUserNickNameReqBody {
	if m != nil {
		return m.GetUserNickNameReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserPrivateReqbody() *UpdateUserPrivateReqBody {
	if m != nil {
		return m.UpdateUserPrivateReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateLearnMultiLangReqbody() *UpdateLearnMultiLangReqBody {
	if m != nil {
		return m.UpdateLearnMultiLangReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserLangSettingReqbody() *UpdateUserLangSettingReqBody {
	if m != nil {
		return m.UpdateUserLangSettingReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserProfileReqbody() *UpdateUserProfileReqBody {
	if m != nil {
		return m.UpdateUserProfileReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserPropertyReqbody() *UpdateUserPropertyReqBody {
	if m != nil {
		return m.UpdateUserPropertyReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserPassWdReqbody() *UpdateUserPassWdReqBody {
	if m != nil {
		return m.UpdateUserPassWdReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserEmailReqbody() *UpdateUserEmailReqBody {
	if m != nil {
		return m.UpdateUserEmailReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUnregisterAccountReqbody() *UnregisterAccountReqBody {
	if m != nil {
		return m.UnregisterAccountReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserLessonVerReqbody() *UpdateUserLessonVerReqBody {
	if m != nil {
		return m.UpdateUserLessonVerReqbody
	}
	return nil
}

func (m *CacheInterfaceReqBody) GetUpdateUserWalletVerReqbody() *UpdateUserWalletVerReqBody {
	if m != nil {
		return m.UpdateUserWalletVerReqbody
	}
	return nil
}

type CacheInterfaceRspBody struct {
	GetUserCacheRspbody          *GetUserCacheRspBody          `protobuf:"bytes,1,opt,name=get_user_cache_rspbody" json:"get_user_cache_rspbody,omitempty"`
	ReloadUserCacheRspbody       *ReloadUserCacheRspBody       `protobuf:"bytes,2,opt,name=reload_user_cache_rspbody" json:"reload_user_cache_rspbody,omitempty"`
	UpdateUserNameRspbody        *UpdateUserNameRspBody        `protobuf:"bytes,3,opt,name=update_user_name_rspbody" json:"update_user_name_rspbody,omitempty"`
	UpdateUserSummerTimeRspbody  *UpdateUserSummerTimeRspBody  `protobuf:"bytes,4,opt,name=update_user_summer_time_rspbody" json:"update_user_summer_time_rspbody,omitempty"`
	UpdateUserTimeZoneRspbody    *UpdateUserTimeZoneRspBody    `protobuf:"bytes,5,opt,name=update_user_time_zone_rspbody" json:"update_user_time_zone_rspbody,omitempty"`
	GetUserNickNameRspbody       *GetUserNickNameRspBody       `protobuf:"bytes,6,opt,name=get_user_nick_name_rspbody" json:"get_user_nick_name_rspbody,omitempty"`
	UpdateUserPrivateRspbody     *UpdateUserPrivateRspBody     `protobuf:"bytes,7,opt,name=update_user_private_rspbody" json:"update_user_private_rspbody,omitempty"`
	UpdateLearnMultiLangRspbody  *UpdateLearnMultiLangRspBody  `protobuf:"bytes,8,opt,name=update_learn_multi_lang_rspbody" json:"update_learn_multi_lang_rspbody,omitempty"`
	UpdateUserLangSettingRspbody *UpdateUserLangSettingRspBody `protobuf:"bytes,9,opt,name=update_user_lang_setting_rspbody" json:"update_user_lang_setting_rspbody,omitempty"`
	UpdateUserProfileRspbody     *UpdateUserProfileRspBody     `protobuf:"bytes,10,opt,name=update_user_profile_rspbody" json:"update_user_profile_rspbody,omitempty"`
	UpdateUserPropertyRspbody    *UpdateUserPropertyRspBody    `protobuf:"bytes,11,opt,name=update_user_property_rspbody" json:"update_user_property_rspbody,omitempty"`
	UpdateUserPassWdRspbody      *UpdateUserPassWdRspBody      `protobuf:"bytes,12,opt,name=update_user_pass_wd_rspbody" json:"update_user_pass_wd_rspbody,omitempty"`
	UpdateUserEmailRspbody       *UpdateUserEmailRspBody       `protobuf:"bytes,13,opt,name=update_user_email_rspbody" json:"update_user_email_rspbody,omitempty"`
	UnregisterAccountRspbody     *UnregisterAccountRspBody     `protobuf:"bytes,14,opt,name=unregister_account_rspbody" json:"unregister_account_rspbody,omitempty"`
	UpdateUserLessonVerRspbody   *UpdateUserLessonVerRspBody   `protobuf:"bytes,15,opt,name=update_user_lesson_ver_rspbody" json:"update_user_lesson_ver_rspbody,omitempty"`
	UpdateUserWalletVerRspbody   *UpdateUserWalletVerRspBody   `protobuf:"bytes,16,opt,name=update_user_wallet_ver_rspbody" json:"update_user_wallet_ver_rspbody,omitempty"`
	XXX_unrecognized             []byte                        `json:"-"`
}

func (m *CacheInterfaceRspBody) Reset()                    { *m = CacheInterfaceRspBody{} }
func (m *CacheInterfaceRspBody) String() string            { return proto.CompactTextString(m) }
func (*CacheInterfaceRspBody) ProtoMessage()               {}
func (*CacheInterfaceRspBody) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *CacheInterfaceRspBody) GetGetUserCacheRspbody() *GetUserCacheRspBody {
	if m != nil {
		return m.GetUserCacheRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetReloadUserCacheRspbody() *ReloadUserCacheRspBody {
	if m != nil {
		return m.ReloadUserCacheRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserNameRspbody() *UpdateUserNameRspBody {
	if m != nil {
		return m.UpdateUserNameRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserSummerTimeRspbody() *UpdateUserSummerTimeRspBody {
	if m != nil {
		return m.UpdateUserSummerTimeRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserTimeZoneRspbody() *UpdateUserTimeZoneRspBody {
	if m != nil {
		return m.UpdateUserTimeZoneRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetGetUserNickNameRspbody() *GetUserNickNameRspBody {
	if m != nil {
		return m.GetUserNickNameRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserPrivateRspbody() *UpdateUserPrivateRspBody {
	if m != nil {
		return m.UpdateUserPrivateRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateLearnMultiLangRspbody() *UpdateLearnMultiLangRspBody {
	if m != nil {
		return m.UpdateLearnMultiLangRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserLangSettingRspbody() *UpdateUserLangSettingRspBody {
	if m != nil {
		return m.UpdateUserLangSettingRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserProfileRspbody() *UpdateUserProfileRspBody {
	if m != nil {
		return m.UpdateUserProfileRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserPropertyRspbody() *UpdateUserPropertyRspBody {
	if m != nil {
		return m.UpdateUserPropertyRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserPassWdRspbody() *UpdateUserPassWdRspBody {
	if m != nil {
		return m.UpdateUserPassWdRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserEmailRspbody() *UpdateUserEmailRspBody {
	if m != nil {
		return m.UpdateUserEmailRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUnregisterAccountRspbody() *UnregisterAccountRspBody {
	if m != nil {
		return m.UnregisterAccountRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserLessonVerRspbody() *UpdateUserLessonVerRspBody {
	if m != nil {
		return m.UpdateUserLessonVerRspbody
	}
	return nil
}

func (m *CacheInterfaceRspBody) GetUpdateUserWalletVerRspbody() *UpdateUserWalletVerRspBody {
	if m != nil {
		return m.UpdateUserWalletVerRspbody
	}
	return nil
}

func init() {
	proto.RegisterType((*CacheInterfaceReqBody)(nil), "ht.cache_interface.CacheInterfaceReqBody")
	proto.RegisterType((*CacheInterfaceRspBody)(nil), "ht.cache_interface.CacheInterfaceRspBody")
	proto.RegisterEnum("ht.cache_interface.CMD_TYPE", CMD_TYPE_name, CMD_TYPE_value)
	proto.RegisterEnum("ht.cache_interface.CACHE_RET_CODE", CACHE_RET_CODE_name, CACHE_RET_CODE_value)
	proto.RegisterEnum("ht.cache_interface.CACHE_SYS_TYPE", CACHE_SYS_TYPE_name, CACHE_SYS_TYPE_value)
}

func init() { proto.RegisterFile("HT_cache_interface_head.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 1051 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0xd4, 0xcb, 0x57, 0xdb, 0xc6,
	0x17, 0x07, 0xf0, 0x1f, 0x38, 0x0f, 0x32, 0x04, 0x32, 0xd1, 0x2f, 0x10, 0x5e, 0x01, 0x4e, 0x36,
	0x6d, 0x69, 0x4b, 0x7b, 0xba, 0xee, 0x46, 0x91, 0xa6, 0x46, 0x27, 0x7a, 0x75, 0x66, 0x04, 0x81,
	0xcd, 0x1c, 0xd5, 0x1e, 0x8c, 0x4e, 0x6c, 0xd9, 0x91, 0xe4, 0xe4, 0xd0, 0xbf, 0xb2, 0x7f, 0x46,
	0x77, 0xdd, 0xf4, 0xbd, 0xea, 0xd1, 0x68, 0xf4, 0xb0, 0x2d, 0x19, 0xb4, 0x03, 0x5d, 0xdf, 0x8f,
	0xae, 0xee, 0x48, 0x5f, 0xf0, 0xea, 0x8c, 0xb2, 0x9e, 0xdf, 0xbb, 0xe1, 0x2c, 0x08, 0x13, 0x1e,
	0x5d, 0xfb, 0x3d, 0xce, 0x6e, 0xb8, 0xdf, 0x3f, 0x9d, 0x44, 0xe3, 0x64, 0xac, 0x28, 0x37, 0xc9,
	0xe9, 0x5c, 0x79, 0xaf, 0xae, 0xe5, 0xa7, 0x71, 0xff, 0x36, 0x6b, 0x79, 0xfd, 0x0b, 0x00, 0x5b,
	0x5a, 0x5a, 0x36, 0xf2, 0x2a, 0xe6, 0x1f, 0xde, 0x8c, 0xfb, 0xb7, 0x4a, 0x17, 0x6c, 0x0f, 0x78,
	0xc2, 0xa6, 0x31, 0x8f, 0x24, 0x10, 0xf1, 0x0f, 0x69, 0xe7, 0xce, 0xca, 0xf1, 0xca, 0xe7, 0xeb,
	0xdf, 0x7d, 0x76, 0xba, 0x78, 0xb7, 0xd3, 0x2e, 0x4f, 0xbc, 0x98, 0x47, 0x42, 0xcc, 0x21, 0x0b,
	0xec, 0x46, 0x7c, 0x38, 0xf6, 0xfb, 0x75, 0xd6, 0xaa, 0xb0, 0x4e, 0xea, 0x2c, 0x2c, 0x9a, 0x16,
	0xb8, 0xb7, 0x60, 0x67, 0x3a, 0xe9, 0xfb, 0x09, 0xcf, 0xb8, 0xd0, 0x1f, 0x95, 0x5a, 0x47, 0x68,
	0x5f, 0xd4, 0x69, 0x9e, 0xe8, 0x49, 0x35, 0xdb, 0x1f, 0x15, 0xd8, 0x3b, 0x70, 0x54, 0xc5, 0xe2,
	0xe9, 0x68, 0xc4, 0x23, 0x96, 0x04, 0x15, 0xf3, 0x81, 0x30, 0xbf, 0x59, 0x6e, 0x12, 0xd1, 0x48,
	0x83, 0x52, 0xa6, 0xe0, 0x55, 0x55, 0x16, 0xe4, 0xcf, 0xe3, 0xb0, 0x74, 0x1f, 0x0a, 0xf7, 0xeb,
	0xe5, 0x6e, 0x2a, 0x5e, 0x8d, 0xc3, 0x42, 0xb5, 0xc1, 0x5e, 0x71, 0x28, 0x61, 0xd0, 0x7b, 0x3f,
	0xfb, 0xf8, 0x8f, 0x9a, 0x97, 0x29, 0x0f, 0xc6, 0x0e, 0x7a, 0xef, 0xab, 0xcf, 0xff, 0x23, 0xd8,
	0xaf, 0x4e, 0x39, 0x89, 0x82, 0x8f, 0xe9, 0x3f, 0x39, 0xf8, 0x58, 0x80, 0x5f, 0x2d, 0x9f, 0xd1,
	0xcd, 0x9a, 0x16, 0x57, 0x3a, 0xe4, 0x7e, 0x14, 0xb2, 0xd1, 0x74, 0x98, 0x04, 0x6c, 0xe8, 0x87,
	0x83, 0x82, 0x5d, 0xbb, 0x6b, 0xa5, 0x66, 0xda, 0x69, 0xa5, 0x8d, 0xa6, 0x1f, 0x0e, 0x72, 0xf9,
	0x0a, 0x1c, 0x57, 0x87, 0x15, 0x64, 0xcc, 0x93, 0x24, 0xa8, 0xd0, 0x4f, 0x04, 0xfd, 0xed, 0xf2,
	0x89, 0x53, 0x94, 0x64, 0x8d, 0x8d, 0x8b, 0x18, 0x5f, 0x07, 0xc3, 0x72, 0x11, 0xe0, 0x7e, 0x8b,
	0x10, 0x4d, 0x39, 0x49, 0xc0, 0xc1, 0x1c, 0x39, 0xe1, 0x51, 0x72, 0x5b, 0x98, 0xeb, 0xf7, 0x79,
	0x01, 0x5c, 0xd9, 0x95, 0xa3, 0xee, 0xdc, 0x9c, 0x7e, 0x1c, 0xb3, 0x4f, 0xfd, 0xc2, 0x7c, 0x2a,
	0xcc, 0x2f, 0xef, 0x30, 0xfd, 0x38, 0xbe, 0xe8, 0x57, 0x3e, 0xcf, 0xaa, 0xc8, 0x47, 0x7e, 0x30,
	0x2c, 0xbc, 0x8d, 0xe6, 0x37, 0xaa, 0xf4, 0x50, 0xda, 0x52, 0x0e, 0xb8, 0x37, 0x0d, 0x23, 0x3e,
	0x08, 0xe2, 0x84, 0x47, 0xcc, 0xef, 0xf5, 0xc6, 0xd3, 0x30, 0x29, 0xbc, 0xcd, 0x25, 0x7b, 0x2c,
	0xba, 0xd4, 0xac, 0x29, 0x17, 0xcf, 0xc1, 0xe1, 0xcc, 0xb1, 0xf3, 0x38, 0x1e, 0x87, 0xec, 0x23,
	0x8f, 0x0a, 0xf5, 0x99, 0x50, 0x4f, 0xef, 0x38, 0x74, 0xd1, 0x77, 0xce, 0xa3, 0x06, 0xf7, 0x93,
	0x3f, 0x1c, 0xf2, 0x64, 0xc6, 0x85, 0xf7, 0x71, 0x2f, 0x44, 0x5f, 0xe9, 0xd6, 0x45, 0x6a, 0x3c,
	0x69, 0x8a, 0xd4, 0x78, 0xd2, 0x2a, 0x52, 0x25, 0x54, 0x1f, 0xa9, 0xd2, 0x6a, 0x11, 0xa9, 0x92,
	0xab, 0x8d, 0x54, 0xa9, 0xdd, 0x3f, 0x52, 0x25, 0xb6, 0x2c, 0x52, 0xa5, 0xd9, 0x36, 0x52, 0xa5,
	0xdc, 0x1c, 0xa9, 0xd2, 0x6d, 0x17, 0xa9, 0x52, 0x6d, 0x88, 0x54, 0x49, 0xb6, 0x88, 0x54, 0xe9,
	0x35, 0x45, 0xaa, 0x04, 0x5b, 0x45, 0xea, 0xc2, 0x4a, 0x17, 0x23, 0x55, 0xb2, 0x6d, 0x23, 0x55,
	0xca, 0x4b, 0x23, 0x55, 0xd2, 0xad, 0x23, 0xb5, 0x69, 0x11, 0x32, 0x52, 0x25, 0xdb, 0x2a, 0x52,
	0x25, 0xd9, 0x18, 0xa9, 0xd2, 0x6c, 0x17, 0xa9, 0x12, 0x6d, 0x8a, 0x54, 0x69, 0xb6, 0x89, 0xd4,
	0xf2, 0xf3, 0xac, 0x89, 0x54, 0xe9, 0xb5, 0x88, 0xd4, 0x62, 0xc0, 0xda, 0x48, 0x95, 0x5e, 0xab,
	0x48, 0x95, 0xe2, 0x92, 0x48, 0x95, 0x6a, 0xcb, 0x48, 0xad, 0x77, 0xab, 0x91, 0x2a, 0xdd, 0x96,
	0x91, 0x9a, 0xb9, 0x27, 0xbf, 0x76, 0xc0, 0x9a, 0x66, 0xe9, 0x8c, 0x5e, 0xba, 0x48, 0xd9, 0x06,
	0x4a, 0xfa, 0x77, 0x17, 0x51, 0xe6, 0x11, 0x84, 0x99, 0xa6, 0x6a, 0x67, 0x08, 0xae, 0x28, 0xbb,
	0x60, 0x2b, 0xbd, 0x8e, 0x91, 0xe9, 0xa8, 0x7a, 0xb5, 0xb4, 0xaa, 0xec, 0x80, 0x17, 0x69, 0xc9,
	0x73, 0x75, 0x95, 0xa2, 0xac, 0x64, 0xab, 0x16, 0x82, 0x1d, 0xe5, 0x35, 0x38, 0xac, 0x54, 0x88,
	0x67, 0x59, 0x08, 0x33, 0x6a, 0x58, 0x88, 0x11, 0x44, 0xa9, 0x61, 0x77, 0xe1, 0x03, 0xe5, 0x00,
	0xec, 0xcc, 0x77, 0xa7, 0xbf, 0xb8, 0x72, 0x6c, 0x04, 0x1f, 0x2a, 0x7b, 0x60, 0x7b, 0x66, 0x1c,
	0xdb, 0xd0, 0xde, 0x66, 0xfa, 0x23, 0x65, 0x1f, 0xbc, 0x9c, 0xef, 0x74, 0xb1, 0x71, 0xae, 0x52,
	0x04, 0x1f, 0x2b, 0x47, 0x60, 0xbf, 0x52, 0x34, 0x91, 0x8a, 0x6d, 0x66, 0x79, 0x26, 0x35, 0x98,
	0xa9, 0xda, 0x5d, 0xb8, 0xa6, 0x1c, 0x83, 0x83, 0xf9, 0xee, 0xb4, 0x52, 0x4c, 0xf6, 0xa4, 0xde,
	0x77, 0x7e, 0x30, 0x4c, 0x04, 0x41, 0xdd, 0xd8, 0x2e, 0x76, 0x5c, 0x84, 0xe9, 0x25, 0x5c, 0xcf,
	0xc7, 0x9e, 0xa9, 0xaa, 0x84, 0x5c, 0xe8, 0xf0, 0x69, 0xbe, 0xc9, 0x6a, 0x0d, 0x59, 0xaa, 0x61,
	0xc2, 0x8d, 0x62, 0x68, 0x1b, 0xa3, 0xae, 0x41, 0x28, 0xc2, 0x59, 0x59, 0xd5, 0x34, 0xc7, 0xb3,
	0x29, 0xdc, 0x54, 0x0e, 0xc1, 0xde, 0xc2, 0xd0, 0x88, 0x10, 0xc7, 0x66, 0xe7, 0x08, 0xc3, 0x67,
	0x75, 0xf5, 0x0b, 0xd5, 0x34, 0x11, 0x15, 0x75, 0x78, 0xf2, 0xdb, 0x2a, 0xd8, 0x14, 0xc7, 0xc6,
	0x30, 0xa2, 0x4c, 0x73, 0x74, 0xa4, 0x6c, 0x81, 0xe7, 0xe5, 0x15, 0xe2, 0x69, 0x1a, 0x22, 0x04,
	0xfe, 0x4f, 0x1c, 0x6a, 0x71, 0x19, 0x23, 0x17, 0xa9, 0x94, 0xa9, 0xba, 0x0e, 0xfb, 0xb3, 0x15,
	0xdb, 0xa1, 0x4c, 0x3b, 0x53, 0xed, 0x2e, 0x82, 0x7c, 0x96, 0xb2, 0x1d, 0x66, 0x39, 0x18, 0xc1,
	0x6b, 0xb1, 0xc7, 0xe2, 0xb2, 0x61, 0x9f, 0xab, 0xa6, 0xa1, 0x33, 0x57, 0xc5, 0xaa, 0x05, 0x07,
	0x62, 0xe2, 0xa2, 0xa8, 0x7b, 0xae, 0x69, 0x68, 0xe9, 0xe8, 0x18, 0x69, 0x0e, 0xd6, 0xe1, 0x8d,
	0xb2, 0x05, 0x60, 0x59, 0x77, 0xdf, 0x30, 0x84, 0x31, 0xfc, 0xbd, 0xa3, 0xec, 0x83, 0xed, 0xaa,
	0x49, 0x11, 0xb6, 0x55, 0x53, 0x14, 0xff, 0xe8, 0x28, 0x47, 0x55, 0x93, 0x20, 0x42, 0xc4, 0x1b,
	0xe5, 0x78, 0x54, 0xfc, 0xe0, 0xcf, 0x8e, 0x72, 0x08, 0x76, 0xab, 0xdd, 0xae, 0x47, 0xb3, 0x79,
	0x44, 0xfd, 0xaf, 0xce, 0xec, 0x4d, 0xf5, 0xec, 0xa6, 0x7f, 0x77, 0x94, 0x1d, 0xf0, 0xff, 0xd9,
	0x27, 0x47, 0xef, 0x0c, 0x42, 0xe1, 0x3f, 0x73, 0x15, 0x8c, 0x74, 0x83, 0x88, 0x9e, 0x7f, 0x3b,
	0x27, 0xdf, 0xe7, 0x0b, 0x27, 0x97, 0x24, 0xfb, 0xc2, 0x9e, 0x83, 0x8d, 0xf2, 0x8a, 0xe9, 0xa9,
	0x70, 0x45, 0x79, 0x99, 0xb7, 0xe7, 0x3f, 0x62, 0xd4, 0x71, 0x4c, 0xf8, 0xe2, 0xbf, 0x00, 0x00,
	0x00, 0xff, 0xff, 0xe2, 0x14, 0x8c, 0xe5, 0x93, 0x0e, 0x00, 0x00,
}
