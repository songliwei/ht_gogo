#include <stdint.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include "lib_gen_mid.h"
#include "lib_hash.h"
#include <stdbool.h>

static char mac_str[64];
static uint32_t mac;
static uint32_t pid;
static int seq;

static int init_mac()
{
#define MAXINTERFACES 64
	int fd = 0;
   	int	if_cnt = 0;
	struct ifreq ifbuf[MAXINTERFACES];
	struct ifconf ifc;

	if((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		//printf("scoket failed!\n");
		return -1;
	}

	ifc.ifc_len = sizeof(ifbuf);
	ifc.ifc_buf = (caddr_t)ifbuf;
	if (ioctl(fd, SIOCGIFCONF, (char *)&ifc) < 0)
	{
		//printf("ioctl SIOCGIFCONF failed!\n");
		return -1;
	}

	if_cnt = ifc.ifc_len / sizeof(struct ifreq);
	//printf("if_cnt num is %d\n", if_cnt);
	int i = 0;
	for (; i < if_cnt; ++i)
	{
		//printf("net device %s\n", ifbuf[i].ifr_name);
		int ifr_name_len = strlen(ifbuf[i].ifr_name);
		if(ifbuf[i].ifr_name[0] == 'e' 
				&& (ifbuf[i].ifr_name[ifr_name_len - 1] == '0' 
				|| ifbuf[i].ifr_name[ifr_name_len - 1] == '1'))
		{
			if ((ioctl(fd, SIOCGIFHWADDR, (char *)&ifbuf[i])) < 0)
			{
				//printf("ioctl SIOCGIFCONF failed!\n");
				return -1;
			}
			snprintf(mac_str, sizeof(mac_str), "%s:%02X:%02X:%02X:%02X:%02X:%02X",
					ifbuf[i].ifr_name,
					(unsigned char)ifbuf[i].ifr_hwaddr.sa_data[0],
					(unsigned char)ifbuf[i].ifr_hwaddr.sa_data[1],
					(unsigned char)ifbuf[i].ifr_hwaddr.sa_data[2],
					(unsigned char)ifbuf[i].ifr_hwaddr.sa_data[3],
					(unsigned char)ifbuf[i].ifr_hwaddr.sa_data[4],
					(unsigned char)ifbuf[i].ifr_hwaddr.sa_data[5]);

			mac = fnv_32_buf((void*)(ifbuf[i].ifr_hwaddr.sa_data), 6, FNV1_32_INIT);
			mac &= 0XF;
			//printf("mac_str:%s, mac:%u\n", mac_str, mac);
		}
	}
	return 0;
}

static int init_pid()
{
	pid_t p = getpid();
	pid = p & 0XF;
	//printf("pid:%u\n", pid);
	return 0;
}

static int init_seq()
{
	uint32_t seed = getpid();
	srand(seed);
	seq = rand();
	//printf("seq:%u\n", seq);
	return 0;
}

static uint64_t get_time64()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	uint64_t ret = 0;
	ret |= (tv.tv_sec << 32);
	ret |= (tv.tv_usec << 12);
	return ret;
}

static int init_mid_generator()
{
	init_mac();
	init_pid();
	init_seq();
	return 1;
}

uint64_t gen_mid(uint32_t uid, uint32_t type)
{
	static bool bInit = false;
	bInit = bInit ? true : init_mid_generator();

	uint64_t cur_time = get_time64();
	cur_time = (cur_time >> 22) & 0X1FFFFFFFFFF;//get 23~63 bit
	uint32_t cur_seq = __sync_add_and_fetch(&seq, 1);
	cur_seq &= 0X1F;
	uid &= 0X3F;
	type &= 0XF;

	uint64_t mid = 0;
	mid = (cur_time << 23) | (mac << 19) | (pid << 15) | (uid << 10) | (type << 6) | (cur_seq);
	return mid;
}

#if 0
int main(int argc, char** argv)
{
	if(argc != 2)
	{
		printf("Usage: [cnt]\n");
		return 0;
	}
	init_mid_generator();
	int i = 0;
	int c = atoi(argv[1]);
	for(i = 0; i < c; ++i)
	{
		uint64_t mid = gen_mid(100, 1);
		printf("mid[%d]:%lx\n", i, mid);
	}
	return 0;
}
#endif

