package libgenmid

/*
#cgo CFLAGS: -I./
#cgo LDFLAGS: -L/root/go/src/github.com/HT_GOGO/gotcp/libgenmid -lgenmid
#include <stdlib.h>
#include "lib_gen_mid.h"
*/
import "C"

func GenMid() (mid uint64) {
	var mid_c C.uint64_t = C.gen_mid()
	mid = uint64(mid_c)
	return mid
}
