#ifndef LIB_GEN_MID_H
#define LIB_GEN_MID_H
#include <stdint.h>

/*mid
 * 41bit(time) + 4bit(mac) + 4bit(pid) + 5bit(uid) + 4bit(type/reserved) + 6bit(seq)
 * */


uint64_t gen_mid();

#endif

