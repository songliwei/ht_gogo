package main

import (
	"database/sql"
	"fmt"
	"github.com/gansidui/gotcp"
	"github.com/gansidui/gotcp/libcomm"
	tcpcommon "github.com/gansidui/gotcp/tcpfw/common"
	"github.com/gansidui/gotcp/tcpfw/include/ht_moment"
	"github.com/gansidui/gotcp/webapi/common"
	"github.com/gansidui/gotcp/webapi/common/sdk"
	"github.com/golang/protobuf/proto"
	"github.com/nsqio/go-nsq"
	//"io/ioutil"
	"net"
	"os/signal"
	//"regexp"
	"math/rand"
	"strconv"
	"strings"
	"syscall"
	//"github.com/garyburd/redigo/redis"
	"errors"

	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"log"

	"os"
	"runtime"
	"time"
)

const (
	CMD_POST_MOMENT  = 1
	CMD_POST_LIKE    = 2
	CMD_POST_COMMENT = 3

	CMD_DEL_MOMENT  = 4
	CMD_DEL_COMMENT = 5

	CMD_UPDATE_MOMENT_WORDS        = 1002 //从db里面更新信息流贴文关键词
	CMD_UPDATE_COMMENT_WORDS       = 1003 //从db里面更新comment敏感词
	CMD_UPDATE_SELFIE_ALLOW_PERIOD = 1004 //从redis里面更新自拍允许时间段
	CMD_UPDATE_PORN_SCORE          = 1005 //更新图片的得分匹配关系

	//监控指标。
	STAT_POST_MOMENT_REQ  = "mmh/post_moment"
	STAT_POST_COMMENT_REQ = "mmh/post_commet"

	STAT_POST_MOMENT_REQ_FAIL  = "mmh/post_moment_fail"
	STAT_POST_COMMENT_REQ_FAIL = "mmh/post_commet_fail"

	STAT_QRCODE_FOUND          = "mmh/found_qrcode"
	STAT_BADWORD_COMMENT_FOUND = "mmh/found_bad_comment"
	STAT_FACE_FOUND            = "mmh/found_face"
	STAT_SEXY_FOUND            = "mmh/sexy_face"
	STAT_MOMENT_API_FAIL       = "mmh/moment_api_fail"

	MMH_REASON_QRCODE     = "qrcode2"
	MMH_REASON_PORN       = "porn mmt"
	MMH_REASON_PEOPLE     = "people mmt"
	MMH_REASON_SEXY_HIDE  = "sexy hide"
	MMH_REASON_SEXY_MAYBE = "sexy maybe"
	MMH_REASON_AD         = "ad mmt"
	MMH_REASON_BAD_WORDS  = "bad_words"
)

var (
	MAX_SLEFIE_SQR float64

	sexyImageScore float64
	pornImageScore float64

	Mgdb          *sql.DB
	Masterdb      *sql.DB
	Logdb         *sql.DB
	RulerRedis    *tcpcommon.RedisApi
	UserinfoRedis *tcpcommon.RedisApi

	nsqPublisher   *nsq.Producer
	nsqMomentTopic string

	userInfoCacheApi *tcpcommon.UserInfoCacheApi

	momentAPI        *tcpcommon.MntApi
	newMomentAPI     *tcpcommon.MntApi
	counterMomentAPI *tcpcommon.MntApi

	MOMENT_MAX_POST_NUM int //--从第九条开始需要隐藏

	forbidden_face string

	newTestUserid map[uint32]uint32

	momentTextFilter *common.TextFilter

	commentTextFilter *common.TextFilter

	imageChecker *sdk.ImageFilter

	userPri *common.UserPrivilege

	infoLog *log.Logger
	options common.Options
)

type Callback struct{}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*tcpcommon.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:[Uid=%v,Seq=%v,Cmd=%v,Len=%v]", head.Uid, head.Seq, head.Cmd, head.Len)

	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	switch int(head.Cmd) {

	case CMD_UPDATE_SELFIE_ALLOW_PERIOD, CMD_UPDATE_MOMENT_WORDS, CMD_UPDATE_COMMENT_WORDS:

		go func() {
			err := ProcConfigUpdate(c, head, packet.GetBody())
			SendResponseCode(c, head, err)
		}()

	case CMD_POST_MOMENT:
		go func() {
			_, err := ProcPostMoment(c, head, packet.GetBody())
			if err != nil {
				infoLog.Println("OnMessage ProcPostMoment() err=", err, head)
				libcomm.AttrAdd(STAT_POST_MOMENT_REQ_FAIL, 1)
				// infoLog.Printf("OnMessage CMD_POST_MOMENT packet.GetBody()=%x", string(packet.GetBody()))
				// infoLog.Printf("OnMessage CMD_POST_MOMENT packet.GetBody()=%s", string(packet.GetBody()))
				// SendRetCode(c, head, 1, err.Error())
			} else {
				// SendRetCode(c, head, 0, "OK")
			}
		}()
	case CMD_POST_COMMENT:
		go func() {
			_, err := ProcPostComment(c, head, packet.GetBody())
			if err != nil {
				infoLog.Println("OnMessage CMD_POST_COMMENT err=", err, head.Uid)
				libcomm.AttrAdd(STAT_POST_COMMENT_REQ_FAIL, 1)
				common.SendMomentStatusCode(c, head, ht_moment.RET_CODE_RET_INTERNAL_ERR, err.Error())
				//SendRetCode(c, head, 1, err.Error())
			} else {
				// SendRetCode(c, head, 0, "OK")
			}
		}()
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =%d", head.Cmd)
	}
	return true
}

func ProcConfigUpdate(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (err error) {

	infoLog.Printf("ProcConfigUpdate() Cmd=%d", head.Cmd)

	if head.Cmd == CMD_UPDATE_SELFIE_ALLOW_PERIOD {

		err = userPri.InitSelfieAllowTime()

	}

	if head.Cmd == CMD_UPDATE_MOMENT_WORDS {

		err = momentTextFilter.RefreshRegex()

	}

	if head.Cmd == CMD_UPDATE_COMMENT_WORDS {

		err = commentTextFilter.RefreshRegex()

	}

	if head.Cmd == CMD_UPDATE_PORN_SCORE {

		err = errors.New("not finish")

	}

	return

}

func ProcPostMoment(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (bool, error) {

	libcomm.AttrAdd(STAT_POST_MOMENT_REQ, 1)

	ReqBody := &ht_moment.ReqBody{}
	err := proto.Unmarshal(payLoad, ReqBody)
	if err != nil {
		return false, err
	}

	momentPostBody := ReqBody.GetPostMntReqbody()

	if momentPostBody == nil {
		return false, errors.New("ProcPostMoment() GetPostMntReqbody fail")
	}

	momentBody := momentPostBody.GetMoment()

	if momentBody == nil {
		return false, errors.New("ProcPostMoment() GetMoment fail")
	}

	Mid := string(momentBody.GetMid())
	Userid := momentBody.GetUserid()
	OsType := momentPostBody.GetOstype()
	HandleAction := []uint32{}
	text := string(momentBody.GetContent())
	PostTime := (momentBody.GetPostTime())

	AllowSelfie := userPri.IsSelfieAllowedThisTime()
	checkTypes := []string{"porn", "qrcode", "sface"}

	if AllowSelfie {
		checkTypes = []string{"porn", "qrcode"}
	}

	userInfo, err := userInfoCacheApi.GetUserLanguageSetting(Userid)

	if err != nil {
		infoLog.Println(Userid, "ProcPostMoment() GetUserLanguageSetting  fail err=", err)
	}

	images := momentBody.GetImages()

	adNeed := isNeedCheckAd(Mid, Userid, userInfo, len(images))

	if adNeed == true {
		checkTypes = append(checkTypes, "ad")
	}

	//记录是否检测允许发送多条，多条以后只给粉丝看
	OnlyToMe := (momentPostBody.GetOnlyToMe())

	rest := ""
	if len(text) > 2 {

		checkText := momentTextFilter.DropSympos(text)

		//文本过滤
		rest, HandleAction, err = momentTextFilter.IsTextIllegally(checkText, Mid)

		if err != nil {

			infoLog.Println(Mid, Userid, "ProcPostMoment() IsTextIllegally:", err, ",text:", text)

		} else {

			infoLog.Printf("ProcPostMoment() badword=[%s] from=[%s],act=[%d]", rest, text, HandleAction)
			//找到了敏感词

			saveBannedUseridByAction(Mid, "", Userid, "bad_words", rest, HandleAction)
			userPri.HanderUserByActionType(Mid, Userid, HandleAction, "mmt "+rest)

			err := SaveBadMoment(Mid, Userid, OsType, PostTime, rest, nil, nil, nil, nil, momentBody, OnlyToMe, userInfo)

			if err != nil {
				return false, err
			}
			return true, nil

		}
	}

	//没有图片，直接通知通过
	if len(images) == 0 {

		infoLog.Println(Mid, Userid, "ProcPostMoment() not bad words,no images,publish to all")

		ShowMomentToAllUsers(Mid, Userid, momentBody, OnlyToMe, userInfo)

		return true, nil
	}

	foundBad := false

	var QrInfo *sdk.QRInfoBody
	var pornInfo *sdk.PornInfoBody
	var selfieInfo *sdk.SelfieInfoBody
	var adInfo *sdk.AdInfoBody

	if len(images) > 0 {
		//图片过滤

		checkArray := getNeedCheckImageArray(Mid, Userid, adNeed, images)

		// checker := sdk.NewImageFilter(access_key_id, access_key_secret, infoLog)

		imageResult, err := imageChecker.DoCheck(Mid, checkArray, checkTypes)

		if err != nil {
			infoLog.Println(Mid, Userid, "ProcPostMoment() image check err:", err)
			return false, err
		}

		//发现自拍,如果这段时间内可以自拍，那么在场景里面
		if imageResult.SelfieInfo.IsSelfie == 1 {

			libcomm.AttrAdd(STAT_FACE_FOUND, 1)

			foundBad = true
			isThisUserAllow, _ := userPri.IsUserAllowSendSelfie(Userid)

			selfieInfo = &sdk.SelfieInfoBody{
				IsSelfie:   imageResult.SelfieInfo.IsSelfie,
				SelfieRate: imageResult.SelfieInfo.SelfieRate,
				IsNeedHide: 0,
			}

			peoName := imageResult.SelfieInfo.PeopleName

			if isThisUserAllow == false && imageResult.SelfieInfo.SelfieRate >= MAX_SLEFIE_SQR {

				infoLog.Printf("ProcPostMoment() mid=%s,userid=%d,SelfieRate=%f,max=%f,IsNeedHide=1,peoName=%s",
					Mid, Userid, imageResult.SelfieInfo.SelfieRate, MAX_SLEFIE_SQR, peoName)

				selfieInfo.IsNeedHide = 1

			} else {

				if peoName != "" && strings.Contains(forbidden_face, peoName) {
					selfieInfo.IsNeedHide = 1
					selfieInfo.PeopleName = peoName
				}

				infoLog.Printf("ProcPostMoment() mid=%s,userid=%d,SelfieRate=%f,max=%f,isThisUserAllow=%t,peoName=%s",
					Mid, Userid, imageResult.SelfieInfo.SelfieRate, MAX_SLEFIE_SQR, isThisUserAllow, peoName)

			}
		}

		ishtQrCode := 0
		//发现二维码
		if imageResult.QrcodeInfo.QrCode > 0 {

			libcomm.AttrAdd(STAT_QRCODE_FOUND, 1)

			foundBad = true
			isThisUserAllow, _ := userPri.IsUserAllowSendQrCode(Userid)
			allowed := uint8(0)
			//该用户允许发二维码
			if isThisUserAllow {
				allowed = 1
			}

			//二维码是HT的
			// htQrOnly := uint32(sdk.QR_HT_USER_PROFILE | sdk.QR_HT_GROUP_INFO | sdk.QR_HT_OTHER_INFO)
			ishtQrCode = 1
			if (imageResult.QrcodeInfo.QrCode | sdk.HtQrAllowed) == sdk.HtQrAllowed {
				infoLog.Println(Mid, Userid, "ProcPostMoment() this is HT Qrcode", imageResult.QrcodeInfo.QrUrl)
				allowed = 1
				ishtQrCode = 2
			}

			QrInfo = &sdk.QRInfoBody{
				QrCode:    imageResult.QrcodeInfo.QrCode,
				Url:       imageResult.QrcodeInfo.Url,
				AllowedQr: allowed,
			}
		}

		//发现广告
		if imageResult.AdInfo.NeedBlock == 1 {

			infoLog.Printf("ProcPostMoment() find adInfo userid=%d,mid=%s,ishtQrCode=%v",
				Userid, Mid, ishtQrCode)
			if ishtQrCode != 2 {
				adInfo = &imageResult.AdInfo
				foundBad = true
			}
		}

		//发现色情帖子
		if len(imageResult.PornInfo) > 0 {

			libcomm.AttrAdd(STAT_SEXY_FOUND, 1)

			foundBad = true

			sexy := uint8(0)
			porn := uint8(0)
			ImgId := uint8(0)
			rate := float64(0.0)

			//找到分值最大的
			for _, v := range imageResult.PornInfo {

				if v.IsSexy > 0 {
					sexy = 1
				}

				if v.IsPorn > 0 {
					porn = 1
					ImgId = v.ImgId
				}

				if v.Rate > rate {
					rate = v.Rate
				}
			}

			pornInfo = &sdk.PornInfoBody{
				Rate:   rate,
				IsPorn: porn,
				IsSexy: sexy,
				ImgId:  ImgId,
			}
		}

		if foundBad == true {
			err = SaveBadMoment(Mid, Userid, OsType, PostTime, rest, selfieInfo, QrInfo, pornInfo, adInfo, momentBody, OnlyToMe, userInfo)
			if err != nil {
				return false, err
			}
		} else {
			ShowMomentToAllUsers(Mid, Userid, momentBody, OnlyToMe, userInfo)
			infoLog.Println(Mid, Userid, "ProcPostMoment() no bad info found")
		}

	}

	return true, nil
}

func getTextContentFromCommentBody(cmmtBody *ht_moment.CommentBody) string {

	Text := ""

	if cmmtBody.GetCtype() == ht_moment.COMMENT_TYPE_TEXT {

		toidList := cmmtBody.GetToidList()

		Text = string(cmmtBody.GetContent())

		for _, v := range toidList {
			nick := string(v.GetNickname())
			// /Type := v.GetUserType()
			if strings.Contains(Text, "@"+nick) {
				Text = strings.Replace(Text, "@"+nick, "", -1)
			}
		}

	}

	if cmmtBody.GetCtype() == ht_moment.COMMENT_TYPE_CORRECT {

		correctList := cmmtBody.GetCorrection().GetCorrectContent()
		Text = string(cmmtBody.GetCorrection().GetNote())
		for _, v := range correctList {
			Text = Text + "  " + string(v.GetCorrection())
		}

	}

	return Text
}

func ProcPostComment(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (bool, error) {

	libcomm.AttrAdd(STAT_POST_COMMENT_REQ, 1)

	ReqBody := &ht_moment.ReqBody{}
	err := proto.Unmarshal(payLoad, ReqBody)
	if err != nil {
		return false, err
	}

	commentPostBody := ReqBody.GetPostCmntReqbody()

	if commentPostBody == nil {
		return false, errors.New("ProcPostComment() GetPostCmntReqbody fail")
	}

	cmmtBody := commentPostBody.GetCommentBody()

	isNeedSetDeleted := false
	badText := ""
	Text := ""
	HandleAction := []uint32{}
	Mid := string(commentPostBody.GetMid())
	Userid := cmmtBody.GetUserid()

	Text = getTextContentFromCommentBody(cmmtBody)

	infoLog.Println("ProcPostComment() start check ", head.Uid, Text)

	badText, HandleAction, err = commentTextFilter.IsTextIllegally(Text, Mid)

	if err != nil {

		//infoLog.Println(Userid, "not found", rest, err, text)
	} else {
		isNeedSetDeleted = true
		infoLog.Printf("ProcPostComment() userid=%d,mid=%s,badword=%s", Userid, Mid, badText)
	}

	if isNeedSetDeleted == true {

		libcomm.AttrAdd(STAT_BADWORD_COMMENT_FOUND, 1)

		go func() {
			userPri.HanderUserByActionType(Mid, Userid, HandleAction, "cmnt "+badText)
		}()

		//设置帖子为已经删除，再发给 api，再回报给 or
		ReqBody.PostCmntReqbody.CommentBody.Deleted = proto.Uint32(2)

		bodyBuf, err := proto.Marshal(ReqBody)
		if err != nil {
			return false, fmt.Errorf("ReqBody Failed to proto.Marshal err=%s", err.Error())
		}

		infoLog.Println("ProcPostComment()  ", Mid, Userid, *ReqBody.PostCmntReqbody.CommentBody.Deleted)

		return proxyMomentRequest(c, head, bodyBuf, func(p *tcpcommon.HeadV2Packet) {

			pReqBody := &ht_moment.RspBody{}
			err := proto.Unmarshal(p.GetBody(), pReqBody)
			if err != nil {
				infoLog.Printf("proxyMomentRequest() callback error %d,mid=%s,Cid=%s,badword=%s", Userid, Mid, badText)
				return
			}

			pCommentPostBody := pReqBody.GetPostCmntRspbody()

			if pCommentPostBody == nil {
				infoLog.Printf("proxyMomentRequest() callback error %d,mid=%s,Cid=%s,badword=%s", Userid, Mid, badText)
				return
			}

			pCid := string(pCommentPostBody.GetCid())

			saveBannedUseridByAction(Mid, pCid, Userid, MMH_REASON_BAD_WORDS, badText, HandleAction)

			saveBadComment(Mid, Userid, pCid, Text, badText)

		})

	} else {

		//其他的评论,或者无关评论 直接透传
		return proxyMomentRequest(c, head, payLoad, func(p *tcpcommon.HeadV2Packet) {})
	}

	return true, nil
}

func proxyMomentRequest(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte, run func(*tcpcommon.HeadV2Packet)) (bool, error) {

	//其他的评论,或者无关评论 直接透传
	pack, err := momentAPI.SendAndRecvPacket(head, payLoad)

	if err != nil {
		return false, err
	}
	rspPacket, ok := pack.(*tcpcommon.HeadV2Packet)
	if !ok {
		return false, err
	}

	go run(rspPacket)

	return common.SendV2RspBuffer(c, head, rspPacket.GetBody())
}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := common.InitLogAndOption(&options, &infoLog)

	//infoLog = log.New(os.Stdout, "[Info]", log.LstdFlags)

	Mgdb, err = common.InitMySqlFromSection(cfg, infoLog, "MG_MYSQL", 2, 1)
	common.CheckError(err)
	defer Mgdb.Close()

	Masterdb, err = common.InitMySqlFromSection(cfg, infoLog, "MASTER_MYSQL", 2, 1)
	common.CheckError(err)
	defer Masterdb.Close()

	Logdb, err = common.InitMySqlFromSection(cfg, infoLog, "LOGDB_MYSQL", 2, 1)
	common.CheckError(err)
	defer Logdb.Close()

	infoLog.Println("===== init NewTextFiler=======")

	momentTextFilter = common.NewTextFiler(Mgdb, common.CHECK_MOMENT, infoLog)
	commentTextFilter = common.NewTextFiler(Mgdb, common.CHECK_COMMENT, infoLog)

	infoLog.Println("===== init momentApi=======")

	// ali access key info
	access_key_id := cfg.Section("SERVICE_CONFIG").Key("access_key_id").MustString("access_key_id")
	access_key_secret := cfg.Section("SERVICE_CONFIG").Key("access_key_secret").MustString("access_key_secret")

	imageChecker = sdk.NewImageFilter(access_key_id, access_key_secret, infoLog)

	// init moment api
	moment_ip := cfg.Section("SERVICE_CONFIG").Key("moment_ip").MustString("127.0.0.1")
	moment_port := cfg.Section("SERVICE_CONFIG").Key("moment_port").MustString("15500")

	new_moment_ip := cfg.Section("SERVICE_CONFIG").Key("new_moment_ip").MustString("127.0.0.1")
	new_moment_port := cfg.Section("SERVICE_CONFIG").Key("new_moment_port").MustString("15500")

	new_moment_userids := cfg.Section("SERVICE_CONFIG").Key("new_moment_userids").MustString("")

	useridArr := common.SplitStringToUint32Array(new_moment_userids)
	newTestUserid = map[uint32]uint32{}
	for _, v := range useridArr {
		newTestUserid[v] = 1
	}
	infoLog.Println("load new_moment=", new_moment_ip, new_moment_port, "test userid=", newTestUserid)

	forbidden_face = cfg.Section("SERVICE_CONFIG").Key("forbidden_face").MustString("")

	moment_timeout := cfg.Section("SERVICE_CONFIG").Key("moment_timeout").MustUint(8)
	moment_max_conn := cfg.Section("SERVICE_CONFIG").Key("moment_max_conn").MustInt(100)
	moment_url := cfg.Section("SERVICE_CONFIG").Key("moment_url").MustString("http://qtest.hellotalk.org")

	MAX_SLEFIE_SQR = cfg.Section("SERVICE_CONFIG").Key("face_sqrt").MustFloat64(0.04)

	sexyImageScore = cfg.Section("SERVICE_CONFIG").Key("sexy_image_score").MustFloat64(81)
	pornImageScore = cfg.Section("SERVICE_CONFIG").Key("porn_image_score").MustFloat64(90)
	MOMENT_MAX_POST_NUM = cfg.Section("SERVICE_CONFIG").Key("moment_max_post_num").MustInt(9)

	notify_api_host := cfg.Section("SERVICE_CONFIG").Key("notify_api_host").MustString("http://qtest.hellotalk.org")

	v2proto := &tcpcommon.HeadV2Protocol{}
	momentAPI = tcpcommon.NewMntApi(moment_ip, moment_port, time.Duration(moment_timeout)*time.Second, time.Duration(moment_timeout)*time.Second, v2proto, moment_max_conn)

	newMomentAPI = tcpcommon.NewMntApi(new_moment_ip, new_moment_port, time.Duration(moment_timeout)*time.Second, time.Duration(moment_timeout)*time.Second, v2proto, moment_max_conn)

	counter_moment_ip := cfg.Section("SERVICE_CONFIG").Key("counter_moment_ip").MustString("127.0.0.1")
	counter_moment_port := cfg.Section("SERVICE_CONFIG").Key("counter_moment_port").MustString("15500")

	counterMomentAPI = tcpcommon.NewMntApi(counter_moment_ip, counter_moment_port, time.Duration(moment_timeout)*time.Second, time.Duration(moment_timeout)*time.Second, v2proto, moment_max_conn)

	redis_host := cfg.Section("REDIS").Key("ruler_redis_ip").MustString("127.0.0.1")
	redis_port := cfg.Section("REDIS").Key("ruler_redis_port").MustString("6379")

	RulerRedis = tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	redis_host = cfg.Section("REDIS").Key("userinfo_redis_ip").MustString("127.0.0.1")
	redis_port = cfg.Section("REDIS").Key("userinfo_redis_port").MustString("6379")

	UserinfoRedis = tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	userPri = common.NewUserPrivilege(infoLog, Masterdb, RulerRedis, UserinfoRedis, momentAPI, strings.Split(moment_url, ","), notify_api_host)

	userPri.InitSelfieAllowTime()
	//

	nsq_host := cfg.Section("NSQ_CONFIG").Key("host").MustString("10.144.89.163:4050")
	nsqMomentTopic = cfg.Section("NSQ_CONFIG").Key("topic").MustString("post_moment")

	config := nsq.NewConfig()
	nsqPublisher, err = nsq.NewProducer(nsq_host, config)
	common.CheckError(err)
	defer nsqPublisher.Stop()

	infoLog.Println(" =======  NewProducer ======= ", nsq_host, nsqMomentTopic)

	// user info cache api
	userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	infoLog.Printf("user info cache tcpcommonip=%v port=%v", userInfoIp, userInfoPort)
	userInfoCacheApi = tcpcommon.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &tcpcommon.HeadV2Protocol{}, 1000)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	common.CheckError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	common.CheckError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	tcpConfig := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	test()

	srv := gotcp.NewServer(tcpConfig, &Callback{}, &tcpcommon.HeadV2Protocol{})

	// // starts service
	go srv.Start(listener, time.Second)
	// infoLog.Println("listening:", listener.Addr())

	// // catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()

}

//https://promotion.aliyun.com/ntms/act/lvwangdemo.html?spm=5176.doc28428.2.22.HxCIQ4
//测试网址

func test() {

	infoLog.Println("===== init test=======")

	text := "俄经济的\nhttp://qtest.hellotalk.org/s/c/59edba10372cbf7422dce08f?id=zVyPZN=="

	rest, HandeAction, err := momentTextFilter.IsTextIllegally(text, "dfadfa")

	if err != nil {
		infoLog.Println("not found", rest, err, text)
	} else {
		infoLog.Println("found ", HandeAction, rest, "in", text, strings.Contains(text, "line"))
	}

	checkArray := []*sdk.ImageBodyReqbody{}

	item := &sdk.ImageBodyReqbody{
		BigUrl: "http://sz-ht.img-cn-shenzhen.aliyuncs.com/mnt/171115/9000204_e3ccc4ddaf0888ac402a4c012c0a9a64.jpg",
		Width:  295,
		Height: 572,
	}
	checkArray = append(checkArray, item)

	checkTypes := []string{"ad"}

	// checker := sdk.NewImageFilter(access_key_id, access_key_secret, infoLog)

	imageResult, err := imageChecker.DoCheck("900", checkArray, checkTypes)

	infoLog.Println("test() ", imageResult)

}

func HandleMomentWithSelfie(Userid uint32, Mid string, alreadyHide bool, osType uint32, momentBody *ht_moment.MomentBody) {

	//自拍只给粉丝看
	HideMomentForFowllersOnly(Mid, Userid, "selfie", momentBody)

	//infoLog.Printf("HandleMomentWithSelfie() code=%d userid=%d,mid=%s", mmtCode, userid, Mid)

	//通知sdk leanplum

	go func() {
		res, err := userPri.TrackSelfiePost(Userid, osType)

		infoLog.Printf("HandleMomentWithSelfie() Userid=%d,osType=%d,[%s],TrackSelfiePost=[%s]",
			Userid, osType, common.ErrorReturn(err).Error(), res)

	}()

	//自拍不在学习桶 $userid,$mid,$state,$op_type
}

func ProxyPostModMntBucket(Userid uint32, Mid string, BucketType uint32, Reason string, momentBody *ht_moment.MomentBody) (code uint32, err error) {

	_, ok := newTestUserid[Userid]

	if ok {
		// infoLog.Printf("ProxyPostModMntBucket() userid=%d,mid=%s to newMomentApi", userid, Mid)

		// code, err = newMomentAPI.ModMntBucket(Mid, BucketType, Reason, momentBody)
		for i := 0; i < 5000; i++ {
			code, err = PostModMntBucketByNsq(Mid, BucketType, Reason, momentBody)
			// time.Sleep(1 * time.Microsecond)
		}

	} else {

		// infoLog.Printf("ProxyPostModMntBucket() userid=%d,mid=%s to momentAPI", userid, Mid)

		// code, err = momentAPI.ModMntBucket(Mid, BucketType, Reason, momentBody)
		code, err = PostModMntBucketByNsq(Mid, BucketType, Reason, momentBody)
	}

	if err != nil {
		libcomm.AttrAdd(STAT_MOMENT_API_FAIL, 1)
	}

	return
}

func PostModMntBucketByNsq(Mid string, BucketType uint32, opReason string, momentBody *ht_moment.MomentBody) (uint32, error) {

	head := &tcpcommon.HeadV2{
		Version:  tcpcommon.CVerMmedia,
		Cmd:      uint32(ht_moment.CMD_TYPE_CMD_MOD_MNT_BUCKET),
		Seq:      0,
		Ret:      0,
		Reserved: 0,
		Len:      0,
		Uid:      10000,
	}

	reqBody := new(ht_moment.ReqBody)

	opTypeEnum := ht_moment.MntBucketType(BucketType)

	reqBody.ModMntBucketReqbody = &ht_moment.ModMntBucketReqBody{
		Mid:        []byte(Mid),
		BucketType: opTypeEnum.Enum(),
		OpReason:   []byte(opReason),
		Moment:     momentBody,
	}

	pack, err := momentAPI.BuildMomentV2Body(head, reqBody)

	if err != nil {
		return 500, err
	}

	buff := pack.Serialize()

	err = nsqPublisher.Publish(nsqMomentTopic, buff)

	if err != nil {
		return 502, err
	}

	infoLog.Println("PostModMntBucketByNsq() Publish OK ", Mid)

	return 0, err
}

func HideMomentForFowllersOnly(Mid string, Userid uint32, Reason string, momentBody *ht_moment.MomentBody) {

	//code, err := momentAPI.ModMntBucket(Mid, 1, Reason)
	//mmtCode, mmtErr := momentAPI.ModMntStatus(Mid, org.MMT_OP_FOR_FOLLOWER_ONLY, Reason)
	mmtCode, mmtErr := ProxyPostModMntBucket(Userid, Mid, common.INI_TO_FOWLLER, Reason, momentBody)

	if mmtErr != nil {

		infoLog.Printf("HideMomentForFowllersOnly() code=%d userid=%d,mid=%s err=%s,Reason=%s", mmtCode, Userid, Mid, mmtErr.Error(), Reason)
	} else {

		infoLog.Printf("HideMomentForFowllersOnly() code=%d userid=%d,mid=%s,reason=%s", mmtCode, Userid, Mid, Reason)

	}

}

//, momentBody *ht_moment.MomentBody
func HideMoment(Mid string, Userid uint32, Reason string, momentBody *ht_moment.MomentBody, isAlreayHide bool) bool {

	if isAlreayHide == true {
		return true
	}

	//如果要隐藏其实帖子只在自己的桶里面的，这里的操作 其实只是加上理由而已

	//mmtCode, mmtErr := momentAPI.ModMntStatus(Mid, org.MMT_OP_HIDE, Reason)

	//默认已经是只给自己看的情况下，再次设置 只是为了加上理由 和 设置bucket_type 字段
	mmtCode, mmtErr := ProxyPostModMntBucket(Userid, Mid, common.INI_TO_ME, Reason, momentBody)

	if mmtErr != nil {

		infoLog.Printf("HideMoment() code=%d userid=%d,mid=%s,reason=%s,err=%s", mmtCode, Userid, Mid, Reason, mmtErr.Error())
	} else {

		infoLog.Printf("HideMoment() code=%d userid=%d,mid=%s,reason=%s", mmtCode, Userid, Mid, Reason)

	}

	return true

}

//过滤要检测的图片，
//如果有大于7张 只随机检测 3张
//如果大于4张，随机检测2张。
//三张就只检测两张、
//1-2张 全部检测
func getNeedCheckImageArray(Mid string, Userid uint32, CheckAll bool, images []*ht_moment.ImageBody) []*sdk.ImageBodyReqbody {

	checkArray := []*sdk.ImageBodyReqbody{}

	rr := rand.New(rand.NewSource(time.Now().UnixNano()))
	allImageCnt := len(images)

	if allImageCnt <= 2 || CheckAll == true {

		for k, v := range images {

			item := &sdk.ImageBodyReqbody{
				BigUrl: string(v.GetBigUrl()),
				Width:  v.GetWidth(),
				Height: v.GetHeight(),
				Id:     k,
			}
			checkArray = append(checkArray, item)
		}
	} else {

		maxCheck := int(3)

		if allImageCnt < 7 && allImageCnt >= 3 {
			maxCheck = 2
		}

		randIndxs := make(map[int]int, 10)
		choosePos := []int{}

		for i := 0; i < 20; i++ {
			r := rr.Intn(allImageCnt)

			_, ok := randIndxs[r]

			if len(checkArray) < maxCheck && !ok {

				item := &sdk.ImageBodyReqbody{
					BigUrl: string(images[r].GetBigUrl()),
					Width:  images[r].GetWidth(),
					Height: images[r].GetHeight(),
					Id:     r,
				}
				checkArray = append(checkArray, item)

				randIndxs[r] = 1
				choosePos = append(choosePos, r)

			} else {
				if len(checkArray) == maxCheck {
					break
				}
			}
		}

		infoLog.Printf("getNeedCheckImageArray() userid=%d,mid=%s,allImageCnt=%d,checkCnt=%d,Pos=%#v", Userid, Mid, allImageCnt, len(checkArray), choosePos)

	}

	return checkArray
}

// userInfo, err := userInfoCacheApi.GetUserLanguageSetting(userid)

func isNeedCheckAd(Mid string, Userid uint32, userInfo *tcpcommon.CacheLanguageInfo, imageCount int) bool {

	if userInfo == nil {
		return false
	}

	//注册来自 2=weibo,3=tweet,4=facebook 的不检查。
	if userInfo.RegFrom > 1 {
		infoLog.Printf("isNeedCheckAd() %d skip with RegFrom=%d,imageCount=%d", Userid, userInfo.RegFrom, imageCount)
		return false
	}

	//注册电邮被验证的不检查。
	if userInfo.EmailVerify == 4 {
		infoLog.Printf("isNeedCheckAd() %d skip with EmailVerify=%d,imageCount=%d", Userid, userInfo.EmailVerify, imageCount)
		return false
	}

	now := time.Now()
	unixNow := now.Unix()

	secDiff := unixNow - int64(userInfo.RegTime)

	//注册15天以上的用户不检查广告
	if secDiff > 7*86400 {
		infoLog.Printf("isNeedCheckAd() %d skip with secDiff=%d,imageCount=%d", Userid, secDiff, imageCount)
		return false
	}

	mmtCount, likedCount, err := counterMomentAPI.GetUserMomentCountInfo(Userid)

	if err != nil {
		infoLog.Printf("isNeedCheckAd() GetUserMomentCountInfo userid=%d,mid=%s,imageCount=%d err=%s", Userid, Mid, imageCount, err.Error())
		return false
	}

	infoLog.Printf("isNeedCheckAd() GetUserMomentCountInfo userid=%d,mid=%s mmtCount=%d,likedCount=%d,imageCount=%d,secDiff=%d",
		Userid, Mid, mmtCount, likedCount, imageCount, secDiff)

	if mmtCount <= 1 && likedCount == 0 {
		return true
	}

	return false
}

func ShowMomentToAllUsers(Mid string, userid uint32, momentBody *ht_moment.MomentBody, OnlyToMe uint32, userInfo *tcpcommon.CacheLanguageInfo) {

	overLimit := uint32(MOMENT_MAX_POST_NUM)
	if OnlyToMe != 2 {

		code, indexMomentList, err := momentAPI.GetUserIndexedMoment(userid, overLimit)

		if err != nil {
			infoLog.Printf("ShowMomentToAllUsers() GetUserIndexedMoment code=%d,userid=%d,mid=%s err=%s", code, userid, Mid, err.Error())
		}

		if len(indexMomentList) == 1 {

			oneMoment := indexMomentList[0]
			lastPostTime := oneMoment.GetPostTime()
			thisPostTime := momentBody.GetPostTime()

			if userInfo == nil {

				infoLog.Println("ShowMomentToAllUsers() userInfo err=empty")

			} else {
				sameDay := common.IsTwoTimestampSameDay(lastPostTime, thisPostTime, uint64(userInfo.TimeZone_48*1800))

				if sameDay {

					mmtCode, mmtErr := ProxyPostModMntBucket(userid, Mid, common.INI_TO_FOWLLER, fmt.Sprintf("over_%d", overLimit), momentBody)

					if mmtErr != nil {
						infoLog.Printf("ShowMomentToAllUsers() over %d,to fowller, code=%d,userid=%d,mid=%s,OnlyToMe=%d,err=%s", overLimit, mmtCode, userid, Mid, OnlyToMe, mmtErr.Error())
					} else {
						infoLog.Printf("ShowMomentToAllUsers() over %d,to fowller, code=%d,userid=%d,mid=%s,OnlyToMe=%d", overLimit, mmtCode, userid, Mid, OnlyToMe)
					}

					return
				}
			}

		} else {
			infoLog.Printf("ShowMomentToAllUsers() GetUserIndexedMoment code=%d,userid=%d,mid=%s err: mnt count less than 1", code, userid, Mid)
		}

	} else {
		infoLog.Printf("ShowMomentToAllUsers() userid=%d,mid=%s,can send more than %d", userid, Mid, overLimit)
	}

	//Mid string, BucketType uint32, opReason string
	mmtCode, mmtErr := ProxyPostModMntBucket(userid, Mid, common.INI_TO_ALL, "", momentBody)

	if mmtErr != nil {
		infoLog.Printf("ShowMomentToAllUsers() to all code=%d,userid=%d,mid=%s,OnlyToMe=%d,err=%s", mmtCode, userid, Mid, OnlyToMe, mmtErr.Error())
	} else {
		infoLog.Printf("ShowMomentToAllUsers() to all code=%d,userid=%d,mid=%s,OnlyToMe=%d", mmtCode, userid, Mid, OnlyToMe)
	}

}

func SaveBadMoment(
	Mid string,
	Userid uint32,
	OsType uint32,
	PostTime uint64,
	badText string,
	selfieInfo *sdk.SelfieInfoBody,
	QrInfo *sdk.QRInfoBody,
	pornInfo *sdk.PornInfoBody,
	adInfo *sdk.AdInfoBody,
	momentBody *ht_moment.MomentBody,
	OnlyToMe uint32,
	userInfo *tcpcommon.CacheLanguageInfo) error {

	alreadyHide := false
	needSetForFollow := false
	forFollowReason := ""

	IS_BAD_WORDS := uint8(0)

	if badText != "" {
		IS_BAD_WORDS = 1

		if strings.Contains(badText, "line") {

			needSetForFollow = true
			forFollowReason = "line fol"

		} else {

			alreadyHide = HideMoment(Mid, Userid, MMH_REASON_BAD_WORDS, momentBody, alreadyHide)

		}

	}

	//先处理色情
	IMG_CODE := uint8(0)
	HOT_SCORE := uint8(0)
	PORN_SCORE := uint8(0)
	CONFIDENCE := uint8(0)

	if pornInfo != nil {

		//image_code 记录是哪个图片序号。
		if pornInfo.IsPorn == 1 || pornInfo.Rate >= sexyImageScore {

			IMG_CODE = pornInfo.IsPorn

			if pornInfo.IsPorn == 1 {
				alreadyHide = HideMoment(Mid, Userid, MMH_REASON_PORN, momentBody, alreadyHide)
				userPri.BanUserAllAction([]uint32{Userid}, Mid, MMH_REASON_PORN)
				saveBannedUserid(Mid, "", Userid, MMH_REASON_PORN, fmt.Sprintf("%f", pornInfo.Rate))
			} else {

				if pornInfo.Rate >= pornImageScore {
					alreadyHide = HideMoment(Mid, Userid, MMH_REASON_SEXY_HIDE, momentBody, alreadyHide)

				} else {
					needSetForFollow = true
					forFollowReason = MMH_REASON_SEXY_MAYBE
				}

			}

		}

		PORN_SCORE = uint8(pornInfo.Rate)
		HOT_SCORE = uint8(pornInfo.ImgId + 1)
		CONFIDENCE = uint8(pornInfo.Rate)
	}

	//再处理二维码
	IS_QRCODE := uint32(0)
	QR_INFO := ""
	HideByQrCode := false
	if QrInfo != nil {
		IS_QRCODE = QrInfo.QrCode
		qrUrlBytes, _ := json.Marshal(QrInfo.QrUrl)

		QR_INFO = string(qrUrlBytes)

		if QrInfo.AllowedQr == 0 {
			HideByQrCode = true
			alreadyHide = HideMoment(Mid, Userid, MMH_REASON_QRCODE, momentBody, alreadyHide)
		}
	}

	//处理广告
	if adInfo != nil {

		if adInfo.NeedBlock == 1 && HideByQrCode == false {
			ActionTypes := []uint32{common.WORD_BAN_MOMENT, common.WORD_BAN_COMMENT, common.WORD_BAN_REGISTER}
			userPri.HanderUserByActionType(Mid, Userid, ActionTypes, MMH_REASON_AD)
			saveBannedUserid(Mid, "", Userid, MMH_REASON_AD, fmt.Sprintf("%f", adInfo.Rate))
			alreadyHide = HideMoment(Mid, Userid, MMH_REASON_AD, momentBody, alreadyHide)

			if IS_BAD_WORDS == 0 {
				IS_BAD_WORDS = 1
				if adInfo.IsReviewOnly == 1 {
					badText = fmt.Sprintf("review=%v", adInfo.Rate)
				}

			}

		}

		infoLog.Printf("SaveBadMoment() adInfo userid=%d,mid=%s,HideByQrCode=%v,alreadyHide=%v",
			Userid, Mid, HideByQrCode, alreadyHide)
	}

	//最后处理自拍，顺序要严格
	IS_SELFIE := uint8(0)
	if selfieInfo != nil {
		IS_SELFIE = selfieInfo.IsSelfie

		if selfieInfo.IsNeedHide == 1 && alreadyHide == false {

			if selfieInfo.PeopleName != "" && strings.Contains(forbidden_face, selfieInfo.PeopleName) {

				alreadyHide = HideMoment(Mid, Userid, MMH_REASON_PEOPLE, momentBody, alreadyHide)

			} else {

				HandleMomentWithSelfie(Userid, Mid, alreadyHide, OsType, momentBody)

			}

			alreadyHide = true
		}

	}

	if alreadyHide == false && needSetForFollow == true {
		HideMomentForFowllersOnly(Mid, Userid, forFollowReason, momentBody)
		alreadyHide = true
	}

	if alreadyHide == false {
		//上面没有处理。展示给所有人看
		ShowMomentToAllUsers(Mid, Userid, momentBody, OnlyToMe, userInfo)
	}

	// return nil
	r1, err1 := Logdb.Exec("INSERT INTO LOG_BAD_MOMENT (USERID,`OSTYPE`, `MID`, IMG_CODE,HOT_SCORE,PORN_SCORE,CONFIDENCE, IS_BAD_WORDS,  BAD_WORDS,IS_SELFIE,IS_QRCODE,QR_INFO,CREATETIME) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)",
		Userid,
		OsType,
		Mid,
		IMG_CODE,
		HOT_SCORE,
		PORN_SCORE,
		CONFIDENCE,
		IS_BAD_WORDS,
		badText,
		IS_SELFIE,
		IS_QRCODE,
		QR_INFO,
		PostTime,
	)

	if err1 != nil {
		infoLog.Println(Userid, Mid, "SaveBadMoment() LOG_BAD_MOMENT insert failed err=", err1)
		return err1
	}

	id1, _ := r1.LastInsertId()

	infoLog.Println(Userid, Mid, "SaveBadMoment() LOG_BAD_MOMENT ok Id=", id1)

	return nil
}

func saveBadComment(Mid string, Userid uint32, Cid string, Content string, BadWords string) error {

	// return nil
	r1, err1 := Logdb.Exec("INSERT INTO LOG_BAD_COMMENT (MID,USERID,`CID`, `BAD_WORDS`, TEXT,DATETIME)"+
		" VALUES(?,?,?,?,?,UTC_TIMESTAMP())",
		Mid,
		Userid,
		Cid,
		BadWords,
		Content,
	)

	if err1 != nil {
		infoLog.Println(Userid, Mid, "saveBadComment() LOG_BAD_COMMENT insert failed err=", err1, Cid)
		return err1
	}

	id1, _ := r1.LastInsertId()

	infoLog.Printf("saveBadComment() LOG_BAD_COMMENT ok ,logid=%d,Cid=%s,userid=%d,mid=%s", id1, Cid, Userid, Mid)

	return nil

}

func saveBannedUseridByAction(Mid string, Cid string, Userid uint32, Reason string, Content string, BanAction []uint32) error {

	needSave := 0
	for _, v := range BanAction {

		if v == common.WORD_BAN_MOMENT || v == common.WORD_BAN_COMMENT || v == common.WORD_BAN_MESSAGE || v == common.WORD_BAN_REGISTER || v == common.WORD_BAN_USER_LOGIN || v == common.WORD_BAN_DEVICE_LOGIN {
			needSave = 1
		}
	}

	if needSave == 1 {
		return saveBannedUserid(Mid, Cid, Userid, Reason, Content)
	}

	return errors.New("no need to save")

}

func saveBannedUserid(Mid string, Cid string, Userid uint32, Reason string, Content string) error {

	if Cid != "" {
		Mid = Mid + "." + Cid
	}
	// return nil
	r1, err1 := Logdb.Exec("INSERT INTO LOG_AUTO_USER_BANNED (MID,USERID,  `REASON`,`CONTENT`,CREATETIME)"+
		" VALUES(?,?,?,?,UTC_TIMESTAMP())",
		Mid,
		Userid,
		Reason,
		Content,
	)

	if err1 != nil {
		infoLog.Println(Userid, Mid, "saveBannedUserid() LOG_AUTO_USER_BANNED insert failed err=", err1, Cid)
		return err1
	}

	id1, _ := r1.LastInsertId()

	infoLog.Printf("saveBannedUserid() LOG_BAD_COMMENT ok ,logid=%d,Cid=%s,userid=%d,mid=%s", id1, Cid, Userid, Mid)

	return nil

}
