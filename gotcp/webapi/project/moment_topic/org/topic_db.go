// Copyright 2017 Liling
//
// HelloTalk.inc

package org

import (
	"database/sql"
	"strings"
	// "database/sql/errors"
	"errors"
	"fmt"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	sphinx "github.com/lilien1010/sphinx" //经过改造的sphinx go driver
	"log"
	"math/rand"
	"strconv"
	"sync"
	"time"
	// "unicode/utf8"
)

//SphinxDbAPI handle sphinx
//Conn exposes a set of callbacks for the various events that occur on a connection
type TopicDbAPI struct {
	SphinxClient *sphinx.Client
	//MyInfo       *UserResult
	infoLog           *log.Logger
	Db                *sql.DB
	queryIndex        string
	sphinx_tag_index  string
	sphinxMomentIndex string
	dbMomentListTable string

	MaxShowNum       int
	ObjId            int
	tagIdMap         map[string]uint32
	userCacheInfoApi *tcpcommon.UserInfoCacheApi
	tagIdMapLocker   *sync.RWMutex
}

type TagListReqBuckinfo struct {
	Userid  uint32
	TagId   uint64
	TagName string

	IsFirstReq int

	QType ht_moment.QUERY_TYPE

	HotList []uint64 //在bucket body 里面记录 HOT1，HOT2，HOT3

	LatestNoMore  int // NO_LATEST    描述最新发布区域 没有，
	RelatedNoMore int // NO_RELATED  如果相关的都没有了，表示请求应该终止。

	LatestMinMid uint64 // MIN_LATESTID  作为 最新发布区域的翻页

	RelatedMinMid uint64 // MIN_RELATEDID 作为 最新发布区域的翻页

	RelatedTagIdList []uint64 //在body 里面 存储 要过滤tagid，避免翻页的时候要不断去加载更多。
	RelatedIdCount   uint32   //相关Id的数量   RELATED_CNT
}

type TagBody struct {
	Id       uint64 //标签ID
	LangType uint32 //记录语言类型
	IsBad    uint8  //记录是不是有敏感词
}

const TagHashTag = "#"

//帖子的被删除状态
const (
	MS_NORMAL            = 0
	MS_DELETED_BY_SELF   = 1
	MS_HIDE              = 2
	MS_DELETED_BY_SYSTEM = 3

	TAG_MID_HOT     = 1
	TAG_MID_LATEST  = 2
	TAG_MID_RELATED = 3

	//对贴文的操作的描述
	NSQ_EVENT_LIKE               = 1
	NSQ_EVENT_CANCLE_LIKE        = 2
	NSQ_EVENT_COMMENT            = 3
	NSQ_EVENT_DELETE_COMMENT     = 4
	NSQ_EVENT_REPORT             = 5 //暂时没做。
	NSQ_EVENT_FAVORITE           = 6
	NSQ_EVENT_SHARE              = 7
	NSQ_EVENT_BUCKET_TO_ME       = 8
	NSQ_EVENT_BUCKET_TO_FOLLOW   = 9
	NSQ_EVENT_BUCKET_TO_ALL      = 10
	NSQ_EVENT_OP_RESTORE         = 11
	NSQ_EVENT_OP_HIDE            = 12
	NSQ_EVENT_OP_DELETE          = 13
	NSQ_EVENT_OP_FOLLOWER_ONLY   = 14
	NSQ_EVENT_DELETE_USER        = 15
	NSQ_EVENT_RESTORE_USER       = 16
	NSQ_EVENT_DELETE_MNT_BY_USER = 17
	NSQ_EVENT_CANCLE_FAVORITE    = 18
)

// 最终的输出结果
type MomentTagResult struct {
	TagId    uint32
	Name     string
	LangType uint32
	Type     uint32
}

//NewSphinxDbAPI 初始化一个sphinx 客户端对象
func NewTopicDbAPI(destIP string, destPort int, queryIndex string, sphinxMomentIndex string, MilliTimeOut int, openCount int, idleCount int,
	Masterdb *sql.DB,
	userCacheInfoApi *tcpcommon.UserInfoCacheApi,
	logger *log.Logger) (topicAPI *TopicDbAPI) {

	sc := sphinx.NewClient().SetServer(destIP, destPort).SetConnectTimeout(MilliTimeOut).SetConnectDuration(1000).SetRetries(2, 1)

	if err := sc.Error(); err != nil {
		// return nil, err
		logger.Println("NewTopicDbAPI() SphinxClient NewClient fail,err=", err)
	}

	if err := sc.Open(); err != nil {
		// return nil, err
		logger.Println("NewTopicDbAPI() SphinxClient Open fail,err=", err)
	}

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	ObjId := r.Intn(10000)

	logger.Println("NewTopicDbAPI() start,ObjId=", ObjId)

	if err := sc.GetDb(MilliTimeOut/1000, openCount, idleCount); err != nil {
		// return nil, err
		logger.Println("NewTopicDbAPI() SphinxClient GetDb fail,err=", err)

	}

	sc.SetMaxQueryTime(3)

	api := &TopicDbAPI{
		SphinxClient:      sc,
		Db:                Masterdb,
		queryIndex:        queryIndex,
		infoLog:           logger,
		sphinxMomentIndex: sphinxMomentIndex,
		MaxShowNum:        300,
		ObjId:             ObjId,
		userCacheInfoApi:  userCacheInfoApi,
		tagIdMap:          map[string]uint32{},
		tagIdMapLocker:    new(sync.RWMutex),
	}

	return api
}

func (this *TopicDbAPI) SetTagedDbTable(Name string) {

	this.dbMomentListTable = Name

}

//Filter 搜索sphinx 过滤器入口
// http://sphinxsearch.com/wiki/doku.php?id=charset_tables  ngram_chars 设置
// ngram_chars #要進行一元字元切分模式認可的有效字元集。
// sphinxSql 必须依靠 sphinx 2.2.11才可以。 并且 dict 模式得是 keywords
func (this *TopicDbAPI) FilterTagByName(Userid uint32, Text string, appendCount int) (foundExact bool, results []*MomentTagResult, err error) {
	//tag_id | lang_type | type | name | status | score

	//这里要区分 如果是中日韩 就不要 *，如果是英文 就加上*

	query := fmt.Sprintf("SELECT tag_id,lang_type,type,name from %s WHERE match(%s) and status=1 ORDER BY  score DESC limit %d", this.queryIndex, sphinx.QuoteStr(Text+"*"), appendCount)

	// rows, err := this.SphinxClient.DB.Query("select tag_id,lang_type,type,name from ?"+
	// 	" where match('?') and status=1 order by score desc", this.queryIndex, Text)

	rows, err := this.SphinxClient.DB.Query(query)

	if err != nil {
		return false, nil, err
	}
	tagId := uint32(0)
	langType := uint32(0)
	Type := uint32(0)
	Name := ""

	foundTagMap := map[uint32]*MomentTagResult{}
	foundTagList := []*MomentTagResult{}

	for rows.Next() {

		if err := rows.Scan(&tagId, &langType, &Type, &Name); err != nil {
			this.infoLog.Printf("FilterBySql() rows.Scan failed")
			continue
		}

		item := &MomentTagResult{
			TagId:    tagId,
			LangType: langType,
			Type:     Type,
			Name:     Name,
		}

		this.infoLog.Println("FilterBySql() res=", Userid, tagId, langType, Type, Name)

		foundTagMap[tagId] = item
		foundTagList = append(foundTagList, item)

		if Name == Text {
			foundExact = true
		}
	}
	rows.Close()

	//是否找到一模一样的 标签
	if len(this.tagIdMap) > 0 {

		this.tagIdMapLocker.Lock()
		defer this.tagIdMapLocker.Unlock()

		for _name, _tagid := range this.tagIdMap {

			if strings.Contains(_name, Text) {

				_, ok := foundTagMap[_tagid]
				//不存在才写入
				if !ok {

					item := &MomentTagResult{
						TagId:    _tagid,
						LangType: langType,
						Type:     Type,
						Name:     _name,
					}
					foundTagMap[_tagid] = item
					foundTagList = append(foundTagList, item)
					this.infoLog.Printf("FilterBySql() insert from tagIdMap,userid=%d,name=%s,tagid=%d", Userid, _name, _tagid)
				} else {
					//存在就删掉
					delete(this.tagIdMap, _name)
					this.infoLog.Printf("FilterBySql() triggered deleted,userid=%d,name=%s,tagid=%d", Userid, _name, _tagid)
				}
			}
		}

	}

	//resList := []*MomentTagResult{}

	// for _, v := range foundTagList {

	// 	val, ok := foundTagMap[v]
	// 	if ok {
	// 		resList = append(resList, val)
	// 	}
	// }

	return foundExact, foundTagList, nil
}

//二进制协议访问 sphinx 会被断开连接，所以只支持 FilterBySql 模式
//下面的函数尽量不要使用。
func (this *TopicDbAPI) Filter(Name string) (results []MomentTagResult, err error) {

	Len := len([]rune(Name))

	if Len < 2 {
		return nil, errors.New("too short")
	}

	this.SphinxClient.SetFilter("status", []uint64{1}, false)

	this.SphinxClient.SetMatchMode(sphinx.SPH_MATCH_EXTENDED2)
	this.SphinxClient.SetSortMode(sphinx.SPH_SORT_ATTR_DESC, "score")

	query := Name

	this.SphinxClient.SetLimits(0, this.MaxShowNum, 500, 0)

	this.SphinxClient.SetSelect("*")
	res, err := this.SphinxClient.Query(query, this.queryIndex, "Test Query()")

	// 失败的时候，重试
	if err != nil {

		this.infoLog.Printf("Query() failed,err=%s,name=%s ObjId=%d", err.Error(), Name, this.ObjId)

		closeErr := this.SphinxClient.Close()

		if closeErr != nil {
			this.infoLog.Printf("Close() failed,err=%s,name=%s ObjId=%d", closeErr.Error(), Name, this.ObjId)
		}

		openErr := this.SphinxClient.Open()

		if openErr != nil {

			this.infoLog.Printf("ReOpen() failed,err=%s,name=%s ObjId=%d", closeErr.Error(), Name, this.ObjId)

			return nil, err
		} else {
			this.infoLog.Printf("ReOpen() OK,name=%s ObjId=%d", Name, this.ObjId)
			res, err = this.SphinxClient.Query(query, this.queryIndex, "Test Query()")
			if err != nil {
				return nil, err
			}
		}

	}

	this.SphinxClient.ResetFilters()
	this.SphinxClient.ResetGroupBy()

	if res.Total == 0 {
		return nil, errors.New("no result for tag=" + Name)
	}

	this.infoLog.Println("Filter() ", Name, ",Time:", res.Time, ",Warning:", res.Warning, ",Status:", res.Status, ",TotalFound:", res.TotalFound, ",Total:", res.Total)

	list := changeMatched2TopicList(res)

	return list, nil
}

func (this *TopicDbAPI) addTagToMap(TagIdMaps *map[string]uint32, oneName string, TagId uint32, status uint32) {

	if status == 0 {
		this.infoLog.Printf("addTagToMap() hide tag Name=%s,TagId=%d,status=%d", oneName, TagId, status)
		return
	}

	(*TagIdMaps)[oneName] = TagId

	this.tagIdMapLocker.Lock()
	this.tagIdMap[oneName] = TagId
	this.tagIdMapLocker.Unlock()

}

//将数据库的默认id作为 标签id。
func (this *TopicDbAPI) UpdateTagId(Userid uint32, oneName string, newId uint32) {

	strNewSql := fmt.Sprintf("UPDATE MMT_TAGS SET tag_id =%d,update_time=UTC_TIMESTAMP() WHERE name = ?", newId)
	_, err := this.Db.Exec(strNewSql, oneName)

	if err != nil {
		this.infoLog.Printf("AddTag() update MMT_TAGS fail,err=%s,Name=%s,Userid=%d,newId=%d", err.Error(), oneName, Userid, newId)
	}
}

//添加标签
func (this *TopicDbAPI) AddTag(Userid uint32, Names map[string]*TagBody) (idMap map[string]uint32, err error) {

	TagIdMaps := map[string]uint32{}

	if len(Names) == 0 {
		return TagIdMaps, errors.New("empty new Names")
	}

	TagType := uint32(0) //0:用户,1:官方

	TagId := uint32(0)
	recordId := uint32(0)
	oneName := ""

	for v, tagInfo := range Names {

		oneName = strings.TrimLeft(v, TagHashTag)

		val, ok := this.tagIdMap[oneName]

		if ok {
			this.infoLog.Printf("AddTag() load tagId=%d from cache Name=%s,Userid=%d", val, oneName, Userid)
			TagIdMaps[oneName] = val
			continue
		}

		status := uint32(1)

		if tagInfo.IsBad == 1 {
			status = 0
		}
		res, err := this.Db.Exec("INSERT INTO MMT_TAGS(`tag_id`,`lang_type`,`type`,`name`,`status`,create_uid,update_time,create_time) "+
			" VALUES(?,?,?,?,?,?,UTC_TIMESTAMP(),UTC_TIMESTAMP());",
			0,
			tagInfo.LangType,
			TagType,
			oneName,
			status,
			Userid)

		if err != nil {

			mysqlerr, ok := err.(*mysql.MySQLError)

			this.infoLog.Printf("AddTag() db failed,err=%s,Name=%s,Userid=%d", err.Error(), oneName, Userid, ok, mysqlerr.Number)

			//如果数据库里面存在了
			if ok && mysqlerr.Number == uint16(1062) {

				err = this.Db.QueryRow("SELECT id,tag_id from MMT_TAGS WHERE `name`= ? ", oneName).Scan(&recordId, &TagId)

				if err != nil {
					//失败了用crc32 替代
					this.addTagToMap(&TagIdMaps, oneName, common.Crc32(oneName), status)
					this.infoLog.Printf("AddTag() load tag_id from db failed,err=%s,Name=%s,Userid=%d", err.Error(), oneName, Userid)

				} else {
					this.infoLog.Printf("AddTag() db exist,load tagid=%d,Name=%s,Userid=%d", TagId, oneName, Userid)
					if TagId == 0 {
						TagId = recordId
						this.UpdateTagId(Userid, oneName, TagId)
					}
					this.addTagToMap(&TagIdMaps, oneName, TagId, status)
				}

			} else {

				this.infoLog.Printf("insert MMT_TAGS faield,Userid=%d Name=%s error=%s", Userid, oneName, err.Error())
				TagIdMaps[oneName] = common.Crc32(oneName)
				//return TagIdMaps, fmt.Errorf("insert MMT_TAGS faield,Userid=%d Name=%s error=%s", Userid, oneName, err.Error())
			}

			continue

		} else {

			TagIdMaps[oneName] = common.Crc32(oneName)

			newId, err := res.LastInsertId()

			this.UpdateTagId(Userid, oneName, uint32(newId))

			if err != nil {
				this.infoLog.Printf("AddTag() to database fail,err=%s,status=%d,Name=%s,Userid=%d", err.Error(), status, oneName, Userid)
			} else {

				this.addTagToMap(&TagIdMaps, oneName, uint32(newId), status)

				this.infoLog.Printf("AddTag() to database id=%d,status=%d,Name=%s,Userid=%d", newId, status, oneName, Userid)
			}

		}

	}

	//更新数据库的实时索引
	// err = this.SetTagToSphinx(TagIdMaps)

	// if err != nil {
	// 	this.infoLog.Printf("SetTagToSphinx() fail,err=%s,Name=%s,Userid=%d", err.Error(), Userid)
	// }

	return TagIdMaps, nil
}

//实时更新标签里面的得分
func (this *TopicDbAPI) IncTagSphinxScore(Userid uint32, TagIdMaps map[string]uint32) (err error) {

	if len(TagIdMaps) == 0 {
		return errors.New("empty map")
	}

	score := uint32(0)

	for name, tagId := range TagIdMaps {

		querySql := fmt.Sprintf("select score from %s where tag_id = %d  and status=1 limit 1 ", this.queryIndex, tagId)

		err = this.SphinxClient.DB.QueryRow(querySql).Scan(&score)

		if err != nil {
			score = 0
			this.infoLog.Printf("IncTagSphinxScore() get sphinx fail,err=%s,tag_id=%d,name=%s,Userid=%d,", err.Error(), tagId, name, Userid)
			continue
		}

		strNewSql := fmt.Sprintf("UPDATE %s SET score=%d WHERE tag_id = %d", this.queryIndex, score+1, tagId)
		_, err = this.SphinxClient.DB.Exec(strNewSql)
		score = 0

		if err != nil {
			this.infoLog.Printf("IncTagSphinxScore() update sphinx fail,err=%s,tag_id=%d,name=%s,Userid=%d",
				err.Error(), tagId, name, Userid)
			continue
		}

	}

	return nil
}

func (this *TopicDbAPI) GetTagNameByTagId(tagId uint64) (string, error) {

	querySql := fmt.Sprintf("SELECT `name`,`lang_type`,`type` MMT_TAGS WHERE tag_id = %v limit 1", tagId)

	Name := ""
	lang_type := uint32(0)
	typ := uint32(0)

	err := this.Db.QueryRow(querySql).Scan(&Name, &lang_type, &typ)

	if err != nil {
		return "#", err
	}

	return Name, nil
}

func (this *TopicDbAPI) IncTagDbScore(Userid uint32, TagIdMaps map[string]uint32) (err error) {

	if len(TagIdMaps) == 0 {
		return errors.New("empty map")
	}

	tagsArr := []string{}

	for _, v := range TagIdMaps {
		tagsArr = append(tagsArr, fmt.Sprintf("%d", v))
	}

	tagStr := strings.Join(tagsArr, ",")

	strNewSql := fmt.Sprintf("UPDATE MMT_TAGS SET score=score+1,update_time=UTC_TIMESTAMP() WHERE tag_id in (%s)", tagStr)
	_, err = this.Db.Exec(strNewSql)

	if err != nil {
		this.infoLog.Printf("IncTagDbScore() update MMT_TAGS fail,Userid=%d,tagsArr=%s,err=%s", Userid, tagStr, err.Error())
		return
	} else {
		this.infoLog.Printf("IncTagDbScore() update MMT_TAGS OK,Userid=%d,tagsArr=%s", Userid, tagStr)
	}

	return
}

//把标签写入到sphinx的实时索引
func (this *TopicDbAPI) SetTagToSphinx(TagIdMaps map[string]uint32) error {

	return nil
}

func changeMatched2TopicList(result *sphinx.Result) (userlistp []MomentTagResult) {

	resList := make([]MomentTagResult, result.Total)

	for _id, item := range result.Matches {

		for k1, it1 := range item.AttrValues {

			switch result.AttrNames[k1] {
			case "tag_id":
				resList[_id].TagId = uint32(it1.(uint32))
			case "lang_type":
				resList[_id].LangType = uint32(it1.(uint32))
			case "type":
				resList[_id].Type = uint32(it1.(uint32))
			case "name":
				resList[_id].Name = it1.(string)
			}
		}

		if resList[_id].TagId == 0 {
			resList[_id].TagId = uint32(item.DocId)
		}
	}

	return resList
}

//用户新发帖 ,默认只给自己看 deleted的状态控制只给自己看
func (this *TopicDbAPI) PostNewTagedMoment(userLangInfo *tcpcommon.CacheLanguageInfo, MomentBody *ht_moment.MomentBody) error {

	Mid := MomentBody.GetMid()
	Userid := MomentBody.GetUserid()

	if Userid == 0 {
		return errors.New("bad userid")
	}

	tagIdList := []uint32{}

	Tags := MomentBody.GetTags()
	tagStr := ""
	for _, v := range Tags {
		tagIdList = append(tagIdList, uint32(v.GetId()))
	}

	if len(tagIdList) == 0 {
		return errors.New("no tag")
	}

	tagStr = common.JoinUint32(tagIdList, ",")

	//默认用 只给自己看来区分。
	deleted := 2
	langType := MomentBody.GetLangType()
	postTime := MomentBody.GetPostTime()

	nativeLang := userLangInfo.BuildTeachArray()
	learnLang := userLangInfo.BuildLearnArray()

	nativeStr := common.JoinUint32(nativeLang, ",")
	learnStr := common.JoinUint32(learnLang, ",")

	r1, err := this.Db.Exec("INSERT IGNORE "+this.dbMomentListTable+
		" ( mid,userid,tag_id,lang_type,user_nation,user_sex,user_native,user_learn,deleted,is_new,post_time  ) "+
		"VALUES(?,?,?,?,?,?,?,?,?,?,? )",
		Mid,
		Userid,
		tagStr,
		langType,
		common.GetCountryCode(userLangInfo.Nationality),
		userLangInfo.Sex,
		nativeStr,
		learnStr,
		deleted,
		1,
		postTime)

	if err != nil {
		this.infoLog.Printf("PostNewTagedMoment() fail,db=%s,userid=%d,mid=%s,err=%s", this.dbMomentListTable, Userid, Mid, err.Error())
		return err
	} else {
		rowaf, _ := r1.RowsAffected()
		this.infoLog.Printf("PostNewTagedMoment() ok,db=%s,userid=%d,mid=%s,rowaf=%d", this.dbMomentListTable, Userid, Mid, rowaf)
	}

	insertSql := fmt.Sprintf("INSERT INTO  %s (id,userid, tag_id,user_nation,user_sex,user_native,user_learn,deleted, post_time) VALUES(%s,%d,(%s),%d,%d,(%s),(%s),%d,%v)",
		this.sphinxMomentIndex,
		Mid,
		Userid,
		tagStr,
		common.GetCountryCode(userLangInfo.Nationality),
		userLangInfo.Sex,
		nativeStr,
		learnStr,
		deleted,
		postTime,
	)

	r1, err = this.SphinxClient.DB.Exec(insertSql)

	if err != nil {
		this.infoLog.Printf("PostNewTagedMoment() fail,index=%s fail,userid=%d,mid=%s,err=%s", this.sphinxMomentIndex, Userid, Mid, err.Error())
		return err
	} else {
		rowaf, _ := r1.RowsAffected()
		this.infoLog.Printf("PostNewTagedMoment() ok,index=%s,userid=%d,mid=%s,rowaf=%d", this.sphinxMomentIndex, Userid, Mid, rowaf)
	}

	return nil
}

//用户对帖子操作,修改得分计数器, fromSrc= [like,comment,share,collect]
func (this *TopicDbAPI) ModMomentScore(fromSrc string, Mid string, Userid uint32, score int) error {

	strUpdateSql := ""

	if score >= 0 {

		strUpdateSql = fmt.Sprintf("UPDATE %s SET %s_cnt=%s_cnt+%d  WHERE mid=%s ", this.dbMomentListTable, fromSrc, fromSrc, score, Mid)

	} else {
		strUpdateSql = fmt.Sprintf("UPDATE %s SET %s_cnt=%s_cnt %d  WHERE mid=%s AND %s_cnt>0", this.dbMomentListTable, fromSrc, fromSrc, score, Mid, fromSrc)
	}

	r1, err := this.Db.Exec(strUpdateSql)

	if err != nil {

		this.infoLog.Printf("ModMomentScore() update %s fail,err=%s,Mid=%s,fromSrc=%s,Userid=%d,strUpdateSql=%s",
			this.dbMomentListTable, err.Error(), Mid, fromSrc, Userid, strUpdateSql)

	} else {

		rowaf, _ := r1.RowsAffected()
		this.infoLog.Printf("ModMomentScore() update %s ok,Mid=%s,fromSrc=%s,Userid=%d,rowaf=%d,score=%d",
			this.dbMomentListTable, Mid, fromSrc, Userid, rowaf, score)

		this.IncSphinxMomentScore(fromSrc, Mid, Userid, score)

		if rowaf == 0 {
			return errors.New("not_exist")
		}
	}

	return nil
}

//实时更新帖子里面的得分
func (this *TopicDbAPI) IncSphinxMomentScore(fromSrc string, Mid string, Userid uint32, incScore int) (err error) {

	querySql := fmt.Sprintf("SELECT %s_cnt FROM %s where  id = %s LIMIT 1 ", fromSrc, this.sphinxMomentIndex, Mid)

	score := int(0)

	err = this.SphinxClient.DB.QueryRow(querySql).Scan(&score)

	if err != nil {
		score = 0
		this.infoLog.Printf("IncSphinxMomentScore() get %s sphinx fail,err=%s,Mid=%s,fromSrc=%s,Userid=%d,score=%d,incScore=%d",
			this.sphinxMomentIndex, err.Error(), Mid, fromSrc, Userid, score, incScore)
		return
	}

	if score+incScore < 0 {
		this.infoLog.Printf("IncSphinxMomentScore()  update sphinx %s fail,Mid=%d,err=%s can't be negetive,Userid=%d", this.sphinxMomentIndex, Mid, fromSrc, Userid)
		return
	}

	strNewSql := fmt.Sprintf("UPDATE %s SET %s_cnt=%d WHERE  id = %s", this.sphinxMomentIndex, fromSrc, score+incScore, Mid)
	r1, err := this.SphinxClient.DB.Exec(strNewSql)

	if err != nil {
		this.infoLog.Printf("IncSphinxMomentScore() update %s fail,err=%s,Mid=%s,fromSrc=%s,Userid=%d,score=%d,incScore=%d",
			this.sphinxMomentIndex, err.Error(), Mid, fromSrc, Userid, score, incScore)
	} else {

		rowaf, _ := r1.RowsAffected()
		this.infoLog.Printf("IncSphinxMomentScore() update %s ok,Mid=%s,fromSrc=%s,Userid=%d,rowaf=%d,score=%d,incScore=%d",
			this.sphinxMomentIndex, Mid, fromSrc, Userid, rowaf, score, incScore)

		// this.infoLog.Printf("IncSphinxMomentScore() querySql=%s,strNewSql=%s", querySql, strNewSql)

	}

	return nil
}

//跟新帖子的deleted状态
func (this *TopicDbAPI) ChangeMomentStatus(fromSrc string, Type uint32, Mid string, Userid uint32, deleted uint) (err error) {

	if Type == NSQ_EVENT_BUCKET_TO_FOLLOW || Type == NSQ_EVENT_BUCKET_TO_ALL {
		//异步操作的时候可能用户的贴文还没有入库，就已经发出来了。
		time.Sleep(10 * time.Millisecond)

	}

	strUpdateSql := fmt.Sprintf("UPDATE %s SET deleted=%d  WHERE mid=%s", this.dbMomentListTable, deleted, Mid)
	r1, err := this.Db.Exec(strUpdateSql)

	if err != nil {
		this.infoLog.Printf("ChangeMomentStatus() update %s fail,err=%s,fromSrc=%s,Mid=%s,deleted=%d", this.dbMomentListTable, err.Error(), fromSrc, Mid, deleted)

	} else {
		rowaf, _ := r1.RowsAffected()
		this.infoLog.Printf("ChangeMomentStatus() update %s ok,fromSrc=%s,Mid=%s,rowaf=%d,deleted=%d",
			this.dbMomentListTable, fromSrc, Mid, rowaf, deleted)
	}

	_, err = this.ChangeMomentSphinxStatus(fromSrc, Mid, Userid, deleted)

	return err
}

//跟新帖子的deleted状态
func (this *TopicDbAPI) ChangeMomentSphinxStatus(fromSrc string, Mid string, Userid uint32, deleted uint) (rowaf int64, err error) {

	strUpdateSql := fmt.Sprintf("UPDATE %s SET deleted=%d  WHERE id=%s", this.sphinxMomentIndex, deleted, Mid)
	r1, err := this.SphinxClient.DB.Exec(strUpdateSql)
	if err != nil {
		this.infoLog.Printf("ChangeMomentSphinxStatus() update %s fail,err=%s,fromSrc=%s,Mid=%s,deleted=%d", this.sphinxMomentIndex,
			err.Error(), fromSrc, deleted, Mid)
		return 0, err
	} else {
		rowaf, _ := r1.RowsAffected()
		this.infoLog.Printf("ChangeMomentSphinxStatus() update %s ok,fromSrc=%s,Mid=%s,rowaf=%d,deleted=%d",
			this.sphinxMomentIndex, fromSrc, Mid, rowaf, deleted)

		return rowaf, nil
	}

}

//delete tag
func (this *TopicDbAPI) DeleteTag(name string, tagId uint32) error {

	strNewSql := fmt.Sprintf("UPDATE %s SET status=0 WHERE tag_id = %d and status=1", this.queryIndex, tagId)
	_, err := this.SphinxClient.DB.Exec(strNewSql)

	if err != nil {
		this.infoLog.Printf("DeleteTag() update sphinx fail,err=%s,tag_id=%d,name=%s,Userid=%d", err.Error(), tagId, name)

	}

	return nil
}

func MidArrayToMomentIdResult(MidList []uint64, level uint32) []*ht_moment.MomentIdResult {

	IdList := []*ht_moment.MomentIdResult{}

	for _, v := range MidList {
		IdList = append(IdList, &ht_moment.MomentIdResult{
			Mid:   []byte(strconv.FormatUint(v, 10)),
			Level: proto.Uint32(level),
		})
	}

	return IdList
}

//
func (this *TopicDbAPI) GetMidListByTag(Userid uint32,
	userLangInfo *tcpcommon.CacheLanguageInfo,
	lastReqInfo *TagListReqBuckinfo, firstPageCount int) (arr []*ht_moment.MomentIdResult, p *TagListReqBuckinfo, perr error) {

	rspBucketInfo := &TagListReqBuckinfo{
		HotList:          []uint64{},
		RelatedTagIdList: []uint64{},
	}

	*rspBucketInfo = *lastReqInfo

	nativeLang := userLangInfo.BuildTeachArray()
	learnLang := userLangInfo.BuildLearnArray()

	nativeStr := common.JoinUint32(nativeLang, ",")
	learnStr := common.JoinUint32(learnLang, ",")

	var momentIdResult []*ht_moment.MomentIdResult

	hotPageCount := 3

	//得到最热门的帖
	hotestMidArr, err := this.getHotestMidArray(Userid, rspBucketInfo, nativeStr, learnStr, hotPageCount)
	if err != nil {
		this.infoLog.Println("GetMidListByTag()  getHotestMidArray err:", err)
	} else {
		momentIdResult = MidArrayToMomentIdResult(hotestMidArr, TAG_MID_HOT)
		this.infoLog.Println("GetMidListByTag() after getHotestMidArray ", Userid, lastReqInfo.TagId, hotestMidArr)
	}

	//得到最新的贴文
	latestMidArr, err := this.getLastestMidArray(Userid, rspBucketInfo, nativeStr, learnStr, firstPageCount-len(momentIdResult))

	if err != nil {
		this.infoLog.Println("GetMidListByTag()  getLastestMidArray err:", err)
	} else {
		momentIdResult = append(momentIdResult, MidArrayToMomentIdResult(latestMidArr, TAG_MID_LATEST)...)
		this.infoLog.Println("GetMidListByTag() after getLastestMidArray ", Userid, lastReqInfo.TagId, latestMidArr)
	}

	if len(momentIdResult) >= firstPageCount {
		return momentIdResult, rspBucketInfo, nil
	}

	//得到相关标签的贴文
	RelatedMidArr, err := this.getRelatedMidArray(Userid, rspBucketInfo, nativeStr, learnStr, firstPageCount-len(momentIdResult))

	if err != nil {
		this.infoLog.Println("GetMidListByTag()  getRelatedMidArray err:", err)
	} else {
		momentIdResult = append(momentIdResult, MidArrayToMomentIdResult(RelatedMidArr, TAG_MID_RELATED)...)
		this.infoLog.Println("GetMidListByTag() after getRelatedMidArray ", Userid, lastReqInfo.TagId, RelatedMidArr)
	}

	return momentIdResult, rspBucketInfo, nil
}

//获取最热门的贴文
func (this *TopicDbAPI) getHotestMidArray(Userid uint32, lastReqInfo *TagListReqBuckinfo, nativeStr string, learnStr string, appendCount int) ([]uint64, error) {

	MidArr := []uint64{}

	//只有首次请求才返回
	if lastReqInfo.IsFirstReq != 1 {
		return MidArr, errors.New("no need to add")
	}

	whereStr := getQtypeWhereStr(lastReqInfo.QType, lastReqInfo.Userid, []uint64{lastReqInfo.TagId}, nativeStr, learnStr, 0)

	calScore := " LOG10(commented_cnt*3+liked_cnt*1+ shared_cnt*2 + collected_cnt*2) + ((post_time-1482726335000)/45000000) as score2"

	query := fmt.Sprintf("SELECT id,IF( deleted = 0 OR (userid = %d and deleted = 2) OR (userid = %d and deleted = 10),1,0) as open_to_me,%s  FROM %s WHERE %s ORDER BY score2 DESC LIMIT %d",
		Userid,
		Userid,
		calScore,
		this.sphinxMomentIndex,
		whereStr,
		appendCount)

	rows, err := this.SphinxClient.DB.Query(query)

	if err != nil {
		lastReqInfo.LatestNoMore = 1
		return MidArr, errors.New(err.Error() + ",no content,query=" + query)
	}

	this.infoLog.Printf("getHotestMidArray() uid=%d,query=%s ", Userid, query)

	Mid := uint64(0)
	openToMe := uint32(0)
	score2 := float64(0)

	for rows.Next() {
		if err := rows.Scan(&Mid, &openToMe, &score2); err != nil {
			this.infoLog.Printf("getHotestMidArray() Scan top rows.Scan failed ", err)
			continue
		}
		lastReqInfo.HotList = append(lastReqInfo.HotList, Mid)
	}
	rows.Close()

	if len(lastReqInfo.HotList) < appendCount {
		lastReqInfo.LatestNoMore = 1
	}
	return lastReqInfo.HotList, nil
}

//获取最新贴文
func (this *TopicDbAPI) getLastestMidArray(Userid uint32, lastReqInfo *TagListReqBuckinfo, nativeStr string, learnStr string, appendCount int) ([]uint64, error) {
	MidArr := []uint64{}

	//历史信息证明一件不存在更新的贴文了。
	if lastReqInfo.LatestNoMore == 1 {
		return MidArr, errors.New("no more content")
	}

	whereStr := getQtypeWhereStr(lastReqInfo.QType, lastReqInfo.Userid, []uint64{lastReqInfo.TagId}, nativeStr, learnStr, lastReqInfo.LatestMinMid)

	if lastReqInfo.LatestMinMid != 0 {
		whereStr = fmt.Sprintf("%s AND id < %d ", whereStr, lastReqInfo.LatestMinMid)
	}

	if len(lastReqInfo.HotList) > 0 {
		whereStr = fmt.Sprintf("%s AND id NOT IN (%s) ", whereStr, common.JoinUint64(lastReqInfo.HotList, ","))
	}

	query := fmt.Sprintf("SELECT id,IF( deleted = 0 OR (userid = %d and deleted = 2) OR (userid = %d and deleted = 10),1,0) as open_to_me   FROM %s WHERE %s ORDER BY post_time DESC LIMIT %d",
		Userid,
		Userid,
		this.sphinxMomentIndex,
		whereStr,
		appendCount)

	rows, err := this.SphinxClient.DB.Query(query)

	this.infoLog.Printf("getHotestMidArray() uid=%d,query=%s ", Userid, query)

	if err != nil {
		lastReqInfo.LatestNoMore = 1
		if err == sql.ErrNoRows {
			return MidArr, errors.New("no content,query=" + query)
		}
		return MidArr, err
	}

	defer rows.Close()

	foundCount := 0
	Mid := uint64(0)
	openToMe := uint32(0)

	for rows.Next() {
		if err := rows.Scan(&Mid, &openToMe); err != nil {
			this.infoLog.Printf("GetMidListByTag() Scan latest rows.Scan failed", err)
			continue
		}
		foundCount++

		MidArr = append(MidArr, Mid)
	}
	if len(MidArr) > 0 {
		lastReqInfo.LatestMinMid = MidArr[len(MidArr)-1]
	}

	if len(MidArr) < appendCount {
		lastReqInfo.LatestNoMore = 1
	}

	return MidArr, nil
}

//获取相关的贴文
func (this *TopicDbAPI) getRelatedMidArray(Userid uint32, lastReqInfo *TagListReqBuckinfo, nativeStr string, learnStr string, appendCount int) ([]uint64, error) {

	MidArr := []uint64{}

	//历史信息证明一件不存在相关贴文了
	if lastReqInfo.RelatedNoMore == 1 {
		return MidArr, errors.New("no more content")
	}

	//历史信息证明一件不存在相关贴文了
	if lastReqInfo.TagName == "" {
		return MidArr, errors.New("tag name empty")
	}

	//历史信息证明一件不存在相关贴文了
	if len(lastReqInfo.RelatedTagIdList) == 0 {
		relatedTagId, err := this.GetRelatedTagidList(Userid, lastReqInfo.TagId, lastReqInfo.TagName)
		if err != nil {
			this.infoLog.Printf("getRelatedMidArray() GetRelatedTagidList empty,err=%s,uid=%d,tag=%s,lastmid=%d", err.Error(), Userid, lastReqInfo.TagName, lastReqInfo.RelatedMinMid)
		} else {
			lastReqInfo.RelatedTagIdList = append(lastReqInfo.RelatedTagIdList, relatedTagId...)

			this.infoLog.Printf("getRelatedMidArray() GetRelatedTagidList ok uid=%d,tag=%s,lastmid=%d", Userid, lastReqInfo.TagName, lastReqInfo.RelatedMinMid)
		}

	}

	if len(lastReqInfo.RelatedTagIdList) == 0 {
		lastReqInfo.LatestNoMore = 1
		lastReqInfo.RelatedNoMore = 1

		this.infoLog.Printf("getRelatedMidArray() no related tagid,uid=%d,tag=%s,lastmid=%d", Userid, lastReqInfo.TagName, lastReqInfo.RelatedMinMid)

		return MidArr, nil
	}

	whereStr := getQtypeWhereStr(lastReqInfo.QType, lastReqInfo.Userid, lastReqInfo.RelatedTagIdList, nativeStr, learnStr, lastReqInfo.RelatedMinMid)

	if len(lastReqInfo.HotList) > 0 {
		whereStr = fmt.Sprintf("%s AND id NOT IN (%s) ", whereStr, common.JoinUint64(lastReqInfo.HotList, ","))
	}

	query := fmt.Sprintf("SELECT id,IF( deleted = 0 OR (userid = %d and deleted = 2) OR (userid = %d and deleted = 10),1,0) as open_to_me  FROM %s WHERE %s ORDER BY post_time DESC LIMIT %d",
		Userid,
		Userid,
		this.sphinxMomentIndex,
		whereStr,
		appendCount)

	this.infoLog.Printf("getRelatedMidArray() uid=%d,query=%s ", Userid, query)

	rows, err := this.SphinxClient.DB.Query(query)

	if err != nil {
		lastReqInfo.RelatedNoMore = 1
		if err == sql.ErrNoRows {
			return MidArr, errors.New("no content,query=" + query)
		}
		return MidArr, err
	}

	defer rows.Close()

	Mid := uint64(0)
	openToMe := uint32(0)

	for rows.Next() {
		if err := rows.Scan(&Mid, &openToMe); err != nil {
			this.infoLog.Printf("getRelatedMidArray() Scan latest rows.Scan failed", err)
			continue
		}
		MidArr = append(MidArr, Mid)
	}

	if len(MidArr) > 0 {
		lastReqInfo.RelatedMinMid = MidArr[len(MidArr)-1]
	}

	if len(MidArr) < appendCount {
		lastReqInfo.RelatedNoMore = 1
	}

	return MidArr, nil
}

//获取相关的贴文
func (this *TopicDbAPI) GetRelatedTagidList(Userid uint32, TagId uint64, Text string) ([]uint64, error) {

	Text = strings.TrimLeft(Text, TagHashTag)

	_, List, err := this.FilterTagByName(Userid, Text, 10)

	TagIdList := []uint64{}
	if err != nil {
		return TagIdList, err
	}

	for _, v := range List {
		if v.TagId != uint32(TagId) {
			TagIdList = append(TagIdList, uint64(v.TagId))
		}
	}

	if len(TagIdList) == 0 {
		return TagIdList, errors.New("not related tag")
	}

	return TagIdList, nil
}

func getQtypeWhereStr(qType ht_moment.QUERY_TYPE, Userid uint32, tagIds []uint64, nativeStr string, learnStr string, lessThanMid uint64) string {

	whereStr := ""
	if qType == ht_moment.QUERY_TYPE_TAGED_CLASSMATE {

		whereStr = fmt.Sprintf(" tag_id IN (%s) and user_learn IN (%s) and user_native IN (%s)   AND  open_to_me =1  ",
			common.JoinUint64(tagIds, ","),
			nativeStr,
			learnStr,
		)

	} else {

		whereStr = fmt.Sprintf(" tag_id IN (%s) and user_learn IN (%s) and user_native IN (%s)   AND open_to_me =1 ",
			common.JoinUint64(tagIds, ","),
			learnStr,
			nativeStr,
		)

	}

	if lessThanMid != 0 {
		whereStr = fmt.Sprintf("%s AND id > %d ", whereStr, lessThanMid)
	}

	return whereStr
}

func buildBucketInfo(Name string, Index uint64) *ht_moment.BucketBody {

	bucket := &ht_moment.BucketBody{
		BucketName: []byte(Name),
		Index:      proto.Uint64(Index),
	}

	return bucket
}

//把 bucketInfo 信息转化为 TagListReqBuckinfo 结构体
func (this *TopicDbAPI) BucketInfoToReqInfo(reqBucketInfo *ht_moment.BucketInfo) *TagListReqBuckinfo {

	lastReqInfo := &TagListReqBuckinfo{
		HotList:          []uint64{},
		RelatedTagIdList: []uint64{},
	}

	if reqBucketInfo == nil {
		return lastReqInfo
	}

	for _, v := range reqBucketInfo.BucketList {

		name := string(v.BucketName)
		Idx := v.GetIndex()

		// if name == "HOT1" {
		// 	lastReqInfo.HotList = append(lastReqInfo.HotList, Idx)
		// } else if name == "HOT2" {
		// 	lastReqInfo.HotList = append(lastReqInfo.HotList, Idx)
		// } else if name == "HOT3" {
		// 	lastReqInfo.HotList = append(lastReqInfo.HotList, Idx)
		// }

		if name == "NO_LATEST" {
			lastReqInfo.LatestNoMore = 1
		}

		if name == "NO_RELATED" {
			lastReqInfo.RelatedNoMore = 1
		}

		if strings.Index(name, "MIN_LATESTID") >= 0 {
			mid, _ := strconv.ParseUint(name[12:len(name)], 10, 64)
			lastReqInfo.LatestMinMid = mid
		}

		if strings.Index(name, "MIN_RELATED") >= 0 {
			mid, _ := strconv.ParseUint(name[11:len(name)], 10, 64)
			lastReqInfo.RelatedMinMid = mid
		}

		if name == "RELATED_CNT" {
			lastReqInfo.RelatedIdCount = uint32(Idx)
		}

		if strings.Index(name, "HOT") >= 0 {
			mid, _ := strconv.ParseUint(name[3:len(name)], 10, 64)
			lastReqInfo.RelatedTagIdList = append(lastReqInfo.RelatedTagIdList, mid)
		}

		if strings.Index(name, "RETAG") >= 0 {
			// mid, _ := strconv.ParseUint(name[5:len(name)], 10, 64)
			lastReqInfo.RelatedTagIdList = append(lastReqInfo.RelatedTagIdList, Idx)
		}

	}

	return lastReqInfo
}

//把请求信息格式化为 bucketInfo
func (this *TopicDbAPI) ReqInfoToBucketInfo(lastReqInfo *TagListReqBuckinfo) *ht_moment.BucketInfo {

	reqBucketInfo := &ht_moment.BucketInfo{}

	if lastReqInfo == nil {
		return reqBucketInfo
	}

	//记录相关热门贴文mid
	for _, v := range lastReqInfo.HotList {
		item := buildBucketInfo(fmt.Sprintf("HOT%d", v), 0)
		reqBucketInfo.BucketList = append(reqBucketInfo.BucketList, item)
	}

	//记录相关贴文的 标签id
	for k, v := range lastReqInfo.RelatedTagIdList {
		item := buildBucketInfo(fmt.Sprintf("RETAG%d", k), v)
		reqBucketInfo.BucketList = append(reqBucketInfo.BucketList, item)
	}

	if lastReqInfo.LatestMinMid != 0 {
		// item := buildBucketInfo("MIN_RELATED", lastReqInfo.LatestMinMid)
		item := buildBucketInfo(fmt.Sprintf("MIN_LATESTID%v", lastReqInfo.LatestMinMid), 0)
		reqBucketInfo.BucketList = append(reqBucketInfo.BucketList, item)
	}

	if lastReqInfo.RelatedMinMid != 0 {
		// item := buildBucketInfo("RELATED_MIN_MID", lastReqInfo.RelatedMinMid)
		item := buildBucketInfo(fmt.Sprintf("MIN_RELATED%v", lastReqInfo.RelatedMinMid), 0)
		reqBucketInfo.BucketList = append(reqBucketInfo.BucketList, item)
	}

	if lastReqInfo.LatestNoMore != 0 {
		item := buildBucketInfo("NO_LATEST", uint64(lastReqInfo.LatestNoMore))
		reqBucketInfo.BucketList = append(reqBucketInfo.BucketList, item)
	}

	if lastReqInfo.RelatedNoMore != 0 {
		item := buildBucketInfo("NO_RELATED", uint64(lastReqInfo.LatestNoMore))
		reqBucketInfo.BucketList = append(reqBucketInfo.BucketList, item)
	}

	// if len(lastReqInfo.RelatedTagIdList) != 0 {
	// 	item := buildBucketInfo("RELATED_CNT", uint64(len(lastReqInfo.RelatedTagIdList)))
	// 	reqBucketInfo.BucketList = append(reqBucketInfo.BucketList, item)
	// }

	return reqBucketInfo
}
