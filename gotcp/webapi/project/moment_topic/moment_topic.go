package main

import (
	"database/sql"
	// "fmt"
	"github.com/HT_GOGO/gotcp"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/HT_GOGO/gotcp/webapi/project/moment_topic/org"
	"github.com/golang/protobuf/proto"
	//"io/ioutil"
	"net"
	"os/signal"
	// "regexp"
	"strconv"
	// "strings"
	"syscall"
	//"github.com/garyburd/redigo/redis"
	"errors"

	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	// 对应了 表 MMT_TAG_DEFAULT 里面type的节点类型
	TAG_TYPE_NOMARL = 1
	TAG_TYPE_MULTI  = 2

	// 对应了 官方标签和用户自定义标签
	TAG_TYPE_OFFICAL = 1
	TAG_TYPE_USRE    = 0

	CMD_UPDATE_MOMENT_WORDS = 1002 //从db里面更新信息流贴文关键词

	//用户发布的帖子是新的标签
	CDM_GENERATE_TAG_ID = 201

	//发帖之后，发mid写入到对应的tagid 对应的桶
	//CDM_POST_MOMENT_TO_TAG_BUCKET = 202 取消了 直接用 CMD_POST_MOMENT

	CMD_UPDATE_DEFAULT_TAG_LIST = 1006 //更新默认标签。
)

var (
	MAX_SLEFIE_SQR   float64
	Mgdb             *sql.DB
	Masterdb         *sql.DB
	Logdb            *sql.DB
	topicDbPool      *org.TopicDbAPI
	topicTeenDbPool  *org.TopicDbAPI
	userInfoCacheApi *tcpcommon.UserInfoCacheApi
	langTypeTagGroup map[uint32][]*ht_moment.DefaultTagGroup
	defaultTagLocker *sync.RWMutex
	infoLog          *log.Logger

	momentTextFilter *common.TextFilter

	options common.Options
)

type Callback struct{}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {
	packet, ok := p.(*tcpcommon.HeadV2Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	// head 为一个new出来的对象指针
	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:cmd=%v seq=%v uid=%v", head.Cmd, head.Seq, head.Uid)

	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	switch head.Cmd {
	case uint32(ht_moment.CMD_TYPE_CMD_GET_DEFAULT_MOMENT_TAG):
		go func() {
			_, err := GetDefaultMomentTag(c, head, packet.GetBody())
			if err != nil {
				infoLog.Println("OnMessage GetDefaultMomentTag() err=", err, head)
				// SendRetCode(c, head, 1, err.Error())
			}
		}()
	case CMD_UPDATE_DEFAULT_TAG_LIST:
		go func() {
			err := InitLangTypeTagGroup()
			if err != nil {
				common.SendRetCode(c, head, 0, err.Error())
			} else {
				common.SendRetCode(c, head, 1, "OK")
			}

		}()

	case CMD_UPDATE_MOMENT_WORDS:

		go func() {
			err := ProcConfigUpdate(c, head, packet.GetBody())
			if err != nil {
				common.SendRetCode(c, head, 500, err.Error())
			} else {
				common.SendRetCode(c, head, 0, "OK")
			}

		}()

	case uint32(ht_moment.CMD_TYPE_CMD_SEARCH_MOMENT_TAG):
		go func() {
			_, err := SearchMomentTag(c, head, packet.GetBody())
			if err != nil {
				infoLog.Println("OnMessage SearchMomentTag() err=", err, head)
				// SendRetCode(c, head, 1, err.Error())
			}
		}()
	case uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_BY_TAG):

		go func() {
			_, err := GetTagedMomentList(c, head, packet.GetBody())
			if err != nil {
				infoLog.Println("OnMessage GetTagedMomentList() err=", err, head)

			}
		}()
	case CDM_GENERATE_TAG_ID:
		go func() {
			_, err := GenerateTagIdFromMomentBody(c, head, packet.GetBody())
			if err != nil {
				infoLog.Println("OnMessage GenerateTagIdFromMomentBody() err=", err, head)

				common.SendV2RspBuffer(c, head, []byte("{\"bad\":\""+err.Error()+"\"}"))

			}
		}()
	case uint32(ht_moment.CMD_TYPE_CMD_POST_MOMENT):
		go func() {
			_, err := PostMomentToTagBucket(c, head, packet.GetBody())
			if err != nil {
				infoLog.Println("OnMessage PostMomentWithNewTagId() err=", err, head)
				common.SendV2RspBuffer(c, head, []byte("{\"bad\":\""+err.Error()+"\"}"))
			}
		}()

	default:
		infoLog.Printf("OnMessage UnHandle Cmd =%d", head.Cmd)
	}
	return true
}

func ProcConfigUpdate(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (err error) {

	if head.Cmd == CMD_UPDATE_MOMENT_WORDS {

		err = momentTextFilter.RefreshRegex()

	}

	return

}

//获取默认标签
func GetDefaultMomentTag(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (bool, error) {

	ReqBody := &ht_moment.ReqBody{}
	err := proto.Unmarshal(payLoad, ReqBody)
	if err != nil {
		return false, err
	}

	busiBody := ReqBody.GetGetDefaultMomentTagReqbody()

	if busiBody == nil {
		return false, errors.New("GetDefaultMomentTag() GetDefaultMomentTagReqbody fail")
	}

	defaultTagLocker.RLock()
	defer defaultTagLocker.RUnlock()

	langType := busiBody.GetLangType()
	userid := busiBody.GetUserid()

	if langType < 0 {
		langType = 1
	}

	infoLog.Println("GetDefaultMomentTag() langType=", langType, "userid=", userid)

	tags, ok := langTypeTagGroup[langType]
	var rspBody *ht_moment.RspBody
	if ok {
		rspBody = &ht_moment.RspBody{
			GetDefaultMomentTagRspbody: &ht_moment.GetDefautMomentTagRspBody{
				Status: &ht_moment.Header{
					Code: proto.Uint32(uint32(ht_moment.RET_CODE_RET_SUCCESS)),
				},
				TagGroup: tags,
			},
		}

	} else {

		rspBody = &ht_moment.RspBody{
			GetDefaultMomentTagRspbody: &ht_moment.GetDefautMomentTagRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(ht_moment.RET_CODE_RET_NO_MORE_CONTENT)),
					Reason: []byte("not set"),
				},
			},
		}

	}

	return common.SendMomentRsp(c, head, rspBody)
}

//搜索历史标签
func SearchMomentTag(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (bool, error) {

	ReqBody := &ht_moment.ReqBody{}
	err := proto.Unmarshal(payLoad, ReqBody)
	if err != nil {
		return false, err
	}

	busiBody := ReqBody.GetSearchMomentTagReqbody()

	if busiBody == nil {
		return false, errors.New("GetSearchMomentTagReqbody fail")
	}

	Text := string(busiBody.GetText())
	Userid := busiBody.GetUserid()

	if len(Text) == 0 {
		return false, errors.New("too short")
	}

	foundExact, List, err := topicDbPool.FilterTagByName(Userid, Text, 40)

	var rspBody *ht_moment.RspBody

	groupList := []*ht_moment.DefaultTagGroup{}

	if foundExact == false {
		oneTagGroup := BuildOneTagWithName(Text)
		groupList = append(groupList, oneTagGroup)
	}

	if err != nil {
		infoLog.Println("SearchMomentTag() Filter err:", err)
	} else {

		match := 0

		for _, v := range List {
			item := &ht_moment.DefaultTagGroup{
				Type: proto.Uint32(TAG_TYPE_NOMARL),
				SubTags: []*ht_moment.DefaultTagBody{
					&ht_moment.DefaultTagBody{
						Name:     []byte(org.TagHashTag + v.Name),
						Text:     []byte(org.TagHashTag + v.Name),
						Type:     proto.Uint32(v.Type),
						LangType: proto.Uint32(v.LangType),
						Id:       proto.Uint64(uint64(v.TagId)),
					},
				},
			}

			if v.Name == Text {
				match = 1
			}

			groupList = append(groupList, item)
		}

		if match == 1 && len(groupList) > 1 {
			groupList = groupList[1:]
		}

	}

	rspBody = &ht_moment.RspBody{
		SearchMomentTagRspbody: &ht_moment.SearchMomentTagRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(ht_moment.RET_CODE_RET_SUCCESS)),
				Reason: []byte("ok"),
			},
			TagGroup: groupList,
		},
	}

	infoLog.Printf("SearchMomentTag() text=%s,userid=%d,List=%#v", Text, Userid, len(List))

	return common.SendMomentRsp(c, head, rspBody)

}

//发了新帖之后，插入新的标签ID
func GenerateTagIdFromMomentBody(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (bool, error) {

	ReqBody := &ht_moment.ReqBody{}
	err := proto.Unmarshal(payLoad, ReqBody)
	if err != nil {
		return false, err
	}

	momentPostBody := ReqBody.GetPostMntReqbody()

	if momentPostBody == nil {
		return false, errors.New("GetPostMntReqbody fail")
	}

	momentBody := momentPostBody.GetMoment()

	if momentBody == nil {
		return false, errors.New("GetMoment fail")
	}

	Mid := string(momentBody.GetMid())

	tags := momentBody.GetTags()
	Userid := momentBody.GetUserid()

	newTagNames := map[string]*org.TagBody{}
	IsBad := uint8(0)
	for _, v := range tags {
		if v.GetId() == 0 {

			name := string(v.GetName())
			//newTagNames = append(newTagNames, string(v.GetName()))

			_, ok := newTagNames[name]

			if ok {
				continue
			}

			match, _, err := momentTextFilter.IsTextIllegally(name, Mid)

			if err == nil && match != "" {
				infoLog.Println("GenerateTagIdFromMomentBody() IsTextIllegally name=", name, ",match=", match)
				IsBad = 1
			} else {
				IsBad = 0
			}

			newTagNames[name] = &org.TagBody{
				LangType: momentTextFilter.CheckLanguage(name),
				Id:       0,
				IsBad:    IsBad,
			}
		}
	}

	newTagIdMap := map[string]uint32{}
	if len(newTagNames) > 0 {
		newTagIdMap, err = topicDbPool.AddTag(Userid, newTagNames)

		if err != nil { //失败了但是 还是返回json
			infoLog.Println("GenerateTagIdFromMomentBody() err=", err, head)
			// return false, err
		}
	}

	jsonStr, err := json.Marshal(newTagIdMap)

	common.SendV2RspBuffer(c, head, jsonStr)

	return true, nil
}

//发布新帖子到桶里面
func PostMomentToTagBucket(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (bool, error) {

	ReqBody := &ht_moment.ReqBody{}
	err := proto.Unmarshal(payLoad, ReqBody)
	if err != nil {
		return false, err
	}

	momentPostBody := ReqBody.GetPostMntReqbody()

	if momentPostBody == nil {
		return false, errors.New("PostMomentToTagBucket() GetPostMntReqbody fail")
	}

	momentBody := momentPostBody.GetMoment()

	if momentBody == nil {
		return false, errors.New("PostMomentToTagBucket() GetMoment fail")
	}

	Userid := momentBody.GetUserid()

	// infoLog.Printf("PostMomentToTagBucket() GetUserLanguageSetting start userid=%d", Userid)

	userLangInfo, err := userInfoCacheApi.GetUserLanguageSetting(Userid)

	// infoLog.Printf("PostMomentToTagBucket() GetUserLanguageSetting end userid=%d", Userid)

	isTeen := false
	if err != nil {
		infoLog.Println("PostMomentToTagBucket() get userinfo failed,err:", err, Userid)
		//return false, err
		userLangInfo = &tcpcommon.CacheLanguageInfo{}
	} else {
		isTeen = common.IsBirthDayTeen(userLangInfo.Birthday)
		infoLog.Printf("PostMomentToTagBucket() userid=%d,Birthday=%#v,isTeen=%t", Userid, userLangInfo.Birthday, isTeen)
	}
	tags := momentBody.GetTags()
	if len(tags) == 0 {
		return false, errors.New("no tag")
	}

	tagIdExistMap := map[string]uint32{}
	for _, v := range tags {
		if v.GetId() != 0 {
			item := strings.TrimLeft(string(v.GetName()), org.TagHashTag)
			tagIdExistMap[item] = uint32(v.GetId())
		}
	}

	err = topicDbPool.IncTagDbScore(Userid, tagIdExistMap)
	if err != nil {
		infoLog.Println("PostMomentToTagBucket() IncTagDbScore,err:", err, Userid)
	} else {
		infoLog.Println("PostMomentToTagBucket() IncTagDbScore ", Userid, tagIdExistMap)
	}

	if isTeen {

		err = topicTeenDbPool.PostNewTagedMoment(userLangInfo, momentBody)

	} else {

		err = topicDbPool.PostNewTagedMoment(userLangInfo, momentBody)
	}

	if err != nil {
		infoLog.Println("ProcPostMoment() PostNewTagedMoment failed,err:", err, Userid)
		return false, err
	}

	return false, nil
}

//通过标签id来加载贴文列表
func GetTagedMomentList(c *gotcp.Conn, head *tcpcommon.HeadV2, payLoad []byte) (bool, error) {

	ReqBody := &ht_moment.ReqBody{}
	err := proto.Unmarshal(payLoad, ReqBody)
	if err != nil {
		common.SendMomentStatusCode(c, head, ht_moment.RET_CODE_RET_INTERNAL_ERR, err.Error())
		return false, err
	}

	busiBody := ReqBody.GetGetMidListByTagReqbody()

	if busiBody == nil {
		common.SendMomentStatusCode(c, head, ht_moment.RET_CODE_RET_INTERNAL_ERR, "get body fail")
		return false, errors.New("GetGetMidListByTagReqbody fail")
	}

	bucketInfo := busiBody.GetBucket()

	reqInfo := topicDbPool.BucketInfoToReqInfo(bucketInfo)

	Userid := busiBody.GetUserid()
	reqInfo.Userid = Userid
	reqInfo.TagId = busiBody.GetTag().GetId()

	reqInfo.TagName = string(busiBody.GetTag().GetName())
	reqInfo.QType = busiBody.GetQtype()
	//如果标示了 相关贴文不存在，那么返回没有贴文了
	if reqInfo.RelatedNoMore == 1 || reqInfo.TagId == 0 {

		infoLog.Println("GetTagedMomentList() RelatedNoMore", Userid, reqInfo.RelatedNoMore)

		return common.SendMomentStatusCode(c, head, ht_moment.RET_CODE_RET_NO_MORE_CONTENT, "no more")
	}

	if len(busiBody.GetBucket().GetBucketList()) == 0 {
		reqInfo.IsFirstReq = 1
	}

	userLangInfo, err := userInfoCacheApi.GetUserLanguageSetting(Userid)

	isTeen := false
	if err != nil {
		infoLog.Println("GetTagedMomentList() get userinfo failed,err:", err, Userid)
		common.SendMomentStatusCode(c, head, ht_moment.RET_CODE_RET_INTERNAL_ERR, err.Error())
		return false, err
	} else {
		isTeen = common.IsBirthDayTeen(userLangInfo.Birthday)
	}
	var sphinxDb *org.TopicDbAPI
	if isTeen {
		sphinxDb = topicTeenDbPool
	} else {
		sphinxDb = topicDbPool
	}

	firstPageCount := 40

	//TagName==3描述的是分享页的
	if reqInfo.TagName == "" {
		firstPageCount = 3
	}

	IdResultList, rspInfo, err := sphinxDb.GetMidListByTag(Userid, userLangInfo, reqInfo, firstPageCount)

	var rspBody *ht_moment.RspBody

	pageSize := uint32(10)

	if err != nil {
		return common.SendMomentStatusCode(c, head, ht_moment.RET_CODE_RET_NO_MORE_CONTENT, err.Error())
	}

	if len(IdResultList) == 0 {
		return common.SendMomentStatusCode(c, head, ht_moment.RET_CODE_RET_NO_MORE_CONTENT, "no related moment")
	}

	if reqInfo.TagName == "" {
		tname, err := topicDbPool.GetTagNameByTagId(reqInfo.TagId)

		if err != nil {
			infoLog.Println("GetTagedMomentList() GetTagNameByTagId:", err)
		}

		busiBody.Tag.Name = []byte(tname)
	}

	rspBody = &ht_moment.RspBody{
		GetMidListByTagRspbody: &ht_moment.GetMidListByTagRspBody{
			Status: &ht_moment.Header{
				Code:   proto.Uint32(uint32(ht_moment.RET_CODE_RET_SUCCESS)),
				Reason: []byte("OK"),
			},
			PageSize: proto.Uint32(pageSize),
			Tag:      busiBody.GetTag(),
			Bucket:   sphinxDb.ReqInfoToBucketInfo(rspInfo),
			IdList:   IdResultList,
		},
	}

	return common.SendMomentRsp(c, head, rspBody)
}

func BuildOneTagWithName(Name string) *ht_moment.DefaultTagGroup {

	item := &ht_moment.DefaultTagGroup{
		Type: proto.Uint32(TAG_TYPE_NOMARL),
		SubTags: []*ht_moment.DefaultTagBody{
			&ht_moment.DefaultTagBody{
				Name:     []byte(org.TagHashTag + Name),
				Text:     []byte(org.TagHashTag + Name),
				Type:     proto.Uint32(TAG_TYPE_USRE),
				LangType: proto.Uint32(momentTextFilter.CheckLanguage(Name)),
			},
		},

		IsNew: proto.Uint32(1),
	}
	return item

}

func main() {

	cfg, err := common.InitLogAndOption(&options, &infoLog)

	Mgdb, err = common.InitMySqlFromSection(cfg, infoLog, "MG_MYSQL", 1, 1)
	common.CheckError(err)
	defer Mgdb.Close()

	Masterdb, err = common.InitMySqlFromSection(cfg, infoLog, "MASTER_MYSQL", 3, 1)
	common.CheckError(err)
	defer Masterdb.Close()

	// infoLog.Println("isBirthDayTeen(userLangInfo.Birthday)", common.IsBirthDayTeen("1999-10-24"))
	// return

	// Logdb, err = common.InitMySqlFromSection(cfg,infoLog, "LOGDB_MYSQL", 1, 1)
	// checkError(err)
	// defer Logdb.Close()

	// redis_host := cfg.Section("REDIS").Key("ruler_redis_ip").MustString("127.0.0.1")
	// redis_port := cfg.Section("REDIS").Key("ruler_redis_port").MustString("6379")

	// RulerRedis = tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	// redis_host = cfg.Section("REDIS").Key("userinfo_redis_ip").MustString("127.0.0.1")
	// redis_port = cfg.Section("REDIS").Key("userinfo_redis_port").MustString("6379")
	defaultTagLocker = new(sync.RWMutex)
	langTypeTagGroup = map[uint32][]*ht_moment.DefaultTagGroup{}
	err = InitLangTypeTagGroup()

	// jsonTag, err := json.Marshal(TagGroup)
	if err != nil {
		infoLog.Printf("InitLangTypeTagGroup()  err=%s", err.Error())
	}

	infoLog.Printf("=========NewTextFiler start=========")

	momentTextFilter = common.NewTextFiler(Mgdb, common.CHECK_MOMENT, infoLog)

	infoLog.Println("=========NewTextFiler end=========")

	// user info cache api
	userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	infoLog.Printf("user info cache tcpcommonip=%v port=%v", userInfoIp, userInfoPort)
	userInfoCacheApi = tcpcommon.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &tcpcommon.HeadV2Protocol{}, 1000)

	sphinx_host := cfg.Section("SERVICE_CONFIG").Key("sphinx_host").MustString("127.0.0.1")
	sphinx_port := cfg.Section("SERVICE_CONFIG").Key("sphinx_port").MustInt(9312)
	sphinx_tag_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_tag_index").MustString("momenttag")
	sphinx_moment_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_moment_index").MustString("mmt_taged_list")
	sphinx_teen_moment_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_teen_moment_index").MustString("mmt_teen_taged_list")
	sphinx_timeout := cfg.Section("SERVICE_CONFIG").Key("sphinx_timeout").MustInt(5000)
	sphinx_open_count := cfg.Section("SERVICE_CONFIG").Key("sphinx_open_count").MustInt(100)
	sphinx_idle_count := cfg.Section("SERVICE_CONFIG").Key("sphinx_idle_count").MustInt(10)

	// 建立对象池对象
	topicDbPool = org.NewTopicDbAPI(sphinx_host, sphinx_port, sphinx_tag_index, sphinx_moment_index, sphinx_timeout, sphinx_open_count, sphinx_idle_count, Masterdb, userInfoCacheApi, infoLog)

	topicDbPool.SetTagedDbTable("MMT_TAGED_LIST")

	topicTeenDbPool = org.NewTopicDbAPI(sphinx_host, sphinx_port, sphinx_tag_index, sphinx_teen_moment_index, sphinx_timeout, sphinx_open_count, sphinx_idle_count, Masterdb, userInfoCacheApi, infoLog)
	topicTeenDbPool.SetTagedDbTable("MMT_TEEN_TAGED_LIST")

	infoLog.Printf("NewTopicDbAPI host=%s:%d@%s,%s,timeout=%d", sphinx_host, sphinx_port, sphinx_tag_index, sphinx_moment_index, sphinx_timeout)

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	common.CheckError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	common.CheckError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &tcpcommon.HeadV2Protocol{})

	// starts service
	go srv.Start(listener, time.Second)
	infoLog.Println("listening:", listener.Addr())

	// catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()

}

/*
CREATE TABLE `MMT_TAG_DEFAULT` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag_id` INT(11) UNSIGNED DEFAULT '0' COMMENT ' 标签id ',
  `lang_type` TINYINT(32) UNSIGNED DEFAULT '0' COMMENT ' 该标签的语言类型：Chinese=2,Chinese_yy=3,English=1,Thai=17,Japanese=5,Korean=6,Spanish=7,Portuguese=9,French=8,Italian=11,Russian=12,German=10,Arabic=13,Catalan=27,Danish=23,Esperanto=41,Persian=15,Indonesian=18,Turkish=14,Vietnamese=21, ',
  `type` TINYINT(4) UNSIGNED DEFAULT '1' COMMENT '0:unkown,1:普通节点，2:主题节点',
  `parent_name` VARCHAR(24) DEFAULT '' COMMENT '在type=1的情况下，记录的是主题值，前缀',
  `name` VARCHAR(128) DEFAULT '' COMMENT '标签内容',
  `status` TINYINT(32) UNSIGNED DEFAULT '1' COMMENT '1=可用，0=禁用',
  `createtime` DATETIME DEFAULT '1990-10-10 00:00:00' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`lang_type`)
) ENGINE=INNODB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8
*/

func InitLangTypeTagGroup() error {

	rows, err := Masterdb.Query("SELECT DISTINCT `lang_type`  FROM MMT_TAG_DEFAULT  WHERE `status`=1 ")

	if err != nil {
		return err
	}

	infoLog.Printf("InitLangTypeTagGroup() start")

	langType := uint32(0)

	allLangId := []uint32{}

	for rows.Next() {

		if err := rows.Scan(&langType); err != nil {
			infoLog.Printf("InitLangTypeTagGroup() rows.Scan failed")
			continue
		}
		allLangId = append(allLangId, langType)
	}

	rows.Close()

	for _, v := range allLangId {
		tags, err := GetDefaultTag(v)
		if err != nil {
			infoLog.Println("InitLangTypeTagGroup GetDefaultTag()  failed err:", err)
		}
		langTypeTagGroup[v] = tags
		infoLog.Printf("InitLangTypeTagGroup langType=%d,tag count=%d，tags=%#v", v, len(tags), tags)
		time.Sleep(time.Millisecond * 300)
	}

	return nil
}

func GetDefaultTag(langType uint32) ([]*ht_moment.DefaultTagGroup, error) {

	infoLog.Printf("GetDefaultTag start lang=%d", langType)

	rows, err := Masterdb.Query("SELECT `tag_id`,`type`,`lang_type`,`parent_name`,`name` FROM MMT_TAG_DEFAULT  WHERE `status`=1 AND `lang_type`= ? ORDER BY `order` DESC", langType)

	infoLog.Printf("InitLangTypeTagGroup() defaultTagLocker before err return")

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	if err == sql.ErrNoRows {
		return nil, errors.New("no data")
	}

	defaultTagLocker.Lock()
	defer defaultTagLocker.Unlock()

	TagId := uint64(0)
	Type := uint64(0)
	LangType := uint32(0)
	ParentName := ""
	Name := ""

	multiTagsIdx := map[string]uint32{}

	// rowNum, err := rows.
	tagsGroup := []*ht_moment.DefaultTagGroup{}
	// tagsGroup := make([]*ht_moment.DefaultTagGroup, totalCount)

	for rows.Next() {

		if err := rows.Scan(&TagId, &Type, &LangType, &ParentName, &Name); err != nil {
			infoLog.Printf("rows.Scan failed")
			continue
		}

		if TagId == 0 {

			infoLog.Printf("InitLangTypeTagGroup() GetDefaultTag  bad TagId=%d", TagId)

			continue
		}

		if Type == TAG_TYPE_MULTI && ParentName != "" && Name != "" {

			idx, ok := multiTagsIdx[ParentName]

			itemSub := &ht_moment.DefaultTagBody{
				Id:       proto.Uint64(TagId),
				Name:     []byte(org.TagHashTag + ParentName + Name),
				Text:     []byte(Name),
				LangType: proto.Uint32(LangType),
				Type:     proto.Uint32(uint32(TAG_TYPE_OFFICAL)),
			}

			if ok {

				tagsGroup[idx].SubTags = append(tagsGroup[idx].SubTags, itemSub)

			} else {

				TagBodyGroup := []*ht_moment.DefaultTagBody{itemSub}

				multiTagsIdx[ParentName] = uint32(len(tagsGroup))
				itemGroup := &ht_moment.DefaultTagGroup{
					Type:    proto.Uint32(TAG_TYPE_MULTI),
					SubTags: TagBodyGroup,
					SubName: []byte(org.TagHashTag + ParentName),
				}
				tagsGroup = append(tagsGroup, itemGroup)
			}

		}

		if Type == TAG_TYPE_NOMARL && Name != "" {

			itemSub := &ht_moment.DefaultTagBody{
				Id:       proto.Uint64(TagId),
				Name:     []byte(org.TagHashTag + ParentName + Name),
				Text:     []byte(org.TagHashTag + ParentName + Name),
				LangType: proto.Uint32(LangType),
				Type:     proto.Uint32(uint32(TAG_TYPE_OFFICAL)),
			}

			TagBodyGroup := []*ht_moment.DefaultTagBody{itemSub}

			itemGroup := &ht_moment.DefaultTagGroup{
				Type:    proto.Uint32(TAG_TYPE_NOMARL),
				SubTags: TagBodyGroup,
			}

			tagsGroup = append(tagsGroup, itemGroup)
		}

		infoLog.Printf("GetDefaultTag() tag_id=%d,Type=%d,lang=%d,parent=%s,name=%s", TagId, Type, langType, ParentName, Name)

	}

	return tagsGroup, nil
}
