package main

import (
	"database/sql"
	// "fmt"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/HT_GOGO/gotcp/webapi/project/moment_topic/org"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	//"io/ioutil"

	// "regexp"

	// "strings"

	//"github.com/garyburd/redigo/redis"

	"log"
	"os"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

const (
	// 对应了 表 MMT_TAG_DEFAULT 里面type的节点类型
	TAG_TYPE_NOMARL = 1
	TAG_TYPE_MULTI  = 2

	// 对应了 官方标签和用户自定义标签
	TAG_TYPE_OFFICAL = 1
	TAG_TYPE_USRE    = 0

	//用户发布的帖子是新的标签
	CDM_GENERATE_TAG_ID = 201

	//发帖之后，发mid写入到对应的tagid 对应的桶
	CDM_POST_MOMENT_TO_TAG_BUCKET = 202
)

var (
	MAX_SLEFIE_SQR   float64
	Sphinxdb         *sql.DB
	Masterdb         *sql.DB
	Logdb            *sql.DB
	topicDbPool      *sync.Pool
	langTypeTagGroup map[uint32][]*ht_moment.DefaultTagGroup
	defaultTagLocker *sync.RWMutex
	infoLog          *log.Logger
	options          Options
)

var parser = flags.NewParser(&options, flags.Default)

func main() {

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("")
	if fileName != "" {
		logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		defer logFile.Close()
		if err != nil {
			log.Fatalln("open file error !")
			return
		}

		// 创建一个日志对象
		infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	} else {
		infoLog = log.New(os.Stdout, "[Info]", log.LstdFlags)
	}

	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// Mgdb, err = initMySqlFromSection(cfg, "MG_MYSQL", 1, 1)
	// checkError(err)
	// defer Mgdb.Close()
	Masterdb, err = InitMySqlFromSection(cfg, "MASTER_MYSQL", 3, 1)
	Sphinxdb, err = InitMySqlFromSection(cfg, "MASTER_MYSQL", 512, 12)
	checkError(err)
	defer Masterdb.Close()
	defer Sphinxdb.Close()

	defaultTagLocker = new(sync.RWMutex)
	langTypeTagGroup = map[uint32][]*ht_moment.DefaultTagGroup{}

	sphinx_host := cfg.Section("SERVICE_CONFIG").Key("sphinx_host").MustString("127.0.0.1")
	sphinx_port := cfg.Section("SERVICE_CONFIG").Key("sphinx_port").MustInt(9312)
	sphinx_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_index").MustString("momenttag")
	sphinx_timeout := cfg.Section("SERVICE_CONFIG").Key("sphinx_timeout").MustInt(5000)

	API := org.NewTopicDbAPI(sphinx_host, sphinx_port, sphinx_index, sphinx_timeout, sphinx_timeout, sphinx_timeout, Masterdb, infoLog)

	i := uint32(0)
	max := uint32(100)

	infoLog.Println("common=", common.Crc32("123456789"))

	NewTagIdMap, err := API.AddTag(900866, []string{"大清", "你妹妹"})

	API.IncTagDbScore(90866, map[string]uint32{
		"助我翻譯":   4,
		"帮助我你大爷": 92,
	})

	API.IncTagSphinxScore(90866, map[string]uint32{
		"助我翻譯":   4,
		"帮助我你大爷": 92,
	})

	infoLog.Println("NewTagIdMap=", NewTagIdMap, err)

	for {

		i++

		infoLog.Println("Filter start ", i)

		list, err := API.FilterBySql("he*")
		// list, err := API.Filter("帮助")

		infoLog.Println("Filter", list, err)

		if i >= max {
			break
		}

		time.Sleep(time.Second * time.Duration(i))
	}
}

func InitMySqlFromSection(cfg *ini.File, secName string, OpenCount int, OdleCount int) (*sql.DB, error) {

	// init mysql
	mysqlHost := cfg.Section(secName).Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section(secName).Key("mysql_user").MustString("")
	mysqlPasswd := cfg.Section(secName).Key("mysql_passwd").MustString("")
	mysqlDbName := cfg.Section(secName).Key("mysql_db").MustString("")
	mysqlPort := cfg.Section(secName).Key("mysql_port").MustString("3306")

	cnfStr := mysqlUser + ":" + mysqlPasswd + "@" + "tcp(" + mysqlHost + ":" + mysqlPort + ")/" + mysqlDbName + "?charset=utf8&timeout=10s"

	infoLog.Printf("InitMySqlFromSection() name=%s,Idle=%d,Open=%d start,cnfStr=%s", secName, OdleCount, OpenCount, cnfStr)

	mydb, err := sql.Open("mysql", cnfStr)
	if err != nil {
		infoLog.Println("open ", secName, " failed", time.Now())
		return nil, err
	}

	if err := mydb.Ping(); err != nil {
		infoLog.Fatalln(secName, " ping err", err)
		return nil, err
	}

	mydb.SetMaxIdleConns(OdleCount)

	mydb.SetMaxOpenConns(OpenCount)

	infoLog.Printf("InitMySqlFromSection() name=%s,Idle=%d,Open=%d end", secName, OdleCount, OpenCount)

	return mydb, nil
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
