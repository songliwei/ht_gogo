package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/HT_GOGO/gotcp/libcomm"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/HT_GOGO/gotcp/webapi/project/moment_topic/org"
	_ "github.com/go-sql-driver/mysql"
	"github.com/nsqio/go-nsq"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	// "sync"
	"syscall"
	"time"
)

type MMTEvent struct {
	Type      uint32 `json:"type" binding:"required"`
	Mid       string `json:"mid" binding:"required"`
	Userid    uint32 `json:"uid" binding:"required"`
	TimeStamp uint64 `json:"ts" binding:"required"`
}

var (
	infoLog *log.Logger

	Masterdb *sql.DB
	options  common.Options

	topicDbPool     *org.TopicDbAPI
	topicTeenDbPool *org.TopicDbAPI

	queueCount            int
	GlobalRecvMsgPackChan []chan *MMTEvent
	userInfoCacheApi      *tcpcommon.UserInfoCacheApi
	momentAPI             *tcpcommon.MntApi
)

func MessageHandle(message *nsq.Message) error {

	attr := "mmt/total_mmt_event_count"
	libcomm.AttrAdd(attr, 1)

	PostData(message)

	return nil
}

func PostData(message *nsq.Message) {

	info := &MMTEvent{}
	err := json.Unmarshal(message.Body, info) // JSON to Struct

	if err != nil {
		infoLog.Printf("PostData err=%s", err.Error())
		return
	}

	//确保同一个帖子再同一个处理队列里面
	//因为有时候做分值计算的时候，要先读取 再写入
	midInt, err := strconv.ParseUint(info.Mid, 10, 64)
	chanIdx := midInt % uint64(queueCount)

	infoLog.Printf("PostData enter chanIdx=%d timestamp=%v body=%s ", chanIdx, message.Timestamp, message.Body)

	GlobalRecvMsgPackChan[chanIdx] <- info

}

func filterMMTHandler(info *MMTEvent, idx int) {

	infoLog.Printf("filterMMTHandler() idx=%d Mid=%s,Userid=%d,Type=%d", idx, info.Mid, info.Userid, info.Type)

	code, momentBlockInfo, err := momentAPI.ViewMomentByMid(10000, []string{info.Mid}, "CN")

	if err != nil {
		infoLog.Printf("filterMMTHandler() idx=%d,info=%#v,err=%s", idx, info, err.Error())
		mmtEventHandler(topicDbPool, info, idx)
		return
	}

	if code != 0 {
		infoLog.Printf("filterMMTHandler() idx=%d,info=%#v code!=0", idx, info)
		return
	}

	if len(momentBlockInfo) == 0 {
		infoLog.Printf("filterMMTHandler() moment list empty")
		return
	}

	isTeen := false

	for _, v := range momentBlockInfo {

		body := v.GetMoment()

		postUid := body.GetUserid()

		tags := body.GetTags()

		if len(tags) == 0 {
			infoLog.Printf("filterMMTHandler() no tag ")
			continue
		}

		userLangInfo, err := userInfoCacheApi.GetUserLanguageSetting(postUid)

		if err != nil {
			infoLog.Printf("filterMMTHandler() GetUserLanguageSetting err:", err)
		} else {
			isTeen = common.IsBirthDayTeen(userLangInfo.Birthday)
		}

		if isTeen {
			err = mmtEventHandler(topicTeenDbPool, info, idx)
			if err != nil {
				infoLog.Println("filterMMTHandler() mmtEventHandler handle teen pool err:", err)
				if err.Error() == "not_exist" {
					err = mmtEventHandler(topicDbPool, info, idx)
				}
			}
		} else {

			err = mmtEventHandler(topicDbPool, info, idx)
			if err != nil {
				infoLog.Println("filterMMTHandler() mmtEventHandler handle adult pool err:", err)
			}
		}

		break

	}

}

func mmtEventHandler(sphinxClient *org.TopicDbAPI, info *MMTEvent, idx int) error {

	//获取mid的内容，判断里面有没有 tag 并且判断发布人的生日 小于18 用 topicTeenDbPool
	// deleted  commented_cnt  liked_cnt  shared_cnt  collected_cnt  reported_cnt
	err := errors.New("OK")
	switch info.Type {

	case org.NSQ_EVENT_LIKE:
		err = sphinxClient.ModMomentScore("liked", info.Mid, info.Userid, 1)
	case org.NSQ_EVENT_CANCLE_LIKE:
		err = sphinxClient.ModMomentScore("liked", info.Mid, info.Userid, -1)
	case org.NSQ_EVENT_COMMENT:
		err = sphinxClient.ModMomentScore("commented", info.Mid, info.Userid, 1)
	case org.NSQ_EVENT_DELETE_COMMENT:
		err = sphinxClient.ModMomentScore("commented", info.Mid, info.Userid, -1)
	case org.NSQ_EVENT_REPORT:
		err = sphinxClient.ModMomentScore("commented", info.Mid, info.Userid, -1)
	case org.NSQ_EVENT_FAVORITE:
		err = sphinxClient.ModMomentScore("collected", info.Mid, info.Userid, 1)
	case org.NSQ_EVENT_CANCLE_FAVORITE:
		err = sphinxClient.ModMomentScore("collected", info.Mid, info.Userid, -1)
	case org.NSQ_EVENT_SHARE:
		err = sphinxClient.ModMomentScore("shared", info.Mid, info.Userid, 1)
	case org.NSQ_EVENT_BUCKET_TO_ME:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_BUCKET_TO_ME", info.Type, info.Mid, info.Userid, 2)
	case org.NSQ_EVENT_BUCKET_TO_FOLLOW:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_BUCKET_TO_FOLLOW", info.Type, info.Mid, info.Userid, 10)
	case org.NSQ_EVENT_BUCKET_TO_ALL:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_BUCKET_TO_ALL", info.Type, info.Mid, info.Userid, 0)
	case org.NSQ_EVENT_OP_RESTORE:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_OP_RESTORE", info.Type, info.Mid, info.Userid, 0)
	case org.NSQ_EVENT_OP_HIDE:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_OP_HIDE", info.Type, info.Mid, info.Userid, 2)
	case org.NSQ_EVENT_OP_DELETE:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_OP_DELETE", info.Type, info.Mid, info.Userid, 3)
	case org.NSQ_EVENT_OP_FOLLOWER_ONLY:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_OP_FOLLOWER_ONLY", info.Type, info.Mid, info.Userid, 10)
	case org.NSQ_EVENT_DELETE_USER:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_DELETE_USER", info.Type, info.Mid, info.Userid, 3)
	case org.NSQ_EVENT_RESTORE_USER:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_RESTORE_USER", info.Type, info.Mid, info.Userid, 0)
	case org.NSQ_EVENT_DELETE_MNT_BY_USER:
		err = sphinxClient.ChangeMomentStatus("NSQ_EVENT_DELETE_MNT_BY_USER", info.Type, info.Mid, info.Userid, 1)
	default:
		err = fmt.Errorf("OnMessage UnHandle Mid =%s", info.Mid)

	}

	return err
}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := common.InitLogAndOption(&options, &infoLog)

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	queueCount = cfg.Section("SERVICE_CONFIG").Key("queue_count").MustInt(10)
	chanCount := cfg.Section("CHANLIMIT").Key("handler_chan_count").MustInt(100)

	infoLog.Println("======== init consumer ================ queueCount=", queueCount)

	GlobalRecvMsgPackChan = make([]chan *MMTEvent, queueCount)

	for i := 0; i < queueCount; i++ {
		GlobalRecvMsgPackChan[i] = make(chan *MMTEvent, chanCount)

		go func(idx int) {

			for {

				info := <-GlobalRecvMsgPackChan[idx]

				filterMMTHandler(info, idx)
			}

		}(i)

	}

	Masterdb, err = common.InitMySqlFromSection(cfg, infoLog, "MASTER_MYSQL", 3, 1)
	common.CheckError(err)
	defer Masterdb.Close()

	infoLog.Println("======== NewUserInfoCacheApi ================")

	userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	infoLog.Printf("user info cache tcpcommon,ip=%v port=%v", userInfoIp, userInfoPort)
	userInfoCacheApi = tcpcommon.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &tcpcommon.HeadV2Protocol{}, 1000)

	infoLog.Println("======== NewMntApi ================")

	// init moment api
	moment_ip := cfg.Section("SERVICE_CONFIG").Key("moment_ip").MustString("127.0.0.1")
	moment_port := cfg.Section("SERVICE_CONFIG").Key("moment_port").MustString("15500")
	moment_timeout := cfg.Section("SERVICE_CONFIG").Key("moment_timeout").MustUint(1)
	moment_max_conn := cfg.Section("SERVICE_CONFIG").Key("moment_max_conn").MustInt(3)

	v2proto := &tcpcommon.HeadV2Protocol{}
	momentAPI = tcpcommon.NewMntApi(moment_ip, moment_port, time.Duration(moment_timeout)*time.Second, time.Duration(moment_timeout)*time.Second, v2proto, moment_max_conn)
	infoLog.Printf("NewMntApi,ip=%v port=%v", moment_ip, moment_port)

	infoLog.Println("======== NewTopicDbAPI sphinx ================")
	sphinx_host := cfg.Section("SERVICE_CONFIG").Key("sphinx_host").MustString("127.0.0.1")
	sphinx_port := cfg.Section("SERVICE_CONFIG").Key("sphinx_port").MustInt(9312)
	sphinx_tag_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_tag_index").MustString("momenttag")
	sphinx_moment_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_moment_index").MustString("moment_taged_list")
	sphinx_teen_moment_index := cfg.Section("SERVICE_CONFIG").Key("sphinx_teen_moment_index").MustString("moment_teen_taged_list")
	sphinx_timeout := cfg.Section("SERVICE_CONFIG").Key("sphinx_timeout").MustInt(5000)
	sphinx_open_count := cfg.Section("SERVICE_CONFIG").Key("sphinx_open_count").MustInt(100)
	sphinx_idle_count := cfg.Section("SERVICE_CONFIG").Key("sphinx_idle_count").MustInt(10)

	// 建立对象池对象
	topicDbPool = org.NewTopicDbAPI(sphinx_host, sphinx_port, sphinx_tag_index, sphinx_moment_index, sphinx_timeout, sphinx_open_count, sphinx_idle_count, Masterdb, userInfoCacheApi, infoLog)
	infoLog.Printf("NewTopicDbAPI,ip=%v port=%v", sphinx_host, sphinx_port)

	topicDbPool.SetTagedDbTable("MMT_TAGED_LIST")

	topicTeenDbPool = org.NewTopicDbAPI(sphinx_host, sphinx_port, sphinx_tag_index, sphinx_teen_moment_index, sphinx_timeout, sphinx_open_count, sphinx_idle_count, Masterdb, userInfoCacheApi, infoLog)
	topicTeenDbPool.SetTagedDbTable("MMT_TEEN_TAGED_LIST")

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
