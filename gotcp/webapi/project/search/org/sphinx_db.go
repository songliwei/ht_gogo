// Copyright 2017 Liling
//
// HelloTalk.inc

package org

import (
	//"strings"
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
	sphinx "github.com/yunge/sphinx"
)

//SphinxDbAPI handle sphinx
//Conn exposes a set of callbacks for the various events that occur on a connection
type SphinxDbAPI struct {
	SphinxClient *sphinx.Client
	MyInfo       *UserResult
	infoLog      *log.Logger
	queryIndex   string
}

//NewSphinxDbAPI 初始化一个sphinx 客户端对象
func NewSphinxDbAPI(destIP string, destPort int, queryIndex string, logger *log.Logger) (ssbAPI *SphinxDbAPI, err error) {

	sc := sphinx.NewClient().SetServer(destIP, destPort).SetConnectTimeout(5000)

	if err := sc.Error(); err != nil {
		return nil, err
	}

	if err := sc.Open(); err != nil {
		return nil, err
	}

	api := &SphinxDbAPI{
		SphinxClient: sc,
		queryIndex:   queryIndex,
		MyInfo:       &UserResult{},
		infoLog:      logger,
	}

	return api, nil
}

//SetUser 设置搜索发起人的
func (api *SphinxDbAPI) SetUser(cnf *UserResult) {

}

//GetSphinxAddfield sphinx的字段设置规则
func GetSphinxAddfield(skillArr []uint32, cmp []string) (fieldDes string) {

	all := ""
	for _, _field := range cmp {
		for _, naID := range skillArr {
			all = fmt.Sprintf(" %s %s = %d OR ", all, _field, naID)
		}
	}

	StrLen := len(all)
	if StrLen < 4 {
		return ""
	}
	all = all[:StrLen-4]
	return all
}

//Filter 搜索sphinx 过滤器入口
func (api *SphinxDbAPI) Filter(cnf *SearchOptions) (userlist []UserResult, err error) {

	api.SphinxClient.ResetFilters()

	api.SphinxClient.SetFilter("ISHIDE", []uint64{0}, false)

	timestamp := time.Now().Unix()

	if cnf.CityCdi.Country != "" {

		countryID := getCountryIDbyName(cnf.CityCdi.Country)
		fmt.Println("countryID", countryID)
		api.SphinxClient.SetFilter("COUNTRY", []uint64{countryID}, false)

		if cnf.CityCdi.Country != "" {
			api.SphinxClient.SetFilter("CITYPLACEID", cnf.CityCdi.CityPlaceID, false)
			api.SphinxClient.SetFilter("HIDECITY", []uint64{0}, false)
		}

		api.SphinxClient.SetFilter("HIDELOCATION", []uint64{0}, false)

		api.SphinxClient.SetFilterRange("LASTACTIVE", uint64(timestamp-25*24*3600), uint64(timestamp+900), false)

	}

	if cnf.Sort == "distance" {

		radLong := Deg2Rad(float64(cnf.Longitute))
		radLat := Deg2Rad(float64(cnf.Latitude))
		radius := cnf.disRadius //搜索10公里以内的地点

		api.SphinxClient.SetGeoAnchor("latitude", "longitude", float32(radLat), float32(radLong))
		api.SphinxClient.SetFilterFloatRange("@geodist", 0.0, radius, false)

		//限制15天之内的活跃用户才能出现在附近的人里面
		api.SphinxClient.SetFilterRange("LASTACTIVE", uint64(timestamp-15*24*3600), uint64(timestamp+900), false)

		//不出现在附件人的开关
		api.SphinxClient.SetFilter("absence_nearest", []uint64{0}, false)
		api.SphinxClient.SetFilter("HIDELOCATION", []uint64{0}, false)
		api.SphinxClient.SetFilter("HIDECITY", []uint64{0}, false)

		//$cl->SetMatchMode(SPH_MATCH_EXTENDED);
		api.SphinxClient.SetSortMode(sphinx.SPH_SORT_EXTENDED, "@geodist asc") // 按距离正向排序

	} else if cnf.Sort == "online" {
		//限制10天之内的活跃用户才能出现在在线状态里面
		api.SphinxClient.SetFilterRange("LASTACTIVE", uint64(timestamp-15*24*3600), uint64(timestamp+900), false)

		api.SphinxClient.SetFilter("HIDEONLINE", []uint64{0}, false)

		api.SphinxClient.SetMatchMode(sphinx.SPH_MATCH_EXTENDED)
		api.SphinxClient.SetSortMode(sphinx.SPH_SORT_ATTR_DESC, "LASTACTIVE")
	} else {
		api.SphinxClient.SetMatchMode(sphinx.SPH_MATCH_EXTENDED)
		api.SphinxClient.SetSortMode(sphinx.SPH_SORT_ATTR_DESC, "BASESCORE")
	}

	api.SetAgeRange(cnf.Age)

	if cnf.Country != "All" && cnf.Country != "" {
		countryID := getCountryIDbyName(cnf.Country)
		fmt.Println("countryID", countryID)

		api.SphinxClient.SetFilter("nationality", []uint64{countryID}, false)
	}

	querySelect := "*"

	if len(cnf.Nativelang) > 0 {

		cmp := []string{"nativelang", "teachlang2", "teachlang3"}

		fielC := GetSphinxAddfield(cnf.Nativelang, cmp)

		if "" != fielC {
			querySelect = querySelect + (",if((" + fielC + "),1,0) as naids")
			api.SphinxClient.SetFilter("naids", []uint64{1}, false)
		}
	}

	if len(cnf.Skilllevel) > 0 {

		cmp := []string{"skilllevel1", "SKILLLEVEL2", "SKILLLEVEL3"}

		fielC := GetSphinxAddfield(cnf.Skilllevel, cmp)

		if "" != fielC {
			querySelect = querySelect + (",if((" + fielC + "),1,0) as level")
			api.SphinxClient.SetFilter("level", []uint64{1}, false)
		}
	}

	if len(cnf.Learnlang) > 0 {

		cmp := []string{"LEARNLANG1", "LEARNLANG2", "LEARNLANG3"}

		fielC := GetSphinxAddfield(cnf.Learnlang, cmp)

		if "" != fielC {
			querySelect = querySelect + (",if((" + fielC + "),1,0) as leids")
			api.SphinxClient.SetFilter("leids", []uint64{1}, false)
		}
	}

	query := ""

	fmt.Println("querySelect:", querySelect)

	api.SphinxClient.SetSelect(querySelect)

	api.SphinxClient.SetLimits(0, cnf.MaxShowNum, 500, 0)

	/**/
	res, err := api.SphinxClient.Query(query, api.queryIndex, "Test Query()")

	if err != nil {
		return nil, err
	}

	fmt.Println("Time", res.Time, ",Warning:", res.Warning, ",Status:", res.Status, ",TotalFound:", res.TotalFound, ",Total:", res.Total)

	list := changeMatched2UserList(res)

	return list, nil
}

//SetAgeRange 年龄范围设置器
func (api *SphinxDbAPI) SetAgeRange(age string) {

	if age == "" || age == STRALL {
		return
	}

	max, min := GetAgeMinMax(age)

	if max > min {

		api.SphinxClient.SetFilterRange("BIRTHDAY", min, max, false)

		return
	}

	// if($this->myInfo['AGE']>=18 && $this->myInfo['AGE']<=22 ){
	if api.MyInfo.Age >= 18 {
		age = "18-99"
	} else {
		//未成年人是 0 -22 岁
		if api.MyInfo.IsTeen > 0 {
			age = "0-17"
		} else {
			age = "18-99"
		}
	}

	max, min = GetAgeMinMax(age)

	if max > min {
		api.SphinxClient.SetFilterRange("BIRTHDAY", min, min, false)
		return
	}

}

func changeMatched2UserList(result *sphinx.Result) (userlistp []UserResult) {

	userlist := make([]UserResult, result.Total)

	for _id, item := range result.Matches {

		userlist[_id].UserID = uint32(item.DocId)

		for k1, it1 := range item.AttrValues {

			//fmt.Println(result.AttrNames[k1], " = ", it1, ",", reflect.TypeOf(it1))

			switch v := it1.(type) {

			case uint32:
				val := int32(v)

				switch result.AttrNames[k1] {
				case "sex":
					userlist[_id].Sex = uint8(v)
				case "birthday":
					birthdayStr := fmt.Sprintf("%d-%d-%d", v/10000, uint32((v%10000)/100), (v % 100))
					userlist[_id].Birthday = birthdayStr
					userlist[_id].Age = GetAgeByDate(birthdayStr)
				case "voiceduration":
					userlist[_id].Voiceduration = val
				case "allowed":
					userlist[_id].Allowed = val
				case "placeid":
					userlist[_id].PlaceID = val
				case "samegender":
					userlist[_id].SameGender = val
				case "exactmatch":
					userlist[_id].ExactMatch = val
				case "country":
					userlist[_id].Country = getCountryNamebyID(val)

				case "nativelang":
					userlist[_id].Nativelang = val
				case "learnlang1":
					userlist[_id].Learnlang1 = val
				case "skilllevel1":
					userlist[_id].Skilllevel1 = val
				case "teachlang2":
					userlist[_id].Teachlang2 = val
				case "teachskilllevel2":
					userlist[_id].Teachskilllevel2 = val
				case "learnlang2":
					userlist[_id].Learnlang2 = val
				case "skilllevel2":
					userlist[_id].Skilllevel2 = val
				case "teachlang3":
					userlist[_id].Teachlang3 = val
				case "teachskilllevel3":
					userlist[_id].Teachskilllevel3 = val
				case "learnlang3":
					userlist[_id].Learnlang3 = val
				case "skilllevel3":
					userlist[_id].Skilllevel3 = val
				case "hidelocation":
					userlist[_id].Hidelocation = val
				case "hideage":
					userlist[_id].Hideage = val

				case "agemin":
					userlist[_id].AgeMin = val
				case "agemax":
					userlist[_id].AgeMax = val
				case "absencenearest":
					userlist[_id].AbsenceNearest = val
				case "findbyname":
					userlist[_id].Findbyname = val
				case "ishide":
					userlist[_id].Ishide = val
				case "vip":
					userlist[_id].Vip = val

				case "vip2":
					userlist[_id].Vip2 = val

				case "allcount":
					userlist[_id].Allcount = val

				case "correct":
					userlist[_id].Correct = val

				case "readcount":
					userlist[_id].Readcount = val

				case "transcriptioncount":
					userlist[_id].Transcriptioncount = val

				case "totaltranscount":
					userlist[_id].Totaltranscount = val
				case "nationality":
					userlist[_id].Nationality = getCountryNamebyID(val)
				case "transliterationcount":
					userlist[_id].Transliterationcount = val

				}

			case uint64:

				switch result.AttrNames[k1] {
				case "timezone48":
					userlist[_id].Timezone48 = int32(v)
				}

			case float32:

				switch result.AttrNames[k1] {
				case "latitude":
					userlist[_id].Latitude = (v)
				case "longitude":
					userlist[_id].Longitude = (v)
				}

			case string:

				switch result.AttrNames[k1] {
				case "username":
					userlist[_id].Username = v
				case "nickname":
					userlist[_id].NickName = v
				case "fullpy":
					userlist[_id].Fullpy = v
				case "shortpy":
					userlist[_id].Shortpy = v
				case "signature":
					userlist[_id].Signature = v
				case "headurl":
					userlist[_id].HeadUrl = v
				case "voiceurl":
					userlist[_id].VoiceUrl = v
				case "administrative1":
					userlist[_id].Administrative1 = v
				case "administrative2":
					userlist[_id].Administrative2 = v
				case "administrative3":
					userlist[_id].Administrative3 = v
				case "locality":
					userlist[_id].Locality = v
				case "sublocality":
					userlist[_id].Sublocality = v
				case "neighborhood":
					userlist[_id].Neighborhood = v

				}

				//fmt.Println(result.AttrNames[k1], " = ", v, ", ", reflect.TypeOf(v))

			}

		}

		userlist[_id].Points = GetLearnPoints(&userlist[_id])
		userlist[_id].City = GetCityName(&userlist[_id])

	}

	return userlist
}
