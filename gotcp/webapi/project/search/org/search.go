package org

import (
	"math"
	"strconv"
	"strings"
	"time"
)

const (
	STRALL = "All"
)

type citySearchConfig struct {
	Country     string
	Province    string
	CityPlaceID []uint64
}

// 最终的输出结果
type UserResult struct {
	UserID   uint32 `json:"UI"`
	Username string `json:"UN"`
	NickName string `json:"NK"`
	Fullpy   string `json:"FP"`
	Shortpy  string `json:"SP"`
	Sex      uint8  `json:"SE"`
	Birthday string `json:"BR"`
	Age      uint32 `json:"-"`
	IsTeen   uint8  `json:"-"`

	Signature string `json:"SG"`

	HeadUrl       string `json:"HU"`
	VoiceUrl      string `json:"VU"`
	Voiceduration int32  `json:"VD"`

	Timezone48 int32 `json:"TZ2"`

	Allowed    int32 `json:"-"`
	PlaceID    int32 `json:"-"`
	SameGender int32 `json:"-"`
	ExactMatch int32 `json:"-"`

	Nationality string `json:"NN"` //国籍

	Country string `json:"CN"` //需要数字映射到字母 ，再映射都多语言

	Latitude  float32 `json:"LA"`
	Longitude float32 `json:"LO"`

	City string `json:"CT"`

	Administrative1 string `json:"-"`
	Administrative2 string `json:"-"`
	Administrative3 string `json:"-"`
	Locality        string `json:"-"`
	Sublocality     string `json:"-"`
	Neighborhood    string `json:"-"`

	Nativelang       int32 `json:"NL"`
	Learnlang1       int32 `json:"LL"`
	Skilllevel1      int32 `json:"SL"`
	Teachlang2       int32 `json:"NL2"`
	Teachskilllevel2 int32 `json:"TL2"`
	Learnlang2       int32 `json:"LL2"`
	Skilllevel2      int32 `json:"SL2"`
	Teachlang3       int32 `json:"NL3"`
	Teachskilllevel3 int32 `json:"TL3"`
	Learnlang3       int32 `json:"LL3"`
	Skilllevel3      int32 `json:"SL3"`
	Hidelocation     int32 `json:"HL"`
	Hideage          int32 `json:"HA"`
	Hidecity         int32 `json:"HC"`
	Hideonline       int32 `json:"HO"`

	AgeMin         int32  `json:"-"`
	AgeMax         int32  `json:"-"`
	AbsenceNearest int32  `json:"-"`
	Findbyname     int32  `json:"-"`
	Ishide         int32  `json:"-"`
	Vip            int32  `json:"TL"`
	Vip2           int32  //记录到最后判断是不是终生会员
	Points         int32  `json:"-"`
	Distance       uint32 `json:"DS"`

	Allcount             int32 `json:"-"`
	Correct              int32 `json:"-"`
	Readcount            int32 `json:"-"`
	Transcriptioncount   int32 `json:"-"`
	Totaltranscount      int32 `json:"-"`
	Transliterationcount int32 `json:"-"`
}

//结果列表 , 完成该列表的相关方法就，主要是排序和过滤的内容
type UserResultList struct {
	userlist []UserResult `json:"results"`
}

//搜索的配置项目
type SearchParams struct {
	Userid     int    `form:"userid" binding:"required"`
	Age        string `form:"age"`
	Country    string `form:"country"`
	Nativelang uint32 `form:"nativelang"`

	Learnlang uint32 `form:"learnlang"`

	Skilllevel      string `form:"skilllevel"`
	SkilllevelRange string `form:"skilllevelrange"`

	CityCdi string `form:"city"`

	Sort string `form:"sort"`

	Latitude float32 `form:"latitude"`

	Longitute float32 `form:"longitude"`

	Lang string `form:"lang"`

	Version string `form:"version"`

	Terminaltype int `form:"terminaltype"`

	Page int `form:"page"`
}

//搜索的配置项目
type SearchOptions struct {
	Userid int
	//$age,$country,$nativelang,$learnlang,$skilllevel,$cityCdi=[],$maxShowNum,$sort='default'
	Age string

	Country string

	Nativelang []uint32

	Learnlang []uint32

	Skilllevel []uint32

	CityCdi citySearchConfig

	MaxShowNum int

	Sort string

	Latitude float32

	Longitute float32

	Lang string

	disRadius float32

	Version string

	Terminaltype int

	Page int
}

func SearchParamOption(param *SearchParams) (op *SearchOptions) {

	options := &SearchOptions{}

	options.Userid = param.Userid
	options.Age = param.Age
	options.Country = param.Country

	options.Nativelang = append(options.Nativelang, param.Nativelang)
	options.Learnlang = append(options.Learnlang, param.Learnlang)

	Skilllevel := "All"

	if param.Skilllevel != "" {
		Skilllevel = param.Skilllevel
	}

	if param.SkilllevelRange != "" {
		Skilllevel = param.SkilllevelRange
	}

	if Skilllevel != "" && Skilllevel != "1-5" && Skilllevel != STRALL {

		ageArr := strings.Split(Skilllevel, "-")

		age0, err0 := strconv.Atoi(ageArr[0])
		age1, err1 := strconv.Atoi(ageArr[1])

		if err1 == nil && err0 == nil {

			if age0 == age1 {
				options.Skilllevel = []uint32{uint32(age1)}
			} else {

				for j := age0; j <= age1; j++ {
					options.Skilllevel = append(options.Skilllevel, uint32(j))
				}

			}

		}

	}

	if param.CityCdi != "" && param.CityCdi != STRALL {
		cityArr := strings.Split(param.CityCdi, ",,")

		if cityArr[0] == "CN" {
			options.CityCdi.Country = cityArr[1]
		}

		if cityArr[0] == "PRO" {
			options.CityCdi.Country = cityArr[1]
			options.CityCdi.Province = cityArr[2]
		}

		if cityArr[0] == "CT" {
			options.CityCdi.Country = cityArr[2]
			citiID, err3 := strconv.Atoi(cityArr[1])

			if err3 == nil {
				options.CityCdi.CityPlaceID = append(options.CityCdi.CityPlaceID, uint64(citiID))
			}

		}

	}

	options.Latitude = param.Latitude
	options.Longitute = param.Longitute

	options.Lang = param.Lang
	options.Sort = param.Sort

	options.Version = param.Version
	options.Terminaltype = param.Terminaltype
	options.Page = param.Page
	return options
}

func Rad2Deg(radian float64) float64 {
	return radian * 180 / math.Pi
}

func Deg2Rad(deg float64) float64 {
	return deg * math.Pi / 180
}

func CalculatePythagorasEquirectangular(lat1, lon1, lat2, lon2 float64) (d float64) {
	lat1 = Deg2Rad(lat1)
	lat2 = Deg2Rad(lat2)
	lon1 = Deg2Rad(lon1)
	lon2 = Deg2Rad(lon2)
	var R = 6371.0 // km
	var x = (lon2 - lon1) * math.Cos((lat1+lat2)/2)
	var y = (lat2 - lat1)
	d = math.Sqrt(x*x+y*y) * R

	return
}

func CalculateHaversine(lat1, lon1, lat2, lon2 float64) (d float64) {
	var R = 6372.8 // Earth Radius in Kilometers

	var dLat = Deg2Rad(lat2 - lat1)
	var dLon = Deg2Rad(lon2 - lon1)
	var a = math.Sin(dLat/2)*math.Sin(dLat/2) +
		math.Cos(Deg2Rad(lat1))*math.Cos(Deg2Rad(lat2))*
			math.Sin(dLon/2)*math.Sin(dLon/2)
	var c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	d = R * c

	return
}

func GetCityName(user *UserResult) (city string) {

	if user.Locality != "" {
		return user.Locality
	}

	if user.Sublocality != "" {
		return user.Sublocality
	}

	if user.Neighborhood != "" {
		return user.Neighborhood
	}

	if user.Administrative3 != "" {
		return user.Administrative3
	}

	if user.Administrative2 != "" {
		return user.Administrative2
	}

	if user.Administrative1 != "" {
		return user.Administrative1
	}

	return ""
}

func GetLearnPoints(user *UserResult) (Points int32) {

	var allPoint int32

	allPoint += user.Allcount
	allPoint += user.Correct
	allPoint += user.Readcount
	allPoint += user.Transliterationcount
	allPoint += user.Totaltranscount
	allPoint += user.Transcriptioncount

	return allPoint

}

var G_COUNTRY_CODE map[string]uint64 = map[string]uint64{"US": 1, "UK": 2, "CN": 3, "HK": 4, "TW": 5, "JP": 6, "KR": 7, "FR": 8, "DE": 9, "IT": 10, "ES": 11, "AF": 12, "AL": 13, "DZ": 14, "AD": 15, "AO": 16, "AG": 17, "AR": 18, "AM": 19, "AU": 20, "AT": 21, "AZ": 22, "BS": 23, "BH": 24, "BD": 25, "BB": 26, "BY": 27, "BE": 28, "BZ": 29, "BJ": 30, "BT": 31, "BO": 32, "BA": 33, "BW": 34, "BR": 35, "BN": 36, "BG": 37, "BF": 38, "BI": 39, "KH": 40, "CM": 41, "CA": 42, "CV": 43, "CF": 44, "TD": 45, "CL": 46, "CO": 47, "KM": 48, "CD": 49, "CK": 50, "CR": 51, "CI": 52, "HR": 53, "CY": 54, "CU": 55, "CZ": 56, "DK": 57, "DJ": 58, "DM": 59, "DO": 60, "EC": 61, "EG": 62, "SV": 63, "GQ": 64, "ER": 65, "EE": 66, "ET": 67, "FO": 68, "FJ": 69, "FI": 70, "GA": 71, "GM": 72, "GE": 73, "GH": 74, "GR": 75, "GL": 76, "GD": 77, "GT": 78, "GG": 79, "GN": 80, "GW": 81, "GY": 82, "HT": 83, "HN": 84, "HU": 85, "IS": 86, "IN": 87, "ID": 88, "IR": 89, "IQ": 90, "IE": 91, "IL": 92, "JM": 93, "JE": 94, "JO": 95, "KZ": 96, "KE": 97, "KI": 98, "KW": 99, "KG": 100, "XK": 101, "LA": 102, "LV": 103, "LB": 104, "LS": 105, "LR": 106, "LY": 107, "LI": 108, "LT": 109, "LU": 110, "MO": 111, "MK": 112, "MG": 113, "MW": 114, "MY": 115, "MV": 116, "ML": 117, "MT": 118, "MH": 119, "MR": 120, "MU": 121, "MX": 122, "FM": 123, "MC": 124, "MZ": 125, "MD": 126, "MN": 127, "ME": 128, "MA": 129, "MM": 130, "NA": 131, "NR": 132, "NP": 133, "NL": 134, "NZ": 135, "NI": 136, "NE": 137, "NG": 138, "NU": 139, "KP": 140, "NO": 141, "OM": 142, "PK": 143, "PW": 144, "PS": 145, "PA": 146, "PG": 147, "PY": 148, "PE": 149, "PH": 150, "PL": 151, "PT": 152, "PR": 153, "QA": 154, "RO": 155, "RU": 156, "RW": 157, "KN": 158, "LC": 159, "VC": 160, "WS": 161, "SM": 162, "ST": 163, "SA": 164, "SN": 165, "RS": 166, "SC": 167, "SL": 168, "SG": 169, "SB": 170, "SK": 171, "SI": 172, "SO": 173, "ZA": 174, "LK": 175, "SD": 176, "SR": 177, "SZ": 178, "SE": 179, "CH": 180, "SY": 181, "SX": 182, "SS": 183, "TJ": 184, "TZ": 185, "TH": 186, "TL": 187, "TG": 188, "TO": 189, "TT": 190, "TN": 191, "TR": 192, "TM": 193, "TV": 194, "AE": 195, "UG": 196, "UA": 197, "UY": 198, "UZ": 199, "VE": 200, "VN": 201, "VU": 202, "YE": 203, "ZM": 204, "ZW": 205, "CG": 206, "IK": 207, "AC": 208, "AJ": 209, "AN": 210, "AI": 211, "AS": 212, "AW": 213, "AX": 214, "BL": 215, "BM": 216, "BQ": 217, "CW": 218, "CX": 219, "EH": 220, "GF": 221, "GI": 222, "GP": 223, "GU": 224, "IM": 225, "KY": 226, "MF": 227, "MP": 228, "MQ": 229, "MS": 230, "NC": 231, "PF": 232, "RE": 233, "SH": 234, "SJ": 235, "TC": 236, "VG": 237, "VI": 238, "YT": 239}

func getCountryIDbyName(country string) uint64 {

	if country == "GB" {
		country = "UK"
	}

	if v, ok := G_COUNTRY_CODE[country]; ok {
		return v
	} else {
		return 0
	}
}

var G_COUNTRY_CODE_STR []string = []string{"", "US", "UK", "CN", "HK", "TW", "JP", "KR", "FR", "DE", "IT", "ES", "AF", "AL", "DZ", "AD", "AO", "AG", "AR", "AM", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BT", "BO", "BA", "BW", "BR", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "CF", "TD", "CL", "CO", "KM", "CD", "CK", "CR", "CI", "HR", "CY", "CU", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FO", "FJ", "FI", "GA", "GM", "GE", "GH", "GR", "GL", "GD", "GT", "GG", "GN", "GW", "GY", "HT", "HN", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IL", "JM", "JE", "JO", "KZ", "KE", "KI", "KW", "KG", "XK", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MR", "MU", "MX", "FM", "MC", "MZ", "MD", "MN", "ME", "MA", "MM", "NA", "NR", "NP", "NL", "NZ", "NI", "NE", "NG", "NU", "KP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PL", "PT", "PR", "QA", "RO", "RU", "RW", "KN", "LC", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SB", "SK", "SI", "SO", "ZA", "LK", "SD", "SR", "SZ", "SE", "CH", "SY", "SX", "SS", "TJ", "TZ", "TH", "TL", "TG", "TO", "TT", "TN", "TR", "TM", "TV", "AE", "UG", "UA", "UY", "UZ", "VE", "VN", "VU", "YE", "ZM", "ZW", "CG", "IK", "AC", "AJ", "AN", "AI", "AS", "AW", "AX", "BL", "BM", "BQ", "CW", "CX", "EH", "GF", "GI", "GP", "GU", "IM", "KY", "MF", "MP", "MQ", "MS", "NC", "PF", "RE", "SH", "SJ", "TC", "VG", "VI", "YT"}

func getCountryNamebyID(id int32) (name string) {

	if id > 0 && id < int32(len(G_COUNTRY_CODE_STR)) {
		return G_COUNTRY_CODE_STR[id]
	}

	return ""

}

func GetAgeMinMax(age string) (max, min uint64) {

	ageArr := strings.Split(age, "-")

	age0, err0 := strconv.Atoi(ageArr[0])

	age1, err1 := strconv.Atoi(ageArr[1])

	if err1 != nil || err0 != nil {
		return 0, 0
	}

	now := time.Now()

	year, mon, day := now.UTC().Date()

	age1++

	age0Str := (year-age0)*10000 + (int(mon) * 100) + (day)

	age1Str := (year-age1)*10000 + (int(mon) * 100) + (day)

	max = uint64(age0Str)
	min = uint64(age1Str)
	return
}

//GetAgeByDate 得到年龄
func GetAgeByDate(date string) (Age uint32) {

	ageArr := strings.Split(date, "-")

	age0, err0 := strconv.Atoi(ageArr[0])
	age1, err1 := strconv.Atoi(ageArr[1])
	age2, err2 := strconv.Atoi(ageArr[2])

	if err0 != nil || err1 != nil || err2 != nil {
		return 0
	}

	now := time.Now()
	year, month, day := now.UTC().Date()

	yearDiff := year - age0
	monthDiff := int(month) - age1
	dayDiff := day - age2

	if monthDiff < 0 || (monthDiff == 0 && dayDiff < 0) {
		yearDiff--
	}

	return uint32(yearDiff)
}
