package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	org "github.com/HT_GOGO/gotcp/webapi/project/search/org"
	gin "gopkg.in/gin-gonic/gin.v1"
)

var (
	infoLog             *log.Logger
	SEARCH_SPHINX_HOST  string = "127.0.0.1"
	SEARCH_SPHINX_PORT  int    = 9312
	SEARCH_SPHINX_INDEX string = "dis_user"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

func filter(c *gin.Context) {

	var content org.SearchParams

	//log.Fatal("start test filter")

	if c.Bind(&content) == nil {

		option := org.SearchParamOption(&content)

		option.MaxShowNum = 300

		fmt.Printf("%#v\n", content)
		fmt.Printf("%#v\n", option)

		sphinxAPI, err := org.NewSphinxDbAPI(SEARCH_SPHINX_HOST, SEARCH_SPHINX_PORT, SEARCH_SPHINX_INDEX, infoLog)

		if err != nil {
			fmt.Println("sphinxAPI err", err)
			return
		}

		result, err := sphinxAPI.Filter(option)

		if err != nil {
			fmt.Println("sphinxAPI Filter err", err)
			return
		}

		fmt.Printf("result len %#v\n", len(result))

	} else {
		fmt.Printf("%#v\n", content)
	}

}

func main() {

	router := gin.Default()

	router.GET("/htserver/se/filter", filter)

	router.GET("/htserver/se/filter/", filter)

	s := &http.Server{
		Addr:           ":8095",
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	err := s.ListenAndServe()
	if err != nil {
		fmt.Println("ListenAndServe err:", err)
	}

}
