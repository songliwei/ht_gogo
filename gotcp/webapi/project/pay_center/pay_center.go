package main

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"database/sql"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"sort"

	"github.com/HT_GOGO/gotcp/libcrypto"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_pay_center"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_wallet"
	"github.com/HT_GOGO/gotcp/webapi/common"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/dchest/uniuri"

	// "github.com/bsm/sarama-cluster"
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/HT_GOGO/gotcp/libgenmid"
	"github.com/HT_GOGO/gotcp/webapi/project/pay_center/util"
	wechatutil "github.com/chanxuehong/util"
	"github.com/gin-gonic/gin"
	"gopkg.in/chanxuehong/wechat.v2/mch/core"
	"gopkg.in/chanxuehong/wechat.v2/mch/pay"
	// "github.com/jessevdk/go-flags"
	"encoding/json"
	"errors"
	"log"
	"net"
	"net/http"
	"net/url"
	// "reflect"
	"bytes"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/proto"
)

var (
	ErrInputParam      = errors.New("err param error")
	ErrAlreadyPurchase = errors.New("err already purchase error")
)

const (
	//应用id
	ALIPAY_GATEWAY  = "https://openapi.alipay.com/gateway.do"
	APP_ID          = "2016081001729306"
	PAY_METHOD      = "alipay.trade.app.pay"
	PAY_FORMAT      = "JSON"
	PAY_CHARSET     = "utf-8"
	SIGN_TYPE       = "RSA2"
	PAY_VERSION     = "1.0"
	RMB_CURRENCY    = "CNY"
	DOLLER_CURRENCY = "USD"
	//商户私钥，pkcs1格式、
	RSA_PRIVATE = `-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAzijHcXNgHdti7mAI5AzSoXNLA13OdIvFhGKlvlHhBqE8odkAcj39PP6tiSf5bqmcbk/juinmj0Lf40cu3dhQbCT6SlnCA65W5LEnEZnpljGH1cd/SQpL7TXW3ocZMluVlSbV8WAw3tuN+wTKUdB8K/ZiszQmNtA58Vx6iZftdiTQxSRO+9vFIxWY8yHexBNZPnK1t4B43JnSuJcJpqxIt0/8oOkDGT+3m59kiii3ytUIAVR2fMGz2nFA0Vo+dtTDjsXwfYZmBfHG6TGoLvsWZYLI/XiDtrKHE0lFCZ2a9QwkKfN39H1XOJEf+uxCokQVSIwbxJ0A3JKL1uj2RJORtwIDAQABAoIBADbN5GcmcrC9N4mDAhoNxpig25ojf9S/q1xMFR+JCLcIeePUmifEjuN+GIfTlp1yfWFd334H0YDYGEiFDdJvxMFa+yZ0wfzUhcj5xxcIwW3p8OBFznKOSCzhGSPC6lHWwucJkeo+1f8Igq/uUBJ7x+av8wEMpfQAWsbs/K5DMx8qo/1mHiU87IWyt1Ni6WYMtrShuhfS3UOg+Xlxamxl1AsQ2Pr9bt3ZTJCCa2mrH7UnGpKWUwS4KtXumKKAqbbS+drkHoy349hY0aiZIpfx66oQ4rvDSPYw7UFifE3WGyPhp+mdqtI5D9DvoyT+sgc9MFw23kEXR++HuVlLXbhTuAECgYEA8hwxxpsZpRBgtXpDpYrlMS7zuP9twQqOxCJfyNY73EvcN4r6GFjb+uHnKEi0mPCOqIt93NJCUCKWIUIZK9eDoP6M/Q0PJRDsFM89oKP3m68UVSPFbVSlPgVfAaPlvW1NqKdUq/3ESKYtho66krLtKARLl8VEfPg3/fHfvzPnh7cCgYEA2fyVnTk+tIDdT8QRnKEYVuKsO/gJeWSRo254UgnUmeZAi7Frd0vN8JksLDVeABvMaotOzbJxwo1eeto0SSUU4R4yZ9VZTIhVzsNsRKeKurDW8ep623eu5hmWAxdkkKatllowO28+zT33CdKsQAagX6drPTO45Ke7I+Bw4arwRgECgYEAim9GotoSishXk8zqGyDVNg8zuqcKuaNwyIMjI7Ijni4eKAAmSn+coNJEm0sfQIfj+klwWTA8TJfKIhAqVmAvWoHSWer6quYHJ0rsYQsATJuKU7hVbdNFfLYTgduWRH7YNcQf7Qv4DNZAF71KvxDh+8yg8JISPCat1V1U434pN/cCgYBQbTT2UUylZHZDG+AznexRAkjxx04Xj9d8KSITgrUCI0XlRIyV804EWz9FAFuyRo9hMAzfL9txIJ+fTCrGYfwWIwIN9VWBOzwNkWS83+P7hYuIri6uErZhB0el1FeyVwzs0FHDXAQbzaT75tLOvKMnJ6oVZqPtGUWeswxy9pK4AQKBgQDRBVUod318VbtX6WCh3zHSCm7PiOYuKQqjClfElGIRwMu34vaA9E0zb/a+JTpxmQC7znZc+GtYPuipVLs6TznGIdeEVtzrKwVTH9vqXj0CIx2hnBqWDhJFZS8x0B+H6Y4OLectpXkz+1pYC29Hu9jEp4cuyQAt0njFvsdCTxzLYg==
-----END RSA PRIVATE KEY-----
`
	// 支付宝（RSA）公钥 用签约支付宝账号登录ms.alipay.com后，在密钥管理页面获取。
	// 	RSA_ALIPAY_PUBLIC = `-----BEGIN RSA PUBLIC KEY-----
	// MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzijHcXNgHdti7mAI5AzSoXNLA13OdIvFhGKlvlHhBqE8odkAcj39PP6tiSf5bqmcbk/juinmj0Lf40cu3dhQbCT6SlnCA65W5LEnEZnpljGH1cd/SQpL7TXW3ocZMluVlSbV8WAw3tuN+wTKUdB8K/ZiszQmNtA58Vx6iZftdiTQxSRO+9vFIxWY8yHexBNZPnK1t4B43JnSuJcJpqxIt0/8oOkDGT+3m59kiii3ytUIAVR2fMGz2nFA0Vo+dtTDjsXwfYZmBfHG6TGoLvsWZYLI/XiDtrKHE0lFCZ2a9QwkKfN39H1XOJEf+uxCokQVSIwbxJ0A3JKL1uj2RJORtwIDAQAB"
	// -----END RSA PUBLIC KEY-----
	// `
	RSA_ALIPAY_PUBLIC = `-----BEGIN RSA PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4C/3bxmqowjMsSsYO7KDk0X6vD3+szLRDBMfChCLtQkbRO3MsgZby7NJXRXRd/YddOfj2raAy+0G0HBeFrdpeuLps+rXg18uNfT1SOCxT2j45oa2Qataft3TSDJTCmwhQ6ttebacYeSOfR3IxxILSeXIoeJRKPJH3Kev6z9RxWffZ7tD8wTe4bIph2v/apJGaL4pA8eeHrWgU/Yo96v5nqOxxpiGzub/7YZ9otgt9EaeBJz3LkWJFBKwjP4V3NkXBkagsoURROISLzDkCbXTcEz+JHNLLgOzOfDmIE11JS3x1ev8vsMnA5+7pFIMIus1IWmAbN0vwvolrfF+ung51wIDAQAB
-----END RSA PUBLIC KEY-----
`
)

const (
	PURCHASE_PWD                 = "f2e40870103240f7b485b6f186fa141a"
	PACKAGE_NAME                 = "com.hellotalk"
	WECHAT_PAY_APPID             = "wxd061f34c48968028"
	WECHAT_PAY_COMMERCIAL_TENANT = "1233820102"
	WECHAT_PAY_SIGN_KEY          = "B3A32EF18D804B1BA5B8CA07BEBCBE3E"
	// WECHAT_CALL_BACK             = "https://www.hellotalk.com/htserver/wechatpay"
	WECHAT_QUERY_ORDER_URL   = "https://api.mch.weixin.qq.com/pay/orderquery"
	WECHAT_TRADE_TYPE        = "APP"
	WECHAT_SUCCESS           = "SUCCESS"
	WECHAT_PACKAGE           = "Sign=WXPay"
	WECHAT_ORDER_EXPIRE_HOUR = 2
	HTTP_TIME_OUT            = 8
)

const (
	WECHAT_PAY_TYPE = 0
	ALIPAY_PAY_TYPE = 1
)

const (
	KCC2018          = "bin/cc2018"
	KCC2018KeyLength = 16
)

var (
	infoLog                 *log.Logger
	options                 common.Options
	walletApi               *tcpcommon.WalletApi
	userinfoRedis           *tcpcommon.RedisApi
	dbUtil                  *util.DbUtil
	weChatClient            *core.Client
	aliPayNotifyUrl         string
	wechatPayNotifyUrl      string
	brainTreeTokenUrl       string
	brainTreeBackupTokenUrl string
	brainTreePayUrl         string
	brainTreeBackupPayUrl   string
	brainTreePayUser        map[uint32]bool
	alipayStatCodeMap       map[string]int32
	wechatpayStatCodeMap    map[string]int32
	brainTreeEnv            string
)

type GetBrainTreeTokenReq struct {
	UserId       uint32  `json:"userid" binding:"required"`
	ProductId    string  `json:"product_id" binding:"required"`
	Ts           uint64  `json:"t" binding:"required"`
	Amount       float64 `json:"amount" binding:"required"`
	Currency     string  `json:"currency" binding:"required"`
	TerminalType uint32  `json:"terminaltype" binding:"required"`
	Version      string  `json:"version" binding:"required"`
	Env          string  `json:"env" binding:"required"`
}

type GetBrainTreeTokenRsp struct {
	Status    uint32   `json:"status" binding:"required"`
	Token     string   `json:"token" binding:"omitempty"`
	Env       string   `json:"env" binding:"omitempty"`
	Price     uint64   `json:"price" binding:"omitempty"`
	Currency  string   `json:"currency" binding:"omitempty"`
	Message   string   `json:"message" binding:"omitempty"`
	PayMethod []string `json:"pay_method" binding:"omitempty"`
}

type SdkInfoStru struct {
	TypeLebel string `json:"type_label" binding:"required"`
}

type BrainTreePayReq struct {
	UserId    uint32      `json:"userid" binding:"required"`
	Token     string      `json:"token" binding:"required"`
	Nonce     string      `json:"nonce" binding:"required"`
	Amount    float64     `json:"amount" binding:"required"`
	Currency  string      `json:"currency" binding:"required"`
	Env       string      `json:"env" binding:"required"`
	ProductId string      `json:"product_id" binding:"required"`
	UserName  string      `json:"user_name" binding:"required"`
	PayMethod string      `json:"pay_method" binding:"required"`
	Ts        uint64      `json:"t" binding:"required"`
	SdkInfo   SdkInfoStru `json:"sdk_info" binding:"required"`
}

type BrainTreePayRsp struct {
	Status  uint32 `json:"status" binding:"required"`
	Message string `json:"message" binding:"omitempty"`
}

type AliPayNotify struct {
	NotifyTime string `json:"notify_time" binding:"required"`
	NotifyType string `json:"notify_type" binding:"required"`
	NotifyId   string `json:"notify_id" binding:"required"`
	AppId      string `json:"app_id" binding:"required"`
	Chatset    string `json:"chatset" binding:"required"`
	Version    string `json:"version" binding:"required"`
	SignType   string `json:"sign_type" binding:"required"`
	Sign       string `json:"sign" binding:"required"`
	TradeNo    string `json:"trade_no" binding:"required"`
	OutTradeNo string `json:"out_trade_no" binding:"required"`

	OutBizNo          string  `json:"out_biz_no" binding:"omitempty"`
	BuyerId           string  `json:"buyer_id" binding:"omitempty"`
	BuyerLogonId      string  `json:"buyer_logon_id" binding:"omitempty"`
	SellerId          string  `json:"seller_id" binding:"omitempty"`
	SellerEmail       string  `json:"seller_email" binding:"omitempty"`
	TradeStatus       string  `json:"trade_status" binding:"omitempty"`
	TotalAmount       float64 `json:"total_amount" binding:"omitempty"`
	ReceiptAmount     float64 `json:"receipt_amount" binding:"omitempty"`
	InvoiceAmount     float64 `json:"invoice_amount" binding:"omitempty"`
	BuyerPayAmount    float64 `json:"buyer_pay_amount" binding:"omitempty"`
	PointAmount       float64 `json:"point_amount" binding:"omitempty"`
	RefundFee         float64 `json:"refund_fee" binding:"omitempty"`
	Subject           string  `json:"subject" binding:"omitempty"`
	Body              string  `json:"body" binding:"omitempty"`
	GmtCreate         string  `json:"gmt_create" binding:"omitempty"`
	GmtPayment        string  `json:"gmt_payment" binding:"omitempty"`
	GmtRefund         string  `json:"gmt_refund" binding:"omitempty"`
	GmtClose          string  `json:"gmt_close" binding:"omitempty"`
	FundBillList      string  `json:"fund_bill_list" binding:"omitempty"`
	PassbackParams    string  `json:"passback_params" binding:"omitempty"`
	VoucherDetailList string  `json:"voucher_detail_list" binding:"omitempty"`
}

func getUserInfo(userid uint32) (jsonObj *simplejson.Json, err error) {
	key := common.GetUserInfoRedisKey(userid)
	// infoLog.Println("GetUserinfo key :", key)
	val, err := userinfoRedis.Get(key)
	if err != nil {
		return nil, err
	}

	jsonObj, err = simplejson.NewJson([]byte(val))
	if err != nil {
		return nil, err
	}
	infoLog.Println("GetUserinfo userid:%d result:%+v", userid, jsonObj)
	return jsonObj, nil
}

func transferMsec(timeStr uint64) (str string) {
	msec := timeStr - (timeStr/1000)*1000
	nsec := msec * 1000 * 1000
	sec := (timeStr - msec) / 1000
	return time.Unix(int64(sec), int64(nsec)).Format(time.RFC3339)
}

func GetClientIp(c *gin.Context) (ip string, err error) {
	if c == nil {
		err = ErrInputParam
		return ip, err
	}
	// infoLog.Printf("GetClientIp=%#v", c.Request.Header)
	if (c.GetHeader("X-Wns-Qua") != "" || c.GetHeader("X-Wns-DeviceInfo") != "" || c.GetHeader("X-Wns-Wid") != "") &&
		c.GetHeader("X-Forwarded-For") != "" {
		ip = c.GetHeader("X-Forwarded-For")
		// infoLog.Printf("GetClientIp c.GetHeader x-forwarded-for ip=%s", ip)
		return ip, nil
	} else {
		ip, _, err = net.SplitHostPort(strings.TrimSpace(c.Request.RemoteAddr))
		// infoLog.Printf("GetClientIp c.Request.RemoteAddr ip=%s", ip)
		return ip, err
	}
}

func GetSignString(params map[string]string) (sign string, keys []string, err error) {
	if len(params) == 0 {
		infoLog.Printf("GetSignString params empty")
		err = ErrInputParam
		return sign, keys, err
	}
	// Step1: 删除输入参数中的sign参数
	delete(params, "sign")
	// Step2: 删除为空的参数并将参数
	// var keys []string
	for k, v := range params {
		if v != "" {
			keys = append(keys, k)
		}
	}
	// Step3: 对参数进行排序
	sort.Strings(keys)
	for i, k := range keys {
		if i == 0 {
			sign = k + "=" + params[k]
		} else {
			sign = sign + "&" + k + "=" + params[k]
		}
	}
	return sign, keys, nil
}

func GetAliPaySign(params map[string]string) (sign string, keys []string, err error) {
	sign, keys, err = GetSignString(params)
	if err != nil {
		return sign, keys, err
	}
	// infoLog.Printf("GetAliPaySign origin sign=%s", sign)
	//对排序后的数据进行rsa2加密，获得sign
	b, _ := rsaEncrypt([]byte(sign))
	sign = base64.StdEncoding.EncodeToString(b)
	// infoLog.Printf("GetAliPaySign base64 encode sing=%s", sign)
	return sign, keys, nil
}

func rsaEncrypt(origData []byte) (sign []byte, err error) {
	key := RSA_PRIVATE
	block, _ := pem.Decode([]byte(key)) //PiravteKeyData为私钥文件的字节数组
	if block == nil {
		infoLog.Printf("rsaEncrypt block is empty abort")
		err = ErrInputParam
		return nil, err
	}
	//priv即私钥对象,block2.Bytes是私钥的字节流
	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		infoLog.Printf("rsaEncrypt impoisile recover private key err=%s", err)
		return nil, err
	}
	h := sha256.New()
	h.Write(origData)
	hashed := h.Sum(nil)
	signature, err := rsa.SignPKCS1v15(rand.Reader, privateKey,
		crypto.SHA256, hashed) //签名
	return signature, err
}

func rsaVerifyAlipay(signStr, verifySign string) (err error) {
	// 校验阿里通知RSA签名
	h := sha256.New()
	h.Write([]byte(signStr))
	hashed := h.Sum(nil)

	// 解析公钥
	block, _ := pem.Decode([]byte(RSA_ALIPAY_PUBLIC))
	if block == nil {
		infoLog.Printf("decode alipay public key err:%v", err)
		err = ErrInputParam
		return err
	}
	alipayRsaPublicKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		infoLog.Printf("ParsePKIXPublicKey alipay public key err:%v", err)
		return err
	}

	signBase64, err := base64.StdEncoding.DecodeString(verifySign)
	if err != nil {
		infoLog.Printf("rsaVerifyAlipay base64decode verifySign fail, verifySign:%s err:%+v", verifySign, err)
		return err
	}
	err = rsa.VerifyPKCS1v15(alipayRsaPublicKey.(*rsa.PublicKey), crypto.SHA256, hashed, signBase64)
	if err != nil {
		infoLog.Printf("rsaVerifyAlipay match sign fail, err:%+v", err)
		return err
	}
	return nil
}

func PrepareAliPayParams(reqUid uint32, transId uint64, productId uint64, rmbAmount uint64) (strParam string, err error) {
	params := map[string]string{}
	params["app_id"] = APP_ID
	params["method"] = PAY_METHOD
	params["format"] = PAY_FORMAT
	params["charset"] = PAY_CHARSET
	params["sign_type"] = SIGN_TYPE
	timeStr := time.Now().UTC().Format("2006-01-02 15:04:05")
	// infoLog.Printf("PrepareAliPayParams timeStr=%s", timeStr)
	params["timestamp"] = timeStr
	params["version"] = PAY_VERSION
	params["notify_url"] = aliPayNotifyUrl
	bizContent := simplejson.New()
	bizContent.Set("body", "purchase lesson"+fmt.Sprintf("%v", productId))
	bizContent.Set("subject", "purchase lesson")
	bizContent.Set("out_trade_no", fmt.Sprintf("%v", transId))
	bizContent.Set("total_amount", fmt.Sprintf("%.2f", float64(rmbAmount)/1000)) // 之前计算的都是实际金额*1000 所以除以1000才是实际金额
	// bizContent.Set("total_amount", "0.02") // 之前计算的都是实际金额*1000 所以除以1000才是实际金额
	bizContent.Set("product_code", "QUICK_MSECURITY_PAY")
	bizContentSlic, err := bizContent.MarshalJSON()
	if err != nil {
		infoLog.Printf("simpleJson.MarshalJSON failed err=%s", err)
		return strParam, err
	}
	infoLog.Printf("bizContentSlic=%s", bizContentSlic)
	params["biz_content"] = string(bizContentSlic)
	// Step1: 获取签名
	sign, keys, err := GetAliPaySign(params)
	if err != nil {
		infoLog.Printf("GetAliPaySign failed reqUid=%v transId=%v productId=%v err=%s",
			reqUid,
			transId,
			productId,
			err)
		return strParam, err
	}
	// Step2: 将签名插入参数列表中
	// params["sign"] = sign

	// Step3: 将参数拼接成完整的字符串
	for k, v := range keys {
		if k == 0 {
			strParam += v + "=" + url.QueryEscape(params[v])
		} else {
			strParam += "&" + v + "=" + url.QueryEscape(params[v])
		}
	}

	strParam += "&sign=" + url.QueryEscape(sign)

	infoLog.Printf("GetAliPaySign reqUid=%v transId=%v productId=%v rmbAmount=%v strParam=%s",
		reqUid,
		transId,
		productId,
		rmbAmount,
		strParam)
	return strParam, nil
}

func HandleAliPay(reqPayLoad []byte) (rspPayLoad []byte, err error) {
	if len(reqPayLoad) == 0 {
		infoLog.Printf("HandleAliPay rspPayLoad empty")
		err = ErrInputParam
		return rspPayLoad, err
	}
	reqBody := new(ht_pay_center.GetAliPayTransParamsReqBody)
	err = proto.Unmarshal(reqPayLoad, reqBody)
	if err != nil {
		infoLog.Printf("HandleAliPay proto Unmarshal failed err=%s", err)
		return rspPayLoad, err
	}

	var rmbAmount uint64

	reqUid := reqBody.GetReqUid()
	productId := reqBody.GetProductId()
	currence := reqBody.GetCurrenceCode()
	amount := reqBody.GetAmount()
	transId := libgenmid.GenMid()
	// strTransId := fmt.Sprintf("%s", transId)
	infoLog.Printf("HandleAliPay reqUid=%v productId=%v currence=%s amount=%v transId=%s", reqUid, productId, currence, amount, transId)
	// Step1: 首先查询商品的详情
	chargeInof, err := walletApi.GetChargingInfoById(reqUid, productId)
	if err != nil {
		infoLog.Printf("HandleAliPay walletApi.GetChargingInfoById reqUid=%v productId=%v currence=%s amount=%v transId=%s failed err=%s",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			err)
		return nil, err
	}

	if currence == RMB_CURRENCY {
		// 如果是支付币种是人民币，不用汇率转换
		rmbAmount = amount
	} else {
		// 如果是支付币种比是人民币，需要转成美元，再转回人民币
		// Step2: 然后查询人民币对美元汇率
		rate, err := walletApi.GetExchangeRate(reqUid, RMB_CURRENCY)
		if err != nil {
			infoLog.Printf("HandleAliPay walletApi.GetExchangeRate reqUid=%v productId=%v currence=%s amount=%v transId=%s failed err=%s",
				reqUid,
				productId,
				currence,
				amount,
				transId,
				err)
			return nil, err
		}
		// Step3: 换算之后实际扣除的人民币金额
		rmbAmount = uint64(float64(chargeInof.GetPayCurrency().GetCorrespondingDollar()) / float64(rate) * 1000)
		infoLog.Printf("HandleAliPay after exchange rate reqUid=%v productId=%v currence=%s amount=%v transId=%s rmbAmpunt=%v",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			rmbAmount)
	}
	// Step4: 写购买记录
	err = dbUtil.InsertAliPayPurchaseOrder(transId,
		productId,
		reqUid,
		chargeInof.GetOpUid(),
		RMB_CURRENCY,
		rmbAmount,
		chargeInof.GetPayCurrency().GetCurrencyType(),
		chargeInof.GetPayCurrency().GetAmount(),
		chargeInof.GetPayCurrency().GetCorrespondingDollar())
	if err != nil {
		infoLog.Printf("HandleAliPay dbUtil.InsertPurchaseOrder failed reqUid=%v productId=%v currence=%s amount=%v transId=%s rmbAmpunt=%v err=%s",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			rmbAmount,
			err)
		return nil, err
	}
	// Step5: 获取响应参数
	strParams, err := PrepareAliPayParams(reqUid, transId, productId, rmbAmount)
	if err != nil {
		infoLog.Printf("HandleAliPay PrepareAliPayParams failed reqUid=%v productId=%v currence=%s amount=%v transId=%s rmbAmpunt=%v err=%s",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			rmbAmount,
			err)
		return nil, err
	}
	// 调用预支付

	// Step6: 返回序列化之后的响应
	ret := uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_SUCCESS)
	rspBody := &ht_pay_center.GetAliPayTransParamsRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: []byte("success"),
		},
		ParamStr: proto.String(strParams),
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("GenerateAliPayRetCode message proto.Marshal failed reqUid=%v productId=%v currence=%s amount=%v transId=%s rmbAmpunt=%v err=%s",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			rmbAmount,
			err)
		return nil, err
	}
	return rspPayLoad, nil
}

func HandleAliPayResult(reqPayLoad []byte) (rspPayLoad []byte, err error) {
	if len(reqPayLoad) == 0 {
		infoLog.Printf("HandleAliPayResult reqPayLoad empty")
		err = ErrInputParam
		return rspPayLoad, err
	}

	reqBody := new(ht_pay_center.CommitAliPayResultReqBody)
	err = proto.Unmarshal(reqPayLoad, reqBody)
	if err != nil {
		infoLog.Printf("HandleAliPayResult proto Unmarshal failed err=%s", err)
		return rspPayLoad, err
	}

	productId := reqBody.GetProductId()
	reqUid := reqBody.GetReqUid()
	payRet := reqBody.GetPayResult()
	userName := reqBody.GetUserName()
	headUrl := reqBody.GetHeadUrl()
	national := reqBody.GetNational()
	infoLog.Printf("reqBody info: productid :%v  reqUid: %v national:%v, headUrl:%v  payRet:%v", productId, reqUid, national, headUrl, payRet)

	// 解析支付宝回调内容
	res, err := simplejson.NewJson([]byte(payRet))
	if err != nil {
		infoLog.Printf("HandleAliPayResult json decode fail: %s \n err:%s", payRet, err)
		return rspPayLoad, err
	}
	// infoLog.Printf("HandleAliPayResult json decode success: %+v", res)

	// Step0: 检查支付宝返回代码是否支付成功
	resultStatus, err := res.Get("resultStatus").String()
	if err != nil {
		infoLog.Printf("HandleAliPayResult parse resultStatus fail: %+v", err)
		return nil, err
	}
	// 只有9000才是支付调用成功的
	if resultStatus != "9000" {
		if val, ok := alipayStatCodeMap[resultStatus]; ok {
			ret := uint32(val)
			reason, err := res.Get("memo").Bytes()
			if err != nil {
				infoLog.Printf("HandleAliPayResult parse memo fail: %+v", err)
				return nil, err
			}
			rspPayLoad, err = GenerateAliPayResultRetCode(ret, reason)
			return rspPayLoad, nil
		} else {
			infoLog.Printf("HandleAliPayResult unknown resultCode: %+v", resultStatus)
			return GenerateAliPayResultRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_NOT_CHANGE), []byte("Unknown resultCode"))
		}
	}

	resultByte, err := res.Get("result").Bytes()
	if err != nil {
		infoLog.Printf("HandleAliPayResult parse result fail: %+v", err)
		return nil, err
	}
	result, err := simplejson.NewJson(resultByte)
	if err != nil {
		infoLog.Printf("HandleAliPayResult parse resultByte fail: %+v", err)
		return nil, err
	}
	payResponse, err := result.Get("alipay_trade_app_pay_response").Map()
	if err != nil {
		infoLog.Printf("HandleAliPayResult parse payResponse fail: %+v", err)
		return nil, err
	}
	infoLog.Printf("HandleAliPayResult payResponse succ: %+v", payResponse)

	transId := payResponse["out_trade_no"].(string)

	// Step1: 首先查询商品的详情
	chargeInfo, err := walletApi.GetChargingInfoById(reqUid, productId)
	if err != nil {
		infoLog.Printf("HandleAliPayResult walletApi.GetChargingInfoById reqUid=%v productId=%v failed err=%s",
			reqUid,
			productId,
			err)
		return nil, err
	}
	// infoLog.Printf("HandleAliPayResult chargeInfo doller=%v", chargeInfo.GetPayCurrency().GetCorrespondingDollar())
	// 检查用户是否已经支付过
	alreadyPurchase := false
	for _, v := range chargeInfo.GetPayUserList() {
		if v.GetUid() == reqUid {
			alreadyPurchase = true
			break
		}
	}
	if alreadyPurchase {
		infoLog.Printf("HandleAliPayResult reqUid=%v productId=%v payEntrance=%v already purchase", reqUid, productId, "Aipay")
	} else {
		// 如果支付成功了则发起用户已经付费成功
		code, err := walletApi.SuccessPurchaseLesson(
			reqUid,
			userName,
			headUrl,
			national,
			uint64(time.Now().Unix()),
			productId,
			uint32(ht_wallet.AccountType_OTHER_ACCOUNT),
			fmt.Sprintf("%v", transId),
			"Alipay")
		if err != nil || code != 0 {
			infoLog.Printf("HandleAliPayResult walletApi.SuccessPurchaseLesson reqUid=%v userName=%s transId=%v productId=%v err=%s",
				reqUid,
				userName,
				transId,
				productId,
				err)
			attr := "gopaycenter/success_pur_lesson_failed"
			libcomm.AttrAdd(attr, 1)
			return nil, err
		}
	}
	ret := uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_SUCCESS)
	rspPayLoad, err = GenerateAliPayResultRetCode(ret, []byte("success"))
	return rspPayLoad, err
}

func HandleAliPayNotify(notifyParam url.Values) (result string, err error) {
	// 计算签名
	signParam := make(map[string]string, 32)

	outTradeNo := notifyParam["out_trade_no"][0]
	notifySign := notifyParam["sign"][0]
	tradeStatus := notifyParam["trade_status"][0]

	for k, v := range notifyParam {
		if k == "sign" || k == "sign_type" {
			continue
		}
		signParam[k], err = url.QueryUnescape(v[0])
		if err != nil {
			infoLog.Printf("HandleAliPayNotify QueryUnescape fail, value:%+v err:%+v", v, err)
			return "FAIL", err
		}
		// infoLog.Printf("HandleAliPayNotify param, key:%s value:%s, QueryUnescape:%s", k, v[0], signParam[k])
	}

	sign, keys, err := GetSignString(signParam)
	// infoLog.Printf("HandleAliPayNotify GetAliPaySign sign:%s  keys:%+v err:%+v", sign, keys, err)
	if err != nil {
		infoLog.Printf("HandleAliPayNotify GetAliPaySign fail, sign:%s  keys:%+v err:%+v", sign, keys, err)
		return result, err
	}
	// 校验签名
	err = rsaVerifyAlipay(sign, notifySign)
	if err != nil {
		return "FAIL", nil
	}

	// 这两个状态可以不处理
	if tradeStatus == "TRADE_FINISHED" || tradeStatus == "TRADE_CLOSED" {
		infoLog.Printf("HandleAliPayNotify TradeStatus:%s return success", tradeStatus)
		return "success", nil
	}
	// 异常状态
	if tradeStatus != "TRADE_SUCCESS" {
		return "FAIL", nil
	}

	// 剩下只有交易成功了 TRADE_SUCCESS
	// 从数据库里根据transId 查询记录
	transId, err := strconv.ParseUint(outTradeNo, 10, 64)
	if err != nil {
		infoLog.Printf("HandleAliPayNotify ParseInt outTradeNo fail: outTradeNo=%s, err=%s", outTradeNo, err)
		return "FAIL", nil
	}
	record, err := dbUtil.GetAliPayOrderByTransId(transId)
	if err != nil {
		infoLog.Printf("HandleAliPayNotify GetAliPayOrderByTransId fail: outTradeNo=%s, err=%s", outTradeNo, err)
		return "FAIL", nil
	}
	infoLog.Printf("HandleAliPayNotify GetAliPayOrderByTransId: %+v", record)

	// 获取用户信息
	userInfo, err := getUserInfo(record.PayId)
	if err != nil {
		infoLog.Printf("HandleAliPayNotify getUserInfo fail: userid=%s  err=%+v", record.PayId, err)
		return "FAIL", nil
	}
	infoLog.Printf("HandleAliPayNotify getUserInfo success:%+v", userInfo)

	// 首先查询商品的详情
	chargeInfo, err := walletApi.GetChargingInfoById(record.PayId, record.ChargingId)
	if err != nil {
		infoLog.Printf("HandleAliPayNotify walletApi.GetChargingInfoById reqUid=%v productId=%v failed err=%s",
			record.PayId,
			record.ChargingId,
			err)
		return "FAIL", err
	}
	// 检查用户是否已经支付过
	// alreadyPurchase := false
	for _, v := range chargeInfo.GetPayUserList() {
		if v.GetUid() == record.PayId {
			// alreadyPurchase = true
			infoLog.Printf("HandleAliPayNotify reqUid=%v productId=%v payEntrance=%v already purchase", record.PayId, record.ChargingId, "Aipay")
			return "success", nil
		}
	}

	// 如果支付成功了则发起用户已经付费成功
	code, err := walletApi.SuccessPurchaseLesson(
		record.PayId,
		fmt.Sprintf("%s", userInfo.Get("NK").MustString()), // userName
		fmt.Sprintf("%s", userInfo.Get("HU").MustString()), // 头像
		fmt.Sprintf("%s", userInfo.Get("NN").MustString()), // 国籍
		uint64(time.Now().Unix()),
		record.ChargingId,
		uint32(ht_wallet.AccountType_OTHER_ACCOUNT),
		outTradeNo,
		"Alipay")
	if err != nil || code != 0 {
		infoLog.Printf("HandleAliPayNotify walletApi.SuccessPurchaseLesson reqUid=%v userName=%s transId=%v productId=%v err=%s",
			record.PayId,
			fmt.Sprintf("%s", userInfo.Get("NK")),
			outTradeNo,
			record.ChargingId,
			err)
		attr := "gopaycenter/success_pur_lesson_failed"
		libcomm.AttrAdd(attr, 1)
		return "FAIL", err
	}

	infoLog.Printf("HandleAliPayNotify PurchaseLesson success reqUid=%v userName=%s transId=%v productId=%v", record.PayId, fmt.Sprintf("%s", userInfo.Get("NK").MustString()), outTradeNo, record.ChargingId)
	return "success", nil
}

func GenerateAliPayResultRetCode(ret uint32, msg []byte) (rspPayLoad []byte, err error) {
	rspBody := &ht_pay_center.CommitAliPayResultRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: msg,
		},
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("CommitAliPayResultRspBody message proto.Marshal failed err=%s", err)
	}
	return rspPayLoad, err
}

func GenerateAliPayRetCode(ret uint32, errMsg []byte) (rspPayLoad []byte, err error) {
	rspBody := &ht_pay_center.GetAliPayTransParamsRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: errMsg,
		},
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("GenerateAliPayRetCode message proto.Marshal failed err=%s", err)
	}
	return rspPayLoad, err
}

func GenerateWeChatTime() (timeStart, timeExpire string) {
	curTime := time.Now()
	// 订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
	timeStart = fmt.Sprintf("%4d%02d%02d%02d%02d%02d", curTime.Year(), curTime.Month(), curTime.Day(), curTime.Hour(), curTime.Minute(), curTime.Second())
	// 订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。其他详见时间规则
	timeExpire = fmt.Sprintf("%4d%02d%02d%02d%02d%02d", curTime.Year(), curTime.Month(), curTime.Day(), curTime.Hour()+WECHAT_ORDER_EXPIRE_HOUR, curTime.Minute(), curTime.Second())
	return timeStart, timeExpire
}

// Notice: 微信的transationId 是32位的
func PrepareWeChatPayParams(reqUid uint32, transId uint64, productId uint64, rmbAmount uint64, clientIp string, deviceInfo string) (prepayId string,
	tradeType string,
	noncestr string,
	sign string,
	timestamp string,
	err error) {
	infoLog.Printf("PrepareWeChatPayParams reqUid=%v transId=%v productId=%v rmbAmount=%v clientIp=%s deviceInfo=%s",
		reqUid,
		transId,
		productId,
		rmbAmount,
		clientIp,
		deviceInfo)
	if reqUid == 0 || transId == 0 || productId == 0 || rmbAmount == 0 {
		infoLog.Printf("PrepareWeChatPayParams reqUid=%v transId=%v productId=%v rmbAmount=%v invalid param",
			reqUid,
			transId,
			productId,
			rmbAmount)
		err = ErrInputParam
		return prepayId, tradeType, noncestr, sign, timestamp, err
	}
	timeStart, timeExpire := GenerateWeChatTime()
	timestamp = fmt.Sprintf("%v", time.Now().Unix())
	orderRequest := make(map[string]string)
	orderRequest["appid"] = WECHAT_PAY_APPID
	orderRequest["mch_id"] = WECHAT_PAY_COMMERCIAL_TENANT
	orderRequest["device_info"] = deviceInfo
	orderRequest["nonce_str"] = uniuri.New()
	orderRequest["sign_type"] = core.SignType_MD5
	orderRequest["body"] = "purchase lesson" + fmt.Sprintf("%v", productId)
	orderRequest["detail"] = "purchase lesson" + fmt.Sprintf("%v", productId)
	// orderRequest["attach"] = ""
	orderRequest["out_trade_no"] = fmt.Sprintf("%v", transId)
	orderRequest["fee_type"] = RMB_CURRENCY
	orderRequest["total_fee"] = fmt.Sprintf("%v", rmbAmount)
	// orderRequest["total_fee"] = fmt.Sprintf("%v", 2)
	orderRequest["spbill_create_ip"] = clientIp
	orderRequest["time_start"] = timeStart
	orderRequest["time_expire"] = timeExpire
	// orderRequest["notify_url"] = WECHAT_CALL_BACK
	orderRequest["notify_url"] = wechatPayNotifyUrl
	orderRequest["trade_type"] = WECHAT_TRADE_TYPE
	resp, err := pay.UnifiedOrder(weChatClient, orderRequest)
	if err != nil {
		infoLog.Printf("PrepareWeChatPayParams pay.UnifiedOrder2 reqUid=%v transId=%v productId=%v rmbAmount=%v err=%s",
			reqUid,
			transId,
			productId,
			rmbAmount,
			err)
		return prepayId, tradeType, noncestr, sign, timestamp, err
	}
	infoLog.Printf("PrepareWeChatPayParams reqUid=%v transId=%v productId=%v rmbAmount=%v notify_url=%v resp=%#v",
		reqUid,
		transId,
		productId,
		rmbAmount,
		wechatPayNotifyUrl,
		resp)
	returnCode := resp["return_code"]
	resultCode := resp["result_code"]
	if returnCode == WECHAT_SUCCESS && resultCode == WECHAT_SUCCESS {
		tradeType = resp["trade_type"]
		prepayId = resp["prepay_id"]
		noncestr = resp["nonce_str"]
		signParam := map[string]string{
			"appid":     WECHAT_PAY_APPID,
			"partnerid": WECHAT_PAY_COMMERCIAL_TENANT,
			"prepayid":  prepayId,
			"package":   WECHAT_PACKAGE,
			"noncestr":  noncestr,
			"timestamp": timestamp,
		}
		sign = core.Sign(signParam, WECHAT_PAY_SIGN_KEY, nil)
		return prepayId, tradeType, noncestr, sign, timestamp, nil
	} else {
		err = util.ErrInternalErr
		return prepayId, tradeType, noncestr, sign, timestamp, err
	}
	// resp=map[string]string{"appid":"wxd061f34c48968028",
	// "nonce_str":"PZu5YcnfpoEtSL1K",
	// "trade_type":"APP",
	// "device_info":"56ded3d1bb9755a21cfc28453d981c1d",
	// "sign":"53ADEA96E0AFDE2A988BD6738B7EFDF0",
	// "result_code":"SUCCESS",
	// "prepay_id":"wx20180222181658de99cb95b30514702901",
	// "return_code":"SUCCESS",
	// "return_msg":"OK",
	// "mch_id":"1233820102"}
}

func HandleWeChatPay(reqPayLoad []byte, clientIp string) (rspPayLoad []byte, err error) {
	if len(reqPayLoad) == 0 {
		infoLog.Printf("HandleWeChatPay reqPayLoad empty")
		err = ErrInputParam
		return rspPayLoad, err
	}
	reqBody := new(ht_pay_center.GetWeChatPayTransParamsReqBody)
	err = proto.Unmarshal(reqPayLoad, reqBody)
	if err != nil {
		infoLog.Printf("HandleWeChatPay proto Unmarshal failed err=%s", err)
		return rspPayLoad, err
	}

	var rmbAmount uint64

	reqUid := reqBody.GetReqUid()
	productId := reqBody.GetProductId()
	currence := reqBody.GetCurrenceCode()
	amount := reqBody.GetAmount()
	deviceInfo := reqBody.GetDeviceInfo()
	transId := uint64(libgenmid.GenMid())
	infoLog.Printf("HandleWeChatPay reqUid=%v productId=%v currence=%s amount=%v deviceInfo=%s transId=%s",
		reqUid,
		productId,
		currence,
		amount,
		deviceInfo,
		transId)
	// Step1: 首先查询商品的详情
	chargeInof, err := walletApi.GetChargingInfoById(reqUid, productId)
	if err != nil {
		infoLog.Printf("HandleWeChatPay walletApi.GetChargingInfoById reqUid=%v productId=%v currence=%s amount=%v transId=%s failed err=%s",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			err)
		return nil, err
	}

	if currence == RMB_CURRENCY {
		// 如果是支付币种是人民币，不用汇率转换
		rmbAmount = amount / 10 // 这里的10是 amount/1000*100的结果， 微信是以分为单位
	} else {
		// 如果是支付币种比是人民币，需要转成美元，再转回人民币
		// Step2: 然后查询人民币对美元汇率
		rate, err := walletApi.GetExchangeRate(reqUid, RMB_CURRENCY)
		if err != nil {
			infoLog.Printf("HandleWeChatPay walletApi.GetExchangeRate reqUid=%v productId=%v currence=%s amount=%v transId=%s failed err=%s",
				reqUid,
				productId,
				currence,
				amount,
				transId,
				err)
			return nil, err
		}
		// Step3: 换算之后实际扣除的人民币金额
		rmbAmount = uint64(float64(chargeInof.GetPayCurrency().GetCorrespondingDollar()) / float64(rate) * 1000)
		infoLog.Printf("HandleWeChatPay after exchange rate reqUid=%v productId=%v currence=%s amount=%v transId=%s rmbAmpunt=%v GetCorrespondingDollar=%v rate=%v",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			rmbAmount,
			chargeInof.GetPayCurrency().GetCorrespondingDollar(),
			rate)
	}
	// Step4: 写购买记录
	err = dbUtil.InsertWeChatPayPurchaseOrder(transId,
		productId,
		reqUid,
		chargeInof.GetOpUid(),
		RMB_CURRENCY,
		rmbAmount*10, // 写入数据库要在『分』的基础上*10
		chargeInof.GetPayCurrency().GetCurrencyType(),
		chargeInof.GetPayCurrency().GetAmount(),
		chargeInof.GetPayCurrency().GetCorrespondingDollar())
	if err != nil {
		infoLog.Printf("HandleWeChatPay dbUtil.InsertPurchaseOrder failed reqUid=%v productId=%v currence=%s amount=%v transId=%s rmbAmpunt=%v err=%s",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			rmbAmount,
			err)
		return nil, err
	}
	// Step5: 获取响应参数
	prepayId, _, noncestr, sign, timestamp, err := PrepareWeChatPayParams(reqUid,
		transId,
		productId,
		rmbAmount,
		clientIp,
		deviceInfo)
	if err != nil {
		infoLog.Printf("HandleWeChatPay PrepareAliPayParams failed reqUid=%v productId=%v currence=%s amount=%v transId=%s rmbAmpunt=%v err=%s",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			rmbAmount,
			err)
		return nil, err
	}
	// Step6: 返回序列化之后的响应
	rspBody := &ht_pay_center.GetWeChatPayTransParamsRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_SUCCESS)),
			Reason: []byte("success"),
		},
		AppId:      proto.String(WECHAT_PAY_APPID),
		PartnerId:  proto.String(WECHAT_PAY_COMMERCIAL_TENANT),
		PrepayId:   proto.String(prepayId),
		Package:    proto.String(WECHAT_PACKAGE),
		NonceStr:   proto.String(noncestr),
		TimeStamp:  proto.String(timestamp),
		OutTradeNo: proto.String(fmt.Sprintf("%v", transId)),
		Sign:       proto.String(sign),
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("HandleWeChatPay message proto.Marshal failed reqUid=%v productId=%v currence=%s amount=%v transId=%s rmbAmpunt=%v err=%s",
			reqUid,
			productId,
			currence,
			amount,
			transId,
			rmbAmount,
			err)
		return nil, err
	}
	return rspPayLoad, nil
}

func GenerateWeChatPayRetCode(ret uint32, errMsg []byte) (rspPayLoad []byte, err error) {
	rspBody := &ht_pay_center.GetWeChatPayTransParamsRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: errMsg,
		},
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("GenerateWeChatPayRetCode message proto.Marshal failed err=%s", err)
	}
	return rspPayLoad, err
}

func HandleWeChatPayResult(reqPayLoad []byte, clientIp string) (rspPayLoad []byte, err error) {
	if len(reqPayLoad) == 0 {
		infoLog.Printf("HandleWeChatPayResult reqPayLoad empty")
		err = ErrInputParam
		return rspPayLoad, err
	}
	reqBody := new(ht_pay_center.CommitWeChatPayResultReqBody)
	err = proto.Unmarshal(reqPayLoad, reqBody)
	if err != nil {
		infoLog.Printf("HandleWeChatPayResult proto Unmarshal failed err=%s", err)
		return rspPayLoad, err
	}

	productId := reqBody.GetProductId()
	reqUid := reqBody.GetReqUid()
	payRet := reqBody.GetPayResult()
	userName := reqBody.GetUserName()
	headUrl := reqBody.GetHeadUrl()
	national := reqBody.GetNational()
	transId := reqBody.GetOutTradeNo()

	infoLog.Printf("HandleWeChatPayResult reqUid=%v productId=%v payRet=%s userName=%v headUrl=%s transId=%s",
		reqUid,
		productId,
		payRet,
		userName,
		headUrl,
		transId)

	// Step0: 检查微信支付返回代码是否支付成功
	res, err := simplejson.NewJson([]byte(payRet))
	if err != nil {
		infoLog.Printf("HandleWeChatPayResult json decode fail: %s \n err:%s", payRet, err)
		return rspPayLoad, err
	}
	infoLog.Printf("HandleWeChatPayResult json decode success: %+v", res)

	resultStatus, err := res.Get("errCode").String()
	if err != nil {
		infoLog.Printf("HandleWeChatPayResult parse errCode fail: %+v", err)
		return nil, err
	}
	// 只有0才是支付调用成功的
	if resultStatus != "0" {
		if val, ok := wechatpayStatCodeMap[resultStatus]; ok {
			ret := uint32(val)
			rspPayLoad, err = GenerateWeChatPayResultRetCode(ret, []byte("Pay fail"))
			return rspPayLoad, nil
		} else {
			infoLog.Printf("HandleWeChatPayResult unknown errCode: %+v", resultStatus)
			return GenerateWeChatPayResultRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_NOT_CHANGE), []byte("Unknown errCode"))
		}
	}

	// 去微信支付服务器查询
	orderQueryReq := &pay.OrderQueryRequest{
		OutTradeNo: transId,
	}
	orderQueryRsp, err := pay.OrderQuery2(weChatClient, orderQueryReq)
	if err != nil {
		infoLog.Printf("HandleWeChatPayResult OrderQuery2 fail outTradeNo:%s err:%+v", transId, err)
		return GenerateWeChatPayResultRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL), []byte("Query from wechat server fail"))
	}
	infoLog.Printf("HandleWeChatPayResult OrderQuery2 success: %+v", orderQueryRsp)
	if orderQueryRsp.TradeState != "SUCCESS" {
		infoLog.Printf("HandleWeChatPayResult OrderQuery2 TradeState not SUCCESS")
		return GenerateWeChatPayResultRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL), []byte("Pay fail"))
	}

	// Step1: 首先查询商品的详情
	chargeInfo, err := walletApi.GetChargingInfoById(reqUid, productId)
	if err != nil {
		infoLog.Printf("HandleWeChatPayResult walletApi.GetChargingInfoById reqUid=%v productId=%v transId=%s failed err=%s",
			reqUid,
			productId,
			transId,
			err)
		return nil, err
	}
	infoLog.Printf("HandleWeChatPayResult chargeInfo doller=%v", chargeInfo.GetPayCurrency().GetCorrespondingDollar())
	// 检查用户是否已经支付过
	alreadyPurchase := false
	for _, v := range chargeInfo.GetPayUserList() {
		if v.GetUid() == reqUid {
			alreadyPurchase = true
			break
		}
	}

	if alreadyPurchase {
		infoLog.Printf("HandleWeChatPayResult reqUid=%v productId=%v payEntrance=%v already purchase", reqUid, productId, "WeChatPay")
	} else {
		// 如果支付成功了则发起用户已经付费成功
		code, err := walletApi.SuccessPurchaseLesson(
			reqUid,
			userName,
			headUrl,
			national,
			uint64(time.Now().Unix()),
			productId,
			uint32(ht_wallet.AccountType_OTHER_ACCOUNT),
			fmt.Sprintf("%v", transId),
			"WeChatPay")
		if err != nil || code != 0 {
			infoLog.Printf("HandleWeChatPayResult walletApi.SuccessPurchaseLesson reqUid=%v userName=%s transId=%v productId=%v err=%s",
				reqUid,
				userName,
				transId,
				productId,
				err)
			attr := "gopaycenter/success_pur_lesson_failed"
			libcomm.AttrAdd(attr, 1)
			return nil, err
		}
	}
	ret := uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_SUCCESS)
	rspPayLoad, err = GenerateWeChatPayResultRetCode(ret, []byte("success"))
	return rspPayLoad, err
}

func HandleWeChatPayNotify(notifyParam map[string]string) (result string, err error) {
	// 校验签名
	notifySign := notifyParam["sign"]
	delete(notifyParam, "sign")
	sign := core.Sign(notifyParam, WECHAT_PAY_SIGN_KEY, nil)
	// infoLog.Printf("HandleWeChatPayNotify sign compare:\n notifySign:\n%s\n sign \n%s", notifySign, sign)
	if sign != notifySign {
		infoLog.Printf("HandleWeChatPayNotify sign not match:\n notifySign:\n%s\n sign \n%s", notifySign, sign)
		return "FAIL", nil
	}

	// 判断状态
	if notifyParam["result_code"] != "SUCCESS" || notifyParam["return_code"] != "SUCCESS" {
		infoLog.Printf("HandleWeChatPayNotify return_code is Not SUCCESS:%s msg:%s", notifyParam["return_code"], notifyParam["return_msg"])
		return "FAIL", nil
	}

	// 查询数据库记录
	outTradeNo := notifyParam["out_trade_no"]
	transId, err := strconv.ParseUint(outTradeNo, 10, 64)
	if err != nil {
		infoLog.Printf("HandleWeChatPayNotify ParseInt outTradeNo fail: outTradeNo=%s, err=%s", outTradeNo, err)
		return "FAIL", nil
	}
	record, err := dbUtil.GetWeChatPayOrderByTransId(transId)
	if err != nil {
		infoLog.Printf("HandleWeChatPayNotify GetWeChatPayOrderByTransId fail: outTradeNo=%s, err=%s", outTradeNo, err)
		return "FAIL", nil
	}
	infoLog.Printf("HandleWeChatPayNotify GetWeChatPayOrderByTransId: %+v", record)

	// 获取用户信息
	userInfo, err := getUserInfo(record.PayId)
	if err != nil {
		infoLog.Printf("HandleWeChatPayNotify getUserInfo fail: userid=%s  err=%+v", record.PayId, err)
		return "FAIL", nil
	}
	// infoLog.Printf("HandleWeChatPayNotify getUserInfo success:%+v", userInfo)

	// 首先查询商品的详情
	chargeInfo, err := walletApi.GetChargingInfoById(record.PayId, record.ChargingId)
	if err != nil {
		infoLog.Printf("HandleWeChatPayNotify walletApi.GetChargingInfoById reqUid=%v productId=%v failed err=%s",
			record.PayId,
			record.ChargingId,
			err)
		return "FAIL", err
	}
	// 检查用户是否已经支付过，支付过则直接返回成功
	for _, v := range chargeInfo.GetPayUserList() {
		if v.GetUid() == record.PayId {
			infoLog.Printf("HandleWeChatPayNotify reqUid=%v productId=%v payEntrance=%v already purchase", record.PayId, record.ChargingId, "Aipay")
			return "SUCCESS", nil
		}
	}

	// 如果支付成功了则发起用户已经付费成功
	code, err := walletApi.SuccessPurchaseLesson(
		record.PayId,
		fmt.Sprintf("%v", userInfo.Get("NK").MustString()), // userName
		fmt.Sprintf("%v", userInfo.Get("HU").MustString()), // 头像
		fmt.Sprintf("%v", userInfo.Get("NN").MustString()), // 国籍
		uint64(time.Now().Unix()),
		record.ChargingId,
		uint32(ht_wallet.AccountType_OTHER_ACCOUNT),
		outTradeNo,
		"Alipay")
	if err != nil || code != 0 {
		infoLog.Printf("HandleWeChatPayNotify walletApi.SuccessPurchaseLesson reqUid=%v userName=%s transId=%v productId=%v err=%s",
			record.PayId,
			fmt.Sprintf("%v", userInfo.Get("NK").MustString()),
			outTradeNo,
			record.ChargingId,
			err)
		attr := "gopaycenter/success_pur_lesson_failed"
		libcomm.AttrAdd(attr, 1)
		return "FAIL", err
	}

	infoLog.Printf("HandleWeChatPayNotify PurchaseLesson success reqUid=%v userName=%s transId=%v productId=%v", record.PayId, fmt.Sprintf("%v", userInfo.Get("NK").MustString()), outTradeNo, record.ChargingId)
	return "SUCCESS", nil
}

// 向微信支付服务器查询订单详情
func QueryWeChatOrder(outTradeNo string) (resp *pay.OrderQueryResponse, err error) {
	queryReq := &pay.OrderQueryRequest{
		OutTradeNo: outTradeNo,
	}

	resp, err = pay.OrderQuery2(weChatClient, queryReq)
	return resp, err
}

func GenerateWeChatPayResultRetCode(ret uint32, msg []byte) (rspPayLoad []byte, err error) {
	rspBody := &ht_pay_center.CommitWeChatPayResultRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: msg,
		},
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("CommitWeChatPayResultRspBody message proto.Marshal failed err=%s", err)
	}
	return rspPayLoad, err
}

func GenerateBrainTreeTokenRetCode(ret uint32, errMsg []byte) (rspPayLoad []byte, err error) {
	rspBody := &ht_pay_center.GetBrainTreeTokenRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: errMsg,
		},
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("GenerateBrainTreeTokenRetCode message proto.Marshal failed err=%s", err)
	}
	return rspPayLoad, err
}

func HandleBrainTreeGetToken(reqPayLoad []byte) (rspPayLoad []byte, err error) {
	if len(reqPayLoad) == 0 {
		infoLog.Printf("HandleBrainTreeGetToken reqPayLoad empty")
		err = ErrInputParam
		return rspPayLoad, err
	}
	reqBody := new(ht_pay_center.GetBrainTreeTokenReqBody)
	err = proto.Unmarshal(reqPayLoad, reqBody)
	if err != nil {
		infoLog.Printf("HandleBrainTreeGetToken proto Unmarshal failed err=%s", err)
		return rspPayLoad, err
	}
	teminalType := reqBody.GetClientInfo().GetTermianlType()
	version := reqBody.GetClientInfo().GetVersion()
	reqUid := reqBody.GetReqUid()
	productIdStr := reqBody.GetProductId()
	clientTs := reqBody.GetTs()
	transId := libgenmid.GenMid()
	payEntrance := reqBody.GetPayEntrance()
	purchaseType := reqBody.GetPurchaseType()
	currency := reqBody.GetCurrency()
	amount := reqBody.GetAmount()
	// strTransId := fmt.Sprintf("%s", transId)

	var (
		productId           uint64
		correspondingDollar uint64
		opUid               uint32
	)

	infoLog.Printf("HandleBrainTreeGetToken reqUid=%v productIdStr=%v terminalType=%v version=%v ts=%v transId=%v payEntrance=%v amount=%v currency=%v", reqUid, productIdStr, teminalType, version, clientTs, transId, payEntrance, amount, currency)
	// Step1: 首先查询商品的详情
	if purchaseType == ht_pay_center.PURCHASE_TYPE_PURCHASE_PRODUCT {
		// 如果是购买HT商品，需要先到数据库查产品信息
		return rspPayLoad, nil

	} else {
		// 如果是群课程收款
		productId, err = strconv.ParseUint(productIdStr, 10, 64)
		if err != nil {
			infoLog.Printf("HandleBrainTreeGetToken ParseUint productIdStr fail, productIdStr:%s, err:%+v", productIdStr, err)
			return rspPayLoad, nil
		}

		chargeInfo, err := walletApi.GetChargingInfoById(reqUid, productId)
		if err != nil {
			infoLog.Printf("HandleBrainTreeGetToken walletApi.GetChargingInfoById reqUid=%v productId=%v terminalType=%v version=%v ts=%s failed err=%s",
				reqUid,
				productId,
				teminalType,
				version,
				clientTs,
				err)
			return nil, err
		}
		infoLog.Printf("HandleBrainTreeGetToken chargeInfo doller=%v", chargeInfo.GetPayCurrency().GetCorrespondingDollar())
		// 检查用户是否已经支付过
		alreadyPurchase := false
		for _, v := range chargeInfo.GetPayUserList() {
			if v.GetUid() == reqUid {
				alreadyPurchase = true
				break
			}
		}
		if alreadyPurchase {
			infoLog.Printf("HandleBrainTreeGetToken reqUid=%v productId=%v terminalType=%v version=%v ts=%v transId=%v payEntrance=%v already purchase",
				reqUid,
				productId,
				teminalType,
				version,
				clientTs,
				transId,
				payEntrance)
			err = ErrAlreadyPurchase
			return nil, err
		}

		// 下面会用到
		correspondingDollar = chargeInfo.GetPayCurrency().GetCorrespondingDollar()
		opUid = chargeInfo.GetOpUid()
	}
	infoLog.Printf("HandleBrainTreeGetToken chargeInfo amount=%v currency=%v correspondingDollar=%v opUid=%v already purchase", amount, currency, correspondingDollar, opUid)

	// Step2: 群求brainTree 的token
	getTokenReqBody := &GetBrainTreeTokenReq{
		UserId:    reqUid,
		ProductId: fmt.Sprintf("%v", productId),
		Ts:        uint64(time.Now().UnixNano() / 1000000),
		// Amount:    float64(amount),
		Amount: float64(amount) / 1000,
		// Amount:       float64(100),
		Currency:     currency,
		TerminalType: teminalType,
		Version:      version,
		Env:          brainTreeEnv,
	}

	getTokenSlic, err := json.Marshal(getTokenReqBody)
	if err != nil {
		infoLog.Printf("HandleBrainTreeGetToken json.Marshal reqUid=%v productId=%v terminalType=%v version=%v ts=%s failed err=%s",
			reqUid,
			productId,
			teminalType,
			version,
			clientTs,
			err)
		return nil, err
	}
	infoLog.Printf("HandleBrainTreeGetToken brainTreeTokenUrl=%s postBody=%s", brainTreeTokenUrl, getTokenSlic)
	result, err := common.HttpPost(brainTreeTokenUrl, getTokenSlic, HTTP_TIME_OUT)
	if err != nil {
		infoLog.Printf("HandleBrainTreeGetToken common.HttpPost reqUid=%v productId=%v terminalType=%v version=%v ts=%s result=%s failed err=%s",
			reqUid,
			productId,
			teminalType,
			version,
			clientTs,
			result,
			err)
		// 使用备机
		result, err = common.HttpPost(brainTreeBackupTokenUrl, getTokenSlic, HTTP_TIME_OUT)
		if err != nil {
			infoLog.Printf("HandleBrainTreeGetToken common.HttpPost use backup reqUid=%v productId=%v terminalType=%v version=%v ts=%s result=%s failed err=%s",
				reqUid,
				productId,
				teminalType,
				version,
				clientTs,
				result,
				err)
			return nil, err
		}
	}
	infoLog.Printf("HandleBrainTreeGetToken ioutil.ReadAll reqUid=%v productId=%v terminalType=%v version=%v ts=%s result=%s",
		reqUid,
		productId,
		teminalType,
		version,
		clientTs,
		result)
	postRspBody := GetBrainTreeTokenRsp{}
	err = json.Unmarshal([]byte(result), &postRspBody)
	if err != nil {
		infoLog.Printf("HandleBrainTreeGetToken json.Unmarshal reqUid=%v productId=%v terminalType=%v version=%v ts=%s result=%s failed err=%s",
			reqUid,
			productId,
			teminalType,
			version,
			clientTs,
			result,
			err)
		return nil, err
	}
	if postRspBody.Status != 0 {
		infoLog.Printf("HandleBrainTreeGetToken common.HttpPost return staus=%v message=%s", postRspBody.Status, postRspBody.Message)
		err = util.ErrInternalErr
		return nil, err
	}
	// Step3: 写购买记录
	err = dbUtil.InsertBrainTreePurchaseOrder(transId,
		productId,
		reqUid,
		opUid,
		currency,
		amount,
		correspondingDollar,
		postRspBody.Token,
		uint32(payEntrance))
	if err != nil {
		infoLog.Printf("HandleBrainTreeGetToken dbUtil.InsertBrainTreePurchaseOrder failed reqUid=%v productId=%v currence=%s amount=%v transId=%s payEntrance=%v err=%s",
			reqUid,
			productId,
			currency,
			amount,
			transId,
			payEntrance,
			err)
		return nil, err
	}

	// Step4: 返回序列化之后的响应
	ret := uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_SUCCESS)
	rspBody := &ht_pay_center.GetBrainTreeTokenRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: []byte("success"),
		},
		TransId:   proto.Uint64(transId),
		Token:     proto.String(postRspBody.Token),
		Env:       proto.String(postRspBody.Env),
		Currency:  proto.String(currency),
		Amount:    proto.Uint64(amount),
		PayMethod: postRspBody.PayMethod,
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("HandleBrainTreeGetToken message proto.Marshal failed reqUid=%v productId=%v terminalType=%v version=%v ts=%s failed err=%s",
			reqUid,
			productId,
			teminalType,
			version,
			clientTs,
			err)
		return nil, err
	}
	return rspPayLoad, nil
}

func GetPayMethodByInt(uintPayMethod ht_pay_center.PAY_CENTER_BRAINTREE_PAY_METHOD) (strPayMethod string) {
	if uintPayMethod == ht_pay_center.PAY_CENTER_BRAINTREE_PAY_METHOD_PAYPAL {
		strPayMethod = "paypal"
	} else if uintPayMethod == ht_pay_center.PAY_CENTER_BRAINTREE_PAY_METHOD_APPLEPAY {
		strPayMethod = "applepay"
	} else if uintPayMethod == ht_pay_center.PAY_CENTER_BRAINTREE_PAY_METHOD_CREDIT_CARD {
		strPayMethod = "credit_card"
	} else if uintPayMethod == ht_pay_center.PAY_CENTER_BRAINTREE_PAY_METHOD_GOOGLEPAY {
		strPayMethod = "googlepay"
	} else {
		strPayMethod = "unknow"
	}
	return strPayMethod
}

func HandleBrainTreePay(reqPayLoad []byte) (rspPayLoad []byte, err error) {
	if len(reqPayLoad) == 0 {
		infoLog.Printf("HandleBrainTreePay reqPayLoad empty")
		err = ErrInputParam
		return rspPayLoad, err
	}
	// Step1: unmarshal req body
	reqBody := new(ht_pay_center.BrainTreePayReqBody)
	err = proto.Unmarshal(reqPayLoad, reqBody)
	if err != nil {
		infoLog.Printf("HandleBrainTreePay proto Unmarshal failed err=%s", err)
		return rspPayLoad, err
	}

	teminalType := reqBody.GetClientInfo().GetTermianlType()
	version := reqBody.GetClientInfo().GetVersion()
	reqUid := reqBody.GetReqUid()
	userName := reqBody.GetUserName()
	headUrl := reqBody.GetHeadUrl()
	national := reqBody.GetNational()
	token := reqBody.GetToken()
	nonce := reqBody.GetNonce()
	transId := reqBody.GetTransId()
	productId := reqBody.GetProductId()
	typeLabel := reqBody.GetSkdInfo().GetTypeLabel()
	ts := reqBody.GetTs()
	currency := reqBody.GetCurrency()
	amount := reqBody.GetAmount()
	payMethod := GetPayMethodByInt(reqBody.GetPayMethod())
	infoLog.Printf("HandleBrainTreePay terminalType=%v version=%v reqUid=%v userName=%s headUrl=%s national=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v currency=%s amount=%v paymethod=%s",
		teminalType,
		version,
		reqUid,
		userName,
		headUrl,
		national,
		token,
		nonce,
		transId,
		productId,
		typeLabel,
		ts,
		currency,
		amount,
		payMethod)

	// Step2: 查询tranid的详情
	// transRecord, err := dbUtil.GetBrainTreeRecordByTransId(transId)
	// if err != nil {
	// 	infoLog.Printf("HandleBrainTreePay dbUtil.GetBrainTreeRecordByTransId terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v failed err=%s",
	// 		teminalType,
	// 		version,
	// 		reqUid,
	// 		userName,
	// 		token,
	// 		nonce,
	// 		transId,
	// 		productId,
	// 		typeLabel,
	// 		ts,
	// 		err)
	// 	return nil, err
	// }

	// Step3: 群求brainTree 的token
	payReqBody := &BrainTreePayReq{
		UserId:    reqUid,
		Token:     token,
		Nonce:     nonce,
		Amount:    float64(amount) / float64(1000),
		Currency:  currency,
		Env:       brainTreeEnv,
		PayMethod: payMethod,
		ProductId: fmt.Sprintf("%v", productId),
		UserName:  userName,
		Ts:        uint64(time.Now().UnixNano() / 1000000),
		SdkInfo: SdkInfoStru{
			TypeLebel: typeLabel,
		},
	}

	paySlic, err := json.Marshal(payReqBody)
	if err != nil {
		infoLog.Printf("HandleBrainTreePay json.Marshal terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v failed err=%s",
			teminalType,
			version,
			reqUid,
			userName,
			token,
			nonce,
			transId,
			productId,
			typeLabel,
			ts,
			err)
		return nil, err
	}
	infoLog.Printf("HandleBrainTreePay brainTreePayUrl=%s postBody=%s", brainTreePayUrl, paySlic)
	result, err := common.HttpPost(brainTreePayUrl, paySlic, HTTP_TIME_OUT)
	if err != nil {
		infoLog.Printf("HandleBrainTreePay common.HttpPost terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v failed err=%s",
			teminalType,
			version,
			reqUid,
			userName,
			token,
			nonce,
			transId,
			productId,
			typeLabel,
			ts,
			err)
		// 使用备机
		result, err = common.HttpPost(brainTreeBackupPayUrl, paySlic, HTTP_TIME_OUT)
		if err != nil {
			infoLog.Printf("HandleBrainTreePay common.HttpPost terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v user back url failed err=%s",
				teminalType,
				version,
				reqUid,
				userName,
				token,
				nonce,
				transId,
				productId,
				typeLabel,
				ts,
				err)
			return nil, err
		}
	}
	infoLog.Printf("HandleBrainTreePay terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v result=%s",
		teminalType,
		version,
		reqUid,
		userName,
		token,
		nonce,
		transId,
		productId,
		typeLabel,
		ts,
		result)
	postRspBody := BrainTreePayRsp{Status: 1} // 给一个默认不是0的值，如果返回没有status=0,说明有错误
	err = json.Unmarshal([]byte(result), &postRspBody)
	if err != nil {
		infoLog.Printf("HandleBrainTreePay json.Unmarshal terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v err=%s",
			teminalType,
			version,
			reqUid,
			userName,
			token,
			nonce,
			transId,
			productId,
			typeLabel,
			ts,
			err)
		return nil, err
	}
	var stat uint32
	if postRspBody.Status == 0 {
		stat = util.BRAIN_TREE_PAY_SUCC
		// 如果支付成功了则发起用户已经付费成功
		code, err := walletApi.SuccessPurchaseLesson(
			reqUid,
			userName,
			headUrl,
			national,
			uint64(time.Now().Unix()),
			productId,
			uint32(ht_wallet.AccountType_OTHER_ACCOUNT),
			fmt.Sprintf("%v", transId),
			"braintree pay")
		if err != nil || code != 0 {
			infoLog.Printf("HandleBrainTreePay walletApi.SuccessPurchaseLesson terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v err=%s",
				teminalType,
				version,
				reqUid,
				userName,
				token,
				nonce,
				transId,
				productId,
				typeLabel,
				ts,
				err)
			attr := "gopaycenter/success_pur_lesson_failed"
			libcomm.AttrAdd(attr, 1)
		}
	} else {
		stat = util.BRAIN_TREE_PAY_FAILED
	}
	// Step3: 更新购买记录的状态
	err = dbUtil.UpdateBrainTreeStat(transId, stat)
	if err != nil {
		infoLog.Printf("HandleBrainTreePay terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v update brain tree stat failed err=%s",
			teminalType,
			version,
			reqUid,
			userName,
			token,
			nonce,
			transId,
			productId,
			typeLabel,
			ts,
			err)
	}

	if postRspBody.Status != 0 {
		infoLog.Printf("HandleBrainTreePay common.HttpPost return staus=%v message=%s", postRspBody.Status, postRspBody.Message)
		err = util.ErrInternalErr
		return GenerateBrainTreePayRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL), []byte(postRspBody.Message))
	}

	// Step4: 返回序列化之后的响应
	ret := uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_SUCCESS)
	rspBody := &ht_pay_center.BrainTreePayRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: []byte("success"),
		},
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("HandleBrainTreePay proto.Marshal terminalType=%v version=%v reqUid=%v userName=%s token=%s nonce=%s transId=%v productId=%v typeLabel=%s ts=%v failed err=%s",
			teminalType,
			version,
			reqUid,
			userName,
			token,
			nonce,
			transId,
			productId,
			typeLabel,
			ts,
			err)
		return nil, err
	}
	return rspPayLoad, nil
}

func GenerateBrainTreePayRetCode(ret uint32, errMsg []byte) (rspPayLoad []byte, err error) {
	rspBody := &ht_pay_center.BrainTreePayRspBody{
		Status: &ht_pay_center.PayCenterHeader{
			Code:   proto.Uint32(ret),
			Reason: errMsg,
		},
	}
	rspPayLoad, err = proto.Marshal(rspBody)
	if err != nil {
		infoLog.Printf("GenerateBrainTreePayRetCode message proto.Marshal failed err=%s", err)
	}
	return rspPayLoad, err
}

func main() {
	// 加载配置文件
	cfg, err := common.InitLogAndOption(&options, &infoLog)
	common.CheckError(err)

	router := gin.Default()

	LOCAL_SERVER := cfg.Section("LOCAL_SERVER")
	serviceAccessPrefix := LOCAL_SERVER.Key("service_access_prefix").MustString("")
	aliPayNotifyUrl = cfg.Section("NOTIFY_URL").Key("alipay").MustString("")
	wechatPayNotifyUrl = cfg.Section("NOTIFY_URL").Key("wechatpay").MustString("")
	infoLog.Printf("aliPayNotifyUrl=%v  wechatPayNotifyUrl=%v", aliPayNotifyUrl, wechatPayNotifyUrl)
	brainTreeTokenUrl = cfg.Section("BRAINTREE").Key("token_url").MustString("")
	brainTreeBackupTokenUrl = cfg.Section("BRAINTREE").Key("backup_token_url").MustString("")
	brainTreePayUrl = cfg.Section("BRAINTREE").Key("pay_url").MustString("")
	brainTreeBackupPayUrl = cfg.Section("BRAINTREE").Key("bakcup_pay_url").MustString("")
	brainTreeEnv = cfg.Section("BRAINTREE").Key("env").MustString("sandbox")

	// 读取walletApi 配置
	walletIp := cfg.Section("WALLET").Key("ip").MustString("127.0.0.1")
	walletPort := cfg.Section("WALLET").Key("port").MustString("0")
	walletConnLimit := cfg.Section("WALLET").Key("pool_limit").MustInt(1000)
	infoLog.Printf("wallet server ip=%v port=%v connLimit=%v", walletIp, walletPort, walletConnLimit)
	walletApi = tcpcommon.NewWalletApi(walletIp, walletPort, 3*time.Second, 3*time.Second, &tcpcommon.HeadV3Protocol{}, walletConnLimit)
	// 初始化已经支付过的用户map
	brainTreePayUser = make(map[uint32]bool)
	// weChat Client
	weChatTimeOut := cfg.Section("WECHAT").Key("time_out").MustInt(2)
	httpClient := &http.Client{
		Timeout: time.Second * time.Duration(weChatTimeOut),
	}
	weChatClient = core.NewClient(WECHAT_PAY_APPID, WECHAT_PAY_COMMERCIAL_TENANT, WECHAT_PAY_SIGN_KEY, httpClient)
	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")
	// 查询用户信息的redis
	redisHost := cfg.Section("REDIS").Key("userinfo_redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("userinfo_redis_port").MustString("6379")
	userinfoRedis = tcpcommon.NewRedisApi(redisHost + ":" + redisPort)

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort)

	db, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s&interpolateParams=true")
	if err != nil {
		infoLog.Printf("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	dbUtil = util.NewDbUtil(db, infoLog)

	// 前端配置信息，包括接口等
	apiList := map[string]string{
		"alipay":           serviceAccessPrefix + "/alipay/lesson",
		"alipay_result":    serviceAccessPrefix + "/alipay/lesson_result",
		"alipay_notify":    serviceAccessPrefix + "/alipay/notify",
		"wechatpay":        serviceAccessPrefix + "/wechatpay/lesson",
		"wechatpay_result": serviceAccessPrefix + "/wechatpay/lesson_result",
		"wechatpay_notify": serviceAccessPrefix + "/wechatpay/notify",
		"braintree_token":  serviceAccessPrefix + "/braintree/get_token",
		"braintree_pay":    serviceAccessPrefix + "/braintree/pay",
	}
	// 自身服务的配置
	srvIP := LOCAL_SERVER.Key("bind_ip").MustString("0.0.0.0")
	srvPort := LOCAL_SERVER.Key("bind_port").MustString("9888")

	alipayStatCodeMap = map[string]int32{
		"8000": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAYING),
		"4000": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL),
		"5000": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_REPEAT_ADD),
		"6001": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_CANCEL),
		"6002": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL),
		"6004": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAYING),
	}

	wechatpayStatCodeMap = map[string]int32{
		"-1": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL),
		"-2": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_CANCEL),
		"-3": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL),
		"-4": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL),
		"-5": int32(ht_pay_center.PAY_CENTER_RET_CODE_RET_PAY_FAIL),
	}

	// 请求alipay支付的订单详情
	router.POST(apiList["alipay"], func(c *gin.Context) {
		// 处理message
		// 统计总量
		infoLog.Printf("alipay recv req")
		attr := "gopaycenter/alipay_recv_req_count"
		libcomm.AttrAdd(attr, 1)
		message, err := c.GetRawData()
		if err != nil {
			infoLog.Printf("alipay c.GetRawData failed err=%s", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if len(message) == 0 {
			infoLog.Printf("alipay message empty")
			attr := "gopaycenter/alipay_empty_msg_count"
			libcomm.AttrAdd(attr, 1)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		// 根据头部的content-type 进行解密
		var decryText string
		var cryKey string
		contentType := c.GetHeader("content-type")
		if contentType == KCC2018 {
			cryKey = string(message[0:KCC2018KeyLength])
			cryText := string(message[KCC2018KeyLength:])
			infoLog.Printf("alipay cryKeyLen=%v cryText=%v", len(cryKey), len(cryText))
			decryText = libcrypto.TEADecrypt(cryText, cryKey)
		}
		// infoLog.Printf("alipay message=%s key=%x decryText=%v", message, cryKey, len(decryText))
		remoteIp, err := GetClientIp(c)
		infoLog.Printf("alipay remoteIp=%s err=%s", remoteIp, err)
		rspPayLoad, err := HandleAliPay([]byte(decryText))
		if err != nil {
			infoLog.Printf("alipay HandleAliPay return err=%s", err)
			rspPayLoad, err = GenerateAliPayRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_INTERNAL_ERR), []byte("internal error"))
			if err != nil {
				infoLog.Printf("alipay GenerateAliPayRetCode return err=%s", err)
			}
		}
		encryText := libcrypto.TEAEncrypt(string(rspPayLoad), cryKey)
		payLoad := cryKey + encryText
		// infoLog.Printf("message resp payLoad=%x payLoadLen=%v", payLoad, len(payLoad))
		c.String(http.StatusOK, payLoad)
	})

	// 请求alipay支付完成后的同步通知
	router.POST(apiList["alipay_result"], func(c *gin.Context) {
		// 处理message
		// 统计总量
		infoLog.Printf("alipay_result recv req")
		message, err := c.GetRawData()
		if err != nil {
			infoLog.Printf("alipay_result c.GetRawData failed err=%s", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if len(message) == 0 {
			infoLog.Printf("alipay_result message empty")
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		// 根据头部的content-type 进行解密
		var decryText string
		var cryKey string
		contentType := c.GetHeader("content-type")
		if contentType == KCC2018 {
			cryKey = string(message[0:KCC2018KeyLength])
			cryText := string(message[KCC2018KeyLength:])
			infoLog.Printf("alipay_result cryKeyLen=%v cryText=%v", len(cryKey), len(cryText))
			decryText = libcrypto.TEADecrypt(cryText, cryKey)
		}
		// infoLog.Printf("alipay_result message=%s key=%x decryText=%v", message, cryKey, len(decryText))
		remoteIp, err := GetClientIp(c)
		infoLog.Printf("alipay_result remoteIp=%s", remoteIp)
		rspPayLoad, err := HandleAliPayResult([]byte(decryText))
		if err != nil {
			infoLog.Printf("alipay_result HandleAliPayResult return err=%s", err)
			rspPayLoad, err = GenerateAliPayResultRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_INTERNAL_ERR), []byte("internal error"))
			if err != nil {
				infoLog.Printf("alipay GenerateAliPayResultRetCode return err=%s", err)
			}
			// c.AbortWithStatus(http.StatusBadRequest)
		}
		encryText := libcrypto.TEAEncrypt(string(rspPayLoad), cryKey)
		payLoad := cryKey + encryText
		// infoLog.Printf("message resp payLoad=%x payLoadLen=%v", payLoad, len(payLoad))
		c.String(http.StatusOK, payLoad)
	})

	// alipay异步通知接口
	router.POST(apiList["alipay_notify"], func(c *gin.Context) {
		infoLog.Printf("alipay_notify receive")
		req := c.Request
		req.ParseForm()
		infoLog.Printf("alipay_notify PostForm:%+v", req.PostForm)

		result, err := HandleAliPayNotify(req.PostForm)
		if err != nil {
			infoLog.Printf("HandleAliPayNotify return err:%+v", err)
		}

		c.String(http.StatusOK, result)
	})

	// 请求wechatpay支付的订单详情
	router.POST(apiList["wechatpay"], func(c *gin.Context) {
		// 处理message
		// 统计总量
		infoLog.Printf("wechatpay recv req")
		attr := "gopaycenter/recv_req_count"
		libcomm.AttrAdd(attr, 1)
		message, err := c.GetRawData()
		if err != nil {
			infoLog.Printf("wechatpay c.GetRawData failed err=%s", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if len(message) == 0 {
			infoLog.Printf("wechatpay message empty")
			attr := "gopaycenter/empty_msg_count"
			libcomm.AttrAdd(attr, 1)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		// 根据头部的content-type 进行解密
		var decryText string
		var cryKey string
		contentType := c.GetHeader("content-type")
		if contentType == KCC2018 {
			cryKey = string(message[0:KCC2018KeyLength])
			cryText := string(message[KCC2018KeyLength:])
			infoLog.Printf("wechatpay cryKeyLen=%v cryText=%v", len(cryKey), len(cryText))
			decryText = libcrypto.TEADecrypt(cryText, cryKey)
		}
		// infoLog.Printf("wechatpay message=%s key=%x decryText=%v", message, cryKey, len(decryText))
		remoteIp, err := GetClientIp(c)
		infoLog.Printf("wechatpay remoteIp=%s err=%s", remoteIp, err)
		rspPayLoad, err := HandleWeChatPay([]byte(decryText), remoteIp)
		if err != nil {
			infoLog.Printf("HandleWeChatPay return err=%s", err)
			rspPayLoad, err = GenerateWeChatPayRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_INTERNAL_ERR), []byte("internal error"))
			if err != nil {
				infoLog.Printf("WeChat Pay GenerateWeChatPayRetCode return err=%s", err)
			}
		}

		encryText := libcrypto.TEAEncrypt(string(rspPayLoad), cryKey)
		payLoad := cryKey + encryText
		// infoLog.Printf("wechatpay resp payLoad=%x payLoadLen=%v", payLoad, len(payLoad))
		c.String(http.StatusOK, payLoad)
	})

	// 请求wechatpay支付完成后的同步通知
	router.POST(apiList["wechatpay_result"], func(c *gin.Context) {
		// 处理message
		// 统计总量
		infoLog.Printf("wechatpay_result recv req")
		attr := "gopaycenter/recv_req_count"
		libcomm.AttrAdd(attr, 1)
		message, err := c.GetRawData()
		if err != nil {
			infoLog.Printf("wechatpay_result c.GetRawData failed err=%s", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if len(message) == 0 {
			infoLog.Printf("wechatpay_result message empty")
			attr := "gopaycenter/empty_msg_count"
			libcomm.AttrAdd(attr, 1)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		// 根据头部的content-type 进行解密
		var decryText string
		var cryKey string
		contentType := c.GetHeader("content-type")
		if contentType == KCC2018 {
			cryKey = string(message[0:KCC2018KeyLength])
			cryText := string(message[KCC2018KeyLength:])
			infoLog.Printf("wechatpay_result cryKeyLen=%v cryText=%v", len(cryKey), len(cryText))
			decryText = libcrypto.TEADecrypt(cryText, cryKey)
		}
		// infoLog.Printf("wechatpay_result message=%s key=%x decryText=%v", message, cryKey, len(decryText))
		remoteIp, err := GetClientIp(c)
		infoLog.Printf("wechatpay_result remoteIp=%s", remoteIp)
		rspPayLoad, err := HandleWeChatPayResult([]byte(decryText), remoteIp)
		if err != nil {
			infoLog.Printf("HandleWeChatPayResult return err=%s", err)
			rspPayLoad, err = GenerateWeChatPayRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_INTERNAL_ERR), []byte("internal error"))
			if err != nil {
				infoLog.Printf("WeChat Pay GenerateWeChatPayRetCode return err=%s", err)
			}
		}

		encryText := libcrypto.TEAEncrypt(string(rspPayLoad), cryKey)
		payLoad := cryKey + encryText
		// infoLog.Printf("wechatpay_result resp payLoad=%x payLoadLen=%v", payLoad, len(payLoad))
		c.String(http.StatusOK, payLoad)
	})

	// wechat异步通知接口
	router.POST(apiList["wechatpay_notify"], func(c *gin.Context) {
		infoLog.Printf("wechatpay_notify receive")

		respMap, err := wechatutil.DecodeXMLToMap(c.Request.Body)
		if err != nil {
			infoLog.Printf("wechatpay_notify DecodeXMLToMap fail err:%+v", err)
			c.String(http.StatusOK, "FAIL")
		}
		infoLog.Printf("wechatpay_notify DecodeXMLHttpResponse:%+v", respMap)

		returnMsg := ""
		result, err := HandleWeChatPayNotify(respMap)
		if err != nil {
			infoLog.Printf("HandleWeChatPayNotify return err:%+v", err)
			returnMsg = fmt.Sprintf("%s", err)
		}
		// 组装返回XML
		returnMap := map[string]string{
			"return_code": result,
			"return_msg":  returnMsg,
		}
		var buf bytes.Buffer
		if err = wechatutil.EncodeXMLFromMap(&buf, returnMap, "xml"); err != nil {
			infoLog.Printf("HandleWeChatPayNotify wechatutil.EncodeXMLFromMap err:%+v", err)
			c.String(http.StatusOK, result)
		}

		xmlStr := buf.String()
		infoLog.Printf("HandleWeChatPayNotify buf xml %s", xmlStr)
		c.String(http.StatusOK, xmlStr)
	})

	// 请求brainTree的token
	router.POST(apiList["braintree_token"], func(c *gin.Context) {
		// 处理message
		// 统计总量
		infoLog.Printf("braintree_token recv req")
		attr := "gopaycenter/recv_req_count"
		libcomm.AttrAdd(attr, 1)
		message, err := c.GetRawData()
		if err != nil {
			infoLog.Printf("braintree_token c.GetRawData failed err=%s", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if len(message) == 0 {
			infoLog.Printf("braintree_token message empty")
			attr := "gopaycenter/empty_msg_count"
			libcomm.AttrAdd(attr, 1)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		// 根据头部的content-type 进行解密
		var decryText string
		var cryKey string
		contentType := c.GetHeader("content-type")
		if contentType == KCC2018 {
			cryKey = string(message[0:KCC2018KeyLength])
			cryText := string(message[KCC2018KeyLength:])
			infoLog.Printf("braintree_token cryKeyLen=%v cryText=%v", len(cryKey), len(cryText))
			decryText = libcrypto.TEADecrypt(cryText, cryKey)
		}
		// infoLog.Printf("braintree_token message=%s key=%x decryText=%v", message, cryKey, len(decryText))
		remoteIp, err := GetClientIp(c)
		infoLog.Printf("braintree_token remoteIp=%s err=%s", remoteIp, err)
		// reqBody := &ht_pay_center.GetBrainTreeTokenReqBody{
		// 	ClientInfo: &ht_pay_center.ClientInfo{
		// 		TermianlType: proto.Uint32(1),
		// 		Version:      proto.String("2.5.2"),
		// 	},
		// 	ReqUid:    proto.Uint32(2325982),
		// 	ProductId: proto.Uint64(10),
		// 	Ts:        proto.Uint64(uint64(time.Now().UnixNano() / 1000000)),
		// }
		// reqSlic, err := proto.Marshal(reqBody)
		// if err != nil {
		// 	infoLog.Printf("braintree_token message proto.Marshal failed err=%s", err)
		// }
		// rspPayLoad, err := HandleBrainTreeGetToken(reqSlic)
		rspPayLoad, err := HandleBrainTreeGetToken([]byte(decryText))
		// infoLog.Printf("braintree_token rspPayLoad=%s err=%s", rspPayLoad, err)
		if err != nil {
			infoLog.Printf("braintree_token HandleBrainTreeGetToken return err=%s", err)
			if err == ErrAlreadyPurchase {
				rspPayLoad, err = GenerateBrainTreeTokenRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_ALREADY_PURCHASE), []byte("already pruchase"))
				if err != nil {
					infoLog.Printf("braintree_token Pay GenerateBrainTreeTokenRetCode return err=%s", err)
				}
			} else {
				rspPayLoad, err = GenerateBrainTreeTokenRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_INTERNAL_ERR), []byte("internal error"))
				if err != nil {
					infoLog.Printf("braintree_token Pay GenerateBrainTreeTokenRetCode return err=%s", err)
				}
			}
		}
		encryText := libcrypto.TEAEncrypt(string(rspPayLoad), cryKey)
		payLoad := cryKey + encryText
		// infoLog.Printf("braintree_token resp payLoad=%x payLoadLen=%v", payLoad, len(payLoad))
		c.String(http.StatusOK, payLoad)
	})
	// 请求brainTree的支付
	router.POST(apiList["braintree_pay"], func(c *gin.Context) {
		// 处理message
		// 统计总量
		infoLog.Printf("braintree_pay recv req")
		attr := "gopaycenter/bp_recv_req_count"
		libcomm.AttrAdd(attr, 1)
		message, err := c.GetRawData()
		if err != nil {
			infoLog.Printf("braintree_pay c.GetRawData failed err=%s", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if len(message) == 0 {
			infoLog.Printf("braintree_pay message empty")
			attr := "gopaycenter/bp_empty_msg_count"
			libcomm.AttrAdd(attr, 1)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		// 根据头部的content-type 进行解密
		var decryText string
		var cryKey string
		contentType := c.GetHeader("content-type")
		if contentType == KCC2018 {
			cryKey = string(message[0:KCC2018KeyLength])
			cryText := string(message[KCC2018KeyLength:])
			infoLog.Printf("braintree_pay cryKeyLen=%v cryText=%v", len(cryKey), len(cryText))
			decryText = libcrypto.TEADecrypt(cryText, cryKey)
		}
		// infoLog.Printf("braintree_pay message=%s key=%x decryText=%v", message, cryKey, len(decryText))
		remoteIp, err := GetClientIp(c)
		infoLog.Printf("braintree_pay remoteIp=%s err=%s", remoteIp, err)

		rspPayLoad, err := HandleBrainTreePay([]byte(decryText))
		infoLog.Printf("braintree_pay rspPayLoad=%s err=%s", rspPayLoad, err)
		if err != nil {
			infoLog.Printf("braintree_pay HandleBrainTreePay return err=%s", err)
			rspPayLoad, err = GenerateBrainTreePayRetCode(uint32(ht_pay_center.PAY_CENTER_RET_CODE_RET_INTERNAL_ERR), []byte("internal error"))
			if err != nil {
				infoLog.Printf("braintree_pay Pay GenerateBrainTreePayRetCode return err=%s", err)
			}
		}
		encryText := libcrypto.TEAEncrypt(string(rspPayLoad), cryKey)
		payLoad := cryKey + encryText
		// infoLog.Printf("braintree_pay resp payLoad=%x payLoadLen=%v", payLoad, len(payLoad))
		c.String(http.StatusOK, payLoad)
	})

	router.Run(srvIP + ":" + srvPort)
}
