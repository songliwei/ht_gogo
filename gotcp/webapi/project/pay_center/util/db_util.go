// Copyright 2016 songliwei
//
// HelloTalk.inc

package util

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

// Error type
var (
	ErrNilDbObject      = errors.New("not set  object current is nil")
	ErrDbParam          = errors.New("err param error")
	ErrBalanceNotEnough = errors.New("balance is not enough")
	ErrInternalErr      = errors.New("internal error")
)

const (
	BRAIN_TREE_GET_TOKEN  = 0
	BRAIN_TREE_PAY_SUCC   = 1
	BRAIN_TREE_PAY_FAILED = 2
)

type DbUtil struct {
	db      *sql.DB
	infoLog *log.Logger
}

func NewDbUtil(mysqlDb *sql.DB, logger *log.Logger) *DbUtil {
	return &DbUtil{
		db:      mysqlDb,
		infoLog: logger,
	}
}

type BrainTreeRecord struct {
	TransId         uint64
	ChargingId      uint64
	PayUid          uint32
	CollectUid      uint32
	CollectCurrency string
	CollectAmount   uint64
	DollerAmount    uint64
	UpdateTimeStamp uint64
	Stat            uint32
}

type ItemProductRecord struct {
	Id        uint64
	ProductId string
	// Name      string
	Currency string
	Amount   uint64
}

type AliPayTransRecord struct {
	TransId    uint64
	ChargingId uint64
	PayId      uint32
	// CollectId uint32
	// PayCurrency string
	// PayAmount uint64
	// CollectCurrency string
	// CollectAmount uint64
}

type WeChatPayTransRecord struct {
	TransId    uint64
	ChargingId uint64
	PayId      uint32
}

// 1、插入购买记录
func (this *DbUtil) InsertAliPayPurchaseOrder(transId uint64,
	chargingId uint64,
	payUid uint32,
	collectUid uint32,
	payCurrency string,
	payAmount uint64,
	collectCurrency string,
	collectAmount uint64,
	dollerAmount uint64) (err error) {
	if this.db == nil ||
		transId == 0 ||
		chargingId == 0 ||
		payUid == 0 ||
		collectUid == 0 ||
		payCurrency == "" ||
		payAmount == 0 ||
		collectCurrency == "" ||
		collectAmount == 0 ||
		dollerAmount == 0 {
		this.infoLog.Printf("InsertAliPayPurchaseOrder transId=%v chargingId=%v payUid=%v collectUid=%v payCurrency=%s payAmount=%v collectCurrency=%s collectAmount=%v dollerAmount=%v param error",
			transId,
			chargingId,
			payUid,
			collectUid,
			payCurrency,
			payAmount,
			collectCurrency,
			collectAmount,
			dollerAmount)
		err = ErrDbParam
		return err
	}
	// Step1: 插入用户收款记录到数据库中
	_, err = this.db.Exec("insert into HT_ALIPAY_TRANSACTION_ORDER (trans_id, charging_id, pay_uid, collect_uid, pay_currency, pay_amount, collect_currency, collect_amount, doller_amount, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP()) ;",
		fmt.Sprintf("%d", transId),
		chargingId,
		payUid,
		collectUid,
		payCurrency,
		payAmount,
		collectCurrency,
		collectAmount,
		dollerAmount) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("InsertAliPayPurchaseOrder insert HT_ALIPAY_TRANSACTION_ORDER transId=%v chargingId=%v payUid=%v collectUid=%v payCurrency=%s payAmount=%v collectCurrency=%s collectAmount=%v dollerAmount=%v failed err=%s",
			transId,
			chargingId,
			payUid,
			collectUid,
			payCurrency,
			payAmount,
			collectCurrency,
			collectAmount,
			dollerAmount,
			err)
		return err
	}
	return nil
}

func (this *DbUtil) InsertWeChatPayPurchaseOrder(transId uint64,
	chargingId uint64,
	payUid uint32,
	collectUid uint32,
	payCurrency string,
	payAmount uint64,
	collectCurrency string,
	collectAmount uint64,
	dollerAmount uint64) (err error) {
	if this.db == nil ||
		transId == 0 ||
		chargingId == 0 ||
		payUid == 0 ||
		collectUid == 0 ||
		payCurrency == "" ||
		payAmount == 0 ||
		collectCurrency == "" ||
		collectAmount == 0 ||
		dollerAmount == 0 {
		this.infoLog.Printf("InsertWeChatPayPurchaseOrder transId=%v chargingId=%v payUid=%v collectUid=%v payCurrency=%s payAmount=%v collectCurrency=%s collectAmount=%v dollerAmount=%v param error",
			transId,
			chargingId,
			payUid,
			collectUid,
			payCurrency,
			payAmount,
			collectCurrency,
			collectAmount,
			dollerAmount)
		err = ErrDbParam
		return err
	}
	// Step1: 插入用户收款记录到数据库中
	_, err = this.db.Exec("insert into HT_WECHATPAY_TRANSACTION_ORDER (trans_id, charging_id, pay_uid, collect_uid, pay_currency, pay_amount, collect_currency, collect_amount, doller_amount, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, UTC_TIMESTAMP()) ;",
		fmt.Sprintf("%d", transId),
		chargingId,
		payUid,
		collectUid,
		payCurrency,
		payAmount,
		collectCurrency,
		collectAmount,
		dollerAmount) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("InsertWeChatPayPurchaseOrder insert HT_WECHATPAY_TRANSACTION_ORDER transId=%v chargingId=%v payUid=%v collectUid=%v payCurrency=%s payAmount=%v collectCurrency=%s collectAmount=%v dollerAmount=%v failed err=%s",
			transId,
			chargingId,
			payUid,
			collectUid,
			payCurrency,
			payAmount,
			collectCurrency,
			collectAmount,
			dollerAmount,
			err)
		return err
	}
	return nil
}

// 3、插入BrainTree 购买记录
func (this *DbUtil) InsertBrainTreePurchaseOrder(transId uint64,
	chargingId uint64,
	payUid uint32,
	collectUid uint32,
	collectCurrency string,
	collectAmount uint64,
	dollerAmount uint64,
	token string,
	paymentFrom uint32) (err error) {
	if this.db == nil ||
		transId == 0 ||
		chargingId == 0 ||
		payUid == 0 ||
		collectUid == 0 ||
		collectCurrency == "" ||
		collectAmount == 0 ||
		dollerAmount == 0 {
		this.infoLog.Printf("InsertBrainTreePurchaseOrder transId=%v chargingId=%v payUid=%v collectUid=%v collectCurrency=%s collectAmount=%v dollerAmount=%v token=%s paymentFrom=%v param error",
			transId,
			chargingId,
			payUid,
			collectUid,
			collectCurrency,
			collectAmount,
			dollerAmount,
			token,
			paymentFrom)
		err = ErrDbParam
		return err
	}
	// Step1: 插入用户收款记录到数据库中
	_, err = this.db.Exec("insert into HT_BRAINTREE_TRANSACTION_ORDER (trans_id, charging_id, pay_uid, collect_uid, collect_currency, collect_amount, doller_amount, stat, token, entrance_id, update_time) VALUES (?, ?, ?, ?, ?, ?, ?, 0, ?, ?, UTC_TIMESTAMP()) ;",
		fmt.Sprintf("%d", transId),
		fmt.Sprintf("%d", chargingId),
		payUid,
		collectUid,
		collectCurrency,
		collectAmount,
		dollerAmount,
		token,
		paymentFrom) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("InsertBrainTreePurchaseOrder insert HT_ALIPAY_TRANSACTION_ORDER transId=%v chargingId=%v payUid=%v collectUid=%v collectCurrency=%s collectAmount=%v dollerAmount=%v token=%s paymentFrom=%v failed err=%s",
			transId,
			chargingId,
			payUid,
			collectUid,
			collectCurrency,
			collectAmount,
			dollerAmount,
			token,
			paymentFrom,
			err)
		return err
	}
	return nil
}

// 4、更新BrainTree 记录的状态
func (this *DbUtil) UpdateBrainTreeStat(transId uint64, stat uint32) (err error) {
	if this.db == nil ||
		transId == 0 ||
		stat == 0 {
		this.infoLog.Printf("UpdateBrainTreeStat transId=%v stat=%v param error",
			transId,
			stat)
		err = ErrDbParam
		return err
	}
	// Step1: 插入用户收款记录到数据库中
	_, err = this.db.Exec("UPDATE HT_BRAINTREE_TRANSACTION_ORDER SET stat=?, update_time = UTC_TIMESTAMP() WHERE trans_id=?;",
		stat,
		fmt.Sprintf("%d", transId)) // 当前状态为正常账号
	if err != nil {
		this.infoLog.Printf("UpdateBrainTreeStat UPDATE HT_ALIPAY_TRANSACTION_ORDER transId=%v stat=%v failed err=%s",
			transId,
			stat,
			err)
		return err
	}
	return nil
}

// 5、通过transId 查询BrainTree订单状态
func (this *DbUtil) GetBrainTreeRecordByTransId(transId uint64) (brainTreeRecord *BrainTreeRecord, err error) {
	if this.db == nil || transId == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetBrainTreeRecordByTransId transId=%v param error", transId)
		return nil, err
	}

	var storeCharginId, storePayUid, storeCollectUid, storeAmount, storeDollerAmount, storeStat, storeUpdateTime sql.NullInt64
	var storeCurrency sql.NullString
	err = this.db.QueryRow("SELECT charging_id, pay_uid, collect_uid, collect_currency, collect_amount, doller_amount, stat, UNIX_TIMESTAMP(update_time) FROM HT_BRAINTREE_TRANSACTION_ORDER WHERE trans_id=? ;", fmt.Sprintf("%v", transId)).Scan(&storeCharginId,
		&storePayUid,
		&storeCollectUid,
		&storeCurrency,
		&storeAmount,
		&storeDollerAmount,
		&storeStat,
		&storeUpdateTime)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetBrainTreeRecordByTransId not found transId=%v in HT_BRAINTREE_TRANSACTION_ORDER err=%s", transId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetBrainTreeRecordByTransId exec failed HT_BRAINTREE_TRANSACTION_ORDER transId=%v err=%s", transId, err)
		return nil, err
	default:
	}

	brainTreeRecord = &BrainTreeRecord{
		TransId: transId,
	}
	if storeCharginId.Valid {
		brainTreeRecord.ChargingId = uint64(storeCharginId.Int64)
	}
	if storePayUid.Valid {
		brainTreeRecord.PayUid = uint32(storePayUid.Int64)
	}
	if storeCollectUid.Valid {
		brainTreeRecord.CollectUid = uint32(storeCollectUid.Int64)
	}
	if storeAmount.Valid {
		brainTreeRecord.CollectAmount = uint64(storeAmount.Int64)
	}
	if storeDollerAmount.Valid {
		brainTreeRecord.DollerAmount = uint64(storeDollerAmount.Int64)
	}
	if storeStat.Valid {
		brainTreeRecord.Stat = uint32(storeStat.Int64)
	}
	if storeUpdateTime.Valid {
		brainTreeRecord.UpdateTimeStamp = uint64(storeUpdateTime.Int64)
	}
	if storeCurrency.Valid {
		brainTreeRecord.CollectCurrency = storeCurrency.String
	}

	return brainTreeRecord, nil
}

// 通过 HT_ITEM_PRODUCT 的 product_id 字段查询数据库记录
func (this *DbUtil) GetItemProductByProductId(productId string) (itemProductRecord *ItemProductRecord, err error) {
	if this.db == nil || len(productId) == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetItemProductByProductId productId=%v param error", productId)
		return nil, err
	}

	var (
		rowId    sql.NullInt64
		amount   sql.NullFloat64
		currency sql.NullString
	)
	err = this.db.QueryRow("SELECT id, currency, amount FROM HT_ITEM_PRODUCT WHERE product_id =?", productId).Scan(&rowId,
		&currency,
		&amount)
	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetItemProductByProductId not found productId=%v in HT_ITEM_PRODUCT err=%s", productId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetItemProductByProductId exec failed HT_ITEM_PRODUCT productId=%v err=%s", productId, err)
		return nil, err
	default:
	}

	itemProductRecord = &ItemProductRecord{
		ProductId: productId,
	}
	if rowId.Valid {
		itemProductRecord.Id = uint64(rowId.Int64)
	}
	if currency.Valid {
		itemProductRecord.Currency = currency.String
	}
	if amount.Valid {
		itemProductRecord.Amount = uint64(amount.Float64 * 100)
	}
	return itemProductRecord, nil
}

// 通过 HT_ALIPAY_TRANSACTION_ORDER 的 trans_id 字段查询数据库记录
func (this *DbUtil) GetAliPayOrderByTransId(transId uint64) (alipayRecord *AliPayTransRecord, err error) {
	if this.db == nil || transId == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetAliPayOrderByTransId transId=%v param error", transId)
		return nil, err
	}

	var (
		storedChargingId sql.NullInt64
		storedPayUid     sql.NullInt64
	)
	err = this.db.QueryRow("SELECT charging_id, pay_uid FROM HT_ALIPAY_TRANSACTION_ORDER WHERE trans_id = ?", fmt.Sprintf("%v", transId)).Scan(&storedChargingId,
		&storedPayUid)

	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetAliPayOrderByTransId not found transId=%v in HT_ALIPAY_TRANSACTION_ORDER err=%s", transId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetAliPayOrderByTransId exec failed HT_ALIPAY_TRANSACTION_ORDER transId=%v err=%s", transId, err)
		return nil, err
	default:
	}

	alipayRecord = &AliPayTransRecord{
		TransId: transId,
	}
	if storedPayUid.Valid {
		alipayRecord.PayId = uint32(storedPayUid.Int64)
	}
	if storedChargingId.Valid {
		alipayRecord.ChargingId = uint64(storedChargingId.Int64)
	}
	return alipayRecord, nil
}

// 通过 HT_WECHATPAY_TRANSACTION_ORDER 的 trans_id 字段查询数据库记录
func (this *DbUtil) GetWeChatPayOrderByTransId(transId uint64) (alipayRecord *WeChatPayTransRecord, err error) {
	if this.db == nil || transId == 0 {
		err = ErrDbParam
		this.infoLog.Printf("GetWeChatPayOrderByTransId transId=%v param error", transId)
		return nil, err
	}

	var (
		storedChargingId sql.NullInt64
		storedPayUid     sql.NullInt64
	)
	err = this.db.QueryRow("SELECT charging_id, pay_uid FROM HT_WECHATPAY_TRANSACTION_ORDER WHERE trans_id = ?", fmt.Sprintf("%v", transId)).Scan(&storedChargingId,
		&storedPayUid)

	switch {
	case err == sql.ErrNoRows:
		this.infoLog.Printf("GetWeChatPayOrderByTransId not found transId=%v in HT_WECHATPAY_TRANSACTION_ORDER err=%s", transId, err)
		return nil, err
	case err != nil:
		this.infoLog.Printf("GetWeChatPayOrderByTransId exec failed HT_WECHATPAY_TRANSACTION_ORDER transId=%v err=%s", transId, err)
		return nil, err
	default:
	}

	alipayRecord = &WeChatPayTransRecord{
		TransId: transId,
	}
	if storedPayUid.Valid {
		alipayRecord.PayId = uint32(storedPayUid.Int64)
	}
	if storedChargingId.Valid {
		alipayRecord.ChargingId = uint64(storedChargingId.Int64)
	}
	return alipayRecord, nil
}
