package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"math"
	"os"
)

var GROUP []string = []string{
	"1355973870047.jpg",
	"504cd0f8d3094b72f_d95d7.jpg",
	"7183520b6384e4bd8_6cf61.jpg",
	"741eeb929bc6cb774_ff76c.jpg",
	"ace20839b5c4bf030_6bfc9.jpg",
	"c2aa08ac6ad0502a6_0c791.jpg",
	"cs_logo.jpg",
	"othumbsize_100_100_77324250_56beef6a1b7d4.jpg",
	"othumbsize_100_100_85421438_56e29496e2807.jpg",
	"thumbsize_100_100_67757_53eb292a2b3ae.jpg",
}

type circle struct {
	p image.Point
	r int
}

func (c *circle) ColorModel() color.Model {
	return color.AlphaModel
}

func (c *circle) Bounds() image.Rectangle {
	return image.Rect(c.p.X-c.r, c.p.Y-c.r, c.p.X+c.r, c.p.Y+c.r)
}

func (c *circle) At(x, y int) color.Color {
	xx, yy, rr := float64(x-c.p.X)+0.5, float64(y-c.p.Y)+0.5, float64(c.r)
	if xx*xx+yy*yy < rr*rr {
		return color.Alpha{255}
	}
	return color.Alpha{0}
}

func drawImage(dstFilename string, fileArray []string) error {

	file, err := os.Create(dstFilename)
	if err != nil {
		return err
	}
	defer file.Close()

	imageArray := []image.Image{}

	pngAva := image.NewRGBA(image.Rect(0, 0, 366, 366))

	r := 120

	posArray := []int{5, 6, 7, 0, 1, 2, 3, 4}

	drawCount := 0

	for _, v := range fileArray {

		file1, err := os.Open(v)
		if err != nil {
			fmt.Println("drawImage()", err)
			continue
		}
		img, err := jpeg.Decode(file1)
		if err != nil {
			fmt.Println("drawImage()", err)
			continue
		}
		file1.Close()

		// imageArray = append(imageArray, img)

		// draw.Draw(profileImage, profileImage.Bounds(), itemImage, image.Point{0, 0}, draw.Src)
		p := image.Point{
			X: r * math.Cos(math.Pi/4*posArray[drawCount]),
			Y: r * math.Sin(math.Pi/4*posArray[drawCount]),
		}

		draw.DrawMask(pngAva, pngAva.Bounds(), img, image.ZP, &circle{p, r}, image.ZP, draw.Over)
		drawCount++
	}

	draw.Draw(jpg, jpg.Bounds(), img, img.Bounds().Min, draw.Src)

	jpeg.Encode(file, pngAva, nil)

}

func main() {

	err := drawImage("dst.jpg", GROUP)

}
