package org

import (
	"fmt"

	// tcpcommon "github.com/gansidui/gotcp/tcpfw/common"
	// "github.com/disintegration/imaging"
	// "github.com/gansidui/gotcp/webapi/common"
	// _ "github.com/go-sql-driver/mysql"
	"github.com/disintegration/imaging"

	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"
	// "log"
	"math"
	"net/http"
	"os"
)

type drawHeadImageCnf struct {
	PointArray []image.Point //坐标点数组
	Radius     int           //半径
}

var CircalCnfArray []drawHeadImageCnf
var CircalPointMax int = 8

type circle struct {
	p image.Point
	r int
}

func (c *circle) ColorModel() color.Model {
	return color.AlphaModel
}

func (c *circle) Bounds() image.Rectangle {
	return image.Rect(c.p.X-c.r, c.p.Y-c.r, c.p.X+c.r, c.p.Y+c.r)
}

func (c *circle) SetR(x int) {
	c.r = x
}

func (c *circle) GetR() int {
	return c.r
}

func (c *circle) At(x, y int) color.Color {
	xx, yy, rr := float64(x-c.p.X)+0.1, float64(y-c.p.Y)+0.1, float64(c.r)

	if xx*xx+yy*yy < (rr)*(rr) {
		return color.Alpha{255}
	}

	return color.Alpha{0}
}

func DrawGroupAvatar(tofile string, size int, fileArray []string, outputSize int) error {

	file, err := os.Create(tofile)
	if err != nil {
		return err
	}
	defer file.Close()

	pick := int(1)
	if len(fileArray) > 8 {
		pick = 8
	} else {
		pick = len(fileArray)
	}

	width := CircalCnfArray[pick].Radius

	drawCount := int(0)
	p := image.Point{
		X: width / 2,
		Y: width / 2,
	}

	pngAva := image.NewNRGBA(image.Rect(0, 0, size, size))

	dstImage128Array := []image.Image{}

	for k, v := range fileArray {

		// fmt.Println("DrawGroupAvatar() ", drawCount, k, v)

		if drawCount >= CircalPointMax {
			break
		}

		file1, err := os.Open(v)
		if err != nil {
			fmt.Println("DrawGroupAvatar() Open err", err, k)
			continue
		}

		buff := make([]byte, 512) // docs tell that it take only first 512 bytes into consideration
		if _, err = file1.Read(buff); err != nil {
			fmt.Println(err) // do something with that error
			continue
		}
		file1.Seek(0, 0)

		fileType := http.DetectContentType(buff)
		var srcimg image.Image
		if "image/jpeg" == fileType {
			srcimg, err = jpeg.Decode(file1)
			if err != nil {
				fmt.Println("DrawGroupAvatar()  Decode err  ", k, err, v, fileType)
				continue
			}
		} else if "image/png" == fileType {
			srcimg, err = png.Decode(file1)
			if err != nil {
				fmt.Println("DrawGroupAvatar()  Decode eer  ", err, v, fileType)
				continue
			}
		} else {
			fmt.Println("DrawGroupAvatar() skip fileType=", fileType)
			continue
		}

		file1.Close()

		var dstImage128 image.Image

		if srcimg.Bounds().Dx() != width {

			// fmt.Println("DrawGroupAvatar() ", srcimg.Bounds().Dx(), "!=", width)

			dstImage128 = imaging.Resize(srcimg, width, width, imaging.Lanczos)
			dstImage128Array = append(dstImage128Array, dstImage128)

		} else {
			dstImage128Array = append(dstImage128Array, srcimg)
		}

		drawCount++
	}

	pick = 1
	if drawCount > 8 {
		pick = 8
	} else {
		pick = drawCount
	}

	// fmt.Println("DrawGroupAvatar() start pick=", pick, "Radius=", CircalCnfArray[pick].Radius, "r=", r, "len=", len(dstImage128Array))

	for k, v := range dstImage128Array {

		// drawImageToMask(pngAva, &dstImage128Array[k], v, &circle{p, r}, size)
		// drawImageToMask(pngAva, &dstImage128Array[k], v, &circle{p, CircalCnfArray[pick].Radius / 2}, size)
		drawImageToMask(pngAva, &v, CircalCnfArray[pick].PointArray[k], &circle{p, CircalCnfArray[pick].Radius / 2}, size)
	}

	if outputSize != size {
		pngAva = imaging.Resize(pngAva, outputSize, outputSize, imaging.Lanczos)
	}

	return png.Encode(file, pngAva)

}

func ComressLocalImage(tofile string, capacity int) (string, error) {

	file, err := os.Open(tofile)
	if err != nil {
		return "", err
	}

	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return "", err
	}

	afterUrl := tofile + "_min.png"

	newfile, err := os.Create(afterUrl)
	if err != nil {
		return "", err
	}
	defer newfile.Close()

	compressed := Compress(img, NoConversion, 20)

	err = png.Encode(newfile, compressed)

	if err != nil {
		return "", err
	}

	return afterUrl, nil

}

func drawImageToMask(png *image.NRGBA, srcImg *image.Image, pt image.Point, cir *circle, size int) {

	green := color.White
	pr := cir.GetR()
	draw.DrawMask(png, (*srcImg).Bounds().Add(pt), &image.Uniform{green}, image.ZP, cir, image.ZP, draw.Over)

	cir.SetR(pr - size/183)
	draw.DrawMask(png, (*srcImg).Bounds().Add(pt), *srcImg, image.ZP, cir, image.ZP, draw.Over)

}

//转化为弧度(rad)
func rad(d float64) (r float64) {
	r = d * math.Pi / 180.0
	return
}

func buildCircle1Points(size int) ([]image.Point, int) {

	Circal4PointArray := []image.Point{}
	width := size / 2
	x := int(size/2 - width/2)
	y := int(size/2 - width/2)
	Circal4PointArray = append(Circal4PointArray, image.Pt(x, y))

	return Circal4PointArray, int(width)
}

func buildCircle2Points(size int) ([]image.Point, int) {

	Circal4PointArray := []image.Point{}
	gap := size / 25

	width := (size - 3*gap) / 2

	for i := 0; i < 2; i++ {

		x := int(gap + i*(width+gap))
		y := int(size/2 - width/2)
		Circal4PointArray = append(Circal4PointArray, image.Pt(x, y))
	}

	return Circal4PointArray, int(width)
}

func buildCircle3Points(size int) ([]image.Point, int) {

	Circal3PointArray := []image.Point{}
	gap := size / 25

	width := (size - 3*gap) / 2

	x := int(size/2 - width/2)
	y := int(gap)
	Circal3PointArray = append(Circal3PointArray, image.Pt(x, y))

	for i := 1; i < 3; i++ {
		x = int(gap + (i-1)*(width+gap))
		y = int(gap + width + gap)
		Circal3PointArray = append(Circal3PointArray, image.Pt(x, y))
	}

	return Circal3PointArray, int(width)
}

func buildCircle4Points(size int) ([]image.Point, int) {

	Circal4PointArray := []image.Point{}
	gap := size / 25

	width := (size - 4*gap) / 2

	for i := 0; i < 4; i++ {
		row := int(i / 2)
		column := int(i % 2)
		x := int(gap + column*(width+gap*2))
		y := int(gap + row*(width+gap*2))
		Circal4PointArray = append(Circal4PointArray, image.Pt(x, y))
	}

	return Circal4PointArray, int(width)
}

func buildCircle5Points(size int) ([]image.Point, int) {

	gap := size / 25

	width := (size - 4*gap) / 2

	Circal5PointArray := []image.Point{}

	x := int(size/2 - width/2)
	y := int(size/2 - width/2)
	Circal5PointArray = append(Circal5PointArray, image.Pt(x, y))

	for i := 0; i < 4; i++ {
		row := int(i / 2)
		column := int(i % 2)
		x := int(column * (gap*4 + width))
		y := int(row * (gap*4 + width))

		Circal5PointArray = append(Circal5PointArray, image.Pt(x, y))
	}

	return Circal5PointArray, int(width - int(width/36))
}

func buildCircle6Points(size int) ([]image.Point, int) {

	width := size / 3
	gap := width / 3

	Circal6PointArray := []image.Point{}

	for i := 0; i < 6; i++ {
		row := int(i / 3)
		column := int(i % 3)
		x := int(column * (width - gap/5))
		y := gap + row*width
		if column == 1 {
			y = row * (width * 2)
			if i >= 4 {
				y = y - gap + 2
			}
		}

		x = x + int(size/36)
		y = y + int(size/36)

		Circal6PointArray = append(Circal6PointArray, image.Pt(x, y))
	}

	return Circal6PointArray, int(width)
}

func buildCircle7Points(size int) ([]image.Point, int) {

	width := size / 3

	Circal7PointArray := []image.Point{}

	bigR := float64((size - width) / 2)

	smallR := float64(width / 2)

	halfSize := float64(size / 2)

	fmt.Printf("buildCircle7Points() size=%d,width=%v,bigR=%v,smallR=%v,halfSize=%v\n", size, width, bigR, smallR, halfSize)

	fmt.Println("buildCircle7Points() ", len(Circal7PointArray), "!=", 0, int((size-width)/2))

	x2 := int(halfSize - smallR - bigR*math.Cos(rad(360/7)))
	y2 := int(halfSize - smallR - bigR*math.Sin(rad(360/7)))
	Circal7PointArray = append(Circal7PointArray, image.Pt(x2, y2))

	fmt.Println("buildCircle7Points() ", len(Circal7PointArray), "x2=", x2, "y2=", y2, math.Cos(rad(60)))

	x2 = int(halfSize - smallR + bigR*math.Cos(rad(180-360/7*2)))
	y2 = int(halfSize - smallR - bigR*math.Sin(rad(180-360/7*2)))
	Circal7PointArray = append(Circal7PointArray, image.Pt(x2, y2))

	x2 = int(halfSize - smallR + bigR*math.Cos(rad(180-360/7*3)))
	y2 = int(halfSize - smallR - bigR*math.Sin(rad(180-360/7*3)))
	Circal7PointArray = append(Circal7PointArray, image.Pt(x2, y2))

	x2 = int(halfSize - smallR + bigR*math.Cos(rad(360/7*4-180)))
	y2 = int(halfSize - smallR + bigR*math.Sin(rad(360/7*4-180)))
	Circal7PointArray = append(Circal7PointArray, image.Pt(x2, y2))

	x2 = int(halfSize - smallR + bigR*math.Cos(rad(360/7*5-180)))
	y2 = int(halfSize - smallR + bigR*math.Sin(rad(360/7*5-180)))
	Circal7PointArray = append(Circal7PointArray, image.Pt(x2, y2))

	x2 = int(halfSize - smallR - bigR*math.Sin(rad(360/7*6-270)))
	y2 = int(halfSize - smallR + bigR*math.Cos(rad(360/7*6-270)))
	Circal7PointArray = append(Circal7PointArray, image.Pt(x2, y2))

	Circal7PointArray = append(Circal7PointArray, image.Pt(0, int((size-width)/2)))

	return Circal7PointArray, int(width - 1)
}

func buildCircle8Points(size int) ([]image.Point, int) {

	width := size / 3
	gap := width / 3

	Circal8PointArray := []image.Point{}
	for i := 0; i < 8; i++ {
		column := i % 2
		if column == 0 {
			if i == 0 {
				Circal8PointArray = append(Circal8PointArray, image.Pt(gap, gap))
			} else if i == 2 {
				Circal8PointArray = append(Circal8PointArray, image.Pt(size-gap-width, gap))
			} else if i == 4 {
				Circal8PointArray = append(Circal8PointArray, image.Pt(size-gap-width, size-gap-width))
			} else if i == 6 {
				Circal8PointArray = append(Circal8PointArray, image.Pt(gap, size-gap-width))
			}
		} else {

			if i == 1 {
				Circal8PointArray = append(Circal8PointArray, image.Pt(int((size-width)/2), 0))
			} else if i == 3 {

				Circal8PointArray = append(Circal8PointArray, image.Pt(size-width, int((size-width)/2)))
			} else if i == 5 {

				Circal8PointArray = append(Circal8PointArray, image.Pt(int((size-width)/2), size-width))

			} else if i == 7 {

				Circal8PointArray = append(Circal8PointArray, image.Pt(0, int((size-width)/2)))

			}
		}
	}

	return Circal8PointArray, int(width - 1)
}

func init() {

	fmt.Sprintf("init int avatar_builder ")
	CircalCnfArray = make([]drawHeadImageCnf, CircalPointMax+1)

	CircalCnfArray[1].PointArray, CircalCnfArray[1].Radius = buildCircle1Points(360 * 2)
	CircalCnfArray[2].PointArray, CircalCnfArray[2].Radius = buildCircle2Points(360 * 2)
	CircalCnfArray[3].PointArray, CircalCnfArray[3].Radius = buildCircle3Points(360 * 2)
	CircalCnfArray[4].PointArray, CircalCnfArray[4].Radius = buildCircle4Points(360 * 2)
	CircalCnfArray[5].PointArray, CircalCnfArray[5].Radius = buildCircle5Points(360 * 2)
	CircalCnfArray[6].PointArray, CircalCnfArray[6].Radius = buildCircle6Points(360 * 2)
	CircalCnfArray[7].PointArray, CircalCnfArray[7].Radius = buildCircle7Points(360 * 2)
	CircalCnfArray[8].PointArray, CircalCnfArray[8].Radius = buildCircle8Points(360 * 2)

}
