package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/HT_GOGO/gotcp/libcomm"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/HT_GOGO/gotcp/webapi/common/vender"
	"github.com/HT_GOGO/gotcp/webapi/project/group_avatar/org"
	_ "github.com/go-sql-driver/mysql"
	"github.com/nsqio/go-nsq"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"
	// "strconv"
)

const (
	AT_INIT_JOIN_ROOM    = 1 //主动加入群聊
	AT_INVITED_JOIN_ROOM = 2 //被邀请加入群聊
	AT_INIT_QUIT_ROOM    = 3 //主动退出群聊
	AT_KICKED_OUT_ROOM   = 4 //被T出群聊
	AT_CREATE_ROOM       = 5 //create群聊

	DEFAULT_IMAGE = "ga/group_default.png"
)

type GroupActionEvent struct {
	UserId uint32 `json:"userid" binding:"required"` //加入或者退出群的userid

	RoomId uint32 `json:"room_id" binding:"required"` //群ID

	ActionType uint8 `json:"action_type" binding:"required"` //进群的方式 1-4

	HandlerId uint32 `json:"handler_id" binding:"omitempty"` //根据ActionType 决定，这里是谁拉 Userid进群的人的userid，或者谁TUserid出群的人的userid，如果是主动行为，就写0

	TimeStamp uint64 `json:"ts" binding:"omitempty"` //事情发送时间
}

var (
	SlaveDB *sql.DB
	infoLog *log.Logger
	// s3Api                 *vender.S3API
	ossApi                *vender.OSSAPI
	mucApi                *tcpcommon.MucApi
	options               common.Options
	gavaPrefix            string
	tmpFileFolder         string
	headUrlPrefix         string
	workerCount           int
	GlobalRecvMsgPackChan chan *GroupActionEvent
)

func MessageHandle(message *nsq.Message) error {

	attr := "mmt/total_group_move_count"
	libcomm.AttrAdd(attr, 1)

	PostData(message)

	return nil
}

func PostData(message *nsq.Message) {

	info := &GroupActionEvent{}
	err := json.Unmarshal(message.Body, info) // JSON to Struct

	if err != nil {
		infoLog.Printf("PostData err=%s", err.Error())
		return
	}

	//确保同一个帖子再同一个处理队列里面
	//因为有时候做分值计算的时候，要先读取 再写入
	// chanIdx := uint64(info.RoomId) % uint64(queueCount)

	infoLog.Printf("PostData enter RoomId=%d timestamp=%v body=%s ", info.RoomId, message.Timestamp, message.Body)

	GlobalRecvMsgPackChan <- info

}

func GroupAvatarHandler(info *GroupActionEvent, idx int) error {

	infoLog.Printf("GroupAvatarHandler start chanIdx=%d RoomId=%v ", idx, info.RoomId)

	userids, oldHeadUrl, err := GetRoomInfo(info.RoomId)

	if err != nil {
		infoLog.Printf("GroupAvatarHandler GetRoomInfo RoomId=%d err=%s ", info.RoomId, err.Error())
		return err
	}

	if len(userids) == 0 {
		err = fmt.Errorf("GroupAvatarHandler empty userids RoomId=%d", info.RoomId)
		infoLog.Println(err)
		return err
	}

	md5Avator := ""

	if len(userids) > 10 {
		userids = userids[:10]
	}

	infoLog.Println("GroupAvatarHandler() userids=", userids, "RoomId=", info.RoomId)

	//下载前面的人的头像
	localFiles, err := DownloadHeadUrl(userids, tmpFileFolder)
	if err != nil {
		infoLog.Printf("GroupAvatarHandler DownloadHeadUrl RoomId=%d err=%s ", info.RoomId, err.Error())
		return err
	}

	if len(localFiles) == 0 {
		err = fmt.Errorf("GroupAvatarHandler empty local head url RoomId=%d", info.RoomId)
		infoLog.Println(err)
		return err
	}

	md5Avator = common.Md5(strings.Join(localFiles, "|"))

	if oldHeadUrl != "" && strings.Contains(oldHeadUrl, md5Avator) {
		err = fmt.Errorf("room Head no need to change RoomId=%d,%s,%s", info.RoomId, oldHeadUrl, md5Avator)
		infoLog.Println(err)
		return err
	}

	localAvatarName := tmpFileFolder + md5Avator + ".png"
	//绘制头像
	err = org.DrawGroupAvatar(localAvatarName, 360*2, localFiles, 160)
	if err != nil {
		infoLog.Printf("GroupAvatarHandler DrawGroupAvatar RoomId=%d err=%s ", info.RoomId, err.Error())
		return err
	}

	//将群头像上传到服务器
	remoteName, err := PostImageToOSS(localAvatarName, md5Avator, info.RoomId)
	if err != nil {
		infoLog.Printf("GroupAvatarHandler PostImageToOSS RoomId=%d err=%s ", info.RoomId, err.Error())
		return err
	} else {
		infoLog.Printf("Successfully uploaded %d %s to %s\n", info.RoomId, localAvatarName, remoteName)
	}

	//更新群资料
	code, err := mucApi.UpdateRoomAvatar(info.RoomId, 10000, remoteName)

	if err != nil {
		infoLog.Printf("GroupAvatarHandler UpdateRoomAvatar code=%d,RoomId=%d err=%s ", code, info.RoomId, err.Error())
		return err
	} else {
		infoLog.Printf("GroupAvatarHandler UpdateRoomAvatar OK workderId=%d RoomId=%d remoteName=%s", idx, info.RoomId, remoteName)
	}

	return nil
}

func GetRoomInfo(Roomid uint32) ([]uint32, string, error) {

	roomInfo, err := mucApi.GetRoomInfo(Roomid, 10000, 0)

	userids := []uint32{}

	if err != nil {
		return userids, "", err
	}

	members := roomInfo.GetMembers()

	for _, v := range members {
		userids = append(userids, v.GetUid())
	}

	return userids, string(roomInfo.GetRoomAvatar()), nil
}

//download user's headurl and return local file nane array
func DownloadHeadUrl(Userids []uint32, Folder string) ([]string, error) {

	useridStr := common.JoinUint32(Userids, ",")
	sqlStr := fmt.Sprintf("SELECT HEADURL FROM HT_USER_BASE WHERE USERID IN (%s)  ORDER BY FIELD(USERID,%s)", useridStr, useridStr)
	rows, err := SlaveDB.Query(sqlStr)

	localFileArr := []string{}

	if err != nil {
		return localFileArr, err
	}

	HeadUrl := ""
	HeadUrlArray := []string{}
	for rows.Next() {
		if err := rows.Scan(&HeadUrl); err != nil {
			infoLog.Println("DownloadHeadUrl() rows.Scan failed,err=", err)
			continue
		}
		HeadUrlArray = append(HeadUrlArray, HeadUrl)
	}
	rows.Close()

	infoLog.Println("GroupAvatarHandler() HeadUrlArray=", HeadUrlArray)

	for _, v := range HeadUrlArray {

		localName := Folder + strings.Replace(v, "/", "_", -1)

		size, err := common.GetFileSize(localName)

		if size > 0 && err == nil {

			infoLog.Println("DownloadHeadUrl() exist ", localName, v)
			localFileArr = append(localFileArr, localName)

		} else {

			nsize, err := common.DownloadFile(headUrlPrefix+v, localName, 15)

			if err != nil {

				infoLog.Println("DownloadHeadUrl() download fail ", localName, err)

			} else {

				infoLog.Println("DownloadHeadUrl() download ok ", localName, "nsize=", nsize)

				localFileArr = append(localFileArr, localName)
			}

		}

	}

	return localFileArr, nil

}

func PostImageToOSS(localName string, headName string, roomId uint32) (string, error) {

	remoteName := fmt.Sprintf("%s%d_%s.png", gavaPrefix, roomId, headName)

	err := ossApi.Upload(localName, remoteName)
	//err := s3Api.Upload(localName, remoteName, true)

	return remoteName, err

}

//修复老的被删除的群，没有头像的问题，只执行一次
func RepairOldEmpty() {
	allRoomids := []uint32{}
	remoteName := DEFAULT_IMAGE
	for idx, v := range allRoomids {

		//更新群资料
		code, err := mucApi.UpdateRoomAvatar(v, 10000, remoteName)

		if err != nil {
			infoLog.Printf("GroupAvatarHandler UpdateRoomAvatar code=%d,RoomId=%d err=%s ", code, v, err.Error())
			return
		} else {
			infoLog.Printf("GroupAvatarHandler UpdateRoomAvatar OK idx=%d code=%d RoomId=%d remoteName=%s", idx, code, v, remoteName)
		}

		time.Sleep(100 * time.Millisecond)

	}
}
func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := common.InitLogAndOption(&options, &infoLog)

	SlaveDB, err = common.InitMySqlFromSection(cfg, infoLog, "SLAVE_MYSQL", 3, 1)
	common.CheckError(err)
	defer SlaveDB.Close()

	// s3Api, err = vender.NewS3API(cfg, infoLog)
	ossApi, err = vender.NewOSSAPI(cfg, infoLog)
	common.CheckError(err)

	mucIp := cfg.Section("MUC").Key("muc_ip").MustString("127.0.0.1")
	mucPort := cfg.Section("MUC").Key("muc_port").MustString("11750")
	mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	infoLog.Printf("muc server ip=%v port=%v connLimit=%v", mucIp, mucPort, mucConnLimit)
	mucApi = tcpcommon.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &tcpcommon.HeadV3Protocol{}, mucConnLimit)

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	workerCount = cfg.Section("SERVICE_CONFIG").Key("worker_count").MustInt(10)
	tmpFileFolder = cfg.Section("SERVICE_CONFIG").Key("tmp_file_folder").MustString("/tmp/")
	gavaPrefix = cfg.Section("SERVICE_CONFIG").Key("ava_prefix").MustString("/gav/")
	headUrlPrefix = cfg.Section("SERVICE_CONFIG").Key("head_url_prefix").MustString("http://d30cfp13kr8dva.cloudfront.net/")

	infoLog.Println("======== init consumer ================ workerCount=", workerCount)

	url := []string{"/mydata/group_header/30cfe01883f12ccbb_9c85e.jpg",
		"/mydata/group_header/4eac3f4e54c637649_33287.jpg",
		"/mydata/group_header/6d74fedff9b08047d_ac246.jpg",
		// "/mydata/group_header/6d74fedff9b08047d_ac246.jpg",
	}

	//"/mydata/group_header/20170630_d7b231508cf3c9f5f_1dfbc.jpg",
	//"/mydata/group_header/20170112_be2f541af12e15610_b5eae.jpg"
	//绘制头像
	localName := "/var/www/html/dst.png"
	err = org.DrawGroupAvatar(localName, 360*2, url, 240)
	if err != nil {
		infoLog.Printf("GroupAvatarHandler DrawGroupAvatar RoomId=%d err=%s ", 1, err.Error())
		return
	} else {

		newName, err := org.ComressLocalImage(localName, 65)

		infoLog.Println("GroupAvatarHandler ComressLocalImage  ", newName, err)

		// return
	}

	GlobalRecvMsgPackChan = make(chan *GroupActionEvent)

	for i := 0; i < workerCount; i++ {

		go func(idx int) {

			for {
				info := <-GlobalRecvMsgPackChan

				GroupAvatarHandler(info, idx)
			}

		}(i)

	}

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	go RepairOldEmpty()

	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

	// org.DrawGroupAvatar("/var/www/html/dst.png", 360*2, GROUP)
	// drawImage2("/var/www/html/dst2.png", 360*2, GROUP)
	// fmt.Println("drawImage() err=", err)
}
