package main

import (
	"database/sql"
	"encoding/json"
	"github.com/HT_GOGO/gotcp"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/webapi/common"
	//"io/ioutil"
	"net"
	"os/signal"
	//"regexp"
	"fmt"
	"strconv"
	"strings"
	"syscall"
	//"github.com/garyburd/redigo/redis"
	"errors"

	kforg "github.com/gansidui/gotcp/webapi/project/kf_center/org"

	"log"

	_ "github.com/go-sql-driver/mysql"

	"os"
	"runtime"
	"sync"
	"time"
	//reportorg "github.com/gansidui/gotcp/webapi/project/report_handler/org"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"github.com/gansidui/gotcp/webapi/common/model"
)

var (
	Masterdb      *sql.DB
	Mgdb          *sql.DB
	Logdb         *sql.DB
	GormMasterdb  *gorm.DB
	GormMgdb      *gorm.DB
	GormLogdb     *gorm.DB
	SlaveLogdb    *sql.DB
	RulerRedis    *tcpcommon.RedisApi
	UserinfoRedis *tcpcommon.RedisApi
	MgRedis       *tcpcommon.RedisApi

	Chat_api  *tcpcommon.ChatRecordApi
	momentAPI *tcpcommon.MntApi

	// userInfoCacheApi *tcpcommon.UserInfoCacheApi

	userPri *common.UserPrivilege

	infoLog *log.Logger
	options common.Options

	momentReportBanDuration uint32
	momentReportBanCount    uint32

	savedMessageIds       map[uint64]uint8
	savedMessageIdsLocker *sync.Mutex

	extra_users string             //帖子关系户id 10000,100001
	Filter      *common.TextFilter //过滤器
)

const (
	CMD_HANDLER_OLD_REPORT = 1
	CMD_HANDLER_REPORT     = 2

	RT_BLACKPULL    = 0 //拉黑
	RT_REPORTPERSON = 1
	RT_REPORTIMAGE  = 2
	RT_REPORTMOMENT = 3

	//0=敏感词（所有消息url和），1=色情文字（30条内），2=包含联系方式，3=包含uRL（20内），4=包含图片（20内）',
	SENSITIVE_TYPE_ALL     = 0
	SENSITIVE_TYPE_SEXXY   = 1
	SENSITIVE_TYPE_CONTACT = 2
	SENSITIVE_TYPE_URL     = 3
	SENSITIVE_TYPE_IMAGE   = 4

	REPORT_BLACK     = 1 //举报
	REPORT_SENSITICE = 3 //敏感词\
	MOMENT_BLACK     = 2 //信息流举报
)

type IMMsg struct {
	MsgType   string `json:"msg_type" binding:"required"`
	MsgID     string `json:"msg_id" binding:"required"`
	MsgModal  string `json:"msg_model" binding:"omitempty"`
	FromId    uint32 `json:"from_id" binding:"omitempty"`
	ToId      uint32 `json:"to_id" binding:"omitempty"`
	ServerTs  uint64 `json:"server_ts" binding:"omitempty"`
	Sendtime  string `json:"send_time" binding:"omitempty"`
	ProfileTs uint64 `json:"from_profile_ts" binding:"omitempty"`

	Text         kforg.TextMsg         `json:"text" binding:"omitempty"`
	Image        kforg.ImageMsg        `json:"image" binding:"omitempty"`
	Doodle       kforg.ImageMsg        `json:"doodle" binding:"omitempty"`
	Correction   kforg.CorrectionMsg   `json:"correction" binding:"omitempty"`
	Voice        kforg.VoiceMsg        `json:"voice" binding:"omitempty"`
	Video        kforg.VideoMsg        `json:"video" binding:"omitempty"`
	Sticker      kforg.StickerMsg      `json:"new_sticker" binding:"omitempty"`
	Translate    kforg.TranslateMsg    `json:"translate" binding:"omitempty"`
	Introduction kforg.IntroductionMsg `json:"introduction" binding:"omitempty"`
	Location     kforg.LocationMsg     `json:"location" binding:"omitempty"`
	VoiceText    kforg.VoiceTextMsg    `json:"voice_text" binding:"omitempty"`
	Link         kforg.LinkMsg         `json:"link" binding:"omitempty"`
	Card         kforg.LinkMsg         `json:"card" binding:"omitempty"`
}

type ReportMsg struct {
	FromId      uint32
	ToId        uint32
	MsgID       uint64
	SourceType  uint8
	MessageType uint8
	UserMark    uint8
	Message     string
	Sendtime    string
}

type ChatCountAnalysis struct {
	Total uint32
}

func (this *IMMsg) GetReportMsg(check bool) (rptMsg *ReportMsg) { //check 用于检测客户端上报消息 fromid toid 错反

	msgRpt := &ReportMsg{}

	msgRpt.FromId = this.FromId
	msg_id_src := ""
	msgRpt.ToId = this.ToId
	intType, Body, _ := this.GetMessageContentJson()
	if this.MsgID != "" {
		if check { //
			// tmp := strings.Split(this.MsgID, "_")
			tmp := strconv.Itoa(int(msgRpt.ToId))
			// tmp_from_id, _ := strconv.Atoi(tmp[0])
			//if msgRpt.FromId != uint32(tmp_from_id) {
			if strings.Contains(this.MsgID, tmp) {
				msgRpt.FromId = this.ToId
				msgRpt.ToId = this.FromId
			}

		}
		msg_id_src = fmt.Sprintf("%s", this.MsgID)
	} else {
		msg_id_src = fmt.Sprintf("%s_%s", Body, this.Sendtime)
	}

	msgRpt.MsgID = common.Crc64(msg_id_src)

	msgRpt.UserMark = 0

	msgRpt.Message = Body
	msgRpt.MessageType = intType
	msgRpt.SourceType = 1

	if this.ServerTs > 0 {
		msgRpt.Sendtime = common.QtDate(uint32(this.ServerTs / 1000))
	} else {
		msgRpt.Sendtime = this.Sendtime
	}

	return msgRpt
}

func (this *IMMsg) GetMessageContentJson() (uint8, string, error) {

	intType := kforg.GetMessageType(this.MsgType)

	body := ""
	switch intType {
	case kforg.MT_TEXT:
		return intType, (this.Text.Text), nil

	case kforg.MT_IMAGE:
		return intType, (this.Image.Url), nil

	case kforg.MT_DOODLE:
		return intType, (this.Doodle.Url), nil

	case kforg.MT_PROFILE:
		body = getJson(this.Introduction)
	case kforg.MT_STICKERS:
		body = getJson(this.Sticker)
	case kforg.MT_CORRECTION:
		body = getJson(this.Correction)
	case kforg.MT_VOICETEXT:
		body = getJson(this.VoiceText)
	case kforg.MT_LOCATE:
		body = getJson(this.Location)
	case kforg.MT_VOICE:
		body = getJson(this.Voice)
	case kforg.MT_VIDEO:
		body = getJson(this.Video)
	case kforg.MT_TRANSLATE:
		body = getJson(this.Translate)
	case kforg.MT_LINK:
		body = getJson(this.Link)

	case kforg.MT_CARD:
		body = getJson(this.Card)
	default:
		infoLog.Println("GetMessageContentJson() not support json ", this.MsgType)
	}

	//修复客户端提交数据错乱
	if intType == kforg.MT_TRANSLATE && this.Translate.DstText != "" && this.Translate.SrcText == "" {
		body = this.Translate.DstText
		intType = kforg.MT_TRANSLATE
	}

	return intType, body, nil
}

func getJson(v interface{}) string {

	str, _ := json.Marshal(v)

	return string(str)
}

type ReportData struct {
	Confirm       uint32  `json:"report_confirm" binding:"omitempty"`
	Entrance      uint32  `json:"report_entrance" binding:"omitempty"`
	ReportContent string  `json:"report_content" binding:"omitempty"`
	ReportId      uint32  `json:"report_id" binding:"omitempty"` //被举报者id
	ReportMsg     []IMMsg `json:"report_msg" binding:"omitempty"`
	ReportType    uint32  `json:"report_type" binding:"required"`
	Userid        uint32  `json:"user_id" binding:"required"`
	Reason        string  `json:"report_reason" binding:"omitempty"`
}

type SensitiveData struct {
	From       uint32 `json:"from" binding:"omitempty"`
	KeyWords   string `json:"key_words" binding:"omitempty"`
	Msg        string `json:"msg" binding:"omitempty"`
	ReportType uint32 `json:"report_type" binding:"omitempty"`
	To         uint32 `json:"to" binding:"required"`
}

type MqInfoData struct {
	Data interface{} `json:"data" binding:"required"`
	Type string      `json:"type" binding:"required"`
}

type MqInfoStruct struct {
	Cmd      string     `json:"key_words" binding:"omitempty"`
	Info     MqInfoData `json:"info" binding:"omitempty"`
	OsType   uint32     `json:"os_type" binding:"required"`
	ServerTs uint32     `json:"server_ts" binding:"required"`
	Userid   uint32     `json:"user_id" binding:"required"`
}

type Callback struct{}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {

	infoLog.Printf("get message ")
	packet, ok := p.(*tcpcommon.HeadV3Packet)
	if !ok { // 不是HeadV3Packet报文
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	head, err := packet.GetHead()
	if err != nil {
		//SendResp(c, head, uint16(ERR_INVALID_PARAM))
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:[Uid=%v,Seq=%v,Cmd=%v,Len=%v]", head.From, head.Seq, head.Cmd, head.Len)

	//	infoLog.Printf("OnMessage:[%#v] len=%v\n", head, len(packet.GetBody()))
	_, err = packet.CheckPacketValid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	switch int(head.Cmd) {

	case CMD_HANDLER_REPORT:
		go func() {

			//避免数据库里面还没有写入
			time.Sleep(8 * time.Second)

			runRportHandler(packet, head.Cmd)
		}()

	case CMD_HANDLER_OLD_REPORT:
		//插入历史
		runRportHandler(packet, head.Cmd)
	//从数据库更新 不隐藏帖子的用户id
	case common.CMD_UPDATE_IGNORE_USERS:
		err = InitIgnoreUsersFromMgDb(Filter)
		common.SendResponseCodeV3(c, head, err)
		infoLog.Println("monent hide ignore users", extra_users)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =%d", head.Cmd)
	}
	return true
}

func InitIgnoreUsersFromMgDb(regIPFilter *common.TextFilter) (err error) {
	extra_users, err = regIPFilter.LoadConfigString("moment_hide_ignore_users", "customer_config")
	if err != nil {
		infoLog.Println("InitIgnoreUsersFromMgDb()  extra_users err ", err)
	}
	return
}

func runRportHandler(packet *tcpcommon.HeadV3Packet, Cmd uint16) {

	Body := packet.GetBody()

	Info := MqInfoStruct{}

	err := json.Unmarshal(Body, &Info)

	if err != nil {
		infoLog.Println("OnMessage() Unmarshal err=", Body, err, Cmd)

	} else {

		infoLog.Println("OnMessage() Unmarshal body=", string(Body))

		handlerReport(&Info, Cmd)

	}
}

func handlerReport(Info *MqInfoStruct, Cmd uint16) error {

	switch Info.Info.Type {

	case "report_type":

		reportInfo := ReportData{}

		err := common.UnmarshalInterface(Info.Info.Data, &reportInfo)

		if err != nil {
			return err
		}

		// 兼容老的历史数据处理，moment report 不需要导入
		// if Cmd == CMD_HANDLER_OLD_REPORT && reportInfo.ReportType == RT_REPORTMOMENT {
		// return nil
		// }

		err = HandleUserReport(&reportInfo, Info.Userid, Info.ServerTs, Info.OsType)

	case "sensitive_detect":

		sensiInfo := SensitiveData{}

		err := common.UnmarshalInterface(Info.Info.Data, &sensiInfo)

		if err != nil {
			return err
		}

		err = HandleSensitiveMsg(&sensiInfo, Info.Userid, Info.ServerTs)

	default:
		infoLog.Println("PostData() undefined handler ", Info.Info.Type)

	}

	return nil
}

func HandleUserReport(Data *ReportData, Userid uint32, ServerTs, OsType uint32) error {

	now := time.Now()

	tableNames := GetLastNDay(now, "LOG_P2P_MESSAGE_", 5)

	if len(Data.ReportMsg) > 0 {
		upChatTime := Data.ReportMsg[0].Sendtime

		upChatTs, err := common.StrtoTime(upChatTime)

		if err != nil {
			infoLog.Println("HandlerUserReport()  strtotime fail ", Userid, upChatTime)
		}

		// 2017-11-30 23:58:20 之前的在 单独的一个表里面。
		if upChatTs.Unix() < 1512057500 {
			GetMessagFromLogDbToMgDb(SlaveLogdb, "LOG_P2P_MESSAGE", Data.Userid, Data.ReportId, 0, 2500)
			tableNames = []string{}
		} else {

			// 2017-11-30 23:58:20 之前的在 单独的一个表里面。
			if upChatTs.Unix() > 1512057500 && upChatTs.Unix() < 1512143999 {
				GetMessagFromLogDbToMgDb(SlaveLogdb, "LOG_P2P_MESSAGE", Data.Userid, Data.ReportId, 0, 1000)
				tableNames = []string{"LOG_P2P_MESSAGE_20171201", "LOG_P2P_MESSAGE_20171202"}
			} else {
				// 到了 3天以前
				if now.Unix()-upChatTs.Unix() >= (3 * 86400) {
					//取前后两天加上当天的记录
					reportTime := time.Unix(upChatTs.Unix()+2*86400, 0)

					tableNames = GetLastNDay(reportTime, "LOG_P2P_MESSAGE_", 5)

				}

			}

		}

		infoLog.Println("HandlerUserReport() handler tables ", Userid, upChatTime, tableNames)

	}

	if Data.ReportType == RT_REPORTMOMENT {

		handleMomentReport(Data, Userid, ServerTs, OsType)
		addReportBlackUser(Data.ReportId, ServerTs, MOMENT_BLACK) //举报

	} else if Data.ReportType == RT_BLACKPULL { //拉黑

		allCount := uint32(0)

		for _, tableName := range tableNames {

			addCount, _ := GetMessagFromLogDbToMgDb(SlaveLogdb, tableName, Data.Userid, Data.ReportId, 0, 0)

			if allCount > 500 {
				break
			} else {
				allCount = allCount + addCount
			}
		}

		if len(Data.ReportMsg) > 0 { //纠正客户端上报举报信息 fromid toid错反

			saveReportMsg(Data.ReportMsg, 1)
		}

		addReportBlackUser(Data.ReportId, ServerTs, RT_BLACKPULL)
	} else {

		allCount := uint32(0)

		for _, tableName := range tableNames {

			addCount, _ := GetMessagFromLogDbToMgDb(SlaveLogdb, tableName, Data.Userid, Data.ReportId, 0, 0)

			if allCount > 500 {
				break
			} else {
				allCount = allCount + addCount
			}
		}

		if len(Data.ReportMsg) > 0 {

			saveReportMsg(Data.ReportMsg, 1)
		}

		addReportBlackUser(Data.ReportId, ServerTs, REPORT_BLACK) //举报
	}

	return nil
}

func HandleSensitiveMsg(Data *SensitiveData, Userid uint32, ServerTs uint32) error {

	//只有触发了聊天敏感词的才记录消息
	if Data.ReportType == SENSITIVE_TYPE_SEXXY {

		saveSensitiveMsgToUserChat(Data, Userid, ServerTs)

		go func() {

			time.Sleep(10 * time.Second)
			now := time.Now()

			tableNames := GetLastNDay(now, "LOG_P2P_MESSAGE_", 2)

			for _, tableName := range tableNames {
				addCount, _ := GetMessagFromLogDbToMgDb(SlaveLogdb, tableName, Data.From, Data.To, 0, 0)
				if addCount > 50 {
					break
				}
			}

			saveSensitiveMsg(Data, Userid, ServerTs)

			addReportBlackUser(Userid, ServerTs, REPORT_SENSITICE) //敏感词
		}()
	} else {

		saveSensitiveMsg(Data, Userid, ServerTs)

	}

	return nil
}

func GetLastNDay(now time.Time, prefix string, Nday int) []string {

	thists := now.Unix()

	res := []string{}

	for i := 0; i < Nday; i++ {
		tm := time.Unix(int64(thists-int64(i*86400)), 0)
		ServerTime := tm.Format("20060102")
		res = append(res, prefix+ServerTime)
	}

	return res
}

func GetMessagFromLogDbToMgDb(p2pdb *sql.DB, tableName string, fromid, toid uint32, userMark int, limit int) (uint32, error) {

	if limit == 0 {
		limit = 10000
	}
	query := ""

	if tableName == "LOG_P2P_MESSAGE" {
		query = fmt.Sprintf("SELECT FROMID,TOID,PACKET,TYPE,SENDTIME FROM %s where FROMID IN (%d,%d) AND TOID IN (%d,%d) AND MODEL=0 ORDER BY LOGID DESC LIMIT 0,%d",
			tableName, fromid, toid, fromid, toid, limit)

	} else {

		query = fmt.Sprintf("SELECT FROMID,TOID,PACKET,TYPE,SENDTIME FROM %s where FROMID IN (%d,%d) AND TOID IN (%d,%d) AND MODEL=0 LIMIT 0,%d",
			tableName, fromid, toid, fromid, toid, limit)
	}

	rows, err := p2pdb.Query(query)

	if err != nil {
		// infoLog.Println("GetMessagFromLogDbToMgDb() Query failed sql=", query)
		infoLog.Println("GetMessagFromLogDbToMgDb() Query failed kk", err)
		return 0, err
	}

	MsgText := ""
	MsgType := uint8(0)
	Sendtime := ""

	to := uint32(0)
	from := uint32(0)

	msgList := []IMMsg{}

	for rows.Next() {
		if err := rows.Scan(&from, &to, &MsgText, &MsgType, &Sendtime); err != nil {
			infoLog.Println("GetMessagFromLogDbToMgDb() rows.Scan failed ", err)
			continue
		}

		unCompressSlice, err := common.DepressString([]byte(MsgText))

		if err != nil {
			infoLog.Println("GetMessagFromLogDbToMgDb() rows.Scan failed ", err)
			continue
		}

		immsg := IMMsg{}

		err = json.Unmarshal(unCompressSlice, &immsg)

		if err != nil {
			infoLog.Println("GetMessagFromLogDbToMgDb() json decode failed ", err)
			continue
		}

		immsg.FromId = from
		immsg.ToId = to
		immsg.Sendtime = Sendtime
		nowCount := len(msgList)
		if nowCount%5 == 0 {
			infoLog.Println("GetMessagFromLogDbToMgDb() message from db ", tableName, from, to, MsgType, Sendtime, ",nowCount=", nowCount)
		}

		msgList = append(msgList, immsg)
	}

	rows.Close()

	allCount := uint32(len(msgList))

	if allCount == 0 {
		infoLog.Println("GetMessagFromLogDbToMgDb() empty ", tableName, fromid, toid)
		return allCount, nil
	} else {

		infoLog.Println("GetMessagFromLogDbToMgDb() allCount ", tableName, fromid, toid, ",allCount=", allCount)

	}

	bulkCount := uint32(10)
	loop := int(allCount / bulkCount)
	start := uint32(0)
	end := bulkCount

	if bulkCount >= allCount {
		infoLog.Println("GetMessagFromLogDbToMgDb() first save end=", end)
		saveReportMsg(msgList[start:], userMark)
		return allCount, nil
	}

	for i := 0; i < loop+1; i++ {

		saveReportMsg(msgList[start:end], userMark)
		start = end
		end = end + bulkCount

		if start >= allCount {
			break
		}

		if end >= allCount {
			infoLog.Println("GetMessagFromLogDbToMgDb() last save end=", end)
			saveReportMsg(msgList[start:], 0)
			break
		}
	}

	return allCount, nil

}

func handleMomentReport(Data *ReportData, Userid uint32, ServerTs, OsType uint32) {

	Mid := Data.ReportContent

	if Mid == "" {
		infoLog.Println(Userid, "handleMomentReport() Exists failed err=Mid empty")
		return
	}

	redKey := "MID_REPORT_" + Mid

	strUserid := fmt.Sprintf("%d", Userid)

	strFirstReport := fmt.Sprintf("%d", ServerTs)

	ok, err := MgRedis.Exists(redKey)

	if err != nil {
		infoLog.Println(Userid, "handleMomentReport() Exists failed err=", err)
	}

	if ok {

		reportTime, err := MgRedis.HgetInt64(redKey, strUserid)

		if reportTime > 0 && err == nil {
			infoLog.Println(Userid, "handleMomentReport() HgetInt64 reportTime already exist=", reportTime)
			return
		}

		incVal, err := MgRedis.Hincrby(redKey, "reported_count", 1)

		if err != nil {
			infoLog.Println(Userid, "handleMomentReport() Hincrby err=", err, redKey, strUserid)
		}

		report_id := fmt.Sprintf("%d", Data.ReportId) //被举报者的id
		if incVal >= int64(momentReportBanCount) && strings.Contains(extra_users, ","+report_id+",") == false {
			//infoLog.Println("debug1", incVal >= int64(momentReportBanCount))
			//infoLog.Println("debug1", strings.Contains(extra_users, ","+report_id+",") == false, report_id, extra_users)
			//这个post time 实际是首次被举报时间
			postTime, err := MgRedis.HgetInt64(redKey, "post_time")

			if err != nil {
				infoLog.Println(Userid, "handleMomentReport() HgetInt64 post_time fail err=", err)
				return
			}

			reportDiff := uint32(int64(ServerTs) - postTime)
			//infoLog.Println("debug extra_users", strings.Contains(extra_users, ","+strUserid+",") == false, strUserid)
			if reportDiff > 0 && reportDiff < momentReportBanDuration {

				Reason := fmt.Sprintf("report-over-%d", momentReportBanCount)

				userPri.HanderUserByActionType(Mid, Data.ReportId, []uint32{common.WORD_BAN_BOTH_MMT}, Reason)

				saveMomentReportedOver(Mid, Data.ReportId, OsType, ServerTs)

				code, err := momentAPI.ModMntStatus(Mid, 2, Reason)
				if err != nil {
					infoLog.Printf("handleMomentReport() uid=%d,Mid=%s,code=%d,diff=%d ModMntStatus fail err=%s", Userid, Mid, code, reportDiff, err.Error())
				} else {
					infoLog.Printf("handleMomentReport() uid=%d,Mid=%s,reportCnt=%d,code=%d,diff=%d ModMntStatus ok ", Userid, Mid, incVal, code, reportDiff)
				}

			} else {

				infoLog.Printf("handleMomentReport() Hincrby ,not in duration incVal=%d,momentReportBanCount=%d,reportDiff=%d,userid=%d", incVal, momentReportBanCount, reportDiff, Userid)
			}

		} else {

			infoLog.Printf("handleMomentReport() Hincrby ,not enough incVal=%d,momentReportBanCount=%d", incVal, momentReportBanCount)
		}

	} else {

		strpostId := fmt.Sprintf("%d", Data.ReportId)

		vals := []string{
			"poster", strpostId,
			"post_time", strFirstReport,
			"reported_count", "1",
			strUserid, strFirstReport,
		}

		err = MgRedis.Hmset(redKey, vals)

		infoLog.Println(strpostId, Mid, "handleMomentReport() Hmset = ", err)

		//可以达到在某个连续的区间内 被举报N次 隐藏的效果
		MgRedis.Expire(redKey, 2*86400)

	}
}

func saveSensitiveMsgToUserChat(Data *SensitiveData, Userid uint32, ServerTs uint32) {

	msg_id_src := fmt.Sprintf("%s_%d_%d", Data.Msg, Userid, ServerTs)

	MsgID := common.Crc64(msg_id_src)
	msgIntType := 0

	if Data.ReportType == 4 {

		msgIntType = 1
	}

	r1, err1 := Mgdb.Exec("INSERT INTO mg_user_chatlog(fromid,toid,msg_id,source_type,message_type,user_mark,message,sendtime) VALUES(?,?,?,?,?,?,?,?)",
		Data.From,
		Data.To,
		fmt.Sprintf("%v", MsgID),
		3,
		msgIntType,
		1,
		"{"+Data.KeyWords+"}"+Data.Msg,
		common.QtDate(ServerTs),
	)

	if err1 != nil {
		infoLog.Println("saveSensitiveMsgToUserChat() mg_user_chatlog insert failed err=", err1, Userid, MsgID)
		return
	}

	rowaf, _ := r1.RowsAffected()
	infoLog.Printf("saveSensitiveMsgToUserChat() mg_user_chatlog ok from=%d,to=%d,rowaf=%d,MsgID=%v", Data.From, Data.To, rowaf, MsgID)

}

func saveSensitiveMsg(Data *SensitiveData, Userid uint32, ServerTs uint32) {

	r1, err1 := Logdb.Exec("INSERT INTO LOG_SENSITIVE_MSG(FROMID,TOID,REPORT_TYPE,KEYWORDS,MESSAGE,CREATETIME) VALUES(?,?,?,?,?,?)",
		Data.From,
		Data.To,
		Data.ReportType,
		Data.KeyWords,
		Data.Msg,
		common.QtDate(ServerTs),
	)

	if err1 != nil {
		infoLog.Println(Userid, "saveSensitiveMsg() LOG_SENSITIVE_MSG insert failed err=", err1)
		return
	}

	rowaf, _ := r1.RowsAffected()

	infoLog.Printf("saveSensitiveMsg() LOG_SENSITIVE_MSG ok from=%d,to=%d,rowaf=%d", Data.From, Data.To, rowaf)

	if Data.ReportType != SENSITIVE_TYPE_SEXXY {
		return
	}

	query := fmt.Sprintf("SELECT  COUNT(DISTINCT(TOID)) AS send_sensitice_nums, COUNT(*) AS send_sensitice_count FROM `LOG_SENSITIVE_MSG` WHERE FROMID = %v AND  REPORT_TYPE =%d  GROUP BY FROMID",
		Data.From, SENSITIVE_TYPE_SEXXY)
	send_sensitice_nums := 0
	send_sensitice_count := 0

	err1 = Logdb.QueryRow(query).Scan(&send_sensitice_nums, &send_sensitice_count)

	if err1 != nil {
		infoLog.Println(Userid, "saveSensitiveMsg() from QueryRow LOG_SENSITIVE_MSG ", err1)
		return
	}

	query = fmt.Sprintf("SELECT NATIONALITY as nation,SEX as sex,HT_USER_ACCOUNT.REGTIME as regtime,NATIVELANG as nativelang,LEARNLANG1 as learnlang FROM HT_IMDB.HT_USER_ACCOUNT LEFT JOIN HT_IMDB.HT_USER_BASE USING(USERID) LEFT JOIN HT_IMDB.HT_USER_LANGUAGE USING(USERID) WHERE HT_USER_ACCOUNT.USERID  = %d ",
		Data.From)

	nation := ""
	sex := 0
	regtime := ""
	nativelang := ""
	learnlang := ""

	err1 = Mgdb.QueryRow(query).Scan(&nation, &sex, &regtime, &nativelang, &learnlang)

	if err1 != nil {
		infoLog.Println(Userid, "saveSensitiveMsg() from HT_USER_ACCOUNT ", err1)
	}

	// r1, err1 = Mgdb.Exec("INSERT INTO mg_report_black_user "+
	// 	"(userid,send_sensitice_nums,send_sensitice_count,`type`,sensitive_flag,sensitive_time,nation,sex,regtime,nativelang,learnlang,status,updatetime) "+
	// 	" VALUES(?,?,?,?,?,?,?,?,?,?,?,1,NOW()) ON DUPLICATE KEY UPDATE send_sensitice_count=VALUES(send_sensitice_count),send_sensitice_nums=VALUES(send_sensitice_nums),"+
	// 	"sensitive_time=VALUES(sensitive_time),sensitive_flag=VALUES(sensitive_flag) ,status=VALUES(status),updatetime=VALUES(updatetime)",
	// 	Data.From,
	// 	send_sensitice_nums,
	// 	send_sensitice_count,
	// 	0,
	// 	1,
	// 	common.QtDate(ServerTs),
	// 	nation,
	// 	sex,
	// 	regtime,
	// 	nativelang,
	// 	learnlang,
	// )

	// if err1 != nil {
	// 	infoLog.Println(Userid, "saveSensitiveMsg() insert mg_report_black_user fail err=", err1)
	// 	return
	// }

	// rowaf2, _ := r1.RowsAffected()

	// infoLog.Printf("saveSensitiveMsg() mg_report_black_user ok from=%d,to=%d,rowaf=%d", Data.From, Data.To, rowaf2)

}

func saveReportMsg(msgList []IMMsg, userMark int) error {

	sqlStr := "INSERT INTO mg_user_chatlog( fromid,toid,msg_id,source_type,message_type,user_mark,message,sendtime) VALUES "

	vals := []interface{}{}
	savedIds := []uint64{}

	// infoLog.Println("saveSensitiveMsg() savedMessageIds ", savedMessageIds)

	check := false
	if userMark == 1 { //客户端上报消息才需要检查
		check = true
	}

	for _, row := range msgList {

		reportMsg := row.GetReportMsg(check)

		_, ok := savedMessageIds[reportMsg.MsgID]

		if ok && userMark == 0 {
			infoLog.Println("saveReportMsg() skip message ", reportMsg.FromId, reportMsg.ToId, reportMsg.MsgID, reportMsg.Sendtime, row.MsgID)
			continue
		}

		if userMark == 1 {
			infoLog.Println("saveReportMsg() mark message ", reportMsg.FromId, reportMsg.ToId, reportMsg.MsgID, reportMsg.Sendtime, row.MsgID)
		}

		if userMark == 0 {
			infoLog.Println("saveReportMsg() db  message ", reportMsg.FromId, reportMsg.ToId, reportMsg.MsgID, reportMsg.Sendtime, row.MsgID)
		}

		savedIds = append(savedIds, reportMsg.MsgID)

		sqlStr += "(?, ?, ?,?, ?, ?,?, ?), "

		//voip 2018-3-6
		if reportMsg.Message == "" {
			reportMsg.Message = "{voip}"
		}

		vals = append(vals, reportMsg.FromId, reportMsg.ToId, fmt.Sprintf("%v", reportMsg.MsgID),
			reportMsg.SourceType, reportMsg.MessageType, userMark, reportMsg.Message, reportMsg.Sendtime)
	}

	if len(savedIds) == 0 {
		return errors.New("no data to save")
	}

	sqlStr = sqlStr[0 : len(sqlStr)-2]

	if userMark == 1 {
		sqlStr = sqlStr + " ON DUPLICATE KEY UPDATE user_mark=VALUES(user_mark)"
	} else {
		//如果没有打标签的话，覆盖的时候不重写 usermark
		sqlStr = sqlStr + " ON DUPLICATE KEY UPDATE message_type=VALUES(message_type),sendtime=VALUES(sendtime)"
	}

	//prepare the statement
	stmt, err1 := Mgdb.Prepare(sqlStr)

	if err1 != nil {
		infoLog.Println("saveReportMsg() mg_user_chatlog Prepare failed err=", err1, sqlStr, userMark)
		return err1
	}

	//format all vals at once
	r1, err1 := stmt.Exec(vals...)

	if err1 != nil {
		infoLog.Println("saveReportMsg() mg_user_chatlog Prepare Exec err=", err1, sqlStr, userMark)
		return err1
	}

	savedMessageIdsLocker.Lock()
	for _, v1 := range savedIds {
		savedMessageIds[v1] = 1
	}
	savedMessageIdsLocker.Unlock()

	rowaf, _ := r1.RowsAffected()
	infoLog.Printf("saveReportMsg() mg_user_chatlog ok  rowaf=%d,userMark=%d", rowaf, userMark)

	return nil

}

//举报拉黑同步入库 rtype 1 为举报 2为敏感词 3 信息流举报
func addReportBlackUser(Userid uint32, ServerTs uint32, rtype uint8) error {

	//获取用户聊天信息统计
	i_speak_count, s_speak_count, err := Chat_api.GetAllChatCount(Userid)
	checkError("addReportBlackUser get user chat count error ", err)
	infoLog.Println(i_speak_count, s_speak_count)
	init_chat_count := uint32(len(i_speak_count))
	var userids map[uint32]uint8
	user := model.NewUserModel(GormMasterdb)
	userids = make(map[uint32]uint8)
	for _, v := range i_speak_count {
		userids[v.GetToId()] = 0
	}
	for _, v := range s_speak_count {
		userids[v.GetToId()] = 0
	}

	//infoLog.Println("addReportBlackUser users chat ", userids)
	chat_count_country, err0 := user.GetUsersCountry(userids)
	var json_analysis []byte
	checkError("GetUsersCountry failed", err0)
	if chat_count_country != nil {
		chat_count_analysis := countChatUser(chat_count_country, uint32(len(userids)))

		json_analysis, err = json.Marshal(chat_count_analysis)
		checkError("json_analysis failed", err)
	}

	report := model.NewReportModel(GormMgdb)

	//获取用户举报拉黑记录
	report_user, err1 := report.GetReport(Userid)
	checkError("addReportBlackUser get user data failed ", err1)
	ctime := common.QtDate(ServerTs)
	//已存在记录
	if report_user.Id != 0 {
		if rtype == REPORT_BLACK || rtype == MOMENT_BLACK {
			err = report.UpdateReport(Userid, map[string]interface{}{"Report_count": report_user.Report_count + 1, "Types": rtype, "Report_flag": 1, "Init_chat_count": init_chat_count, "Report_time": ctime, "Updatetime": ctime, "Chat_count_analysis": string(json_analysis)})
			checkError("addReportBlackUser update report failed ", err)
		} else if rtype == REPORT_SENSITICE {

			err = report.UpdateReport(Userid, map[string]interface{}{"Send_sensitice_count": report_user.Send_sensitice_count + 1, "Sensitive_flag": 1, "Init_chat_count": init_chat_count, "Sensitive_time": ctime, "Updatetime": ctime, "Chat_count_analysis": string(json_analysis)})
			checkError("addReportBlackUser update sensitive failed ", err)
		} else if rtype == RT_BLACKPULL { //拉黑
			err = report.UpdateReport(Userid, map[string]interface{}{"Black_count": report_user.Black_count + 1, "Init_chat_count": init_chat_count, "Updatetime": ctime, "Black_time": ctime, "Chat_count_analysis": string(json_analysis)})
			checkError("addReportBlackUser update report failed ", err)

		}

		return err
	} else {

		user_info, err2 := user.GetUserDetail(Userid)
		checkError("addReportBlackUser get user info failed", err2)

		add_new := model.Report_black_user{
			Userid:               Userid,
			Black_count:          0,
			Report_count:         0,
			Send_sensitice_count: 0,
			Send_sensitice_nums:  0,
			Init_chat_count:      init_chat_count,
			Report_entrance:      1,
			Black_entrance:       1,
			Types:                1,
			Black_flag:           0,
			Report_flag:          0,
			Sensitive_flag:       0,
			Status:               1,
			Ostype:               user_info.TTYPE,
			Learnlang:            user_info.LEARNLANG1,
			Nativelang:           user_info.NATIVELANG,
			Nation:               user_info.NATIONALITY,
			Sex:                  user_info.SEX,
			Regtime:              user_info.REGTIME,
			Updatetime:           ctime,
			Chat_count_analysis:  string(json_analysis),
		}
		if rtype == REPORT_BLACK || rtype == MOMENT_BLACK { //普通举报 信息流举报
			add_new.Report_count = 1
			add_new.Report_time = ctime
			add_new.Report_flag = 1
			add_new.Types = int64(rtype)
		} else if rtype == REPORT_SENSITICE { //敏感词
			add_new.Send_sensitice_count = 1
			add_new.Sensitive_time = ctime
			add_new.Sensitive_flag = 1
		} else if rtype == RT_BLACKPULL { //拉黑
			add_new.Black_count = 1
			add_new.Black_time = ctime
		}

		err = report.CreateReport(add_new)
		checkError("addReportBlackUser sync user data faile ", err)
		return err

	}

	return nil
}

//统计用户国际比率聊天
func countChatUser(chat_count_country map[string]uint32, count uint32) map[string]uint32 {

	chat_count_analysis := make(map[string]uint32)

	chat_count_analysis["total"] = count
	for k, v := range chat_count_country {
		chat_count_analysis[k] = v
	}
	return chat_count_analysis

}
func saveMomentReportedOver(Mid string, uidWhoPost uint32, OsType uint32, postTime uint32) {

	reason := fmt.Sprintf("%d-reported-over-%d", momentReportBanCount, momentReportBanDuration)

	r1, err1 := Logdb.Exec("INSERT INTO LOG_BAD_MOMENT (USERID,`OSTYPE`, `MID`, IS_BAD_WORDS,BAD_WORDS,CREATETIME) VALUES(?,?,?,?,?,?) "+
		"ON DUPLICATE KEY UPDATE IS_BAD_WORDS=VALUES(IS_BAD_WORDS),BAD_WORDS=VALUES(BAD_WORDS),CREATETIME=VALUES(CREATETIME)",
		uidWhoPost,
		OsType,
		Mid,
		2,
		reason,
		postTime*1000,
	)

	if err1 != nil {
		infoLog.Println(uidWhoPost, Mid, "saveMomentReportedOver() LOG_BAD_MOMENT insert failed err=", err1)
		return
	}

	id1, _ := r1.LastInsertId()

	infoLog.Println(uidWhoPost, Mid, "saveMomentReportedOver() LOG_BAD_MOMENT ok Id=", id1)
}

func checkError(info string, err error) {
	if err != nil {
		infoLog.Printf(info, err)
	}
}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := common.InitLogAndOption(&options, &infoLog)

	Masterdb, err = common.InitMySqlFromSectionWithMb4(cfg, infoLog, "MASTER_MYSQL", 3, 1)
	common.CheckError(err)
	defer Masterdb.Close()

	Mgdb, err = common.InitMySqlFromSectionWithMb4(cfg, infoLog, "MG_MYSQL", 3, 1)
	common.CheckError(err)
	defer Mgdb.Close()

	Logdb, err = common.InitMySqlFromSection(cfg, infoLog, "LOGDB_MYSQL", 4, 1)
	common.CheckError(err)
	defer Logdb.Close()

	SlaveLogdb, err = common.InitMySqlFromSection(cfg, infoLog, "SLAVE_LOGDB_MYSQL", 13, 1)
	common.CheckError(err)
	defer SlaveLogdb.Close()

	//gorm db master
	GormMasterdb, err = common.InitGormFromDb("mysql", Masterdb)
	common.CheckError(err)
	defer GormMasterdb.Close()

	GormMgdb, err = common.InitGormFromDb("mysql", Mgdb)
	common.CheckError(err)
	defer GormMgdb.Close()

	GormLogdb, err = common.InitGormFromDb("mysql", SlaveLogdb)
	common.CheckError(err)
	defer GormLogdb.Close()
	redis_host := cfg.Section("REDIS").Key("ruler_redis_ip").MustString("127.0.0.1")
	redis_port := cfg.Section("REDIS").Key("ruler_redis_port").MustString("6379")

	RulerRedis = tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	redis_host = cfg.Section("REDIS").Key("userinfo_redis_ip").MustString("127.0.0.1")
	redis_port = cfg.Section("REDIS").Key("userinfo_redis_port").MustString("6379")

	UserinfoRedis = tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	redis_host = cfg.Section("REDIS").Key("mg_redis_ip").MustString("127.0.0.1")
	redis_port = cfg.Section("REDIS").Key("mg_redis_port").MustString("6379")

	MgRedis = tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	chat_record_ip := cfg.Section("CHATRECORD").Key("ip").MustString("127.0.0.1")
	chat_record_port := cfg.Section("CHATRECORD").Key("port").MustString("17600")
	chat_record_conn_limit := cfg.Section("CHATRECORD").Key("pool_limit").MustInt(1000)

	Chat_api = tcpcommon.NewChatRecordApi(chat_record_ip, chat_record_port, 1*time.Second, 1*time.Second, &tcpcommon.HeadV2Protocol{}, chat_record_conn_limit)
	// init moment api
	moment_ip := cfg.Section("SERVICE_CONFIG").Key("moment_ip").MustString("127.0.0.1")
	moment_port := cfg.Section("SERVICE_CONFIG").Key("moment_port").MustString("15500")

	moment_timeout := cfg.Section("SERVICE_CONFIG").Key("moment_timeout").MustUint(8)
	moment_max_conn := cfg.Section("SERVICE_CONFIG").Key("moment_max_conn").MustInt(100)
	moment_url := cfg.Section("SERVICE_CONFIG").Key("moment_url").MustString("http://qtest.hellotalk.org")

	notify_api_host := cfg.Section("SERVICE_CONFIG").Key("notify_api_host").MustString("http://qtest.hellotalk.org")

	v2proto := &tcpcommon.HeadV2Protocol{}
	momentAPI = tcpcommon.NewMntApi(moment_ip, moment_port, time.Duration(moment_timeout)*time.Second,
		time.Duration(moment_timeout)*time.Second, v2proto, moment_max_conn)

	momentReportBanDuration = uint32(cfg.Section("SERVICE_CONFIG").Key("moment_report_ban_duration").MustUint(86400))
	momentReportBanCount = uint32(cfg.Section("SERVICE_CONFIG").Key("moment_report_ban_count").MustUint(5))

	savedMessageIds = map[uint64]uint8{}
	savedMessageIdsLocker = new(sync.Mutex)
	//extra_users = cfg.Section("SERVICE_CONFIG").Key("extra_users").MustString("")

	// user info cache api
	// userInfoIp := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	// userInfoPort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	// infoLog.Printf("user info cache tcpcommonip=%v port=%v", userInfoIp, userInfoPort)
	// userInfoCacheApi = tcpcommon.NewUserInfoCacheApi(userInfoIp, userInfoPort, 1*time.Second, 1*time.Second, &tcpcommon.HeadV2Protocol{}, 1000)
	Filter = common.NewTextFiler(Mgdb, common.CHECK_IP_REG_ADDRESS, infoLog)
	userPri = common.NewUserPrivilege(infoLog, Masterdb, RulerRedis, UserinfoRedis, momentAPI, strings.Split(moment_url, ","), notify_api_host)

	err = InitIgnoreUsersFromMgDb(Filter)
	if err != nil {
		infoLog.Println("InitIgnoreUsersFromMgDb() get moment hide ignore users failed", err)
	}
	infoLog.Println("monent hide ignore users start", extra_users)
	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	common.CheckError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	common.CheckError(err)

	// creates a server
	sendChanLimit := cfg.Section("CHANLIMIT").Key("max_send_chan_count").MustUint(1000)
	recvChanLimit := cfg.Section("CHANLIMIT").Key("max_recv_chan_count").MustUint(1000)
	config := &gotcp.Config{
		PacketSendChanLimit:    uint32(sendChanLimit),
		PacketReceiveChanLimit: uint32(recvChanLimit),
	}

	srv := gotcp.NewServer(config, &Callback{}, &tcpcommon.HeadV3Protocol{})

	// // starts service
	go srv.Start(listener, time.Second)
	// infoLog.Println("listening:", listener.Addr())

	// // catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()

}
