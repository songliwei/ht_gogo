package main

import (
	"encoding/json"

	"github.com/HT_GOGO/gotcp/libcrypto"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_data_report"
	"github.com/HT_GOGO/gotcp/webapi/common"
	// "github.com/bsm/sarama-cluster"
	"github.com/gansidui/gotcp/libcomm"
	"github.com/gin-gonic/gin"
	// "github.com/jessevdk/go-flags"
	"errors"
	"github.com/golang/protobuf/proto"
	"log"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	KCC2018          = "bin/cc2018"
	KCC2018KeyLength = 16
)

var (
	ErrInputParam = errors.New("err param error")
)

type EventTask struct {
	TimeStamp int64  // 产生任务的时间戳
	RemoteIp  string // remote ip
	Message   []byte // PB加密之后的字符串
}

type TaskEleme struct {
	Uid          uint32            `json:"uid" binding:"required"`
	TerminalType uint32            `json:"terminaltype" binding:"required"`
	Version      string            `json:"version" binding:"required"`
	Seq          uint32            `json:"seq" binding:"required"`
	TimeStamp    string            `json:"@timestamp" binding:"required"`
	Event        string            `json:"event" binding:"required"`
	EventTag     map[string]string `json:"event_tag" binding:"required"`
	National     string            `json:"national" binding:"omitempty"`
	Birthday     string            `json:"birthday" binding:"omitempty"`
	Sex          uint8             `json:"sex" binding:"omitempty"`
	VipType      uint8             `json:"vip_type" binding:"omitempty"`
	TeachLang    uint8             `json:"teach_lang" binding:"omitempty"`
	LearnLang    uint8             `json:"learn_lang" binding:"omitempty"`
	AreaCode     string            `json:"area_code" binding:"omitempty"`
	ClientIp     string            `json:"client_ip" binding:"omitempty"`
}

var (
	infoLog       *log.Logger
	options       common.Options
	elkRedis      *tcpcommon.RedisApi
	redisTopic    string
	eventTaskChan chan *EventTask
)

func TaskHandleLoop(index int) {
	defer func() {
		recover()
	}()
	infoLog.Printf("TaskHandleLoop index=%v", index)
	for {
		taskPoint := <-eventTaskChan
		infoLog.Printf("task ts=%v", taskPoint.TimeStamp)
		reqBody := new(ht_data_report.ReportGroupLessonProductDataReqBody)
		err := proto.Unmarshal(taskPoint.Message, reqBody)
		if err != nil {
			infoLog.Printf("TaskHandleLoop proto Unmarshal failed  err=%s", err)
			attr := "goelk/proto_unmarshal_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		attr := "goelk/" + reqBody.GetEvent() + "_count"
		libcomm.AttrAdd(attr, 1)
		userInfo := reqBody.GetUserInfo()
		infoLog.Printf("TaskHandleLoop recv uid=%v terminal_type=%v cli_version=%s national=%s vip_type=%v seq=%v ts=%v event=%s",
			userInfo.GetReqUid(),
			userInfo.GetTerminalType(),
			userInfo.GetCliVersion(),
			userInfo.GetNational(),
			userInfo.GetVipType(),
			reqBody.GetSeq(),
			reqBody.GetTs(),
			reqBody.GetEvent())
		tagList := reqBody.GetTagBody()
		tagMap := map[string]string{}
		for _, v := range tagList {
			tagMap[v.GetName()] = v.GetValue()
		}

		taskEleme := TaskEleme{
			Uid:          userInfo.GetReqUid(),
			TerminalType: userInfo.GetTerminalType(),
			Version:      userInfo.GetCliVersion(),
			Seq:          reqBody.GetSeq(),
			TimeStamp:    transferMsec(reqBody.GetTs()),
			Event:        reqBody.GetEvent(),
			EventTag:     tagMap,
			National:     userInfo.GetNational(),
			Birthday:     userInfo.GetBirthday(),
			Sex:          uint8(userInfo.GetSex()),
			VipType:      uint8(userInfo.GetVipType()),
			TeachLang:    uint8(userInfo.GetTeachLang()),
			LearnLang:    uint8(userInfo.GetLearnLang()),
			AreaCode:     userInfo.GetAreaCode(),
			ClientIp:     taskPoint.RemoteIp,
		}
		taksSlic, err := json.Marshal(taskEleme)
		if err != nil {
			infoLog.Printf("TaskHandleLoop uid=%v terminal_type=%v cli_version=%s unmarshal failed err=%s",
				userInfo.GetReqUid(),
				userInfo.GetTerminalType(),
				userInfo.GetCliVersion(),
				err)
			attr := "goelk/json_marshal_failed"
			libcomm.AttrAdd(attr, 1)
			continue
		}
		err = PublishToRedisQueue(redisTopic, taksSlic)
	}
}

func transferMsec(timeStr uint64) (str string) {
	msec := timeStr - (timeStr/1000)*1000
	nsec := msec * 1000 * 1000
	sec := (timeStr - msec) / 1000
	return time.Unix(int64(sec), int64(nsec)).Format(time.RFC3339)
}

func PublishToRedisQueue(topic string, taskSlic []byte) (err error) {
	if topic == "" || len(taskSlic) == 0 {
		infoLog.Printf("PublishToRedisQueue failed input param error")
		err = errors.New("input param error")
		return err
	}
	listSize, err := elkRedis.Rpush(redisTopic, string(taskSlic))
	if err != nil {
		infoLog.Println("redis exec RPUSH failed err=%s", err)
		return err
	}
	if listSize > 5000 {
		infoLog.Printf("PublishToRedisQueue listSize=%v", listSize)
		attr := "goelk/redis_queue_full"
		libcomm.AttrAdd(attr, 1)
	}
	return nil
}

func GetClientIp(c *gin.Context) (ip string, err error) {
	if c == nil {
		err = ErrInputParam
		return ip, err
	}
	// infoLog.Printf("GetClientIp=%#v", c.Request.Header)
	if (c.GetHeader("X-Wns-Qua") != "" || c.GetHeader("X-Wns-DeviceInfo") != "" || c.GetHeader("X-Wns-Wid") != "") &&
		c.GetHeader("X-Forwarded-For") != "" {
		ip = c.GetHeader("X-Forwarded-For")
		infoLog.Printf("GetClientIp c.GetHeader x-forwarded-for ip=%s", ip)
		return ip, nil
	} else {
		ip, _, err = net.SplitHostPort(strings.TrimSpace(c.Request.RemoteAddr))
		infoLog.Printf("GetClientIp c.Request.RemoteAddr ip=%s", ip)
		return ip, err
	}
}

func main() {
	// 加载配置文件
	cfg, err := common.InitLogAndOption(&options, &infoLog)
	common.CheckError(err)

	router := gin.Default()

	redisTopic = cfg.Section("REDIS").Key("topic").MustString("topic")
	redisIp := cfg.Section("REDIS").Key("ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("port").MustInt(12500)
	elkRedis = tcpcommon.NewRedisApi(redisIp + ":" + strconv.Itoa(redisPort))
	infoLog.Printf("REDIS topic=%s redisHost=%s redisPort=%v", redisTopic, redisIp, redisPort)

	LOCAL_SERVER := cfg.Section("LOCAL_SERVER")
	serviceAccessPrefix := LOCAL_SERVER.Key("service_access_prefix").MustString("")
	// 前端配置信息，包括接口等
	apiList := map[string]string{
		"groupLesson": serviceAccessPrefix + "/group_lesson",
	}
	// 自身服务的配置
	srvIP := LOCAL_SERVER.Key("bind_ip").MustString("0.0.0.0")
	srvPort := LOCAL_SERVER.Key("bind_port").MustString("9888")

	taskChanLen := cfg.Section("CHAN_LIMIT").Key("length").MustInt(10000)
	workerCount := cfg.Section("CHAN_LIMIT").Key("worker_count").MustInt(30)
	eventTaskChan = make(chan *EventTask, taskChanLen)
	for i := 0; i < workerCount; i += 1 {
		go TaskHandleLoop(i)
	}

	// 查看课程介绍
	router.POST(apiList["groupLesson"], func(c *gin.Context) {
		// 处理message
		// 统计总量
		infoLog.Printf("group_lesson recv req")
		attr := "goelk/recv_req_count"
		libcomm.AttrAdd(attr, 1)
		message, err := c.GetRawData()
		if err != nil {
			infoLog.Printf("groupLesson c.GetRawData failed err=%s", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if len(message) == 0 {
			infoLog.Printf("groupLesson message empty")
			attr := "goelk/empty_msg_count"
			libcomm.AttrAdd(attr, 1)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		// 根据头部的content-type 进行解密
		var decryText string
		var cryKey string
		contentType := c.GetHeader("content-type")
		if contentType == KCC2018 {
			cryKey = string(message[0:KCC2018KeyLength])
			cryText := string(message[KCC2018KeyLength:])
			infoLog.Printf("groupLesson cryKeyLen=%v cryText=%v", len(cryKey), len(cryText))
			decryText = libcrypto.TEADecrypt(cryText, cryKey)
		}
		infoLog.Printf("groupLesson message=%s key=%x decryText=%v", message, cryKey, len(decryText))
		remoteIp, err := GetClientIp(c)
		infoLog.Printf("groupLesson remoteIp=%s err=%s", remoteIp, err)
		curTaskPoint := &EventTask{
			TimeStamp: time.Now().UnixNano() / 1000000,
			RemoteIp:  remoteIp,
			Message:   []byte(decryText),
		}
		select {
		case eventTaskChan <- curTaskPoint:
			infoLog.Printf("message length=%v timestamp=%v put into chan succ", len(curTaskPoint.Message), curTaskPoint.TimeStamp)
		default:
			attr := "goelk/chan_is_full"
			libcomm.AttrAdd(attr, 1)
			infoLog.Printf("message chan is full")
		}
		rspBody := &ht_data_report.ReportGroupLessonProductDataRspBody{
			Status: &ht_data_report.ReportHeader{
				Code:   proto.Uint32(uint32(ht_data_report.REPORT_RET_CODE_RET_SUCCESS)),
				Reason: []byte("success"),
			},
		}
		s, err := proto.Marshal(rspBody)
		if err != nil {
			infoLog.Printf("message proto.Marshal failed err=%s", err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		encryText := libcrypto.TEAEncrypt(string(s), cryKey)
		payLoad := cryKey + encryText
		infoLog.Printf("message resp payLoad=%x payLoadLen=%v", payLoad, len(payLoad))
		c.String(http.StatusOK, payLoad)
	})

	router.Run(srvIP + ":" + srvPort)
}
