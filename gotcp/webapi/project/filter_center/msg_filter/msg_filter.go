package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gansidui/gotcp"
	"github.com/gansidui/gotcp/libcomm"
	tcpcommon "github.com/gansidui/gotcp/tcpfw/common"
	"github.com/gansidui/gotcp/webapi/common"
	"github.com/gansidui/gotcp/webapi/common/sdk"
	"github.com/gansidui/gotcp/webapi/project/filter_center/org"
	_ "github.com/go-sql-driver/mysql"
	sphinx "github.com/lilien1010/sphinx" //经过改造的sphinx go driver
	"github.com/nsqio/go-nsq"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	// "sync"
	"syscall"
	"time"
)

/*
功能说明：

决定谁可以和谁聊天，
1：拉黑
2：关键词
3：后台禁言
4：敏感词聊天
5：测试用户，客服账号
6：聊天5条的限制
*/

const (
	PF_CHECK_SIG      = 1
	PF_CHECK_HEADURL  = 2
	PF_CHECK_NICKNAME = 3

	DEAFULT_HEAD_HOST = "http://cn-head-cdn.nihaotalk.com/"
)

var (
	infoLog *log.Logger

	MasterDB *sql.DB
	LogDB    *sql.DB
	MgDB     *sql.DB
	options  common.Options

	userInfoCacheApi *tcpcommon.UserInfoCacheApi
	momentAPI        *tcpcommon.MntApi

	msgFilter *common.TextFilter

	userPri *common.UserPrivilege

	userinfoDBApi *org.UserInfoDbApi

	imageChecker *sdk.ImageFilter
)

type Callback struct{}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {

	packet, ok := p.(*tcpcommon.HeadV2Packet)
	if !ok {
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:[Uid=%v,Seq=%v,Cmd=%v,Len=%v]", head.Uid, head.Seq, head.Cmd, head.Len)

	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	return true

}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := common.InitLogAndOption(&options, &infoLog)

	MasterDB, err = common.InitMySqlFromSection(cfg, infoLog, "MASTER_MYSQL", 3, 1)
	common.CheckError(err)
	defer MasterDB.Close()

	LogDB, err = common.InitMySqlFromSection(cfg, infoLog, "LOGDB_MYSQL", 3, 2)
	common.CheckError(err)
	defer LogDB.Close()

	MgDB, err = common.InitMySqlFromSection(cfg, infoLog, "MG_MYSQL", 3, 2)
	common.CheckError(err)
	defer MgDB.Close()

	tcpConfig := &gotcp.Config{
		PacketSendChanLimit:    uint32(10),
		PacketReceiveChanLimit: uint32(10),
	}

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	common.CheckError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	common.CheckError(err)

	srv := gotcp.NewServer(tcpConfig, &Callback{}, &tcpcommon.HeadV2Protocol{})

	// // starts service
	go srv.Start(listener, time.Second)
	// infoLog.Println("listening:", listener.Addr())

	// // catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()

}
