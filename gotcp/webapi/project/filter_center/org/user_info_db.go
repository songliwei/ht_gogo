// Copyright 2017 Liling
//
// HelloTalk.inc

package org

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/webapi/common"
	sphinx "github.com/lilien1010/sphinx" //经过改造的sphinx go driver
	"log"
	"strconv"
	"strings"
	"time"
	// "reflect"
	// "unicode/utf8"
)

const (
	PMC_SEX         = 0  // 性别
	PMC_BIRTHDAY    = 1  // 生日
	PMC_NATIONALITY = 2  // 国籍检查
	PMC_NICK        = 3  // 用户昵称
	PMC_FULLPY      = 4  // 名称全拼
	PMC_SHORTPY     = 5  // 名称简拼
	PMC_TEXT        = 6  // 文本自我介绍
	PMC_VOICE       = 7  // 语音自我介绍
	PMC_HEADURL     = 8  // 个人头像URL
	PMC_NATIVELANG  = 9  // 用户母语
	PMC_LEARN1      = 10 //
	PMC_LEARN2      = 11 //
	PMC_LEARN3      = 12 //
	PMC_LEARN4      = 13 //
	PMC_TEACH2      = 14 // for multilanguage. The second teach language(NATIVE is the first)
	PMC_TEACH3      = 15
	PMC_COUNT       = 16
)

//作为存储到数据库的日志下表
var UserUpdateInfoCmdMapInt map[string]uint32 = map[string]uint32{
	"user_name":              1,
	"nick_name":              2,
	"sex":                    3,
	"birthday":               4,
	"signature":              5,
	"head_url":               6,
	"timezone":               7,
	"nationality":            8,
	"voice_url":              9,
	"location":               10,
	"lang_native":            11,
	"lang_learn_1":           12,
	"lang_learn_2":           13,
	"lang_learn_3":           14,
	"lang_teach_2":           15,
	"lang_teach_3":           16,
	"password":               17,
	"email":                  18,
	"unregister":             19,
	"user_privacy":           20,
	"notify_setting":         21,
	"exact_match":            22,
	"once_modified":          23,
	"stop_location":          24,
	"same_gender":            25,
	"find_by_name":           26,
	"find_by_email":          27,
	"dnd_set":                28,
	"hide_self":              29,
	"search_age_range":       30,
	"correct_sentence":       31,
	"absence_nearest":        32,
	"sensitive_detect":       33,
	"lang_modify":            34,
	"multi_2_vip":            35,
	"user_tag":               36,
	"correct_lang_info":      37,
	"correct_group_sentence": 38,
	"report_type":            39,
	"update_location":        41,
	"user_update_location":   41,
}

//不记录到log库
var STOP_LOG_COMMON map[string]uint8 = map[string]uint8{
	"sensitive_detect":  1,
	"autorenew_expired": 1,
	"purchase_item":     1,
	"version_update":    1,
	"report_type":       1,
}

var langFieldMap map[string]string = map[string]string{
	"lang_native": "nativelang", "lang_learn_1": "learnlang1",
	"lang_learn_2": "learnlang2", "lang_learn_3": "learnlang3",
	"lang_teach_2": "teachlang2", "lang_teach_3": "teachlang3",
}

var levelFieldMap map[string]string = map[string]string{
	"lang_native": "", "lang_learn_1": "skilllevel1",
	"lang_learn_2": "skilllevel2", "lang_learn_3": "skilllevel3",
	"lang_teach_2": "teachskilllevel2", "lang_teach_3": "teachskilllevel3",
}

type UserEventData struct {
	Type string      `json:"type" binding:"required"`
	Data interface{} `json:"data" binding:"required"`
}

//{"cmd":"setting_modify","info":{"data":{"status":"off"},"type":"find_by_name"},"os_type":1,"server_ts":1510899056,"user_id":9009567
type UserEvent struct {
	Cmd      string        `json:"cmd" binding:"required"`
	Info     UserEventData `json:"info" binding:"required"`
	Userid   uint32        `json:"user_id" binding:"required"`
	ServerTs uint64        `json:"server_ts" binding:"required"`
	Ostype   uint32        `json:"os_type" binding:"required"`
}

//SphinxDbAPI handle sphinx
//Conn exposes a set of callbacks for the various events that occur on a connection
type UserInfoDbApi struct {
	SphinxClient     *sphinx.Client
	userPri          *common.UserPrivilege
	infoLog          *log.Logger
	LogDb            *sql.DB
	MgDb             *sql.DB
	queryIndex       string
	userCacheInfoApi *tcpcommon.UserInfoCacheApi
}

//NewSphinxDbAPI 初始化一个sphinx 客户端对象
func NewUserInfoDbApi(destIP string, destPort int, queryIndex string,
	MilliTimeOut int,
	openCount int,
	idleCount int,
	LogDb *sql.DB,
	userPri *common.UserPrivilege,
	infoLog *log.Logger) (topicAPI *UserInfoDbApi) {

	sc := sphinx.NewClient().SetServer(destIP, destPort).SetConnectTimeout(MilliTimeOut).SetConnectDuration(1000).SetRetries(2, 1)

	if err := sc.Error(); err != nil {
		// return nil, err
		infoLog.Println("NewUserInfoDbApi() SphinxClient NewClient fail,err=", err)
	}

	if err := sc.Open(); err != nil {
		// return nil, err
		infoLog.Println("NewUserInfoDbApi() SphinxClient Open fail,err=", err)
	}

	if err := sc.GetDb(MilliTimeOut/1000, openCount, idleCount); err != nil {
		// return nil, err
		infoLog.Println("NewUserInfoDbApi() SphinxClient GetDb fail,err=", err)

	}

	sc.SetMaxQueryTime(3)

	api := &UserInfoDbApi{
		SphinxClient: sc,
		LogDb:        LogDb,
		queryIndex:   queryIndex,
		infoLog:      infoLog,
		userPri:      userPri,
	}

	return api
}

func (this *UserInfoDbApi) HideUser(Userid uint32, reason string) error {

	err1 := this.userPri.HideUser([]uint32{Userid}, 1)

	if err1 != nil {
		this.infoLog.Println("HideUser() failed,err=", err1, Userid, reason)
	}

	this.UpdateSphinxSingleVal(Userid, "ishide", 1)

	return err1
}

func (this *UserInfoDbApi) ChangeHead(Userid uint32, head string) error {

	err1 := this.UpdateMemSingleVal(Userid, "headurl", head)

	if err1 != nil {
		this.infoLog.Println("ChangeHead() failed,err=", err1, Userid, head)
	}

	return err1

}

func (this *UserInfoDbApi) ChangeLang(Userid uint32, userData map[string]interface{}) error {

	updateDict := map[string]uint32{}
	updateDictMem := map[string]interface{}{}

	for k, v := range userData {

		// if reflect.TypeOf(v) != map[string]uint32 {
		// 	return errors.New("type error")
		// }

		valMap := v.(map[string]interface{})

		field1, ok := langFieldMap[k]

		if ok {

			langid, ok2 := valMap["language"]

			if ok2 {
				updateDict[field1] = uint32(langid.(float64))
				updateDictMem[field1] = langid
			}

			levelId, okLevel := valMap["level"]
			levelField, ok3 := levelFieldMap[k]

			if ok3 && okLevel {
				updateDict[levelField] = uint32(levelId.(float64))
				updateDictMem[levelField] = levelId
			}

		}

	}

	this.UpdateSpinx(Userid, updateDict)
	err2 := this.UpdateMemUserInfo(Userid, updateDictMem)

	this.infoLog.Println(Userid, " ChangeLang() ", updateDictMem, updateDict)

	return err2
}

func (this *UserInfoDbApi) ChangeSex(Userid uint32, sex uint32) error {

	err1 := this.UpdateMemSingleVal(Userid, "sex", sex)

	this.UpdateSphinxSingleVal(Userid, "sex", sex)

	return err1
}

func (this *UserInfoDbApi) ChangeBirthday(Userid uint32, Birthday string) error {

	pbirt := strings.Replace(Birthday, "-", "", -1)

	pbirtInt, _ := strconv.ParseUint(pbirt, 10, 64)

	err1 := this.UpdateMemSingleVal(Userid, "birthday", Birthday)

	this.UpdateSphinxSingleVal(Userid, "birthday", uint32(pbirtInt))

	return err1
}

func (this *UserInfoDbApi) ChangeTimezone(Userid uint32, Timezone string) error {

	pbirtInt, _ := strconv.ParseUint(Timezone, 10, 64)

	err1 := this.UpdateMemSingleVal(Userid, "timezone48", pbirtInt)

	this.UpdateSphinxSingleVal(Userid, "timezone48", uint32(pbirtInt))

	return err1
}

func (this *UserInfoDbApi) ChangeLocation(Userid uint32, Latitude, Longitude, Country, City string, PlaceId, CityPlaceId uint32) error {

	updateDictMem := map[string]interface{}{
		"country":   Country,
		"locality":  City,
		"placeid":   CityPlaceId,
		"latitude":  Latitude,
		"longitude": Longitude,
	}

	err1 := this.UpdateMemUserInfo(Userid, updateDictMem)

	pbirtInt := common.GetCountryCode(Country)

	SPdict := map[string]uint32{
		"country":     uint32(pbirtInt),
		"cityplaceid": CityPlaceId,
		"placeid":     PlaceId,
	}

	err2 := this.UpdateSpinx(Userid, SPdict)

	if err1 != nil {
		this.infoLog.Println("ChangeLocation() UpdateMemUserInfo failed,err=", err1, "->", Userid, Country, City, PlaceId)
		return err1
	}
	if err2 != nil {
		this.infoLog.Println("ChangeLocation() UpdateSphinxSingleVal failed,err=", err2, "->", Userid, Country, pbirtInt)
		return err2
	}
	return nil
}

func (this *UserInfoDbApi) ChangeNationality(Userid uint32, Nationality string) error {

	pbirtInt := common.GetCountryCode(Nationality)

	err1 := this.UpdateMemSingleVal(Userid, "nationality", Nationality)

	this.UpdateSphinxSingleVal(Userid, "nationality", uint32(pbirtInt))

	return err1
}

func (this *UserInfoDbApi) ChangeVoiceUrl(Userid uint32, userData map[string]interface{}) error {

	userData["voiceduration"] = userData["duration"]

	err1 := this.UpdateMemUserInfo(Userid, userData)

	return err1
}

func (this *UserInfoDbApi) ChangeUsername(Userid uint32, nickname string) error {

	err1 := this.UpdateMemSingleVal(Userid, "username", nickname)

	if err1 != nil {
		this.infoLog.Println("ChangeUsername() failed,err=", err1, Userid, nickname)
	}

	return err1
}

func (this *UserInfoDbApi) ChangeUnregister(Userid uint32, nickname string) error {
	return nil
}

func (this *UserInfoDbApi) ChangeUserPrivacy(Userid uint32, nickname string) error {
	return nil
}

func (this *UserInfoDbApi) ChangeExactMatch(Userid uint32, nickname string) error {
	return nil
}

func (this *UserInfoDbApi) ChangeSameGender(Userid uint32, nickname string) error {
	return nil
}
func (this *UserInfoDbApi) ChangeAbsenceNearest(Userid uint32, nickname string) error {
	return nil
}

func (this *UserInfoDbApi) ChangeNickname(Userid uint32, nickname string) error {

	err1 := this.UpdateMemSingleVal(Userid, "nickname", nickname)

	if err1 != nil {
		this.infoLog.Println("ChangeNickname() failed,err=", err1, Userid, nickname)
	}

	return err1
}

func (this *UserInfoDbApi) ChangeSignature(Userid uint32, signature string) error {

	err1 := this.UpdateMemSingleVal(Userid, "signature", signature)

	if err1 != nil {
		this.infoLog.Println("ChangeSignature() failed,err=", err1, Userid)
	}

	return err1
}

func (this *UserInfoDbApi) UpdateMemSingleVal(Userid uint32, field string, val interface{}) error {

	return this.userPri.UpdateMemSingleVal(Userid, field, val)
}

func (this *UserInfoDbApi) UpdateMemUserInfo(Userid uint32, dict map[string]interface{}) error {

	return this.userPri.UpdateMemUserInfo(Userid, dict)
}

func (this *UserInfoDbApi) SaveLog(info *UserEvent) error {

	changeInt, ok := UserUpdateInfoCmdMapInt[info.Info.Type]

	if !ok {
		// return errors.New("bad change type " + info.Info.Type)
		return nil
	}

	_, ok = STOP_LOG_COMMON[info.Info.Type]

	if ok {
		return errors.New("no need to save " + info.Info.Type)
	}

	tm := time.Unix(int64(info.ServerTs), 0)
	ServerTime := tm.UTC().Format("2006-01-02 03:04:05")

	Content := ""
	switch v := info.Info.Data.(type) {

	case uint32:
		Content = fmt.Sprintf("%d", int32(v))
	case uint64:
		Content = fmt.Sprintf("%d", uint64(v))
	case float32:
		Content = fmt.Sprintf("%f", float32(v))
	case string:
		Content = string(v)
	case map[string]interface{}:
		Content1, _ := json.Marshal(v)
		Content = string(Content1)
	}

	// return nil
	r1, err1 := this.LogDb.Exec("INSERT INTO LOG_USER_UPDATEUSERINFO  (USERID,`CHANGETYPE`, `OSTYPE`,CONTENT, CREATETIME) VALUES(?,?,?,?,?)",
		info.Userid,
		changeInt,
		info.Ostype,
		Content,
		ServerTime,
	)

	if err1 != nil {
		this.infoLog.Println(info.Userid, info.Info.Type, "SaveLog() LOG_USER_UPDATEUSERINFO insert failed err=", err1, Content)
		return err1
	}

	id1, _ := r1.LastInsertId()

	this.infoLog.Println(info.Userid, info.Info.Type, "SaveLog() LOG_USER_UPDATEUSERINFO ok Id=", id1)

	return nil

}

func (this *UserInfoDbApi) UpdateSphinxSingleVal(Userid uint32, field string, val uint32) error {

	dict := map[string]uint32{}

	dict[field] = val

	return this.UpdateSpinx(Userid, dict)
}

func (this *UserInfoDbApi) UpdateSpinx(Userid uint32, dict map[string]uint32) error {

	setUpField := ""

	for k, v := range dict {
		setUpField = setUpField + fmt.Sprintf(", %s=%d ", k, v)
	}

	if len(setUpField) <= 2 {
		return errors.New("no udpate field")
	}

	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Panic info is: ", err)
		}
	}()

	setUpField = setUpField[1:]

	updateSql := fmt.Sprintf("UPDATE %s SET %s WHERE id = %d", this.queryIndex, setUpField, Userid)

	r1, err := this.SphinxClient.DB.Exec(updateSql)

	if err != nil {
		this.infoLog.Println("UpdateSpinx() failed,err=", err, Userid, dict)
		return err
	}

	this.infoLog.Println(Userid, "UpdateSpinx()   setUpField=", updateSql)

	return nil

	id1, err := r1.LastInsertId()

	this.infoLog.Println(Userid, "UpdateSpinx() ok afrow=", id1, dict)

	return err
}
