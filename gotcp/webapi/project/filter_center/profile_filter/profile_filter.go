package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/HT_GOGO/gotcp"
	"github.com/HT_GOGO/gotcp/libcomm"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	lbsorg "github.com/HT_GOGO/gotcp/tcpfw/project/lbs_headquarter/org"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/HT_GOGO/gotcp/webapi/common/sdk"
	"github.com/HT_GOGO/gotcp/webapi/project/filter_center/org"
	_ "github.com/go-sql-driver/mysql"
	sphinx "github.com/lilien1010/sphinx" //经过改造的sphinx go driver
	"github.com/nsqio/go-nsq"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	// "sync"
	"regexp"
	"syscall"
	"time"
)

const (
	PF_CHECK_SIG      = 1
	PF_CHECK_HEADURL  = 2
	PF_CHECK_NICKNAME = 3
	PF_CHECK_REG_IP   = 4

	DEAFULT_HEAD_HOST = "http://cn-head-cdn.nihaotalk.com/"
)

var (
	infoLog *log.Logger

	MasterDB *sql.DB
	LogDB    *sql.DB
	MgDB     *sql.DB
	options  common.Options

	forbidden_face string

	userInfoCacheApi *tcpcommon.UserInfoCacheApi
	momentAPI        *tcpcommon.MntApi
	SphinxClient     *sphinx.Client

	sigFilter      *common.TextFilter
	nicknameFilter *common.TextFilter
	regIPFilter    *common.TextFilter

	userPri *common.UserPrivilege

	userinfoDBApi *org.UserInfoDbApi

	imageChecker *sdk.ImageFilter

	CheaterFakeIpCountry string //骗子IP所在国家

	CheaterFakeNation string //骗子IP所在国籍 定义骗子ip国家不配的假国籍

	CheaterFakeAgeSplit uint32 //骗子IP所在国籍检测的年龄分界线

	GlobalRecvMsgPackChan   chan *nsq.Message
	workerCount             int
	regSleepToHandleCheater int
)

type Callback struct{}

func (this *Callback) OnConnect(c *gotcp.Conn) bool {
	addr := c.GetRawConn().RemoteAddr()
	c.PutExtraData(addr)
	infoLog.Println("OnConnect:", addr)
	return true
}

func (this *Callback) OnClose(c *gotcp.Conn) {
	infoLog.Println("OnClose:", c.GetExtraData())
}

func (this *Callback) OnMessage(c *gotcp.Conn, p gotcp.Packet) bool {

	packet, ok := p.(*tcpcommon.HeadV2Packet)
	if !ok {
		infoLog.Printf("OnMessage packet can not change to HeadV2packet")
		return false
	}

	head, err := packet.GetHead()
	if err != nil {
		infoLog.Printf("OnMessage Get head failed", err)
		return false
	}

	infoLog.Printf("OnMessage:[Uid=%v,Seq=%v,Cmd=%v,Len=%v]", head.Uid, head.Seq, head.Cmd, head.Len)

	_, err = packet.CheckPacketV2Valid()
	if err != nil {
		infoLog.Printf("OnMessage Invalid packet", err)
		return false
	}

	switch int(head.Cmd) {

	case common.CMD_UPDATE_NICKNAME_WORDS:
		err = nicknameFilter.RefreshRegex()
		common.SendResponseCode(c, head, err)
	case common.CMD_UPDATE_SIGNATURE_WORDS:
		err = sigFilter.RefreshRegex()
		common.SendResponseCode(c, head, err)

	case common.CMD_UPDATE_IP_REG_ADDRESS:
		err = regIPFilter.RefreshRegex()
		common.SendResponseCode(c, head, err)
	case common.CMD_UPDATE_MG_CONFIG:
		err = InitCheaterInfoFromMgDb(regIPFilter)
		common.SendResponseCode(c, head, err)
	default:
		infoLog.Printf("OnMessage UnHandle Cmd =%d", head.Cmd)

	}

	return true

}

func MessageHandle(message *nsq.Message) error {

	attr := "mmt/total_profile_event_count"
	libcomm.AttrAdd(attr, 1)

	GlobalRecvMsgPackChan <- message
	return nil
}

/*
1：修改语言
2：修改头像
3：修改userid
4：修改nickname
6：修改签名
*/
func PostDataToHandler(message *nsq.Message, idx int) {

	defer func() {
		if err := recover(); err != nil {
			infoLog.Println("NewRegHandler() ", err, idx)
		}
	}()

	info := &org.UserEvent{}
	err := json.Unmarshal(message.Body, info) // JSON to Struct

	if err != nil {
		infoLog.Printf("PostData err=%s", err.Error())
		return
	}

	infoLog.Printf("PostData enter info=%#v timestamp=%v body=%s ", info, message.Timestamp, message.Body)

	switch info.Info.Type {

	case "user_name":
		err = userinfoDBApi.ChangeUsername(info.Userid, info.Info.Data.(string))

	case "nick_name":
		err = CheckNickName(info.Userid, info.Info.Data.(string))
	case "head_url":
		err = CheckProfileImage(info.Userid, info.Info.Data.(string), info.ServerTs)
	case "signature":
		err = CheckSignature(info.Userid, info.Info.Data.(string))
	case "lang_modify":
		err = userinfoDBApi.ChangeLang(info.Userid, info.Info.Data.(map[string]interface{}))

	case "user_update_location":

		err = CheckUserLbsInfo(info.Userid, message)

	case "new_register":
		err = NewRegHandler(info.Userid, info.Info.Data.(map[string]interface{}))
	default:
		infoLog.Panicln("PostData() undefined handler ", info.Info.Type)

	}

	if err != nil {
		infoLog.Println("PostData() failed,err=", err, info.Userid, info.Info.Type)
	}

	err = userinfoDBApi.SaveLog(info)

	if err != nil {
		infoLog.Println("PostData() SaveLog failed,err=", err, info.Userid)
	}
}

func NewRegHandler(Userid uint32, Data map[string]interface{}) (err error) {

	attr := "mmt/total_new_register"
	libcomm.AttrAdd(attr, 1)

	url, ok := Data["headurl"]

	now := time.Now()

	//检验头像
	if ok {
		err = CheckProfileImage(Userid, url.(string), uint64(now.Unix()))
	}

	//检验昵称
	nickname, ok := Data["nickname"]

	if ok && nickname != nil {
		err = CheckNickName(Userid, nickname.(string))
	}

	ServerTs := common.GetMilliNow()

	//检验昵称
	unixnow, ok := Data["unixnow"]

	if ok && unixnow != nil {
		ServerTs = uint64(unixnow.(uint32) * 1000)
	}

	//ip昵称
	ip, ok := Data["ip"]

	if ok && ip != nil {
		ipStr := ip.(string)
		_, handlerActions, err := regIPFilter.IsTextIllegally(ipStr, "")

		if len(handlerActions) > 0 && err != nil {

			reason := "cheater_ip"

			errHide := userinfoDBApi.HideUser(Userid, reason)

			if errHide != nil {
				infoLog.Println("CheckNickName() HideUser failed,err=", errHide, Userid, reason)
			}

			//
			saveBannedUserid(Userid, reason, ipStr)

			//打标签
			saveHiddenUserToMg(Userid, reason, ServerTs)

			go func(Userid uint32, Actions []uint32, reason string) {

				infoLog.Println("NewRegHandler() sleep wait to handler=", Userid, Actions, reason, ipStr)
				time.Sleep(time.Minute * time.Duration(regSleepToHandleCheater))
				userPri.HanderUserByActionType("NewRegHandler", Userid, Actions, reason)
				infoLog.Println("NewRegHandler() sleep end=", Userid, Actions, reason)

			}(Userid, handlerActions, reason)

		} else {
			//检测国籍和地址微信信息
			IPcountryCode, ok1 := Data["IPcountryCode"]
			//IPcountryCode, ok1 := Data["ipcountrycode"]
			birthday, ok2 := Data["birthday"]
			nationality, ok3 := Data["nationality"]

			if ok1 && ok2 && ok3 && IPcountryCode != nil && birthday != nil && nationality != nil {
				strIpCountry := IPcountryCode.(string)
				strBirthday := birthday.(string)
				strNationality := nationality.(string)

				age, err := common.GetAgeByBirth(strBirthday)

				if err != nil {
					//骗子都是38以上的
					infoLog.Println("NewRegHandler() GetAgeByBirth err=", err)
				}

				if strIpCountry == "" {

					ipInBadCountry := strings.Contains(CheaterFakeIpCountry, strIpCountry)

					if ipInBadCountry {
						CheckCheater(Userid, strIpCountry, strNationality, age, ServerTs)
					}
				}
			} else {
				infoLog.Println("NewRegHandler()  data err when get info")
			}
		}

	}

	//

	return
}

func CheckCheater(Userid uint32, ipCountry string, nationality string, Age uint32, ServerTs uint64) (uint32, error) {

	ipInBadCountry := strings.Contains(CheaterFakeIpCountry, ipCountry)
	nationInIpBad := strings.Contains(CheaterFakeIpCountry, nationality)
	nationChooseNiceCountry := strings.Contains(CheaterFakeNation, nationality)

	if false == ipInBadCountry {
		return 1, nil
	}

	handlerActions := []uint32{common.WORD_BAN_MOMENT, common.WORD_BAN_COMMENT, common.WORD_BAN_MESSAGE}

	//年龄小于38 但是，国籍选的欧美等等
	if Age < CheaterFakeAgeSplit && nationChooseNiceCountry {
		reason := "bad ip location"
		//
		saveBannedUserid(Userid, reason, "IP IN "+ipCountry)

		//打标签
		saveHiddenUserToMg(Userid, reason, ServerTs)

		go func(Userid uint32, Actions []uint32, reason string) {

			infoLog.Println("CheckCheater() sleep wait to handler=", Userid, Actions, reason, ipCountry)
			time.Sleep(time.Minute * time.Duration(regSleepToHandleCheater))
			userPri.HanderUserByActionType("NewRegHandler", Userid, Actions, reason)
			infoLog.Println("CheckCheater() sleep end=", Userid, Actions, reason)

		}(Userid, handlerActions, reason)

	}

	//age 大于 38 ，国籍和IP国籍不匹配
	if Age >= CheaterFakeAgeSplit && !nationInIpBad {

		reason := fmt.Sprintf("age=%d,IP Bad", Age)

		saveBannedUserid(Userid, reason, "IP IN "+ipCountry)

		//打标签
		saveHiddenUserToMg(Userid, reason, ServerTs)

		go func(Userid uint32, Actions []uint32, reason string) {

			infoLog.Println("CheckCheater() sleep wait to handler=", Userid, Actions, reason, ipInBadCountry)
			time.Sleep(time.Minute * time.Duration(regSleepToHandleCheater))
			userPri.HanderUserByActionType("NewRegHandler", Userid, Actions, reason)
			infoLog.Println("CheckCheater() sleep end=", Userid, Actions, reason)

		}(Userid, handlerActions, reason)

	}

	return 0, nil

}

func CheckUserLbsInfo(Userid uint32, message *nsq.Message) (err error) {

	lbsinfo := &lbsorg.UserEvent{}
	err = json.Unmarshal(message.Body, lbsinfo) // JSON to Struct
	if err != nil {
		infoLog.Printf("CheckUserLbsInfo err=%s", err.Error())
		return
	}

	loca := lbsinfo.Info.Data

	err = userinfoDBApi.ChangeLocation(Userid, loca.Latitude, loca.Longtitude, loca.PlaceInfo.Country, loca.PlaceInfo.City, loca.PlaceId, loca.CityPlaceId)

	return
}

func CheckNickName(Userid uint32, Text string) error {

	checkText := nicknameFilter.DropSympos(Text)

	badText, HandleAction, err := nicknameFilter.IsTextIllegally(checkText, fmt.Sprintf("%d", Userid))

	userinfoDBApi.ChangeNickname(Userid, Text)

	if err != nil {
		infoLog.Println(Userid, "CheckNickName() IsTextIllegally:", err, ",text:", Text)
	} else {
		reason := "bad nick " + badText

		errHide := userinfoDBApi.HideUser(Userid, reason)

		if errHide != nil {
			infoLog.Println("CheckNickName() HideUser failed,err=", errHide, Userid, reason)
		}

		saveBadProfileInfo(Userid, PF_CHECK_NICKNAME, Text, badText)

		saveBannedUseridByAction(Userid, "bad nick", badText, HandleAction)

		userPri.HanderUserByActionType("", Userid, HandleAction, reason)
	}

	return nil
}

func CheckSignature(Userid uint32, Text string) error {

	checkText := sigFilter.DropSympos(Text)

	badText, HandleAction, err := sigFilter.IsTextIllegally(checkText, fmt.Sprintf("%d", Userid))

	userinfoDBApi.ChangeSignature(Userid, Text)

	if err != nil {

		infoLog.Println(Userid, "CheckSignature() IsTextIllegally:", err, ",text:", Text)

	} else {

		saveBadProfileInfo(Userid, PF_CHECK_SIG, Text, badText)

		infoLog.Println(Userid, "CheckSignature() IsTextIllegally:", badText)

		saveBannedUseridByAction(Userid, "bad sig", badText, HandleAction)

		userPri.HanderUserByActionType("", Userid, HandleAction, "bad sig "+badText)

		errHide := userinfoDBApi.HideUser(Userid, badText)

		if errHide != nil {
			infoLog.Println("CheckSignature() HideUser failed,err=", errHide, Userid, Text)
		}

	}

	return nil
}

func CheckProfileImage(Userid uint32, Url string, ServerTs uint64) error {

	checkTypes := []string{"porn", "sface"}

	checkArray := []*sdk.ImageBodyReqbody{}

	item := &sdk.ImageBodyReqbody{
		BigUrl: DEAFULT_HEAD_HOST + Url,
		Width:  150,
		Height: 150,
	}

	userinfoDBApi.ChangeHead(Userid, Url)

	checkArray = append(checkArray, item)
	Mid := fmt.Sprintf("%d", Userid)

	imageResult, err := imageChecker.DoCheck(Mid, checkArray, checkTypes)

	if err != nil {
		infoLog.Println(Userid, "CheckProfileImage() image check err:", err)
		return err
	}

	if len(imageResult.PornInfo) > 0 {

		infoLog.Println(Userid, "CheckProfileImage() find porn ", imageResult.PornInfo[0])

		if imageResult.PornInfo[0].IsPorn == 1 {

			jsonStr, _ := json.Marshal(imageResult.PornInfo[0])
			saveBannedUserid(Userid, "porn", string(jsonStr))
			saveHiddenUserToMg(Userid, "porn head", ServerTs)

			return handleBadProfileImage(Userid, Url, "porn")
		}

		if imageResult.PornInfo[0].Rate > 97 {

			jsonStr, _ := json.Marshal(imageResult.PornInfo[0])
			saveBannedUserid(Userid, "sexy", string(jsonStr))
			saveHiddenUserToMg(Userid, "sexy head", ServerTs)

			return handleBadProfileImage(Userid, Url, "sexy")

		}

	}

	if imageResult.SelfieInfo.IsSelfie == 1 {

		infoLog.Println(Userid, "CheckProfileImage() find face ", imageResult.SelfieInfo)
		if imageResult.SelfieInfo.PeopleName != "" && strings.Contains(forbidden_face, imageResult.SelfieInfo.PeopleName) {
			reason := "face=" + imageResult.SelfieInfo.PeopleName
			saveHiddenUserToMg(Userid, reason, ServerTs)
			return handleBadProfileImage(Userid, Url, reason)
		}

	}

	return nil

}

func saveBannedUseridByAction(Userid uint32, Reason string, Content string, BanAction []uint32) error {

	needSave := 0
	for _, v := range BanAction {

		if v == common.WORD_BAN_MOMENT || v == common.WORD_BAN_COMMENT || v == common.WORD_BAN_MESSAGE || v == common.WORD_BAN_REGISTER || v == common.WORD_BAN_USER_LOGIN || v == common.WORD_BAN_DEVICE_LOGIN {
			needSave = 1
		}
	}

	if needSave == 1 {
		return saveBannedUserid(Userid, Reason, Content)
	}

	return errors.New("no need to save")

}

func saveBannedUserid(Userid uint32, Reason string, Content string) error {

	// return nil
	r1, err1 := LogDB.Exec("INSERT INTO LOG_AUTO_USER_BANNED (MID,USERID,  `REASON`,`CONTENT`,CREATETIME)"+
		" VALUES(?,?,?,?,UTC_TIMESTAMP())",
		"",
		Userid,
		Reason,
		Content,
	)

	if err1 != nil {
		infoLog.Println(Userid, "saveBannedUserid() LOG_AUTO_USER_BANNED insert failed err=", err1)
		return err1
	}

	id1, _ := r1.LastInsertId()

	infoLog.Printf("saveBannedUserid() LOG_BAD_COMMENT ok ,logid=%d,userid=%d", id1, Userid)

	return nil

}

func handleBadProfileImage(Userid uint32, Url, reason string) error {

	errHide := userinfoDBApi.HideUser(Userid, reason)

	if errHide != nil {
		infoLog.Println("handleBadProfileImage() HideUser failed,err=", errHide, Userid, reason)
	}

	ActionTypes := []uint32{common.WORD_BAN_MOMENT, common.WORD_BAN_COMMENT}

	if reason == "porn" {
		ActionTypes = append(ActionTypes, common.WORD_BAN_MESSAGE)
		ActionTypes = append(ActionTypes, common.WORD_BAN_BUY)
		ActionTypes = append(ActionTypes, common.WORD_BAN_REGISTER)
	}

	userPri.HanderUserByActionType("", Userid, ActionTypes, reason)

	saveBadProfileInfo(Userid, PF_CHECK_HEADURL, Url, reason)

	return nil
}

func saveBadProfileInfo(Userid, Type uint32, srcContent string, badInfo string) error {

	// return nil
	r1, err := LogDB.Exec("INSERT INTO LOG_BAD_PROFILE (USERID,`TYPE`,  SRC_CONTENT,BAD_WORDS,CREATETIME) VALUES(?,?,?,?,UTC_TIMESTAMP())",
		Userid,
		Type,
		srcContent,
		badInfo,
	)

	if err != nil {
		infoLog.Println(Userid, Type, "saveBadProfileInfo() LOG_BAD_PROFILE insert failed err=", err)
		return err
	}

	id1, err := r1.LastInsertId()
	if err != nil {
		return err
	}

	infoLog.Println(Userid, Type, "saveBadProfileInfo() LOG_BAD_PROFILE ok Id=", id1)

	return nil
}

func saveHiddenUserToMg(Userid uint32, Reason string, ServerTs uint64) error {
	var old_reason string
	err := MasterDB.QueryRow("select REASON from MG_USER_STATE where USERID = ? limit 1", Userid).Scan(&old_reason)
	if err != nil {
		infoLog.Println("saveHiddenUserToMg() query row failed", err)
		return err
	}
	re, _ := regexp.Compile("[0-9]+/[0-9]+")

	old_reason = re.ReplaceAllString(old_reason, "")
	pReason := fmt.Sprintf("%s %s GOGO", common.QtDateWithFormat(uint32(ServerTs), "01/02"), Reason)
	// return nil
	r1, err := MasterDB.Exec("INSERT INTO MG_USER_STATE (USERID,`STATE`,REASON,OPERATOR,UPDATETIME) VALUES(?,?,?,?,?) ON DUPLICATE KEY UPDATE UPDATETIME=VALUES(UPDATETIME), REASON=VALUES(REASON),OPERATOR=VALUES(OPERATOR)",
		Userid,
		"HIDE",
		pReason+"_"+old_reason,
		"go-check",
		common.QtDate(uint32(ServerTs)),
	)

	if err != nil {
		infoLog.Println(Userid, Reason, "saveHiddenUserToMg() MG_USER_STATE insert failed err=", err)
		return err
	}

	id1, err := r1.LastInsertId()
	if err != nil {
		return err
	}

	infoLog.Println(Userid, Reason, "saveHiddenUserToMg() MG_USER_STATE ok Id=", id1)

	return nil
}

func InitCheaterInfoFromMgDb(regIPFilter *common.TextFilter) (err error) {

	CheaterFakeIpCountry, err = regIPFilter.LoadConfigString("cheater_ip_country", "customer_config")
	if err != nil {
		infoLog.Println("InitCheaterInfoFromMgDb  cheater_ip_country err ", err)
	}
	CheaterFakeNation, err = regIPFilter.LoadConfigString("cheater_fake_nation", "customer_config")
	if err != nil {
		infoLog.Println("InitCheaterInfoFromMgDb cheater_fake_nation  err ", err)
	}
	CheaterFakeAgeSplit, err = regIPFilter.LoadConfigUint32("cheater_fake_age_split", "customer_config")
	if err != nil {
		infoLog.Println("InitCheaterInfoFromMgDb cheater_fake_age_split err ", err)
	}
	infoLog.Printf("InitCheaterInfoFromMgDb() CheaterFakeIpCountry=%s,CheaterFakeNation=%s,CheaterFakeAgeSplit=%d", CheaterFakeIpCountry, CheaterFakeNation, CheaterFakeAgeSplit)

	return
}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := common.InitLogAndOption(&options, &infoLog)

	MasterDB, err = common.InitMySqlFromSection(cfg, infoLog, "MASTER_MYSQL", 3, 1)
	common.CheckError(err)
	defer MasterDB.Close()

	LogDB, err = common.InitMySqlFromSection(cfg, infoLog, "LOGDB_MYSQL", 3, 2)
	common.CheckError(err)
	defer LogDB.Close()

	MgDB, err = common.InitMySqlFromSection(cfg, infoLog, "MG_MYSQL", 3, 2)
	common.CheckError(err)
	defer MgDB.Close()

	// ali access key info
	access_key_id := cfg.Section("SERVICE_CONFIG").Key("access_key_id").MustString("access_key_id")
	access_key_secret := cfg.Section("SERVICE_CONFIG").Key("access_key_secret").MustString("access_key_secret")

	imageChecker = sdk.NewImageFilter(access_key_id, access_key_secret, infoLog)

	sigFilter = common.NewTextFiler(MgDB, common.CHECK_SIGNATURE, infoLog)
	nicknameFilter = common.NewTextFiler(MgDB, common.CHECK_NICKNAME, infoLog)
	regIPFilter = common.NewTextFiler(MgDB, common.CHECK_IP_REG_ADDRESS, infoLog)

	ret := nicknameFilter.IsNumber("2497101919")
	InitCheaterInfoFromMgDb(regIPFilter)

	infoLog.Println("========== IsNumber ================ ret ", ret)

	cahceIP := cfg.Section("USER_INFO_CACHE").Key("ip").MustString("127.0.0.1")
	cahcePort := cfg.Section("USER_INFO_CACHE").Key("port").MustString("12600")
	infoLog.Printf("user info cache tcpcommon,ip=%v port=%v", cahceIP, cahcePort)
	userInfoCacheApi = tcpcommon.NewUserInfoCacheApi(cahceIP, cahcePort, 1*time.Second, 1*time.Second, &tcpcommon.HeadV2Protocol{}, 1000)

	infoLog.Println("========== NewUserInfoCacheApi ================ ", cahceIP, cahcePort)

	// init moment api
	moment_ip := cfg.Section("SERVICE_CONFIG").Key("moment_ip").MustString("127.0.0.1")
	moment_port := cfg.Section("SERVICE_CONFIG").Key("moment_port").MustString("15500")
	moment_timeout := cfg.Section("SERVICE_CONFIG").Key("moment_timeout").MustUint(1)
	moment_max_conn := cfg.Section("SERVICE_CONFIG").Key("moment_max_conn").MustInt(3)
	moment_url := cfg.Section("SERVICE_CONFIG").Key("moment_url").MustString("http://qtest.hellotalk.org")
	notify_api_host := cfg.Section("SERVICE_CONFIG").Key("notify_api_host").MustString("http://qtest.hellotalk.org")

	v2proto := &tcpcommon.HeadV2Protocol{}
	momentAPI = tcpcommon.NewMntApi(moment_ip, moment_port, time.Duration(moment_timeout)*time.Second, time.Duration(moment_timeout)*time.Second, v2proto, moment_max_conn)

	infoLog.Println("========== NewMntApi ================ ", moment_ip, moment_port)

	redis_host := cfg.Section("REDIS").Key("ruler_redis_ip").MustString("127.0.0.1")
	redis_port := cfg.Section("REDIS").Key("ruler_redis_port").MustString("6379")

	RulerRedis := tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	redis_host = cfg.Section("REDIS").Key("userinfo_redis_ip").MustString("127.0.0.1")
	redis_port = cfg.Section("REDIS").Key("userinfo_redis_port").MustString("6379")

	infoRedis := tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	userPri = common.NewUserPrivilege(infoLog, MasterDB, RulerRedis, infoRedis, momentAPI, strings.Split(moment_url, ","), notify_api_host)

	infoLog.Println("========== initial userPrivilege ================ ", redis_host, redis_port)

	sphinx_host := cfg.Section("SPHINX").Key("sphinx_host").MustString("127.0.0.1")
	sphinx_port := cfg.Section("SPHINX").Key("sphinx_port").MustInt(9312)
	sphinx_index := cfg.Section("SPHINX").Key("sphinx_index").MustString("dis_user")
	sphinx_timeout := cfg.Section("SPHINX").Key("sphinx_timeout").MustInt(5000)
	sphinx_open_count := cfg.Section("SPHINX").Key("sphinx_open_count").MustInt(100)
	sphinx_idle_count := cfg.Section("SPHINX").Key("sphinx_idle_count").MustInt(10)

	userinfoDBApi = org.NewUserInfoDbApi(sphinx_host, sphinx_port, sphinx_index, sphinx_timeout, sphinx_open_count, sphinx_idle_count,
		LogDB, userPri, infoLog)

	infoLog.Println("========== NewUserInfoDbApi sphinx ================ ", sphinx_host, sphinx_port, sphinx_index)

	forbidden_face = cfg.Section("SERVICE_CONFIG").Key("forbidden_face").MustString("")

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	infoLog.Println("========== init consumer ================ ", lookupdHost)

	workerCount = cfg.Section("SERVICE_CONFIG").Key("worker_count").MustInt(5)
	regSleepToHandleCheater = cfg.Section("SERVICE_CONFIG").Key("cheater_sleep_handler").MustInt(5)

	GlobalRecvMsgPackChan = make(chan *nsq.Message)

	for i := 0; i < workerCount; i++ {

		go func(idx int) {

			for {
				info := <-GlobalRecvMsgPackChan
				PostDataToHandler(info, idx)
			}

		}(i)
	}

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	tcpConfig := &gotcp.Config{
		PacketSendChanLimit:    uint32(10),
		PacketReceiveChanLimit: uint32(10),
	}

	// creates a tcp listener
	serverIp := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("127.0.0.1")
	serverPort := cfg.Section("LOCAL_SERVER").Key("bind_port").MustInt(8990)
	infoLog.Printf("serverIp=%v serverPort=%v", serverIp, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", serverIp+":"+strconv.Itoa(serverPort))
	common.CheckError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	common.CheckError(err)

	srv := gotcp.NewServer(tcpConfig, &Callback{}, &tcpcommon.HeadV2Protocol{})

	// // starts service
	go srv.Start(listener, time.Second)
	// infoLog.Println("listening:", listener.Addr())

	// // catchs system signal
	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	infoLog.Println("Signal: ", <-chSig)

	// stops service
	srv.Stop()

}
