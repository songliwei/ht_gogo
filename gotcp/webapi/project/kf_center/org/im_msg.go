package org

import (
	"encoding/json"
	"errors"
	"net"
	"strings"

	"github.com/HT_GOGO/gotcp/libcrypto"
)

// 消息类型
const (
	MT_TEXT       = 0  // 文字
	MT_TRANSLATE  = 1  // 带翻译的文字
	MT_IMAGE      = 2  // 图片
	MT_VOICE      = 3  // 音频
	MT_LOCATE     = 4  // 定位
	MT_PROFILE    = 5  // 用户介绍
	MT_VOICETEXT  = 6  // 语音转文字
	MT_CORRECTION = 7  // 修改句子
	MT_STICKERS   = 8  // 自定义表情
	MT_DOODLE     = 9  // 涂鸦
	MT_VOIP       = 10 // VOIP？
	MT_NOTIFY     = 11 // 公众消息？忽略不处理
	MT_VIDEO      = 12 // 视频 video
	MT_GVOIP      = 13 // group voip
	MT_LINK       = 14
	MT_CARD       = 15
	MT_UNKNOWN    = 100
)

// 推送类型  需要将消息类型转到推送的类型上
const (
	PUSH_TEXT                    = 0
	PUSH_VOICE                   = 1
	PUSH_IMAGE                   = 2
	PUSH_INTRODUCE               = 3
	PUSH_LOCATION                = 4
	PUSH_FRIEND_INVITE           = 5
	PUSH_LANGUAGE_EXCHANGE       = 6
	PUSH_LANGUAGE_EXCHANGE_REPLY = 7
	PUSH_CORRECT_SENTENCE        = 8
	PUSH_STICKERS                = 9
	PUSH_DOODLE                  = 10
	PUSH_GIFT                    = 11
	PUSH_VOIP                    = 12
	PUSH_ACCEPT_INVITE           = 13
	PUSH_VIDEO                   = 14
	PUSH_GVOIP                   = 15 // group voip
	PUSH_LINK                    = 16
	PUSH_CARD                    = 17
	PUSH_FOLLOW                  = 18
	PUSH_REPLY_YOUR_COMMENT      = 19
	PUSH_COMMENTED_YOUR_POST     = 20
	PUSH_CORRECTED_YOUR_POST     = 21
	PUSH_MOMENT_LIKE             = 22
	PUSH_POST_MOMENT             = 23
	PUSH_WHITE_BOARD             = 24
	PUSH_NEW_MSG_NOTIFY          = 25
)

const (
	E_NONE_KEY    = 0x00
	E_SESSION_KEY = 0x01
	E_RAND_KEY    = 0x02
	E_SERV_KEY    = 0x03

	HT_CUSTOMER_ACCOUNT = 10000
	HT_CUSTOMER_IOS     = 42900
	HT_CUSTOMER_ANDROID = 10002

	HT_HELLOTALK_TEAM2 = 10010

	HT_NOTEBOOK_ACCOUNT = 104

	/*
	 * 登录请求
	 */
	CMD_CLIENT_LOGIN = 0x1001

	CMD_CLIENT_LOGIN_ACK = 0x1002

	/*
	 * 登录验证
	 */
	CMD_LOGIN_AUTH = 0x1003

	CMD_LOGIN_AUTH_ACK = 0x1004

	/*
	 * 用户重连
	 */
	CMD_CLIENT_RECONNECT = 0x1005

	CMD_CLIENT_RECONNECT_ACK = 0x1006

	/*
	 * 用户退出
	 */
	CMD_CLIENT_LOGOUT = 0x1007

	CMD_CLIENT_LOGOUT_ACK = 0x1008

	/*
	 * 客户端状态报告
	 */
	CMD_STATE_REPORT = 0x1009

	CMD_STATE_REPORT_ACK = 0x100A

	/*
	 * 获取客户端最新版本信息
	 */
	CMD_GET_LATESTVERSION     = 0x1011
	CMD_GET_LATESTVERSION_ACK = 0x1012

	/*
	 * 用户自己删除帐号
	 */
	CMD_ACCOUNT_UNREGISTER     = 0x1013
	CMD_ACCOUNT_UNREGISTER_ACK = 0x1014

	/*
	 * P2P消息
	 */
	CMD_P2P_MESSAGE     = 0x4001
	CMD_RECEIPT_MESSAGE = 0x4002
	MAX_MESSAGE_LEN     = 16 * 1024

	/*
	 * P2P系统通知(5条没回复不能收到更多的对方消息)
	 */
	CMD_NOTIFY_P2P_SYSTERM  = 0x400B
	CMD_RECEIPT_P2P_SYSTERM = 0x400C

	NOTIFY_CODE_NOMOREMSG_UNTILREPLY = 0x01

	/*
	     自己发送给自己的消息

	   跟0x4001 一样的消息体只是做了如下改动
	   1.cmd变成了0x400D
	   2.from to 颠倒了过来
	*/
	CMD_SELF_TO_SELF_MESSAGE = 0x400D

	CMD_SELF_TO_SELF_MESSAGE_RECEIPT = 0x400E

	/*
	 * (已废除)获取离线消息
	 */
	CMD_GET_OFFLINE_MESSAGE     = 0x4011
	CMD_GET_OFFLINE_MESSAGE_ACK = 0x4012

	/*
	 * 离线消息获取成功确认
	 */
	CMD_CONFIRM_OFFLINE_MESSAGE     = 0x4013
	CMD_CONFIRM_OFFLINE_MESSAGE_ACK = 0x4014

	/*
	 * 消息已读通知
	 */
	CMD_P2P_MESSAGESEEN_NOTIFY     = 0x4015
	CMD_RECEIPT_MESSAGESEEN_NOTIFY = 0x4016

	/*
	 * 对方正在输入通知
	 */
	CMD_P2P_INPUTTING_NOTIFY = 0x4017

	/*
	 * 公众号消息
	 */
	CMD_P2P_PUBLIC_MESSAGE         = 0x4031
	CMD_P2P_PUBLIC_MESSAGE_RECEIPT = 0x4032

	CMD_NOTIFY_SYNC_CHATLIST = 0x6080

	/*
	 * 新的获取离线消息协议
	 */
	CMD_NEW_GET_OFFLINEMESSAGE     = 0x4041
	CMD_NEW_GET_OFFLINEMESSAGE_ACK = 0x4042

	CMD_GET_OFFLINEMESSAGE_V2     = 0x4045
	CMD_GET_OFFLINEMESSAGE_ACK_V2 = 0x4046

	/*
	 * 群聊消息
	 */
	CMD_MUC_MESSAGE         = 0x7009
	CMD_MUC_MESSAGE_RECEIPT = 0x700A

	/*
	 * 邀请群成员通知
	 */
	CMD_NOTIFY_INVITE_MEMBER  = 0x700B
	CMD_RECEIPT_INVITE_MEMBER = 0x700C

	/*
	 * 移出群成员通知
	 */
	CMD_NOTIFY_REMOVE_MEMBER  = 0x700D
	CMD_RECEIPT_REMOVE_MEMBER = 0x700E

	/*
	 * 群成员自己退出通知
	 */
	CMD_NOTIFY_MEMBER_QUIT  = 0x700F
	CMD_RECEIPT_MEMBER_QUIT = 0x7010

	/*
	 * 修改群名称
	 */
	CMD_MUC_MODIFY_ROOMNAME     = 0x7011
	CMD_MUC_MODIFY_ROOMNAME_ACK = 0x7012

	/*
	 * 群名称改变通知
	 */
	CMD_NOTIFY_ROOMNAME_CHANGED  = 0x7013
	CMD_RECEIPT_ROOMNAME_CHANGED = 0x7014

	/*
	 * 修改自己的群昵称
	 */
	CMD_MUC_MODIFY_NICKNAME     = 0x7015
	CMD_MUC_MODIFY_NICKNAME_ACK = 0x7016

	/*
	 * 群成员昵称改变通知
	 */
	CMD_NOTIFY_MEMBERNAME_CHANGED  = 0x7017
	CMD_RECEIPT_MEMBERNAME_CHANGED = 0x7018

	/*
	 * 群聊推送设置
	 */
	CMD_MUC_MODIFY_PUSHSETTING     = 0x7019
	CMD_MUC_MODIFY_PUSHSETTING_ACK = 0x701A

	/*
	 * 更新群聊详细资料
	 */
	CMD_MUC_UPDATE_ROOMINFO     = 0x7021
	CMD_MUC_UPDATE_ROOMINFO_ACK = 0x7022

	/*
	 * 添加群聊到通讯录
	 */
	CMD_MUC_ADDTO_CONTACTLIST     = 0x7031
	CMD_MUC_ADDTO_CONTACTLIST_ACK = 0x7032

	/*
	 * 获取通讯录群聊列表
	 */
	CMD_MUC_GET_CONTACTLIST     = 0x7033
	CMD_MUC_GET_CONTACTLIST_ACK = 0x7034

	/**
	 *  try to create a group voip
	 */
	CMD_GVOIP_CREATE_REQUEST  = 0x7101
	CMD_GVOIP_CREATE_RESPONSE = 0x7102

	/**
	 *  new group voip invite broadcast
	 */
	CMD_GVOIP_INVITE_BROADCAST         = 0x7103
	CMD_GVOIP_INVITE_BROADCAST_RECEIPT = 0x7104

	/**
	 *  join group voip request
	 */
	CMD_GVOIP_JOIN_REQUEST  = 0x7105
	CMD_GVOIP_JOIN_RESPONSE = 0x7106

	/**
	 *  success login voip server
	 */
	CMD_GVOIP_LOGIN_REPORT         = 0x7107
	CMD_GVOIP_LOGIN_REPORT_RECEIPT = 0x7108

	/**
	 *  new member join group voip broadcast
	 */
	CMD_GVOIP_MEMBER_JOIN_BROADCAST         = 0x7109
	CMD_GVOIP_MEMBER_JOIN_BROADCAST_RECEIPT = 0x710A

	/**
	 *  member leave group voip report
	 */
	CMD_GVOIP_LOGOUT_REPORT         = 0x710B
	CMD_GVOIP_LOGOUT_REPORT_RECEIPT = 0x710C

	/**
	 *  member leave group voip broadcast
	 */
	CMD_GVOIP_MEMBER_LEAVE_BROADCAST         = 0x710D
	CMD_GVOIP_MEMBER_LEAVE_BROADCAST_RECEIPT = 0x710E

	/**
	 *  member leave group voip broadcast
	 */
	CMD_GVOIP_GET_MEMBER_LIST          = 0x710F
	CMD_GVOIP_GET_MEMBER_LIST_RESPONSE = 0x7110

	/**
	 *  group voip end broadcast
	 */
	CMD_GVOIP_END_BROADCAST         = 0x7111
	CMD_GVOIP_END_BROADCAST_RECEIPT = 0x7112

	/**
	 *  group voip ping
	 */
	CMD_GVOIP_PING     = 0x7113
	CMD_GVOIP_PING_ACK = 0x7114

	/**
	 *  unwatch gvoip member chage event
	 */
	CMD_GVOIP_UNWATCH_MEMBER_CHANGE     = 0x7115
	CMD_GVOIP_UNWATCH_MEMBER_CHANGE_RES = 0x7116

	CMD_GVOIP_ADD_WATCHER         = 0x7117
	CMD_GVOIP_ADD_WATCHER_RECEIPT = 0x7118

	CMD_GVOIP_DEL_WATCHER         = 0x7119
	CMD_GVOIP_DEL_WATCHER_RECEIPT = 0x711A

	/**
	 *   block voip
	 */
	CMD_VOIP_BLOCK_SETTING     = 0x7151
	CMD_VOIP_BLOCK_SETTING_RES = 0x7152

	/**
	 *  get blocked voip list by me
	 */
	CMD_GET_BLOCKED_VOIP_LIST     = 0x7153
	CMD_GET_BLOCKED_VOIP_LIST_RES = 0x7154

	IMPROTOCOL_VERSION_2 = 2
	IMPROTOCOL_VERSION_3 = 3
	IMPROTOCOL_VERSION_4 = 4

	// 强制用户下线
	// 信令流向：IMServer => IMServer
	CMD_S2S_KICKOFFLINE_USER     = 0x8007
	CMD_S2S_KICKOFFLINE_USER_ACK = 0x8008

	/*
	 * 心跳包
	 * 心跳包没有应答 只做SOCKET链接的保活作用
	 * 信令流向：Client => Server
	 */
	CMD_HEART_BEAT     = 0x9001
	CMD_HEART_BEAT_ACK = 0x9002

	/*
	 * 类PING包
	 * 信令流向：Client => Server
	 */
	CMD_NETWORK_PING = 0x9003

	/*
	 * 类PING包应答
	 * 信令流向：Server => Server 或 Server => Client
	 */
	CMD_NETWORK_PING_ACK = 0x9004

	/*
	 * 用户SESSION失效 需要重新连接
	 * 信令流向：IMServer => Client
	 */
	CMD_INVALID_SESSION = 0x9006

	/*
	 * 用户在别处登录
	 * 信令流向：IMServer => Client
	 */
	CMD_USER_RELOGIN = 0x9008

	/*
	 * force user out
	 * a.self management
	 * b.third part interface acros WebServer
	 *
	 */
	CMD_FORCE_USER_OUT = 0x900A
)

var (
	ErrReadPacketLengthError = errors.New("read packet legnth error")
)

type IMProtocol struct {
	//ReadPacket(conn *net.TCPConn) (Packet, error)
	SessionKey string
	client     *IMClient
}

/**/
func (p *IMProtocol) ReadPacket(conn *net.TCPConn) (packet Packet, err error) {

	/*return nil,errors.New("text")*/

	buff := make([]byte, XTHeadLen)
	min := uint32(XTHeadLen)
	var n uint32 = 0
	for n < min && err == nil {
		var nn int
		nn, err = conn.Read(buff[n:])
		n += uint32(nn)
	}

	//p.client.infoLog.Println("head ReadPacket() ", n, min, buff, err)

	if n != min {
		if err == nil {
			err = ErrReadPacketLengthError
		}
		return
	}

	head, err := NewXTHead(buff)
	if err != nil {
		return nil, err
	}

	err = head.CheckXTHeaderValid()
	if err != nil {
		return nil, err
	}

	Body := make([]byte, head.Len)
	n = 0
	for n < head.Len && err == nil {
		var nn int
		nn, err = conn.Read(Body[n:])
		n += uint32(nn)
	}

	//p.client.infoLog.Println("body ReadPacket() ", n, min, buff, head, err)

	if n != head.Len {
		if err == nil {
			err = ErrReadPacketLengthError
		}
		return
	}

	var pack *XTHeadPacket

	if head.CryKey == E_SESSION_KEY {
		rest := libcrypto.TEADecrypt(string(Body), p.client.SessionKey)
		pack = NewXTHeadPacket(head, []byte(rest))
	}

	p.client.ReqSeq = head.Seq

	if head.CryKey == E_RAND_KEY {
		//fmt.Println("Body:", Body)
		//fmt.Println("rest key:", Body[0:16], ",data=", Body[16:])
		rest := libcrypto.TEADecrypt(string(Body[16:]), string(Body[0:16]))
		//fmt.Println("rest rest:", []byte(rest))

		pack = NewXTHeadPacket(head, []byte(rest))
	}

	return pack, nil

}

//基本消息类型结构体
type TextMsg struct {
	Text string `json:"text" binding:"required"`
}

type ImageMsg struct {
	Type     string `json:"type" binding:"required"`
	ThumbUrl string `json:"thumb_url" binding:"required"`
	Url      string `json:"url" binding:"required"`
	Width    uint32 `json:"width" binding:"required"`
	Height   uint32 `json:"height" binding:"required"`
	Size     uint32 `json:"size" binding:"omitempty"`
}

type VoiceMsg struct {
	Url      string `json:"url" binding:"required"`
	Size     uint32 `json:"size" binding:"required"`
	Duration uint32 `json:"duration" binding:"required"`
	Type     string `json:"type" binding:"omitempty"`
	//Name     string `json:"name" binding:"omitempty"`
}

type StickerMsg struct {
	ProductId uint32 `json:"product_id" binding:"required"`
	ImageID   uint32 `json:"image_id" binding:"required"`
}

//{"dst_lang":"zh-TW","dst_text":"一次我都","src_lang":"zh-CN","src_text":"一次我都"}
type TranslateMsg struct {
	SrcText string `json:"src_text" binding:"required"`
	SrcLang string `json:"src_lang" binding:"omitempty"`
	DstLang string `json:"dst_lang" binding:"omitempty"`
	DstText string `json:"dst_text" binding:"omitempty"`
}

type VideoMsg struct {
	Url      string `json:"url" binding:"required"`
	Size     uint32 `json:"size" binding:"required"`
	Duration uint32 `json:"duration" binding:"required"`
	Type     string `json:"type" binding:"omitempty"`
	//Name     string `json:"name" binding:"omitempty"`
}

type IntroductionProfile struct {
	UserId         uint32 `json:"user_id" binding:"required"`
	HeadUrl        string `json:"head_url" binding:"required"`
	NickName       string `json:"nick_name" binding:"omitempty"`
	Country        string `json:"country" binding:"omitempty"`
	Birthday       string `json:"birthday" binding:"omitempty"`
	Nativelanguage string `json:"native_language" binding:"omitempty"`
	Learnlanguage  string `json:"learn_language" binding:"omitempty"`
	Learnlanguage2 string `json:"learn_language2" binding:"omitempty"`
	Learnlanguage3 string `json:"learn_language3" binding:"omitempty"`
	Teachlanguage2 string `json:"teach_language2" binding:"omitempty"`
	Teachlanguage3 string `json:"teach_language3" binding:"omitempty"`

	Sex         uint32 `json:"sex" binding:"omitempty"`
	LearnLevel  uint32 `json:"learn_level" binding:"omitempty"`
	LearnLevel2 uint32 `json:"learn_level2" binding:"omitempty"`
	LearnLevel3 uint32 `json:"learn_level3" binding:"omitempty"`
	TeachLevel2 uint32 `json:"teach_level2" binding:"omitempty"`
	TeachLevel3 uint32 `json:"teach_level3" binding:"omitempty"`
}

type IntroductionMsg struct {
	SenderNick  string              `json:"sender_nick" binding:"omitempty"`
	UserProfile IntroductionProfile `json:"user_profile" binding:"required"`
}

type LocationMsg struct {
	//{"address":"coastal city","longitude":"113.932681","latitude":"22.521179","place_name":"新桃园酒店 Grand View Hotel"}
	Address   string `json:"address" binding:"required"`
	Longitude string `json:"longitude" binding:"omitempty"`
	Latitude  string `json:"latitude" binding:"omitempty"`
	PlaceName string `json:"place_name" binding:"omitempty"`
}

//{"confidence":"0","display_confidence":true,"duration":4,"language":"zh-cn","name":"170911/1505118164.hta","size":15780,"text":"你行，","type":"hta","url":"http://jp-ht.oss-ap-northeast-1.aliyuncs.com/cvoc/170911/900866_EBB2D3FF45880EE74E88C5F889F83719.hta"}}

type VoiceTextMsg struct {
	//{"address":"coastal city","longitude":"113.932681","latitude":"22.521179","place_name":"新桃园酒店 Grand View Hotel"}
	Confidence string `json:"confidence" binding:"omitempty"`
	Display    bool   `json:"display_confidence" binding:"omitempty"`
	Duration   uint32 `json:"duration" binding:"omitempty"`
	Language   string `json:"language" binding:"omitempty"`
	Size       uint32 `json:"size" binding:"omitempty"`
	Text       string `json:"text" binding:"omitempty"`
	Type       string `json:"type" binding:"omitempty"`
	Url        string `json:"url" binding:"required"`
}

//{"source":"q3","target":"q3？在这"}
type CorrectionItem struct {
	Source string `json:"source" binding:"omitempty"`
	Target string `json:"target" binding:"omitempty"`
}

type CorrectionMsg struct {
	//{"address":"coastal city","longitude":"113.932681","latitude":"22.521179","place_name":"新桃园酒店 Grand View Hotel"}
	Body         []CorrectionItem `json:"body" binding:"omitempty"`
	Comment      string           `json:"comment" binding:"omitempty"`
	ModifyUserid uint32           `json:"modify_userid" binding:"omitempty"`
	UserID       uint32           `json:"userid" binding:"required"`
}
type LinkMsg struct {
	// { "des":"在jjj", "goto_url":"https://qtest.hellotalk.org/m/xNZmqMp0BNnWVD==?id=PVyPZN", "pic_url":"http://sz-ali-cdn-img.nihaotalk.com/mnt/171127/5547679_570931268cccd2740953ce07db7d9da6.jpg@0o_1l_300w_75q", "title":"理发师的手艺's moment on HelloTalk" }
	Type        string `json:"type" binding:"omitempty"`
	TrackId     string `json:"track_id" binding:"omitempty"`
	ProducerId  uint32 `json:"producer_id" binding:"omitempty"`
	Description string `json:"des" binding:"omitempty"`
	GotoUrl     string `json:"goto_url" binding:"omitempty"`
	PicUrl      string `json:"pic_url" binding:"omitempty"`
	Title       string `json:"title" binding:"omitempty"`
	SubTitle    string `json:"sub_title" binding:"omitempty"`
}

//==================================================

type IMMsg struct {
	FromNickName string `json:"from_nickname" binding:"required"`
	MsgType      string `json:"msg_type" binding:"required"`
	MsgID        string `json:"msg_id" binding:"required"`
	MsgModal     string `json:"msg_model" binding:"required"`
	ServerTs     uint64 `json:"server_ts" binding:"required"`
	ServerTime   string `json:"server_time" binding:"omitempty"`
	ProfileTs    uint64 `json:"from_profile_ts" binding:"omitempty"`
	Push         string `json:"push" binding:"omitempty"` //消息是不是推送。none,sound,no_sound

	Text         TextMsg         `json:"text" binding:"omitempty"`
	Image        ImageMsg        `json:"image" binding:"omitempty"`
	Doodle       ImageMsg        `json:"doodle" binding:"omitempty"`
	Correction   CorrectionMsg   `json:"correction" binding:"omitempty"`
	Voice        VoiceMsg        `json:"voice" binding:"omitempty"`
	Video        VideoMsg        `json:"video" binding:"omitempty"`
	Sticker      StickerMsg      `json:"new_sticker" binding:"omitempty"`
	Translate    TranslateMsg    `json:"translate" binding:"omitempty"`
	Introduction IntroductionMsg `json:"introduction" binding:"omitempty"`
	Location     LocationMsg     `json:"location" binding:"omitempty"`
	VoiceText    VoiceTextMsg    `json:"voice_text" binding:"omitempty"`
	Link         LinkMsg         `json:"link" binding:"omitempty"`
}

func GetMessageType(strMsgType string) uint8 {
	if 0 == strings.Compare(strMsgType, "text") {
		return MT_TEXT
	} else if 0 == strings.Compare(strMsgType, "translate") {
		return MT_TRANSLATE
	} else if 0 == strings.Compare(strMsgType, "voice") {
		return MT_VOICE
	} else if 0 == strings.Compare(strMsgType, "image") {
		return MT_IMAGE
	} else if 0 == strings.Compare(strMsgType, "introduction") {
		return MT_PROFILE
	} else if 0 == strings.Compare(strMsgType, "location") {
		return MT_LOCATE
	} else if 0 == strings.Compare(strMsgType, "voice_text") {
		return MT_VOICETEXT
	} else if 0 == strings.Compare(strMsgType, "correction") {
		return MT_CORRECTION
	} else if (0 == strings.Compare(strMsgType, "sticker")) ||
		(0 == strings.Compare(strMsgType, "new_sticker")) {
		return MT_STICKERS
	} else if 0 == strings.Compare(strMsgType, "doodle") {
		return MT_DOODLE
	} else if 0 == strings.Compare(strMsgType, "video") {
		return MT_VIDEO
	} else if 0 == strings.Compare(strMsgType, "link") {
		return MT_LINK
	} else if 0 == strings.Compare(strMsgType, "card") {
		return MT_CARD
	} else if 0 == strings.Compare(strMsgType, "notify") {
		return MT_NOTIFY
	}

	return MT_UNKNOWN
}

func GetPushTypeByMessageType(strMsgType string) uint32 {
	if 0 == strings.Compare(strMsgType, "text") {
		return PUSH_TEXT
	} else if 0 == strings.Compare(strMsgType, "voice") {
		return PUSH_VOICE
	} else if 0 == strings.Compare(strMsgType, "image") {
		return PUSH_IMAGE
	}
	return 0
}

func GetMessageContentInterface(Msg *IMMsg, intType uint8) interface{} {

	var body interface{}

	if intType == MT_TEXT {
		body = Msg.Text
	} else if intType == MT_TRANSLATE {
		body = Msg.Translate
	} else if intType == MT_IMAGE {
		body = Msg.Image
	} else if intType == MT_VOICE {
		body = Msg.Voice
	} else if intType == MT_PROFILE {
		body = Msg.Introduction
	} else if intType == MT_LOCATE {
		body = Msg.Location
	} else if intType == MT_VOICETEXT {
		body = Msg.VoiceText
	} else if intType == MT_CORRECTION {
		body = Msg.Correction
	} else if intType == MT_STICKERS {
		body = Msg.Sticker
	} else if intType == MT_DOODLE {
		body = Msg.Doodle
	} else if intType == MT_VIDEO {
		body = Msg.Video
	} else if intType == MT_LINK {
		body = Msg.Link
	}

	return body
}

func SetMessageInterface(maper map[string]interface{}, Msg *IMMsg, intType uint8) {

	msgType := Msg.MsgType

	if intType == MT_TEXT {
		maper[msgType] = Msg.Text
	} else if intType == MT_TRANSLATE {
		maper[msgType] = Msg.Translate
	} else if intType == MT_IMAGE {
		maper[msgType] = Msg.Image
	} else if intType == MT_VOICE {
		maper[msgType] = Msg.Voice
	} else if intType == MT_PROFILE {
		maper[msgType] = Msg.Introduction
	} else if intType == MT_LOCATE {
		maper[msgType] = Msg.Location
	} else if intType == MT_VOICETEXT {
		maper[msgType] = Msg.VoiceText
	} else if intType == MT_CORRECTION {
		maper[msgType] = Msg.Correction
	} else if intType == MT_STICKERS {
		maper[msgType] = Msg.Sticker
	} else if intType == MT_DOODLE {
		maper[msgType] = Msg.Doodle
	} else if intType == MT_VIDEO {
		maper[msgType] = Msg.Video
	} else if intType == MT_LINK {
		maper[msgType] = Msg.Link
	}

}

func GetMessageContentJson(Msg *IMMsg, intMsgType uint8) ([]byte, error) {

	if intMsgType == MT_TEXT {
		return []byte(Msg.Text.Text), nil
	}

	body := GetMessageContentInterface(Msg, intMsgType)
	// fmt.Printf("GetMessageContentJson body %+v\n", body)

	str, err := json.Marshal(body)
	// fmt.Printf("GetMessageContentJson bodyStr %+v\n", str)

	if err != nil {
		return []byte(""), err
	}
	return []byte(str), nil
}

type NTTime struct {
	Year    uint16
	Month   uint8
	Day     uint8
	Hour    uint8
	Minute  uint8
	Sencond uint8
}

type MgUserInfo struct {
	Status     uint32 `json:"status" binding:"required"`
	Message    string `json:"message" binding:"omitempty"`
	MgUserId   uint32 `json:"mg_user_id" binding:"omitempty"`
	MgNickName string `json:"mg_nickname" binding:"omitempty"`
}

type SimpleMgUserInfo struct {
	MgUserId   uint32 `json:"mg_user_id" binding:"omitempty"`
	MgNickName string `json:"mg_nickname" binding:"omitempty"`
	ReLogin    uint32 `json:"-"`
	IP         string `json:"ip" binding:"omitempty"`
}

type IMMsgPack struct {
	Header     *XTHead `json:"header" binding:"required"`
	Msg        *IMMsg  `json:"msg" binding:"required"`
	MgUserId   uint32  `json:"mg_user_id" binding:"omitempty"`  // 目的UID TO_SERVER = 0
	MgNickName string  `json:"mg_nickname" binding:"omitempty"` // 目的UID TO_SERVER = 0
}

//前端收到和发送的报文的格式。Data 字段 描述了真正的 消息字段
//type=msg 的时候，Data字段描述的是 一个 IMMsg 类型的字典

//type=chatting 如果kf打开了某个用户[to描述用户id]的对话框，则发送这个类型的信息，广播给其他用户。其他用户要维护一个chatting的字典 kfid=>user_id,
//type=tagging  如果kf标记了某个用户[to描述用户id]，发送typ=tagging的消息到服务端，同时会广播给其他用户，其他用户data={tag_id:3,name:"暴力"}

type WSMsgPack struct {
	TermType uint8  `json:"re"`                      // 终端类型
	Cmd      uint16 `json:"cmd" binding:"required"`  // 命令字
	Seq      uint16 `json:"id" binding:"required"`   // 序列号
	From     uint32 `json:"from" binding:"required"` // uint32_t uiFrom
	To       uint32 `json:"to" binding:"required"`   // 目的UID TO_SERVER = 0

	MgUserId        uint32 `json:"mg_user_id" binding:"omitempty"`  // 目的UID TO_SERVER = 0
	MgNickName      string `json:"mg_nickname" binding:"omitempty"` // 目的UID TO_SERVER = 0
	ServerTs        uint64 `json:"-"`
	ProfileTs       uint64 `json:"-"`
	ProfileNickName string `json:"-"`

	Type string                 `json:"typ" binding:"required"`
	Data map[string]interface{} `json:"data" binding:"required"`
}

func (p *IMMsgPack) ToWSMsgPack() (*WSMsgPack, error) {

	ws := &WSMsgPack{
		TermType:   p.Header.TermType,
		Cmd:        p.Header.Cmd,
		Seq:        p.Header.Seq,
		From:       p.Header.From,
		To:         p.Header.To,
		Type:       "msg",
		MgUserId:   p.MgUserId,
		MgNickName: p.MgNickName,
		ProfileTs:  p.Msg.ProfileTs,
	}

	intMsgType := GetMessageType(p.Msg.MsgType)

	if intMsgType == MT_UNKNOWN {
		return nil, errors.New("message type wrong msg_type=" + p.Msg.MsgType)
	}

	Data := make(map[string]interface{})

	Data["from_nickname"] = p.Msg.FromNickName
	ws.ProfileNickName = p.Msg.FromNickName
	Data["msg_type"] = p.Msg.MsgType
	Data["msg_id"] = p.Msg.MsgID
	Data["msg_model"] = p.Msg.MsgModal
	Data["server_ts"] = p.Msg.ServerTs
	Data["server_time"] = p.Msg.ServerTime
	Data["from_profile_ts"] = p.Msg.ProfileTs

	SetMessageInterface(Data, p.Msg, intMsgType)

	ws.Data = Data

	return ws, nil
}

func (ws *WSMsgPack) ToXTHeadPacket() (*XTHeadPacket, error) {

	head := &XTHead{
		Flag:     0xF4,
		Version:  0x04,
		CryKey:   E_SESSION_KEY, // 加密类型  E_NONE_KEY = 0, E_SESSION_KEY = 1, E_RAND_KEY = 2, E_SERV_KEY= 3
		TermType: 2,             // 终端类型
		Cmd:      ws.Cmd,
		Seq:      ws.Seq,
		From:     ws.From,
		To:       ws.To,
	}

	ws.Data["server_ts"] = ws.ServerTs
	ws.Data["from_profile_ts"] = ws.ProfileTs
	ws.Data["from_nickname"] = ws.ProfileNickName

	str, err := json.Marshal(ws.Data)

	if err != nil {
		return nil, err
	}

	htpack := &XTHeadPacket{
		Header: head,
		Body:   str,
	}

	return htpack, nil

}

func (ws *WSMsgPack) ToIMMsgPack() (*IMMsgPack, error) {

	head := &XTHead{
		Flag:     0xF4,
		Version:  IMPROTOCOL_VERSION_4,
		CryKey:   E_SESSION_KEY, // 加密类型  E_NONE_KEY = 0, E_SESSION_KEY = 1, E_RAND_KEY = 2, E_SERV_KEY= 3
		TermType: 2,             // 终端类型
		Cmd:      ws.Cmd,
		Seq:      ws.Seq,
		From:     ws.From,
		To:       ws.To,
	}

	str, err := json.Marshal(ws.Data)

	if err != nil {
		return nil, err
	}

	info := &IMMsg{}
	err = json.Unmarshal(str, info) // JSON to Struct

	info.ServerTs = ws.ServerTs

	if err != nil {
		return nil, err
	}

	impack := &IMMsgPack{
		Header:     head,
		Msg:        info,
		MgUserId:   ws.MgUserId,
		MgNickName: ws.MgNickName,
	}

	return impack, nil

}

//客服之前的通知，比如通知某个客服上线了，下线了某个客服接管了某个用户
// online 通知其他kf某个kf用户登陆了
// offline 通知其他某个kf用户下线了。
// all_online_kf 用户自己登录以后，返回所有的当前在线的kf
// //
type NotifyPack struct {
	Data       interface{} `json:"data" binding:"required"`
	Type       string      `json:"typ" binding:"required"`  //online offline all_online_kf ...
	MgUserId   uint32      `json:"from" binding:"required"` //这个包从谁那里发起的。登陆的时候记录的是自己的iD
	Seq        uint16      `json:"id" binding:"omitempty"`  // 序列号
	MgNickName string      `json:"mg_nickname" binding:"required"`
}
