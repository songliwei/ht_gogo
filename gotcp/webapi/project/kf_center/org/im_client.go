package org

import (
	// simplejson "github.com/bitly/go-simplejson"
	//"github.com/HT_GOGO/gotcp/libcrypto"

	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_p2p"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/golang/protobuf/proto"
	"log"
	"math"
	//"sync"
	"time"
)

/*	IM 服务端有如下逻辑，10000的消息 分发到 42900{ios} 或者 10002{android}
if (pHeader->to == HT_CUSTOMER_ACCOUNT)
{
    // 客服消息的特殊处理
    if (pHeader->reserved == 0) // iOS
    {
        theModifyHeader.to = HT_CUSTOMER_IOS;
    }
    else if (pHeader->reserved == 1)
    {
        theModifyHeader.to = HT_CUSTOMER_ANDROID;
    }
    bAPNSPush = false;
}
else if (pHeader->from == HT_CUSTOMER_IOS || pHeader->from == HT_CUSTOMER_ANDROID)
{
    theModifyHeader.from = HT_CUSTOMER_ACCOUNT;
}

*/

type handleMessageFunc func(im *IMClient, p *IMMsgPack)
type handleNotifyFunc func(im *IMClient, p *NotifyPack)
type handleCloseFunc func(im *IMClient)

type IMClient struct {
	infoLog *log.Logger
	Conn    *HTConn
	Db      *sql.DB

	SessionID  string
	SessionKey string
	profile_ts uint64

	UserID      uint32
	Email       string
	PassWord    string
	MyProfileTs uint64
	NickName    string

	lastOfflineTs   uint64 //离线消息的id
	lastOfflineTime NTTime
	ReqSeq          uint16 //发包的SeqId

	ReConnectAfterSecond uint32 //

	MessageHandler   handleMessageFunc //收到消息的回调函数
	NotifyHandler    handleNotifyFunc  //收到消息的回调函数
	CloseHandler     handleCloseFunc   //改用户连接断开的回调
	ReConnectHandler handleCloseFunc   //改用户连接断开的回调

	UserInfo  UserBaseInfo
	tcp_conf  TcpConfig
	RandomKey string

	RetryCount    uint32 //记录重试次数
	ReConectCount uint32 //记录重连次数

	im_host string
	im_port int

	PushTypeMap map[string]uint32

	p2pSvr *tcpcommon.P2PWorkerApiV2
}

// ConnCallback is an interface of methods that are used as callbacks on a connection
type IMCallback struct {
	client *IMClient
}

func (c *IMCallback) OnMessage(conn *HTConn, p Packet) bool {

	pack, ok := p.(*XTHeadPacket)

	if !ok { // 不是HeadV3Packet报文
		c.client.infoLog.Printf("OnMessage error packet can not change to HeadV3packet")
		return false
	}

	head := pack.Header

	c.client.infoLog.Printf("OnMessage() from=%d,to=%d,cmd=0x%x,cry=%d,TermType=%d,seq=%d,len=%d",
		head.From,
		head.To,
		head.Cmd,
		head.CryKey,
		head.TermType,
		head.Seq,
		head.Len,
	)

	if pack.Header.Cmd == CMD_P2P_MESSAGE {
		err := c.client.HandleSingleIMMessage(pack)
		if err != nil {
			c.client.infoLog.Println(c.client.UserID, "HandleSingleIMMessage() failed, with error=", err)
		}
	}
	str := ""
	if pack.Header.Cmd == CMD_USER_RELOGIN {
		c.client.ReConnectAfterSecond = 300
		str = fmt.Sprintf("%s [%d] is logined in somewhere else,relogin after %d second", c.client.NickName, c.client.UserID, c.client.ReConnectAfterSecond)
	}

	if pack.Header.Cmd == CMD_INVALID_SESSION {
		str = fmt.Sprintf("%s [%d] encoutner with invalid session", c.client.NickName, c.client.UserID)
	}

	if pack.Header.Cmd == CMD_FORCE_USER_OUT {
		c.client.ReConnectAfterSecond = 300
		str = fmt.Sprintf("%s [%d] forced to logout,relogin after %d second", c.client.NickName, c.client.UserID, c.client.ReConnectAfterSecond)
	}

	if pack.Header.Cmd == CMD_INVALID_SESSION || pack.Header.Cmd == CMD_USER_RELOGIN || pack.Header.Cmd == CMD_FORCE_USER_OUT {

		c.client.infoLog.Printf("OnMessage error %s", str)

		c.client.NotifyHandler(c.client, &NotifyPack{
			Type: "error",
			Data: str,
		})

		return false
	}

	if pack.Header.Cmd == CMD_GET_OFFLINEMESSAGE_ACK_V2 {
		err := c.client.ParamOfflineMessage(pack)
		if err != nil {
			c.client.infoLog.Println(c.client.UserID, "ParamOfflineMessage() failed, with error=", err)
		}
	}

	return true

}

func (c *IMCallback) OnClose(conn *HTConn, src string) {

	c.client.infoLog.Println(c.client.UserID, " closed() ", src)

	c.client.CloseHandler(c.client)
	ReTryCount := 0
	MaxRetryCount := 500
	go func() {
		for {

			time.Sleep(time.Second * 2)

			c.client.infoLog.Println(c.client.UserID, " start Reconnect() ")

			callback := &IMCallback{client: c.client}

			proto := &IMProtocol{client: c.client}

			host := fmt.Sprintf("%s:%d", c.client.im_host, c.client.im_port)

			reconn := newHTConn(host, c.client.tcp_conf, callback, proto)

			err := reconn.Do()
			c.client.ReConectCount++
			if err != nil {
				c.client.SessionID = ""
				c.client.infoLog.Println(c.client.UserID, "StartLogin() OnClose() error,", err, c.client.Email, c.client.im_host, c.client.im_port)
				time.Sleep(time.Second * time.Duration(2*int(math.Sqrt(float64(ReTryCount)))))
				ReTryCount++
				if ReTryCount >= MaxRetryCount {
					c.client.infoLog.Println(c.client.UserID, "StartLogin() OnClose() ReTryCount over limit", ReTryCount)
					break
				}
				continue
			}

			c.client.Conn = reconn
			return
		}

	}()
}

func (c *IMCallback) Login(conn *HTConn) bool {

	var loginPayLoad []byte
	email := c.client.Email

	var seq uint16
	seq = 1

	_thisUID := c.client.UserID

	tcpcommon.MarshalUint8(0, &loginPayLoad)
	tcpcommon.MarshalSlice([]byte(email), &loginPayLoad)

	//fmt.Println("OnConnect() buff", buff)

	pack := NewXTHeadPacketWithParam(0, 0, CMD_CLIENT_LOGIN, E_RAND_KEY, seq, c.client.RandomKey, loginPayLoad)

	// fmt.Printf("OnConnect() pack=%v ,body=%v", pack.Header, pack.Body)

	// fmt.Printf("pack.Serialize()=%v", pack.Serialize())

	err := conn.WritePackToConn(pack)

	if err != nil {
		c.client.infoLog.Println(_thisUID, "send() error:", err)
		return false
	}

	p, err := conn.ReadDataFromConn()
	if err != nil {
		c.client.infoLog.Println(_thisUID, "read() error:", err)
		return false
	}

	pack, ok := p.(*XTHeadPacket)

	if !ok { // 不是HeadV3Packet报文
		c.client.infoLog.Printf("ReadDataFromConn packet can not change to XTHeadPacket")
		return false
	}

	if pack.Header.Cmd != CMD_CLIENT_LOGIN_ACK {
		c.client.infoLog.Println(_thisUID, "read() error: cmd not mactch", pack.Header)
		return false
	}

	bin_data := pack.Body

	ret := tcpcommon.UnMarshalUint8(&bin_data)

	if ret != 0 {
		c.client.infoLog.Println(_thisUID, "login() error ret=", ret)
		return false
	}

	Userid := tcpcommon.UnMarshalUint32(&bin_data)
	cnonce := tcpcommon.UnMarshalSlice(&bin_data)
	once := tcpcommon.UnMarshalSlice(&bin_data)
	reg_time := tcpcommon.UnMarshalUint64(&bin_data)

	if Userid != _thisUID {
		c.client.infoLog.Println(_thisUID, "login() userid not match=", Userid)
		return false
	}

	c.client.infoLog.Println(_thisUID, "login() once=", string(once), ",cnonce=", string(cnonce), ",reg_time=", reg_time)

	var Authbuff []byte

	terminalType := 2
	verInt := 132357
	deviceID := "web"
	OSVersion := "9.2.1"
	OSLang := "en"
	Terminal := "web.1"
	PushToken := "Invalid_Token"
	operator := "null"
	operatorCounry := "COM"
	appStoreCountry := "HK"
	jailBreak := 0
	backGround := 0
	androidPush := 0

	passwordSend := common.Md5(c.client.PassWord + string(once))

	tcpcommon.MarshalUint32(Userid, &Authbuff)
	tcpcommon.MarshalSlice([]byte(passwordSend), &Authbuff)
	tcpcommon.MarshalUint8(uint8(terminalType), &Authbuff)
	tcpcommon.MarshalUint32(uint32(verInt), &Authbuff)
	tcpcommon.MarshalSlice([]byte(deviceID), &Authbuff)
	tcpcommon.MarshalSlice([]byte(OSVersion), &Authbuff)
	tcpcommon.MarshalSlice([]byte(OSLang), &Authbuff)
	tcpcommon.MarshalSlice([]byte(Terminal), &Authbuff)
	tcpcommon.MarshalSlice([]byte(PushToken), &Authbuff)
	tcpcommon.MarshalSlice([]byte(operator), &Authbuff)
	tcpcommon.MarshalSlice([]byte(operatorCounry), &Authbuff)
	tcpcommon.MarshalSlice([]byte(appStoreCountry), &Authbuff)
	tcpcommon.MarshalUint8(uint8(jailBreak), &Authbuff)
	tcpcommon.MarshalUint8(uint8(backGround), &Authbuff)
	tcpcommon.MarshalUint8(uint8(androidPush), &Authbuff)

	Authpack := NewXTHeadPacketWithParam(0, 0, CMD_LOGIN_AUTH, E_RAND_KEY, seq+1, c.client.RandomKey, Authbuff)

	err = conn.WritePackToConn(Authpack)

	if err != nil {
		c.client.infoLog.Println(_thisUID, "send Auth() error:", err)
		return false
	}

	p, err = conn.ReadDataFromConn()
	if err != nil {
		c.client.infoLog.Println(_thisUID, "read Auth() error:", err)
		return false
	}

	pack, ok = p.(*XTHeadPacket)

	if !ok { // 不是HeadV3Packet报文
		c.client.infoLog.Printf("ReadDataFromConn auth packet can not change to XTHeadPacket")
		return false
	}

	if pack.Header.Cmd != CMD_LOGIN_AUTH_ACK {
		c.client.infoLog.Println(_thisUID, "Auth() error: cmd not mactch", pack.Header)
		return false
	}

	bin_data = pack.Body

	ret = tcpcommon.UnMarshalUint8(&bin_data)

	if ret != 0 {
		c.client.infoLog.Println(_thisUID, "Auth() error ret=", ret, pack.Body)
		return false
	}

	Userid = tcpcommon.UnMarshalUint32(&bin_data)

	if Userid != _thisUID {
		c.client.infoLog.Println(_thisUID, "Auth() userid not match=", Userid)
		return false
	}

	sessionKey := pack.Body[5:21]

	c.client.SessionKey = string(sessionKey)

	bin_data = pack.Body[21:]

	sessionID := tcpcommon.UnMarshalSlice(&bin_data)

	MyProfileTs := tcpcommon.UnMarshalUint64(&bin_data)

	c.client.MyProfileTs = (MyProfileTs)
	c.client.SessionID = string(sessionID)

	c.client.infoLog.Println(_thisUID, c.client.NickName, "read Auth() session_id=", c.client.SessionID, ",profile_ts=", c.client.MyProfileTs)

	return true

}

func (c *IMCallback) ReConnect(conn *HTConn, SessionID string) (bool, error) {

	var Authbuff []byte
	_thisUID := c.client.UserID
	md5 := common.Md5(fmt.Sprintf("%d%s", _thisUID, SessionID))
	verInt := uint32(132357)
	terminalType := uint8(0)
	backGround := uint8(0)

	tcpcommon.MarshalUint32(_thisUID, &Authbuff)
	tcpcommon.MarshalSlice([]byte(md5), &Authbuff)
	tcpcommon.MarshalUint32(verInt, &Authbuff)
	tcpcommon.MarshalUint8(terminalType, &Authbuff)
	tcpcommon.MarshalUint8(backGround, &Authbuff)

	Authpack := NewXTHeadPacketWithParam(0, 0, CMD_CLIENT_RECONNECT, E_RAND_KEY, 1, c.client.RandomKey, Authbuff)

	err := conn.WritePackToConn(Authpack)
	if err != nil {
		return false, fmt.Errorf("first write pack errror %s", err.Error())
	}

	p, err := conn.ReadDataFromConn()
	if err != nil {
		return false, fmt.Errorf("read pack errror %s", err.Error())
	}

	pack, ok := p.(*XTHeadPacket)

	if !ok { // 不是HeadV3Packet报文
		return false, errors.New("ReadDataFromConn packet can not change to XTHeadPacket")
	}

	if pack.Header.Cmd != CMD_CLIENT_RECONNECT_ACK {
		return false, fmt.Errorf("cmd not mactch %x != %x", pack.Header.Cmd, CMD_CLIENT_RECONNECT_ACK)
	}

	bin_data := pack.Body

	ret := tcpcommon.UnMarshalUint8(&bin_data)

	if ret != 0 {
		errDes := ""
		if len(pack.Body) > 5 {
			errDes = string(pack.Body[3:])
		}
		return false, fmt.Errorf("auth ret=%x err=%s", ret, errDes)
	}

	sessionKey := pack.Body[1:17]

	c.client.SessionKey = string(sessionKey)

	bin_data = pack.Body[17:]

	serverTs := tcpcommon.UnMarshalUint64(&bin_data)

	c.client.infoLog.Println(_thisUID, "ReConnect() ok serverTs=", serverTs)

	return true, nil

}

func (c *IMCallback) OnConnect(conn *HTConn) bool {

	var connect_ret bool
	var err error

	if c.client.ReConectCount > 0 {
		c.client.ReConnectHandler(c.client)
	}

	if c.client.ReConnectAfterSecond > 0 {
		c.client.infoLog.Println(c.client.UserID, "OnConnect() affter period:", c.client.ReConnectAfterSecond)

		time.Sleep(time.Duration(c.client.ReConnectAfterSecond) * time.Second)
	}

	if c.client.SessionID == "" {
		connect_ret = c.Login(conn)
	} else {
		connect_ret, err = c.ReConnect(conn, c.client.SessionID)

		if err != nil {

			str := fmt.Sprintf("%s [%d] ReConnect fail %s", c.client.NickName, c.client.UserID, err.Error())

			c.client.infoLog.Println(str)

			c.client.NotifyHandler(c.client, &NotifyPack{
				Type: "error",
				Data: str,
			})
		}

	}

	if connect_ret {
		c.client.ReConnectAfterSecond = 0
		err := c.client.GetOfflineMessage()
		if err != nil {
			c.client.infoLog.Println(c.client.UserID, "GetOfflineMessage() affter Connect", err)
		}

	}

	return connect_ret
}

func NewIMClient(
	im_host string,
	im_port int,
	UserID uint32,
	Db *sql.DB,
	logger *log.Logger,
	p2p *tcpcommon.P2PWorkerApiV2) *IMClient {

	cnf := TcpConfig{
		PacketSendChanLimit:    128,
		PacketReceiveChanLimit: 128,
	}

	host := fmt.Sprintf("%s:%d", im_host, im_port)

	c := &IMClient{
		infoLog:              logger,
		im_host:              im_host,
		im_port:              im_port,
		Db:                   Db,
		tcp_conf:             cnf,
		MessageHandler:       func(*IMClient, *IMMsgPack) {},
		NotifyHandler:        func(*IMClient, *NotifyPack) {},
		RandomKey:            "0123456789111111",
		Email:                "hellotalk@hellotalk.com",
		UserID:               UserID,
		PassWord:             "2a18f08370824dfb7159e099ddcd2dac",
		ReConnectAfterSecond: 0,
		p2pSvr:               p2p,
	}

	callback := &IMCallback{client: c}
	proto := &IMProtocol{client: c}

	c.Conn = newHTConn(host, cnf, callback, proto)

	// 推送类型
	c.PushTypeMap = map[string]uint32{
		"text":   PUSH_TEXT,
		"voice":  PUSH_VOICE,
		"image":  PUSH_IMAGE,
		"video":  PUSH_VIDEO,
		"notify": PUSH_LINK,
	}

	return c
}

func (c *IMClient) startTimer(fn func()) {

	// 启动tikcer
	ticker := time.NewTicker(time.Second * 1 * 60)

	go func() {
		index := 0
		for range ticker.C {
			fn()
			c.infoLog.Printf("main ticker tick index=%v", index)
			index++
		}
	}()
}

func (im *IMClient) GetEmailAndPassWord(Userid uint32) (string, string, error) {

	var (
		email    string
		password string
	)

	db := im.Db

	err := db.QueryRow("SELECT  `EMAIL`,`PASSWORD`  from HT_USER_ACCOUNT WHERE `USERID`=?", Userid).Scan(&email, &password)

	if err == sql.ErrNoRows {
		return "", "", errors.New("no")
	}

	if err != nil {
		return email, password, err
	}

	return email, password, nil

}

func (im *IMClient) GetNickName(Userid uint32) (string, error) {

	var (
		nickname string
		username string
	)

	db := im.Db

	err := db.QueryRow("SELECT  `USERNAME`,`NICKNAME`  from HT_USER_BASE WHERE `USERID`=?", Userid).Scan(&username, &nickname)

	if err == sql.ErrNoRows {
		return "HelloTalk Team", errors.New("no")
	}

	if err != nil {
		return nickname, err
	}

	if nickname != "" {
		return nickname, nil
	}

	return username, nil

}

// HandleDisconnect fires fn when a session disconnects.
func (m *IMClient) HandleMessage(fn func(im *IMClient, p *IMMsgPack)) {
	m.MessageHandler = fn
}

// HandleDisconnect fires fn when a session disconnects.
func (m *IMClient) HandleNotify(fn func(im *IMClient, p *NotifyPack)) {
	m.NotifyHandler = fn
}

// HandleDisconnect fires fn when a session disconnects.
func (m *IMClient) HandleClosed(fn func(im *IMClient)) {
	m.CloseHandler = fn
}

// HandleDisconnect fires fn when a session disconnects.
func (m *IMClient) HandleReConnected(fn func(im *IMClient)) {
	m.ReConnectHandler = fn
}

func (im *IMClient) HandleSingleIMMessage(pack *XTHeadPacket) error {

	// 解压缩
	unCompressSlice, err := common.DepressString(pack.Body)

	if err != nil {
		im.infoLog.Printf("%d HandleSingleIMMessage() bad pack %s,%x", im.UserID, err.Error(), string(pack.Body))
		return fmt.Errorf("read zip error=%s", err.Error())
	}

	//im.infoLog.Println(im.UserID, "HandleSingleIMMessage() one_full_pack ", pack.Body)

	info := &IMMsg{}
	err = json.Unmarshal(unCompressSlice, info) // JSON to Struct

	if err != nil {
		return fmt.Errorf("json error=%s,body=%s", err.Error(), string(unCompressSlice))
	}

	im.infoLog.Printf("%d HandleSingleIMMessage()  json=%s", im.UserID, string(unCompressSlice))

	msgPack := &IMMsgPack{}

	msgPack.Msg = info
	msgPack.Header = pack.Header

	// im.infoLog.Printf("HandleSingleIMMessage() Msg=%#v", info)

	im.MessageHandler(im, msgPack)

	err = im.SendAck(pack)

	return nil
}

func (im *IMClient) SendAck(pack *XTHeadPacket) error {

	head := pack.Header

	var buff []byte
	resp := NewXTHeadPacketWithParam(head.From, head.To, head.Cmd+1, head.CryKey, head.Seq, im.SessionKey, buff)

	err := im.Conn.AsyncWritePacket(resp, 60*time.Second)

	if err != nil {
		im.infoLog.Println(im.UserID, "SendAck err=", err, head)
		return err
	}

	im.infoLog.Println(im.UserID, "SendAck() ok head=", resp.Header)

	return err
}

func (im *IMClient) StartLogin() error {

	email, PassWord, err := im.GetEmailAndPassWord(im.UserID)

	if err != nil {
		return err
	}
	c := im
	c.Email = email
	c.PassWord = PassWord
	nickname, err := im.GetNickName(im.UserID)
	c.NickName = nickname

	c.infoLog.Println(c.UserID, "StartLogin()  ", email, c.im_host, c.im_port)

	err = c.Conn.Do()

	if err != nil {

		c.infoLog.Println(c.UserID, "StartLogin()  error,", err, email, c.im_host, c.im_port)

	} else {
		c.infoLog.Println(c.UserID, "StartLogin() success ", email, c.im_host, c.im_port)

		c.startTimer(func() {
			c.SendHeartBeat()
		})

	}

	return err
}

func (im *IMClient) SendHeartBeat() {

	var buff []byte

	now := time.Now()
	nanos := now.UnixNano()

	tcpcommon.MarshalUint32(im.UserID, &buff)
	tcpcommon.MarshalUint64(uint64(nanos/1000000), &buff)

	beat_pack := NewXTHeadPacketWithParam(im.UserID, 0, CMD_HEART_BEAT, E_SESSION_KEY, 1, im.SessionKey, buff)

	err := im.Conn.AsyncWritePacket(beat_pack, time.Second*60)

	if err != nil {
		im.infoLog.Println(im.UserID, "SendHeartBeat() error:", err)
	} else {
		im.infoLog.Println(im.UserID, "SendHeartBeat() success ")
	}
}

//原来是通过IMserver发送消息的，现在改成用P2Pserver
func (im *IMClient) SendMsgPack(wsmsg *WSMsgPack) error {

	wsmsg.ProfileTs = im.MyProfileTs
	wsmsg.ProfileNickName = im.NickName

	xtpack, err := wsmsg.ToXTHeadPacket()

	im.infoLog.Println(wsmsg)

	if err != nil {
		im.infoLog.Println("ToXTHeadPacket() err:", err)
		return err
	}

	var buff []byte

	if xtpack.Header.Cmd == CMD_P2P_MESSAGE {
		// 压缩JSON消息数据
		buff = common.CompressString(xtpack.Body)
	} else {
		buff = xtpack.Body
	}

	// msg_pack := NewXTHeadPacketWithParam(xtpack.Header.From, xtpack.Header.To, xtpack.Header.Cmd, xtpack.Header.CryKey, xtpack.Header.Seq, im.SessionKey, buff)

	// return im.SendPack(msg_pack)
	head, payLoad, err := im.BuildP2PPacket(xtpack.Header, buff, wsmsg)
	if err != nil {
		im.infoLog.Println("BuildP2PPacket() err:", err)
		return err
	}
	// return err;
	ret, err := im.p2pSvr.SendPacket(head, payLoad)
	if err != nil {
		im.infoLog.Printf("p2pSvr.SendPacket() err:%v, ret:%v", err, ret)
		return err
	}
	return err
}

func (im *IMClient) BuildP2PPacket(xtHead *XTHead, buff []byte, wsmsg *WSMsgPack) (*tcpcommon.HeadV3, []uint8, error) {

	pbBody := &ht_p2p.P2PMsgBody{
		JustOnline: proto.Uint32(0),
		DontReply:  proto.Uint32(1),
		P2PData:    buff,
		ToType:     ht_p2p.TO_CLIENT_TYPE_SEND_TO_ALL.Enum(),
	}

	immsg, err := wsmsg.ToIMMsgPack()

	if err != nil {
		im.infoLog.Printf("BuildP2PPacket ToIMMsgPack fail")
	}

	// 处理推送
	if wsmsg.Data["push"] != nil && wsmsg.Data["push"] != "none" {

		var pushSound, PushType uint32
		pushSound = 0 // 有声音是1，没声音是0
		if wsmsg.Data["push"] == "sound" {
			pushSound = 1
		}
		PushType = GetPushTypeByMessageType(immsg.Msg.MsgType)

		pbBody.PushInfo = &ht_p2p.PushInfo{
			PushType:  &PushType,
			NickName:  []byte(wsmsg.ProfileNickName),
			PushSound: &pushSound,
		}
		// 只有文字类型才添加推送内容
		if PushType == PUSH_TEXT {
			pbBody.PushInfo.Content = []byte(immsg.Msg.Text.Text)
		}

		im.infoLog.Printf("Found push info, build result: %+v", pbBody.PushInfo)
	}

	payLoad, err := proto.Marshal(pbBody)

	if err != nil {
		im.infoLog.Printf("proto.Marshal failed")
	}

	// 10002 和 42900 都转成 10000 发到P2P
	var from uint32
	if xtHead.From == 10002 || xtHead.From == 42900 {
		from = 10000
	} else {
		from = xtHead.From
	}

	head := &tcpcommon.HeadV3{}
	head.Flag = 0XF0
	head.CryKey = 0
	head.Version = 0x04
	head.TermType = xtHead.TermType
	head.Seq = 1
	head.Cmd = xtHead.Cmd
	head.From = from
	head.To = xtHead.To
	head.Len = uint32(len(payLoad) + tcpcommon.HeadV3Len)

	im.infoLog.Printf("BuildP2PPacket head: %+v", head)

	if err != nil {
		im.infoLog.Printf("proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return head, payLoad, err
	}
	return head, payLoad, nil
}

func (im *IMClient) GetOfflineMessage() error {

	var buff []byte
	_UserID := im.UserID

	tcpcommon.MarshalUint32(_UserID, &buff)
	tcpcommon.MarshalUint16(im.lastOfflineTime.Year, &buff)
	tcpcommon.MarshalUint8(im.lastOfflineTime.Month, &buff)
	tcpcommon.MarshalUint8(im.lastOfflineTime.Day, &buff)
	tcpcommon.MarshalUint8(im.lastOfflineTime.Hour, &buff)
	tcpcommon.MarshalUint8(im.lastOfflineTime.Minute, &buff)
	tcpcommon.MarshalUint8(im.lastOfflineTime.Sencond, &buff)
	tcpcommon.MarshalUint64(im.lastOfflineTs, &buff)

	msg_pack := NewXTHeadPacketWithParam(_UserID, 0, CMD_GET_OFFLINEMESSAGE_V2, E_SESSION_KEY, im.ReqSeq+1, im.SessionKey, buff)

	return im.SendPack(msg_pack)

}

func (im *IMClient) SendPack(xtpack *XTHeadPacket) error {

	err := im.Conn.AsyncWritePacket(xtpack, time.Second*60)

	if err != nil {
		im.infoLog.Println(im.UserID, "SendPack() error:", err)
	} else {
		im.infoLog.Println(im.UserID, "SendPack() success ")
	}

	return nil
}

func (im *IMClient) ParamOfflineMessage(pack *XTHeadPacket) error {

	// local cnf 	=	{
	// 	{'ret_code','uint8'},
	// 	{'last_offline','dnttime'},
	// 	{'last_offline_id','uint64s'},
	// 	{'msg_count','uint16'},
	// 	{'any_more','uint8'},
	// }

	bin_data := pack.Body

	ret_code := tcpcommon.UnMarshalUint8(&bin_data)
	_thisUID := im.UserID
	if ret_code != 0 {
		return fmt.Errorf("fail error ret=%d", ret_code)
	}

	year := tcpcommon.UnMarshalUint16(&bin_data)
	month := tcpcommon.UnMarshalUint8(&bin_data)
	day := tcpcommon.UnMarshalUint8(&bin_data)
	hour := tcpcommon.UnMarshalUint8(&bin_data)
	minute := tcpcommon.UnMarshalUint8(&bin_data)
	sencond := tcpcommon.UnMarshalUint8(&bin_data)

	if year < 2000 {
		//return fmt.Errorf("year error with year=%d", year)

	}

	nttime := NTTime{
		Year:    year,
		Month:   month,
		Day:     day,
		Hour:    hour,
		Minute:  minute,
		Sencond: sencond,
	}

	im.lastOfflineTime = nttime
	im.lastOfflineTs = tcpcommon.UnMarshalUint64(&bin_data)
	msg_count := tcpcommon.UnMarshalUint16(&bin_data)
	any_more := tcpcommon.UnMarshalUint8(&bin_data)

	im.infoLog.Printf("%d ParamOfflineMessage() last date=%d-%d-%d %d:%d:%d,last_offline_ts=%d,msg_count=%d,any_more=%d",
		_thisUID, year, month, day, hour, minute, sencond, im.lastOfflineTs, msg_count, any_more)

	if msg_count == 0 {
		return nil
	}

	for i := 0; i < int(msg_count); i++ {

		one_full_pack := UnMarshalSliceWithOutZero(&bin_data)

		if one_full_pack == nil {
			return errors.New("pack length check fail")
		}

		head, err := NewXTHead(one_full_pack[0:XTHeadLen])

		if err != nil {
			return errors.New("build head fail")
		}

		im.infoLog.Printf("ParamOfflineMessage() from=%d,to=%d,cmd=0x%x,cry=%d,TermType=%d,seq=%d,len=%d,id=%d",
			head.From,
			head.To,
			head.Cmd,
			head.CryKey,
			head.TermType,
			head.Seq,
			head.Len,
			i,
		)

		head.IsOffline = 1
		pack := &XTHeadPacket{
			Header: head,
			Body:   one_full_pack[XTHeadLen:],
		}

		if head.Cmd == CMD_P2P_MESSAGE {
			err := im.HandleSingleIMMessage(pack)
			if err != nil {
				im.infoLog.Println(_thisUID, "HandleSingleIMMessage() error when get more,", err)

			}

			//c.client.infoLog.Println(c.client.UserID, "HandleSingleIMMessage()  pack.Body =", pack.Body)
		}

	}

	if any_more > 0 {
		err := im.GetOfflineMessage()
		if err != nil {
			im.infoLog.Println(_thisUID, "GetOfflineMessage() error when get more,", err)
		}

	}

	return nil
}
