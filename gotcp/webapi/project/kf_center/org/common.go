package org

type UserBaseInfo struct {
	// HT_USER_BASE
	UserName      string //用户名
	UserType      uint32 //用户类别：0-普通用户
	UserNickName  string //用户别名 昵称
	Fullpy        string //用户别名的拼音
	ShortFullpy   string //用户别名的拼音缩写
	Sex           uint32 //性别
	Birthday      uint32 //生日
	Signature     string //签名
	HeadUrl       string //用户头像URL
	VoiceUrl      string //用户语音介绍的URL
	VoiceDuration uint32 //语音介绍录音的时长
	Nationality   string //用户国籍英文简称
	TimeZone      uint8  //用户所在时区(24时区制)
	SummerTime    uint8  //夏令制设置
	TimeZone48    uint8  //用户时区(半时区为单位 -24 到 24)

	// HT_USER_LANGUAGE
	AllowCount      uint32 //可以教学的语言数量(使用权限)
	NativeLang      uint32 //母语 语言语种使用数字代表
	TeachLang2      uint32 //教学语言二
	TeachLang2Level uint32 //教学语言二等级
	TeachLang3      uint32 //教学语言三
	TeachLang3Level uint32 //教学语言三等级
	LearnLang1      uint32 //学习语言一
	LearnLang1Level uint32 //学习语言一等级
	LearnLang2      uint32 //学习语言二
	LearnLang2Level uint32 //学习语言二等级
	LearnLang3      uint32 //学习语言三
	LearnLang3Level uint32 //学习语言三等级
	LearnLang4      uint32 //学习语言四
	LearnLang4Level uint32 //学习语言四等级

	// HT_USER_PRIVATE
	HideLocation uint32 //隐藏定位信息
	HideCity     uint32 //隐藏城市信息
	HideAge      uint32 //隐藏年龄信息
	HideOnline   uint32 //隐藏在线状态

	// HT_USER_PROPERTY PROFILEVERSION
	ProfileVer       uint64 //用户资料版本时间戳
	FriendVer        uint64 //用户好友版本时间戳
	BlackListVer     uint64 //黑名单版本时间戳
	LocationVer      uint64 //用户定位信息版本时间戳
	InviteVer        uint64 //好友邀请版本戳
	LastGetOffMsgVer int64  //最后成功获取的离线时间戳
	HeadModifyVer    int64  //头像最后修改时间
}
