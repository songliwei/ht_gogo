package org

import (
	"database/sql"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/webapi/common"
	// "io/ioutil"
	"strconv"
	"strings"
	//"github.com/garyburd/redigo/redis"
	"encoding/json"
	"errors"
	"fmt"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/go-sql-driver/mysql"
	"github.com/lilien1010/melody"
	"log"
	"net/http"
	"sort"
	"sync"
	"sync/atomic"
	"time"
)

const (
	VIP_CACHE_CAP = 100
)

// 忽略不处理的消息
type IgnoreMsgTypeErr struct {
	MsgType string
}

func (err *IgnoreMsgTypeErr) Error() string {
	return fmt.Sprintf("Ignore message type : %s", err.MsgType)
}

type KfClient struct {
	Session    *melody.Session
	MgUserID   uint32
	MgNickName string
	ConnectTs  uint64
}

type handleWsMessageFunc func(wsmsg *WSMsgPack)
type filterFunc func(id uint32) bool

type KfGroup struct {
	KfIdServiceIdMap map[uint32][]uint32 // [10000] => {1,2,3} 记录某个服务号由哪些个客服接管消息
	KfIdServiceLock  *sync.Mutex

	KfGroupLock      *sync.Mutex
	ManagedKfIdGroup map[uint32]*melody.Session //[2] => websocket  记录客服id的连接
	OnlineKfCount    int32

	mrouter       *melody.Melody
	Mgdb          *sql.DB
	infoLog       *log.Logger
	mcUserState   *tcpcommon.MemcacheApi
	userinfoRd    *tcpcommon.RedisApi
	messageHandle handleWsMessageFunc

	TaggedVIP    map[uint32]uint64 // 缓存已经打过VIP标签的用户，以及他们最新消息的时间戳
	InformUserid []uint32          // 每条客服发出的消息都复制一份发给这个列表的人
}

func NewKfGroup(Mgdb *sql.DB, infoLog *log.Logger, mcUserState *tcpcommon.MemcacheApi, userinfoRd *tcpcommon.RedisApi) *KfGroup {

	ManagedKfIdGroup := map[uint32]*melody.Session{}
	KfIdServiceIdMap := make(map[uint32][]uint32)
	TaggedVIP := make(map[uint32]uint64)
	infoLog.Printf("NewKfGroup TaggedVIP:%+v", TaggedVIP)

	mrouter := melody.New()

	kf_group := &KfGroup{
		KfIdServiceIdMap: KfIdServiceIdMap,
		ManagedKfIdGroup: ManagedKfIdGroup,
		infoLog:          infoLog,
		mcUserState:      mcUserState,
		userinfoRd:       userinfoRd,
		Mgdb:             Mgdb,
		KfGroupLock:      new(sync.Mutex),
		KfIdServiceLock:  new(sync.Mutex),
		TaggedVIP:        TaggedVIP,
		messageHandle: func(wsmsg *WSMsgPack) {

		},
	}

	kf_group.mrouter = mrouter

	return kf_group

}

func (m *KfGroup) InitRouteHandler() {

	m.mrouter.HandleConnect(func(s *melody.Session) {

		err := m.SaveNewSession(s)
		if err != nil {
			m.infoLog.Println("HandleConnect SaveNewSession() err=", err)
			return
		}

	})

	m.mrouter.HandleDisconnect(func(s *melody.Session) {

		err := m.RemoveWsSession(s)
		if err != nil {
			m.infoLog.Println("HandleDisconnect() err=", err)
			return
		}

	})

	m.mrouter.HandleError(func(s *melody.Session, errp error) {

		value, err := m.getMgUserInfoFromSession(s)
		if err != nil {
			m.infoLog.Println("HandleError() err=", err)
			return
		}
		fmt.Println("HandleError()  ", value.MgUserId, value.MgNickName, ",websocket errp=", errp)
		err = s.Close()
		if err != nil {
			fmt.Println("HandleError() close session err :", err)
		}
		return
	})

	m.mrouter.HandleMessage(func(s *melody.Session, msg []byte) {

		value, err := m.getMgUserInfoFromSession(s)
		if err != nil {
			m.infoLog.Println("HandleMessage() err=", err)
			m.NotifyOtherKf(nil, s, value, "error", "get userinfo fail ,error="+err.Error())
			return
		}

		wsmsg := &WSMsgPack{}
		err = json.Unmarshal(msg, wsmsg) // JSON to Struct

		if err != nil {
			m.infoLog.Println("HandleMessage() json Unmarshal fail, json=", string(msg))
			m.NotifyOtherKf(nil, s, value, "error", "json Unmarshal fail, error="+err.Error())
			return
		}

		now := time.Now()
		nanos := now.UnixNano()

		wsmsg.ServerTs = uint64(nanos / 1000000)
		wsmsg.MgUserId = value.MgUserId
		wsmsg.MgNickName = value.MgNickName

		//只有message类型的消息才转发到客户端
		if wsmsg.Type == "msg" {

			if wsmsg.From < 10000 || wsmsg.To < 10000 || wsmsg.Cmd == 0 {
				m.infoLog.Println("HandleMessage() message format error, json=", string(msg))
				m.NotifyKfWithId(value.MgUserId, "error", "message format error")
				return
			}

			m.messageHandle(wsmsg)
		} else {

			//ping 协议通知
			if wsmsg.Type == "ping" {
				m.NotifyOtherKf(nil, s, value, "pong", "")
				return
			}

			//其他消息转发到其他客服
			notifyJson, err := json.Marshal(wsmsg)
			if err != nil {
				m.infoLog.Println("HandleMessage() json marshal fail ", err)
			}
			err = m.mrouter.BroadcastOthers(notifyJson, s)
			if err != nil {
				m.infoLog.Println("HandleMessage() Broadcast fail ", err)
			}

		}

	})
}

func (m *KfGroup) GetOnlineCount() int32 {
	totalOnline := atomic.LoadInt32(&m.OnlineKfCount)
	return totalOnline
}

func (m *KfGroup) OnMessage(fn handleWsMessageFunc) {

	m.messageHandle = fn
}

func (m *KfGroup) InitWsSession(w http.ResponseWriter, r *http.Request, keys map[string]interface{}) {
	m.mrouter.HandleRequestWithKeys(w, r, keys)
}

func (m *KfGroup) getMgUserInfoFromSession(s *melody.Session) (*SimpleMgUserInfo, error) {

	info, exist := s.Get("mguserinfo")

	if exist == false {
		return nil, errors.New("error: mguserinfo not exist")
	}

	value, ok := info.(*SimpleMgUserInfo)
	if !ok {
		return nil, errors.New("error: mguserinfo type not match,exist=1")
	}

	return value, nil
}

func (m *KfGroup) RemoveWsSession(s *melody.Session) error {
	value, err := m.getMgUserInfoFromSession(s)
	if err != nil {
		m.infoLog.Println("HandleDisconnect() err=", err)
		return err
	}

	m.KfGroupLock.Lock()
	defer m.KfGroupLock.Unlock()
	delete(m.ManagedKfIdGroup, value.MgUserId)
	atomic.AddInt32(&m.OnlineKfCount, -1)
	totalOnline := atomic.LoadInt32(&m.OnlineKfCount)

	m.infoLog.Println("HandleDisconnect() offline MgUserId=", value.MgUserId, "nickname=", value.MgNickName, ",OnlineKfCount=", totalOnline)

	err = m.NotifyOtherKf(m.mrouter, s, value, "offline", &map[string]int32{"total_online": totalOnline})

	if err != nil {
		m.infoLog.Println("NotifyOtherKf() err=", err)
	}

	return nil
}

//init that serviceid => Kfid  maps
func (m *KfGroup) InitKfServiceMap() error {

	rows, err := m.Mgdb.Query("SELECT  mg_user_id,service_id from mg_kf_service_map WHERE `status`=1")

	if err != nil {
		return err
	}

	if err == sql.ErrNoRows {
		return errors.New("InitKfServiceMap()  no data ")
	}

	defer rows.Close()
	m.KfIdServiceLock.Lock()
	defer m.KfIdServiceLock.Unlock()
	// 清空KfIdServiceIdMap
	m.KfIdServiceIdMap = make(map[uint32][]uint32)

	for rows.Next() {
		var mg_user_id uint32
		var service_id uint32
		if err := rows.Scan(&mg_user_id, &service_id); err != nil {
			m.infoLog.Printf("InitKfServiceMap() mg_kf_service_map rows.Scan failed")
			continue
		}

		if service_id > 0 {
			_, ok := m.KfIdServiceIdMap[service_id]
			if !ok {
				m.KfIdServiceIdMap[service_id] = []uint32{}
				m.infoLog.Printf("InitKfServiceMap() kf_service_map mg_user_id=%d service_id=%d set", mg_user_id, service_id)
				//continue
			}
			m.KfIdServiceIdMap[service_id] = append(m.KfIdServiceIdMap[service_id], mg_user_id)
		}

		//expireList = append(expireList, userId)
	}

	m.infoLog.Printf("InitKfServiceMap() mg_kf_service_map=%v \n", m.KfIdServiceIdMap)

	m.InitRouteHandler()

	return nil
}

func (m *KfGroup) GetClientIP(req *http.Request) string {
	var ip string
	xForward := req.Header.Get("X-Forwarded-For")
	// m.infoLog.Printf("getClientIP X-Forwarded-For : %s", xForward)

	if xForward != "" {
		splice := strings.Split(xForward, ",")
		ip = strings.Trim(splice[0], " ")
	} else {
		splice := strings.Split(req.RemoteAddr, ":")
		ip = splice[0]
	}
	// m.infoLog.Printf("getClientIP return %s", ip)
	return ip
}

func (m *KfGroup) SaveUserMessageToDb(msg *IMMsgPack, service_id uint32, mg_user_id uint32) error {

	msgIntType := GetMessageType(msg.Msg.MsgType)

	// 不支持的消息类型
	if msgIntType == MT_UNKNOWN {
		return errors.New("msgType not support=" + msg.Msg.MsgType)
	}
	// 应该被忽略不处理的类型：公众号通知
	if msgIntType == MT_NOTIFY {
		return &IgnoreMsgTypeErr{msg.Msg.MsgType}
	}

	head := msg.Header
	user_id := head.From
	MsgBody := msg.Msg

	ServerTime := ""
	if MsgBody.ServerTime != "" {
		ServerTime = MsgBody.ServerTime
		m.infoLog.Printf("MsgBody.ServerTime %+v", ServerTime)
	} else {
		tm := time.Unix(int64(MsgBody.ServerTs/1000), 0)
		m.infoLog.Printf("MsgBody.ServerTs int %+v", MsgBody.ServerTs)
		ServerTime = tm.UTC().Format("2006-01-02 15:04:05")
		m.infoLog.Printf("MsgBody.ServerTs %+v", ServerTime)
	}

	textContent, err := GetMessageContentJson(MsgBody, msgIntType)
	// m.infoLog.Printf("GetMessageContentJson msgIntType : %+v MsgBody : %+v \ntextContent:%+v", msgIntType, MsgBody, string(textContent))

	if err != nil {
		return fmt.Errorf("GetMessageContentJson error =%s", err.Error())
	}
	msg_id_src := fmt.Sprintf("%d_%d_%s", head.From, head.To, MsgBody.MsgID)
	msg_id := common.Crc64(msg_id_src)

	var version uint32
	version = 0
	// 查询用户的版本
	if mg_user_id == 0 {
		userState, err := m.mcUserState.GetUserOnlineStat(head.From)
		if err != nil {
			m.infoLog.Printf("GetUserOnlineStat %d failed: %+v", head.From, err)
		} else {
			// m.infoLog.Printf("GetUserOnlineStat : %+v", userState)
			version = userState.Version
		}
	}

	// 把数据写入 mg_customer_p2p
	_, err = m.Mgdb.Exec("INSERT INTO mg_customer_p2p(fromid,toid,msg_id,msg_src,type,mg_user_id, version, message,sendtime) "+
		" VALUES(?,?,?,?,?,?,?,?,?)",
		head.From,
		head.To,
		fmt.Sprintf("%d", msg_id),
		head.TermType,
		msgIntType,
		mg_user_id,
		version,
		textContent,
		ServerTime)

	if err != nil {

		mysqlerr, ok := err.(*mysql.MySQLError)

		m.infoLog.Println("SaveUserMessageToDb() save err", mg_user_id, service_id, err)

		//如果数据库里面存在了,忽视。
		if ok && mysqlerr.Number == uint16(1062) {
			return nil
		}

		return fmt.Errorf("insert mg_customer_p2p faield,From=%d To=%d,msg_id=%d error=%s", head.From, head.To, msg_id, err.Error())
	}

	// 更新 mg_customer_user_list
	from_count := 1
	reply_count := 0

	if mg_user_id > 0 {
		user_id = head.To
		reply_count = 1
		from_count = 0

		_, err = m.Mgdb.Exec("INSERT INTO mg_customer_user_list(userid,service_id,from_count,reply_count,updatetime,msg_nonread_num,ostype,nativelang,nation,acked) "+
			" VALUES(?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE from_count=VALUES(from_count)+from_count,reply_count=VALUES(reply_count)+reply_count",
			user_id,
			service_id,
			from_count,
			reply_count,
			ServerTime,
			from_count,
			head.TermType,
			0,
			0,
			0,
		)

		if err != nil {
			return fmt.Errorf("insert mg_customer_user_list faield From=%d To=%d error=%s", head.From, head.To, err.Error())
		}

		// 客服发送的消息还需要记录在操作日志里面
		var ip string
		if mg_user_id > 0 {
			session := m.ManagedKfIdGroup[mg_user_id] // 先获取IP
			userInfo, err := m.getMgUserInfoFromSession(session)
			if err != nil {
				return fmt.Errorf("getMgUserInfoFromSession failed when logging operation")
			}
			ip = userInfo.IP
		} else {
			ip = "127.0.0.1"
		}

		description := fmt.Sprintf("[content]:%s[post]:[type]:%s[push]:%s", textContent, MsgBody.MsgType, MsgBody.Push)
		_, err = m.Mgdb.Exec("INSERT INTO mg_log_operation(OPERATOR, USERID, ACTION, DESCRIPTION, IP, DOTIME) VALUES (?,?,?,?,?,?)",
			msg.MgNickName,
			user_id,
			"replyMessage",
			description,
			ip,
			ServerTime,
		)

		if err != nil {
			return fmt.Errorf("insert mg_log_operation faield From=%d To=%d error=%s", head.From, head.To, err.Error())
		}

	} else {

		_, err = m.Mgdb.Exec("INSERT INTO mg_customer_user_list(userid,service_id,from_count,reply_count,updatetime,msg_nonread_num,ostype,nativelang,nation,acked,last_message_type,last_message) "+
			" VALUES(?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE service_id=VALUES(service_id),from_count=VALUES(from_count)+from_count,reply_count=VALUES(reply_count)+reply_count,msg_nonread_num=VALUES(msg_nonread_num)+msg_nonread_num,updatetime=VALUES(updatetime),ostype=VALUES(ostype),last_message_type=VALUES(last_message_type),last_message=VALUES(last_message),acked=0",
			user_id,
			service_id,
			from_count,
			reply_count,
			ServerTime,
			from_count,
			head.TermType,
			0,
			0,
			0,
			msgIntType,
			textContent,
		)

		if err != nil {
			return fmt.Errorf("insert mg_customer_user_list faield From=%d To=%d error=%s", head.From, head.To, err.Error())
		}
	}

	return nil

}

func (m *KfGroup) SaveNewSession(s *melody.Session) error {

	value, err := m.getMgUserInfoFromSession(s)
	if err != nil {
		return err
	}

	oldConn, ok := m.ManagedKfIdGroup[value.MgUserId]

	if ok && oldConn != nil {

		msgtip := fmt.Sprintf("login on=%s,with broser=%s", s.Request.RemoteAddr, s.Request.Header["User-Agent"])

		m.NotifyOtherKf(nil, oldConn, value, "loged_another_place", msgtip)
		oldConn.Set("mguserinfo", nil)
		m.KfGroupLock.Lock()
		delete(m.ManagedKfIdGroup, value.MgUserId)
		m.KfGroupLock.Unlock()

		atomic.AddInt32(&m.OnlineKfCount, -1)

		oldConn.Close()

		value.ReLogin = 1
		s.Set("mguserinfo", value)

		//time.Sleep(time.Microsecond * 2000)
		m.infoLog.Println("close old connect MgUserId and relogin", value.MgUserId)
	}

	m.KfGroupLock.Lock()
	defer m.KfGroupLock.Unlock()

	//return nil, fmt.Errorf("error: ManagedKfIdGroup not exist MgUserId=%d\n", value.MgUserId)
	all_kf := []*SimpleMgUserInfo{}

	for _, v := range m.ManagedKfIdGroup {
		op, err := m.getMgUserInfoFromSession(v)
		if op != nil && err == nil {
			all_kf = append(all_kf, op)
		}
	}
	//把不在管理器里面的用户加入管理
	//同时告诉当前登录的用户，目前有哪些用户登陆了。
	m.NotifyOtherKf(nil, s, value, "all_online_kf", all_kf)

	m.ManagedKfIdGroup[value.MgUserId] = s

	atomic.AddInt32(&m.OnlineKfCount, 1)

	totalOnline := atomic.LoadInt32(&m.OnlineKfCount)
	m.infoLog.Println("HandleConnect() login MgUserId=", value.MgUserId, "nickname=", value.MgNickName, ",OnlineKfCount=", totalOnline)

	if value.ReLogin == 1 {

		m.infoLog.Println("HandleConnect() relogin MgUserId=", value.MgUserId, ",dont notify other kf")

	}

	err = m.NotifyOtherKf(m.mrouter, s, value, "online", &map[string]int32{"total_online": totalOnline})
	if err != nil {
		m.infoLog.Println("NotifyOtherKf() err=", err)
	}
	return nil
}

//handler message from users,mapId means service id
func (m *KfGroup) OnUserMessage(immsg *IMMsgPack, service_id uint32, mg_user_id uint32) error {

	// 保存到数据库
	err := m.SaveUserMessageToDb(immsg, service_id, mg_user_id)

	// m.infoLog.Printf(fmt.Sprintf("OnUserMessage immsg: %+v", immsg.Msg))

	//如果消息是来自客服号发出去的，那么就server_id 反转。
	if service_id > 20000 && immsg.Header.From < 20000 {
		service_id = immsg.Header.From
	}

	if err != nil {

		// 如果是忽略不处理的消息类型
		if ignore, ok := err.(*IgnoreMsgTypeErr); ok {
			m.infoLog.Printf("OnUserMessage IgnoreMsgTypeErr msg type : %+v", ignore)
			return nil
		}

		notifyStr := "save message to db fail error=" + err.Error()
		var err2 error
		if mg_user_id > 0 {
			err2 = m.NotifyKfWithId(mg_user_id, "error", notifyStr)
		} else {
			err2 = m.NotifyKfWithServiceId(service_id, "error", notifyStr)
		}

		if err2 != nil {
			m.infoLog.Printf("OnUserMessage() NotifyKf fail, service_id=%d mg_user_id=%d,err=%s", service_id, mg_user_id, err.Error())
		}

		m.infoLog.Printf("OnUserMessage() SaveUserMessageToDb fail, service_id=%d mg_user_id=%d,err=%s", service_id, mg_user_id, err.Error())
	}

	wsPack, err := immsg.ToWSMsgPack()

	if err != nil {
		return errors.New("ToWSMsgPack() err=" + err.Error())
	}

	json, err := json.Marshal(wsPack)

	if err != nil {
		return errors.New("Marshal() err=" + err.Error())
	}

	m.infoLog.Printf("OnUserMessage() mg_user_id=%d,json msg=%s\n", mg_user_id, string(json), err)

	err = m.BroadCastWithServiceIdOther(service_id, json, func(mg_id uint32) bool {
		return mg_id != mg_user_id
	})

	return err
}

//notify something to kf that belong to this service_id
func (m *KfGroup) NotifyKfWithServiceId(service_id uint32, Type string, Data interface{}) error {

	notifyObj := &NotifyPack{
		Data:       Data,
		Type:       Type,
		MgUserId:   0,
		MgNickName: "me",
	}

	//通知其他人客服上线了。
	notifyJson, err1 := json.Marshal(notifyObj)
	if err1 != nil {
		return err1
	}

	return m.BroadCastWithServiceIdOther(service_id, notifyJson, nil)
}

//通知 service_id 绑定的客服人员，通过fn 过滤 要处理的 不发送的
func (m *KfGroup) BroadCastWithServiceIdOther(service_id uint32, json []byte, fn filterFunc) error {

	kfIds, ok := m.KfIdServiceIdMap[service_id]

	if !ok {
		return fmt.Errorf("error,no kf to handle this service = %d", service_id)
	}

	m.KfGroupLock.Lock()
	defer m.KfGroupLock.Unlock()
	for _, v := range kfIds {

		//消息转发的到其他客服的时候，不能再发给自己
		//只发给有权限接收该条消息的 kf

		if fn != nil && !fn(v) {
			continue
		}

		sock, ok := m.ManagedKfIdGroup[v]
		if ok {

			wrierr := sock.Write(json)
			if wrierr != nil {
				m.infoLog.Printf("BroadCastWithServiceIdOther() send message json=%s error=%s", string(json), wrierr.Error())
			}
		} else {
			m.infoLog.Printf("BroadCastWithServiceIdOther() mg_user_id=%d not loged in,service_id=%d", v, service_id)
		}
	}

	return nil
}

//notify to all kf
func (m *KfGroup) NotifyToAllKf(Type string, Data interface{}) error {

	notifyObj := &NotifyPack{
		Data:       Data,
		Type:       Type,
		MgUserId:   0,
		MgNickName: "",
	}

	notifyJson, err1 := json.Marshal(notifyObj)
	if err1 != nil {
		return err1
	}

	return m.mrouter.Broadcast(notifyJson)

}

//notify only One user with kfId
func (m *KfGroup) NotifyKfWithId(kfId uint32, Type string, Data interface{}) error {

	m.KfGroupLock.Lock()
	defer m.KfGroupLock.Unlock()

	sock, ok := m.ManagedKfIdGroup[kfId]
	if ok {

		notifyObj := &NotifyPack{
			Data:       Data,
			Type:       Type,
			MgUserId:   kfId,
			MgNickName: "me",
		}

		//通知其他人客服上线了。
		notifyJson, err1 := json.Marshal(notifyObj)
		if err1 != nil {
			return err1
		}
		return sock.Write(notifyJson)

	} else {
		return fmt.Errorf("kfId=%d not in the group", kfId)
	}

}

func (m *KfGroup) Close() error {

	return m.mrouter.Close()

}

func (m *KfGroup) InitInformUser(strInformUserid string) error {

	if len(strInformUserid) == 0 {
		return nil
	}

	informUseridList := strings.Split(strInformUserid, ",")
	for _, useridStr := range informUseridList {
		informUserid, err := strconv.Atoi(useridStr)
		if err != nil {
			m.infoLog.Printf("Parse strInformUserid failed %s", useridStr)
			return err
		}
		m.InformUserid = append(m.InformUserid, uint32(informUserid))
	}

	m.infoLog.Printf("InitInformUser success: %+v", m.InformUserid)
	return nil
}

func (m *KfGroup) NotifyOtherKf(r *melody.Melody, s *melody.Session, value *SimpleMgUserInfo, Type string, Data interface{}) error {

	notify := &NotifyPack{
		Data:       Data,
		Type:       Type,
		MgUserId:   0,
		MgNickName: "",
	}

	if value != nil {
		notify.MgUserId = value.MgUserId
		notify.MgNickName = value.MgNickName
	}

	//通知其他人客服上线了。
	notifyJson, err1 := json.Marshal(notify)

	if err1 != nil {
		return err1
	} else {
		//通知除了自己以外的其他客服
		//如果要通知所有人，s 传nil
		if r != nil {
			return r.BroadcastOthers(notifyJson, s)
		} else {
			if s != nil {
				return s.Write(notifyJson)
			}
		}

	}

	return nil
}

func (m *KfGroup) GetUserInfo(userid uint32) (jsonObj *simplejson.Json, err error) {
	key := common.GetUserInfoRedisKey(userid)
	val, err := m.userinfoRd.Get(key)
	if err != nil {
		return nil, err
	}

	jsonObj, err = simplejson.NewJson([]byte(val))
	if err != nil {
		return nil, err
	}
	return jsonObj, nil
}

// 判断用户是不是VIP,如果是VIP,自动打上vip的标签
func (m *KfGroup) CheckVIPTag(userid uint32) error {
	// m.infoLog.Printf("CheckVIPTag called TaggedVIP=%v userid=%d", m.TaggedVIP, userid)
	nowTs := uint64(time.Now().Unix())
	// 如果缓存中已存在该用户VIP标签的记录
	if _, ok := m.TaggedVIP[userid]; ok {
		// m.infoLog.Printf("CheckVIPTag userid=%v already in cache", userid)
		m.TaggedVIP[userid] = nowTs
		return nil
	}
	// 查该用户的信息
	userInfo, err := m.GetUserInfo(userid)
	if err != nil {
		m.infoLog.Printf("CheckVIPTag GetUserInfo err:%+v", err)
		return err
	}

	// 用户信息的json结构里，VIP2这个字段有时候是字符串，有时候是整形
	var VIP2 uint64
	if VipStr, err := userInfo.Get("VIP2").String(); err == nil {
		// m.infoLog.Printf("CheckVIPTag userInfo.Get(VIP2) type string")
		VIP2, err = strconv.ParseUint(VipStr, 10, 64)
		if err != nil {
			m.infoLog.Printf("CheckVIPTag ParseUint fail VipStr=%s", VipStr)
			return err
		}
	} else {
		// m.infoLog.Printf("CheckVIPTag userInfo.Get(VIP2) type int")
		VIP2 = userInfo.Get("VIP2").MustUint64(0)
	}
	// m.infoLog.Printf("CheckVIPTag GetUserInfo VIP2=%d nowTs=%d", VIP2, nowTs)
	if VIP2 == 0 || nowTs > VIP2 {
		// 该用户不是VIP，或者会员已过期
		return nil
	}

	// 在数据库查一下该VIP在是否已经打过标签
	var storedIsTag sql.NullInt64
	vipTagId := 114
	err = m.Mgdb.QueryRow("SELECT COUNT(*) FROM mg_customer_user_tag WHERE userid=? AND tag_id=?", userid, vipTagId).Scan(&storedIsTag)
	m.infoLog.Printf("CheckVIPTag Query Tag existed storedIsTag=%v err=%v", storedIsTag.Int64, err)

	if storedIsTag.Int64 == 0 {
		_, err = m.Mgdb.Exec("INSERT INTO mg_customer_user_tag (userid, tag_id, mg_user_id, updatetime) VALUES (?,?,0,UTC_TIMESTAMP() )", userid, vipTagId)
		if err != nil {
			m.infoLog.Printf("CheckVIPTag save user_tag fail : userid=%v tag_id=%v", userid, vipTagId)
			return err
		}
	}

	// 写入到缓存，如果缓存满了，需要先清一半
	if len(m.TaggedVIP) >= VIP_CACHE_CAP {
		m.ClearVipCache()
	}

	m.TaggedVIP[userid] = nowTs
	return nil
}

func (m *KfGroup) ClearVipCache() {
	m.infoLog.Printf("ClearVipCache called")
	// 先给整个map排序
	// 定义一个结构体存储kv
	type kv struct {
		Key   uint32
		Value uint64
	}

	var ss []kv
	for k, v := range m.TaggedVIP {
		ss = append(ss, kv{k, v})
	}

	sort.Slice(ss, func(i, j int) bool {
		return ss[i].Value < ss[j].Value
	})

	TaggedVIP := make(map[uint32]uint64)
	for _, kv := range ss[VIP_CACHE_CAP/2:] {
		m.infoLog.Printf("%d, %d\n", kv.Key, kv.Value)
		TaggedVIP[kv.Key] = kv.Value
	}
	m.TaggedVIP = TaggedVIP
}
