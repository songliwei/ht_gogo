package org

import (
	"errors"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

// Error type
var (
	ErrConnClosing   = errors.New("use of closed network connection")
	ErrWriteBlocking = errors.New("write packet was blocking")
	ErrReadBlocking  = errors.New("read packet was blocking")
)

type Packet interface {
	Serialize() []byte
}

type Protocol interface {
	ReadPacket(conn *net.TCPConn) (Packet, error)
}

// Conn exposes a set of callbacks for the various events that occur on a connection
type HTConn struct {
	host              string        // the raw connection
	conn              *net.TCPConn  // the raw connection
	extraData         interface{}   // to save extra data
	closeOnce         sync.Once     // close the conn, once, per instance
	closeFlag         int32         // close flag
	closeChan         chan struct{} // close chanel
	packetSendChan    chan Packet   // packet send chanel
	packetReceiveChan chan Packet   // packeet receive chanel

	exitChan  chan struct{}   // notify all goroutines to shutdown
	waitGroup *sync.WaitGroup // wait for all goroutines
	callback  ConnCallback
	protocol  Protocol
}

type TcpConfig struct {
	PacketSendChanLimit    uint32 // the limit of packet send channel
	PacketReceiveChanLimit uint32 // the limit of packet receive channel
}

// ConnCallback is an interface of methods that are used as callbacks on a connection
type ConnCallback interface {
	// OnConnect is called when the connection was accepted,
	// If the return value of false is closed
	OnConnect(*HTConn) bool

	// OnMessage is called when the connection receives a packet,
	// If the return value of false is closed
	OnMessage(*HTConn, Packet) bool

	// OnClose is called when the connection closed
	OnClose(*HTConn, string)
}

// newConn returns a wrapper of raw conn
func newHTConn(host string, config TcpConfig, callback ConnCallback, protocol Protocol) *HTConn {

	return &HTConn{
		callback:          callback,
		protocol:          protocol,
		host:              host,
		closeChan:         make(chan struct{}),
		packetSendChan:    make(chan Packet, config.PacketSendChanLimit),
		packetReceiveChan: make(chan Packet, config.PacketReceiveChanLimit),
		waitGroup:         &sync.WaitGroup{},
	}
}

// GetExtraData gets the extra data from the Conn
func (c *HTConn) GetExtraData() interface{} {
	return c.extraData
}

// PutExtraData puts the extra data with the Conn
func (c *HTConn) PutExtraData(data interface{}) {
	c.extraData = data
}

// GetRawConn returns the raw net.HTConn from the Conn
func (c *HTConn) GetRawConn() *net.TCPConn {
	return c.conn
}

// Close closes the connection
func (c *HTConn) Close(src string) {
	c.closeOnce.Do(func() {
		atomic.StoreInt32(&c.closeFlag, 1)
		close(c.closeChan)
		close(c.packetSendChan)
		close(c.packetReceiveChan)
		c.conn.Close()
		c.callback.OnClose(c, src)
	})

	c.waitGroup.Wait()
}

// IsClosed indicates whether or not the connection is closed
func (c *HTConn) IsClosed() bool {
	return atomic.LoadInt32(&c.closeFlag) == 1
}

// AsyncWritePacket async writes a packet, this method will never block
func (c *HTConn) AsyncWritePacket(p Packet, timeout time.Duration) (err error) {
	if c.IsClosed() {
		return ErrConnClosing
	}

	defer func() {
		if e := recover(); e != nil {
			err = ErrConnClosing
		}
	}()

	if timeout == 0 {
		select {
		case c.packetSendChan <- p:
			return nil

		default:
			return ErrWriteBlocking
		}

	} else {
		select {
		case c.packetSendChan <- p:
			return nil

		case <-c.closeChan:
			return ErrConnClosing

		case <-time.After(timeout):
			return ErrWriteBlocking
		}
	}
}

// Do it
func (c *HTConn) Do() error {

	tcpAddr, err := net.ResolveTCPAddr("tcp4", c.host)

	conn, err := net.DialTCP("tcp", nil, tcpAddr)

	if err != nil {
		return err
	}

	//	conn.SetReadDeadline(time.Now().Add(time.Minute * 2))

	c.conn = conn

	if !c.callback.OnConnect(c) {
		return errors.New("Connect fail")
	}

	asyncDo(c.handleLoop, c.waitGroup)
	asyncDo(c.readLoop, c.waitGroup)
	asyncDo(c.writeLoop, c.waitGroup)

	return nil
}

func (c *HTConn) ReadDataFromConn() (Packet, error) {
	p, err := c.protocol.ReadPacket(c.conn)
	return p, err
}

func (c *HTConn) WritePackToConn(p Packet) error {

	_, err := c.conn.Write(p.Serialize())

	return err

}

func (c *HTConn) readLoop() {
	src := "readLoop exit"
	defer func() {
		recover()
		c.Close(src)
	}()

	for {
		select {
		case <-c.exitChan:
			return

		case <-c.closeChan:
			return

		default:
		}

		p, err := c.protocol.ReadPacket(c.conn)
		if err != nil {
			src = "readLoop ReadPacket err=" + err.Error()
			return
		}

		c.packetReceiveChan <- p
	}
}

func (c *HTConn) writeLoop() {
	defer func() {
		recover()
		c.Close("writeLoop")
	}()

	for {
		select {
		case <-c.exitChan:
			return

		case <-c.closeChan:
			return

		case p := <-c.packetSendChan:
			if c.IsClosed() {
				return
			}
			if _, err := c.conn.Write(p.Serialize()); err != nil {
				return
			}
		}
	}
}

func (c *HTConn) handleLoop() {
	src := "handleLoop exit"
	defer func() {
		recover()
		c.Close(src)
	}()

	for {
		select {
		case <-c.exitChan:
			src = "handleLoop exitChan trigger"
			return

		case <-c.closeChan:
			src = "handleLoop closeChan trigger"
			return

		case p := <-c.packetReceiveChan:
			if c.IsClosed() {
				src = "handleLoop close already"
				return
			}
			if !c.callback.OnMessage(c, p) {
				src = "handleLoop OnMessage fail"
				return
			}
		}
	}
}

func asyncDo(fn func(), wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		fn()
		wg.Done()
	}()
}
