package org

import (
	"encoding/binary"
	"errors"
	//"fmt"
	"github.com/HT_GOGO/gotcp/libcrypto"
	//"io"
	//"net"
)

const (
	XTHeadLen       = 20
	PacketXTHeadLen = 20
	EmptyPacktXTLen = 20
	PacketXTLimit   = 128 * 1024
)

// Error type
var (
	XTErrShortLen = errors.New("use byte[] lne is not enough")
	XTErrLenErr   = errors.New("Len error")
)

type XTHead struct {
	Flag      uint8  `json:"-"`                       // 0xF0客户端请求，0xF1 服务器应答, 0xF2  服务器主动发包, 0xF3  客户端应答, 0xF4 服务器之间的包
	Version   uint8  `json:"-"`                       // 版本号  VER_MMEDIA = 4
	CryKey    uint8  `json:"-"`                       // 加密类型  E_NONE_KEY = 0, E_SESSION_KEY = 1, E_RAND_KEY = 2, E_SERV_KEY= 3
	TermType  uint8  `json:"re"`                      // 终端类型
	Cmd       uint16 `json:"cmd" binding:"required"`  // 命令字
	Seq       uint16 `json:"id" binding:"required"`   // 序列号
	From      uint32 `json:"from" binding:"required"` // uint32_t uiFrom
	To        uint32 `json:"to" binding:"required"`   // 目的UID TO_SERVER = 0
	Len       uint32 `json:"-"`                       // PayLoad的总长度不包含包头
	IsOffline uint8  `json:"-"`                       // 包是否来自离线的业务
}

func NewXTHead(buf []byte) (head *XTHead, err error) {
	if len(buf) < XTHeadLen {
		return nil, XTErrLenErr
	}
	head = new(XTHead)
	head.Flag = buf[0]
	head.Version = buf[1]
	head.CryKey = buf[2]
	head.TermType = buf[3]
	head.Cmd = binary.LittleEndian.Uint16(buf[4:6])
	head.Seq = binary.LittleEndian.Uint16(buf[6:8])
	head.From = binary.LittleEndian.Uint32(buf[8:12])
	head.To = binary.LittleEndian.Uint32(buf[12:16])
	head.Len = binary.LittleEndian.Uint32(buf[16:20])
	return head, nil
}

func (this *XTHead) CheckXTHeaderValid() error {

	if this.Version != 0x04 {
		return errors.New("Version error")
	}

	if this.Len > 65536 {
		return errors.New("Len error")
	}
	if this.CryKey > 4 {
		return errors.New("CryKey error")
	}

	if this.From > 8309181*10 || this.To > 8309181*10 {
		return errors.New(" fromid or toid error")
	}

	if this.Cmd > 0x9999 {
		return errors.New(" Cmd error")
	}

	return nil
}

func SerialXTHeadToSlice(head *XTHead, buf []byte) (err error) {
	if len(buf) < XTHeadLen {
		return XTErrShortLen
	}
	buf[0] = head.Flag
	buf[1] = head.Version
	buf[2] = head.CryKey
	buf[3] = head.TermType
	binary.LittleEndian.PutUint16(buf[4:6], head.Cmd)
	binary.LittleEndian.PutUint16(buf[6:8], head.Seq)
	binary.LittleEndian.PutUint32(buf[8:12], head.From)
	binary.LittleEndian.PutUint32(buf[12:16], head.To)
	binary.LittleEndian.PutUint32(buf[16:20], head.Len)
	return nil
}

// XTHead 格式如下
// XTHead + payload

type XTHeadPacket struct {
	Header *XTHead
	Body   []byte
}

func (this *XTHeadPacket) Serialize() []byte {

	buff := make([]byte, XTHeadLen+this.Header.Len)

	SerialXTHeadToSlice(this.Header, buff)

	copy(buff[XTHeadLen:], this.Body)
	return buff
}

func (this *XTHeadPacket) CheckXTPacketValid() (bool, error) {
	if this.Header.Len != uint32(len(this.Serialize())-XTHeadLen) {
		return false, XTErrLenErr
	}

	return true, nil
}

func NewXTHeadPacket(head *XTHead, buff []byte) *XTHeadPacket {
	p := &XTHeadPacket{}
	p.Header = head
	p.Body = buff
	return p
}

func NewXTHeadPacketWithParam(from uint32, to uint32, cmd uint16, CryKey uint8, Seq uint16, key string, buff []byte) *XTHeadPacket {

	p := &XTHeadPacket{}

	BodyLen := uint32(len(buff))
	if BodyLen > 0 && (CryKey == E_RAND_KEY || CryKey == E_SESSION_KEY) {

		rest := libcrypto.TEAEncrypt(string(buff), key)
		//fmt.Println("rest:", libcrypto.TEADecrypt(rest, key))
		if CryKey == E_RAND_KEY {
			p.Body = make([]byte, len(rest)+16)
			copy(p.Body, []byte(key))
			copy(p.Body[16:], []byte(rest))
		} else {

			p.Body = []byte(rest)
			//copy(p.Body, )
		}

		BodyLen = uint32(len(p.Body))
	} else {
		p.Body = buff
	}

	p.Header = &XTHead{
		Flag:     0xF0,
		Version:  0x04,
		CryKey:   CryKey,
		TermType: 0,
		Cmd:      cmd,
		Seq:      Seq,
		From:     from,
		To:       to,
		Len:      BodyLen,
	}

	return p
}

func UnMarshalSliceWithOutZero(packet *[]byte) (value []byte) {
	if len(*packet) < 2 {
		return nil
	}

	length := uint16((*packet)[0]) | uint16((*packet)[1])<<8
	if length == 0 { // empty slice
		*packet = (*packet)[2:]
		return nil
	}

	// 删除value中的'\0'
	payLoadLen := length
	value = make([]byte, payLoadLen)
	copy(value, (*packet)[2:]) // 跳过长度 2字节
	// 使用2+length 自动跳过'\0'
	*packet = (*packet)[length+2:]
	return
}
