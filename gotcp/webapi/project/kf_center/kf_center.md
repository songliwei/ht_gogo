
# 客服中心系统的设计说明
    2017-09-12
    李凌
    

 

## 基本流程：
    
    IMServer -> kf_center -> WebSocket
                |
                |--> DataBase(HT_MGDB.mg_customer_p2p)
                      同时更新 HT_MGDB.mg_customer_user_list
                |
    WebSocket -> kf_center  ->   IMServer    

## 角色说明
    IMServer：C++ IM服务器
    WebSocket：前端JS模块
    kf_center：golang所写的消息中间件模块，负责消息转发和链接 关系 管理

## 客服中心代码接口和数据流向

    IMClient.OnMessage [客服中心收到用户消息，To 表示service_id]
            |
    GlobalRecvMsgPackChan [接收IM消息的队列]
            |
    kfGroup.OnUserMessage [接收处理消息]  -> SaveUserMessageToDb(存储消息到DB)
            |
    kfGroup.BroadCastWithServiceIdOther [分发消息到已经连上的客服]
            |
          Write

    kfGroup.OnMessage [收到后台客服的消息，From 表示service_id]]
            |
    GlobalSendMsgPackChan [收到WebSocket消息的队列]      
            |
       SendMessageToUser  [做消息报文的转义WSMsgPack->IMMsgPack] 
            |
            |----> IMClient.SendMsgPack [消息转发到用户]
            |
       kfGroup.OnUserMessage [消息转到其他客服]->SaveUserMessageToDb(存储到DB)


 

## IMClient的设计
    
    包括一个完整的IM客户端的连接管理，超时重试机制，自动重连机制， 自动加解密
    依赖的 HTConn 作为连接管理。


IMClient 会暴露三个接口回调函数作为和外部通讯的接口
    
> * HandleMessage，作为消息的消息处理函数，只接受和处理0x4001的消息命令字
> * HandleClosed，告诉外界连接断开了。在线的客服会收到一个错误通知。
> * HandleReConnected，告诉外界重连成功了 
 


## KfGroup的设计
    
    包括对websocket连接和用户id的映射管理，
    同时也管理 service_id -> kfid 的映射管理 
    KfIdServiceIdMap map[uint32][]uint32  
    [10000] => {1,2,3} 记录某个服务号由哪些个客服接管消息   
    【数据记录在 HT_MGDB.mg_kf_service_map】
    


## kf_center 协议设计

###下面的消息格式 目前定义下面几种:

  1. typ = msg【消息格式】
  2. typ = online【其他客服的上线通知】
  3. typ = offline【其他客服的下线通知】
  4. typ = notify【一般的notify，前端做提示通知就好了，】
  5. typ = error【error通知，前端也有要做相应的提示】
  6. typ = all_online_kf【连接成功之后，会返回前所有在线的客服列表】
  7. typ = chatting 
    【如果kf打开了某个用户[to描述用户id]的对话框，则发送这个类型的信息，广播给其他用户。其他用户要维护一个字典 kfid=>user_id,下线or切换要更新该字典】
  8. type= tagging  如果kf标记了某个用户[to描述用户id]，发送typ=tagging的消息到服务端，同时会广播给其他用户，其他用户data={tag_id:3,name:"暴力"}


#### 消息格式  [Server => Client or Client => Server]
```
{
    "re" : 0, //消息来源 0=iOS，1=安卓，2=Web端
    "cmd" : 16385, //命令字 
    "id" : 28332,  //协议报文的ID
    "from" : 900866, //消息来自于
    "to" : 42900,   //消息发送给
    "mg_user_id" : 0, //消息是不是后台客服发出去的，如果大于0 表示消息是来自于其他客服，发送方不填充此字段
    "mg_nickname" : "", //发消息的客服名字，发送方不填充此字段
    "typ" : "msg", //报文的类型
    "data" : {
        "from_nickname" : "Lee.Lien", //用户的昵称
        "from_profile_ts" : 1504882809, //用户的profile 时间戳
        "msg_id" : "3c8605e80d457bc0d3t30x47d1505197480359877", //消息的id，作为消息去重的机制
        "msg_model" : "normal", //固定值，不用理他，构造消息的也固定为normal
        "msg_type" : "text",   //消息的类型
        "server_time" : "2017-09-12 06:24:40", //消息的发送时间，忽视本字段
        "server_ts" : 1505197480428, //和上面一样，消息的发送时间戳，发出的消息，忽视本字段
        "text" : {   //消息字段
            "text" : "参观团！"
        }
    }
}
```

#### 对话框打开通知 [Server => Client or Client => Server]
```
{
    "re" : 0,
    "cmd" : 0, 
    "id" : 28332, 
    "to" : 900866,   //打开了谁的对话框
    "mg_user_id" : 1, //消息是不是后台客服发出去的，if>0 表示消息是来自于其他客服，发送方不填充此字段
    "mg_nickname" : "Yiwan", //发消息的客服名字，发送方不填充此字段
    "typ" : "chatting",
    "data" : {
        "tag_id" : 3, 
        "name" : "暴力",  
    }
}
```

#### 打标签通知 [Server => Client]
```
{
    "re" : 0, //
    "cmd" : 0, //
    "id" : 28332,  //
    "to" : 900866,   //给谁打了标签
    "mg_user_id" : 0, //消息是不是后台客服发出去的，if>0 表示消息是来自于其他客服，发送方不填充此字段
    "mg_nickname" : "", //发消息的客服名字，发送方不填充此字段
    "typ" : "tagging", //报文的类型
    "data" : {
        "tag_id" : 3, 
        "name" : "暴力",  
    }
}
```


#### 上线通知 [Server => Client]
```
{
    "data" : {
        "total_online" : 2  //描述当前总共几个客服在线
    },
    "typ" : "online",  //类型是 上线通知，通知某个客服上线了
    "from" : 101,      //上线了的客服id
    "id" : 0,
    "mg_nickname" : "yiwan"    //上线了的客服名称
}
```

#### 下线通知 [Server => Client]
```
{
    "data" : {
        "total_online" : 2  //描述当前总共几个客服在线
    },
    "typ" : "offline",  //类型是 上线通知，通知某个客服上线了
    "from" : 101,      //上线了的客服id
    "id" : 0,
    "mg_nickname" : "yiwan"    //上线了的客服名称
}
```
 
### 错误 or 通知 [Server => Client]
```
{
    "data" : "this is a error or notify from kf_center",
    "typ" : "error",  //类型是 通知 
    "from" : 101,      //上线了的客服id
    "id" : 0,
    "mg_nickname" : ""    
}
```

---

### 后台提供接口

1：鉴权接口 【kg_center进程做为登陆用户的验证】

2：用户列表【最近发消息没被处理的用户】

3：客服列表 【默认回复消息的时候以用户发消息的To为准，也可以选某个客服选择】

4：获取用户信息接口 【收到用户消息之后要去获取】

5：获取用户分组信息接口 

6：设置用户标签

7：删除用户标签

8：标记已经处理

9：标签管理【标签的增删改，和分配给某个用户】

10：设置消息已读【前端只有在可视界面才设置为已读】

11：读取历史消息列表，读取表mg_customer_p2p 里面的数据，拼装到 typ = msg 的类型json

### mg_customer_p2p 说明
```sql

CREATE TABLE `mg_customer_p2p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromid` int(11) DEFAULT '0' COMMENT '用户id',
  `toid` int(11) DEFAULT '0' COMMENT '服务账号iD',
  `msg_src` tinyint(11) unsigned DEFAULT '0' COMMENT '消息渠道：0=ios发送，1=android发送，2=web发送,5=从管理后台发送：从XThead的reaserverd 字段获取',
  `msg_id` bigint(11) unsigned DEFAULT '0' COMMENT '消息id的唯一性,保证消息的唯一性',
  `type` tinyint(4) unsigned DEFAULT '0' COMMENT '消息类型 0-text 1ranslate 2mage 3-voice 4ocate  5-introduction 6-voicetext 7-correct 8-sticker 9-doodle 10-voip 11-notify 12-video',
  `version` int(11) unsigned DEFAULT '0' COMMENT '版本 int类型',
  `mg_user_id` int(11) unsigned DEFAULT '0' COMMENT '如果msg_src=5，要记录后台的用户id',
  `nation` tinyint(4) unsigned DEFAULT '0' COMMENT '来自国家，兼容 sphinx 的整型国家',
  `message` text COMMENT '消息内容',
  `sendtime` datetime DEFAULT NULL COMMENT '发送时间，UTC 时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fromid` (`fromid`,`msg_id`),
  KEY `toid` (`toid`),
  KEY `sendtime` (`sendtime`)
) ENGINE=MyISAM AUTO_INCREMENT=343 DEFAULT CHARSET=utf8mb4

```




### mg_customer_user_list 字段说明
```sql
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(20) unsigned NOT NULL COMMENT '用户ID',
  `service_id` int(11) unsigned DEFAULT '0' COMMENT '客服账号',
  `from_count` int(11) DEFAULT '0' COMMENT '发送数量',
  `reply_count` int(11) DEFAULT '0' COMMENT '客服回复消息数量',
  `updatetime` datetime DEFAULT '2001-10-10 00:00:00' COMMENT '最后一次消息时间 UTC 时间 ',
  `msg_nonread_num` int(11) DEFAULT '0',
  `ostype` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '最近一次发消息的机型，0=ios发送，1=android发送，2=web发送',
  `nativelang` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `nation` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '来自国家，兼容 sphinx 的整型国家',
  `acked` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '标记用已经被处理。后台可以提供按钮设置为已经处理完毕', 

```
 
---
 
 
