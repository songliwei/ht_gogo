package main

import (
	"database/sql"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/HT_GOGO/gotcp/webapi/project/kf_center/org"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"strconv"
	"strings"
	//"github.com/garyburd/redigo/redis"
	"encoding/json"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/lilien1010/melody"
	"github.com/nsqio/go-nsq"
	"log"
	"net/http"
	// "os"
	"runtime"
	"time"
)

type KfClient struct {
	Session  *melody.Session
	MgUserID uint32
}

var (
	db                    *sql.DB
	Mgdb                  *sql.DB
	manager_userids       []uint32
	get_userid_url        string
	GlobalRecvMsgPackChan chan *org.IMMsgPack
	GlobalSendMsgPackChan chan *org.WSMsgPack
	infoLog               *log.Logger
	ManagedServiceGroup   map[uint32]*org.IMClient //记录[10000] => client 记录服务号和登陆客户端的映射关系
	kf_group              *org.KfGroup

	options       common.Options
	p2pSvr        *tcpcommon.P2PWorkerApiV2
	mcUserState   *tcpcommon.MemcacheApi
	userinfoRedis *tcpcommon.RedisApi
	// KfIdServiceIdMap map[uint32][]uint32 // [10000] => {1,2,3} 记录某个服务号由哪些个客服接管消息
	// KfIdServiceLock  *sync.Mutex

	// KfGroupLock      *sync.Mutex
	// ManagedKfIdGroup map[uint32]*melody.Session //[2] => websocket  记录客服id的连接
	// OnlineKfCount    int32
)

func NsqMessageHandle(message *nsq.Message) error {

	msgPack := &org.IMMsgPack{}
	err := json.Unmarshal(message.Body, msgPack) // JSON to Struct

	if err != nil {
		infoLog.Printf("NsqMessageHandle err=%s", err.Error())
		return nil
	}

	GlobalRecvMsgPackChan <- msgPack

	return nil
}

//handler that handle message from users  to customer, OnUserMessage() do the real work
func BatchRecvMessageHandler() {

	infoLog.Printf("BatchRecvMessageHandler() start ")

	for {
		p := <-GlobalRecvMsgPackChan

		infoLog.Printf("BatchRecvMessageHandler() p=%#v", p.Msg)

		err := kf_group.CheckVIPTag(p.Header.From)
		if err != nil {
			infoLog.Println("BatchRecvMessageHandler() CheckVIPTag  error=", err)
		}

		err = kf_group.OnUserMessage(p, p.Header.To, 0)
		if err != nil {
			infoLog.Println("BatchRecvMessageHandler() OnUserMessage  error=", err)
		}
	}
}

//handler that handle message from customer to users, SendMessageToUser() do the real work
func BatchSendMessageHandler() {

	for {
		select {
		case p := <-GlobalSendMsgPackChan:
			err := SendMessageToUser(p, p.From)
			if err != nil {

				errNotify := kf_group.NotifyKfWithId(p.MgUserId, "error", "send message error "+err.Error())
				if errNotify != nil {
					infoLog.Println("BatchRecvMessageHandler() NotifyKfWithId  error=", errNotify)
				}
				infoLog.Printf("BatchSendMessageHandler() mg_user_ud=%d,SendMessageToUser error=%s", p.MgUserId, err.Error())
			}
			// 发送成功要返回一个ack的包确认。
			data := make(map[string]string)
			data["msg_id"] = p.Data["msg_id"].(string)
			data["code"] = "0"
			err = kf_group.NotifyKfWithId(p.MgUserId, "ack", data)
			if err != nil {
				infoLog.Println("Notify Ack failed, wsmsg=%+v error=%s", p, err.Error())
			}
		}
	}
}

//send message from custome to users, service_id is the FromId
func SendMessageToUser(wsmsg *org.WSMsgPack, service_id uint32) error {

	infoLog.Printf("SendMessageToUser() service_id=%d ,To=%d,mg_user_id=%d,mg_nickname=%s", service_id, wsmsg.To, wsmsg.MgUserId, wsmsg.MgNickName)
	// infoLog.Printf("SendMessageToUser() wsmsg: %+v", wsmsg)

	im, ok := ManagedServiceGroup[service_id]

	if !ok { // 不存在直接打印日志返回错误
		return fmt.Errorf("service_id:%v not login", service_id)
	}

	err := im.SendMsgPack(wsmsg)

	if err != nil {
		return err
	}

	//消息没有发送成功直接不转发 也不存储
	immsg, err := wsmsg.ToIMMsgPack()

	if err != nil {
		return err
	}

	//客服发的消息也转发给其他客服   msg.From 描述的是 10000 等客服ID
	err = kf_group.OnUserMessage(immsg, service_id, wsmsg.MgUserId)

	if err != nil {
		infoLog.Printf("OnUserMessage() err:", err)
	}

	// 复制一份发给通知用户
	wsmsg.Data["push"] = "none"
	originUserid := wsmsg.To
	for _, infoId := range kf_group.InformUserid {
		if originUserid == infoId {
			continue
		}
		infoLog.Printf("Inform user %d ", infoId)
		wsmsg.To = infoId
		if err := im.SendMsgPack(wsmsg); err != nil {
			infoLog.Printf("Inform user %d failed", infoId)
			return err
		}
	}

	return err
}

//start the bluk login action
func startManagerUserid(im_host string, im_port int, str_manager_userids string) {

	strs_group := strings.Split(str_manager_userids, ",")

	if len(strs_group) == 0 {
		infoLog.Fatalln(str_manager_userids, " manager_userids empty ")
	}

	ManagedServiceGroup = make(map[uint32]*org.IMClient, len(strs_group))

	GlobalRecvMsgPackChan = make(chan *org.IMMsgPack, 512)

	GlobalSendMsgPackChan = make(chan *org.WSMsgPack, 512)

	infoLog.Println("startManagerUserid() id_group=", strs_group)

	for _, v := range strs_group {

		_userid, err := strconv.Atoi(v)

		if err == nil {

			userid := uint32(_userid)

			im_client := org.NewIMClient(im_host, im_port, userid, db, infoLog, p2pSvr)

			im_client.HandleMessage(func(im *org.IMClient, msgPack *org.IMMsgPack) {
				infoLog.Println(im.UserID, "recv user's message ", msgPack.Msg.Text.Text)
				GlobalRecvMsgPackChan <- msgPack
			})

			im_client.HandleNotify(func(im *org.IMClient, msgPack *org.NotifyPack) {
				infoLog.Println(im.UserID, "recv user's noify ", msgPack.Type)
				kf_group.NotifyToAllKf(msgPack.Type, msgPack.Data)
			})

			im_client.HandleClosed(func(im *org.IMClient) {
				infoLog.Println(im.UserID, "HandleClosed() is Offline")
				kf_group.NotifyToAllKf("error", fmt.Sprintf("%s [%d] is offline", im.NickName, im.UserID))
			})

			im_client.HandleReConnected(func(im *org.IMClient) {
				infoLog.Println(im.UserID, "HandleReConnected() is ReConnected")
				kf_group.NotifyToAllKf("error", fmt.Sprintf("%s [%d] is ReConnected", im.NickName, im.UserID))
			})

			ManagedServiceGroup[userid] = im_client
			go im_client.StartLogin()

		} else {
			infoLog.Println(" manager_userids convert fail id=", v)
		}
	}

	go BatchRecvMessageHandler()

	go BatchSendMessageHandler()

}

//check the customer id with the session offered
func getUserByToken(token string) (*org.MgUserInfo, error) {

	url := get_userid_url + token

	response, err := http.Get(url)

	if err != nil {
		return nil, errors.New("authorization fail 1")
	}

	if response.StatusCode != 200 {
		return nil, errors.New("authorization fail 2")
	}

	szTong_b, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil, err
	}

	info := &org.MgUserInfo{}
	err = json.Unmarshal(szTong_b, info) // JSON to Struct

	if err != nil {
		return nil, err
	}

	if info.Status == 0 {
		return info, nil
	} else {
		return nil, errors.New("bad info " + info.Message)
	}

}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())

	cfg, err := common.InitLogAndOption(&options, &infoLog)

	//infoLog = log.New(os.Stdout, "[Info]", log.LstdFlags)

	db, err = common.InitMySqlFromSection(cfg, infoLog, "MYSQL", 2, 1)
	common.CheckError(err)
	defer Mgdb.Close()

	Mgdb, err = common.InitMySqlFromSectionWithMb4(cfg, infoLog, "MG_MYSQL", 2, 1)
	common.CheckError(err)
	defer Mgdb.Close()

	// 读取P2PWorker 配置
	im_host := cfg.Section("P2PWORKER").Key("im_ip").MustString("127.0.0.1")
	im_port := cfg.Section("P2PWORKER").Key("im_port").MustUint(8883)

	get_userid_url = cfg.Section("SERVICE_CONFIG").Key("get_userid_url").MustString("")

	if get_userid_url == "" {
		infoLog.Fatalln("get_userid_url not config")
	}

	str_manager_userids := cfg.Section("SERVICE_CONFIG").Key("manager_userids").MustString("")

	if str_manager_userids == "" {
		infoLog.Fatalln("manager_userids not config")
	}

	// 建立Memcache链接
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustString("11211")
	mcUserState = new(tcpcommon.MemcacheApi)
	mcUserState.Init(mcIp + ":" + mcPort)
	// fmt.Printf("mcUserState : %+v", mcUserState);
	// 查询用户信息的redis
	redisHost := cfg.Section("REDIS").Key("redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("redis_port").MustString("6379")
	userinfoRedis = tcpcommon.NewRedisApi(redisHost + ":" + redisPort)

	router := gin.Default()
	kf_group = org.NewKfGroup(Mgdb, infoLog, mcUserState, userinfoRedis)

	// 读取P2PWorker 配置
	p2p_ip := cfg.Section("P2PWORKER").Key("p2p_ip").MustString("127.0.0.1")
	p2p_port := cfg.Section("P2PWORKER").Key("p2p_port").MustString("61200")
	p2pSvr = tcpcommon.NewP2PWorkerApiV2(p2p_ip,
		p2p_port,
		time.Minute,
		time.Minute,
		&tcpcommon.HeadV3Protocol{},
		100)
	startManagerUserid(im_host, int(im_port), str_manager_userids)

	// 初始化每条消息通知到哪些人
	strInformUserid := cfg.Section("SERVICE_CONFIG").Key("inform_userids").MustString("")
	if err := kf_group.InitInformUser(strInformUserid); err != nil {
		infoLog.Fatalln("InitInformUser err %s %+v", strInformUserid, err)
	}

	if err := kf_group.InitKfServiceMap(); err != nil {
		infoLog.Fatalln("InitKfServiceMap err", err)
	}

	html_tpl := cfg.Section("SERVICE_CONFIG").Key("html_tpl").MustString("tpl/*")
	infoLog.Println("html_tpl=", html_tpl)
	router.LoadHTMLGlob(html_tpl)

	router.GET("/kf_center/testpage/:token", func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")

		c.HTML(http.StatusOK, "chatpage.html", gin.H{

			"token": c.Param("token"),
		})
	})

	router.GET("/kf_center/gettoken/:token", func(c *gin.Context) {

		token := c.Param("token")

		if token == "xiangxiaoqin" {
			c.JSON(200, gin.H{
				"status":      0,
				"mg_user_id":  75,
				"mg_nickname": "xiangxiaoqin",
			})
			return
		}

		if token == "xuan" {
			c.JSON(200, gin.H{
				"status":      0,
				"mg_user_id":  394,
				"mg_nickname": "xuan",
			})
			return
		}

		if token == "lien" || token == "liling" {
			c.JSON(200, gin.H{
				"status":      0,
				"mg_user_id":  41,
				"mg_nickname": "liling",
			})
			return
		}

	})

	router.GET("/kf_center/chat/:token", func(c *gin.Context) {

		token := c.Param("token")

		mguserinfo, err := getUserByToken(token)

		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		info := make(map[string]interface{})
		ip := kf_group.GetClientIP(c.Request)

		sim := &org.SimpleMgUserInfo{
			MgUserId:   mguserinfo.MgUserId,
			MgNickName: mguserinfo.MgNickName,
			IP:         ip,
		}

		info["mguserinfo"] = sim
		infoLog.Printf("chat start with mguserinfo=%v\n", sim)

		kf_group.InitWsSession(c.Writer, c.Request, info)
	})

	router.GET("/kf_center/updateServiceMap", func(c *gin.Context) {

		infoLog.Printf("Receive request updateServiceMap")

		if err := kf_group.InitKfServiceMap(); err != nil {
			infoLog.Printf("InitKfServiceMap err", err)
			c.JSON(http.StatusOK, gin.H{
				"status":  1,
				"message": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  0,
			"message": "update success",
		})
		return
	})

	kf_group.OnMessage(func(wsmsg *org.WSMsgPack) {
		GlobalSendMsgPackChan <- wsmsg
	})

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(NsqMessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	server_ip := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("0.0.0.0")
	listen_port := cfg.Section("LOCAL_SERVER").Key("bind_port").MustString("8021")

	router.Run(server_ip + ":" + listen_port) // listen and serve on 0.0.0.0:8080

	close(GlobalRecvMsgPackChan)
	close(GlobalSendMsgPackChan)
	kf_group.Close()
}
