package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_p2p"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	simplejson "github.com/bitly/go-simplejson"
	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/proto"
	"github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	"io"
	"io/ioutil"
	"path"
	//"github.com/garyburd/redigo/redis"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"mime/multipart"
	"net"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

//编译拷贝
//go build yingyutalk.go && sleep 1 &&  supervisorctl stop  go-yingyutalk   && cp ./yingyutalk  /home/ht/goproj/yingyutalk/bin/ &&  supervisorctl start  go-yingyutalk

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`
}

var (
	infoLog *log.Logger
	db      *sql.DB
	p2pSvr  *tcpcommon.P2PWorkerApiV2
	//redPool *redis.Pool

	//用于二维码的锁

	LoginInfo map[string]*sync.Mutex

	options Options

	get_userid_url string
	OSSBukect      string = "ht-blog"
	OSSRegion      string = "oss-cn-hongkong.aliyuncs.com"
	OSSAppID       string
	OSSAppKey      string
	OSSPrefix      string
	ConvertUrl     string
	TmpFileDir     string
)

var parser = flags.NewParser(&options, flags.Default)

type fileCheckData struct {
	Md5    string `json:"md5" binding:"required"`
	Name   string `json:"name" binding:"required"`
	Length uint32 `json:"length" binding:"required"`
}

type rpcUserID struct {
	Status  uint32 `json:"status" binding:"required"`
	UserID  uint32 `json:"userid" binding:"omitempty"`
	Message string `json:"message" binding:"omitempty"`
}

//checkfile 返回的状态码
const (
	STA_NO_DATA         = 0 //服务器没有任何文件，需要上传。
	STA_LOCAL_EXIST     = 1 //服务器存在文件，
	STA_RECOVER_DELTED  = 2 //文件已经存储，并且自己删除过，只要恢复。
	STA_EXIST_IN_CLOUD  = 3 //文件已经，存云端，存储过。
	STA_SERVER_ERROR    = 4 //服务器内部错误
	STA_PARAM_ERR       = 5 //参数错误
	STA_FILE_NOT_ALLOWD = 6 //文件有问题，名字非法或者内容不和

	CMD_NOTIFY_FILE_UPLOADED     = 0x6086 //通知客户端 某文件上传成功了
	CMD_NOTIFY_FILE_FAILED       = 0x6087 //通知客户端 某文件处理失败了
	CMD_NOTIFY_FILE_START_UPLOAD = 0x6088 //通知客户端 某文件要准备开始上传了
	CMD_NOTIFY_FILE_CANCELED     = 0x6090 //通知客户端 某文件取消了上传
)

func notifyClientUploaded(userid uint32, ptype uint32, md5 string, filename string) error {

	msgObj1 := simplejson.New()

	msgObj1.Set("type", ptype)
	msgObj1.Set("url", filename)

	now := time.Now()
	nanos := now.UnixNano()

	msgObj := simplejson.New()
	msgObj.Set("from_nickname", "HelloTalk team")
	msgObj.Set("msg_model", "normal")
	msgObj.Set("msg_type", "notify")
	msgObj.Set("msg_id", md5)
	msgObj.Set("server_ts", nanos/1000000)
	msgObj.Set("notify", msgObj1)

	strMsgBody, err := msgObj.MarshalJSON()
	if err != nil {
		infoLog.Println(userid, "notifyClientUploaded() json error ", ptype, md5, filename)
		return nil
	}

	infoLog.Printf("%d notifyClientUploaded() ptype=0x%x strMsgBody=%v", userid, ptype, string(strMsgBody))

	compressed := common.CompressString(strMsgBody)

	pbBody := &ht_p2p.P2PMsgBody{
		JustOnline: proto.Uint32(2),
		DontReply:  proto.Uint32(1),
		P2PData:    compressed,
		ToType:     ht_p2p.TO_CLIENT_TYPE_SEND_TO_MOBILE.Enum(),
	}

	payLoad, err := proto.Marshal(pbBody)

	head := &tcpcommon.HeadV3{}

	head.Flag = 0XF0
	head.CryKey = 0
	head.Version = 0x04
	head.TermType = 0
	head.Seq = 1
	head.Cmd = 0x4031
	head.From = 10000
	head.To = userid
	head.Len = uint32(len(payLoad) + tcpcommon.HeadV3Len)

	if err != nil {
		infoLog.Printf("proto.Marshal failed from=%v to=%v cmd=%v seq=%v",
			head.From,
			head.To,
			head.Cmd,
			head.Seq)
		return nil
	}

	_, err = p2pSvr.SendPacket(head, payLoad)

	if err != nil {
		infoLog.Println(userid, "notifyClientUploaded() send failed ", err)
		return err
	}

	return nil
}

func ConvertHandler(c *gin.Context) (uint32, error) {

	var chunkSize uint32
	chunkSize = 1 * 1024

	var (
		chunkBytes []byte = make([]byte, chunkSize)
		readLen    uint32
		leftLength uint32
	)

	fmt.Println("start upload", c.Request.Header)
	strLength := ""

	arr := c.Request.Header["Content-Length"]

	if len(arr) > 0 {
		strLength = arr[0]
	} else {
		return STA_PARAM_ERR, errors.New("bad content length")
	}

	if strLength == "" {
		return STA_PARAM_ERR, errors.New("bad content length")
	}

	totalLen, err := strconv.Atoi(strLength)

	if err != nil {
		return STA_PARAM_ERR, errors.New("content Length error")
	}
	totalLen2 := uint32(totalLen)

	leftLength = totalLen2

	readLen = 0

	for leftLength > 0 {

		readLen = chunkSize

		if chunkSize >= leftLength {
			readLen = leftLength
		}

		// read length
		if _, err := io.ReadAtLeast(c.Request.Body, chunkBytes, int(readLen)); err != nil {
			c.AbortWithError(http.StatusNotAcceptable, err)
			return 3, err
		}

		if leftLength == totalLen2 {
			fmt.Println("leftLength:", leftLength, string(chunkBytes))
		}

		leftLength = leftLength - readLen

	}

	return 0, nil

}

//上传用户的图片和原始文件
func uploadToOSS(userid uint32, md5 string, srcFile string, userFilename string, imagePaths []string) error {

	endPoint := OSSRegion
	AccessKeyId := OSSAppID
	AccessKeySecret := OSSAppKey
	Bucket := OSSBukect

	client, err := oss.New(endPoint, AccessKeyId, AccessKeySecret)
	if err != nil {
		return err
	}

	bucket, err := client.Bucket(Bucket)
	if err != nil {
		return err
	}
	//file 1 2 3, //
	//命名规则 bai/{fileType}/{imagecount}/9.jpg

	fileExt := common.GetExt(userFilename)
	intType := common.ExtToDBType[fileExt]

	timeoday_str := fmt.Sprintf("%d/%d", intType, len(imagePaths))

	prefix := OSSPrefix

	toOSSName := prefix + timeoday_str
	srcFileObj := toOSSName + "/m_" + md5 + fileExt

	err = bucket.PutObjectFromFile(srcFileObj, srcFile)

	if err != nil {
		return err
	}

	for k, v := range imagePaths {
		tmpName := fmt.Sprintf("%s/%s/%s", toOSSName, md5, path.Base(v))

		infoLog.Println(userid, "idx=", k, " uploadToOSS() OK", tmpName, " start ")

		err = bucket.PutObjectFromFile(tmpName, v)
		if err != nil {
			infoLog.Println(userid, "uploadToOSS() error", err, tmpName)
			return err
		}
		infoLog.Println(userid, "uploadToOSS() OK", tmpName, " done ")
	}

	return nil
}

//check file from
func getUserByToken(token string) (uint32, error) {

	url := get_userid_url + token

	response, err := http.Get(url)

	if err != nil {
		infoLog.Println("getUserByToken() fail", err)
		return 0, errors.New("authorization fail 1")
	}

	if response.StatusCode != 200 {
		return 0, errors.New("authorization fail 2")
	}

	szTong_b, err := ioutil.ReadAll(response.Body)

	//infoLog.Println("getUserByToken() error opening", string(szTong_b))

	if err != nil {
		return 0, err
	}

	info := &rpcUserID{}
	err = json.Unmarshal(szTong_b, info) // JSON to Struct

	if err != nil {
		return 0, err
	}

	if info.Status == 0 {
		return info.UserID, nil
	} else {
		return 0, errors.New(info.Message)
	}

}

// check file from db
func isFileUploaded(md5 string) (bool, error) {
	return false, nil
}

func getLocalFileName(md5 string) string {
	return TmpFileDir + md5
}

func postFile(userid uint32, url, filename, userFilename, returnPath string) error {

	//打开文件句柄操作
	file, err := os.Open(filename)
	if err != nil {
		infoLog.Println(userid, "postFile() error opening", filename)
		return err
	}
	defer file.Close()

	//创建一个模拟的form中的一个选项,这个form项现在是空的
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)
	//bodyBuf.WriteByte(c)
	//fileWriter, err := bodyWriter.CreateFormFile("uploadfile", filename)

	fileType, fileExt := common.TypeByExtension(userFilename)

	tmpName := filename + fileExt

	fileWriter, err := common.CreateFormFile2(bodyWriter, "uploadfile", tmpName, fileType)
	if err != nil {
		return err
	}

	// log.Println("userFilename=", userFilename, ",tmpName=", tmpName, ",fileType=", fileType)
	_, err = io.Copy(fileWriter, file)
	if err != nil {
		return err
	}

	//获取上传文件的类型,multipart/form-data; boundary=...
	contentType := bodyWriter.FormDataContentType()

	//这个很关键,必须这样写关闭,不能使用defer关闭,不然会导致错误
	bodyWriter.Close()

	//这里就是上传的其他参数设置,可以使用 bodyWriter.WriteField(key, val) 方法
	//也可以自己在重新使用  multipart.NewWriter 重新建立一项,这个再server 会有例子
	params := map[string]string{
		"filename": userFilename,
	}

	//这种设置值得仿佛 和下面再从新创建一个的一样
	for key, val := range params {
		_ = bodyWriter.WriteField(key, val)
	}

	infoLog.Println(userid, "postFile() start post:", filename, url)

	//发送post请求到服务端
	// resp, err := http.Post(url, contentType, bodyBuf)

	req, err := http.NewRequest("POST", url, bodyBuf)

	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Content-Length", strconv.Itoa(bodyBuf.Len()))

	c := http.Client{
		Transport: &http.Transport{
			Dial: func(netw, addr string) (net.Conn, error) {
				c, err := net.DialTimeout(netw, addr, time.Second*15) //设置建立连接超时
				if err != nil {
					infoLog.Println(userid, "postFile() DialTimeout:", err.Error())
					return nil, err
				}
				c.SetDeadline(time.Now().Add(60 * 20 * time.Second)) //设置发送接收数据超时
				return c, nil
			},
		},
		Timeout: time.Second * 60 * 20,
	}

	resp, err := c.Do(req)

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	infoLog.Printf("%d postFile() code=%d,", userid, resp.StatusCode)

	typs := resp.Header["Content-Type"]
	if resp.StatusCode == 200 && len(typs) > 0 && typs[0] == "application/x-zip-compressed" {
		return common.ReadDataToFile(resp.Body, resp.ContentLength, returnPath)
	} else {
		body, err := ioutil.ReadAll(resp.Body)
		infoLog.Println(userid, "postFile() Error=", string(body), err)
	}

	return errors.New("bad api result")
}

func saveUploadedStatus(userid uint32, md5 string, userFileName string, imageCount uint32) error {

	_, fileExt := common.TypeByExtension(userFileName)

	intType := common.ExtToDBType[fileExt]
	infoLog.Println(userid, "saveUploadedStatus() intType=", intType, ",fileExt=", fileExt)

	// return nil
	r1, err1 := db.Exec("INSERT INTO FS_FILE_LIST (userid,`md5`, `type`, status,image_count, bukect,  createtime) "+
		"VALUES(?,?,?,?,?,?, now())"+
		"ON DUPLICATE KEY UPDATE `type`=VALUES(`type`), image_count=VALUES(image_count)",
		userid,
		md5,
		intType,
		3,
		imageCount,
		OSSBukect)

	if err1 != nil {
		infoLog.Println(userid, "FS_FILE_LIST insert failed err=", err1)
		return err1
	}

	id1, err := r1.LastInsertId()

	infoLog.Println(userid, "saveUploadedStatus() FS_FILE_LIST ok Id=", id1)

	r2, err2 := db.Exec("INSERT INTO FS_USERS_FILE (userid, `md5`, file_name,`deleted`, update_ts, createtime) "+
		"VALUES(?, ?, ?,0, UNIX_TIMESTAMP(), NOW())"+
		"ON DUPLICATE KEY UPDATE file_name=VALUES(file_name),deleted=VALUES(deleted), update_ts=VALUES(update_ts)  ",
		userid,
		md5,
		userFileName,
	)

	if err2 != nil {
		infoLog.Println(userid, "saveUploadedStatus() FS_USERS_FILE insert failed err=", err2)
		return err2
	}

	id2, err := r2.LastInsertId()

	infoLog.Println(userid, "saveUploadedStatus() rId1=", id1, ",rId2=", id2, err)

	//通知客户端 p2p 协议

	return nil

}

func doConvert(userid uint32, md5 string, localName string, userFileName string) {

	url := ConvertUrl
	//url := "http://127.0.0.1:8080/secconvert"
	returnPath := localName + ".zip"

	err := postFile(userid, url, localName, userFileName, returnPath)
	var errp error
	if err != nil {
		errp = notifyClientUploaded(userid, CMD_NOTIFY_FILE_FAILED, md5, userFileName+",fail with err="+err.Error())
		infoLog.Println(userid, "postFile() convert file error:", err, errp)
		return
	}

	unzipPath := localName + "_dir/"

	// save userFileName and md5 and userid to db
	filenameArray, err1 := common.DeCompress(returnPath, unzipPath)

	if err1 != nil {
		errp = notifyClientUploaded(userid, CMD_NOTIFY_FILE_FAILED, md5, err.Error())
		log.Println(userid, "DeCompress() unzip error:", err1, errp)
		return
	}

	err = uploadToOSS(userid, md5, localName, userFileName, filenameArray)
	if err != nil {
		errp = notifyClientUploaded(userid, CMD_NOTIFY_FILE_FAILED, md5, err.Error())
		infoLog.Println(userid, "uploadToOSS() post error:", err, errp)
		return
	}

	infoLog.Println(userid, "uploadToOSS() finished with OK filenameArray=", filenameArray)

	infoLog.Println(userid, "saveUploadedStatus() begain")

	err = saveUploadedStatus(userid, md5, userFileName, uint32(len(filenameArray)))

	if err != nil {
		errp = notifyClientUploaded(userid, CMD_NOTIFY_FILE_FAILED, md5, err.Error())
		infoLog.Println(userid, "notifyClientUploaded() saveUploadedStatus  save db error:", err, errp)
		return
	}

	ReErr1 := os.Remove(localName)
	ReErr2 := os.Remove(returnPath)
	ReErr3 := os.RemoveAll(unzipPath)

	if ReErr1 != nil || ReErr2 != nil || ReErr3 != nil {
		infoLog.Println(userid, "notifyClientUploaded() os.Remove() done fail", ReErr1, ReErr2, ReErr3)
	}

	err = notifyClientUploaded(userid, CMD_NOTIFY_FILE_UPLOADED, md5, userFileName)

	if err != nil {
		infoLog.Println(userid, "notifyClientUploaded() error:", err)
		return
	}

	infoLog.Println(userid, "saveUploadedStatus() done ")
}

func doUpload(c *gin.Context, pmd5 *string, plocalName *string) (uint32, uint32, error) {

	token := c.Param("token")

	userid, err := getUserByToken(token)

	if err != nil {

		infoLog.Println(" getUserByToken() doUpload error=", err)

		return STA_PARAM_ERR, 0, err
	}

	md5 := c.PostForm("md5")

	if md5 == "" {
		return STA_PARAM_ERR, userid, errors.New("client param error,code 1")
	}
	*pmd5 = md5

	fileSize := c.PostForm("filesize")

	if fileSize == "" {
		return STA_PARAM_ERR, userid, errors.New("client param error,code 2")
	}

	totalLen, err := strconv.Atoi(fileSize)

	if err != nil {
		return STA_PARAM_ERR, userid, errors.New("convert filesize fail")
	}

	localName := getLocalFileName(md5)
	*plocalName = localName

	file, header, err1 := c.Request.FormFile("myfile")

	if err1 != nil {
		infoLog.Println(userid, "doUpload() FormFile error=", err1)
		return STA_PARAM_ERR, userid, errors.New("client param error,code 3")
	}

	// if header.Size != int64(totalLen) {
	// 	return STA_PARAM_ERR, errors.New("filesize not match 1")
	// }

	userFileName := header.Filename

	infoLog.Println(userid, "doUpload() handle file=", userFileName, md5, "length=", totalLen)

	fileInfo := &fileCheckData{
		Md5:    md5,
		Name:   userFileName,
		Length: uint32(totalLen),
	}

	code, err := doCheckfile(userid, fileInfo)

	if code == STA_LOCAL_EXIST || code == STA_RECOVER_DELTED || code == STA_EXIST_IN_CLOUD {

		infoLog.Println(userid, "doCheckfile()", err)
		return STA_EXIST_IN_CLOUD, userid, err
	}

	// 创建临时接收文件
	out, err := os.Create(localName)
	if err != nil {
		infoLog.Fatal(userid, err)
		return STA_SERVER_ERROR, userid, err
	}
	defer out.Close()

	contentBuf := make([]byte, 6)

	n, err := file.Read(contentBuf)

	if err != nil {
		return STA_SERVER_ERROR, userid, err
	}

	infoLog.Println(userid, "CheckFileContent() ", contentBuf, n)

	err = common.CheckFileContent(contentBuf, userFileName)

	if err != nil {
		return STA_FILE_NOT_ALLOWD, userid, err
	}

	writeLen1, err := out.Write(contentBuf)

	writeLen, err := io.Copy(out, file)

	if err != nil {
		infoLog.Fatal(err)
		return STA_SERVER_ERROR, userid, err
	}

	if writeLen+int64(writeLen1) != int64(totalLen) {
		return STA_PARAM_ERR, userid, errors.New("filesize not match")
	}

	realMd5, err := common.Md5File(localName)

	if err != nil {
		return STA_SERVER_ERROR, userid, err
	}

	if realMd5 != md5 {
		infoLog.Println(userid, "doUpload() md5:", md5, "!= realMd5:", realMd5)
		return STA_SERVER_ERROR, userid, errors.New("md5 not match")
	}

	go doConvert(userid, md5, localName, userFileName)

	return 0, userid, nil
}

func convert(c *gin.Context) {

	//	chunkSize := 64 * 1024

	md5 := c.DefaultQuery("md5", "")
	name := c.DefaultQuery("name", "")
	localName := ""

	infoLog.Println(c.Request.URL.Path, " started upload,md5:", md5)

	code, userid, err := doUpload(c, &md5, &localName)

	if (code == STA_SERVER_ERROR || code == STA_PARAM_ERR || code == STA_FILE_NOT_ALLOWD) && md5 != "" && userid > 0 {
		errp := notifyClientUploaded(userid, CMD_NOTIFY_FILE_FAILED, md5, name+" "+err.Error())
		if errp != nil {
			infoLog.Printf("%d convert() notifyClientUploaded error:%s", userid, errp.Error())

		}
	}

	if localName != "" && err != nil {
		os.Remove(localName)
	}

	if err != nil {
		infoLog.Printf("%d doUpload() upload file failed code=%d md5:%s,name=%s,error:%s", userid, code, md5, name, err.Error())
		outputAjax(c, code, err)
	} else {
		infoLog.Println(userid, "doUpload() ,success  md5=:", md5)
		outputAjax(c, 0, errors.New("ok"))
	}

}

func doCheckfile(userid uint32, p *fileCheckData) (uint32, error) {

	if p.Md5 == "" || p.Name == "" || p.Length < 2 {
		infoLog.Printf("%d doCheckfile() params p=%v", userid, p)
		return STA_PARAM_ERR, errors.New("bad params")
	}

	err := common.CheckExt(p.Name)

	if err != nil {
		return STA_FILE_NOT_ALLOWD, err
	}

	var strUid, strMd5 string

	err = db.QueryRow("SELECT userid,`md5`  from FS_FILE_LIST WHERE `md5`=?", p.Md5).Scan(&strUid, &strMd5)

	switch {
	case err == sql.ErrNoRows:
		localName := getLocalFileName(p.Md5)
		exist, _ := common.CheckFileExistWithLen(localName, strconv.Itoa(int(p.Length)))
		if exist == true {
			go doConvert(userid, p.Md5, localName, p.Name)
			return STA_LOCAL_EXIST, errors.New("local exist params,wait for handle")
		}
		return STA_NO_DATA, errors.New("need to upload now")
	case err != nil:
		return STA_SERVER_ERROR, err
	default:

		r, err := db.Exec("INSERT INTO FS_USERS_FILE (userid,`md5`,file_name, deleted,update_ts,createtime) VALUES(?, ?, ?,0,UNIX_TIMESTAMP(), NOW())"+
			"ON DUPLICATE KEY UPDATE update_ts=VALUES(update_ts),deleted=VALUES(deleted),file_name=VALUES(file_name)",
			userid,
			p.Md5,
			p.Name)

		if err != nil {
			infoLog.Println(userid, "doCheckfile() FS_USERS_FILE insert failed err=", err)
			return STA_SERVER_ERROR, errors.New("save error")
		}

		affectRow, err := r.RowsAffected()
		if err != nil {
			infoLog.Println(userid, "doCheckfile() FS_USERS_FILE affectRow failed err=", err)
			return STA_SERVER_ERROR, errors.New("upload,error")
		}

		infoLog.Println(userid, "doCheckfile() FS_USERS_FILE affectRow=", affectRow)

		time.Sleep(time.Second * 1)
		err = notifyClientUploaded(userid, CMD_NOTIFY_FILE_UPLOADED, p.Md5, p.Name)

		if err != nil {
			infoLog.Println(userid, "notifyClientUploaded() error:", err)
		}

		return STA_EXIST_IN_CLOUD, errors.New("no need upload,already saved")
	}

}

func outputAjax(c *gin.Context, code uint32, err error) {

	c.Header("Access-Control-Allow-Origin", "*")
	c.Header("Access-Control-Allow-Methods", "*")
	c.Header("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type")
	c.Header("Access-Control-Allow-Credentials", "true")
	//c.Header("Content-Type", "application/json; charset=utf-8")

	c.JSON(200, gin.H{
		"status":  code,
		"message": err.Error(),
	})
}

func main() {

	//runtime.GOMAXPROCS(runtime.NumCPU())

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("main Must input config file name")
	}

	// log.Println("config name =", options.ServerConf)
	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return
	}
	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("")

	if fileName != "" {
		logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		defer logFile.Close()
		if err != nil {
			log.Fatalln("open file error !")
			return
		}

		// 创建一个日志对象
		infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	} else {
		infoLog = log.New(os.Stdout, "[Info]", log.LstdFlags)
	}

	// 配置log的Flag参数
	infoLog.SetFlags(infoLog.Flags() | log.LstdFlags)

	// init mysql
	mysqlHost := cfg.Section("MYSQL").Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section("MYSQL").Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section("MYSQL").Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section("MYSQL").Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section("MYSQL").Key("mysql_port").MustString("3306")

	// init mysql
	OSSBukect = cfg.Section("SERVICE_CONFIG").Key("oss_bucket").MustString("ht-blog")
	OSSRegion = cfg.Section("SERVICE_CONFIG").Key("oss_region").MustString("oss-cn-hongkong.aliyuncs.com")
	OSSAppID = cfg.Section("SERVICE_CONFIG").Key("oss_appid").MustString("ULuE3LK66Y6ZlEWS")
	OSSAppKey = cfg.Section("SERVICE_CONFIG").Key("oss_appkey").MustString("JTFyxX7TW9K1TSVMwglj272CxfvwUC")
	ConvertUrl = cfg.Section("SERVICE_CONFIG").Key("convert_url").MustString("http://tools.classroomcloud.cn/Doc/Convert")
	TmpFileDir = cfg.Section("SERVICE_CONFIG").Key("tmp_file_dir").MustString("/tmp/")
	OSSPrefix = cfg.Section("SERVICE_CONFIG").Key("oss_prefix").MustString("bai/")
	get_userid_url = cfg.Section("SERVICE_CONFIG").Key("get_userid_url").MustString("")

	//mysqlHost := ("10.243.134.240")
	//mysqlUser := ("IMServer")
	//mysqlPasswd := ("Hellotalk*IMServer")
	//mysqlDbName := ("HT_IMDB")
	//mysqlPort := ("3306")

	// 读取P2PWorker 配置
	p2p_ip := cfg.Section("P2PWORKER").Key("p2p_ip").MustString("127.0.0.1")
	p2p_port := cfg.Section("P2PWORKER").Key("p2p_port").MustString("61200")

	infoLog.Printf("mysql host=%v user=%v passwd=%v dbname=%v port=%v p2p_ip=%v p2p_port=%v",
		mysqlHost,
		mysqlUser,
		mysqlPasswd,
		mysqlDbName,
		mysqlPort,
		p2p_ip,
		p2p_port,
	)

	p2pSvr = tcpcommon.NewP2PWorkerApiV2(p2p_ip,
		p2p_port,
		time.Minute,
		time.Minute,
		&tcpcommon.HeadV3Protocol{},
		100)

	db, err = sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=90s")
	if err != nil {
		infoLog.Println("open mysql failed")
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()
	db.SetMaxIdleConns(1)
	db.SetMaxOpenConns(2)

	if err := db.Ping(); err != nil {
		infoLog.Fatalln(err)
	}

	// redisIp := "127.0.0.1"
	// redisPort := 6379

	// redPool = newPool(redisIp + ":" + strconv.Itoa(redisPort))

	r := gin.Default()

	html_tpl := cfg.Section("SERVICE_CONFIG").Key("html_tpl").MustString("tpl/*")

	r.LoadHTMLGlob(html_tpl)

	r.GET("/yingyutalk/upload/:token", func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")

		c.HTML(http.StatusOK, "upload.html", gin.H{
			"action": "/yingyutalk/convert",
			"token":  c.Param("token"),
		})
	})

	r.OPTIONS("/yingyutalk/*action", func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Methods", "*")
		c.Header("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.String(http.StatusOK, "ok")
	})

	r.POST("/yingyutalk/cancel/:token", func(c *gin.Context) {

		var p fileCheckData

		if c.BindJSON(&p) == nil {

			token := c.Param("token")

			userid, err := getUserByToken(token)

			if err != nil {
				infoLog.Println("getUserByToken() cancel error:", err)
				outputAjax(c, STA_PARAM_ERR, err)
				return
			}

			err = notifyClientUploaded(userid, CMD_NOTIFY_FILE_CANCELED, p.Md5, p.Name)

			if err != nil {
				infoLog.Println(userid, "notifyClientUploaded() error:", err)
				outputAjax(c, STA_PARAM_ERR, err)
				return
			}

			outputAjax(c, 0, errors.New("ok"))

		} else {
			outputAjax(c, STA_PARAM_ERR, errors.New("bad json"))
		}

	})

	r.POST("/yingyutalk/convert/:token", convert)

	r.POST("/yingyutalk/checkfile/:token", func(c *gin.Context) {

		var p fileCheckData

		if c.BindJSON(&p) == nil {

			token := c.Param("token")

			userid, err := getUserByToken(token)

			if err != nil {
				infoLog.Println("getUserByToken() checkfile error:", err)
				outputAjax(c, STA_PARAM_ERR, err)
				return
			}

			err = notifyClientUploaded(userid, CMD_NOTIFY_FILE_START_UPLOAD, p.Md5, p.Name)

			if err != nil {
				infoLog.Println(userid, "notifyClientUploaded() error:", err)
			}

			ret, err := doCheckfile(userid, &p)

			if err != nil {
				outputAjax(c, ret, err)
			} else {
				infoLog.Println("checkfile() ,success")
				outputAjax(c, 0, errors.New("ok"))
			}

		} else {
			outputAjax(c, STA_PARAM_ERR, errors.New("bad json"))
		}
	})

	r.POST("/secconvert", func(c *gin.Context) {
		ConvertHandler(c)
	})

	server_ip := cfg.Section("LOCAL_SERVER").Key("bind_ip").MustString("0.0.0.0")
	listen_port := cfg.Section("LOCAL_SERVER").Key("bind_port").MustString("8021")

	r.Run(server_ip + ":" + listen_port) // listen and serve on 0.0.0.0:8080

}
