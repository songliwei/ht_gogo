package main

import (
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_group_lesson"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_muc"
	"github.com/HT_GOGO/gotcp/webapi/common"
	"github.com/gin-gonic/gin"
	// "github.com/jessevdk/go-flags"
	"bytes"
	"encoding/base64"
	"encoding/hex"
	// "encoding/json"
	simplejson "github.com/bitly/go-simplejson"
	// "errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type RecordUrl struct {
	RecordObid     string  `json:"record_obid,omitempty"`
	VoiceUrl       string  `json:"voice_url,omitempty"`
	PptUrl         string  `json:"ppt_url,omitempty"`
	BeginTimeStamp *uint64 `json:"begin_time_stamp,omitempty"`
}

type LessonInfo struct {
	RoomId        string      `json:"room_id" binding:"omitempty"`
	ReqUid        string      `json:"uid" binding:"omitempty"`
	RecordUrlList []RecordUrl `json:"record_url_list,binding:"omitempty""`
	// LessonCoursewareUrl string                       `json:"lesson_courseware_url,omitempty"`
}

func PbRecordToJsonRecord(pbRecord *ht_group_lesson.RecordUrl) RecordUrl {
	Re := RecordUrl{
		RecordObid:     string(pbRecord.RecordObid),
		VoiceUrl:       string(pbRecord.VoiceUrl),
		PptUrl:         string(pbRecord.PptUrl),
		BeginTimeStamp: pbRecord.BeginTimeStamp,
	}
	return Re
}

var (
	infoLog          *log.Logger
	options          common.Options
	groupLessonApi   *tcpcommon.GroupLessonApi
	mucApi           *tcpcommon.MucApi
	userinfoRedis    *tcpcommon.RedisApi
	placeholderRedis *tcpcommon.RedisApi
	frontEndConfig   *simplejson.Json
	defUid           uint32 = 10000
	bigOpenidSalt    uint64 = 1024
	fileCache        map[string]string
)

func DropEqual(s string) string {
	re := strings.Replace(s, "=", "", -1)
	return re
}

// 十六进制转二进制
func HexToBin(s string) (string, error) {
	ret, err := hex.DecodeString(s)
	if err != nil {
		return "", err
	}
	return string(ret), nil
}

func BinToHex(s string) string {
	ret := hex.EncodeToString([]byte(s))
	return ret
}

func ReverseString(s string) string {
	n := len(s)
	runes := make([]rune, n)
	for _, rune := range s {
		n--
		runes[n] = rune
	}
	return string(runes[n:])
}

var ascii_uppercase = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
var ascii_lowercase = []byte("abcdefghijklmnopqrstuvwxyz")
var ascii_uppercase_len = len(ascii_uppercase)
var ascii_lowercase_len = len(ascii_lowercase)

func Rot13(b byte) byte {
	pos := bytes.IndexByte(ascii_uppercase, b)
	if pos != -1 {
		return ascii_uppercase[(pos+13)%ascii_uppercase_len]
	}
	pos = bytes.IndexByte(ascii_lowercase, b)
	if pos != -1 {
		return ascii_lowercase[(pos+13)%ascii_lowercase_len]
	}
	return b
}

func StrRot13(s string) string {
	output := make([]byte, len(s))
	for k, v := range s {
		output[k] = Rot13(byte(v))
	}

	return string(output)
}

/**
 * 加密OpenUid
 */
func EncodeOpenid(uid uint64) (openid string) {
	// uidStr := string(uid)
	uidStr := strconv.Itoa(int(uid))
	if len(uidStr)%2 == 1 {
		uidStr = "0" + uidStr
	}
	// infoLog.Println("uidStr:", uidStr)
	// 翻转字符串
	openid = ReverseString(uidStr)
	// infoLog.Println("ReverseString:", openid)
	openid, err := HexToBin(openid)
	if err != nil {
		infoLog.Println("HexToBin failed ", err)
		return ""
	}
	// infoLog.Println("HexToBin:", openid)

	openid = base64.StdEncoding.EncodeToString([]byte(openid))
	// infoLog.Println("base64:", openid)
	openid = StrRot13(openid)
	// infoLog.Println("StrRot13:", openid)
	return openid
}

func DecodeOpenid(openid string) (uid uint64) {
	// infoLog.Println("DecodeOpenid:", openid)
	openid = StrRot13(openid)
	// infoLog.Println("StrRot13:", openid)
	b64, err := base64.StdEncoding.DecodeString(openid)
	if err != nil {
		infoLog.Println("base64 decode failed ", err)
		return 0
	}
	// infoLog.Println("base64.StdEncoding.DecodeString:", string(b64))
	openid = BinToHex(string(b64))
	// infoLog.Println("BinToHex:", openid)
	openid = ReverseString(openid)
	// infoLog.Println("ReverseString:", openid)
	userid, err := strconv.ParseUint(openid, 10, 64)
	// infoLog.Println("ParseUint:", userid)
	if err != nil {
		infoLog.Println("DecodeOpenid strconv.Atoi failed ", err)
		return 0
	}
	uid = uint64(userid)
	return uid
}

var UseridSalt []uint64 = []uint64{
	63459385400637, 96758825373835, 73850592134985, 17853334364481, 29095726802479, 48581308808643,
	45153107875958, 52821049776393, 28458780909423, 99010032464283, 84264457395765, 69680115925147,
	14599702679552, 75469297710805, 16626202743500, 97913173136766, 53365264264866, 31085748760961,
	98400995207484, 86518860841169, 95476880087516, 49741324142087, 93772559824864, 33454501733649,
	17835195884108, 65449503331910, 10923225039151, 83197505285497, 85210160058922, 24028311369475,
	78715100279077, 60525647017639, 56720790932886, 17154537506867, 54409491191618, 42125174838584,
	21509365504607, 44921836582943, 54450478823855, 50878059556707, 26089497236535, 75615769457071,
	19530880041420, 56635332233272, 70337635702453, 80236816192045, 92234435456338, 42027962657157,
	80279652872122, 59795395936816, 97855834548826, 26468058810569, 97829653653315, 64918365827761,
	26726799444295, 62734406893141, 31632230721880, 67216263136360, 42279870174825, 25819314718246,
	24423196825664, 64838831855449, 97044314588419, 17967497981153, 28425309958402, 85468719664495,
	53176743758376, 76526843041646, 86142634507268, 65002349899150, 46175003631506, 97692735181189,
	12602403699420, 86586893605999, 82814457989297, 69198711856733, 64127300141844, 11117488322780,
	47496318863704, 50320400642231, 25517377087381, 47909070199821, 67091980781406, 71812290393281,
	83561734517570, 65634371445048, 73139554513618, 80178754811640, 27230201943312, 60303771460894,
	91499291460495, 88464226534124, 83903576775919, 17120843261946, 48096422876697, 95543857612646,
	20864610653370, 21543089777697, 54567299776244, 49052044977434,
}

// 对于一些encodeOpenid结果出来还是比较短的id进行进一步加工
func EncodeBigOpenid(sid, salt uint64) string {
	if sid <= 0 {
		return string(sid)
	}

	var ends uint64
	if salt == 0 {
		ends = sid % 100
	} else {
		ends = salt % 100
	}
	// infoLog.Println("ends ", ends)
	all := sid + UseridSalt[ends]
	// infoLog.Printf("sid %d + UseridSalt[ends] %d = %d ", sid, UseridSalt[ends], all)
	ret := EncodeOpenid(all)
	// infoLog.Println("EncodeOpenid ", ret)
	ret = DropEqual(ret)
	// infoLog.Println("DropEqual ", ret)
	if ends < 10 {
		return "0" + strconv.Itoa(int(ends)) + ret
	} else {
		return strconv.Itoa(int(ends)) + ret
	}
}

func DecodeBigOpenid(s string) (uint64, error) {
	endsp := s[0:2]
	// infoLog.Println("endsp ", endsp)
	ends, err := strconv.Atoi(endsp)
	// infoLog.Println("ends ", ends)
	if err != nil {
		infoLog.Println("DecodeBigOpenid fail :", err)
		return 0, err
	}

	rest := s[2:]
	// infoLog.Println("rest ", rest)
	userid1 := DecodeOpenid(rest + "==")
	// numberid, err := strconv.ParseUint(userid1, 10, 64)
	userid2 := userid1 - UseridSalt[ends]

	// infoLog.Printf("userid2 %d userid1 %d fail :", userid2, userid1)
	return userid2, nil
}

func parseLessonInfo(detailInfo *ht_group_lesson.LessonDetailInfo,
	// groupLesson *ht_group_lesson.GroupLessonItem,
	roomInfo *ht_muc.RoomInfoBody) gin.H {

	groupLesson := detailInfo.GetGroupLesson()
	persionLesson := detailInfo.GetPersonalLesson()

	RoomOpenid := EncodeBigOpenid(uint64(groupLesson.GetRoomId()), bigOpenidSalt)
	TeacherOpenid := EncodeBigOpenid(uint64(persionLesson.GetCreaterUid()), bigOpenidSalt)

	// 以JSON格式返回给前端的数据
	// lessonInfo := make()

	assign := gin.H{
		"LessonTitle":         string(persionLesson.GetLessonTitle()),
		"LessonAbstract":      string(persionLesson.GetLessonAbstract()),
		"TeacherAbstract":     string(persionLesson.GetTeacherAbstract()),
		"LessonCoursewareUrl": string(persionLesson.GetLessonCoursewareUrl()),
		"LessonCover":         string(persionLesson.GetCoverUrl()),
		"TeacherOpenid":       TeacherOpenid,
		"RoomOpenid":          RoomOpenid,
		"RoomName":            string(roomInfo.GetRoomName()),
		"LessonTime":          string(groupLesson.GetLessonTime()),
	}
	return assign
}

// 缓存获取url文件，用于解决前端跨域问题
func getCacheFile(url string) (content string, err error) {
	if v, ok := fileCache[url]; ok {
		content = v
		return content, nil
	} else {
		rsp, err := http.Get(url)
		if err != nil {
			infoLog.Printf("getCacheFile error : %v, url : %s", err, url)
			content = ""
			return content, err
		}
		defer rsp.Body.Close()

		body, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			return "", err
		}

		content = string(body)

		fileCache[url] = content

		return content, nil
	}
}

// 查询用户信息
func getUserInfo(Userid uint32) (jsonObj *simplejson.Json, err error) {
	key := common.GetUserInfoRedisKey(Userid)
	// infoLog.Println("GetUserinfo key :", key)
	val, err := userinfoRedis.Get(key)
	if err != nil {
		return nil, err
	}

	jsonObj, err = simplejson.NewJson([]byte(val))
	if err != nil {
		return nil, err
	}
	return jsonObj, nil
}

// 允许跨域
func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func GetPlaceHolderList(keys []string, lang string) map[string]string {
	translateRe := make(map[string]string, len(keys))
	lang = strings.ToLower(lang)
	lang = strings.Replace(lang, "-", "_", -1)
	for _, k := range keys {
		key := "CLIENT_STRING_" + k
		v, err := placeholderRedis.Hget(key, lang)
		if err != nil {
			infoLog.Printf("placeholderRedis.Hget %s %s failed: %s , v : %s", key, lang, err, v)
			// 如果为空，尝试取英文结果
			en, err := placeholderRedis.Hget("CLIENT_STRING_"+k, "en")
			if err != nil {
				infoLog.Printf("placeholderRedis.Hget %s %s try en failed: %s", k, lang, err)
				v = ""
			} else {
				v = en
			}
		}
		// 如果还是为空，只能取key值
		infoLog.Printf("GetPlaceHolderList last v: %s", k)
		if v == "" {
			translateRe[k] = k
		} else {
			translateRe[k] = v
		}
	}
	return translateRe
}

func GetAcceptLanguage(AL string) string {
	if AL == "*" {
		return "en"
	}
	langList := strings.Split(AL, ";")
	// infoLog.Printf("langList : %+v", langList)
	splice := strings.Split(langList[0], ",")
	// infoLog.Printf("splice : %+v", splice)
	re := splice[len(splice)-1]
	return re
}

func MergeStringMapGinH(h gin.H, s map[string]string) {
	for k, v := range s {
		h[k] = v
	}
}

func main() {
	// 加载配置文件
	cfg, err := common.InitLogAndOption(&options, &infoLog)
	common.CheckError(err)

	router := gin.Default()
	router.Use(Cors())

	LOCAL_SERVER := cfg.Section("LOCAL_SERVER")

	// 自身服务的配置
	srvIP := LOCAL_SERVER.Key("bind_ip").MustString("0.0.0.0")
	srvPort := LOCAL_SERVER.Key("bind_port").MustString("9888")
	// 页面配置
	htmlGlob := LOCAL_SERVER.Key("html_location").MustString("tpl/html/*")
	sharePage := LOCAL_SERVER.Key("share_page").MustString("index.html")
	recordPage := LOCAL_SERVER.Key("record_page").MustString("record.html")
	recordDelPage := LOCAL_SERVER.Key("record_del_page").MustString("deleted.html")
	// 静态资源
	staticRoot := LOCAL_SERVER.Key("CDN_HOST").MustString("")
	headUrlPrefix := LOCAL_SERVER.Key("HEAD_URL_PREFIX").MustString("")
	serviceAccessPrefix := LOCAL_SERVER.Key("service_access_prefix").MustString("")
	lessonPicPlaceHolder := LOCAL_SERVER.Key("lesson_pic_place_holder").MustString("")
	pptPicDefault := LOCAL_SERVER.Key("ppt_pic_default").MustString("")
	accOldHost := LOCAL_SERVER.Key("aac_old_host").MustString("")
	videoDelPic := LOCAL_SERVER.Key("video_delete_pic").MustString("")
	CDN := LOCAL_SERVER.Key("CDN").MustString("")
	// 群组课程配置
	groupLessonIp := cfg.Section("LESSON_SERVER").Key("ip").MustString("127.0.0.1")
	groupLessonPort := cfg.Section("LESSON_SERVER").Key("port").MustString("0")
	groupLessonConnLimit := cfg.Section("LESSON_SERVER").Key("pool_limit").MustInt(1000)
	infoLog.Printf("group lesson server ip=%v port=%v connLimit=%v", groupLessonIp, groupLessonPort, groupLessonConnLimit)
	groupLessonApi = tcpcommon.NewGroupLessonApi(groupLessonIp, groupLessonPort, 3*time.Second, 3*time.Second, &tcpcommon.HeadV3Protocol{}, groupLessonConnLimit)
	// MUC服务配置
	mucIp := cfg.Section("MUC").Key("ip").MustString("127.0.0.1")
	mucPort := cfg.Section("MUC").Key("port").MustString("0")
	mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	mucApi = tcpcommon.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &tcpcommon.HeadV3Protocol{}, mucConnLimit)
	// 查询用户信息的redis
	redisHost := cfg.Section("REDIS").Key("userinfo_redis_ip").MustString("127.0.0.1")
	redisPort := cfg.Section("REDIS").Key("userinfo_redis_port").MustString("6379")
	userinfoRedis = tcpcommon.NewRedisApi(redisHost + ":" + redisPort)
	// 查询多语言的redis
	redisHost = cfg.Section("PLACEHOLDER_REDIS").Key("ip").MustString("127.0.0.1")
	redisPort = cfg.Section("PLACEHOLDER_REDIS").Key("port").MustString("6379")
	placeholderRedis = tcpcommon.NewRedisApi(redisHost + ":" + redisPort)

	router.LoadHTMLGlob(htmlGlob)
	// router.Static("/groupLesson/img", "./img")

	// 前端配置信息，包括接口等
	apiList := map[string]string{
		"groupLesson":  serviceAccessPrefix + "/groupLesson",
		"lessonRecord": serviceAccessPrefix + "/lessonRecord",
		"recordMsg":    serviceAccessPrefix + "/recordMsg",
		"userInfo":     serviceAccessPrefix + "/userInfo",
		"getFileData":  serviceAccessPrefix + "/getFileData",
	}
	frontEndConfig = simplejson.New()
	frontEndConfig.Set("apiList", apiList)
	frontEndConfig.Set("lessonPicPlaceHolder", lessonPicPlaceHolder) // 课程默认封面
	frontEndConfig.Set("pptPicDefault", pptPicDefault)
	feConfByte, err := frontEndConfig.MarshalJSON()
	feConf := "{}"
	if err != nil {
		infoLog.Println("Marshal frontEndConfig error:", err)
	} else {
		feConf = string(feConfByte)
	}

	// 查看课程介绍
	router.GET(apiList["groupLesson"]+"/:obid", func(c *gin.Context) {
		obid := c.Param("obid")
		if obid == "" {
			c.AbortWithStatus(404)
		}

		// 处理userid
		openid := c.Query("openid")
		uid := uint64(defUid)
		if openid != "" {
			uid, err = DecodeBigOpenid(openid)
			if uid == 0 || err != nil {
				infoLog.Printf("groupLesson DecodeBigOpenid error:", err)
				uid = uint64(defUid)
			}
		}

		detailinfo, err := groupLessonApi.GetGroupLessonInfo(uint32(uid), []byte(obid))
		if err != nil {
			infoLog.Println("GetGroupLessonInfo error:", err)
			c.AbortWithStatus(404)
			return
		}

		// 用户是否在群里
		isIn := detailinfo.GetIsUserInGroup()

		infoLog.Printf("uid = %d, isIn:%d \n", uid, isIn)

		groupLesson := detailinfo.GetGroupLesson()

		roomInfo, err := mucApi.GetRoomInfo(groupLesson.GetRoomId(), uint32(uid), uint64(0))
		infoLog.Printf("GetRoomInfo param, roomId:%d reqUid:%d  roomInfo: %+v \n", groupLesson.GetRoomId(), uid, roomInfo)
		if err != nil {
			infoLog.Println("GetRoomInfo error:", err)
			c.AbortWithStatus(404)
			return
		}

		// infoLog.Printf("detailinfo=%#v groupLesson=%+v", *detailinfo, groupLesson)

		assign := parseLessonInfo(detailinfo, roomInfo)
		assign["CDN_HOST"] = staticRoot
		assign["obid"] = obid

		// 把课程信息转成json输出
		pbUrlList := groupLesson.GetRecordUrlList()
		urlList := make([]RecordUrl, len(pbUrlList))
		for k, v := range pbUrlList {
			urlList[k] = PbRecordToJsonRecord(v)
		}

		lessonInfo := simplejson.New()
		lessonInfo.Set("RoomId", assign["RoomOpenid"])
		lessonInfo.Set("LessonObid", obid)
		if isIn != 0 {
			lessonInfo.Set("RecordUrlList", urlList)
		} else {
			lessonInfo.Set("RecordUrlList", urlList)
		}
		lessonInfo.Set("isInRoom", isIn)
		if openid != "" {
			lessonInfo.Set("Openid", openid)
		}
		jsonInfo, err := lessonInfo.MarshalJSON()
		if err != nil {
			infoLog.Println("Marshal lessonInfo error:", err)
		}

		assign["lessonInfo"] = string(jsonInfo)
		assign["serviceAccessPrefix"] = serviceAccessPrefix
		assign["Openid"] = openid
		assign["frontEndConfig"] = feConf

		// 处理多语言
		acceptLanguage := GetAcceptLanguage(c.GetHeader("Accept-Language"))
		infoLog.Printf("acceptLanguage : %s", acceptLanguage)

		// 多语言替换
		placeHolderKey := []string{
			"gl_lesson_time",
			"gl_from_group",
			"gl_lesson_description",
			"gl_teacher_description",
			"gl_record",
			"gl_join_group",
			"gl_contact_teacher",

			"gl_hide",
			"gl_more",
			"gl_check_record",
			"gl_no_time",
			"gl_no_abstract",
			"gl_no_record",
		}
		placeHolderList := GetPlaceHolderList(placeHolderKey, acceptLanguage)
		infoLog.Printf("placeHolderList : %+v", placeHolderList)
		// 把翻译结果合并进assign
		MergeStringMapGinH(assign, placeHolderList)
		// infoLog.Printf("assign : %+v", assign)

		c.HTML(200, sharePage, assign)
	})

	// 查看课程录像
	router.GET(apiList["lessonRecord"]+"/:record_obid/:lesson_obid", func(c *gin.Context) {
		recordObid := c.Param("record_obid")
		if recordObid == "" {
			c.AbortWithStatus(404)
		}

		lessonObid := c.Param("lesson_obid")
		if lessonObid == "" {
			c.AbortWithStatus(404)
		}

		// 处理userid
		openid := c.DefaultQuery("openid", "")
		infoLog.Printf("lessonRecord openid:%s len=%d", openid, len(openid))
		if len(openid) == 0 {
			c.AbortWithStatus(403)
		}
		uid, err := DecodeBigOpenid(openid)
		if err != nil || uid == 0 {
			// c.AbortWithStatus(403)
			uid = 0
		}

		// 处理多语言
		acceptLanguage := GetAcceptLanguage(c.GetHeader("Accept-Language"))
		infoLog.Printf("acceptLanguage : %s", acceptLanguage)
		// 获取课程信息
		detailinfo, err := groupLessonApi.GetGroupLessonInfo(uint32(uid), []byte(lessonObid))
		if err != nil {
			infoLog.Println("GetGroupLessonInfo error:", err)

			// 多语言替换
			placeHolderKey := []string{
				"gl_video_deleted",
			}
			placeHolderList := GetPlaceHolderList(placeHolderKey, acceptLanguage)

			c.HTML(200, recordDelPage, gin.H{
				"video_del_pic":    videoDelPic,
				"LessonTitle":      "",
				"gl_video_deleted": placeHolderList["gl_video_deleted"],
			})
			return
		}
		// 获取房间信息
		groupLesson := detailinfo.GetGroupLesson()
		roomInfo, err := mucApi.GetRoomInfo(groupLesson.GetRoomId(), uint32(uid), uint64(0))
		// infoLog.Printf("GetRoomInfo param, roomId:%d reqUid:%d  roomInfo: %+v \n", groupLesson.GetRoomId(), uid, roomInfo)
		if err != nil {
			infoLog.Println("GetRoomInfo error:", err)
			c.AbortWithStatus(404)
			return
		}

		assign := parseLessonInfo(detailinfo, roomInfo)
		// 获取录像信息
		recordInfo, err := groupLessonApi.GetRecordInfo([]byte(recordObid), uint32(uid))
		if err != nil {
			// 查不到，视为已经被删除
			infoLog.Printf("GetRecordInfo error : %+v obid :%s uid %d", err, string(recordObid), uid)
			// 多语言替换
			placeHolderKey := []string{
				"gl_video_deleted",
			}
			placeHolderList := GetPlaceHolderList(placeHolderKey, acceptLanguage)
			MergeStringMapGinH(assign, placeHolderList)

			assign["video_del_pic"] = videoDelPic
			assign[""] = videoDelPic
			c.HTML(200, recordDelPage, assign)
			return
		}

		// 管理员信息
		roomAdminList := roomInfo.GetListAdminUid()
		infoLog.Printf("roomAdminList %v", roomAdminList)
		adminOpenidList := make([]string, len(roomAdminList))
		for _, v := range roomAdminList {
			adminOpenidList = append(adminOpenidList, EncodeBigOpenid(uint64(v), bigOpenidSalt))
		}

		recordUrl := recordInfo.GetRecordInfo()
		st := time.Unix(int64(recordUrl.GetBeginTimeStamp()/1000), 0)

		// 处理pptUrl 和 voiceUrl
		voiceUrl := strings.Replace(string(recordUrl.GetVoiceUrl()), accOldHost, CDN, 1)

		assign["CDN_HOST"] = staticRoot
		assign["pptUrl"] = string(recordUrl.GetPptUrl())
		assign["voiceUrl"] = voiceUrl
		assign["beginTime"] = st.Format("2006-01-02 15:04:05")
		assign["openid"] = openid
		assign["recordObid"] = recordObid
		assign["serviceAccessPrefix"] = serviceAccessPrefix
		assign["frontEndConfig"] = feConf

		adminOpenidList = append(adminOpenidList, assign["TeacherOpenid"].(string))
		infoLog.Printf("adminOpenidList %v", adminOpenidList)
		assign["adminList"] = adminOpenidList

		if assign["LessonCover"] == "" {
			assign["LessonCover"] = lessonPicPlaceHolder
		}

		// 多语言替换
		placeHolderKey := []string{
			"gl_lesson_time",
		}
		placeHolderList := GetPlaceHolderList(placeHolderKey, acceptLanguage)
		infoLog.Printf("placeHolderList : %+v", placeHolderList)
		// 把翻译结果合并进assign
		MergeStringMapGinH(assign, placeHolderList)

		// infoLog.Printf("assign info : %+v", assign)

		c.HTML(200, recordPage, assign)
	})

	// 查询录像消息记录
	router.GET(apiList["recordMsg"]+"/:obid", func(c *gin.Context) {
		obid := c.Param("obid")
		if obid == "" {
			c.AbortWithStatus(404)
		}

		roomidQuery := c.Query("roomid")
		roomid, err := DecodeBigOpenid(roomidQuery)
		if err != nil {
			infoLog.Printf("recordMsg roomid decode failed: openid %d roomidQuery %s err %v", roomid, roomidQuery, err)
		}

		cliseq := c.DefaultQuery("cliseq", "0")
		seq, err := strconv.Atoi(cliseq)
		if err != nil {
			c.AbortWithStatus(403)
		}

		curMaxSeq, historyMsg, err := groupLessonApi.GetRecordChatMsgList([]byte(obid), defUid, uint32(roomid), uint64(seq))
		if err != nil {
			infoLog.Printf("GetRecordChatMsgList error : %+v", err)
		}
		// 加密userid
		for _, v := range historyMsg {
			// infoLog.Printf("historyMsg %d : %+v", k, v)
			senderId, err := v.Get("sender_id").Int()
			if err != nil {
				infoLog.Printf("simplejson.Int() error : %s,  historyMsg: %+v", err, v)
				continue
			}
			v.Set("sender_id", EncodeBigOpenid(uint64(senderId), bigOpenidSalt))
			v.Set("room_id", roomidQuery)
		}

		c.JSON(200, gin.H{
			"status":      0,
			"data":        historyMsg,
			"cur_max_seq": curMaxSeq,
		})
	})

	// 查询用户信息
	router.GET(apiList["userInfo"], func(c *gin.Context) {
		openidStr := c.Query("openid")
		if openidStr == "" {
			c.AbortWithStatus(404)
		}

		openidList := strings.Split(openidStr, ",")
		data := make(map[string]*simplejson.Json, len(openidList))
		for _, v := range openidList {
			userid, err := DecodeBigOpenid(v)
			if err != nil {
				infoLog.Printf("userInfo DecodeBigOpenid fail v:%+v err:%s\n", v, err)
			}
			info, err := getUserInfo(uint32(userid))
			if err != nil {
				infoLog.Printf("getUserInfo fail v:%s , userid:%d err:%s\n", v, userid, err)
			} else {
				headUrl, err := info.Get("HU").String()
				if err != nil {
					infoLog.Printf("parse HU fail v:%s , userid:%d err:%s\n", v, userid, err)
				} else {
					info.Set("HU", headUrlPrefix+headUrl)
				}
				data[v] = info
			}
		}

		c.JSON(200, gin.H{
			"status": 0,
			"data":   data,
		})
	})

	// 缓存获取url文件，用于解决前端跨域问题
	fileCache = make(map[string]string, 100)
	router.GET(apiList["getFileData"], func(c *gin.Context) {
		url := c.Query("url")
		if url == "" {
			c.AbortWithStatus(404)
		}

		content, err := getCacheFile(url)
		if err != nil {
			infoLog.Println("getCacheFile err : ", err)
			c.AbortWithStatus(404)
		}
		content = strings.Replace(content, "http://", "//", -1)
		content = strings.Replace(content, "https://", "//", -1)
		c.String(200, content)
	})

	router.GET("/s/gl/encoder/:fun/:id", func(c *gin.Context) {
		fun := c.Param("fun")
		id := c.Param("id")
		data := ""
		if fun == "EncodeBigOpenid" {
			intid, err := strconv.Atoi(id)
			if err != nil {
				c.AbortWithStatus(500)
			}
			re := EncodeBigOpenid(uint64(intid), bigOpenidSalt)
			data = re
		} else {
			re, err := DecodeBigOpenid(id)
			if err != nil {
				c.AbortWithStatus(500)
			}
			infoLog.Println("re=", re)
			data = fmt.Sprintf("%d", re)
		}
		c.JSON(200, gin.H{
			"status": 0,
			"data":   data,
		})
	})

	router.Run(srvIP + ":" + srvPort)
}
