#!/bin/bash
CURDIR=`pwd`

go build $CURDIR/lesson_share.go

cp -f lesson_share /home/ht/goproj/lesson_share/bin/.
cp -r tpl /home/ht/goproj/lesson_share/bin/.

sleep 1
killall lesson_share
