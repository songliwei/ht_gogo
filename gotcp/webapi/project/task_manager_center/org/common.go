package org

import (
	"errors"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strings"
	"time"
)

type CommonHttpTask struct {
	Id        uint64            `json:"id" binding:"required"`          //
	Source    string            `json:"source" binding:"required"`      //
	Url       string            `json:"url" binding:"required"`         //
	BackUpUrl string            `json:"backup_url" binding:"omitempty"` // 备用的url
	Method    string            `json:"method" binding:"required"`      //
	Data      string            `json:"data" binding:"required"`        //要post的数据
	Header    map[string]string `json:"header" binding:"required"`      //http 头
	Timeout   uint32            `json:"timeout" binding:"required"`     //超时时间
}

const (
	TT_ERROR_CODE_BAD_PARAM   = 999
	TT_ERROR_CODE_CACHE_ERR   = 11
	TT_ERROR_CODE_TIMEOUT     = 12
	TT_ERROR_CODE_JSON_DECODE = 13
)

func SendHttpByTask(task *CommonHttpTask) ([]byte, error) {

	client := &http.Client{
		Transport: &http.Transport{
			Dial: func(netw, addr string) (net.Conn, error) {
				c, err := net.DialTimeout(netw, addr, time.Second*time.Duration(task.Timeout)) //设置建立连接超时
				if err != nil {
					log.Println("SendHttpByTask() DialTimeout:", err.Error(), task.Source, task.Id, task.Url)
					return nil, err
				}
				c.SetDeadline(time.Now().Add(time.Duration(task.Timeout) * time.Second)) //设置发送接收数据超时
				return c, nil
			},
		},
		Timeout: time.Second * time.Duration(task.Timeout),
	}

	log.Printf("SendHttpByTask() task=%#v", task)

	req, err := http.NewRequest(task.Method, task.Url, strings.NewReader(task.Data))

	if err != nil {
		log.Println("SendHttpByTask() NewRequest:", err.Error(), task.Source, task.Url)
		return nil, err
	}

	for k, v := range task.Header {
		req.Header.Add(k, v)
	}

	resp, err := client.Do(req)

	if err != nil {
		log.Println("SendHttpByTask() Do:", err.Error(), task.Source, task.Url)
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("SendHttpByTask() Do:", err.Error(), task.Source, task.Url)
		return nil, err
	}

	return body, nil
}

// 发送通用http 请求
func HttpNotify(task *CommonHttpTask, callback string) (code uint32, err error) {

	if task == nil {
		return TT_ERROR_CODE_BAD_PARAM, errors.New("bad url")
	}

	output, err := SendHttpByTask(task)

	if err != nil {

		if task.BackUpUrl != "" && task.BackUpUrl != task.Url {

			log.Println("HttpNotify() SendHttpByTask fail retry with backup:", err.Error(), task.Source, task.Url, task.BackUpUrl)
			task.Url = task.BackUpUrl
			output, err = SendHttpByTask(task)

		} else {

			return 1, err
		}

	}

	if err != nil {
		log.Println("HttpNotify() fail retry :", err.Error(), task.Source, task.Url)
		return 1, err
	}

	strOutput := string(output)

	log.Printf("HttpNotify() Source=[%s],Id=[%d],Url=[%s],strOutput=[%s]", task.Source, task.Id, task.Url, strOutput)

	if callback != "" {
		callbackTask := &CommonHttpTask{
			Url:     callback,
			Method:  "POST",
			Data:    strOutput,
			Timeout: 3,
			Id:      task.Id,
			Source:  task.Source + "_callback",
		}

		HttpNotify(callbackTask, "")
	}

	return 0, nil
}
