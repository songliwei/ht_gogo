package org

import (
	"fmt"

	simplejson "github.com/bitly/go-simplejson"
	tcpcommon "github.com/gansidui/gotcp/tcpfw/common"
	"github.com/gansidui/gotcp/tcpfw/include/ht_p2p"
	"github.com/gansidui/gotcp/webapi/common"
	kforg "github.com/gansidui/gotcp/webapi/project/kf_center/org"
	"github.com/golang/protobuf/proto"
	"log"
	"net/url"
	"strings"
	"time"
)

type MessageTaskCenter struct {
	p2pSvr             *tcpcommon.P2PWorkerApiV2
	publicAccountRedis *tcpcommon.RedisApi
	infoLog            *log.Logger
	notifyMessageProxy string
}

type NormalP2PMessageTask struct {
	Fromid      uint32 `json:"fromid" binding:"required"`       //
	Toid        uint32 `json:"toid" binding:"required"`         //
	MessageJson string `json:"message_json" binding:"required"` //已经构造好的消息数据json 字符串形式
	Push        uint32 `json:"push" binding:"omitempty"`
	ToDevice    uint32 `json:"to_device" binding:"omitempty"` //发到pc 还是手机，还是都发。to_type = SEND_TO_ALL=0,SEND_TO_MOBILE=1,SEND_TO_PC=2
	Sleep       uint32 `json:"sleep" binding:"omitempty"`     //多条消息的情况下，间隔毫秒
}

type PubAccountTask struct {
	Userid   uint32 `json:"userid" binding:"required"`    //
	HpUserid uint32 `json:"hp_userid" binding:"required"` //
	Push     uint32 `json:"push" binding:"omitempty"`
	Sleep    uint32 `json:"sleep" binding:"omitempty"` //多条消息的情况下，间隔毫秒
}

//infoLog *log.Logger, Masterdb *sql.DB, RulerRedis, UserinfoRedis *tcpcommon.RedisApi, momentAPI *tcp
func NewMessageTaskCenter(p2pSvr *tcpcommon.P2PWorkerApiV2, publicAccountRedis *tcpcommon.RedisApi, infoLog *log.Logger, notifyMessageProxy string) *MessageTaskCenter {

	return &MessageTaskCenter{
		p2pSvr:             p2pSvr,
		publicAccountRedis: publicAccountRedis,
		infoLog:            infoLog,
		notifyMessageProxy: notifyMessageProxy,
	}
}

func (this *MessageTaskCenter) HandlerP2PMessage(Task *NormalP2PMessageTask, ServerTs uint64) (code uint32, err error) {

	rootObj, err := simplejson.NewJson([]byte(Task.MessageJson))
	if err != nil {
		this.infoLog.Printf("PublicAccountInitMessage() simplejson new packet error", err)
		return TT_ERROR_CODE_BAD_PARAM, err
	}

	rootObj.Set("msg_id", fmt.Sprintf("%d_%d_%d", Task.Fromid, Task.Toid, ServerTs))
	rootObj.Set("server_ts", ServerTs)

	if Task.Sleep > 0 {

		if common.GetMilliNow()-ServerTs < uint64(Task.Sleep) {
			time.Sleep(time.Duration(Task.Sleep) * time.Millisecond)
		}

	}
	code, err = this.SendP2PMessage(rootObj, Task.Fromid, Task.Toid, Task.Push, Task.ToDevice)

	if err != nil {
		this.infoLog.Println("HandlerP2PMessage() SendP2PMessage fail", Task, err, ",ServerTs=", ServerTs)
	}

	return
}

func (this *MessageTaskCenter) PublicAccountInitMessage(Task *PubAccountTask, ServerTs uint64) (code uint32, err error) {

	key := fmt.Sprintf("HPU_%d", Task.HpUserid)

	vals, err := this.publicAccountRedis.Hmget(key, []string{"init_msg", "init_msg2"})

	if err != nil {
		return TT_ERROR_CODE_CACHE_ERR, err
	}

	for k, v := range vals {
		if v != "" {

			rootObj, err := simplejson.NewJson([]byte(v))
			if err != nil {
				this.infoLog.Printf("PublicAccountInitMessage() simplejson new packet error", err)
				continue
			}

			rootObj.Set("msg_id", fmt.Sprintf("%d_%d_%d", Task.Userid, Task.HpUserid, ServerTs))
			rootObj.Set("server_ts", ServerTs+uint64(k))

			this.ModifyP2PMessage(rootObj, this.notifyMessageProxy, Task.Userid, Task.HpUserid)
			if Task.Sleep > 0 {
				time.Sleep(time.Duration(Task.Sleep) * time.Millisecond)
			}
			code, err = this.SendP2PMessage(rootObj, Task.HpUserid, Task.Userid, Task.Push, 0)

			if err != nil {
				this.infoLog.Println("PublicAccountInitMessage() SendP2PMessage fail", Task, err, ",ServerTs=", ServerTs)
			}
		}
	}

	return
}

func (this *MessageTaskCenter) ModifyP2PMessage(rootObj *simplejson.Json, NotifyProxy string, userid, hpUserid uint32) {

	if rootObj != nil && rootObj.Get("msg_type").MustString() == "notify" {
		NotifyObj := rootObj.Get("notify")
		// 群固有属性
		Type := NotifyObj.Get("type").MustInt(-1)
		Url := NotifyObj.Get("url").MustString()

		if Type == 0 && Url != "" {

			if strings.Contains(Url, "hellotalk.org") || strings.Contains(Url, "hellotalk.com") {

				StrHpuserid := fmt.Sprintf("%d", hpUserid)
				NotifyObj.Set("url", Url+"/hp_userid/"+StrHpuserid)

			} else {

				if NotifyProxy == "" {
					return
				}

				openId := common.EncodeBigOpenid(uint64(userid), uint64(hpUserid))

				proxyUrl := fmt.Sprintf("%s/api/htm/notify_url?link=%s&openid=%s", NotifyProxy, url.QueryEscape(Url), openId)

				NotifyObj.Set("url", proxyUrl)

				this.infoLog.Println(openId, " SendP2PMessage() ", proxyUrl, Url)

			}

			rootObj.Set("notify", NotifyObj)
		}

	}

}

//fromid,toid,push_sound,to_type
// to_type = SEND_TO_ALL=0,SEND_TO_MOBILE=1,SEND_TO_PC=2
func (this *MessageTaskCenter) SendP2PMessage(rootObj *simplejson.Json, Fromid, Toid, pushSound, toType uint32) (uint32, error) {

	buff, err := rootObj.MarshalJSON()

	if err != nil {
		return TT_ERROR_CODE_BAD_PARAM, err
	}

	p2pBuff := common.CompressString(buff)

	pbBody := &ht_p2p.P2PMsgBody{
		JustOnline: proto.Uint32(0),
		DontReply:  proto.Uint32(1),
		P2PData:    p2pBuff,
		ToType:     ht_p2p.TO_CLIENT_TYPE(int32(toType)).Enum(),
	}

	this.infoLog.Println("buff=", string(buff))

	msgType := rootObj.Get("msg_type").MustString()

	if msgType == "notify" {
		pushSound = 0
	}

	if pushSound > 0 {

		PushType := kforg.GetPushTypeByMessageType(msgType)

		pbBody.PushInfo = &ht_p2p.PushInfo{
			PushType:  &PushType,
			NickName:  []byte(rootObj.Get("from_nickname").MustString()),
			PushSound: &pushSound,
		}
		// 只有文字类型才添加推送内容
		if PushType == kforg.PUSH_TEXT {
			pbBody.PushInfo.Content = []byte(rootObj.Get("text").Get("text").MustString())
		}

		// this.infoLog.Printf("SendP2PMessage() Found push info, build result: %+v", pbBody.PushInfo)
	}

	payLoad, err := proto.Marshal(pbBody)

	if err != nil {
		this.infoLog.Println("SendP2PMessage() proto.Marshal failed", Fromid, Toid)
		return TT_ERROR_CODE_BAD_PARAM, err
	}

	head := &tcpcommon.HeadV3{}
	head.Flag = 0XF0
	head.CryKey = 0
	head.Version = 0x04
	head.TermType = 0
	head.Seq = 1
	head.Cmd = kforg.CMD_P2P_MESSAGE
	head.From = Fromid
	head.To = Toid
	head.Len = uint32(len(payLoad) + tcpcommon.HeadV3Len)

	if msgType == "notify" {
		head.Cmd = kforg.CMD_P2P_PUBLIC_MESSAGE
	}

	// return err;
	ret, err := this.p2pSvr.SendPacket(head, payLoad)
	if err != nil {
		this.infoLog.Printf("p2pSvr.SendPacket() err:%v, ret:%v", err, ret)
		return 502, err
	}

	return uint32(ret), nil
}
