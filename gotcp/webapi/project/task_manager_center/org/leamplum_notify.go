package org

import (
	"errors"
	"fmt"
	"log"
)

type LeanplumTask struct {
	Id            uint64 `json:"id" binding:"required"`              //随机的
	Source        string `json:"source" binding:"required"`          //
	Userid        uint32 `json:"userid" binding:"required"`          //
	Action        string `json:"action" binding:"required"`          //
	AdditionParam string `json:"addition_param" binding:"omitempty"` // LP参数
	Ostype        string `json:"ostype" binding:"required"`          //使用字符串，防止0 作为 默认值
}

const (
	IOS_LP_APP_ID     = "app_npy0CmR2cfUQ2xdJbTjFoJOixexrYRByuwbftRyaIGI"
	IOS_LP_CLIENT_KEY = "prod_eMFSfEHK2rudw7n7fvDWX2cYLMglpZ2NBrZhkxp3sRs"

	IOS_APPSFLYER_DEV_ID = "24x87VxiXLw2VYVi7dstfB"
	IOS_BUNDLE_ID        = "com.helloTalk.helloTalk"
	IOS_APPSFLYER_URL    = "https://api2.appsflyer.com/inappevent/id557130558"

	ANDORID_LP_APP_ID     = "app_xhrgeGWGWnlfA2oaAE7YCJ0AoEQIanQGAONyhG9v6ic"
	ANDORID_LP_CLIENT_KEY = "prod_MpVW2KtSIbDqyJ8XDaF64Ql9lWj2ZxJkhhAjrqjC8yQ"

	ANDORID_APPSFLYER_DEV_ID = "24x87VxiXLw2VYVi7dstfB"
	ANDORID_APPSFLYER_URL    = "https://api2.appsflyer.com/inappevent/com.hellotalk"

	LP_HOST = "https://www.leanplum.com/api"
)

type FT_GET_OSTYPE func(uint32) string

// 发送事件到Leampum
//action 只有 track,setUserAttributes
//additionParam 是添加在 querystring 后面的额外参数
//ostype 描述的机型，用来区分不同的key
func ToLeanplum(task *LeanplumTask, callback string, getostype FT_GET_OSTYPE) (code uint32, err error) {

	url := ""

	if task.Userid < 1000 {
		return TT_ERROR_CODE_BAD_PARAM, errors.New("bad userid")
	}

	if task.Ostype == "" {
		task.Ostype = getostype(task.Userid)
	}

	log.Println("ToLeanplum() task=", task)

	if task.Ostype == "0" {
		url = fmt.Sprintf("%s?appId=%s&clientKey=%s&apiVersion=1.0.6&userId=%d&action=%s&%s", LP_HOST, IOS_LP_APP_ID, IOS_LP_CLIENT_KEY, task.Userid, task.Action, task.AdditionParam)
	} else {
		url = fmt.Sprintf("%s?appId=%s&clientKey=%s&apiVersion=1.0.6&userId=%d&action=%s&%s", LP_HOST, ANDORID_LP_APP_ID, ANDORID_LP_CLIENT_KEY, task.Userid, task.Action, task.AdditionParam)
	}

	httpTask := &CommonHttpTask{
		Id:      task.Id,
		Source:  task.Source,
		Url:     url,
		Method:  "GET",
		Data:    "",
		Timeout: 10,
	}

	return HttpNotify(httpTask, callback)

}
