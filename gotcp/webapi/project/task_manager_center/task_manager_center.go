package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gansidui/gotcp/libcomm"
	tcpcommon "github.com/gansidui/gotcp/tcpfw/common"
	"github.com/gansidui/gotcp/webapi/common"
	"github.com/gansidui/gotcp/webapi/project/task_manager_center/org"
	_ "github.com/go-sql-driver/mysql"
	"github.com/nsqio/go-nsq"
	"log"
	"os"
	"os/signal"
	"runtime"
	// "strings"
	"syscall"
	"time"
	// "strconv"
)

// 通过一个独立的任务管理器去对接第三方的任务

const (
	TT_LEANPLUM_NOTIFY  = 1 //通知到 leamplum
	TT_COMMON_HTTP_PUSH = 2 //一般的http 通知，例如 yoli购买。通过 source 来区分。
	TT_GROUP_MESSAGE    = 3 //群聊
	TT_P2P_MESSAGE      = 4 //单人聊
	TT_CREATE_ROOM      = 5 //appsfly 通知

	TT_PUBLIC_ACCOUNT_INIT_MESSAGE = 6 //初始化推送公众号消息给客户端，达到曝光的目的
)

type TaskEvent struct {
	UserId uint32 `json:"userid" binding:"omitempty"` //要处理或者操作的userid，根据 TaskType 决定的具体作用。

	TaskType uint32 `json:"type" binding:"required"` //任务类型

	Timing uint32 `json:"time" binding:"omitempty"` //便于以后的定时任务

	TaskSource string `json:"source" binding:"required"` //任务类型来源，仅仅做区分

	Ostype uint8 `json:"ostype" binding:"omitempty"` //用户的机型，具体作用有TaskType决定，

	Data string `json:"data" binding:"required"` //任务的具体描述，是一个json 字符串。需要根据 event_type再 decode 到具体的结构体

	CallBack string `json:"callback" binding:"omitempty"` //回调接口，将任务的返回值以json的形式返回到该接口。

	FailOver int `json:"fail_over" binding:"omitempty"` //失效备缓，如果当前处理失败了，记录到 FailCacheChan，同时该值减1

	FailSleep uint32 `json:"fail_sleep" binding:"omitempty"` //失效之后多少秒重试

	ServerTs uint64 `json:"server_ts" binding:"omitempty"` //任务开始的时间戳。毫秒级

}

var (
	SlaveDB     *sql.DB
	infoLog     *log.Logger
	mcUserState *tcpcommon.MemcacheApi

	options common.Options

	skipLeamplum          int
	workerCount           int
	GlobalRecvMsgPackChan chan *TaskEvent //数量为1 的队列，只消费当前队列里面的

	messageTaskCenter *org.MessageTaskCenter

	FailCacheChan chan *TaskEvent //首次失败的情况下的延后处理
)

func MessageHandle(message *nsq.Message) error {

	attr := "taskcenter/total_task"
	libcomm.AttrAdd(attr, 1)

	info := &TaskEvent{}
	err := json.Unmarshal(message.Body, info) // JSON to Struct

	if err != nil {
		infoLog.Printf("MessageHandle err=%s,body=%s", err.Error(), string(message.Body))
		return err
	}

	infoLog.Println("MessageHandle get ", info.TaskType, message.Timestamp, string(message.Body))

	GlobalRecvMsgPackChan <- info

	return nil
}

func TaskHandler(task *TaskEvent, idx int) error {

	code := uint32(0)
	err := errors.New("ok")

	attr := "taskcenter/source_" + task.TaskSource
	libcomm.AttrAdd(attr, 1)

	attr = fmt.Sprintf("taskcenter/type_%d", task.TaskType)
	libcomm.AttrAdd(attr, 1)

	switch task.TaskType {

	case TT_P2P_MESSAGE:
		httpTask := &org.NormalP2PMessageTask{}
		err = json.Unmarshal([]byte(task.Data), httpTask) // JSON to Struct

		if err != nil {
			infoLog.Printf("TaskHandler() NormalP2PMessageTask,json decode fail,err", err.Error())
			return err
		}

		code, err = messageTaskCenter.HandlerP2PMessage(httpTask, task.ServerTs)

	case TT_PUBLIC_ACCOUNT_INIT_MESSAGE:

		httpTask := &org.PubAccountTask{}
		err = json.Unmarshal([]byte(task.Data), httpTask) // JSON to Struct

		if err != nil {
			infoLog.Printf("TaskHandler() TT_PUBLIC_ACCOUNT_INIT_MESSAGE,json decode fail,err", err.Error())
			return err
		}

		code, err = messageTaskCenter.PublicAccountInitMessage(httpTask, task.ServerTs)

	case TT_LEANPLUM_NOTIFY:

		httpTask := &org.LeanplumTask{}
		err = json.Unmarshal([]byte(task.Data), httpTask) // JSON to Struct

		if err != nil {
			infoLog.Printf("TaskHandler() TT_LEANPLUM_NOTIFY,json decode fail,err", err.Error())
			return err
		}

		if skipLeamplum == 1 {

			infoLog.Printf("TaskHandler() TT_COMMON_HTTP_PUSH,httpTask=%#v", httpTask)
			err = errors.New("skip by config")

		} else {
			code, err = org.ToLeanplum(httpTask, task.CallBack, GetStrOstype)
		}

	case TT_COMMON_HTTP_PUSH:

		httpTask := &org.CommonHttpTask{}
		err = json.Unmarshal([]byte(task.Data), httpTask) // JSON to Struct

		if err != nil {
			infoLog.Printf("TaskHandler() TT_COMMON_HTTP_PUSH,json decode fail,err", err.Error())
			return err
		}
		httpTask.Source = task.TaskSource
		code, err = org.HttpNotify(httpTask, task.CallBack)

	}

	if err != nil {
		infoLog.Printf("TaskHandler() TaskType=%d,Userid=%d,source=%s,retcode=%d,fail err=%s", task.TaskType, task.UserId, task.TaskSource, code, err.Error())
	} else {
		infoLog.Printf("TaskHandler() TaskType=%d,Userid=%d,source=%s,retcode=%d done", task.TaskType, task.UserId, task.TaskSource, code)
	}

	//999表示参数错误，不再重试
	if code != 0 && code != org.TT_ERROR_CODE_BAD_PARAM {
		if task.FailOver > 0 {
			task.FailOver--

			if err != nil {

				infoLog.Printf("TaskHandler() type=%d,failcnt=%d,sleep=%d,code=%d,err=%s",
					task.TaskType, task.FailOver, task.FailSleep, code, err.Error())
			} else {

				infoLog.Printf("TaskHandler() type=%d,failcnt=%d,sleep=%d,code=%d,",
					task.TaskType, task.FailOver, task.FailSleep, code)
			}

			FailCacheChan <- task
		}
	}

	return nil
}

func GetOstype(Userid uint32) (uint8, error) {

	userState, err := mcUserState.GetUserOnlineStat(Userid)
	if err != nil {

		infoLog.Printf("GetOstype() GetUserOnlineStat %d failed: %+v", Userid, err)

		Type := uint8(0)

		err = SlaveDB.QueryRow("SELECT TTYPE FROM HT_USER_STATE WHERE USERID = ? LIMIT 1", Userid).Scan(&Type)

		if err != nil {
			infoLog.Printf("GetOstype() HT_USER_STATE %d failed: %s", Userid, err.Error())
		}

		return Type, err

	} else {

		return userState.ClientType, nil

	}

}

func GetStrOstype(Userid uint32) string {

	code, _ := GetOstype(Userid)

	return fmt.Sprintf("%d", code)
}

func main() {

	runtime.GOMAXPROCS(runtime.NumCPU())
	cfg, err := common.InitLogAndOption(&options, &infoLog)

	SlaveDB, err = common.InitMySqlFromSection(cfg, infoLog, "SLAVE_MYSQL", 3, 1)
	common.CheckError(err)
	defer SlaveDB.Close()

	// 建立Memcache链接
	mcIp := cfg.Section("MEMCACHE").Key("mc_ip").MustString("127.0.0.1")
	mcPort := cfg.Section("MEMCACHE").Key("mc_port").MustString("11211")
	mcUserState = new(tcpcommon.MemcacheApi)
	mcUserState.Init(mcIp + ":" + mcPort)

	// 读取P2PWorker 配置
	p2p_ip := cfg.Section("P2PWORKER").Key("p2p_ip").MustString("127.0.0.1")
	p2p_port := cfg.Section("P2PWORKER").Key("p2p_port").MustString("61200")
	p2pSvr := tcpcommon.NewP2PWorkerApiV2(p2p_ip,
		p2p_port,
		time.Minute,
		time.Minute,
		&tcpcommon.HeadV3Protocol{},
		100)

	redis_host := cfg.Section("REDIS").Key("public_account_redis_ip").MustString("127.0.0.1")
	redis_port := cfg.Section("REDIS").Key("public_account_redis_port").MustString("6379")

	publicAccountRedis := tcpcommon.NewRedisApi(redis_host + ":" + redis_port)

	publiAccountProxy := cfg.Section("SERVICE_CONFIG").Key("public_account_proxy_host").MustString("http://qtest.hellotalk.org ")

	messageTaskCenter = org.NewMessageTaskCenter(p2pSvr, publicAccountRedis, infoLog, publiAccountProxy)
	// mucIp := cfg.Section("MUC").Key("muc_ip").MustString("127.0.0.1")
	// mucPort := cfg.Section("MUC").Key("muc_port").MustString("11750")
	// mucConnLimit := cfg.Section("MUC").Key("pool_limit").MustInt(1000)
	// infoLog.Printf("muc server ip=%v port=%v connLimit=%v", mucIp, mucPort, mucConnLimit)
	// mucApi = tcpcommon.NewMucApi(mucIp, mucPort, 3*time.Second, 3*time.Second, &tcpcommon.HeadV3Protocol{}, mucConnLimit)

	lookupdHost := cfg.Section("LOOKUPD").Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section("MESSAGE").Key("topic").MustString("test")
	channel := cfg.Section("MESSAGE").Key("chan").MustString("ch")

	workerCount = cfg.Section("SERVICE_CONFIG").Key("worker_count").MustInt(10)
	skipLeamplum = cfg.Section("SERVICE_CONFIG").Key("skip_leamplum").MustInt(0)

	FailCacheChan = make(chan *TaskEvent, 1204)

	GlobalRecvMsgPackChan = make(chan *TaskEvent)

	for i := 0; i < workerCount; i++ {

		go func(idx int) {

			for {
				info := <-GlobalRecvMsgPackChan

				TaskHandler(info, idx)
			}

		}(i)

	}

	//失败的异常队列 重试机制
	for i := 0; i < 2; i++ {

		go func(idx int) {

			for {
				taskInfo := <-FailCacheChan
				if taskInfo.FailOver >= 0 {

					if taskInfo.FailSleep > 0 {
						//需要延时的任务
						go func(failTask *TaskEvent) {
							time.Sleep(time.Second * time.Duration(failTask.FailSleep))
							TaskHandler(failTask, idx)
						}(taskInfo)
					} else {

						TaskHandler(taskInfo, idx)
					}

				}
			}

		}(i)

	}

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(topic, channel, config)
	q.AddHandler(nsq.HandlerFunc(MessageHandle))
	err = q.ConnectToNSQLookupd(lookupdHost)
	if err != nil {
		log.Printf("main Could not connect")
	}

	chSig := make(chan os.Signal)
	signal.Notify(chSig, syscall.SIGINT, syscall.SIGTERM)
	log.Println("Signal: ", <-chSig)

}
