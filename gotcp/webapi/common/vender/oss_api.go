package vender

import (
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	// "github.com/jessevdk/go-flags"
	"gopkg.in/ini.v1"
	"log"
)

type OSSAPI struct {
	OSSBukect string
	OSSRegion string
	OSSAppID  string
	OSSPrefix string
	OSSAppKey string
	infoLog   *log.Logger
}

// func InitMySqlFromSection(cfg *ini.File, infoLog *log.Logger, secName string, OpenCount int, OdleCount int) (*sql.DB, error) {

func NewOSSAPI(cfg *ini.File, infoLog *log.Logger) (*OSSAPI, error) {

	// init mysql
	OSSBukect := cfg.Section("SERVICE_CONFIG").Key("oss_bucket").MustString("ht-blog")
	OSSRegion := cfg.Section("SERVICE_CONFIG").Key("oss_region").MustString("oss-cn-hongkong.aliyuncs.com")
	OSSAppID := cfg.Section("SERVICE_CONFIG").Key("oss_appid").MustString("ULuE3LK66Y6ZlEWS")
	OSSAppKey := cfg.Section("SERVICE_CONFIG").Key("oss_appkey").MustString("JTFyxX7TW9K1TSVMwglj272CxfvwUC")
	// TmpFileDir := cfg.Section("SERVICE_CONFIG").Key("tmp_file_dir").MustString("/tmp/")
	// OSSPrefix := cfg.Section("SERVICE_CONFIG").Key("oss_prefix").MustString("bai/")

	// TmpFileDir = cfg.Section("SERVICE_CONFIG").Key("tmp_file_dir").MustString("/tmp/")
	OSSPrefix := cfg.Section("SERVICE_CONFIG").Key("oss_prefix").MustString("")

	api := &OSSAPI{
		OSSBukect: OSSBukect,
		OSSRegion: OSSRegion,
		OSSAppID:  OSSAppID,
		OSSPrefix: OSSPrefix,
		OSSAppKey: OSSAppKey,
		infoLog:   infoLog,
	}

	return api, nil
}

func (this *OSSAPI) Upload(filename string, remoteName string) error {

	client, err := oss.New(this.OSSRegion, this.OSSAppID, this.OSSAppKey)
	if err != nil {
		return err
	}

	bucket, err := client.Bucket(this.OSSBukect)
	if err != nil {
		return err
	}

	err = bucket.PutObjectFromFile(remoteName, filename)

	return err

}
