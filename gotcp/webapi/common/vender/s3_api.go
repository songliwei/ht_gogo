package vender

import (
	"github.com/aws/aws-sdk-go/aws"
	// "github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	// "github.com/aws/aws-sdk-go/service/s3/s3manager"
	// "github.com/jessevdk/go-flags"
	"bytes"
	// "fmt"
	"gopkg.in/ini.v1"
	"log"
	"net/http"
	"os"
)

type S3API struct {
	S3Bukect string
	S3Region string
	S3AppID  string
	S3Prefix string
	S3AppKey string
	infoLog  *log.Logger
}

// func InitMySqlFromSection(cfg *ini.File, infoLog *log.Logger, secName string, OpenCount int, OdleCount int) (*sql.DB, error) {

func NewS3API(cfg *ini.File, infoLog *log.Logger) (*S3API, error) {

	// init mysql
	S3Bukect := cfg.Section("SERVICE_CONFIG").Key("s3_bucket").MustString("hthead.hellotalk.com")
	S3Region := cfg.Section("SERVICE_CONFIG").Key("s3_region").MustString("ap-southeast-1")
	S3AppID := cfg.Section("SERVICE_CONFIG").Key("s3_appid").MustString("AKIAITU3O3YBVUCD475A")
	S3AppKey := cfg.Section("SERVICE_CONFIG").Key("s3_appkey").MustString("6k2ykorIH95jAOgaRatPVN")
	// TmpFileDir := cfg.Section("SERVICE_CONFIG").Key("tmp_file_dir").MustString("/tmp/")
	// OSSPrefix := cfg.Section("SERVICE_CONFIG").Key("oss_prefix").MustString("bai/")

	infoLog.Printf("NewS3API() S3Bukect=%v S3Region=%v", S3Bukect, S3Region)

	api := &S3API{
		S3Bukect: S3Bukect,
		S3Region: S3Region,
		S3AppID:  S3AppID,
		S3AppKey: S3AppKey,
		infoLog:  infoLog,
	}

	return api, nil
}

func (this *S3API) Upload(filename string, remoteName string, isPublic bool) error {

	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	// Get file size and read the file content into a buffer
	fileInfo, _ := file.Stat()
	var size int64 = fileInfo.Size()
	buffer := make([]byte, size)
	file.Read(buffer)

	// Config settings: this is where you choose the bucket, filename, content-type etc.
	// of the file you're uploading.

	ACL := "private"

	if isPublic {
		ACL = "public-read"
	}

	// Create a single AWS session (we can re use this if we're uploading many files)
	s, err := session.NewSession(&aws.Config{Region: aws.String(this.S3Region),
		// Credentials:                   credentials.NewSharedCredentials("", "default"),
		CredentialsChainVerboseErrors: aws.Bool(true)})
	if err != nil {
		return err
	}

	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket:        aws.String(this.S3Bukect),
		Key:           aws.String(remoteName),
		ACL:           aws.String(ACL),
		Body:          bytes.NewReader(buffer),
		ContentLength: aws.Int64(size),
		ContentType:   aws.String(http.DetectContentType(buffer)),
		// ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
	})

	return err
}

//使用了了s3manager
/*

func (this *S3API) Upload2(filename string, remoteName string, isPublic bool) error {

	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	ACL := "private"

	if isPublic {
		ACL = "public-read"
	}

	//select Region to use.
	conf := aws.Config{Region: aws.String(this.S3Region)}
	sess := session.New(&conf)
	svc := s3manager.NewUploader(sess)

	result, err := svc.Upload(&s3manager.UploadInput{
		Bucket: aws.String(this.S3Bukect),
		Key:    aws.String(remoteName),
		Body:   file,
		ACL:    aws.String(ACL),
	})

	if err != nil {
		return err
	}

	fmt.Printf("Successfully uploaded %s %s to %s\n", ACL, filename, result.Location)

	return err
}
*/
