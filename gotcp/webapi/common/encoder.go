package common

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	// "fmt"
	"log"
	"strconv"
	"strings"
)

func DropEqual(s string) string {
	re := strings.Replace(s, "=", "", -1)
	return re
}

// 十六进制转二进制
func HexToBin(s string) (string, error) {
	ret, err := hex.DecodeString(s)
	if err != nil {
		return "", err
	}
	return string(ret), nil
}

func BinToHex(s string) string {
	ret := hex.EncodeToString([]byte(s))
	return ret
}

func ReverseString(s string) string {
	n := len(s)
	runes := make([]rune, n)
	for _, rune := range s {
		n--
		runes[n] = rune
	}
	return string(runes[n:])
}

var ascii_uppercase = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
var ascii_lowercase = []byte("abcdefghijklmnopqrstuvwxyz")
var ascii_uppercase_len = len(ascii_uppercase)
var ascii_lowercase_len = len(ascii_lowercase)

func Rot13(b byte) byte {
	pos := bytes.IndexByte(ascii_uppercase, b)
	if pos != -1 {
		return ascii_uppercase[(pos+13)%ascii_uppercase_len]
	}
	pos = bytes.IndexByte(ascii_lowercase, b)
	if pos != -1 {
		return ascii_lowercase[(pos+13)%ascii_lowercase_len]
	}
	return b
}

func StrRot13(s string) string {
	output := make([]byte, len(s))
	for k, v := range s {
		output[k] = Rot13(byte(v))
	}

	return string(output)
}

/**
 * 加密OpenUid
 */
func EncodeOpenid(uid uint64) (openid string) {
	// uidStr := string(uid)
	uidStr := strconv.Itoa(int(uid))
	if len(uidStr)%2 == 1 {
		uidStr = "0" + uidStr
	}
	// infoLog.Println("uidStr:", uidStr)
	// 翻转字符串
	openid = ReverseString(uidStr)
	// infoLog.Println("ReverseString:", openid)
	openid, err := HexToBin(openid)
	if err != nil {
		log.Println("HexToBin failed ", err)
		return ""
	}
	// infoLog.Println("HexToBin:", openid)

	openid = base64.StdEncoding.EncodeToString([]byte(openid))
	// infoLog.Println("base64:", openid)
	openid = StrRot13(openid)
	// infoLog.Println("StrRot13:", openid)
	return openid
}

func DecodeOpenid(openid string) (uid uint64) {
	// infoLog.Println("DecodeOpenid:", openid)
	openid = StrRot13(openid)
	// infoLog.Println("StrRot13:", openid)
	b64, err := base64.StdEncoding.DecodeString(openid)
	if err != nil {
		log.Println("base64 decode failed ", err)
		return 0
	}
	// infoLog.Println("base64.StdEncoding.DecodeString:", string(b64))
	openid = BinToHex(string(b64))
	// infoLog.Println("BinToHex:", openid)
	openid = ReverseString(openid)
	// infoLog.Println("ReverseString:", openid)
	userid, err := strconv.ParseUint(openid, 10, 64)
	// infoLog.Println("ParseUint:", userid)
	if err != nil {
		log.Println("DecodeOpenid strconv.Atoi failed ", err)
		return 0
	}
	uid = uint64(userid)
	return uid
}

var UseridSalt []uint64 = []uint64{
	63459385400637, 96758825373835, 73850592134985, 17853334364481, 29095726802479, 48581308808643,
	45153107875958, 52821049776393, 28458780909423, 99010032464283, 84264457395765, 69680115925147,
	14599702679552, 75469297710805, 16626202743500, 97913173136766, 53365264264866, 31085748760961,
	98400995207484, 86518860841169, 95476880087516, 49741324142087, 93772559824864, 33454501733649,
	17835195884108, 65449503331910, 10923225039151, 83197505285497, 85210160058922, 24028311369475,
	78715100279077, 60525647017639, 56720790932886, 17154537506867, 54409491191618, 42125174838584,
	21509365504607, 44921836582943, 54450478823855, 50878059556707, 26089497236535, 75615769457071,
	19530880041420, 56635332233272, 70337635702453, 80236816192045, 92234435456338, 42027962657157,
	80279652872122, 59795395936816, 97855834548826, 26468058810569, 97829653653315, 64918365827761,
	26726799444295, 62734406893141, 31632230721880, 67216263136360, 42279870174825, 25819314718246,
	24423196825664, 64838831855449, 97044314588419, 17967497981153, 28425309958402, 85468719664495,
	53176743758376, 76526843041646, 86142634507268, 65002349899150, 46175003631506, 97692735181189,
	12602403699420, 86586893605999, 82814457989297, 69198711856733, 64127300141844, 11117488322780,
	47496318863704, 50320400642231, 25517377087381, 47909070199821, 67091980781406, 71812290393281,
	83561734517570, 65634371445048, 73139554513618, 80178754811640, 27230201943312, 60303771460894,
	91499291460495, 88464226534124, 83903576775919, 17120843261946, 48096422876697, 95543857612646,
	20864610653370, 21543089777697, 54567299776244, 49052044977434,
}

// 对于一些encodeOpenid结果出来还是比较短的id进行进一步加工
func EncodeBigOpenid(sid, salt uint64) string {
	if sid <= 0 {
		return string(sid)
	}

	var ends uint64
	if salt == 0 {
		ends = sid % 100
	} else {
		ends = salt % 100
	}
	all := sid + UseridSalt[ends]
	ret := EncodeOpenid(all)
	ret = DropEqual(ret)
	if ends < 10 {
		return "0" + strconv.Itoa(int(ends)) + ret
	} else {
		return strconv.Itoa(int(ends)) + ret
	}
}

func DecodeBigOpenid(s string) (uint64, error) {
	endsp := s[0:2]
	ends, err := strconv.Atoi(endsp)
	if err != nil {
		log.Println("DecodeBigOpenid fail :", err)
		return 0, err
	}

	rest := s[2:]
	userid1 := DecodeOpenid(rest + "==")
	userid2 := userid1 - UseridSalt[ends]

	return userid2, nil
}
