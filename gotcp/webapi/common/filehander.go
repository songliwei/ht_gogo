package common

import (
	"archive/zip"
	"bytes"
	"compress/zlib"
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/textproto"
	"os"
	"strconv"
	"strings"
)

func CompressString(strMsgBody []byte) []byte {
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(strMsgBody)
	w.Close()
	return b.Bytes()
}

func DepressString(strMsgBody []byte) ([]byte, error) {
	compressBuff := bytes.NewBuffer(strMsgBody)
	r, err := zlib.NewReader(compressBuff)
	if err != nil {
		return nil, err
	}
	defer r.Close()
	// 解压缩
	unCompressSlice, err := ioutil.ReadAll(r)
	return unCompressSlice, err
}

var ExtToDBType = map[string]uint32{
	".docx": 1,
	".pptx": 2,
	".pdf":  3,
}

func CheckExt(name string) error {

	fileExt := GetExt(name)
	intType := ExtToDBType[fileExt]

	if intType <= 0 {
		return fmt.Errorf("file ext not allowd %s", name)
	}
	return nil
}

func CheckFileContent(buf []byte, name string) error {

	fileExt := GetExt(name)
	intType := ExtToDBType[fileExt]

	if intType <= 0 {
		return fmt.Errorf("file ext not allowd %s", name)
	}

	if intType == 3 {
		if buf[0] == 0x25 && buf[1] == 0x50 && buf[2] == 0x44 && buf[3] == 0x46 {
			return nil
		} else {
			return errors.New("pdf file not allowed,content bad")
		}
	}

	//504B34、504B56、504B78
	if intType == 1 || intType == 2 {
		if buf[0] == 0x50 && buf[1] == 0x4B {
			return nil
		} else {
			return errors.New("office file not allowed,content bad")
		}
	}

	return errors.New("check failed")
}

func Md5File(path string) (string, error) {
	file, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer file.Close()

	h := md5.New()
	_, err = io.Copy(h, file)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

func subString(str string, start, end int) string {
	rs := []rune(str)
	length := len(rs)

	if start < 0 || start > length {
		panic("start is wrong")
	}

	if end < start || end > length {
		panic("end is wrong")
	}

	return string(rs[start:end])
}

func getDir(path string) string {
	return subString(path, 0, strings.LastIndex(path, "/"))
}

func CreateFormFile2(w *multipart.Writer, fieldname, filename string, contentType string) (io.Writer, error) {
	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition",
		fmt.Sprintf(`form-data; name="%s"; filename="%s"`, fieldname, filename))
	h.Set("Content-Type", contentType)
	return w.CreatePart(h)
}

//解压
func DeCompress(zipFile, dest string) ([]string, error) {

	reader, err := zip.OpenReader(zipFile)

	imagePaths := []string{}

	if err != nil {
		return imagePaths, err
	}

	var count int
	count = 0
	defer reader.Close()
	for _, file := range reader.File {
		rc, err := file.Open()
		if err != nil {
			return imagePaths, err
		}
		defer rc.Close()
		filename := dest + file.Name
		//fmt.Println("test:", filename, getDir(filename))

		err = os.MkdirAll(getDir(filename), 0755)
		if err != nil {
			return imagePaths, err
		}
		w, err := os.Create(filename)
		if err != nil {
			return imagePaths, err
		}
		defer w.Close()
		_, err = io.Copy(w, rc)
		if err != nil {
			return imagePaths, err
		}
		imagePaths = append(imagePaths, filename)
		count++
		w.Close()
		rc.Close()
	}
	return imagePaths, nil
}

func GetFileSize(fileName string) (int64, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		return 0, err
	}

	return stat.Size(), nil
}

func CheckFileExistWithLen(loclaName string, fileSize string) (bool, error) {

	totalLen, err := strconv.Atoi(fileSize)

	if err != nil {
		// c.AbortWithError(http.StatusNotAcceptable, errors.New("content Length error"))
		return false, errors.New("content Length error")
	}

	totalLen64 := int64(totalLen)

	size, err1 := GetFileSize(loclaName)

	if size == 0 {
		return false, err1
	}

	if size == totalLen64 {
		return true, nil
	} else {
		return false, errors.New("content length not match")
	}

}

func ReadDataToFile(reader io.Reader, length int64, filename string) error {

	// 创建临时接收文件
	out, err := os.Create(filename)
	if err != nil {
		//log.Fatal("ReadDataToFile() ", err.Error(), length)
		return fmt.Errorf("ReadDataToFile()  create file error:%s", err.Error())
	}

	defer out.Close()

	writeLen, err := io.Copy(out, reader)

	if err != nil {
		//infoLog.Fatal("readDataToFile() ", filename, err.Error(), ",write:", writeLen, ",real len:", length)
		return err
	}

	if length != writeLen {
		//infoLog.Println("readDataToFile() error length not match", filename, ",write:", writeLen, ",real len:", length)
		return fmt.Errorf("ReadDataToFile()  length not match ,write:%d,reallen%d", writeLen, length)
	}

	return nil
}

func DeleteFile(name string) error {
	err := os.Remove(name) //删除文件test.txt
	if err != nil {
		//如果删除失败则输出 file remove Error!
		fmt.Println("file remove Error!")
		//输出错误详细信息
		return fmt.Errorf("DeleteFile()  fail delete %s", name)
	} else {
		return nil
	}
}
