package common

import (
	"github.com/HT_GOGO/gotcp/libcomm"
	"github.com/nsqio/go-nsq"
	"gopkg.in/ini.v1"
	"log"
)

type NsqLoopHandler struct {
	lookupdHost     string
	topic           string
	channel         string
	workerCount     int
	StaticAttr      string
	recvMsgPackChan chan *nsq.Message
	infoLog         *log.Logger
}

type handleNsqLoopMessageFunc func(message *nsq.Message, idx int) (code uint32, err error)

func NewNsqLoopHandler(lookupdHost, topic, channel string, workerCount int, StaticAttr string, infoLog *log.Logger) *NsqLoopHandler {

	return &NsqLoopHandler{
		lookupdHost:     lookupdHost,
		topic:           topic,
		channel:         channel,
		infoLog:         infoLog,
		workerCount:     workerCount,
		StaticAttr:      StaticAttr,
		recvMsgPackChan: make(chan *nsq.Message),
	}

}

func NewNsqLoopHandlerFromIni(cfg *ini.File, cnfFiledname string, StaticAttr string, infoLog *log.Logger) *NsqLoopHandler {

	lookupdHost := cfg.Section(cnfFiledname).Key("host").MustString("127.0.0.1:4161")
	topic := cfg.Section(cnfFiledname).Key("topic").MustString("lbs_update")
	channel := cfg.Section(cnfFiledname).Key("chan").MustString("ch2")

	workerCount := cfg.Section(cnfFiledname).Key("worker_count").MustInt(2)

	infoLog.Printf("NewNsqLoopHandlerFromIni lookupdHost=%v topic=%v channel=%v workerCount=%v", lookupdHost, topic, channel, workerCount)

	return &NsqLoopHandler{
		lookupdHost:     lookupdHost,
		topic:           topic,
		channel:         channel,
		infoLog:         infoLog,
		workerCount:     workerCount,
		StaticAttr:      StaticAttr,
		recvMsgPackChan: make(chan *nsq.Message),
	}

}

func (api *NsqLoopHandler) Start(worker handleNsqLoopMessageFunc) error {

	for i := 0; i < api.workerCount; i++ {
		go func(idx int) {

			for {
				info := <-api.recvMsgPackChan

				code, err := worker(info, idx)
				if err != nil {
					api.infoLog.Println("NsqLoopHandler start err=", err, ",code=", code, ",ts=", info.Timestamp, ",NSQDAddress=", info.NSQDAddress)
				}
			}

		}(i)
	}

	config := nsq.NewConfig()
	q, _ := nsq.NewConsumer(api.topic, api.channel, config)

	nsqhandler := nsq.HandlerFunc(func(message *nsq.Message) error {
		libcomm.AttrAdd(api.StaticAttr, 1)
		if message != nil {
			api.recvMsgPackChan <- message
		} else {
			api.infoLog.Println(" NsqLoopHandler Start() message is nil")
		}

		return nil
	})

	q.AddHandler(nsqhandler)

	return q.ConnectToNSQLookupd(api.lookupdHost)

}
