package common

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	simplejson "github.com/bitly/go-simplejson"

	_ "github.com/go-sql-driver/mysql"
	"log"
	"strconv"
	"strings"
	"time"
)

const (
	MMT_CHANGE_BLOCK   = 1
	MMT_CHANGE_UNBLOCK = 0
	MMT_CHANGE_MORE    = 2

	MMT_OP_ADD = 1 // 添加到列表
	MMT_OP_DEL = 2 // 从列表中删除

	INI_TO_ME      = 0
	INI_TO_FOWLLER = 1
	INI_TO_ALL     = 2

	// moment status for ModMntStatus func
	MMT_OP_RESTORE        = 0
	MMT_OP_DELETE_BY_USER = 1 //用户主动删除
	MMT_OP_HIDE           = 2 //后台隐藏

	MMT_OP_DELETE = 3 //后台删除

	// backend server op type for ModMntStatus func
	MMT_OP_FOR_FOLLOWER_ONLY = 10 // 只给粉丝看帖子
	MMT_OP_FOR_SELF_ONLY     = 11 // 只给自己看

	//for SetUserMomentSelfOrFollower fun
	MMT_OP_MOMENT_FOR_FOLLOWER_LIST = 8 // Momnet 只能给粉丝看用户列表
	MMT_OP_MOMENT_FOR_SELF_LIST     = 9 // Momnet 只能给自己看

	NOTIFY_URL       = "/api/notify/email"
	NOTIFY_RENEW_URL = "/api/notify/track"

	//记录在 sensitive word里面
	//1=禁止发帖,2=禁止评论,3=禁止发帖+禁止评论,4=禁止聊天,5=禁止前面所有的 敏感词命中之后的操作
	WORD_BAN_MOMENT   = 1
	WORD_BAN_COMMENT  = 2
	WORD_BAN_BOTH_MMT = 3
	WORD_BAN_MESSAGE  = 4
	// WORD_BAN_ALL_ACTION = 5
	WORD_BAN_REGISTER         = 6
	WORD_BAN_BUY              = 7
	WORD_HIDE_USER            = 8
	WORD_BAN_SEARCH_RECOMMEND = 9
	WORD_BAN_SEARCH_FILTER    = 10
	WORD_BAN_USER_LOGIN       = 11
	WORD_BAN_DEVICE_LOGIN     = 12
	WORD_DELETE_ALL_MOMENT    = 13
	WORD_NEW_MOMENT_SELF_ONLY = 14
)

var UserUpdateCacheMap map[string]string = map[string]string{
	"username": "UN", "nickname": "NK", "fullpy": "FP",
	"shortpy": "SP", "sex": "SE", "signature": "SG",
	"headurl": "HU", "voiceurl": "VU", "voiceduration": "VD",
	"timezone": "TZ", "timezone48": "TZ2", "nativelang": "NL",
	"learnlang1": "LL", "skilllevel1": "SL", "teachlang2": "NL2",
	"teachskilllevel2": "TL2", "learnlang2": "LL2", "skilllevel2": "SL2",
	"teachlang3": "NL3", "teachskilllevel3": "TL3", "learnlang3": "LL3",
	"skilllevel3": "SL3", "hidelocation": "HL", "hideage": "HA", "hidecity": "HC",
	"hideonline": "HO", "allowed": "ALO", "placeid": "PLACEID",
	"samegender": "SAMEGENDER", "exactmatch": "EXACTMATCH", "agemin": "AGEMIN",
	"agemax": "AGEMAX", "basescore": "SC", "ishide": "ISHIDE",
	"birthday": "BR", "country": "CN", "nationality": "NN",
	"locality": "CT", "longitude": "LO", "latitude": "LA",
	"absence_nearest": "ABNE", "vip": "TL", "vip2": "VIP2",
}

type UserPrivilege struct {
	AccessKey       string
	AccessId        string
	notify_api_host string
	infoLog         *log.Logger
	Mgdb            *sql.DB
	Masterdb        *sql.DB
	RulerRedis      *tcpcommon.RedisApi
	UserinfoRedis   *tcpcommon.RedisApi
	momentAPI       *tcpcommon.MntApi

	MOMENT_API_HOSTS []string
	SelfieAllowStart int64
	SelfieAllowEnd   int64
}

func NewUserPrivilege(infoLog *log.Logger, Masterdb *sql.DB, RulerRedis, UserinfoRedis *tcpcommon.RedisApi, momentAPI *tcpcommon.MntApi, MOMENT_API_HOSTS []string, notify_api_host string) *UserPrivilege {

	pt := &UserPrivilege{
		infoLog:          infoLog,
		Masterdb:         Masterdb,
		SelfieAllowStart: 0,
		SelfieAllowEnd:   0,
		UserinfoRedis:    UserinfoRedis,
		RulerRedis:       RulerRedis,
		momentAPI:        momentAPI,
		MOMENT_API_HOSTS: MOMENT_API_HOSTS,
		notify_api_host:  notify_api_host,
	}

	return pt
}

func ErrorReturn(err error) error {
	if err != nil {
		return err
	} else {
		return errors.New("OK")
	}
}

//初始化自拍允许时间段，检查通过
func (this *UserPrivilege) InitSelfieAllowTime() error {

	key := "ALLOW_SELFIE_TIME"

	val, err := this.RulerRedis.Hmget(key, []string{"from", "end"})
	if err != nil {
		return err
	}

	if len(val) == 2 {

		from, err1 := strconv.ParseInt(val[0], 10, 64)

		to, err2 := strconv.ParseInt(val[1], 10, 64)

		if err1 != nil || err2 != nil {
			return fmt.Errorf("InitSelfieAllowTime() parse error %d >= %d ", val[0], val[1])
		}

		if from >= to {
			return fmt.Errorf("InitSelfieAllowTime() %d >= %d ", from, to)
		}

		this.SelfieAllowStart = from
		this.SelfieAllowEnd = to
	}
	this.infoLog.Println("InitSelfieAllowTime() ALLOW_SELFIE_TIME", val)

	return nil
}

//主要用于后台检测到关键词自动处理机制
func (this *UserPrivilege) HanderUserByActionType(Mid string, Userid uint32, ActionTypes []uint32, Reason string) {

	banSysRet := errors.New("unkown")
	mntRuleRet := errors.New("unkown")

	if len(ActionTypes) == 0 {
		return
	}

	userids := []uint32{Userid}
	retCode := uint32(99999)

	for _, ActionType := range ActionTypes {

		if ActionType == WORD_BAN_BOTH_MMT {
			banSysRet = this.BanSysBehavior(userids, []string{"POST_COMMENT", "POST_MOMENT"}, 1, Reason)
			mntRuleRet = this.MomentActionChange(userids, []string{"POST_MOMENT", "POST_COMMENT"}, MMT_CHANGE_BLOCK, Mid)
		}

		if ActionType == WORD_BAN_MOMENT {

			banSysRet = this.BanSysBehavior(userids, []string{"POST_MOMENT"}, 1, Reason)
			mntRuleRet = this.MomentActionChange(userids, []string{"POST_MOMENT"}, MMT_CHANGE_BLOCK, Mid)

		}

		if ActionType == WORD_BAN_COMMENT {

			banSysRet = this.BanSysBehavior(userids, []string{"POST_COMMENT"}, 1, Reason)

			mntRuleRet = this.MomentActionChange(userids, []string{"POST_COMMENT"}, MMT_CHANGE_BLOCK, Mid)

		}

		if ActionType == WORD_BAN_MESSAGE {

			banSysRet = this.BanSysBehavior(userids, []string{"MESSAGE"}, 1, Reason)

		}

		if ActionType == WORD_BAN_BUY {
			banSysRet = this.BanSysBehavior(userids, []string{"BUY"}, 1, Reason)
		}

		if ActionType == WORD_BAN_REGISTER {
			banSysRet = this.BanRegister(userids, "REGISTER_BAN")
		}

		if ActionType == WORD_HIDE_USER {
			banSysRet = this.HideUser(userids, 1)
		}

		if ActionType == WORD_BAN_SEARCH_FILTER {

			banSysRet = this.BanSysBehavior(userids, []string{"FILTER"}, 1, Reason)
		}

		if ActionType == WORD_BAN_SEARCH_RECOMMEND {
			banSysRet = this.BanSysBehavior(userids, []string{"RECOMMEND"}, 1, Reason)
		}

		if ActionType == WORD_BAN_USER_LOGIN {
			banSysRet = this.BanSysBehavior(userids, []string{"LOGIN"}, 1, Reason)
		}

		if ActionType == WORD_BAN_DEVICE_LOGIN {
			banSysRet = this.BanDeviceLogin(userids, 1)
		}

		if ActionType == WORD_DELETE_ALL_MOMENT {
			retCode, banSysRet = this.momentAPI.DeleteUser(userids)
		}

		if ActionType == WORD_NEW_MOMENT_SELF_ONLY {
			retCode, banSysRet = this.momentAPI.SetUserMomentSelfOrFollower(Userid, MMT_OP_MOMENT_FOR_SELF_LIST, MMT_OP_ADD, Reason)
		}

		if banSysRet == nil {
			banSysRet = errors.New("OK")
		}

		this.infoLog.Printf("HanderUserByActionType() ActionType=%d,banSysRet=%s,mntRuleRet=%s,Mid=%s,Userid=%d,Reason=%s,retCode=%d",
			ActionType, banSysRet.Error(), mntRuleRet.Error(), Mid, Userid, Reason, retCode)

	}

}

//判断时间段是不是可以自拍，检查通过
func (this *UserPrivilege) IsSelfieAllowedThisTime() bool {

	if this.SelfieAllowStart == this.SelfieAllowEnd {
		return false
	}

	secs := int64(time.Now().Unix())

	if this.SelfieAllowStart <= secs && secs <= this.SelfieAllowEnd {
		return true
	}

	return false

}

//判断这个用户是不是可以发自拍
func (this *UserPrivilege) IsUserAllowSendQrCode(userid uint32) (bool, error) {

	blockKey := GetBlockRedisKey(userid)

	field := "POST_ALLOW_QRCODE"

	val, err := this.RulerRedis.Hget(blockKey, field)

	if err != nil {
		return false, err
	}

	if val == "1" {
		return true, nil
	}

	return false, nil

}

//判断这个用户是不是可以发自拍
func (this *UserPrivilege) IsUserAllowSendSelfie(userid uint32) (bool, error) {

	blockKey := GetBlockRedisKey(userid)

	field := "POST_ALLOW_SELFIE"

	val, err := this.RulerRedis.Hget(blockKey, field)

	if err != nil {
		return false, err
	}

	if val == "1" {
		return true, nil
	}

	return false, nil

}

// 禁止注册等行为，行为描述由 field 数组指定，POST_MOMENT,POST_COMMENT,MESSAGE,BUY等 POST_ALLOW_SELFIE
// blockOrNot: 1=block,0=unblock

func (this *UserPrivilege) BanSysBehavior(userids []uint32, field []string, blockOrNot int, reason string) (err error) {

	fieldStr := strings.Join(field, ",")
	swStr := strings.Repeat(fmt.Sprintf(",%d", blockOrNot), len(field))
	upStr := ""

	for _, v := range field {
		upStr = upStr + fmt.Sprintf("`%s`=VALUES(`%s`),", v, v)
	}
	// this.infoLog.Println("BanSysBehavior() upStr", upStr, swStr, fieldStr)
	for _, v := range userids {

		r1, err1 := this.Masterdb.Exec("INSERT INTO SYS_BEHAVIOR_BLOCK (`USERID`,"+fieldStr+",`REASON`,`OPERATOR`,`UPDATETIME`,`CREATETIME`) "+
			"VALUES(? "+swStr+",?,'go-ban',UTC_TIMESTAMP(),UTC_TIMESTAMP())"+
			"ON DUPLICATE KEY UPDATE "+upStr+" UPDATETIME=VALUES(UPDATETIME), REASON=VALUES(REASON), OPERATOR=VALUES(OPERATOR)",
			v,
			reason,
		)

		if err1 != nil {
			this.infoLog.Println(v, "BanSysBehavior() SYS_BEHAVIOR_BLOCK insert failed err=", err1)
			err = err1
			return err
		}

		rowaf, _ := r1.RowsAffected()

		this.infoLog.Println(v, "BanSysBehavior() SYS_BEHAVIOR_BLOCK afftectet=", rowaf, field, reason)
	}

	return ErrorReturn(err)
}

//是否隐藏用户
func (this *UserPrivilege) HideUser(userids []uint32, HideOrNot uint8) (err error) {

	for _, v := range userids {

		r1, err1 := this.Masterdb.Exec("INSERT INTO SE_HIDE_STATUS (`USERID`,`MANAGER`) "+
			"VALUES(? ,?)"+
			"ON DUPLICATE KEY UPDATE MANAGER=VALUES(MANAGER)",
			v,
			HideOrNot,
		)

		if err1 != nil {
			this.infoLog.Println(v, "HideUser() SE_HIDE_STATUS insert failed err=", err1)
			err = err1
			return
		}

		hideOk := 0
		err = this.UpdateMemSingleVal(v, "ishide", HideOrNot)

		if err != nil {
			this.infoLog.Printf("HideUser() UpdateMemSingleVal %d,err=%s", v, err.Error())
		} else {
			hideOk = 1
		}

		rowaf, _ := r1.RowsAffected()

		this.infoLog.Printf("HideUser() SE_HIDE_STATUS userid=%d,afftectet=%d,hideOk=%d", v, rowaf, hideOk)
	}

	return ErrorReturn(err)
}

func (this *UserPrivilege) UpdateMemSingleVal(Userid uint32, field string, val interface{}) error {

	dict := map[string]interface{}{}

	dict[field] = val

	return this.UpdateMemUserInfo(Userid, dict)
}

func (this *UserPrivilege) UpdateMemUserInfo(Userid uint32, dict map[string]interface{}) error {

	key := GetUserInfoRedisKey(Userid)

	val, err := this.UserinfoRedis.Get(key)

	if err != nil {
		return err
	}

	jsonObj, err := simplejson.NewJson([]byte(val))

	if err != nil {
		return err
	}

	for k, v := range dict {
		field, ok := UserUpdateCacheMap[k]
		if ok {
			jsonObj.Set(field, v)
		}
	}

	payLoad, err := jsonObj.MarshalJSON()
	if err != nil {
		return err
	}

	newStr := string(payLoad)

	if newStr == val {
		return errors.New("not changed")
	}

	err = this.UserinfoRedis.Setex(key, newStr, 3600*24*5)

	this.infoLog.Println(Userid, " UpdateMemUserInfo() ", dict, err)

	return err

}

//禁止用户发帖，发言，注册等行为
func (this *UserPrivilege) BanUserAllAction(userids []uint32, Mid string, reason string) map[string]string {

	banRegRet := this.BanRegister(userids, "REGISTER_BAN")

	banSysRet := this.BanSysBehavior(userids, []string{"POST_MOMENT", "POST_COMMENT", "MESSAGE", "BUY"}, 1, reason)

	mntRuleRet := this.MomentActionChange(userids, []string{"POST_MOMENT", "POST_COMMENT"}, MMT_CHANGE_BLOCK, Mid)

	//这个用户发的帖子都只给粉丝看

	//userMntRet := this.UserMomentChange(userids, []uint32{MMT_OP_MOMENT_FOR_SELF_LIST}, MMT_OP_ADD, reason, Mid)

	hideRet := this.HideUser(userids, 1)

	this.infoLog.Println("BanUserAllAction() result=", Mid, userids, banRegRet, banSysRet, mntRuleRet, hideRet)

	result := map[string]string{
		"banRegRet":  banRegRet.Error(),
		"banSysRet":  banSysRet.Error(),
		"mntRuleRet": mntRuleRet.Error(),
		// "userMntRet": userMntRet.Error(),
		"hideRet": hideRet.Error(),
	}

	return result
}

func (this *UserPrivilege) MomentActionChange(userids []uint32, typs []string, behavior uint8, Mid string) (err error) {

	moment_host := "/v1/moment/httpAPI/change_block"

	behDef := []string{"unblock", "block", "more"}

	if behavior < 0 || behavior > 2 {
		return errors.New("bad param")
	}

	for _, Id := range userids {

		for _, vt := range typs {
			beh := behDef[behavior]
			data := map[string]interface{}{
				"userid":   Id,
				"type":     vt,
				"behavior": beh,
			}

			blockKey := GetBlockRedisKey(Id)

			err = this.RulerRedis.Hset(blockKey, vt, fmt.Sprintf("%d", behavior))
			if err != nil {
				this.infoLog.Printf("MomentActionChange() key=%s,Mid=%s,err=", blockKey, Mid, err.Error())
			}

			str, err := this.SendMomentRequest(moment_host, data, vt)

			this.infoLog.Println("MomentActionChange() SendMomentRequest ", Id, Mid, vt, beh, str, err)
		}

	}

	return ErrorReturn(err)
}

//types = REGISTER_BAN
func (this *UserPrivilege) BanRegister(userids []uint32, typ string) (err error) {

	deviceIDs, err := this.GetDeviceID(userids, "login")

	if err != nil {
		return err
	}

	this.infoLog.Println("deviceIDs", deviceIDs)

	type_arr := map[string][]string{
		"REGISTER_BAN":             {"REGISTERBAN", "1"},
		"REGISTER_NOTBAN":          {"REGISTERBAN", "0"},
		"REGISTER_RELEASECOUNT":    {"RELEASECOUNT", "1"},
		"REGISTER_NO_RELEASECOUNT": {"RELEASECOUNT", "0"},
		"REGISTER_NOLIMIT":         {"COUNTOPEN", "1"},
		"REGISTER_LIMIT":           {"COUNTOPEN", "0"},
	}

	same, ok := type_arr[typ]

	if !ok {
		return errors.New("not tye in type_arr")
	}

	for _, v := range deviceIDs {

		r1, err1 := this.Masterdb.Exec("INSERT INTO SYS_DEVICE_STATUS (`DEVICEID`,`"+same[0]+"`,`UPDATETIME`) "+
			"VALUES(?,?,UTC_TIMESTAMP())"+
			"ON DUPLICATE KEY UPDATE `"+same[0]+"`=VALUES(`"+same[0]+"`), UPDATETIME=VALUES(UPDATETIME)",
			v,
			same[1],
		)

		if err1 != nil {
			this.infoLog.Println(v, "BanRegister() SYS_DEVICE_STATUS insert failed err=", err1)
			err = err1
			return err1
		}

		rowaf, _ := r1.RowsAffected()

		this.infoLog.Println("BanRegister() ", v, " row affacted", rowaf)

	}

	return ErrorReturn(err)
}

//types = REGISTER_BAN
func (this *UserPrivilege) BanDeviceLogin(userids []uint32, banOrNot uint8) (err error) {

	deviceIDs, err := this.GetDeviceID(userids, "login")

	if err != nil {
		return err
	}

	// this.infoLog.Println("BanDeviceLogin() DEVICEID", deviceIDs)

	for _, v := range deviceIDs {

		r1, err1 := this.Masterdb.Exec("INSERT INTO SYS_BANNED_DEVICE (`DEVICEID`,`FLAG`,`TIME`,`BANTIME`) "+
			"VALUES(?,?,UTC_TIMESTAMP(),UTC_TIMESTAMP())"+
			"ON DUPLICATE KEY UPDATE  FLAG=VALUES(FLAG), BANTIME=VALUES(BANTIME), TIME=VALUES(TIME)",
			v,
			banOrNot,
		)

		if err1 != nil {
			this.infoLog.Println(v, "BanDeviceLogin() SYS_BANNED_DEVICE insert failed err=", err1)
			err = err1
			return err1
		}

		rowaf, _ := r1.RowsAffected()

		this.infoLog.Println("BanDeviceLogin() ", v, " row affacted", rowaf)

	}

	return ErrorReturn(err)
}

func (this *UserPrivilege) GetDeviceID(userids []uint32, source string) (deviceids []string, rt error) {

	useridStr := JoinUint32(userids, ",")
	table := ""

	if source == "login" {
		table = "HT_USER_TERMINAL"
	} else {
		table = "HT_USER_REGISTER"
	}

	rows, err := this.Masterdb.Query("SELECT DEVICEID FROM " + table + " WHERE USERID IN(" + useridStr + ")")

	if err != nil {
		return nil, err
	}

	if err == sql.ErrNoRows {
		return nil, errors.New("GetDeviceID()  no data ")
	}

	defer rows.Close()

	strs := []string{}

	deviceid := ""

	for rows.Next() {

		if err := rows.Scan(&deviceid); err != nil {
			this.infoLog.Printf("GetDeviceID() rows.Scan failed")
			continue
		}
		strs = append(strs, deviceid)
	}

	return strs, nil
}

func (this *UserPrivilege) SendMomentRequest(url string, data interface{}, tag string) (str string, err error) {

	for _, v := range this.MOMENT_API_HOSTS {

		moment_host := v + url

		jsonStr, err := json.Marshal(data)

		str, err = HttpPost(moment_host, jsonStr, 10)
		if err != nil {
			return "", err
		}
	}

	return str, err
}

// op_reason mg_user_change
func (this *UserPrivilege) UserMomentChange(userids []uint32, pListType []uint32, opType uint32, opReason string, Mid string) (erro error) {

	code := uint32(2)

	for _, v := range userids {

		if opType == MMT_OP_ADD {
			code, erro = this.momentAPI.SetUserMomentSelfOrFollower(v, MMT_OP_MOMENT_FOR_FOLLOWER_LIST, MMT_OP_DEL, "del_be4_add")
			if erro != nil {
				this.infoLog.Printf("UserMomentChange() %s,userid=%d OP_DEL OP_MOMENT_FOR_FOLLOWER_LIST error=%s", Mid, v, erro.Error())
			}

			code, erro = this.momentAPI.SetUserMomentSelfOrFollower(v, MMT_OP_MOMENT_FOR_SELF_LIST, MMT_OP_DEL, "del_be4_add")

			if erro != nil {
				this.infoLog.Printf("UserMomentChange() %s,userid=%d OP_DEL OP_MOMENT_FOR_SELF_LIST error=%s", Mid, v, erro.Error())
			}
		}

		for _, vt := range pListType {

			code, erro = this.momentAPI.SetUserMomentSelfOrFollower(v, vt, opType, opReason)

			if erro != nil {
				this.infoLog.Printf("UserMomentChange() %s,userid=%d,code=%d,opType%d,opReason=%s fail set,error = %s", Mid, v, code, opType, opReason, erro.Error())
			} else {

				this.infoLog.Printf("UserMomentChange() %s,userid=%d,code=%d,opType%d,opReason=%s", Mid, v, code, opType, opReason)
			}

			time.Sleep(50 * time.Millisecond)
		}

	}

	return ErrorReturn(erro)
}

func (this *UserPrivilege) TrackSelfiePost(userid uint32, OsType uint32) (string, error) {

	params := "event=SelfiePosted&allowOffline=true"

	data := &map[string]interface{}{
		"userid": userid,
		"params": params,
		"action": "track",
		"ostype": OsType,
	}

	postData, _ := json.Marshal(data)

	host := this.notify_api_host

	url := host + NOTIFY_RENEW_URL
	this.infoLog.Println("TrackSelfiePost()", url, string(postData))
	str, err := HttpPost(url, postData, 10)
	if err != nil {
		return "", err
	}
	return str, err
}
