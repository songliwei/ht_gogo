package common

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"hash/crc32"
	"hash/crc64"
	"io"
	"io/ioutil"
	"math"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	KC_RAND_KIND_NUM   = 0 // 纯数字
	KC_RAND_KIND_LOWER = 1 // 小写字母
	KC_RAND_KIND_UPPER = 2 // 大写字母
	KC_RAND_KIND_ALL   = 3 // 数字、大小写字母
)

var DEFINED_COUNTRY = []string{"", "US", "UK", "CN", "HK", "TW", "JP", "KR", "FR", "DE", "IT", "ES", "AF", "AL", "DZ", "AD", "AO", "AG", "AR", "AM", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BT", "BO", "BA", "BW", "BR", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "CF", "TD", "CL", "CO", "KM", "CD", "CK", "CR", "CI", "HR", "CY", "CU", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FO", "FJ", "FI", "GA", "GM", "GE", "GH", "GR", "GL", "GD", "GT", "GG", "GN", "GW", "GY", "HT", "HN", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IL", "JM", "JE", "JO", "KZ", "KE", "KI", "KW", "KG", "XK", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MR", "MU", "MX", "FM", "MC", "MZ", "MD", "MN", "ME", "MA", "MM", "NA", "NR", "NP", "NL", "NZ", "NI", "NE", "NG", "NU", "KP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PL", "PT", "PR", "QA", "RO", "RU", "RW", "KN", "LC", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SB", "SK", "SI", "SO", "ZA", "LK", "SD", "SR", "SZ", "SE", "CH", "SY", "SX", "SS", "TJ", "TZ", "TH", "TL", "TG", "TO", "TT", "TN", "TR", "TM", "TV", "AE", "UG", "UA", "UY", "UZ", "VE", "VN", "VU", "YE", "ZM", "ZW", "CG", "IK", "AC", "AJ", "AN", "AI", "AS", "AW", "AX", "BL", "BM", "BQ", "CW", "CX", "EH", "GF", "GI", "GP", "GU", "IM", "KY", "MF", "MP", "MQ", "MS", "NC", "PF", "RE", "SH", "SJ", "TC", "VG", "VI", "YT"}

var COUNTRY_TO_CODE = map[string]uint32{
	"": 0, "US": 1, "UK": 2, "GB": 2, "CN": 3, "HK": 4, "TW": 5, "JP": 6, "KR": 7, "FR": 8,
	"DE": 9, "IT": 10, "ES": 11, "AF": 12, "AL": 13, "DZ": 14, "AD": 15, "AO": 16, "AG": 17, "AR": 18,
	"AM": 19, "AU": 20, "AT": 21, "AZ": 22, "BS": 23, "BH": 24, "BD": 25, "BB": 26, "BY": 27, "BE": 28,
	"BZ": 29, "BJ": 30, "BT": 31, "BO": 32, "BA": 33, "BW": 34, "BR": 35, "BN": 36, "BG": 37, "BF": 38,
	"BI": 39, "KH": 40, "CM": 41, "CA": 42, "CV": 43, "CF": 44, "TD": 45, "CL": 46, "CO": 47, "KM": 48,
	"CD": 49, "CK": 50, "CR": 51, "CI": 52, "HR": 53, "CY": 54, "CU": 55, "CZ": 56, "DK": 57, "DJ": 58,
	"DM": 59, "DO": 60, "EC": 61, "EG": 62, "SV": 63, "GQ": 64, "ER": 65, "EE": 66, "ET": 67, "FO": 68,
	"FJ": 69, "FI": 70, "GA": 71, "GM": 72, "GE": 73, "GH": 74, "GR": 75, "GL": 76, "GD": 77, "GT": 78,
	"GG": 79, "GN": 80, "GW": 81, "GY": 82, "HT": 83, "HN": 84, "HU": 85, "IS": 86, "IN": 87, "ID": 88,
	"IR": 89, "IQ": 90, "IE": 91, "IL": 92, "JM": 93, "JE": 94, "JO": 95, "KZ": 96, "KE": 97, "KI": 98,
	"KW": 99, "KG": 100, "XK": 101, "LA": 102, "LV": 103, "LB": 104, "LS": 105, "LR": 106, "LY": 107, "LI": 108,
	"LT": 109, "LU": 110, "MO": 111, "MK": 112, "MG": 113, "MW": 114, "MY": 115, "MV": 116, "ML": 117, "MT": 118,
	"MH": 119, "MR": 120, "MU": 121, "MX": 122, "FM": 123, "MC": 124, "MZ": 125, "MD": 126, "MN": 127, "ME": 128,
	"MA": 129, "MM": 130, "NA": 131, "NR": 132, "NP": 133, "NL": 134, "NZ": 135, "NI": 136, "NE": 137, "NG": 138,
	"NU": 139, "KP": 140, "NO": 141, "OM": 142, "PK": 143, "PW": 144, "PS": 145, "PA": 146, "PG": 147, "PY": 148,
	"PE": 149, "PH": 150, "PL": 151, "PT": 152, "PR": 153, "QA": 154, "RO": 155, "RU": 156, "RW": 157, "KN": 158,
	"LC": 159, "VC": 160, "WS": 161, "SM": 162, "ST": 163, "SA": 164, "SN": 165, "RS": 166, "SC": 167, "SL": 168,
	"SG": 169, "SB": 170, "SK": 171, "SI": 172, "SO": 173, "ZA": 174, "LK": 175, "SD": 176, "SR": 177, "SZ": 178,
	"SE": 179, "CH": 180, "SY": 181, "SX": 182, "SS": 183, "TJ": 184, "TZ": 185, "TH": 186, "TL": 187, "TG": 188,
	"TO": 189, "TT": 190, "TN": 191, "TR": 192, "TM": 193, "TV": 194, "AE": 195, "UG": 196, "UA": 197, "UY": 198,
	"UZ": 199, "VE": 200, "VN": 201, "VU": 202, "YE": 203, "ZM": 204, "ZW": 205, "CG": 206, "IK": 207, "AC": 208,
	"AJ": 209, "AN": 210, "AI": 211, "AS": 212, "AW": 213, "AX": 214, "BL": 215, "BM": 216, "BQ": 217, "CW": 218,
	"CX": 219, "EH": 220, "GF": 221, "GI": 222, "GP": 223, "GU": 224, "IM": 225, "KY": 226, "MF": 227, "MP": 228,
	"MQ": 229, "MS": 230, "NC": 231, "PF": 232, "RE": 233, "SH": 234, "SJ": 235, "TC": 236, "VG": 237, "VI": 238,
	"YT": 239,
}

func Md5(input string) string {

	md5Ctx := md5.New()
	md5Ctx.Write([]byte(input))
	cipherStr := md5Ctx.Sum(nil)
	return hex.EncodeToString(cipherStr)
}

func Crc64(input string) uint64 {
	tab := crc64.MakeTable(crc64.ISO)

	return crc64.Checksum([]byte(input), tab)
}

func Crc32(input string) uint32 {
	return crc32.ChecksumIEEE([]byte(input))
}

func GetUserInfoRedisKey(id uint32) string {
	return fmt.Sprintf("SEARCH_USERINFO_%d", id)
}

func GetBlockRedisKey(id uint32) string {
	return fmt.Sprintf("BLOCK_%d", id)
}

func UnmarshalInterface(src interface{}, v interface{}) error {

	buff, err := json.Marshal(src)

	if err != nil {
		return err
	}

	return json.Unmarshal(buff, v)

}

func HttpPost(url string, Data []byte, timeout int) (string, error) {

	client := &http.Client{
		Transport: &http.Transport{
			Dial: func(netw, addr string) (net.Conn, error) {
				c, err := net.DialTimeout(netw, addr, time.Second*time.Duration(timeout)) //设置建立连接超时
				if err != nil {
					// infoLog.Println(userid, "aliyun filter GetResponse() DialTimeout:", err.Error())
					return nil, err
				}
				c.SetDeadline(time.Now().Add(time.Duration(timeout) * time.Second)) //设置发送接收数据超时
				return c, nil
			},
		},
		Timeout: time.Second * time.Duration(timeout),
	}

	req, err := http.NewRequest("POST", url, strings.NewReader(string(Data)))

	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")

	resp, err := client.Do(req)

	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func HttpGet(Url string, timeoutSec int) ([]byte, error) {

	c := &http.Client{
		Timeout: time.Duration(timeoutSec) * time.Second,
	}

	resp, err := c.Get(Url)

	if err != nil {
		return nil, fmt.Errorf("Error while HttpGet %s-%s", Url, err.Error())
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil

}

func GetJsonByHTTPAPI(Url string, timeoutSec int, v interface{}) error {

	content, err := HttpGet(Url, timeoutSec)

	if err != nil {
		return err
	}

	err = json.Unmarshal(content, v)

	return err
}

func DownloadFile(Url string, localName string, timeoutSec int) (int64, error) {

	c := &http.Client{
		Timeout: time.Duration(timeoutSec) * time.Second,
	}

	resp, err := c.Get(Url)

	if err != nil {
		return 0, fmt.Errorf("Error while downloading %s-%s", Url, err.Error())
	}

	defer resp.Body.Close()

	// TODO: check file existence first with io.IsExist
	output, err := os.Create(localName)
	if err != nil {
		return 0, fmt.Errorf("Error while creating %s-%s", localName, err.Error())
	}
	defer output.Close()
	n, err := io.Copy(output, resp.Body)
	if err != nil {
		return 0, fmt.Errorf("Error while copy file %s-%s", localName, err.Error())
	}

	return n, nil

}

func JoinUint32(ids []uint32, stp string) string {

	count := len(ids)

	if count == 1 {
		return fmt.Sprintf("%d", ids[0])
	}

	strs := make([]string, count)

	for i := 0; i < count; i++ {

		strs[i] = fmt.Sprintf("%d", ids[i])
	}

	return strings.Join(strs, stp)
}

func JoinUint64(ids []uint64, stp string) string {

	count := len(ids)

	if count == 1 {
		return fmt.Sprintf("%v", ids[0])
	}

	strs := make([]string, count)

	for i := 0; i < count; i++ {
		strs[i] = fmt.Sprintf("%v", ids[i])
	}

	return strings.Join(strs, stp)
}

func SplitStringToUint32Array(param string) []uint32 {
	useridArr := strings.Split(param, ",")
	newTestUserid := []uint32{}
	for _, v := range useridArr {
		uid, err := strconv.Atoi(v)
		if err == nil {
			newTestUserid = append(newTestUserid, uint32(uid))
		}
	}

	return newTestUserid
}

func IsBirthDayTeen(birth string) bool {

	age, err := GetAgeByBirth(birth)

	if err != nil {
		return false
	}

	return age < 18

}

func QtDate(timestamp uint32) string {

	tm := time.Unix(int64(timestamp), 0)

	ServerTime := tm.UTC().Format("2006-01-02 15:04:05")

	return ServerTime

}

func QtDateWithFormat(timestamp uint32, format string) string {

	tm := time.Unix(int64(timestamp), 0)

	ServerTime := tm.UTC().Format(format)

	return ServerTime

}

func StrtoTime(str string) (time.Time, error) {

	layout := "2006-01-02 15:04:05"

	t, err := time.Parse(layout, str)

	if err != nil {
		return t, nil
	}

	return t, nil
}

func GetMilliNow() uint64 {

	now := time.Now().UnixNano() / 1000000
	return uint64(now)
}

func IsTwoTimestampSameDay(ts1 uint64, ts2 uint64, seconedOffset uint64) bool {

	_, offset := time.Now().Zone()

	time1 := time.Unix(int64(ts1/uint64(1000)-uint64(offset)+seconedOffset), 0)

	time2 := time.Unix(int64(ts2/uint64(1000)-uint64(offset)+seconedOffset), 0)

	if time1.Year() == time2.Year() && time1.Month() == time2.Month() && time1.Day() == time2.Day() {
		return true
	}

	return false

}

func GetAgeByBirth(birth string) (uint32, error) {

	layout := "2006-01-02T15:04:05.000Z"
	birth = birth + "T00:04:05.000Z"
	t, err := time.Parse(layout, birth)

	if err != nil {
		return 0, errors.New("bad birth")
	}

	now := time.Now()

	if now.Year()-t.Year() < 0 {
		return 0, errors.New(birth + " greater than now")
	}

	age := uint32(now.Year() - t.Year())
	month_diff := now.Month() - t.Month()
	day_diff := now.Day() - t.Day()

	if month_diff < 0 || (month_diff == 0 && day_diff < 0) {
		age--
	}

	return age, nil

}

// 随机字符串
func Krand(size int, kind int) []byte {
	ikind, kinds, result := kind, [][]int{[]int{10, 48}, []int{26, 97}, []int{26, 65}}, make([]byte, size)
	is_all := kind > 2 || kind < 0
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < size; i++ {
		if is_all { // random ikind
			ikind = rand.Intn(3)
		}
		scope, base := kinds[ikind][0], kinds[ikind][1]
		result[i] = uint8(base + rand.Intn(scope))
	}
	return result
}

/*
*	 sphinx 国家code 整数化之后的
 */

func GetCountryCode(idx string) uint32 {

	val, ok := COUNTRY_TO_CODE[idx]

	if ok {
		return val
	} else {
		return 0
	}
}

// 返回值的单位为米
func EarthDistance(lat1, lng1, lat2, lng2 float64) float64 {
	radius := float64(6371000) // 6378137
	rad := math.Pi / 180.0

	lat1 = lat1 * rad
	lng1 = lng1 * rad
	lat2 = lat2 * rad
	lng2 = lng2 * rad

	theta := lng2 - lng1
	dist := math.Acos(math.Sin(lat1)*math.Sin(lat2) + math.Cos(lat1)*math.Cos(lat2)*math.Cos(theta))

	return dist * radius
}

func Deg2rad(angle float64) float64 {
	return angle * 0.017453292519943295
}

func Rad2deg(radian float64) float64 {
	return radian * 180 / math.Pi
}
