package sdk

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type ClientInfo struct {
	// SdkVersion  string `json:"sdkVersion"`
	// CfgVersion  string `json:"cfgVersion"`
	// UserType    string `json:"userType"`
	// UserId      string `json:"userId"`
	// UserNick    string `json:"userNick"`
	// Avatar      string `json:"avatar"`
	// Imei        string `json:"imei"`
	// Imsi        string `json:"imsi"`
	// Umid        string `json:"umid"`
	Ip string `json:"ip"`

	// Os          string `json:"os"`
	// Channel     string `json:"channel"`
	// HostAppName string `json:"hostAppName"`
	// HostPackage string `json:"hostPackage"`
	// HostVersion string `json:"hostVersion"`
}

type AliFilter struct {
	AccessKey string
	AccessId  string

	infoLog *log.Logger
}

type AliTask struct {
	DataId string `json:"dataId"`
	Url    string `json:"url"`
}

type AliFilterBizData struct {
	BizType string    `json:"bizType"`
	Scences []string  `json:"scenes"`
	Tasks   []AliTask `json:"tasks"`
}

type Alifaces struct {
	Rate float64 `json:"rate" binding:"omitempty"`
	Name string  `json:"name" binding:"omitempty"`
}

type sfaceData struct {
	Width  float32 `json:"w" binding:"omitempty"`
	Height float32 `json:"h" binding:"omitempty"`

	X float32 `json:"x" binding:"omitempty"`
	Y float32 `json:"y" binding:"omitempty"`

	Faces []Alifaces `json:"faces" binding:"omitempty"`
}

// https://help.aliyun.com/document_detail/56287.html?spm=5176.doc53419.6.566.vvemIB
// https://promotion.aliyun.com/ntms/act/lvwangdemo.html?spm=5176.doc28428.2.22.oC62cT
// 参考这里的连接 构造参数

//只鉴黄，二维码，和自拍
type ItemResult struct {
	Rate       float64     `json:"rate" binding:"omitempty"`
	Scene      string      `json:"scene" binding:"required"`
	Suggestion string      `json:"suggestion" binding:"omitempty"` //
	Label      string      `json:"label" binding:"required"`       // porn场景下= normal ,sexy，porn，qrcode场景下 qrcode，normal
	QrcodeData []string    `json:"qrcodeData" binding:"omitempty"` //二维码数组
	SfaceData  []sfaceData `json:"sfaceData" binding:"omitempty"`  //人脸信息列表
}

type TaskResult struct {
	Msg    string `json:"msg"`
	Code   uint32 `json:"code"`
	DataId string `json:"dataId"`
	TaskId string `json:"taskId"`
	Url    string `json:"url"`

	Results []ItemResult `json:"results" binding:"omitempty"`
}

type AliFliterRespBody struct {
	Msg       string       `json:"msg"`
	Code      uint32       `json:"code" binding:"required"`
	RequestId string       `json:"requestId"  binding:"required"`
	Data      []TaskResult `json:"data"  binding:"omitempty"`
}

func NewAliFilter(AccessId, AccessKey string, infoLog *log.Logger) *AliFilter {

	pt := &AliFilter{
		infoLog:   infoLog,
		AccessKey: AccessKey,
		AccessId:  AccessId,
	}

	return pt
}

func (this *AliFilter) GetResponse(path string, clientInfo ClientInfo, bizData *AliFilterBizData) (p *AliFliterRespBody, err error) {
	clientInfoJson, _ := json.Marshal(clientInfo)
	bizDataJson, _ := json.Marshal(bizData)

	this.infoLog.Println("GetResponse() request json=", string(bizDataJson))

	client := &http.Client{
		Transport: &http.Transport{
			Dial: func(netw, addr string) (net.Conn, error) {
				c, err := net.DialTimeout(netw, addr, time.Second*15) //设置建立连接超时
				if err != nil {
					this.infoLog.Println("aliyun filter GetResponse() DialTimeout:", err.Error())
					return nil, err
				}
				c.SetDeadline(time.Now().Add(15 * time.Second)) //设置发送接收数据超时
				return c, nil
			},
		},
		Timeout: time.Second * 15,
	}
	req, err := http.NewRequest(method, AliGreenHost+path+"?clientInfo="+url.QueryEscape(string(clientInfoJson)), strings.NewReader(string(bizDataJson)))

	if err != nil {
		// handle error
		return nil, err
	} else {
		addRequestHeader(string(bizDataJson), req, string(clientInfoJson), path, this.AccessId, this.AccessKey)

		response, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return nil, err
		} else {

			this.infoLog.Println("GetResponse() body=", string(body))

			Resp := &AliFliterRespBody{}

			err = json.Unmarshal(body, Resp) // JSON to Struct

			if err != nil {
				return nil, err
			}

			return Resp, nil
		}
	}
}
