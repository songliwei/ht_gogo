package sdk

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
)

const (

	//一定要是二进制的整数倍，防止同一个mid里面有多个 二维码图片
	QR_WEIXIN        = 1
	QR_LINE          = 2
	QR_KAKAO         = 4
	QR_QQ            = 8
	QR_WEBXIN_GROUP  = 16
	QR_FACEBOOK      = 32
	QR_WEIXIN_PUBLIC = 64
	QR_OTHER         = 128

	QR_HT_USER_PROFILE = 256  //hellotalk  个人二维码
	QR_HT_GROUP_INFO   = 512  //hellotalk  群二维码
	QR_HT_OTHER_INFO   = 1024 //hellotalk  其他二维码

	QR_ALIPAY = 2048 //支付宝验证码

	ALI_CHECK_URI = "/green/image/scan"
)

var HtQrAllowed uint32 = QR_HT_USER_PROFILE | QR_HT_GROUP_INFO | QR_HT_OTHER_INFO

type ImageFilter struct {
	AccessKey string
	AccessId  string
	infoLog   *log.Logger
	aliFilter *AliFilter
}

var QrCodeUrlMap = map[string]uint32{
	"weixin.qq.com/r/":    QR_WEIXIN,
	"//u.wechat.com/":     QR_WEIXIN,
	"wechat.com/":         QR_WEIXIN,
	"wxp://":              QR_WEIXIN,
	".alipay.com/":        QR_ALIPAY,
	"weixin.qq.com/q/":    QR_WEIXIN_PUBLIC,
	"line":                QR_LINE,
	"kakao.com":           QR_KAKAO,
	"qm.qq.com":           QR_QQ,
	"weixin.qq.com/g/":    QR_WEBXIN_GROUP,
	"www.facebook.com":    QR_FACEBOOK,
	"//hellotalk.com/r/":  QR_HT_USER_PROFILE,
	"//hellotalk.com/g/":  QR_HT_GROUP_INFO,
	"//www.hellotalk.com": QR_HT_OTHER_INFO,
	"//hellotalk.com":     QR_HT_OTHER_INFO,
}

type QRInfoBody struct {
	Url       string   //文件URL
	QrUrl     []string //QR_LINE 二进制拼接
	QrCode    uint32   //QR_LINE 二进制拼接
	AllowedQr uint8    //这个QR是否允许
	ImgId     uint8    //第几张图片
}

type PornInfoBody struct {
	Rate   float64 //得分
	IsSexy uint8   //是不是性感
	IsPorn uint8   //判断是不是色情
	Url    string  //文件URL
	ImgId  uint8   //第几张图片
}

type SelfieInfoBody struct {
	Url        string //文件URL
	IsSelfie   uint8
	SelfieRate float64
	IsNeedHide uint8
	ImgId      uint8  //第几张图片
	PeopleName string //人脸识别的的名字
}

type AdInfoBody struct {
	Rate         float64 //得分
	NeedBlock    uint8
	IsReviewOnly uint8
}

type ImageResult struct {
	PornInfo   []PornInfoBody
	QrcodeInfo QRInfoBody
	SelfieInfo SelfieInfoBody
	AdInfo     AdInfoBody
}

type ImageBodyReqbody struct {
	ThumbUrl string //小图的url
	BigUrl   string //大图的url。腾讯云环境下，客户端提交的时候只提交大图url
	Width    uint32
	Height   uint32
	Id       int //检测id
}

func NewImageFilter(AccessId, AccessKey string, infoLog *log.Logger) *ImageFilter {

	pt := &ImageFilter{
		infoLog:   infoLog,
		aliFilter: NewAliFilter(AccessId, AccessKey, infoLog),
	}

	return pt
}

func (this *ImageFilter) DataIdUnMarshal(DataId string) (mid string, height uint32, width uint32, err error) {

	Arr := strings.Split(DataId, "-")

	if len(Arr) < 3 {
		return "", 0, 0, errors.New("unmarshal failed")
	}

	Mid := Arr[0]

	Height, err := (strconv.Atoi(Arr[1]))
	Width, err := (strconv.Atoi(Arr[2]))

	return Mid, uint32(Height), uint32(Width), nil
}

func (this *ImageFilter) DoCheck(Mid string, ImageBodyList []*ImageBodyReqbody, scences []string) (*ImageResult, error) {

	path := ALI_CHECK_URI
	clientInfo := ClientInfo{}

	tasks := make([]AliTask, len(ImageBodyList))

	// dataId 构造图片的数据ID， Mid-width-height
	for k, v := range ImageBodyList {
		Url := v.BigUrl

		ret := strings.Contains(Url, "aliyuncs.com")
		if ret && v.Width > v.Height && v.Width > 550 {
			Url = Url + "@0o_1l_500w_96q"

			v.Height = uint32(float64(v.Height) * float64(float64(500)/float64(v.Width)))
			v.Width = 500

		}

		if ret && v.Height >= v.Width && v.Width > 580 {
			Url = Url + "@0o_1l_300w_96q"
			v.Height = uint32(float64(v.Height) * float64(float64(300)/float64(v.Width)))
			v.Width = 300
		}

		tasks[k].DataId = fmt.Sprintf("%s-%d-%d-%d", Mid, v.Height, v.Width, v.Id)
		tasks[k].Url = Url
	}

	data := &AliFilterBizData{
		Scences: scences,
		Tasks:   tasks,
	}

	Resp, err := this.aliFilter.GetResponse(path, clientInfo, data)

	if err != nil {
		this.infoLog.Printf("DoCheck() GetResponse=%s ,err=%s", Mid, err.Error())
		return nil, err
	}

	if Resp.Code != 200 {
		this.infoLog.Printf("DoCheck() Resp msg = %s", Resp.Msg)
		return nil, errors.New("bad response code")
	}

	if len(Resp.Data) == 0 {
		return nil, errors.New("bad reasp data length")
	}

	// this.infoLog.Printf("DoCheck() Resp=%#v", Resp)

	result := &ImageResult{
		QrcodeInfo: QRInfoBody{},
		PornInfo:   []PornInfoBody{},
		SelfieInfo: SelfieInfoBody{},
		AdInfo:     AdInfoBody{},
	}
	this.GetResultFromRespData(result, Mid, Resp.Data, scences, 0)

	this.infoLog.Printf("DoCheck() Mid=%s result=%#v", Mid, result)

	return result, nil
}

//check is image selfie
func (this *ImageFilter) GetResultFromRespData(result *ImageResult, Mid string, List []TaskResult, scences []string, RetryCount uint32) bool {

	// porns := []PornInfoBody{}
	// qrBody := QRInfoBody{}
	// IsSelfie := uint8(0)

	retyTasks := []AliTask{}

	for k1, v1 := range List {

		if v1.Code == 200 {

			for _, v2 := range v1.Results {

				if "porn" == v2.Scene {

					IsSexy := uint8(0)
					IsPorn := uint8(0)

					if v2.Suggestion == "block" {
						IsPorn = 1
					}
					if v2.Suggestion == "review" {
						IsSexy = 1
					}

					if (IsSexy > 0 || IsPorn > 0) && v2.Rate > 55 {

						this.infoLog.Printf("%s find porn=%#v", Mid, v2)

						one := PornInfoBody{
							Rate:   v2.Rate,
							IsSexy: IsSexy,
							IsPorn: IsPorn,
							Url:    v1.Url,
							ImgId:  uint8(k1),
						}
						result.PornInfo = append(result.PornInfo, one)
					}

				} else if "ad" == v2.Scene {

					result.AdInfo.Rate = v2.Rate

					if v2.Suggestion == "block" {
						result.AdInfo.NeedBlock = 1
					}

					if v2.Suggestion == "review" && v2.Rate > 97.0 {
						// result.AdInfo.NeedBlock = 1
						result.AdInfo.IsReviewOnly = 1
					}

				} else if "qrcode" == v2.Scene {

					if len(v2.QrcodeData) == 0 {
						continue
					}

					for _, qrurl := range v2.QrcodeData {

						qrurl = strings.ToLower(qrurl)

						result.QrcodeInfo.Url = v1.Url

						this.infoLog.Printf("%s find qrcode url=%s", Mid, qrurl)

						for url1, code := range QrCodeUrlMap {

							ret := strings.Contains(qrurl, url1)
							if ret {
								result.QrcodeInfo.ImgId = uint8(k1)
								result.QrcodeInfo.QrCode = result.QrcodeInfo.QrCode | code
								result.QrcodeInfo.QrUrl = append(result.QrcodeInfo.QrUrl, qrurl)
								break
							}
						}
					}

				} else if "sface" == v2.Scene {

					if len(v2.SfaceData) == 0 {
						continue
					}

					ImgMid, OrgHeight, OrgWidth, err := this.DataIdUnMarshal(v1.DataId)

					if err != nil || ImgMid != Mid {
						this.infoLog.Println("GetResultFromRespData()", v1.DataId, " DataIdUnMarshal failed:err", err)
						continue
					}

					this.infoLog.Printf("GetResultFromRespData() Mid=%s find face SfaceData=%#v", ImgMid, v2.SfaceData)

					HeiRate := float32(1.31)

					peoRate := float64(0)

					for k2, OneFace := range v2.SfaceData {
						Sqr := float64((HeiRate * OneFace.Width * OneFace.Height) / (float32(OrgHeight) * float32(OrgWidth)))

						this.infoLog.Printf("GetResultFromRespData() Mid=%s find face in %d,Sqrt=%f", ImgMid, k2, Sqr)

						if Sqr > result.SelfieInfo.SelfieRate {
							result.SelfieInfo.ImgId = uint8(k1)
							result.SelfieInfo.SelfieRate = Sqr
							result.SelfieInfo.IsSelfie = 1
							result.SelfieInfo.Url = v1.Url
						}

						for _, vUser := range OneFace.Faces {
							if peoRate < vUser.Rate {
								result.SelfieInfo.PeopleName = vUser.Name
							}
						}

					}

				}
			}

		} else {
			this.infoLog.Println(Mid, "GetResultFromRespData() failed to checkimage,err=", v1.Msg, "Code=", v1.Code, v1.Url)
			if v1.Msg == "DOWNLOAD_TIMEOUT" {

				item := AliTask{
					Url:    v1.Url,
					DataId: v1.DataId,
				}

				retyTasks = append(retyTasks, item)
			}
		}

	}

	if len(retyTasks) > 0 {

		if RetryCount == 0 {

			RetryCount++

			data := &AliFilterBizData{
				Scences: []string{"porn", "qrcode", "sface"},
				Tasks:   retyTasks,
			}

			clientInfo := ClientInfo{}

			Resp, err := this.aliFilter.GetResponse(ALI_CHECK_URI, clientInfo, data)

			if err != nil {
				this.infoLog.Printf("GetResultFromRespData() GetResponse=%s ,err=%s", Mid, err.Error())
				return false
			}

			if Resp.Code != 200 {
				this.infoLog.Printf("GetResultFromRespData() fail Resp msg = %s", Resp.Msg)
				return false
			}

			if len(Resp.Data) == 0 {
				this.infoLog.Printf("GetResultFromRespData() bad reasp data length")
				return false
			}

			this.infoLog.Printf("GetResultFromRespData() Resp=%#v", Resp)

			this.GetResultFromRespData(result, Mid, Resp.Data, scences, RetryCount)
		} else {
			this.infoLog.Printf("GetResultFromRespData() error RetryCount >=", RetryCount)
			return false
		}
	}

	return true

}

func (this *ImageFilter) CheckSelfie() bool {

	return false
}

//check is image Sexual
func (this *ImageFilter) CheckSexual() {

}

//check is qrcode inncluded
func (this *ImageFilter) IsQrCode() uint32 {
	return 0
}
