package common

import (
	"database/sql"
	"log"
	"os"
	"time"

	"github.com/jessevdk/go-flags"
	"github.com/jinzhu/gorm"
	"gopkg.in/ini.v1"
)

type Options struct {
	// Example of verbosity with level
	Verbose []bool `short:"v" long:"verbose" description:"Verbose output"`

	// Example of optional value
	ServerConf string `short:"c" long:"conf" description:"Server Config" optional:"no"`

	TestCount int `short:"n" long:"testcount" description:"test cout"`
}

func InitLogAndOption(options *Options, infoLog **log.Logger) (conf *ini.File, err error) {

	var parser = flags.NewParser(options, flags.Default)

	// 处理命令行参数
	if _, err := parser.Parse(); err != nil {
		log.Fatalln("InitLogAndOption() main parse cmd line failed!")
	}

	if options.ServerConf == "" {
		log.Fatalln("InitLogAndOption() main Must input config file name")
	}

	// 读取配置文件
	cfg, err := ini.Load([]byte(""), options.ServerConf)
	if err != nil {
		log.Fatalln("main load config file=%s failed", options.ServerConf)
		return nil, nil
	}

	// 配置文件只读 设置此标识提升性能
	cfg.BlockMode = false

	// 定义一个文件
	fileName := cfg.Section("LOG").Key("path").MustString("")

	if fileName != "" {
		logFile, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		//defer logFile.Close()
		if err != nil {
			log.Fatalln("open file error !,fileName=", fileName)
			return nil, nil
		}

		// 创建一个日志对象
		*infoLog = log.New(logFile, "[Info]", log.LstdFlags)
	} else {
		*infoLog = log.New(os.Stdout, "[Info]", log.LstdFlags)
	}

	// 配置log的Flag参数
	(*infoLog).SetFlags((*infoLog).Flags() | log.LstdFlags)

	return cfg, nil
}

func InitMySqlFromSection(cfg *ini.File, infoLog *log.Logger, secName string, OpenCount int, OdleCount int) (*sql.DB, error) {

	infoLog.Printf("InitMySqlFromSection() name=%s,Idle=%d,Open=%d start", secName, OdleCount, OpenCount)

	// init mysql
	mysqlHost := cfg.Section(secName).Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section(secName).Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section(secName).Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section(secName).Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section(secName).Key("mysql_port").MustString("3306")

	mydb, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=10s")
	if err != nil {
		infoLog.Println("open ", secName, " failed", time.Now())
		return nil, err
	}

	if err := mydb.Ping(); err != nil {
		infoLog.Fatalln(secName, " ping err", err)
		return nil, err
	}

	mydb.SetMaxIdleConns(OdleCount)

	mydb.SetMaxOpenConns(OpenCount)

	infoLog.Printf("InitMySqlFromSection() name=%s,Idle=%d,Open=%d end", secName, OdleCount, OpenCount)

	return mydb, nil
}

func InitMySqlFromSectionWithMb4(cfg *ini.File, infoLog *log.Logger, secName string, OpenCount int, OdleCount int) (*sql.DB, error) {

	infoLog.Printf("InitMySqlFromSectionWithMb4() name=%s,Idle=%d,Open=%d start", secName, OdleCount, OpenCount)

	// init mysql
	mysqlHost := cfg.Section(secName).Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section(secName).Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section(secName).Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section(secName).Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section(secName).Key("mysql_port").MustString("3306")

	mydb, err := sql.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8mb4&timeout=10s")
	if err != nil {
		infoLog.Println("open ", secName, " failed", time.Now())
		return nil, err
	}

	if err := mydb.Ping(); err != nil {
		infoLog.Fatalln(secName, " ping err", err)
		return nil, err
	}

	mydb.SetMaxIdleConns(OdleCount)

	mydb.SetMaxOpenConns(OpenCount)

	infoLog.Printf("InitMySqlFromSectionWithMb4() name=%s,Idle=%d,Open=%d end", secName, OdleCount, OpenCount)

	return mydb, nil
}

// gorm db connecttion
func InitGormFromDb(dialect string, db *sql.DB) (*gorm.DB, error) {
	mydb, err := gorm.Open(dialect, db)
	return mydb, err
}

//gorm db
func InitMySqlGormSection(cfg *ini.File, infoLog *log.Logger, secName string, OpenCount int, OdleCount int) (*gorm.DB, error) {

	infoLog.Printf("InitMySqlGormSection() name=%s,Idle=%d,Open=%d start", secName, OdleCount, OpenCount)

	// init mysql
	mysqlHost := cfg.Section(secName).Key("mysql_host").MustString("127.0.0.1")
	mysqlUser := cfg.Section(secName).Key("mysql_user").MustString("IMServer")
	mysqlPasswd := cfg.Section(secName).Key("mysql_passwd").MustString("hello")
	mysqlDbName := cfg.Section(secName).Key("mysql_db").MustString("HT_IMDB")
	mysqlPort := cfg.Section(secName).Key("mysql_port").MustString("3306")

	mydb, err := gorm.Open("mysql", mysqlUser+":"+mysqlPasswd+"@"+"tcp("+mysqlHost+":"+mysqlPort+")/"+mysqlDbName+"?charset=utf8&timeout=10s")
	if err != nil {
		infoLog.Println("open ", secName, " failed", time.Now())
		return nil, err
	}

	if err := mydb.DB().Ping(); err != nil {
		infoLog.Fatalln(secName, " ping err", err)
		return nil, err
	}

	mydb.DB().SetMaxIdleConns(OdleCount)

	mydb.DB().SetMaxOpenConns(OpenCount)

	infoLog.Printf("InitMySqlGormSection() name=%s,Idle=%d,Open=%d end", secName, OdleCount, OpenCount)

	return mydb, nil
}

func CheckError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
