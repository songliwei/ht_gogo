package common

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"regexp"
	"strings"
	"sync"
)

const (
	CHECK_MOMENT  = 2
	CHECK_COMMENT = 3

	CHECK_NICKNAME     = 5
	CHECK_SIGNATURE    = 6
	CHECK_CUSTOMER_MSG = 7

	CHECK_IP_REG_ADDRESS = 8 //客服添加的 註冊延時處理的 IP地址。不做正則只做全詞過濾

	CMD_UPDATE_MOMENT_WORDS    = 1002 //从db里面更新信息流贴文关键词
	CMD_UPDATE_COMMENT_WORDS   = 1003 //从db里面更新comment敏感词
	CMD_UPDATE_NICKNAME_WORDS  = 1011 //从db里面更新 nickname 敏感词
	CMD_UPDATE_SIGNATURE_WORDS = 1012 //从db里面更新 自我介绍 敏感词
	CMD_UPDATE_CUSTOMER_MSG    = 1013 //从db里面更新 客服消息 敏感词

	CMD_UPDATE_IP_REG_ADDRESS = 1014 //从db里面更新 注册的骗子的ip地址

	CMD_UPDATE_MG_CONFIG = 1020 //从mgdb.mg_config_values里面更新  cheater_ip_country 定义骗子的ip国家

	CONST_REGEX_TPL = `((^|[^A-Za-z|\xc3\x80|\xc3\x81|\xc3\x82|\xc3\x83|\xc3\x84|\xc3\x85|\xc3\x86|\xc3\x87|\xc3\x88|\xc3\x89|\xc3\x8a|\xc3\x8b|\xc3\x8c|\xc3\x8d|\xc3\x8e|\xc3\x8f|\xc3\x90|\xc3\x91|\xc3\x92|\xc3\x93|\xc3\x94|\xc3\x95|\xc3\x96|\xc3\x98|\xc3\x99|\xc3\x9a|\xc3\x9b|\xc3\x9c|\xc3\x9d|\xc3\x9e|\xc3\x9f|\xc3\xa0|\xc3\xa1|\xc3\xa2|\xc3\xa3|\xc3\xa4|\xc3\xa5|\xc3\xa6|\xc3\xa7|\xc3\xa8|\xc3\xa9|\xc3\xaa|\xc3\xab|\xc3\xac|\xc3\xad|\xc3\xae|\xc3\xaf|\xc3\xb1|\xc3\xb2|\xc3\xb3|\xc3\xb4|\xc3\xb5|\xc3\xb6|\xc3\xb8|\xc3\xb9|\xc3\xba|\xc3\xbb|\xc3\xbc|\xc3\xbd|\xc3\xbe|\xc3\xbf])(?<name>($english$))([^A-Za-z|\xc3\x80|\xc3\x81|\xc3\x82|\xc3\x83|\xc3\x84|\xc3\x85|\xc3\x86|\xc3\x87|\xc3\x88|\xc3\x89|\xc3\x8a|\xc3\x8b|\xc3\x8c|\xc3\x8d|\xc3\x8e|\xc3\x8f|\xc3\x90|\xc3\x91|\xc3\x92|\xc3\x93|\xc3\x94|\xc3\x95|\xc3\x96|\xc3\x98|\xc3\x99|\xc3\x9a|\xc3\x9b|\xc3\x9c|\xc3\x9d|\xc3\x9e|\xc3\x9f|\xc3\xa0|\xc3\xa1|\xc3\xa2|\xc3\xa3|\xc3\xa4|\xc3\xa5|\xc3\xa6|\xc3\xa7|\xc3\xa8|\xc3\xa9|\xc3\xaa|\xc3\xab|\xc3\xac|\xc3\xad|\xc3\xae|\xc3\xaf|\xc3\xb1|\xc3\xb2|\xc3\xb3|\xc3\xb4|\xc3\xb5|\xc3\xb6|\xc3\xb8|\xc3\xb9|\xc3\xba|\xc3\xbb|\xc3\xbc|\xc3\xbd|\xc3\xbe|\xc3\xbf]|$)+?|((?<cname>($cjk$))))`

	CONST_REGEX_TPL2 = "((([^A-Za-z|À|Á|Â|Ã|Ä|Å|Æ|Ç|È|É|Ê|Ë|Ì|Í|Î|Ï|Ð|Ñ|Ò|Ó|Ô|Õ|Ö|Ø|Ù|Ú|Û|Ü|Ý|Þ|ß|à|á|â|ã|ä|å|æ|ç|è|é|ê|ë|ì|í|î|ï|ñ|ò|ó|ô|õ|ö|ø|ù|ú|û|ü|ý|þ|ÿ])|^)(?P<name>(?i)($english$))(([^A-Za-z|À|Á|Â|Ã|Ä|Å|Æ|Ç|È|É|Ê|Ë|Ì|Í|Î|Ï|Ð|Ñ|Ò|Ó|Ô|Õ|Ö|Ø|Ù|Ú|Û|Ü|Ý|Þ|ß|à|á|â|ã|ä|å|æ|ç|è|é|ê|ë|ì|í|î|ï|ñ|ò|ó|ô|õ|ö|ø|ù|ú|û|ü|ý|þ|ÿ]|$)+?)|((?P<cname>($cjk$))))"

	CONST_GROUP_REGEX_TPL = "((([^A-Za-z|À|Á|Â|Ã|Ä|Å|Æ|Ç|È|É|Ê|Ë|Ì|Í|Î|Ï|Ð|Ñ|Ò|Ó|Ô|Õ|Ö|Ø|Ù|Ú|Û|Ü|Ý|Þ|ß|à|á|â|ã|ä|å|æ|ç|è|é|ê|ë|ì|í|î|ï|ñ|ò|ó|ô|õ|ö|ø|ù|ú|û|ü|ý|þ|ÿ]|^)($english$)([^A-Za-z|À|Á|Â|Ã|Ä|Å|Æ|Ç|È|É|Ê|Ë|Ì|Í|Î|Ï|Ð|Ñ|Ò|Ó|Ô|Õ|Ö|Ø|Ù|Ú|Û|Ü|Ý|Þ|ß|à|á|â|ã|ä|å|æ|ç|è|é|ê|ë|ì|í|î|ï|ñ|ò|ó|ô|õ|ö|ø|ù|ú|û|ü|ý|þ|ÿ]|$))|($cjk$))"
)

type HTContentRegexGroup struct {
	mainRegex   *regexp.Regexp
	groupRegex  *regexp.Regexp
	groupCount  map[string][]string
	wordsAction map[string][]uint32 //定义某些敏感词命中之后直接做某些操作
	Locker      *sync.RWMutex
}

type TextFilter struct {
	momentFilter *HTContentRegexGroup

	NumberRegex      *regexp.Regexp
	CJKRegex         *regexp.Regexp
	AllLanguage      *regexp.Regexp
	SourceJoinedWord string //字符拼接的元字符串

	CheckType         uint32
	REG_LANG_CODE_MAP map[string]uint32
	Mgdb              *sql.DB
	infoLog           *log.Logger
}

func NewTextFiler(Mgdb *sql.DB, CheckType uint32, infoLog *log.Logger) *TextFilter {

	NumberRegex := regexp.MustCompile(`^([0-9])+$`)
	CJKRegex := regexp.MustCompile(`[\p{Han}|\p{Hangul}|\p{Hiragana}|\p{Katakana}]+`)
	AllLanguage := regexp.MustCompile(`((?P<Hiragana>[\p{Hiragana}]+)|(?P<Katakana>[\p{Katakana}]+)|(?P<Han>[\p{Han}]+)|(?P<Hangul>[\p{Hangul}]+)|(?P<Thai>[\p{Thai}]+)|(?P<Arabic>[\p{Arabic}]+))`)

	pt := &TextFilter{
		infoLog: infoLog,
		Mgdb:    Mgdb,
		momentFilter: &HTContentRegexGroup{
			groupRegex:  nil,
			mainRegex:   nil,
			wordsAction: map[string][]uint32{},
			groupCount:  map[string][]string{},
			Locker:      new(sync.RWMutex),
		},
		CheckType:        CheckType,
		NumberRegex:      NumberRegex,
		CJKRegex:         CJKRegex,
		SourceJoinedWord: "",
		AllLanguage:      AllLanguage,
		REG_LANG_CODE_MAP: map[string]uint32{
			"Hiragana": 5,
			"Katakana": 5,
			"Han":      2,
			"Hangul":   6,
			"Thai":     17,
			"Arabic":   13,
		},
	}

	pt.RefreshRegex()

	return pt
}

func (this *TextFilter) IsCJK(text string) bool {

	ret := this.CJKRegex.FindAllString(text, -1)

	return len(ret) > 0

}

func (this *TextFilter) IsNumber(text string) bool {

	ret := this.NumberRegex.FindAllString(text, -1)
	// this.infoLog.Panicln("IsNumber ", len(ret), text)
	return len(ret) > 0

}

func (this *TextFilter) DropSympos(text string) string {
	//防止用户在文本里面添加字符来规避
	checkText := text

	noDot := strings.Replace(text, ".", "", -1)

	if noDot != text {
		checkText = checkText + " " + noDot
	}

	noHahTag := strings.Replace(text, "#", "", -1)
	if noHahTag != text {
		checkText = checkText + " " + noHahTag
	}

	noBar := strings.Replace(text, "-", "", -1)
	if noBar != text {
		checkText = checkText + " " + noBar
	}

	noStar := strings.Replace(text, "*", "", -1)
	if noStar != text {
		checkText = checkText + " " + noStar
	}

	return checkText
}

func (this *TextFilter) CheckLanguage(text string) uint32 {

	if this.AllLanguage == nil {
		return 0
	}

	names := this.AllLanguage.SubexpNames()

	singleMainMatch := this.AllLanguage.FindAllStringSubmatch(text, -1)

	result := make(map[string]string)

	for k, name := range names {
		if k != 0 && name != "" {
			for i := 0; i < len(singleMainMatch); i++ {
				if singleMainMatch[i][k] != "" {
					result[name] = singleMainMatch[i][k]
				}
			}
		}
	}

	//Hiragana Katakana Han Hangul Thai Arabic

	if len(result) > 0 {
		for k, v := range this.REG_LANG_CODE_MAP {
			_, _ok := result[k]
			if _ok {
				return v
			}
		}
	}

	return 0

}

func (this *TextFilter) InitRegex() error {
	return this.RefreshRegex()
}

func (this *TextFilter) RefreshRegex() error {

	this.infoLog.Printf("RefreshRegex() start CheckType=%d", this.CheckType)

	err := this.LoadTextGroupsFromDb(this.CheckType, "moment", this.momentFilter)

	return err

}

//从mgdb.mg_config_values加载 数组
func (this *TextFilter) LoadConfigString(keytyp string, typ string) (string, error) {

	var val string

	err := this.Mgdb.QueryRow("SELECT `value` from mg_config_values WHERE `type`=? and `key` =? LIMIT 1", typ, keytyp).Scan(&val)

	if err != nil {
		return "", err
	}
	return val, nil
}

//从mgdb.mg_config_values 加载 数组
func (this *TextFilter) LoadConfigUint32(keytyp string, typ string) (uint32, error) {

	var val uint32

	err := this.Mgdb.QueryRow("SELECT `value` from mg_config_values WHERE `type`=? and `key`=? LIMIT 1", typ, keytyp).Scan(&val)

	if err != nil {
		return 0, err
	}
	return val, nil

}

func (this *TextFilter) LoadTextGroupsFromDb(typ uint32, name string, htreg *HTContentRegexGroup) error {

	rows, err := this.Mgdb.Query("SELECT  words,one_or_more,ban_action from mg_words_sensitive WHERE `status`=1 and type=?", typ)

	if err != nil {
		return err
	}

	defer rows.Close()

	if err == sql.ErrNoRows {
		return errors.New("LoadTextGroupsFromDb()  no data ")
	}

	singleCjkGroup := []string{}
	singleLatinGroup := []string{}

	words := ""
	one_or_more := uint32(0)

	idx := uint32(0)
	action := ""

	latinGroupRegName := []string{}
	CjkGroupRegName := []string{}
	groupCount := map[string][]string{}
	wordsAction := map[string][]uint32{}
	for rows.Next() {

		if err := rows.Scan(&words, &one_or_more, &action); err != nil {
			this.infoLog.Printf("LoadTextGroupsFromDb() rows.Scan failed")
			continue
		}

		words = strings.ToLower(strings.Trim(words, " "))

		isCjk := this.IsCJK(words)
		isNumber := this.IsNumber(words)

		cont := strings.Contains(words, "#")

		if one_or_more == 2 || cont {
			gp := strings.Split(words, "#")

			gpCount := (len(gp))

			if gpCount == 0 {
				continue
			}

			regStr := strings.Replace(words, "#", "|", -1)
			idx++
			key := ""
			if isCjk || isNumber {
				key = fmt.Sprintf("cname%d", idx)
				CjkGroupRegName = append(CjkGroupRegName, fmt.Sprintf("(?P<%s>(%s))", key, regStr))

			} else {
				key = fmt.Sprintf("name%d", idx)
				latinGroupRegName = append(latinGroupRegName, fmt.Sprintf("(?P<%s>(%s))", key, regStr))
			}
			this.infoLog.Printf("multi typ=%d[%s],text=%s,regStr=%s", typ, name, key, regStr)

			groupCount[key] = gp
			if action != "" {
				wordsAction[key] = SplitStringToUint32Array(action)
			}
		}

		if one_or_more == 1 {
			if isCjk || isNumber {
				singleCjkGroup = append(singleCjkGroup, words)
			} else {
				singleLatinGroup = append(singleLatinGroup, words)
			}

			if action != "" {
				wordsAction[words] = SplitStringToUint32Array(action)
			}
		}

	}

	htreg.Locker.Lock()
	defer htreg.Locker.Unlock()

	for k := range htreg.groupCount {
		delete(htreg.groupCount, k)
	}

	for k := range htreg.wordsAction {
		delete(htreg.wordsAction, k)
	}

	htreg.wordsAction = wordsAction
	htreg.groupCount = groupCount

	fakeLatin := "abcafmlzxpl"
	fakeCJK := "媭嫚"

	if len(singleLatinGroup) == 0 && len(singleCjkGroup) == 0 {
		htreg.mainRegex = nil
	} else {

		if len(singleLatinGroup) == 0 {
			singleCjkGroup = []string{fakeLatin}
		} else if len(singleCjkGroup) == 0 {
			singleLatinGroup = []string{fakeCJK}
		}

		this.infoLog.Printf("========mainRegex for %s============", name)
		htreg.mainRegex = this.genPcre(CONST_REGEX_TPL2, singleLatinGroup, singleCjkGroup, "singleword")
	}

	if len(CjkGroupRegName) == 0 && len(latinGroupRegName) == 0 {
		htreg.groupRegex = nil
	} else {

		if len(CjkGroupRegName) == 0 {
			htreg.groupCount["cname1"] = []string{fakeCJK}
			CjkGroupRegName = []string{fmt.Sprintf("(?P<cname1>(%s))", fakeCJK)}
		} else if len(latinGroupRegName) == 0 {
			htreg.groupCount["name1"] = []string{fakeLatin}
			latinGroupRegName = []string{fmt.Sprintf("(?P<name1>(%s))", fakeLatin)}
		}

		this.infoLog.Printf("========groupRegex for %s============", name)
		htreg.groupRegex = this.genPcre(CONST_GROUP_REGEX_TPL, latinGroupRegName, CjkGroupRegName, "multiword")

	}

	//构造批量处理正则表达式

	return nil

}

func (this *TextFilter) IsTextIllegally(text string, mid string) (string, []uint32, error) {

	if len(text) == 0 {
		return "", []uint32{}, errors.New("empty text")
	}

	//为检测IP 做单独的处理。

	if this.CheckType == CHECK_IP_REG_ADDRESS {
		ids, ok := this.momentFilter.wordsAction[text]
		if ok {
			return text, ids, nil
		} else {
			return "", []uint32{}, nil
		}
	}

	return this.Check(&text, this.momentFilter)

}

func (this *TextFilter) Check(textp *string, htreg *HTContentRegexGroup) (string, []uint32, error) {

	text := strings.ToLower(*textp)
	text = strings.Replace(text, "\r\n", " ", -1)
	text = strings.Replace(text, "\n", " ", -1)
	//text = strings.Replace(text, "　", "  ", -1)

	_action := []uint32{}
	_ok := false
	_foundText := ""

	if nil == htreg.mainRegex {
		return "", _action, fmt.Errorf("mainRegex is empty")
	}

	htreg.Locker.RLock()
	defer htreg.Locker.RUnlock()

	names := htreg.mainRegex.SubexpNames()

	singleMainMatch := htreg.mainRegex.FindAllStringSubmatch(text, -1)

	result := make(map[string]string)

	for k, name := range names {
		if k != 0 && name != "" {
			for i := 0; i < len(singleMainMatch); i++ {
				if singleMainMatch[i][k] != "" {
					result[name] = singleMainMatch[i][k]
				}
			}
		}
	}

	_foundText, _ok = result["name"]

	if _ok { // 不存在直接打印日志返回错误
		_action, _ok = htreg.wordsAction[_foundText]
		return _foundText, _action, nil
	}

	_foundText, _ok = result["cname"]
	if _ok {
		_action, _ok = htreg.wordsAction[_foundText]
		return _foundText, _action, nil
	}

	if htreg.groupRegex == nil {
		return "", _action, fmt.Errorf("groupRegex is empty")
	}

	hostGroupMatch := htreg.groupRegex.FindAllStringSubmatch(text, -1)

	groupResult := map[string](map[string]bool){}
	//映射 cname下 多个关键词的存在情况

	names = htreg.groupRegex.SubexpNames()

	for _, name := range names {
		if name != "" {
			groupResult[name] = map[string]bool{}
		}
	}

	found := false
	for _, match := range hostGroupMatch {
		for i, kp := range match {
			if i != 0 && kp != "" && names[i] != "" {
				groupResult[names[i]][kp] = true
				found = true
			}
		}
	}

	if found == false {
		return "", _action, fmt.Errorf("group not find")
	}

	for k, matches := range groupResult {

		_groupKeys, _ok := htreg.groupCount[k]
		if !_ok {
			this.infoLog.Printf("Check() key=%s not count in groupCount", k)
			continue
		}

		_action, _ok = htreg.wordsAction[k]

		if len(matches) == 0 {
			continue
			this.infoLog.Printf("Check() key=%s no matched results ", k)
		}

		if len(matches) == len(_groupKeys) {
			txts := []string{}
			for txt, _ := range matches {
				txts = append(txts, txt)
			}
			return strings.Join(txts, "#"), _action, nil

		} else {

			matchCount := int(0)
			matchedKeys := []string{}

			// this.infoLog.Printf("Check() count less key=%s found %#v,%#v ", k, matches, htreg.groupCount)

			for _, v2 := range _groupKeys {

				if true == strings.Contains(text, v2) {
					matchCount++
					matchedKeys = append(matchedKeys, v2)
				}
			}

			if matchCount == len(_groupKeys) {
				this.infoLog.Printf("Check() key=%s,text=%s found %#v ,then all match", k, text, matchedKeys)
				return strings.Join(matchedKeys, "#"), _action, nil
			}

			this.infoLog.Printf("Check() key=%s found %#v ,but count not match,match=%d,Keys=%d", k, matches, matchCount, len(_groupKeys))
		}

	}

	return "", _action, fmt.Errorf("Check() no result")

}

func (this *TextFilter) genPcre(tpl string, latin []string, cjk []string, name string) (p *regexp.Regexp) {

	latin_str := strings.Join(latin, "|")
	// this.infoLog.Println("latin_str:", latin_str)
	patern := strings.Replace(tpl, "$english$", latin_str, -1)

	cjk_str := strings.Join(cjk, "|")
	// this.infoLog.Println("cjk_str:", cjk_str)
	patern = strings.Replace(patern, "$cjk$", cjk_str, -1)

	this.infoLog.Println(name, " patern:", patern)
	return regexp.MustCompile(patern)
}
