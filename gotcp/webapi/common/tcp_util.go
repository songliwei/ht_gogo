package common

import (
	"errors"
	"fmt"
	"github.com/HT_GOGO/gotcp"
	tcpcommon "github.com/HT_GOGO/gotcp/tcpfw/common"
	"github.com/HT_GOGO/gotcp/tcpfw/include/ht_moment"
	"github.com/golang/protobuf/proto"
	"time"
)

func SendResponseCode(c *gotcp.Conn, reqHead *tcpcommon.HeadV2, err error) {

	if err != nil {
		SendRetCode(c, reqHead, 500, err.Error())
	} else {
		SendRetCode(c, reqHead, 0, "OK")
	}

}

func SendRetCode(c *gotcp.Conn, reqHead *tcpcommon.HeadV2, ret uint16, errMsg string) (bool, error) {
	head := new(tcpcommon.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}

	head.Len = uint32(tcpcommon.PacketV2HeadLen + len(errMsg) + 1) //整个报文长度
	head.Ret = ret
	buf := make([]byte, head.Len)
	buf[0] = tcpcommon.HTV2MagicBegin
	err := tcpcommon.SerialHeadV2ToSlice(head, buf[1:])
	if err != nil {
		return false, fmt.Errorf("SendRetCode SerialHeadV2ToSlice failed")
	}
	copy(buf[tcpcommon.PacketV2HeadLen:], []byte(errMsg)) // return code
	buf[head.Len-1] = tcpcommon.HTV2MagicEnd

	rspPacket := tcpcommon.NewHeadV2Packet(buf)
	c.AsyncWritePacket(rspPacket, time.Second)
	return true, nil
}

func SendV2RspBuffer(c *gotcp.Conn, reqHead *tcpcommon.HeadV2, bodyBuf []byte) (bool, error) {

	head := new(tcpcommon.HeadV2)
	if reqHead != nil {
		*head = *reqHead
	}

	head.Ret = 0
	head.Len = tcpcommon.HeadV2Len + 2 + uint32(len(bodyBuf))
	sendBuf := make([]byte, head.Len)

	sendBuf[0] = tcpcommon.HTV2MagicBegin
	err := tcpcommon.SerialHeadV2ToSlice(head, sendBuf[1:])
	if err != nil {
		return false, fmt.Errorf("SendV2RspBuffer SerialHeadV2ToSlice failed")
	}
	copy(sendBuf[tcpcommon.PacketV2HeadLen:], bodyBuf) // return code
	sendBuf[head.Len-1] = tcpcommon.HTV2MagicEnd

	rspPacket := tcpcommon.NewHeadV2Packet(sendBuf)
	err = c.AsyncWritePacket(rspPacket, time.Second)

	if err != nil {
		return false, err
	}

	return true, err
}

func SendMomentRsp(c *gotcp.Conn, reqHead *tcpcommon.HeadV2, rspBody *ht_moment.RspBody) (bool, error) {

	bodyBuf, err := proto.Marshal(rspBody)
	if err != nil {

		return false, fmt.Errorf("SendRsp Failed to proto.Marshal err=%s", err.Error())
	}

	return SendV2RspBuffer(c, reqHead, bodyBuf)
}

func SendMomentStatusCode(c *gotcp.Conn, reqHead *tcpcommon.HeadV2, code ht_moment.RET_CODE, des string) (bool, error) {

	if reqHead.Cmd == uint32(ht_moment.CMD_TYPE_CMD_GET_DEFAULT_MOMENT_TAG) {

		rspBody := &ht_moment.RspBody{
			GetDefaultMomentTagRspbody: &ht_moment.GetDefautMomentTagRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(code)),
					Reason: []byte(des),
				},
			},
		}

		return SendMomentRsp(c, reqHead, rspBody)
	}

	if reqHead.Cmd == uint32(ht_moment.CMD_TYPE_CMD_SEARCH_MOMENT_TAG) {

		rspBody := &ht_moment.RspBody{
			SearchMomentTagRspbody: &ht_moment.SearchMomentTagRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(code)),
					Reason: []byte(des),
				},
			},
		}

		return SendMomentRsp(c, reqHead, rspBody)
	}

	if reqHead.Cmd == uint32(ht_moment.CMD_TYPE_CMD_GET_MID_LIST_BY_TAG) {

		rspBody := &ht_moment.RspBody{
			GetMidListByTagRspbody: &ht_moment.GetMidListByTagRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(code)),
					Reason: []byte(des),
				},
			},
		}

		return SendMomentRsp(c, reqHead, rspBody)
	}

	if reqHead.Cmd == uint32(ht_moment.CMD_TYPE_CMD_POST_COMMENT) {

		rspBody := &ht_moment.RspBody{
			PostCmntRspbody: &ht_moment.PostCommentRspBody{
				Status: &ht_moment.Header{
					Code:   proto.Uint32(uint32(code)),
					Reason: []byte(des),
				},
			},
		}

		return SendMomentRsp(c, reqHead, rspBody)
	}

	return false, errors.New("not set")
}
